Func<string, string, bool> EqualsIgnoreCase = (left, right) =>
{
    return string.Equals(left, right,  StringComparison.OrdinalIgnoreCase);
};

Func<string, string, bool> StartsWithIgnoreCase = (left, right) =>
{
    return string.Equals(left, right,  StringComparison.OrdinalIgnoreCase);
};

Func<string, string, bool> EndsWithIgnoreCase = (left, right) =>
{
    return string.Equals(left, right,  StringComparison.OrdinalIgnoreCase);
};

Func<SolutionParserResult, string, SolutionProject> GetProject = (sol, projName) =>
{
    return sol.Projects.SingleOrDefault(p => p.Name.Equals(projName, StringComparison.OrdinalIgnoreCase));
};

Func<string, string> GetSetting = (settingPath) =>
{
    var settingsFile = File(Argument("settings", ""));
    return GetSettingValue(settingsFile.Path, settingPath);
};

Action<MSBuildSettings> SetDefaultBuildSettings = settings =>
{
    var verbosity = (Verbosity) Enum.Parse(typeof(Verbosity), Argument("MsBuildVerbosity", "Quiet"), ignoreCase: true);
    settings.UseToolVersion(MSBuildToolVersion.VS2017)
            .SetMSBuildPlatform(MSBuildPlatform.x86)
            .SetVerbosity(verbosity)
            .WithProperty("RestorePackages" , "false");
};

Func<string, FilePath, GitVersion> GetVersion = (dir, sol) =>
{
    GitVersion gitVersion = null;
    var gitVersionSettings = new GitVersionSettings
    {
        WorkingDirectory = dir
    };

    if (BuildSystem.IsLocalBuild)
    {
        gitVersion = GitVersion(gitVersionSettings);
    }
    else
    {
        Information("\n---> Updating AssemblyInfo version for solution {0} ...\n", sol);
        gitVersionSettings.UpdateAssemblyInfo = true;
        gitVersion = GitVersion(gitVersionSettings);

        SerializeJsonToFile($"{workDir}/gitversion.json", gitVersion);
    }
    return gitVersion;
};

Func<string, List<AppPlatform>> GetAppPlatforms = (val) => 
{
    if (EqualsIgnoreCase(val, "all"))     return new List<AppPlatform> { AppPlatform.Android, AppPlatform.IOS };
    if (EqualsIgnoreCase(val, "ios"))     return new List<AppPlatform> { AppPlatform.IOS };
    if (EqualsIgnoreCase(val, "android")) return new List<AppPlatform> { AppPlatform.Android };
    throw new Exception($"Invalid AppPlatform :{val}");
};

Func<string, AppBuildConfiguration> GetBuildConfig = (val) => 
{
    if (EqualsIgnoreCase(val, "debug"))    return AppBuildConfiguration.Debug;
    if (EqualsIgnoreCase(val, "adhoc"))    return AppBuildConfiguration.AdHoc;
    if (EqualsIgnoreCase(val, "appstore")) return AppBuildConfiguration.AppStore;
    throw new Exception($"Invalid AppBuildConfiguration :{val}");
};

Func<AppPlatform, AppBuildConfiguration, string, string, SolutionProject, GitVersion, DirectoryPath, AppBuildSettings> NewAppBuildSettings = (appPlatform, buildConfig, appName, packageName, project, versionInfo, outputDir) => 
{
    var appBuildsettings = new AppBuildSettings
    {
        AppPlatform   = appPlatform,
        Configuration = buildConfig,
        AppName       = appName,
        PackageName   = packageName,
        Project       = project,
        OutputDir     = outputDir,
        Version       = versionInfo
    };

    if (appPlatform == AppPlatform.Android) {
        appBuildsettings.Target   = "SignAndroidPackage"; // PackageForAndroid or SignAndroidPackage
        appBuildsettings.Platform = "Android";
    }
    else {
        appBuildsettings.Target   = "Build";
        appBuildsettings.Platform = "iPhone";
    }
    return appBuildsettings;
};

Func<AppBuildSettings, FilePath> BuildXamarinApp = (appBuildSettings) =>
{
    UpdateXamarinManifest(appBuildSettings);
    var artifact = XamarinBuild(appBuildSettings, settings =>
    {
        SetDefaultBuildSettings(settings);
        if(appBuildSettings.AppPlatform == AppPlatform.IOS && IsRunningOnWindows())
        {
            var serverUser     = GetSetting("$.macos.user");
            var serverAddress  = GetSetting("$.macos.host");
            var serverPassword = GetSetting("$.macos.password");

            settings.WithProperty("ServerAddress" , serverAddress)
                    .WithProperty("ServerUser"    , serverUser)
                    .WithProperty("ServerPassword", serverPassword);
        } 
        else if (appBuildSettings.AppPlatform == AppPlatform.Android)
        {
            if (appBuildSettings.Configuration != AppBuildConfiguration.Debug) 
            {
                var config = appBuildSettings.Configuration.ToString().ToLower();
                var keyStorePass = GetSetting("$.android.keyStorePassword");
                var keyAlias     = GetSetting($"$.android.{config}.keyAlias");
                var keyPass      = GetSetting($"$.android.{config}.keyPassword");

                settings.WithProperty("AndroidSigningStorePass", keyStorePass)
                        .WithProperty("AndroidSigningKeyAlias" , keyAlias)
                        .WithProperty("AndroidSigningKeyPass"  , keyPass);
            }
        }
    });

    return artifact;
};

Func<AppBuildSettings, FilePath> PackageXamarinApp = (appBuildSettings) =>
{
    var input = BuildXamarinApp(appBuildSettings);
    var output = $"{appBuildSettings.OutputDir.FullPath}/{appBuildSettings.PackageName}-{appBuildSettings.Version.FullSemVer}-{appBuildSettings.Version.Sha.Substring(0, 7)}{input.GetExtension()}";

    EnsureDirectoryExists(appBuildSettings.OutputDir);
    CopyFile(input, output);
    return output;
};

Func<FilePath, string, string, object> UploadAppArtifact = (artifact, appCenterProject, appCenterProjectApiKey) =>
{
    var osExtension = artifact.GetExtension() == ".apk" ? "-Android" : "-iOS";
	
	Information("\n--->  AppCenterProject: {0}", appCenterProject);
	Information("\n--->  AppCenterProjectApiKey: {0}", appCenterProjectApiKey);
	
	Information("\n--->  LogIn to AppCenter with api: {0}", appCenterProjectApiKey);
	var loginOutput = StartProcess("appcenter", new ProcessSettings{Arguments = $"login --token {appCenterProjectApiKey}"});
    Information("Process output login: {0}", loginOutput);

    var appCenterProjectName = "";
    if(artifact.FullPath.Contains(".control."))
    {
        appCenterProjectName = $"{appCenterProject}-Control{osExtension}";
    }
    else if(artifact.FullPath.Contains(".config."))
    {
        appCenterProjectName = $"{appCenterProject}-Config{osExtension}";
    }
    else if(artifact.FullPath.Contains(".install."))
    {
        appCenterProjectName = $"{appCenterProject}-Install{osExtension}";
    }
	else
    {
        appCenterProjectName = $"{appCenterProject}{osExtension}";
    }
    
	
	Information("\n--->  Uploading artifact {0} to {1}", artifact, appCenterProjectName);
	var uploadOutput = StartProcess("appcenter", new ProcessSettings{Arguments = $"distribute release --app OSRAM-APP-Dev/{appCenterProjectName} -g Collaborators -f {artifact}"});
    Information($"\n---> Process output upload: {uploadOutput}");
    return null;
};