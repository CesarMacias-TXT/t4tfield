#tool  nuget:?package=GitVersion.CommandLine&version=3.6.5
#addin nuget:?package=Cake.Osram.Utils&version=1.0.0

var workDir        = "..";
var artifactsDir   = MakeAbsolute(new DirectoryPath($"{workDir}/artifacts"));
var slnFile        = GetFiles($"{workDir}/*.sln").Single();
GitVersion version = null;

#load "common.cake"
#load "../build/overrides.cake"
//#load "../build/tests.cake"

var target        = Argument("target", "Default");
var type          = Argument("apptype", "All");
var platform      = Argument("platform", "All");
var configuration = Argument("configuration", "Debug");
var appCenterProject = Argument("appcenterproject", EnvironmentVariable("APPCENTER_PROJECT"));
var appCenterProjectApiKey = Argument("appcenterprojectapikey", EnvironmentVariable("APPCENTER_PROJECT_APIKEY"));
/// Common
Setup(context =>
{
    version = GetVersion(workDir, slnFile);
    Information("\n--->  Is Local Build = {0}, Target = {1}, Configuration = {2}, AppType = {3}, Platform = {4}, AppCenterProject = {5}, appCenterProjectApiKey ={6}\n", BuildSystem.IsLocalBuild, target, configuration, type, platform, appCenterProject, appCenterProjectApiKey);
    EnsureDirectoryExists(artifactsDir);
});

Teardown(context =>
{
    Information("--->  Configuration = {0}, AppType = {1}, Platform = {2}\n", configuration, type, platform);
    Information("\n--->  Version: {0}", $"{version.FullSemVer}-{version.Sha.Substring(0, 7)}");
    Information("--->  Commits Since last Version: {0}", version.CommitsSinceVersionSource);
});

Task("Restore_Packages")
    .Does(() =>
    {
        Information("\n---> Restoring packages {0} ...\n", slnFile);

        NuGetRestore(slnFile);
    });

Task("Build")
    .IsDependentOn("Restore_Packages")
    .Does(() =>
    {
        var appConfig    = GetBuildConfig(configuration);
        var appTypes     = GetAppTypes(type);
        var appPlatforms = GetAppPlatforms(platform);

        foreach(var appPlatform in appPlatforms) {
            foreach(var appType in appTypes) {
                Information($"\n--->  BUILD_{appPlatform.ToString()}_{ToString(appType)}_{appConfig.ToString()}\n");
                var appBuildsettings = GetAppBuildSettings(slnFile, appType, appPlatform, appConfig, version, artifactsDir);

                if (appBuildsettings == null) {
                    Information($"\n---> Skipping. Project not found for Platform: {appPlatform.ToString()}, App Type: {ToString(appType)}, App Config: {appConfig.ToString()}\n");
                } else {
                    BuildXamarinApp(appBuildsettings);
                }
            }
        }
    });

Task("Package")
    .IsDependentOn("Restore_Packages")
    .Does(() =>
    {
        CleanDirectory(artifactsDir);

        var appConfig    = GetBuildConfig(configuration);
        var appTypes     = GetAppTypes(type);
        var appPlatforms = GetAppPlatforms(platform);

        foreach(var appPlatform in appPlatforms) {
            foreach(var appType in appTypes) {
                Information($"\n--->  PACKAGE_{appPlatform.ToString()}_{ToString(appType)}_{appConfig.ToString()}\n");
                var appBuildsettings = GetAppBuildSettings(slnFile, appType, appPlatform, appConfig, version, artifactsDir);

                if (appBuildsettings == null) {
                    Information($"\n---> Skipping. Project not found for Platform: {appPlatform.ToString()}, App Type: {ToString(appType)}, App Config: {appConfig.ToString()}\n");
                } else {
                    var artifact = PackageXamarinApp(appBuildsettings);
                }
            }
        }
    });

Task("Publish_AdHoc")
    .Does(() =>
    {
        var artifacts = GetFiles($"{artifactsDir}/*.*").ToList();
        foreach(var artifact in artifacts) {
            UploadAppArtifact(artifact, appCenterProject, appCenterProjectApiKey);
        }
    });

Task("Publish_AppStore")
    .Does(() =>
    {
        var artifacts = GetFiles($"{artifactsDir}/*.*").ToList();

        foreach(var artifact in artifacts) {
            // UploadTestFairyAppArtifact(artifact);
        }
    });

Task("AlTool")
    .Does(() =>
    {
        HelpItunesConnect();
    });

Task("Help")
    .Does(() =>
    {
        Information("\n--->  Help Information ...\n");
    });

Task("Default")
    .IsDependentOn("Help");

RunTarget(target);