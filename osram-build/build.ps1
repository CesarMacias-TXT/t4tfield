##########################################################################
# This is the Cake bootstrapper script for PowerShell.
# This file was downloaded from https://github.com/cake-build/resources
# Feel free to change this file to fit your needs.
##########################################################################

<#

.SYNOPSIS
This is a Powershell script to bootstrap a Cake build.

.DESCRIPTION
This Powershell script will download NuGet if missing, restore NuGet tools (including Cake)
and execute your Cake build script with the parameters you provide.

.PARAMETER Script
The build script to execute.
.PARAMETER Target
The build script target to run.
.PARAMETER Configuration
The build configuration to use.
.PARAMETER AppType
The app type to build for.
.PARAMETER AppPlatform
The app platform to build for.
.PARAMETER SettingsFile
The settings file.
.PARAMETER Verbosity
.PARAMETER MsBuildVerbosity
.PARAMETER AppCenterProject
.PARAMETER AppCenterProjectApiKey
Specifies the amount of information to be displayed.
.PARAMETER DryRun
Performs a dry run of the build script.
No tasks will be executed.
.PARAMETER ScriptArgs
Remaining arguments are added here.

.LINK
http://cakebuild.net

#>

[CmdletBinding()]
Param(
    [string]$Script = "build.cake",
    [ValidateSet("Build", "Package", "Publish_AdHoc", "Publish_AppStore", "Help", "Default", "Restore_Packages")]
    [string]$Target = "Default",
    [ValidateSet("Debug", "AdHoc", "AppStore")]
    [string]$Configuration = "Debug",
    [ValidateSet("All", "Control", "Config")]
    [string]$AppType = "All",
    [ValidateSet("All", "IOS", "Android")]
    [string]$AppPlatform = "All",
    [string]$SettingsFile = "settings.json",
    [ValidateSet("Quiet", "Minimal", "Normal", "Verbose", "Diagnostic")]
    [string]$Verbosity = "Normal",
    [ValidateSet("Quiet", "Minimal", "Normal", "Verbose", "Diagnostic")]
    [string]$MsBuildVerbosity = "Quiet",
    [Alias("WhatIf", "Noop")]
	[string]$AppCenterProject = "",
	[string]$AppCenterProjectApiKey = "",
    [switch]$DryRun,
    [Parameter(Position=0,Mandatory=$false,ValueFromRemainingArguments=$true)]
    [string[]]$ScriptArgs
)

function MD5HashFile([string] $filePath) {
    if ([string]::IsNullOrEmpty($filePath) -or !(Test-Path $filePath -PathType Leaf)) {
        return $null
    }

    return (Get-FileHash $filePath -Algorithm MD5).Hash
}
function GetProxyEnabledWebClient {
    $wc = New-Object System.Net.WebClient
    $proxy = [System.Net.WebRequest]::GetSystemWebProxy()
    $proxy.Credentials = [System.Net.CredentialCache]::DefaultCredentials
    $wc.Proxy = $proxy
    return $wc
}
function Execute-Command([string] $cmd) {
    $hostExe = ""
    if ($IsOSX -eq $true -or $IsMacOS -eq $true) { $hostExe = "`"mono`"" }
    return Invoke-Expression "& $hostExe $cmd"
}
function Restore-NugetPackages([string] $dir, [string] $message) {
    if (Test-Path (Join-Path $dir "packages.config")) {
        Push-Location
        Set-Location $dir

        Write-Verbose -Message "Restoring $message from NuGet..."
        $NuGetOutput = Execute-Command "`"$NUGET_EXE`" install -ExcludeVersion -OutputDirectory `"$dir`""

        if ($LASTEXITCODE -ne 0) {
            Throw "An error occured while restoring NuGet $message."
        }
        Write-Verbose -Message ($NuGetOutput | out-string)

        Pop-Location
    }
}

Write-Host "Preparing to run build script..."

if(!$PSScriptRoot){
    $PSScriptRoot = Split-Path $MyInvocation.MyCommand.Path -Parent
}

$TOOLS_DIR   = Join-Path $PSScriptRoot "tools"
$ADDINS_DIR  = Join-Path $TOOLS_DIR "Addins"
$MODULES_DIR = Join-Path $TOOLS_DIR "Modules"
$NUGET_EXE   = Join-Path $TOOLS_DIR "nuget.exe"
$CAKE_EXE    = Join-Path $TOOLS_DIR "Cake/Cake.exe"
$Script      = Join-Path $PSScriptRoot $Script
$NUGET_URL   = "https://dist.nuget.org/win-x86-commandline/latest/nuget.exe"

$TOOLS_PACKAGES_CONFIG = Join-Path $TOOLS_DIR "packages.config"

# Make sure tools folder exists
if ((Test-Path $PSScriptRoot) -and !(Test-Path $TOOLS_DIR)) {
    Write-Verbose -Message "Creating tools directory..."
    New-Item -Path $TOOLS_DIR -Type directory | out-null
}

# Make sure that packages.config exist.
if (!(Test-Path $TOOLS_PACKAGES_CONFIG)) {
    Write-Verbose -Message "Downloading packages.config..."
    try {
        $wc = GetProxyEnabledWebClient
        $wc.DownloadFile("https://cakebuild.net/download/bootstrapper/packages", $TOOLS_PACKAGES_CONFIG) } catch {
        Throw "Could not download packages.config."
    }
}

# Try find NuGet.exe in path if not exists
if (!(Test-Path $NUGET_EXE)) {
    Write-Verbose -Message "Trying to find nuget.exe in PATH..."
    $existingPaths = $Env:Path -Split ';' | Where-Object { (![string]::IsNullOrEmpty($_)) -and (Test-Path $_ -PathType Container) }
    $NUGET_EXE_IN_PATH = Get-ChildItem -Path $existingPaths -Filter "nuget.exe" | Select-Object -First 1
    if ($NUGET_EXE_IN_PATH -ne $null -and (Test-Path $NUGET_EXE_IN_PATH.FullName)) {
        Write-Verbose -Message "Found in PATH at $($NUGET_EXE_IN_PATH.FullName)."
        $NUGET_EXE = $NUGET_EXE_IN_PATH.FullName
    }
}

# Try download NuGet.exe if not exists
if (!(Test-Path $NUGET_EXE)) {
    Write-Verbose -Message "Downloading NuGet.exe..."
    try {
        $wc = GetProxyEnabledWebClient
        $wc.DownloadFile($NUGET_URL, $NUGET_EXE)
    } catch {
        Throw "Could not download NuGet.exe."
    }
}

# Save nuget.exe path to environment to be available to child processed
$ENV:NUGET_EXE = $NUGET_EXE

# Restore tools from NuGet?
Restore-NugetPackages $TOOLS_DIR "tools"

# Restore addins from NuGet
Restore-NugetPackages $ADDINS_DIR "addins"

# Restore modules from NuGet
Restore-NugetPackages $MODULES_DIR "modules"

# Make sure that Cake has been installed.
if (!(Test-Path $CAKE_EXE)) {
    Throw "Could not find Cake.exe at $CAKE_EXE"
}

# Build the argument list.
$Arguments = @{
    target=$Target;
    configuration=$Configuration;
    apptype=$AppType;
    platform=$AppPlatform;
    settings=$SettingsFile;
    verbosity=$Verbosity;
    msbuildverbosity=$MsBuildVerbosity;
	appcenterproject=$AppCenterProject;
	appcenterprojectapikey=$AppCenterProjectApiKey;
}.GetEnumerator() | ForEach-Object { "--{0}=`"{1}`"" -f $_.key, $_.value };

# Start Cake
Write-Host "Running build script..."
Execute-Command "`"$CAKE_EXE`" `"$Script`" $Arguments $UseDryRun $ScriptArgs"
exit $LASTEXITCODE
