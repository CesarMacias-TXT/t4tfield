﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MvvmCross.Core.ViewModels;
using MvvmCross.Platform.Plugins;
using Osram.TFTBackend;

namespace zT4TFieldConsole
{
    public class App
        : MvxApplication
    {
        public override void Initialize()
        {
            base.Initialize();
            BackendSetup.InitIoC();
        }
    }
}
