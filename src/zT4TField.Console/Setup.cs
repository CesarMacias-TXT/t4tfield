﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MvvmCross.Core.ViewModels;
using MvvmCross.Console.Platform;
using MvvmCross.Platform.Platform;
using MvvmCross.Platform.Plugins;
using Osram.T4TField.Droid;
using Osram.TFTBackend;

namespace zT4TFieldConsole
{
    public class Setup : MvxConsoleSetup 
    {
        protected override IMvxApplication CreateApp()
        {
            return new App();
        }

        protected override void InitializeIoC()
        {
            base.InitializeIoC();
        }

        protected override IMvxTrace CreateDebugTrace()
        {
            return new DebugOnlyTrace();
        }

    }
}
