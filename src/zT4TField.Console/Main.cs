﻿using System;
using Osram.TFTBackend;
using Osram.TFTBackend.ImportService.Contracts;

namespace zT4TFieldConsole
{
    public class Main
    {

        private void EcecuteRaffaele()
        {
            Logger.Trace("Console Raffaele:");
        }

        private void EcecuteDavide()
        {
            Logger.Trace("Console Davide:");
        }

        private void EcecuteJanos()
        {
            Logger.Trace("Console Janos:");
        }

        private void EcecuteBert()
        {
            Logger.Trace("Console Bert:");
        }

        public void Execute(String user)
        {
            if (user == "bert")
            {
                EcecuteBert();
                return;
            }
            if (user == "janos")
            {
                EcecuteJanos();
                return;
            }
            if (user == "davide")
            {
                EcecuteDavide();
                return;
            }
            if (user == "raffaele")
            {
                EcecuteRaffaele();
                return;
            }
        }
    }
}
