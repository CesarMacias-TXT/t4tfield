﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Remoting.Messaging;
using System.Text;
using System.Threading.Tasks;

namespace zT4TFieldConsole
{
    class Program
    {
        static void Main(string[] args)
        {
            Setup setup = new Setup();
            setup.Initialize();

            if (args.Length < 1)
            {
                Console.WriteLine("Provide an user name as programm commandline argument!!!");
                Console.WriteLine();
            }
            else
            {
                Main main = new Main();
                main.Execute(args[0]);
            }

            Console.WriteLine();
            Console.WriteLine("zT4TFieldConsole done.");
            Console.WriteLine("Press any key to quit.");
            Console.ReadKey(true);
        }
    }
}
