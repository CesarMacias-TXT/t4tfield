﻿using System;
using System.Text;
using System.Collections.Generic;
using System.IO;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Osram.TFTBackend.PersistenceService;

namespace UnitTestProject.PersistenceService
{
    /// <summary>
    /// Summary description for EcgDeviceInfoExtractorTests
    /// </summary>
    [TestClass]
    public class EcgDeviceInfoExtractorTests
    {
        public EcgDeviceInfoExtractorTests()
        {
            //
            // TODO: Add constructor logic here
            //
        }

        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        //
        // You can use the following additional attributes as you write your tests:
        //
        // Use ClassInitialize to run code before running the first test in the class
        // [ClassInitialize()]
        // public static void MyClassInitialize(TestContext testContext) { }
        //
        // Use ClassCleanup to run code after all tests in a class have run
        // [ClassCleanup()]
        // public static void MyClassCleanup() { }
        //
        // Use TestInitialize to run code before running each test 
        // [TestInitialize()]
        // public void MyTestInitialize() { }
        //
        // Use TestCleanup to run code after each test has run
        // [TestCleanup()]
        // public void MyTestCleanup() { }
        //
        #endregion

        [TestMethod]
        public void ExtractFromLuminaireXmlTest()
        {
            String xml = @"<?xml version=""1.0"" encoding=""utf-8""?>
    <LuminaireConfiguration>
                <MetaData>
                    <Description/>
                </MetaData>
                <ECGDescriptions>
                    <ECGDescription>
                        <MetaData>
                            <BasicCode>AB32361</BasicCode>
                            <DeviceDescription>Test</DeviceDescription>
                            <FW_Major>1</FW_Major>
                            <GTIN>4052899324879</GTIN>
                            <HW_Major>1</HW_Major>
                        </MetaData>
                        <SubDevices>
                        </SubDevices>
                    </ECGDescription>
                </ECGDescriptions>
    </LuminaireConfiguration>";

            using (Stream xmlStream = new MemoryStream(Encoding.UTF8.GetBytes(xml)))
            {
                EcgDeviceInfoExtractor extractor = new EcgDeviceInfoExtractor();
                EcgDeviceInfo info = extractor.Extract(xmlStream);
                Assert.AreEqual("AB32361", info.BasicCode);
                Assert.AreEqual(4052899324879, info.Gtin);
                Assert.AreEqual("Test", info.Description);
            }
        }

        [TestMethod]
        public void ExtractFromDescriptionXmlTest()
        {
            String xml = @"<?xml version=""1.0"" encoding=""utf-8""?>
                    <ECGDescription>
                        <MetaData>
                            <GTIN>4052899517424</GTIN>
                            <BasicCode>AM08361</BasicCode>
                            <DeviceTypeName>OT 40/170/240/1A0 1DIMLT2 G2 CE</DeviceTypeName>
                            <FW_Major>0</FW_Major>
                            <HW_Major>1</HW_Major>
                            <LEDSetVersion>2</LEDSetVersion>
                            <Slowprogramming>No</Slowprogramming>
                            <DeviceDescription>Engineering Sample</DeviceDescription>
                        </MetaData>
                        <SubDevices>
                        </SubDevices>
                    </ECGDescription>
";

            using (Stream xmlStream = new MemoryStream(Encoding.UTF8.GetBytes(xml)))
            {
                EcgDeviceInfoExtractor extractor = new EcgDeviceInfoExtractor();
                EcgDeviceInfo info = extractor.Extract(xmlStream);
                Assert.AreEqual("AM08361", info.BasicCode);
                Assert.AreEqual(4052899517424, info.Gtin);
                Assert.AreEqual("Engineering Sample", info.Description);
            }
        }
    }
}
