﻿using System;
using System.Text;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Moq.Language.Flow;
using Osram.TFTBackend.CryptographyService.Contracts;
using Osram.TFTBackend.FileService.Contracts;
using Osram.TFTBackend.PersistenceService;
using Osram.TFTBackend.PersistenceService.Contracts;

namespace UnitTestProject.PersistenceService
{
    /// <summary>
    /// Summary description for PersistenceServiceTests
    /// </summary>
    [TestClass]
    public class PersistenceServiceTests
    {
        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        //
        // You can use the following additional attributes as you write your tests:
        //
        // Use ClassInitialize to run code before running the first test in the class
        // [ClassInitialize()]
        // public static void MyClassInitialize(TestContext testContext) { }
        //
        // Use ClassCleanup to run code after all tests in a class have run
        // [ClassCleanup()]
        // public static void MyClassCleanup() { }
        //
        // Use TestInitialize to run code before running each test 
        // [TestInitialize()]
        // public void MyTestInitialize() { }
        //
        // Use TestCleanup to run code after each test has run
        // [TestCleanup()]
        // public void MyTestCleanup() { }
        //
        #endregion

        [TestMethod]
        public async Task ImportConfigurationTest()
        {
            var fileServiceMock = new Mock<IFileService>();
            fileServiceMock.Setup(o => o.CopyStreamToFile(It.IsAny<Stream>(), It.Is<String>(s => !s.Contains("42")))).Throws(new ArgumentException());
            var configurationDataStoreMock = new Mock<IConfigurationDataStore>();
            configurationDataStoreMock.Setup(o => o.InsertConfiguration(It.Is<EcgDeviceInfo>(info => info.Gtin == 4052899517424))).ReturnsAsync(42);
            configurationDataStoreMock.Setup(o => o.GetFileName(It.IsAny<int>(), It.IsAny<String>())).Returns((int id, String ext) => $"{id}");
            var cryptoMock = new Mock<ICryptographyService>();
            IPersistenceService ps = new Osram.TFTBackend.PersistenceService.PersistenceService(configurationDataStoreMock.Object, fileServiceMock.Object, cryptoMock.Object);

            String xml = @"<?xml version=""1.0"" encoding=""utf-8""?>
                    <ECGDescription>
                        <MetaData>
                            <GTIN>4052899517424</GTIN>
                            <BasicCode>AM08361</BasicCode>
                            <DeviceTypeName>OT 40/170/240/1A0 1DIMLT2 G2 CE</DeviceTypeName>
                            <FW_Major>0</FW_Major>
                            <HW_Major>1</HW_Major>
                            <LEDSetVersion>2</LEDSetVersion>
                            <Slowprogramming>No</Slowprogramming>
                            <DeviceDescription>Engineering Sample</DeviceDescription>
                        </MetaData>
                        <SubDevices>
                        </SubDevices>
                    </ECGDescription>
";
            using (Stream picStream = new MemoryStream(new byte[] { 0, 1, 2, 3 }))
            using (Stream xmlStream = new MemoryStream(Encoding.UTF8.GetBytes(xml)))
            {
                await ps.ImportConfiguration("file name", xmlStream, picStream);
            }
        }
    }
}
