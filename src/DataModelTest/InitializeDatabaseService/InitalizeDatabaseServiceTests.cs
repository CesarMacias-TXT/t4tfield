﻿using System;
using System.Text;
using System.Collections.Generic;
using System.IO;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Osram.TFTBackend.FileService.Contracts;
using Osram.TFTBackend.ImportService.Contracts;
using Osram.TFTBackend.InitalizeDatabaseService;
using Osram.TFTBackend.InitalizeDatabaseService.Contracts;
using Osram.TFTBackend.Services.Contracts;

namespace UnitTestProject.InitializeDatabaseService
{
    /// <summary>
    /// Summary description for InitalizeDatabaseServiceTests
    /// </summary>
    [TestClass]
    public class InitalizeDatabaseServiceTests
    {
        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        //
        // You can use the following additional attributes as you write your tests:
        //
        // Use ClassInitialize to run code before running the first test in the class
        // [ClassInitialize()]
        // public static void MyClassInitialize(TestContext testContext) { }
        //
        // Use ClassCleanup to run code after all tests in a class have run
        // [ClassCleanup()]
        // public static void MyClassCleanup() { }
        //
        // Use TestInitialize to run code before running each test 
        // [TestInitialize()]
        // public void MyTestInitialize() { }
        //
        // Use TestCleanup to run code after each test has run
        // [TestCleanup()]
        // public void MyTestCleanup() { }
        //
        #endregion

        [TestMethod]
        public void TestResourceNameSplitting()
        {
            var importService = new Mock<IImportService>();
            // Indirect check as the splitted resource name is passed as argument to the import function
            // Expected argument here is the config name + extension without the resource namespaces
            importService.Setup(o => o.Import(It.Is<String>(s => s != "Config1.ostrud"), It.IsAny<Stream>())).Throws(new ArgumentException());

            var resourceService = new Mock<IResourceService>();
            resourceService.Setup(o => o.GetManifestResourceNames()).Returns(new String[] { "Osram.Resources.InitialConfigurations.Config1.ostrud" });
            var fileService = new Mock<IFileService>();
            var dbVersion = new Mock<IDatabaseVersionDataStore>();
            IInitalizeDatabaseService sut = new InitalizeDatabaseService(importService.Object, resourceService.Object, fileService.Object, dbVersion.Object);

            sut.Initialize();
        }
        [TestMethod]

        public void TestResourceSelection()
        {
            var importService = new Mock<IImportService>();
            // Indirect check as the splitted resource name is passed as argument to the import function
            // Expectes is only Config1 as Config2 is not located in the InitialConfigurations directory
            importService.Setup(o => o.Import(It.Is<String>(s => s != "Config1.ostrud"), It.IsAny<Stream>())).Throws(new ArgumentException());

            var resourceService = new Mock<IResourceService>();
            resourceService.Setup(o => o.GetManifestResourceNames()).Returns(new String[]
            {
                "Osram.Resources.InitialConfigurations.Config1.ostrud",
                "Osram.Resources.Config2.ostrud"
            });
            var fileService = new Mock<IFileService>();
            var dbVersion = new Mock<IDatabaseVersionDataStore>();
            IInitalizeDatabaseService sut = new InitalizeDatabaseService(importService.Object, resourceService.Object, fileService.Object, dbVersion.Object);

            sut.Initialize();
        }
    }
}
