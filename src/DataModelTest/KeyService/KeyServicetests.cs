﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Osram.TFTBackend.KeyService;

namespace UnitTestProject.KeyService
{
    [TestClass]
    public class KeyServiceTests
    {
        [TestMethod]
        public void TestEncryptPassword()
        {
            var encryptedPassword = Pin.EncryptMsk(0x01020304);
            Assert.AreEqual(0xc88398e3, encryptedPassword);

        }
    }
}
