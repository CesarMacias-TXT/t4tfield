﻿
using System.IO;
using System.Xml.Serialization;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace UnitTestProject
{
    /// <summary>
    /// Please NOTE: the Unit Tests here are not PCL compatible.
    /// The difference is the mechanism for loading / deserializing the xml file.
    /// Checks on the format and validity of the loaded in-memory model are applicable in a PCL context also.
    /// </summary>
    [TestClass]
    public class UnitTest1
    {
        private const string OutdoorEcgDeviceFileName = "Data\\ECGs\\AA64275.xml";
        private const string OutputDirectory = "Data\\ECGs";

        public string GetBinPath()
        {
            return Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location);
        }

        [TestMethod()]
        [DeploymentItem(OutdoorEcgDeviceFileName, OutputDirectory)]
        public void FindResourcefile_Test()
        {
            // Arrange
            // already done by [DeploymentItem]

            //  Act
            // (just a simple lookup, nothing really).
            var binPath = GetBinPath();
  
            //  Assert
            Assert.IsNotNull(binPath);
            Assert.IsTrue(File.Exists(OutdoorEcgDeviceFileName));
        }

        [TestMethod()]
        [DeploymentItem(OutdoorEcgDeviceFileName, OutputDirectory)]
        public void LoadEcgDescriptionFile_Test()
        {
            //TODO write new test as deserialization has changed ...
            //Arrange
            //ECGDescription theDevice = new ECGDescription();
            //var serializer = new XmlSerializer(theDevice.GetType());

            //// Act
            //using (var reader = new StreamReader(OutdoorEcgDeviceFileName))
            //{
            //    theDevice = serializer.Deserialize(reader.BaseStream) as ECGDescription;
            //}

            ////Assert
            //if (theDevice != null)
            //{
            //    Assert.IsNotNull(theDevice.SubDevices);
            //}
            //else
            //{
            //    Assert.Fail("Unable to deserialize an ECG device from {0}", OutdoorEcgDeviceFileName);
            //}
        }
    }
}
