﻿using MvvmCross.Forms.Presenters;
using MvvmCross.Platforms.Ios.Core;
using Osram.T4TField.Contracts;
using Osram.T4TField.iOS.Services;
using Osram.T4TField.iOS.ViewPresenter;
using Osram.T4TField.ViewPresenter;
using Osram.TFTBackend.RestService.Contracts;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using UIKit;
using MvvmCross.Platforms.Ios.Presenters;
using MvvmCross.IoC;
using MvvmCross.Logging;
using MvvmCross;

namespace Osram.T4TField.iOS
{
    public partial class Setup : MvxIosSetup
    {
        public Setup(IMvxApplicationDelegate applicationDelegate, UIWindow window)
        {
            base.PlatformInitialize(applicationDelegate, window);
        }

        public Setup(IMvxApplicationDelegate applicationDelegate, IMvxIosViewPresenter presenter)
        {
            base.PlatformInitialize(applicationDelegate, presenter);
        }

        protected override IMvxIosViewPresenter CreateViewPresenter()
        {
            var presenter = new OsramIOSMvxFromsViewPresenter(Window);
            Mvx.IoCProvider.RegisterSingleton<MasterDetailViewPresenter>(presenter);
            Mvx.IoCProvider.RegisterSingleton<IMvxFormsViewPresenter>(presenter);
            return presenter;
        }

        public override IEnumerable<Assembly> GetViewAssemblies()
        {
            return base.GetViewAssemblies().Union(new[] { typeof(OsramMvxApp).GetTypeInfo().Assembly });
        }

        private void InitializePlatformSpecificIoC()
        {
            //var androidUtils = new AndroidUtils();
            //Mvx.RegisterSingleton<IAndroidUtils>(androidUtils);
            //Mvx.RegisterSingleton<ISystemUtils>(androidUtils);
            //Mvx.LazyConstructAndRegisterSingleton<TabGestureService, TabGestureService>();
            Mvx.IoCProvider.LazyConstructAndRegisterSingleton<IAppInfo>(() => new AppInfo());

            Mvx.IoCProvider.LazyConstructAndRegisterSingleton<IT4THttpClient, T4THttpClient>();
        }

        public override MvxLogProviderType GetDefaultLogProviderType()
        {
            return MvxLogProviderType.Console;
        }

    }
}
