using Acr.UserDialogs;
using CoreNFC;
using Foundation;
using MvvmCross;
using MvvmCross.Base;
using MvvmCross.Forms.Platforms.Ios.Core;
using MvvmCross.Plugin.Email;
using MvvmCross.Plugin.Email.Platforms.Ios;
using MvvmCross.Plugin.Messenger;
using MvvmCross.Plugin.ResourceLoader.Platforms.Ios;
using MvvmCross.ViewModels;
using Osram.T4TField.iOS.Services.Nfc;
using Osram.T4TField.Resources;
using Osram.T4TField.ViewPresenter;
using Osram.TFTBackend;
using Osram.TFTBackend.ImportService.Contracts;
using Osram.TFTBackend.Messaging;
using Osram.TFTBackend.NfcService.Contracts;
using Osram.TFTBackend.NfcService.NfcDataTypes;
using Splat;
using System;
using System.IO;
using System.Threading.Tasks;
using UIKit;
using Xamarin.Forms;

namespace Osram.T4TField.iOS
{
    // The UIApplicationDelegate for the application. This class is responsible for launching the
    // User Interface of the application, as well as listening (and optionally responding) to application events from iOS.
    [Register("AppDelegate")]
    public partial class AppDelegate : MvxFormsApplicationDelegate, INFCNdefReaderSessionDelegate
    {

        public static readonly UIColor OsramColor = UIColor.FromRGB(233, 91, 13);

        public override bool FinishedLaunching(UIApplication uiApplication, NSDictionary launchOptions)
        {
            Window = new UIWindow(UIScreen.MainScreen.Bounds);
            Window.RootViewController = new UIViewController();
            Forms.Init();
            UINavigationBar.Appearance.TintColor = OsramColor;
            CarouselView.FormsPlugin.iOS.CarouselViewRenderer.Init();

            var setup = new Setup(this, Window);

            setup.InitializePrimary();
            setup.InitializeSecondary();

            Rg.Plugins.Popup.Popup.Init();

            var presenter = Mvx.IoCProvider.Resolve<MasterDetailViewPresenter>();
            T4TFieldApp app = new T4TFieldApp();
            presenter.FormsApplication = app;

            _messenger = Mvx.IoCProvider.Resolve<IMvxMessenger>();
            InitialiseNfc();

            LoadApplication(app);
            Mvx.IoCProvider.RegisterType<IMvxComposeEmailTask, MvxComposeEmailTask>();
            Mvx.IoCProvider.RegisterType<IMvxResourceLoader, MvxIosResourceLoader>();

            var startup = Mvx.IoCProvider.Resolve<IMvxAppStart>();
            startup.Start();
            Locator.CurrentMutable.InitializeSplat();
            Locator.CurrentMutable.RegisterPlatformBitmapLoader();
            Window.MakeKeyAndVisible();

            return true;
        }

        private async Task<bool> OpenUrlAsync(UIApplication app, NSUrl url)
        {
            Stream stream = null;
            try
            {
                stream = new FileStream(url.Path, FileMode.Open, FileAccess.Read);

                var importService = Mvx.IoCProvider.Resolve<IImportService>();
                var success = await importService.Import(url.LastPathComponent, stream);

                var msg = success
                             ? string.Format(AppResources.F_ImportSuccessful, url.LastPathComponent)
                             : string.Format(AppResources.F_ImportFailed, url.LastPathComponent);

                await Mvx.IoCProvider.Resolve<IUserDialogs>().AlertAsync(msg);

                return success;
            }
            catch (Exception e)
            {
                Logger.Exception("OpenUrl failed", e);
                throw;
            }
            finally
            {
                if (stream != null)
                {
                    stream.Close();
                }
                File.Delete(url.Path);
            }
        }

        public override bool OpenUrl(UIApplication app, NSUrl url, NSDictionary options)
        {
            if (url != null && url.IsFileUrl)
            {
#pragma warning disable CS4014
                OpenUrlAsync(app, url);
#pragma warning restore CS4014

                return true;
            }

            return base.OpenUrl(app, url, options);
        }

        private MobileNfcReader nfcReader;
        private IMvxMessenger _messenger;
        private MvxSubscriptionToken subscriptionToken;

        private void InitialiseNfc()
        {
            nfcReader = new MobileNfcReader(this);
            MvvmCross.Plugins.BLE.iOS.Plugin plugin = new MvvmCross.Plugins.BLE.iOS.Plugin();
            plugin.Load();
            Mvx.IoCProvider.RegisterSingleton<INfcCommunication>(nfcReader);
            Mvx.IoCProvider.RegisterSingleton<IMobileNfcReader>(nfcReader);
            subscriptionToken = _messenger.Subscribe<Events.OnNfcReaderChangedEvent>(RegisterMobileNfcReader);
        }

        private void RegisterMobileNfcReader(Events.OnNfcReaderChangedEvent ev)
        {
            if (ev.ReaderTypeSelected == NfcReaderType.Mobile)
            {
                Mvx.IoCProvider.RegisterSingleton<INfcCommunication>(nfcReader);
                _messenger.Publish(new Events.OnNfcReaderUpdateEvent(this));
            }
        }

        public void DidDetect(NFCNdefReaderSession session, NFCNdefMessage[] messages)
        {
            Logger.Trace("AppDelegate DidDetect");
            nfcReader.OnDidDetect(session, messages);
        }

        public void DidInvalidate(NFCNdefReaderSession session, NSError error)
        {
            Logger.Trace("AppDelegate DidInvalidate");
            //throw new System.NotImplementedException();
        }

        public override void WillTerminate(UIApplication uiApplication)
        {
            _messenger.Unsubscribe<Events.OnNfcReaderChangedEvent>(subscriptionToken);
        }
    }
}

