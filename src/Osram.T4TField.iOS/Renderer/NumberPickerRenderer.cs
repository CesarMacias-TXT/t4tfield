using Osram.T4TField.Controls;
using Osram.T4TField.iOS.Controls;
using Osram.T4TField.iOS.Renderer;
using System.ComponentModel;
using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;

[assembly: ExportRenderer(typeof(XFormNumberPicker), typeof(NumberPickerRenderer))]

namespace Osram.T4TField.iOS.Renderer
{
    public class NumberPickerRenderer : ViewRenderer<XFormNumberPicker, NumberPickerLayout>
    {

        private NumberPickerLayout tp;


        protected override void OnElementChanged(ElementChangedEventArgs<XFormNumberPicker> e)
        {
            base.OnElementChanged(e);

            if (e.OldElement != null || Element == null)
                return;

            if (Control == null)
            {
                CreateNewControl();
            }
        }

        private void CreateNewControl()
        {
            //var context = Forms.Context;
            tp = new NumberPickerLayout(Element.Type, Element.Current, Element.Min, Element.Max);

            tp.valueChange += (int value) =>
            {
                if (Element != null)
                {
                    Element.Current = value;
                }
            };
            SetNativeControl(tp);
        }

        protected override void OnElementPropertyChanged(object sender, PropertyChangedEventArgs e)
        {

            base.OnElementPropertyChanged(sender, e);
        }
    }
}
