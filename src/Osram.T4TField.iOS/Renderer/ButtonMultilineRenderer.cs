﻿using Osram.T4TField.Controls;
using Osram.T4TField.iOS.Renderer;
using UIKit;
using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;

[assembly: ExportRenderer(typeof(ButtonMultiline), typeof(ButtonMultilineRenderer))]

namespace Osram.T4TField.iOS.Renderer
{


    class ButtonMultilineRenderer : ButtonRenderer
    {

        public ButtonMultilineRenderer() : base()
        {

        }

        protected override void OnElementChanged(ElementChangedEventArgs<Button> e)
        {
            base.OnElementChanged(e);

            if (this.Control != null)
            {
                Control.TitleLabel.LineBreakMode = UILineBreakMode.WordWrap;
                Control.TitleLabel.Lines = 0;
                Control.TitleLabel.TextAlignment = UITextAlignment.Center;
            }
        }
    }
}