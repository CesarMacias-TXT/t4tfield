
using Osram.T4TField.Controls;
using Osram.T4TField.iOS.Controls;
using Osram.T4TField.iOS.Renderer;
using System.ComponentModel;
using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;

[assembly: ExportRenderer(typeof(TouchedBarLayout), typeof(BoxLayoutRenderer))]

namespace Osram.T4TField.iOS.Renderer
{
    public class BoxLayoutRenderer : ViewRenderer<TouchedBarLayout, BarIndicator>
    {
        //TODO: private readonly CustomGestureListner _listener;
        //TODO: private readonly GestureDetector _detector;
        private BarIndicator _bar;

        public BoxLayoutRenderer() : base()
        {
            //TODO: _listener = new CustomGestureListner();
            //TODO: _detector = new GestureDetector(_listener);
        }

        protected override void OnElementChanged(ElementChangedEventArgs<TouchedBarLayout> e)
        {
            base.OnElementChanged(e);

            if (e.NewElement == null)
            {
                // this.GenericMotion -= HandleGenericMotion;
                // this.Touch -= HandleTouch;
            }

            if (e.OldElement == null)
            {
                // this.GenericMotion += HandleGenericMotion;
                //  this.Touch += HandleTouch;
            }
            //var context = Forms.Context;
            Color color = Color.FromRgb(0xE9, 0x5B, 0x0D); //TODO: put this value in Application bunlde as "OsramColor"
            _bar = new BarIndicator(Element.AutomationId, Element.Current, Element.Min, Element.Max, Element.Default, Element.IsEnabled, Element.IsValid, color);
            SetNativeControl(_bar);

            _bar.valueChange += (int value) =>
            {
                if (Element != null)
                {
                    Element.Current = value;
                }
            };

            _bar.OnScrollFinished += () => { Element?.OnGestureFinished?.Execute(null); };

            CoreFoundation.DispatchQueue.MainQueue.DispatchAsync(() => Run());
        }


        public void Run()
        {
            int width = _bar.MeasuredWidth;
            _bar.SetHeightAndText(width);
        }

        //TODO:
        /*
        void HandleTouch(object sender, TouchEventArgs e)
        {
            _detector.OnTouchEvent(e.Event);
        }

        void HandleGenericMotion(object sender, GenericMotionEventArgs e)
        {
            _detector.OnTouchEvent(e.Event);
        }
        */

        protected override void OnElementPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            base.OnElementPropertyChanged(sender, e);

            if (Control == null || Element == null) return;
            if (e.PropertyName == TouchedBarLayout.CurrentProperty.PropertyName)
            {
                if (Element.Current != Control.Current)
                {
                    Control.Current = Element.Current;
                }
            }
            if (e.PropertyName == TouchedBarLayout.MinProperty.PropertyName)
            {
                if (Element.Min != Control.LimitedMinValue)
                {
                    Control.LimitedMinValue = Element.Min;
                }
            }
            if (e.PropertyName == TouchedBarLayout.MaxProperty.PropertyName)
            {
                if (Element.Max != Control.LimitedMaxValue)
                {
                    Control.LimitedMaxValue = Element.Max;
                }
            }
            if (e.PropertyName == TouchedBarLayout.IsValidProperty.PropertyName)
            {
                if (Element.IsValid != Control.IsValid)
                {
                    Control.IsValid = Element.IsValid;
                }
            }
            if (e.PropertyName == VisualElement.IsEnabledProperty.PropertyName)
            {
                if (Element.IsEnabled != Control.IsEnabled)
                {
                    Control.IsEnabled = Element.IsEnabled;
                }
            }

        }
    }
}