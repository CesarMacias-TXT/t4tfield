﻿
using Osram.T4TField.Controls;
using Osram.T4TField.iOS.Renderer;
using UIKit;
using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;

[assembly: ExportRenderer(typeof(BindablePicker), typeof(BindablePickerRenderer))]

namespace Osram.T4TField.iOS.Renderer
{


    class BindablePickerRenderer : Xamarin.Forms.Platform.iOS.PickerRenderer
    {

        protected override void OnElementChanged(ElementChangedEventArgs<Picker> e)
        {
            base.OnElementChanged(e);

            if (this.Control != null)
            {

                CoreFoundation.DispatchQueue.MainQueue.DispatchAsync(() =>
                {
                    if (Element == null) return;
                    int FontSize = (int)(Element as BindablePicker)?.FontSize;

                    UITextField label = (UITextField)Control;
                    label.Font = UIFont.SystemFontOfSize(FontSize);
                    // UITextField is already single line only

                    //TODO: check these
                    //label.SetIncludeFontPadding(false);
                    //label.SetLineSpacing(-5, -5);
                });
            }
        }



    }
}