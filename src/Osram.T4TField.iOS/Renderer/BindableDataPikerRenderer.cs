﻿
using Osram.T4TField.Controls;
using Osram.T4TField.iOS.Renderer;
using UIKit;
using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;

[assembly: ExportRenderer(typeof(BindableDataPicker), typeof(BindableDataPikerRenderer))]

namespace Osram.T4TField.iOS.Renderer
{


    class BindableDataPikerRenderer : DatePickerRenderer
    {

        protected override void OnElementChanged(ElementChangedEventArgs<Xamarin.Forms.DatePicker> e)
        {
            base.OnElementChanged(e);

            if (this.Control != null)
            {
                CoreFoundation.DispatchQueue.MainQueue.DispatchAsync(() =>
                {
                    if (Element == null) return;

                    int FontSize = (int)(Element as BindableDataPicker)?.FontSize;

                    UITextField label = (UITextField)Control;
                    label.Font = UIFont.SystemFontOfSize(FontSize);

                    // UITextField is already single line only


                    //TODO: check these
                    //label.SetIncludeFontPadding(false);
                    //label.SetLineSpacing(-5, -5);
                });

            }
        }



    }
}