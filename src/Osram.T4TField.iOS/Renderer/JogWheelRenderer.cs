﻿using Osram.T4TField.Controls.SliderWheel;
using Osram.T4TField.iOS.Controls;
using Osram.T4TField.iOS.Renderer;
using System;
using System.ComponentModel;
using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;

[assembly: ExportRenderer(typeof(SliderWheelView), typeof(JogWheelRenderer))]

namespace Osram.T4TField.iOS.Renderer
{
    public class JogWheelRenderer : ViewRenderer<SliderWheelView, SliderWheel>
    {

        protected override void OnElementChanged(ElementChangedEventArgs<SliderWheelView> e)
        {
            base.OnElementChanged(e);

            if (e.OldElement != null || Element == null)
                return;

            if (Control == null)
            {
                CreateNewControl();
            }
        }

        private void CreateNewControl()
        {
            DisposeControl();

            var jogWheel = new SliderWheel(Element.Type)
            {
                Text = Element.Text,
                MinValue = Element.MinValue,
                MaxValue = Element.MaxValue,
                Value = Element.LightIntensity,
                OutputValue = Element.Output,
                OutputValueMin = Element.OutputMin,
                OutputValueMax = Element.OutputMax,
                OutputUnit = Element.OutputUnit,
                SecondOutputValue = Element.SecondOutput,
                IsChecked = Element.LightIntensity > 0,
                IsButtonVisible = Element.IsButtonVisible,
                IsEnabled = Element.IsSliderEnabled,
                StepValue = Element.OutputMin / Element.MinValue,
            };

            jogWheel.OnValueChanged += jogWheel_OnValueChanged;
            jogWheel.OnButtonClicked += jogWheel_OnButtonClicked;
            jogWheel.OnValueClicked += jogWheel_OnValueClicked;

            SetNativeControl(jogWheel);
        }

        private void jogWheel_OnValueChanged(object sender, EventArgs e)
        {
            if (Element == null)
                return;

            Element.LightIntensity = Control.Value;
        }

        private void jogWheel_OnValueClicked(object sender, EventArgs e)
        {
            Element?.SetValueCommand?.Execute(null);
        }

        private void jogWheel_OnButtonClicked(object sender, EventArgs e)
        {
            Element?.Command?.Execute(null);
        }

        /*protected override void OnSizeChanged(int width, int height, int oldWidth, int oldHeight)
        {
            Control?.ChangeSize(width, height, oldWidth, oldHeight);
            base.OnSizeChanged(width, height, oldWidth, oldHeight);
        }*/

        protected override void OnElementPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            base.OnElementPropertyChanged(sender, e);

            if (e.PropertyName == SliderWheelView.LightIntensityProperty.PropertyName)
            {
                if (Element.LightIntensity != Control.Value)
                {
                    Control.Value = Element.LightIntensity;
                }
            }
            else if (e.PropertyName == SliderWheelView.OutputProperty.PropertyName)
            {
                if (Element.Output != Control.OutputValue)
                {
                    Control.OutputValue = Element.Output;
                }
            }
            else if (e.PropertyName == SliderWheelView.SecondOutputProperty.PropertyName)
            {
                if (Element.SecondOutput != Control.SecondOutputValue)
                {
                    Control.SecondOutputValue = Element.SecondOutput;
                }
            }
            else if (e.PropertyName == SliderWheelView.OutputUnitProperty.PropertyName)
            {
                if (Element.OutputUnit != Control.OutputUnit)
                {
                    Control.OutputUnit = Element.OutputUnit;
                }
            }
            else if (e.PropertyName == SliderWheelView.TypeProperty.PropertyName)
            {
                if (Element.Type != Control.ControlType)
                {
                    Control.ControlType = Element.Type;
                }
            }
            else if (e.PropertyName == SliderWheelView.IsCheckedProperty.PropertyName)
            {
                if (Element.IsChecked != Control.IsChecked)
                {
                    Control.IsChecked = Element.IsChecked;
                }
            }

            else if (e.PropertyName == SliderWheelView.TextProperty.PropertyName)
            {
                if (Element.Text != Control.Text)
                {
                    Control.Text = Element.Text;
                }
            }
            else if (e.PropertyName == SliderWheelView.IsButtonVisibleProperty.PropertyName)
            {
                if (Element.IsButtonVisible != Control.IsButtonVisible)
                {
                    Control.IsButtonVisible = Element.IsButtonVisible;
                }
            }
            else if (e.PropertyName == SliderWheelView.IsEnabledProperty.PropertyName)
            {
                if (Element.IsSliderEnabled != Control.IsEnabled)
                {
                    Control.IsEnabled = Element.IsSliderEnabled;
                }
            }
        }

        protected override void Dispose(bool disposing)
        {
            DisposeControl();

            base.Dispose(disposing);
        }

        private void DisposeControl()
        {
            if (Control != null)
            {
                Control.OnButtonClicked -= jogWheel_OnButtonClicked;
                Control.OnValueChanged -= jogWheel_OnValueChanged;
                Control.OnValueClicked -= jogWheel_OnValueClicked;
            }
        }
    }
}
