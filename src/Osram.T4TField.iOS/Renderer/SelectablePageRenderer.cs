﻿using Osram.T4TField.iOS.Renderer;
using Osram.TFTBackend.DataModel.Feature.Contracts;
using System.Collections.ObjectModel;
using System.Linq;
using UIKit;
using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;

[assembly: ExportRenderer(typeof(Osram.T4TField.Views.SelectableView), typeof(AdvancedModePageRenderer))]
namespace Osram.T4TField.iOS.Renderer
{
    class AdvancedModePageRenderer : PageRenderer
    {
        //private UIPickerView SelectFeaturesSpinner;

        private FeatureType SelectedFeaturesType { get; set; }

        private ViewModels.FeatureInfoButton CurrentFeature { get; set; }

        private bool IsAdvancedMode { get; set; }

        public ObservableCollection<ViewModels.FeatureInfoButton> ListOfFeatures { get; private set; }

        protected override void OnElementChanged(VisualElementChangedEventArgs e)
        {
            base.OnElementChanged(e);
            AddCustomElemInToolBar();
        }

        protected override void Dispose(bool disposing)
        {
            base.Dispose(true);
            if (IsAdvancedMode)
            {
                //TODO: GetToolbar().RemoveAllViews();
            }
        }

        // FIXME:
        // private static Android.Support.V7.Widget.Toolbar GetToolbar() => (CrossCurrentActivity.Current?.Activity as MainActivity)?.FindViewById<Android.Support.V7.Widget.Toolbar>(Resource.Id.toolbar);
        private static UIView GetToolbar() => null;

        private void AddCustomElemInToolBar()
        {
            //TODO:

            /*if (MainActivity.ToolBar == null || Element == null)
            {
                return;
            }

            IsAdvancedMode = (bool)(Element as SelectableView)?.IsAdvancedMode;
            ListOfFeatures = (ObservableCollection<ViewModels.FeatureInfoButton>)(Element as SelectableView)?.ListOfFeatures;
            SelectedFeaturesType = (FeatureType)(Element as SelectableView)?.SelectedFeature;

            if (IsAdvancedMode)
            {

                Android.Widget.RelativeLayout toolbarLayout = new Android.Widget.RelativeLayout(Context);
                Android.Support.V7.Widget.SwitchCompat SwitchButton = new Android.Support.V7.Widget.SwitchCompat(Context);
                Android.Widget.RelativeLayout.LayoutParams SwitchButtonLayoutParam = new Android.Widget.RelativeLayout.LayoutParams(
                    ViewGroup.LayoutParams.WrapContent,
                    ViewGroup.LayoutParams.MatchParent);
                SwitchButtonLayoutParam.AddRule(Android.Widget.LayoutRules.AlignParentRight);
                SwitchButton.LayoutParameters = SwitchButtonLayoutParam;
                SwitchButton.Checked = getCurrentFeatures().IsSelected;

                SwitchButton.CheckedChange -= selectionChange;
                SwitchButton.CheckedChange += selectionChange;


                SelectFeaturesSpinner = new Android.Widget.Spinner(Context);
                SelectFeaturesSpinner.LayoutParameters = new ViewGroup.LayoutParams(
                   ViewGroup.LayoutParams.WrapContent,
                   ViewGroup.LayoutParams.MatchParent);

                //  var items = createListOfFeatures();
                var adapter = new FeatureAdapter((CrossCurrentActivity.Current?.Activity as MainActivity), ListOfFeatures);
                SelectFeaturesSpinner.Adapter = adapter;
                SelectFeaturesSpinner.SetSelection(getSelectedPositions());
                SelectFeaturesSpinner.ItemSelected -= itemClick;
                SelectFeaturesSpinner.ItemSelected += itemClick;

                toolbarLayout.AddView(SelectFeaturesSpinner);
                toolbarLayout.AddView(SwitchButton);
                toolbarLayout.LayoutParameters = new ViewGroup.LayoutParams(
                ViewGroup.LayoutParams.MatchParent,
                ViewGroup.LayoutParams.MatchParent);
                GetToolbar().Title = String.Empty;
                GetToolbar().AddView(toolbarLayout);

            }*/
        }



        //TODO:
        /*private void selectionChange(object sender, CheckedChangeEventArgs e)
        {
            updateSelectedFeature(e.IsChecked);
        }*/

        private void updateSelectedFeature(bool IsChecked)
        {
            getCurrentFeatures().IsSelected = IsChecked;
        }

        //TODO:
        /*private void itemClick(object sender, AdapterView.ItemSelectedEventArgs e)
        {
            ViewModels.FeatureInfoButton features = ((FeatureAdapter)SelectFeaturesSpinner.Adapter)[e.Position];
            if (features.FeatureInfo.FeatureType != SelectedFeaturesType)
            {
                (features.OnClickButton).Execute(null);
            }
        }*/

        private int getSelectedPositions()
        {
            ViewModels.FeatureInfoButton currentFeatures = getCurrentFeatures();
            var position = ListOfFeatures.IndexOf(currentFeatures);
            return position;
        }

        private ViewModels.FeatureInfoButton getCurrentFeatures()
        {
            if (CurrentFeature == null)
            {
                CurrentFeature = ListOfFeatures.Where(x => x.FeatureInfo.FeatureType == SelectedFeaturesType).First();
            }
            return CurrentFeature;
        }
    }
}