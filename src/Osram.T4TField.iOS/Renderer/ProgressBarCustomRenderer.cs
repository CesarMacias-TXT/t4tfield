using Osram.T4TField.Controls;
using Osram.T4TField.iOS.Renderer;
using System.ComponentModel;
using UIKit;
using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;

[assembly: ExportRenderer(typeof(IndicatorProgressBar), typeof(ProgressBarCustomRenderer))]


namespace Osram.T4TField.iOS.Renderer
{

    class ProgressBarCustomRenderer : ProgressBarRenderer
    {
        private Controls.BarIndicator bar;

        protected override void OnElementChanged(ElementChangedEventArgs<ProgressBar> e)
        {
            base.OnElementChanged(e);
            if(Control == null)
            {
                return;
            }

            Control.LayoutMargins = new UIEdgeInsets(0, 0, 0, 0);
            // TODO: Control.ScaleY = 2; // This changes the height
            // FIXME: There's no such feature on iOS UIProgressView out of the box: Control.Indeterminate = true;
            var margin = Control.Bounds.Height;
            Control.ProgressTintColor = Styles.OsramColor.ToUIColor();
            // TODO: this.SetWillNotDraw(false);
        }

        // FIXME: no such method on iOS
        /*protected override void OnDraw()
        {
            var margin = Control.PivotY + (Control.PivotY / 2);
            canvas.Translate(0, -margin);
            base.OnDraw(canvas);
        }*/

        protected override void OnElementPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            base.OnElementPropertyChanged(sender, e);

            if (e.PropertyName == "Indeterminate")
            {
                // FIXME: There's no such feature on iOS UIProgressView out of the box: Control.Indeterminate
                /*
                if (Control.Indeterminate != ((IndicatorProgressBar)sender).Indeterminate)
                {
                    Control.Indeterminate = ((IndicatorProgressBar)sender).Indeterminate;
                }*/
            }
        }



    }
}