﻿using MvvmCross.Platforms.Ios.Presenters;
using Osram.T4TField.ViewPresenter;
using UIKit;

namespace Osram.T4TField.iOS.ViewPresenter
{
    public class OsramIOSMvxFromsViewPresenter : MasterDetailViewPresenter, IMvxIosViewPresenter
    {
        private UIWindow window;
        public OsramIOSMvxFromsViewPresenter(UIWindow window)
        {
            this.window = window;
        }

        public void ClosedPopoverViewController()
        {
            throw new System.NotImplementedException();
        }

        public void NativeModalViewControllerDisappearedOnItsOwn()
        {

        }

        public bool PresentModalViewController(UIViewController controller, bool animated)
        {
            window.RootViewController.PresentViewController(controller, animated, null);
            return true;
        }
    }
}
