﻿using CoreGraphics;
using Osram.T4TField.Controls.SliderWheel;
using Osram.T4TField.Utils;
using Splat;
using System;
using System.Reflection;
using UIKit;

namespace Osram.T4TField.iOS.Controls
{
    public class SliderWheelOld : UIView
    {
        private nfloat _ballWidth;

        private SliderWheelType _controlType;
        private nfloat _controlWidth;
        private int _currentAngle;
        private CGPoint _currentPosition;
        private bool _dragging;

        private bool _isButtonVisible;

        private bool _isChecked;

        private nfloat _onoffButtonWidth;
        private Area _previousArea = Area.NoKnownArea;
        private SliderWheelSkin _skin;
        private int _value = -1;

        public SliderWheelOld(SliderWheelType controlType = SliderWheelType.OnOffSlider) : base()
        {
            Initialize();
            ControlType = controlType;

            Frame = new CGRect(0, 0, _skin.SliderBkImage.ToNative().Size.Width, _skin.SliderBkImage.ToNative().Size.Height);
        }

        private UIImageView SliderImageView { get; set; }
        public UIView CenterButtonLayout { get; set; }
        private UIView StatusBallLayout { get; set; }
        private UIImageView OnOffButtonImageView { get; set; }
        private UILabel BallStatusText { get; set; }
        private UILabel CenterButtonText { get; set; }
        private UILabel CenterButtonUnitText { get; set; }
        private UILabel CenterStatusText { get; set; }

        private UILabel BottomStatusText { get; set; }

        private UILabel StatusText { get; set; }
        public UIImageView StatusBallImage { get; set; }

        public bool NeedSave { get; set; }

        public int Value {
            get { return _value; }
            set {
                InternalSetValue(value);
            }
        }

        public int MinValue { get; set; }
        public int MaxValue { get; set; }

        private int outputValue;
        public int OutputValue {

            get { return outputValue; }
            set {
                outputValue = value;
                // Change Position of ball if not correct
                if ((Value - 1) * StepValue > value || (Value + 1) * StepValue < value)
                {
                    var s = (int)Math.Round((double)value / StepValue);
                    InternalSetValue(s);
                }
                UpdateCenterText();
            }
        }

        public int OutputValueMin { get; set; }
        public int OutputValueMax { get; set; }

        public int StepValue { get; set; }

        private string secondOutputValue;
        public string SecondOutputValue {

            get { return secondOutputValue; }
            set {
                secondOutputValue = value;
                UpdateCenterText();
            }
        }

        private string outputUnit;
        private bool _isEnabled;

        public string OutputUnit {

            get { return outputUnit; }
            set {
                outputUnit = value;
                UpdateCenterText();
            }
        }

        public bool IsChecked {
            get { return _isChecked; }
            set {
                _isChecked = value;

                OnOffButtonImageView.Image = value
                    ? _skin.MiddleButtonCheckedImage.ToNative()
                    : _skin.MiddleButtonUncheckedImage.ToNative();
            }
        }

        public bool IsEnabled {
            get { return _isEnabled; }
            set {
                _isEnabled = value;
                UpdateTextColorByEnabledStaus();
            }
        }


        public string Text {
            get { return CenterButtonText.Text; }
            set {
                CenterButtonText.Text = value;
                //CenterButtonText.TextSize = 40;//Update on text size
            }
        }

        public bool IsButtonVisible {
            get { return _isButtonVisible; }
            set {
                _isButtonVisible = value;
                UpdateControlType();
            }
        }

        public SliderWheelType ControlType {
            get { return _controlType; }
            set {
                _controlType = value;

                UpdateControlType();

                UpdateSkin();
            }
        }

        public event EventHandler OnValueChanged;
        public event EventHandler OnButtonClicked;

        public event EventHandler OnValueClicked;

        public void InternalSetValue(int value)
        {
            if (_value != value)
            {
                _value = Math.Max(Math.Min(value, MaxValue), MinValue);

                if (ControlType == SliderWheelType.OnOffSlider || ControlType == SliderWheelType.Slider ||
                    ControlType == SliderWheelType.SaveSlider)
                {
                    UpdateSlider();
                    UpdateBallText();
                }
            }
        }

        private void UpdateControlType()
        {
            switch (ControlType)
            {
                case SliderWheelType.SaveSlider:
                    CenterButtonLayout.Hidden = (IsButtonVisible && !_dragging)
                        ? false
                        : true;
                    OnOffButtonImageView.Hidden = false;
                    CenterButtonText.Hidden = false;

                    StatusBallLayout.Hidden = false;
                    BallStatusText.Hidden = false;

                    SliderImageView.Hidden = false;

                    CenterStatusText.Hidden = CenterButtonLayout.Hidden == false
                        ? true
                        : false;

                    break;
                case SliderWheelType.OnOffSlider:
                    CenterButtonLayout.Hidden = (IsButtonVisible && !_dragging)
                        ? false
                        : true;
                    OnOffButtonImageView.Hidden = false;
                    CenterButtonText.Hidden = false;

                    SliderImageView.Hidden = false;

                    StatusBallLayout.Hidden = false;
                    BallStatusText.Hidden = false;

                    CenterStatusText.Hidden = CenterButtonLayout.Hidden == false
                        ? true
                        : false;
                    break;
                case SliderWheelType.Slider:
                    CenterButtonLayout.Hidden = true;

                    SliderImageView.Hidden = false;

                    StatusBallLayout.Hidden = false;
                    BallStatusText.Hidden = false;

                    CenterStatusText.Hidden = false;
                    break;
                case SliderWheelType.OnOff:
                    CenterButtonLayout.Hidden = false;
                    OnOffButtonImageView.Hidden = false;
                    CenterButtonText.Hidden = false;

                    SliderImageView.Hidden = true;
                    StatusBallLayout.Hidden = false;
                    BallStatusText.Hidden = true;

                    CenterStatusText.Hidden = CenterButtonLayout.Hidden == false
                        ? true
                        : false;
                    break;
            }
        }

        private float PtToPx(float pt) => pt / 72 * 96;

        private void Initialize()
        {
            AutoresizingMask = UIViewAutoresizing.All;

            //Background
            SliderImageView = new UIImageView(Frame)
            {
                AutoresizingMask = UIViewAutoresizing.All,
                ContentMode = UIViewContentMode.ScaleAspectFit
            };
            Add(SliderImageView);

            UIStackView centerText = new UIStackView(new CGRect(0, 0, 300, 230))
            {
                Axis = UILayoutConstraintAxis.Vertical,
                Alignment = UIStackViewAlignment.Center,
                AutoresizingMask = UIViewAutoresizing.All,
                Center = this.Center
            };

            CenterStatusText = new UILabel(new CGRect(0, 0, 80, 80));
            CenterStatusText.Font = UIFont.SystemFontOfSize(PtToPx(26));
            CenterStatusText.TextColor = AppDelegate.OsramColor;
            CenterStatusText.TextAlignment = UITextAlignment.Center;
            UITapGestureRecognizer centerStatusTextGestureRecognizer = new UITapGestureRecognizer();
            centerStatusTextGestureRecognizer.AddTarget(OnCenterStatusTextClicked);
            CenterStatusText.AddGestureRecognizer(centerStatusTextGestureRecognizer);

            BottomStatusText = new UILabel(new CGRect(0, 0, 50, 50));
            BottomStatusText.Font = UIFont.SystemFontOfSize(PtToPx(9));
            BottomStatusText.TextAlignment = UITextAlignment.Center;

            CenterButtonUnitText = new UILabel(new CGRect(0, 0, 50, 50));
            CenterButtonUnitText.Font = UIFont.SystemFontOfSize(PtToPx(18));
            CenterButtonUnitText.TextColor = AppDelegate.OsramColor;
            CenterButtonUnitText.TextAlignment = UITextAlignment.Center;

            centerText.AddArrangedSubview(CenterStatusText);
            centerText.AddArrangedSubview(CenterButtonUnitText);
            centerText.AddArrangedSubview(BottomStatusText);
            Add(centerText);

            CenterButtonLayout = new UIView(new CGRect(0, 0, 100, 100));
            OnOffButtonImageView = new UIImageView(CenterButtonLayout.Frame)
            {
                AutoresizingMask = UIViewAutoresizing.All,
                ContentMode = UIViewContentMode.ScaleAspectFit
            };
            OnOffButtonImageView.UserInteractionEnabled = true;
            OnOffButtonImageView.ContentMode = UIViewContentMode.ScaleAspectFit;
            UIPanGestureRecognizer OnOffButtonGestureRecognizer = new UIPanGestureRecognizer();
            OnOffButtonGestureRecognizer.AddTarget(() => OnOffButtonImageViewOnTouch(OnOffButtonGestureRecognizer));
            OnOffButtonImageView.AddGestureRecognizer(OnOffButtonGestureRecognizer);
            CenterButtonLayout.Add(OnOffButtonImageView);

            CenterButtonText = new UILabel(new CGRect(0, 0, 50, 50));
            CenterButtonText.Font = UIFont.SystemFontOfSize(25);
            CenterButtonText.TextColor = AppDelegate.OsramColor;
            CenterButtonLayout.Add(CenterButtonText);
            Add(CenterButtonLayout);
            CenterButtonLayout.Hidden = true;

            //StatusBall
            StatusBallLayout = new UIView(new CGRect(0, 0, 65, 65));
            StatusBallImage = new UIImageView(StatusBallLayout.Frame)
            {
                AutoresizingMask = UIViewAutoresizing.All,
                ContentMode = UIViewContentMode.ScaleAspectFit,
            };
            StatusBallLayout.Add(StatusBallImage);

            BallStatusText = new UILabel(StatusBallLayout.Frame)
            {
                Font = UIFont.SystemFontOfSize(PtToPx(11)),
                TextColor = AppDelegate.OsramColor,
                TextAlignment = UITextAlignment.Center
            };
            StatusBallLayout.Add(BallStatusText);

            Add(StatusBallLayout);

            UIPanGestureRecognizer StatusBallPanGesture = new UIPanGestureRecognizer();
            StatusBallPanGesture.AddTarget(() => OnTouchEvent(StatusBallPanGesture));
            StatusBallLayout.AddGestureRecognizer(StatusBallPanGesture);

            InternalSetValue(MinValue);
        }

        private void OnCenterStatusTextClicked()
        {
            OnValueClicked?.Invoke(this, EventArgs.Empty);
        }

        private void UpdateTextColorByEnabledStaus()
        {
            UIColor color = IsEnabled ?
                AppDelegate.OsramColor :
                UIColor.Gray;

            CenterButtonUnitText.TextColor = color;
            CenterButtonText.TextColor = color;
            BallStatusText.TextColor = color;
            CenterStatusText.TextColor = color;
        }

        private void UpdateSkin()
        {
            _skin = SliderWheelUtils.GetSliderWheelSkin(ControlType);

            SliderImageView.Image = _skin.SliderBkImage.ToNative();

            OnOffButtonImageView.Image = _skin.MiddleButtonUncheckedImage.ToNative();

            StatusBallImage.Image = _skin.StatusBallImage.ToNative();
        }

        private void OnOffButtonImageViewOnTouch(UIPanGestureRecognizer gestureRecognizer)
        {
            nfloat delta = gestureRecognizer.TranslationInView(OnOffButtonImageView).Y;
            if (delta > 0)
            {
                OnMiddleButtonDown();
            }
            else if (delta < 0)
            {
                OnMiddleButtonUp();
            }
        }

        private void OnMiddleButtonUp()
        {
            OnOffButtonImageView.Image = IsChecked
                ? _skin.MiddleButtonCheckedImage.ToNative()
                : _skin.MiddleButtonUncheckedImage.ToNative();

            OnButtonClicked?.Invoke(this, new EventArgs());
        }

        private void OnMiddleButtonDown()
        {
            OnOffButtonImageView.Image = _skin.MiddleButtonPressedImage.ToNative();
        }

        private static UIView FindChildOfType<T>(UIView parent)
        {
            if (parent.Subviews.Length == 0)
                return null;

            for (var i = 0; i < parent.Subviews.Length; i++)
            {
                var child = parent.Subviews[i];

                if (child == null)
                    continue;

                var type = child.GetType();

                if (child is T)
                {
                    return child;
                }

                var result = FindChildOfType<T>(child);
                if (result != null)
                    return result;
            }

            return null;
        }

        public bool OnTouchEvent(UIPanGestureRecognizer motionEvent)
        {
            if (IsEnabled)
            {
                if (motionEvent.NumberOfTouches == 0)
                {
                    return true;
                }

                var l = motionEvent.LocationOfTouch(0, this);
                var x = l.X;
                var y = l.Y;

                _currentPosition = new CGPoint(x, y);

                _currentAngle =
                    (int)SliderWheelUtils.GetTouchDegrees((float)x, (float)y, (float)Center.X, (float)Center.Y, SliderWheelConsts.Clockwise);

                //Logger.Trace("x[" + x + "] y[" + y + "] cx[" + Center.X + "] cy[" + Center.Y + "] angle[" + _currentAngle + "]");

                Area currentArea;
                // Mvx.Resolve<TabGestureService>().SetGestureEnabled(false);
                // { Xamarin.Forms.Platform.Android.ViewRenderer<Xamarin.Forms.CarouselView, Android.Support.V7.Widget.RecyclerView>}

                //var viewGroup = (UIView) this.Window.   ((ViewGroup)main.FindViewById(Android.Resource.Id.Content)).GetChildAt(0);

                //Xamarin.Forms.Platform.Android.ViewRenderer<Xamarin.Forms.CarouselView, Android.Support.V7.Widget.RecyclerView> _currentViewPager = FindChildOfType<Xamarin.Forms.Platform.Android.ViewRenderer<Xamarin.Forms.CarouselView, Android.Support.V7.Widget.RecyclerView>>(viewGroup);
                UIView _currentViewRenderer = FindChildOfType<UIView>(Window);
                switch (motionEvent.State)
                {
                    case UIGestureRecognizerState.Began:
                        //var type = _currentViewPager.GetType();
                        //type.InvokeMember("EnableGesture",
                        // BindingFlags.Instance | BindingFlags.Public | BindingFlags.SetProperty,
                        //Type.DefaultBinder, _currentViewPager, new object[] { false });
                        if (_currentViewRenderer is CarouselView.FormsPlugin.iOS.CarouselViewRenderer)
                        {
                            //((CarouselView.FormsPlugin.iOS.CarouselViewRenderer)_currentViewRenderer).SetIsSwipeEnabled(false);
                            var methodInfo = typeof(CarouselView.FormsPlugin.iOS.CarouselViewRenderer).GetMethod("SetIsSwipeEnabled", BindingFlags.Instance | BindingFlags.NonPublic);
                            if (methodInfo != null)
                            {
                                methodInfo.Invoke(_currentViewRenderer, new object[] { false });
                            }
                        }

                        _dragging = true;
                        currentArea = GetCurrentArea();

                        if (ControlType == SliderWheelType.OnOffSlider || ControlType == SliderWheelType.SaveSlider)
                        {
                            CenterButtonLayout.Hidden = true;
                            CenterStatusText.Hidden = (CenterButtonLayout.Hidden == true)
                                ? false
                                : true;
                        }

                        if (currentArea == Area.SliderArea)
                        {
                            UpdateValueFromSlider(_currentAngle);
                        }
                        _previousArea = currentArea;
                        break;

                    case UIGestureRecognizerState.Changed:
                        _currentViewRenderer.UserInteractionEnabled = false;
                        currentArea = GetCurrentArea();

                        if (_previousArea == Area.NoKnownArea && currentArea == Area.SliderArea)
                        {
                            if (_previousArea == Area.SliderArea)
                            {
                                UpdateValueFromSlider(_currentAngle);
                            }
                            _previousArea = currentArea;
                        }
                        else if (_previousArea == Area.SliderArea)
                        {
                            UpdateValueFromSlider(_currentAngle, true);
                        }
                        break;

                    case UIGestureRecognizerState.Ended:

                        _previousArea = Area.NoKnownArea;
                        _currentAngle = -1;
                        _dragging = false;

                        if (ControlType == SliderWheelType.OnOffSlider || ControlType == SliderWheelType.SaveSlider)
                        {
                            CenterButtonLayout.Hidden = false;
                            CenterStatusText.Hidden = (CenterButtonLayout.Hidden == true)
                                ? false
                                : true;
                        }


                        //_currentViewPager.GetType().InvokeMember("EnableGesture",
                        //BindingFlags.Instance | BindingFlags.Public | BindingFlags.SetProperty,
                        //Type.DefaultBinder, _currentViewPager, new object[] { true });
                        if (_currentViewRenderer is CarouselView.FormsPlugin.iOS.CarouselViewRenderer)
                        {
                            //((CarouselView.FormsPlugin.iOS.CarouselViewRenderer)_currentViewRenderer).SetIsSwipeEnabled(true);
                            var methodInfo = typeof(CarouselView.FormsPlugin.iOS.CarouselViewRenderer).GetMethod("SetIsSwipeEnabled", BindingFlags.Instance | BindingFlags.NonPublic);
                            if (methodInfo != null)
                            {
                                methodInfo.Invoke(_currentViewRenderer, new object[] { true });
                            }
                        }
                        break;
                }
            }

            return true;
        }

        //TODO:
        /*private void handleDrag(UIPanGestureRecognizer gesture)
        {
            if (gesture.State == UIGestureRecognizerState.Began)
            {
                initPosition = StatusBallLayout.Center;
            }
            else if (gesture.State == UIGestureRecognizerState.Ended)
            {

            }
            else if (gesture.State == UIGestureRecognizerState.Changed)
            {
                CGPoint newPososition = CGPoint.Add(initPosition, new CGSize(gesture.TranslationInView(StatusBallLayout))),
                        relativePosition = CGPoint.Subtract(newPososition,
                                            new CGSize(SliderImageView.Frame.Width / 2, SliderImageView.Frame.Height / 2));

                double l = Math.Sqrt(Math.Pow(relativePosition.X, 2) + Math.Pow(relativePosition.Y, 2)),
                       deg = Math.Acos(relativePosition.Y / l) * 180 / Math.PI;

                if (relativePosition.X > 0) deg = 360 - deg;
                deg = Math.Max(MIN_ANGLE, Math.Min(MAX_ANGLE, deg));

                int v = (int)Math.Round(MinValue + (deg - MIN_ANGLE) / (MAX_ANGLE - MIN_ANGLE) * (MaxValue - MinValue));
                InternalSetValue(v);
                OnValueChanged?.Invoke(this, EventArgs.Empty);
            }
        }*/

        public void ChangeSize(nfloat w, nfloat h, nfloat oldw, nfloat oldh)
        {
            var scale = w / (_skin.SliderBkImage.ToNative().Size.Width * 1.0);

            _controlWidth = w;
            _ballWidth = (nfloat)(_skin.StatusBallImage.ToNative().Size.Width * scale);
            _onoffButtonWidth = (nfloat)(_skin.MiddleButtonCheckedImage.ToNative().Size.Width * scale);

            CenterButtonLayout.Transform = CGAffineTransform.Scale(CenterButtonLayout.Transform, (nfloat)scale, (nfloat)scale);

            StatusBallLayout.Frame = new CGRect(StatusBallLayout.Frame.X, StatusBallLayout.Frame.Y, _ballWidth, _ballWidth);

            CenterStatusText.Font = UIFont.SystemFontOfSize(w < 700 ? 17 : 26);
            UpdateSlider();
        }

        private Area GetCurrentArea()
        {
            var distanceFromCenter = SliderWheelUtils.GetDistanceBetweenPoints((float)_currentPosition.X, (float)_currentPosition.Y,
                (float)Center.X, (float)Center.Y);

            if (ControlType == SliderWheelType.OnOff || ControlType == SliderWheelType.OnOffSlider ||
                ControlType == SliderWheelType.SaveSlider)
            {
                if (_onoffButtonWidth / 2 >= distanceFromCenter)
                {
                    return Area.ButtonArea;
                }
            }
            if (ControlType == SliderWheelType.OnOffSlider || ControlType == SliderWheelType.Slider ||
                ControlType == SliderWheelType.SaveSlider)
            {
                if (_controlWidth / 2 >= distanceFromCenter && _onoffButtonWidth / 2 <= distanceFromCenter)
                {
                    if (SliderWheelConsts.Clockwise)
                    {
                        if (((_currentAngle - SliderWheelConsts.StartAngle + 360) % 360) <= SliderWheelConsts.SliderAngle)
                        {
                            return Area.SliderArea;
                        }
                        return Area.NoKnownArea;
                    }
                }
            }

            return Area.NoKnownArea;
        }

        private void UpdateValueFromSlider(int angle, bool limit = false)
        {
            var newValue = SliderWheelUtils.SliderAngleToValue(angle, SliderWheelConsts.StartAngle,
                SliderWheelConsts.SliderAngle, SliderWheelConsts.MinValue, SliderWheelConsts.MaxValue,
                SliderWheelConsts.Clockwise);

            if (limit && Math.Abs(Value - newValue) > SliderWheelConsts.MaxValueChangeAllowed)
            {
                if (newValue - Value < 0)
                {
                    newValue = 100;
                }
                else
                {
                    return;
                }
            }

            InternalSetValue(newValue);

            OnValueChanged?.Invoke(this, EventArgs.Empty);
        }

        private void UpdateSlider()
        {
            /*
            double angle = MIN_ANGLE + (MAX_ANGLE - MIN_ANGLE) / MaxValue * Value;

            var size = Math.Min(SliderImageView.Frame.Width, SliderImageView.Frame.Height);
            var rads = angle * Math.PI / 180;

            var radius = size / 2 * 0.75;
            CGPoint center = new CGPoint(
                SliderImageView.Frame.Width / 2 - Math.Sin(rads) * radius,
                SliderImageView.Frame.Height / 2 + Math.Cos(rads) * radius);

            StatusBallLayout.Center = center;
            */

            var angle = SliderWheelUtils.ValueToSliderAngle(Value, SliderWheelConsts.StartAngle,
                SliderWheelConsts.SliderAngle, SliderWheelConsts.MinValue, SliderWheelConsts.MaxValue,
                SliderWheelConsts.Clockwise);

            var radix = Frame.Width / 2;

            var rads = angle * Math.PI / 180 + Math.PI / 2;
            var halfSliderTrack = (Frame.Width - _onoffButtonWidth) / 4;
            var x = Math.Cos(rads) * (Frame.Width / 2 - halfSliderTrack);
            var y = Math.Sin(rads) * (Frame.Width / 2 - halfSliderTrack);

            StatusBallLayout.Frame = new CGRect(radix - x - _ballWidth / 2, radix - y - _ballWidth / 2, StatusBallLayout.Frame.Width, StatusBallLayout.Frame.Height);
        }

        private void UpdateBallText()
        {
            if (BallStatusText != null)
                BallStatusText.Text = Value + "%";
        }

        private void UpdateCenterText()
        {
            if (CenterStatusText != null)
            {
                CenterStatusText.Text = OutputValue.ToString();
                CenterButtonUnitText.Text = OutputUnit;
                BottomStatusText.Text = SecondOutputValue;
            }
        }

        #region override 

        public override void LayoutSubviews()
        {
            base.LayoutSubviews();
            UpdateSlider();
        }

        public override CGRect Bounds {
            get {
                return base.Bounds;
            }
            set {

                if (value != base.Bounds)
                {
                    ChangeSize(value.Width, value.Height, base.Bounds.Width, base.Bounds.Height);
                }
                base.Bounds = value;
            }
        }

        public override CGRect Frame {
            get {
                return base.Frame;
            }
            set {
                if (value != base.Frame)
                {
                    ChangeSize(value.Width, value.Height, base.Frame.Width, base.Frame.Height);
                }
                base.Frame = value;
            }
        }

        #endregion override
    }
}