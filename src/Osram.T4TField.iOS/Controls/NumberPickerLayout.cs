﻿
using CoreGraphics;
using System;
using System.Collections.Generic;
using System.Linq;
using UIKit;
using static Osram.T4TField.Controls.XFormNumberPicker;

namespace Osram.T4TField.iOS.Controls
{
    public class NumberPickerLayout : UIStackView
    {

        private static readonly CGRect RECT = new CGRect(0, 0, 60, 100);
        private UIPickerView np;

        public Action<int> valueChange;

        public NumberPickerLayout(PickerType type, int currentValue, int? min, int? max) : base(RECT)
        {
            np = new UIPickerView(RECT);

            string[] lm = null;
            if (type.Equals(PickerType.Hour))
            {
                //np.MinValue = min != null ? (int)min : 0;
                //np.MaxValue = max != null ? (int)max : 23;
                lm = new string[] { "00", "01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23" };
                if (min != null)
                {
                    lm = lm.Where(n => int.Parse(n) >= min).ToArray();
                }
                if (max != null)
                {
                    lm = lm.Where(n => int.Parse(n) <= max).ToArray();
                }
            }
            else if (type.Equals(PickerType.Minutes))
            {
                //np.MinValue = min != null ? (int)min : 0;
                //np.MaxValue = max != null ? (int)max : 59;
                lm = new string[] { "00", "01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "24", "25", "26", "27", "28", "29", "30", "31", "32", "33", "34", "35", "36", "37", "38", "39", "40", "41", "42", "43", "44", "45", "46", "47", "48", "49", "50", "51", "52", "53", "54", "55", "56", "57", "58", "59" };
                if (min != null)
                {
                    lm = lm.Where(n => int.Parse(n) >= min).ToArray();
                }
                if (max != null)
                {
                    lm = lm.Where(n => int.Parse(n) <= max).ToArray();
                }
            }
            else if (type.Equals(PickerType.Number))
            {
                if (min != null && max != null)
                {
                    List<String> temp = new List<String>();
                    for (var i = min; i <= max; i++)
                    {
                        temp.Add(i.ToString());
                    }

                    lm = temp.ToArray();
                }


            }

            UIArrayPickerModel model = new UIArrayPickerModel(lm);
            np.Model = model;
            model.PickerChanged += (sender, e) =>
            {
                valueChange(e.SelectedValue);
            };

            np.Select(currentValue, 0, false);

            Add(np);
        }

    }

}
