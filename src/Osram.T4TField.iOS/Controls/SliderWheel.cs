﻿using CoreGraphics;
using Osram.T4TField.Controls.SliderWheel;
using Osram.T4TField.Utils;
using Splat;
using System;
using UIKit;
using Xamarin.Forms;

namespace Osram.T4TField.iOS.Controls
{
    public class SliderWheel : UIView
    {
        /*private int _ballWidth
        private int _controlWidth;
        private int _currentAngle;
        private PointF _currentPosition;
        private bool _dragging;*/

        private SliderWheelType _controlType;
        private bool _isButtonVisible;

        private bool _isChecked;

        //private int _onoffButtonWidth;
        //private Area _previousArea = Area.NoKnownArea;
        private SliderWheelSkin _skin;
        private int _value = -1;

        private const double MIN_ANGLE = 26, MAX_ANGLE = 314;

        public SliderWheel(SliderWheelType controlType = SliderWheelType.OnOffSlider) : base(new CGRect(0, 0, 1000, 1000))
        {

            MaxValue = 100;
            Initialize();
            ControlType = controlType;
        }

        private UIImageView SliderImageView { get; set; }
        public RelativeLayout CenterButtonLayout { get; set; }
        private UIView StatusBallLayout { get; set; }
        private UIImageView OnOffButtonImageView { get; set; }
        private UILabel BallStatusText { get; set; }
        private UILabel CenterButtonText { get; set; }
        private UILabel CenterButtonUnitText { get; set; }
        private UILabel CenterStatusText { get; set; }

        private UILabel BottomStatusText { get; set; }

        private UILabel StatusText { get; set; }
        public UIImageView StatusBallImage { get; set; }

        public bool NeedSave { get; set; }

        public event EventHandler OnValueChanged;
        public event EventHandler OnButtonClicked;

        public event EventHandler OnValueClicked;

        public int Value {
            get { return _value; }
            set {
                InternalSetValue(value);
            }
        }

        private double minAngle;

        private int minValue;
        public int MinValue {
            get {
                return minValue;
            }
            set {
                minValue = value;
                minAngle = (MAX_ANGLE - MIN_ANGLE) / (100 - 10) * (MinValue - 10) + MIN_ANGLE;
            }
        }

        private double maxAngle;

        private int maxValue;
        public int MaxValue {
            get {
                return maxValue;
            }
            set {
                maxValue = value;
                maxAngle = (MAX_ANGLE - MIN_ANGLE) / (100 - 10) * (MaxValue - 10) + MIN_ANGLE;
            }
        }

        private int outputValue;
        public int OutputValue {

            get { return outputValue; }
            set {
                outputValue = value;
                // Change Position of ball if not correct
                if ((Value - 1) * StepValue >= value || (Value + 1) * StepValue <= value)
                {
                    var s = (int)Math.Round((double)value / StepValue);
                    InternalSetValue(s);
                }
                UpdateCenterText();
            }
        }

        public int OutputValueMin { get; set; }
        public int OutputValueMax { get; set; }

        public int StepValue { get; set; }

        private string secondOutputValue;
        public string SecondOutputValue {

            get { return secondOutputValue; }
            set {
                secondOutputValue = value;
                UpdateCenterText();
            }
        }

        private string outputUnit;
        private bool _isEnabled;
        private CGPoint initPosition;

        public string OutputUnit {

            get { return outputUnit; }
            set {
                outputUnit = value;
                UpdateCenterText();
            }
        }

        public bool IsChecked {
            get { return _isChecked; }
            set {
                _isChecked = value;

                OnOffButtonImageView.Image = value
                    ? _skin.MiddleButtonCheckedImage.ToNative()
                    : _skin.MiddleButtonUncheckedImage.ToNative();
            }
        }

        public bool IsEnabled {
            get { return _isEnabled; }
            set {
                _isEnabled = value;
                UpdateTextColorByEnabledStaus();
            }
        }


        public string Text {
            get { return CenterButtonText.Text; }
            set {
                CenterButtonText.Text = value;
                //CenterButtonText.TextSize = 40;//Update on text size
            }
        }

        public bool IsButtonVisible {
            get { return _isButtonVisible; }
            set {
                _isButtonVisible = value;
                UpdateControlType();
            }
        }

        public SliderWheelType ControlType {
            get { return _controlType; }
            set {
                _controlType = value;

                UpdateControlType();

                UpdateSkin();
            }
        }

        public void InternalSetValue(int value)
        {
            if (_value != value)
            {
                _value = Math.Max(Math.Min(value, MaxValue), MinValue);

                if (ControlType == SliderWheelType.OnOffSlider || ControlType == SliderWheelType.Slider ||
                    ControlType == SliderWheelType.SaveSlider)
                {
                    UpdateSlider();
                    UpdateBallText();
                }
            }
        }

        private void UpdateControlType()
        {
            switch (ControlType)
            {
                case SliderWheelType.SaveSlider:
                    //TODO
                    /*CenterButtonLayout.Visibility = (IsButtonVisible && !_dragging)
                        ? ViewStates.Visible
                        : ViewStates.Invisible;*/
                    OnOffButtonImageView.Hidden = false;
                    CenterButtonText.Hidden = false;

                    //StatusBallLayout.Visibility = ViewStates.Visible;
                    BallStatusText.Hidden = false;

                    SliderImageView.Hidden = false;

                    //TODO
                    /*CenterStatusText.Visibility = CenterButtonLayout.Visibility == ViewStates.Visible
                        ? ViewStates.Invisible
                        : ViewStates.Visible;*/

                    break;
                case SliderWheelType.OnOffSlider:
                    //TODO
                    /*CenterButtonLayout.Visibility = (IsButtonVisible && !_dragging)
                        ? ViewStates.Visible
                        : ViewStates.Invisible;*/
                    OnOffButtonImageView.Hidden = false;
                    CenterButtonText.Hidden = false;

                    SliderImageView.Hidden = false;

                    //TODO: StatusBallLayout.Visibility = ViewStates.Visible;
                    BallStatusText.Hidden = false;

                    //TODO:
                    /*CenterStatusText.Visibility = CenterButtonLayout.Visibility == ViewStates.Visible
                        ? ViewStates.Invisible
                        : ViewStates.Visible;*/
                    break;
                case SliderWheelType.Slider:
                    //TODO: CenterButtonLayout.Visibility = ViewStates.Invisible;

                    SliderImageView.Hidden = false;

                    //TODO: StatusBallLayout.Visibility = ViewStates.Visible;
                    BallStatusText.Hidden = false;

                    CenterStatusText.Hidden = false;
                    break;
                case SliderWheelType.OnOff:
                    //TODO: CenterButtonLayout.Visibility = ViewStates.Visible;
                    OnOffButtonImageView.Hidden = false;
                    CenterButtonText.Hidden = false;

                    SliderImageView.Hidden = true;
                    //TODO: StatusBallLayout.Visibility = ViewStates.Invisible;
                    BallStatusText.Hidden = true;

                    //TODO:
                    /*CenterStatusText.Visibility = CenterButtonLayout.Visibility == ViewStates.Visible
                        ? ViewStates.Invisible
                        : ViewStates.Visible;*/
                    break;
            }
        }

        private float pt2Px(float pt) => pt / 72 * 96;

        private void Initialize()
        {
            AutoresizingMask = UIViewAutoresizing.All;

            //Background
            SliderImageView = new UIImageView(Frame)
            {
                AutoresizingMask = UIViewAutoresizing.All,
                ContentMode = UIViewContentMode.ScaleAspectFit
            };
            Add(SliderImageView);

            #region Center Indication
            UIStackView centerText = new UIStackView(new CGRect(0, 0, 400, 280))
            {
                Axis = UILayoutConstraintAxis.Vertical,
                Alignment = UIStackViewAlignment.Center,
                AutoresizingMask = UIViewAutoresizing.All,
                Center = this.Center
            };

            CenterStatusText = new UILabel(new CGRect(0, 0, 100, 0));
            CenterStatusText.Font = UIFont.SystemFontOfSize(pt2Px(32));
            CenterStatusText.TextColor = AppDelegate.OsramColor;
            CenterStatusText.TextAlignment = UITextAlignment.Center;

            CenterButtonUnitText = new UILabel(new CGRect(0, 0, 50, 50));
            CenterButtonUnitText.Font = UIFont.SystemFontOfSize(pt2Px(16));
            CenterButtonUnitText.TextColor = AppDelegate.OsramColor;
            CenterButtonUnitText.TextAlignment = UITextAlignment.Center;

            BottomStatusText = new UILabel(new CGRect(0, 0, 50, 50));
            BottomStatusText.Font = UIFont.SystemFontOfSize(pt2Px(11));
            BottomStatusText.TextAlignment = UITextAlignment.Center;

            centerText.AddArrangedSubview(CenterStatusText);
            centerText.AddArrangedSubview(CenterButtonUnitText);
            centerText.AddArrangedSubview(BottomStatusText);

            UITapGestureRecognizer centerStatusTextGestureRecognizer = new UITapGestureRecognizer();
            centerStatusTextGestureRecognizer.AddTarget(OnCenterStatusTextClicked);
            centerText.AddGestureRecognizer(centerStatusTextGestureRecognizer);

            Add(centerText);
            #endregion

            #region sliding ball
            //StatusBall
            StatusBallLayout = new UIView(new CGRect(0, 0, 65, 65));
            StatusBallImage = new UIImageView(StatusBallLayout.Frame)
            {
                AutoresizingMask = UIViewAutoresizing.All,
                ContentMode = UIViewContentMode.ScaleAspectFit
            };
            StatusBallLayout.Add(StatusBallImage);

            BallStatusText = new UILabel(StatusBallLayout.Frame)
            {
                Font = UIFont.SystemFontOfSize(pt2Px(15)),
                TextColor = AppDelegate.OsramColor,
                TextAlignment = UITextAlignment.Center
            };
            StatusBallLayout.Add(BallStatusText);

            Add(StatusBallLayout);

            UIPanGestureRecognizer panGesture = new UIPanGestureRecognizer();
            panGesture.AddTarget(() => HandleDrag(panGesture));
            StatusBallLayout.AddGestureRecognizer(panGesture);
            #endregion

            #region no more required
            //OnOffButton: THIS IS NOT REQUIRED ANYMORE
            UIView CenterButtonLayout = new UIView(new CGRect(0, 0, 100, 100));
            OnOffButtonImageView = new UIImageView(CenterButtonLayout.Frame)
            {
                AutoresizingMask = UIViewAutoresizing.All,
                ContentMode = UIViewContentMode.ScaleAspectFit
            };

            OnOffButtonImageView.UserInteractionEnabled = true;
            OnOffButtonImageView.ContentMode = UIViewContentMode.ScaleAspectFit;
            /*UIPanGestureRecognizer gestureRecognizer = new UIPanGestureRecognizer();
            gestureRecognizer.AddTarget(() => OnOffButtonImageViewOnTouch(gestureRecognizer));
            OnOffButtonImageView.AddGestureRecognizer(gestureRecognizer);*/
            CenterButtonLayout.Add(OnOffButtonImageView);

            CenterButtonText = new UILabel(new CGRect(0, 0, 50, 50));
            CenterButtonText.Font = UIFont.SystemFontOfSize(25);
            CenterButtonText.TextColor = AppDelegate.OsramColor;
            CenterButtonLayout.Add(CenterButtonText);
            Add(CenterButtonLayout);
            CenterButtonLayout.Hidden = true;
            #endregion

            InternalSetValue(MinValue);
        }

        private void OnCenterStatusTextClicked()
        {
            OnValueClicked?.Invoke(this, EventArgs.Empty);
        }

        private void HandleDrag(UIPanGestureRecognizer gesture)
        {
            if (gesture.State == UIGestureRecognizerState.Began)
            {
                initPosition = StatusBallLayout.Center;
            }
            else if (gesture.State == UIGestureRecognizerState.Ended)
            {

            }
            else if (gesture.State == UIGestureRecognizerState.Changed)
            {
                CGPoint newPosition = CGPoint.Add(initPosition, new CGSize(gesture.TranslationInView(StatusBallLayout)));
                CGPoint relativePosition = CGPoint.Subtract(newPosition, new CGSize(SliderImageView.Frame.Width / 2, SliderImageView.Frame.Height / 2));

                double l = Math.Sqrt(Math.Pow(relativePosition.X, 2) + Math.Pow(relativePosition.Y, 2));
                double deg = Math.Acos(relativePosition.Y / l) * 180 / Math.PI;

                if (relativePosition.X > 0) deg = 360 - deg;
                deg = Math.Max(minAngle, Math.Min(maxAngle, deg));

                int v = (int)Math.Round(MinValue + (deg - minAngle) / (maxAngle - minAngle) * (MaxValue - MinValue));
                if (v == 50)
                {
                    v = 50;
                }
                InternalSetValue(v);
                OnValueChanged?.Invoke(this, EventArgs.Empty);
            }
        }

        private void UpdateTextColorByEnabledStaus()
        {
            UIColor color = IsEnabled ?
                AppDelegate.OsramColor :
                UIColor.Gray;

            CenterButtonUnitText.TextColor =
                CenterButtonText.TextColor =
                BallStatusText.TextColor =
                CenterStatusText.TextColor = color;
        }

        private void UpdateSkin()
        {
            _skin = SliderWheelUtils.GetSliderWheelSkin(ControlType);

            SliderImageView.Image = _skin.SliderBkImage.ToNative();

            //OnOffButtonImageView.Image = _skin.MiddleButtonUncheckedImage.ToNative();

            StatusBallImage.Image = _skin.StatusBallImage.ToNative();
        }

        private void UpdateSlider()
        {
            double angle = minAngle + (maxAngle - minAngle) / (MaxValue - MinValue) * (Value - MinValue) + MIN_ANGLE;

            var size = Math.Min(SliderImageView.Frame.Width, SliderImageView.Frame.Height);
            var rads = angle * Math.PI / 180;

            var radius = size / 2 * 0.75;
            StatusBallLayout.Center = new CGPoint(SliderImageView.Frame.Width / 2 - Math.Sin(rads) * radius, SliderImageView.Frame.Height / 2 + Math.Cos(rads) * radius);
        }

        private void UpdateBallText()
        {
            if (BallStatusText != null)
                BallStatusText.Text = Value + "%";
        }

        private void UpdateCenterText()
        {
            if (CenterStatusText != null)
            {
                CenterStatusText.Text = OutputValue.ToString();
                CenterButtonUnitText.Text = OutputUnit;
                BottomStatusText.Text = SecondOutputValue;
            }
        }

        public override void LayoutSubviews()
        {
            base.LayoutSubviews();
            UpdateSlider();
        }

        #region not required anymore
        /*
        private void OnOffButtonImageViewOnTouch(UIPanGestureRecognizer gestureRecognizer)
        {
            nfloat delta = gestureRecognizer.TranslationInView(OnOffButtonImageView).Y;
            if (delta > 0)
            {
                OnMiddleButtonDown();
            }
            else if (delta < 0)
            {
                OnMiddleButtonUp();
            }
        }

        private void OnMiddleButtonUp()
        {
            OnOffButtonImageView.Image = IsChecked
                ? _skin.MiddleButtonCheckedImage.ToNative()
                : _skin.MiddleButtonUncheckedImage.ToNative();

            OnButtonClicked?.Invoke(this, new EventArgs());
        }

        private void OnMiddleButtonDown()
        {
            OnOffButtonImageView.Image = _skin.MiddleButtonPressedImage.ToNative();
        }
        */

        /*private static ViewGroup FindChildOfType<T>(ViewGroup parent)
        {
            if (parent.ChildCount == 0)
                return null;

            for (var i = 0; i < parent.ChildCount; i++)
            {
                var child = parent.GetChildAt(i) as ViewGroup;

                if (child == null)
                    continue;

                var type = child.GetType();

                if (child is T)
                {
                    return child;
                }

                var result = FindChildOfType<T>(child);
                if (result != null)
                    return result;
            }

            return null;
        }*/

        /*
        public override bool OnTouchEvent(MotionEvent motionEvent)
        {
            if (IsEnabled)
            {

                var index = motionEvent.ActionIndex;
                var x = motionEvent.GetX(index);
                var y = motionEvent.GetY(index);

                _currentPosition = new PointF(x, y);

                _currentAngle =
                    (int)SliderWheelUtils.GetTouchDegrees(x, y, Center.X, Center.Y, SliderWheelConsts.Clockwise);

                //Logger.Trace("x[" + x + "] y[" + y + "] cx[" + Center.X + "] cy[" + Center.Y + "] angle[" + _currentAngle + "]");

                Area currentArea;
                // Mvx.Resolve<TabGestureService>().SetGestureEnabled(false);
                MainActivity main = CrossCurrentActivity.Current.Activity as MainActivity;
                // { Xamarin.Forms.Platform.Android.ViewRenderer<Xamarin.Forms.CarouselView, Android.Support.V7.Widget.RecyclerView>}
                var viewGroup = (ViewGroup)((ViewGroup)main.FindViewById(Android.Resource.Id.Content)).GetChildAt(0);

                //Xamarin.Forms.Platform.Android.ViewRenderer<Xamarin.Forms.CarouselView, Android.Support.V7.Widget.RecyclerView> _currentViewPager = FindChildOfType<Xamarin.Forms.Platform.Android.ViewRenderer<Xamarin.Forms.CarouselView, Android.Support.V7.Widget.RecyclerView>>(viewGroup);
                ViewGroup _currentViewPager = FindChildOfType<ViewPager>(viewGroup);
                switch (motionEvent.Action)
                {
                    case MotionEventActions.Down:
                        //var type = _currentViewPager.GetType();
                        //type.InvokeMember("EnableGesture",
                        // BindingFlags.Instance | BindingFlags.Public | BindingFlags.SetProperty,
                        //Type.DefaultBinder, _currentViewPager, new object[] { false });
                        if (_currentViewPager is CarouselView.FormsPlugin.Android.HorizontalViewPager)
                        {
                            ((CarouselView.FormsPlugin.Android.HorizontalViewPager)_currentViewPager).SetPagingEnabled(false);
                        }

                        _dragging = true;
                        currentArea = GetCurrentArea();

                        if (ControlType == SliderWheelType.OnOffSlider || ControlType == SliderWheelType.SaveSlider)
                        {
                            CenterButtonLayout.Visibility = ViewStates.Invisible;
                            CenterStatusText.Visibility = (CenterButtonLayout.Visibility == ViewStates.Invisible)
                                ? ViewStates.Visible
                                : ViewStates.Invisible;
                        }

                        if (currentArea == Area.SliderArea)
                        {
                            UpdateValueFromSlider(_currentAngle);
                        }
                        _previousArea = currentArea;
                        break;
                    case MotionEventActions.Move:
                        _currentViewPager.Enabled = false;
                        currentArea = GetCurrentArea();

                        if (_previousArea == Area.NoKnownArea && currentArea == Area.SliderArea)
                        {
                            if (_previousArea == Area.SliderArea)
                            {
                                UpdateValueFromSlider(_currentAngle);
                            }
                            _previousArea = currentArea;
                        }
                        else if (_previousArea == Area.SliderArea)
                        {
                            UpdateValueFromSlider(_currentAngle, true);
                        }
                        break;
                    case MotionEventActions.Up:
                        _previousArea = Area.NoKnownArea;
                        _currentAngle = -1;
                        _dragging = false;

                        if (ControlType == SliderWheelType.OnOffSlider || ControlType == SliderWheelType.SaveSlider)
                        {
                            CenterButtonLayout.Visibility = ViewStates.Visible;
                            CenterStatusText.Visibility = (CenterButtonLayout.Visibility == ViewStates.Invisible)
                                ? ViewStates.Visible
                                : ViewStates.Invisible;
                        }


                        //_currentViewPager.GetType().InvokeMember("EnableGesture",
                        //BindingFlags.Instance | BindingFlags.Public | BindingFlags.SetProperty,
                        //Type.DefaultBinder, _currentViewPager, new object[] { true });
                        if (_currentViewPager is CarouselView.FormsPlugin.Android.HorizontalViewPager)
                        {
                            ((CarouselView.FormsPlugin.Android.HorizontalViewPager)_currentViewPager).SetPagingEnabled(true);
                        }
                        break;
                }
            }

            return true;
        }*/


        /*
        protected override void  (int w, int h, int oldw, int oldh)
        {
            ChangeSize(w, h, oldw, oldh);
            base.OnSizeChanged(w, h, oldw, oldh);
        }

        public void ChangeSize(int w, int h, int oldw, int oldh)
        {
            var scale = w / (_skin.SliderBkImage.ToNative().Size.Width * 1.0);

            _controlWidth = w;
            _ballWidth = (int)(_skin.StatusBallImage.ToNative().Size.Width * scale);
            _onoffButtonWidth = (int)(_skin.MiddleButtonCheckedImage.ToNative().Size.Width * scale);

            //TODO: CenterButtonLayout.ScaleX = (float)scale;
            //TODO: CenterButtonLayout.ScaleY = (float)scale;

            //TODO: StatusBallLayout.LayoutParameters.Width = _ballWidth;
            //TODO: StatusBallLayout.LayoutParameters.Height = _ballWidth;

            CenterStatusText.Font = UIFont.SystemFontOfSize(w < 700 ? 17 : 26);
            UpdateSlider();
        }*/
        /*
        private Area GetCurrentArea()
        {
            var distanceFromCenter = SliderWheelUtils.GetDistanceBetweenPoints(_currentPosition.X, _currentPosition.Y,
                (float)Center.X, (float)Center.Y);

            if (ControlType == SliderWheelType.OnOff || ControlType == SliderWheelType.OnOffSlider ||
                ControlType == SliderWheelType.SaveSlider)
            {
                if (_onoffButtonWidth / 2 >= distanceFromCenter)
                {
                    return Area.ButtonArea;
                }
            }
            if (ControlType == SliderWheelType.OnOffSlider || ControlType == SliderWheelType.Slider ||
                ControlType == SliderWheelType.SaveSlider)
            {
                if (_controlWidth / 2 >= distanceFromCenter && _onoffButtonWidth / 2 <= distanceFromCenter)
                {
                    if (SliderWheelConsts.Clockwise)
                    {
                        if (((_currentAngle - SliderWheelConsts.StartAngle + 360) % 360) <= SliderWheelConsts.SliderAngle)
                        {
                            return Area.SliderArea;
                        }
                        return Area.NoKnownArea;
                    }
                }
            }

            return Area.NoKnownArea;
        }

        private void UpdateValueFromSlider(int angle, bool limit = false)
        {
            var newValue = SliderWheelUtils.SliderAngleToValue(angle, SliderWheelConsts.StartAngle,
                SliderWheelConsts.SliderAngle, SliderWheelConsts.MinValue, SliderWheelConsts.MaxValue,
                SliderWheelConsts.Clockwise);

            if (limit && Math.Abs(Value - newValue) > SliderWheelConsts.MaxValueChangeAllowed)
            {
                if (newValue - Value < 0)
                {
                    newValue = 100;
                }
                else
                {
                    return;
                }
            }

            InternalSetValue(newValue);

            OnValueChanged?.Invoke(this, EventArgs.Empty);
        }
        */
        #endregion

    }
}
