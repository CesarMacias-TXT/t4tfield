﻿
using CoreGraphics;
using System;
using UIKit;
using Xamarin.Forms;

namespace Osram.T4TField.iOS.Controls
{
    public class BarIndicator : UIView
    {
        private UITextField textBar;
        private string id;
        private Color Color;

        private int _MaxValue = 100;
        private int current = 100;
        private int valueBeforeDrag = 0;

        public int Current {
            get {
                return current;
            }

            set {

                current = value;
                if (IsValid)
                    SetHeightAndText(MeasuredWidth);
            }

        }

        public int MeasuredWidth {
            get => (int)this.Frame.Width;
        }


        public int LimitedMinValue { get; set; }
        public int LimitedMaxValue { get; set; }
        public int DefaultValue { get; set; }

        private bool isValid;
        public bool IsValid {
            get { return isValid; }
            set {
                isValid = value;
                if (!isValid)
                    NotValidBarValue();
                else
                    //restore current value
                    SetHeightAndText(MeasuredWidth);
            }
        }

        private bool isEnabled;
        public bool IsEnabled {
            get { return isEnabled; }
            set {
                isEnabled = value;
                if (isEnabled)
                {
                    EnableBar();
                }
                else
                {
                    DisableBar();
                }
            }
        }


        public Action<int> valueChange;
        public Action OnScrollFinished;

        public BarIndicator(string automationId, int currentValue, int min, int max,
            int defaultValue, bool isBarEnabled, bool isValid, Color color)
        {
            Current = currentValue;
            LimitedMinValue = min;
            LimitedMaxValue = max;
            DefaultValue = defaultValue;
            IsEnabled = isBarEnabled;
            IsValid = isValid;
            id = automationId + "_Value";
            textBar = InitTextBar();
            Add(textBar);
            Color = color;
            LayoutMargins = new UIEdgeInsets(0, 0, 0, -1);

            UIPanGestureRecognizer panGesture = new UIPanGestureRecognizer();
            panGesture.AddTarget(() => handleDrag(panGesture));
            textBar.AddGestureRecognizer(panGesture);

            UITapGestureRecognizer tapGesture = new UITapGestureRecognizer();
            tapGesture.AddTarget(() => handleTap(tapGesture));
            AddGestureRecognizer(tapGesture);

            tapGesture = new UITapGestureRecognizer();
            tapGesture.AddTarget(() => handleTap(tapGesture));
            textBar.AddGestureRecognizer(tapGesture);
        }

        private void handleTap(UITapGestureRecognizer gesture)
        {
            float position = (float)gesture.LocationInView(this).Y;
            int value = 0;
            if (position <= Frame.Height / 3)
            {
                value = LimitedMaxValue;
            }
            else if (position >= Frame.Height / 3 * 2)
            {
                value = LimitedMinValue;
            }
            else
            {
                value = DefaultValue;
            }
            Current = value;
            valueChange(Current);
        }

        private void handleDrag(UIPanGestureRecognizer gesture)
        {
            if (!IsEnabled) return;

            if (gesture.State == UIGestureRecognizerState.Began)
            {
                valueBeforeDrag = Current;
            }
            else if (gesture.State == UIGestureRecognizerState.Ended)
            {
                if (!IsValuesInLimits() && current != DefaultValue)
                {
                    Current = DefaultValue;
                    SetHeightAndText(MeasuredWidth);
                }
                valueChange(Current);
            }
            else if (gesture.State == UIGestureRecognizerState.Changed)
            {

                CGRect thisFrame = Frame;

                float lh = (float)textBar.Font.LineHeight,
                      maxH = ((float)thisFrame.Height - lh),
                      offset = (float)gesture.TranslationInView(textBar).Y,
                      newValue = valueBeforeDrag - _MaxValue / maxH * offset;

                Current = (int)Math.Round(Math.Max(Math.Min(newValue, LimitedMaxValue), 0));

            }
        }

        public UITextField InitTextBar()
        {
            return new UITextField(new CGRect(0, 0, 50, 50))
            {
                Font = UIFont.SystemFontOfSize(20),
                TextColor = UIColor.White,
                TextAlignment = UITextAlignment.Center,
                BackgroundColor = GetColor(),
                UserInteractionEnabled = true,
                ShouldBeginEditing = (t) => false
            };
        }

        public void SetHeightAndText(int width)
        {
            if (textBar == null) return;

            textBar.Text = current.ToString();

            CGRect thisFrame = Frame,
                   textFrame = textBar.Frame;

            float lh = (float)textBar.Font.LineHeight,
                  maxH = ((float)thisFrame.Height - lh),
                  h = lh + maxH / _MaxValue * current,
                  y = (float)thisFrame.Height - h,
                  alpha = (h * .784f / maxH) + .216f;

            textFrame.Y = y;
            textFrame.Height = h;
            textBar.Frame = textFrame;

            textBar.BackgroundColor = (IsValuesInLimits() || current == DefaultValue) ?
                GetColor(alpha) : UIColor.FromRGB(127, 0, 0); // Dark Red
        }

        private void handleScrollFinished()
        {
            OnScrollFinished?.Invoke();
        }

        private bool IsValuesInLimits()
        {
            var value = Convert.ToInt16(textBar.Text);
            if (value > LimitedMaxValue || value < LimitedMinValue)
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        private void EnableBar()
        {
            if (textBar != null) textBar.BackgroundColor = GetColor();
        }

        private void DisableBar()
        {
            if (textBar != null) textBar.BackgroundColor = UIColor.FromRGB(221, 221, 221);
        }

        private void NotValidBarValue()
        {
            if (textBar != null)
            {
                textBar.BackgroundColor = UIColor.FromRGB(238, 238, 238);
                textBar.Text = string.Empty;
            }
        }

        private UIColor GetColor(float? alpha = null)
        {
            return (IsEnabled) ?
                UIColor.FromRGBA((Single)Color.R, (Single)Color.G, (Single)Color.B, (Single)(alpha ?? 1)) :
                UIColor.FromRGBA(221, 221, 221, 255);
        }
    }
}