﻿using System;
using UIKit;

namespace Osram.T4TField.iOS.Controls
{
    public class UIArrayPickerModel : UIPickerViewModel
    {
        private String[] items;
        public event EventHandler<PickerChangedEventArgs> PickerChanged;

        public UIArrayPickerModel(String[] items)
        {
            this.items = items;
        }

        public override nint GetComponentCount(UIPickerView pickerView) => 1;

        public override nint GetRowsInComponent(UIPickerView pickerView, nint component) => items.Length;

        public override string GetTitle(UIPickerView pickerView, nint row, nint component) => items[row];

        public override nfloat GetRowHeight(UIPickerView pickerView, nint component) => 40f;

        public override void Selected(UIPickerView pickerView, nint row, nint component)
        {
            PickerChanged?.Invoke(this, new PickerChangedEventArgs { SelectedValue = (int)row });
        }

    }

    public class PickerChangedEventArgs : EventArgs
    {
        public int SelectedValue { get; set; }
    }
}
