using MvvmCross.Platform.Plugins;

namespace Osram.T4TField.iOS.Bootstrap
{
    public class FilePluginBootstrap
        : MvxLoaderPluginBootstrapAction<MvvmCross.Plugins.File.PluginLoader, MvvmCross.Plugins.File.iOS.Plugin>
    {
    }
}