using MvvmCross.Platform.Plugins;

namespace Osram.T4TField.iOS.Bootstrap
{
    public class MessengerPluginBootstrap
        : MvxPluginBootstrapAction<MvvmCross.Plugins.Messenger.PluginLoader>
    {
    }
}