using MvvmCross.Platform.Plugins;

namespace Osram.T4TField.iOS.Bootstrap
{
    public class ResourceLoaderPluginBootstrap
        : MvxLoaderPluginBootstrapAction<MvvmCross.Plugins.ResourceLoader.PluginLoader, MvvmCross.Plugins.ResourceLoader.iOS.Plugin>
    {
    }
}