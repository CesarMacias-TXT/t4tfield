﻿using Foundation;
using Osram.T4TField.Contracts;
using T4TField.Ui.Shared.Utils;

namespace Osram.T4TField.iOS
{
    public class AppInfo : IAppInfo
    {
        private readonly IInfoUtils infoUtils = new InfoUtils();

        public string Version => infoUtils.GetAppVersion();

        public string ApplicationName => NSBundle.MainBundle.InfoDictionary["CFBundleName"].ToString();
    }
}