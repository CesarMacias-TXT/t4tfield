﻿using Osram.T4TField.Contracts;

namespace Osram.T4TField.iOS.Services
{
    public class LocalNotificationService : ILocalNotificationService
    {
        public LocalNotificationService()
        {
        }

        public void Cancel(int id)
        {

        }

        public void Show(string title, string body, int id = 0)
        {

        }
    }
}
