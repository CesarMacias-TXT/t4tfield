﻿using Osram.TFTBackend.RestService.Contracts;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;

namespace Osram.T4TField.iOS.Services
{
    public class T4THttpClient : HttpClient, IT4THttpClient
    {
        public T4THttpClient() : base(GetSSL())
        {
        }

        private static HttpClientHandler GetSSL()
        {
            return new HttpClientHandler
            {
                ServerCertificateCustomValidationCallback = (message, certificate, chain, sslPolicyErrors) => true
            };
        }
    }
}
