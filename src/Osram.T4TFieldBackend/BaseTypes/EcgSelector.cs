﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Osram.TFTBackend.BaseTypes
{
    /// <summary>
    /// Combination from Gtin, firmware and hardware version, unique identification for a ecg.
    /// </summary>
    public struct EcgSelector : IEquatable<EcgSelector>
    {
        public long Value { get; }
        public bool IsValid => Value != 0;

        public EcgSelector(long gtin, byte firmwareVersion, byte hardwareVersion)
        {
            Value = gtin + ((long)firmwareVersion << 48) + ((long)hardwareVersion << 56);
        }

        public EcgSelector(long value)
        {
            Value = value;
        }

        public int GetHwVersionDifference(EcgSelector other)
        {
            int hwV1 = (int)((Value >> 56) & 0xFF);
            int hwV2 = (int)((other.Value >> 56) & 0xFF);
            return Math.Abs(hwV1 - hwV2);
        }

        public bool EqualsWithoutHwVersion(EcgSelector other)
        {
            return (Value & 0x00FFFFFFFFFFFFFF) == (other.Value & 0x00FFFFFFFFFFFFFF);
        }

        public override bool Equals(object obj)
        {
            if (!(obj is EcgSelector selector))
                return false;
            return Equals(selector);
        }

        public bool Equals(EcgSelector other)
        {
            return Value == other.Value;
        }

        public static bool operator ==(EcgSelector left, EcgSelector right)
        {
            return left.Equals(right);
        }

        public static bool operator !=(EcgSelector left, EcgSelector right)
        {
            return !left.Equals(right);
        }

        public override int GetHashCode()
        {
            return Value.GetHashCode();
        }

        public override string ToString()
        {
            return $"EcgSelector = {Value:X16}";
        }
    }
}
