﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Osram.TFTBackend.BaseTypes.Exceptions
{
    public class OperationFailedException : Exception
    {
        private int _errorCode;

        public int ErrorCode => _errorCode;

        public OperationFailedException() { }

        public OperationFailedException(String message, int errorCode) : base(message)
        {
            _errorCode = errorCode;
        }

        public OperationFailedException(String message, int errorCode, Exception innerException) : base(message, innerException)
        {
            _errorCode = errorCode;
        }
    }
}
