﻿namespace Osram.TFTBackend.BaseTypes
{
    public class Gtin
    {
        private readonly long _gtin;

        public Gtin(long gtin)
        {
            _gtin = gtin;
        }

        public long GtinValue => _gtin;

        public bool IsValid => _gtin != 0;

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            Gtin second = obj as Gtin;
            return _gtin == second?._gtin;
        }

        public override int GetHashCode()
        {
            return _gtin.GetHashCode();
        }

        public override string ToString()
        {
            return _gtin.ToString();
        }
    }
}
