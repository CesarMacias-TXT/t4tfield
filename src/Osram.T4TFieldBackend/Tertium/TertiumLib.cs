﻿using System.Linq;
using System.Text;
using Osram.TFTBackend.NfcService.NfcDataTypes;

namespace Osram.TFTBackend.Tertium
{
    public class TertiumLib
    {
        const string PREFIX_TUNNELMODE_COMMAND = "#:";
        const string PREFIX_TERTIUM_COMMAND = "$:";
        const string SUFFIX_COMMAND = "\r\n";
        byte[] SuffixArray = Encoding.UTF8.GetBytes(SUFFIX_COMMAND);
        byte[] TertiumUID = new byte[] { 23, 95, 143, 35, 165, 112, 73, 189, 150, 39, 129, 90, 106, 39, 222, 42 };

        //general commands of tertium protocol
        enum CommandCode
        {
            BEEPER = 0x01,
            LED = 0x02,
            IO = 0x03,
            BLUETOOTH = 0x04,
            STATUS = 0x05,
            MODE = 0x06,
            SETIP = 0x0A,
            SETFRAME = 0x0B,
            SETBAUDARATE = 0x0C,
            SETAUTOOFF = 0x0D,
            SETMODE = 0x0E,
            SETSTANDARD = 0x0F
        }

        //return codes of tertium protocol
        enum ReturnCode
        {
            SUCCESS = 0x00, // Successful operation
            INVALID_PARAM = 0x0C, // Invalid parameter
            COMMAND_NOT_IMPLEMENTED = 0x0E,// Command not implemented
            COMMAND_NOT_VALID = 0x0F // Command not valid or incorrect format
        }

        public class RFID_ISO15693_Protocol
        {
            //RFID_ISO15693 commands of tertium protocol
            enum ISO_Command
            {
                INVENTORY = 0x21,
                READ = 0x23,
                WRITE = 0x24,
                LOCK = 0x25,
                SETREGISTER = 0x2E,
                SETPOWER = 0x2F
            }
        }

        public byte[] CreateTertiumCommandFromIsoStandard(ByteArray command)
        {
            //command to set RF Timeout, 0C, don't use tunnel mode
            string cmd = (command[0] == 0x0C ? PREFIX_TERTIUM_COMMAND : PREFIX_TUNNELMODE_COMMAND) + command.ToString("") + SUFFIX_COMMAND;
            return Encoding.UTF8.GetBytes(cmd);
        }     

        internal bool IsResponseCompleted(byte[] resp)
        {
            if(resp.Count() < 2)
                return false;

            return resp[resp.Length - 2] == SuffixArray[0] && resp[resp.Length - 1] == SuffixArray[1];
        }

        public byte[] GetFilterTertiumUID()
        {
            return TertiumUID;
        }
    }
}
