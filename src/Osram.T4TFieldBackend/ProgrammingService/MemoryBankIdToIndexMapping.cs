﻿using System.Collections.Generic;
using Osram.TFTBackend.Nfc2.Tag;

namespace Osram.TFTBackend.ProgrammingService
{
    internal class MemoryBankIdToIndexMapping
    {
        private Dictionary<int, int> _mapping = new Dictionary<int, int>(10);

        public void CreateMapping(Nfc2Tag tag)
        {
            _mapping.Clear();
            for (int i = 0; i < tag.TableOfContent.ContentTable.Count; i++)
            {
                _mapping.Add(tag.TableOfContent.ContentTable[i].MpcId, i);
            }
        }

        public int GetIndexFromId(int id)
        {
            return _mapping[id];
        }
    }
}