﻿using MvvmCross.Plugin.Messenger;
using Osram.TFTBackend.BaseTypes;
using Osram.TFTBackend.BaseTypes.Exceptions;
using Osram.TFTBackend.ConfigurationService.Contracts;
using Osram.TFTBackend.ConfigurationService.DataModel;
using Osram.TFTBackend.DataModel.Feature.Contracts;
using Osram.TFTBackend.KeyService;
using Osram.TFTBackend.KeyService.Contracts;
using Osram.TFTBackend.Messaging;
using Osram.TFTBackend.Nfc2.Tag;
using Osram.TFTBackend.Nfc2.Tag.Operations;
using Osram.TFTBackend.Nfc2.Tag.Services;
using Osram.TFTBackend.Nfc2.TagStructures;
using Osram.TFTBackend.NfcService.Contracts;
using Osram.TFTBackend.PersistenceService;
using Osram.TFTBackend.PersistenceService.Contracts;
using Osram.TFTBackend.ProgrammingService.Contracts;
using Osram.TFTBackend.RestService.Contracts;
using Plugin.Settings.Abstractions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;

namespace Osram.TFTBackend.ProgrammingService
{
    public class ProgrammingService : IProgrammingService
    {
        private IMvxMessenger _messenger;
        private INfcReader _reader;
        private IRestService _restService;
        private readonly ISettings _settings;
        private readonly IConfigurationService _configurationService;
        private readonly IConfigurationDataStore _configurationDataStore;
        private readonly IPersistenceService _persistenceService;
        private readonly IKeyService _keyService;

        private Nfc2Tag _nfc2Tag = new Nfc2Tag();
        private MemoryBankIdToIndexMapping _memoryBankMapping = new MemoryBankIdToIndexMapping();

        private readonly byte[] _banksIdToExclude = new byte[] { 0, 20, 28, 29, 254, 255 };

        public event EventHandler<int> ProgressChange;

        public ProgrammingService(IMvxMessenger messenger, INfcReader reader, IRestService restService, ISettings settings, IConfigurationService configurationService, IConfigurationDataStore configurationDataStore, IPersistenceService persistenceService, IKeyService keyService)
        {
            Logger.Trace("ProgrammingService constructor");
            _messenger = messenger;
            _reader = reader;
            _restService = restService;
            _settings = settings;
            _configurationService = configurationService;
            _configurationDataStore = configurationDataStore;
            _persistenceService = persistenceService;
            _keyService = keyService;
        }

        public EcgSelector ReadDevice(CancellationToken cancellationToken)
        {
            Logger.Trace("ProgrammingService reading device");
            TagOperations tagOperations = new TagOperations(_reader, ProgressChange);
            bool result = tagOperations.ReadTag(_nfc2Tag, cancellationToken);
            if (!result || !IsTagValid(_nfc2Tag))
            {
                Logger.Error("ProgrammingService reading device failed");
                return new EcgSelector();
            }

            _memoryBankMapping.CreateMapping(_nfc2Tag);
            return new EcgSelector(_nfc2Tag.DeviceIdentificationRegisters.Gtin, _nfc2Tag.DeviceIdentificationRegisters.FwMajorVersion, _nfc2Tag.DeviceIdentificationRegisters.HwMajorVersion);
        }

        private bool IsTagValid(Nfc2Tag tag)
        {
            if (!tag.DeviceIdentificationRegisters.CrcIsCorrect) { Logger.Error("IsTagValid: DeviceIdentificationRegisters CRC Error"); return false; }
            if (!tag.TableOfContent.CrcIsCorrect) { Logger.Error("IsTagValid: TableOfContent CRC Error"); return false; }
            if (!tag.StatusRegister.CrcIsCorrect) { Logger.Error("IsTagValid: StatusRegister CRC Error"); return false; }
            if (!tag.ControlRegister.CrcIsCorrect) { Logger.Error("IsTagValid: ControlRegister CRC Error"); return false; }
            int index = 0;
            foreach (var memoryBank in tag.MemoryBanks)
            {
                if (tag.TableOfContent.ContentTable[index].MbBaseAddress >= tag.DeviceIdentificationRegisters.ProtectedMemoryAreaStartAddress)
                    continue;
                if (!memoryBank.CrcIsCorrect) { Logger.Error($"IsTagValid: Memorybank[{index}] CRC Error"); return false; }
                index++;
            }
            return true;
        }

        public void GetPropertyValues(IEnumerable<EcgProperty> properties)
        {
            foreach (var property in properties)
            {
                ReadProperty(property);
            }

            _messenger.Publish(new Events.OnConfigurationPropertiesReadEvent(this));
        }

        public void ProgramConfiguration(IEnumerable<EcgProperty> properties, string type = "")
        {
            Logger.Trace("ProgrammingService.ProgramConfiguration start programming ...");
            var writableProperties = FilterForWritableProperties(properties);
            UpdateMemoryBanks(writableProperties);
            List<int> memoryBankIndices = GetMemoryBankIndices();
            IConfiguration configuration = _configurationService.GetCurrentConfiguration();

            //if the online services are active, send programming data
            if (_settings.GetValueOrDefault(InitalizeDatabaseService.InitalizeDatabaseService.IsOnlineServicesActive, false))
            {
                if(type == "osrtupImport")
                {
                    _restService.PostAsyncProgrammingData(configuration, writableProperties.ToList());
                }
                else
                {
                    _restService.PostAsyncProgrammingDataLocal(configuration, writableProperties.ToList());
                }
                
                ProgrammDevice(_nfc2Tag, memoryBankIndices, 10, _nfc2Tag.NfcTagSerial.Replace("-", ""), configuration);
            }
            else
            {
                ProgrammDevice(_nfc2Tag, memoryBankIndices);
            }

            Logger.Trace("ProgrammingService.ProgramConfiguration programming done.");
        }

        private IEnumerable<EcgProperty> FilterForWritableProperties(IEnumerable<EcgProperty> properties)
        {
            return properties.Where(prop => prop.ReadOnly == false);
        }

        private void UpdateMemoryBanks(IEnumerable<EcgProperty> properties)
        {
            Logger.Trace("ProgrammingService.UpdateMemoryBanks ...");
            foreach (var property in properties)
            {
                WriteProperty(property);
            }
        }

        private void ProgrammDevice(Nfc2Tag tag, List<int> memoryBankIndices, int firstProgress = 10, string serialNumber = "", IConfiguration configuration = null)
        {
            Logger.Trace("ProgrammingService.ProgrammDevice ...");
            TagEcgOperations ecgOperations = new TagEcgOperations(_reader, ProgressChange, firstProgress, new CurrentTimeService(), new GetRandomByteService());

            try
            {
                ecgOperations.ProgramMemoryBanks(tag, memoryBankIndices.ToArray());

                if (_settings.GetValueOrDefault(InitalizeDatabaseService.InitalizeDatabaseService.IsOnlineServicesActive, false))
                {
                    _restService.PostAsyncProgrammingResult(configuration, serialNumber, "0");
                }
            }
            catch (RFPasswordFailedException rfPasswordFailedException)
            {
                if (_settings.GetValueOrDefault(InitalizeDatabaseService.InitalizeDatabaseService.IsOnlineServicesActive, false))
                {
                    _restService.PostAsyncProgrammingResult(configuration, serialNumber, "1");
                }
                throw rfPasswordFailedException;
            }
            catch (Exception e)
            {
                if (_settings.GetValueOrDefault(InitalizeDatabaseService.InitalizeDatabaseService.IsOnlineServicesActive, false))
                {
                    _restService.PostAsyncProgrammingResult(configuration, serialNumber, "1");
                }
                throw e;
            }           
        }

        private List<int> GetMemoryBankIndices()
        {
            List<int> indices = new List<int>(10);
            for (int i = 0; i < _nfc2Tag.MemoryBanks.Count; i++)
            {
                if (_nfc2Tag.MemoryBanks[i].IsModified)
                    indices.Add(i);
            }
            return indices;
        }

        private void WriteProperty(EcgProperty property)
        {
            if (property.Allocation != null)
            {
                MemoryBank mb = GetMemoryBank(property);
                if (WritePropertyValue(property, mb.Data.ToList()))
                    mb.IsModified = true;
            }
        }

        private bool WritePropertyValue(EcgProperty property, List<byte> memoryBankData)
        {
            if (property.Allocation.BitCount == 0)
            {
                if (property.Value.ProgrammingValue.IsArray)
                    return WriteArrayValue(property.Value.ProgrammingValue, property.Allocation.ByteOffset, property.Allocation.ByteCount, property.Allocation.NetworkByteOrder, memoryBankData);
                else
                    return WriteNumberValue(property.Value.ProgrammingValue, property.Allocation.ByteOffset, property.Allocation.ByteCount, property.Allocation.NetworkByteOrder, memoryBankData);
            }
            else
                return WriteBitValue(property.Value.ProgrammingValue, property.Allocation.ByteOffset, property.Allocation.BitOffset, property.Allocation.BitCount, memoryBankData);
        }

        private bool WriteArrayValue(ProgrammingValue value, int offset, int size, Allocation.ByteOrder byteOrder, List<byte> memoryBankData)
        {
            if (memoryBankData.Count == 0)
                return false;
            bool isModified = false;
            for (int i = 0; i < size; i++)
            {
                if (memoryBankData[offset + i] != value[i])
                {
                    memoryBankData[offset + i] = value[i];
                    isModified = true;
                }
            }
            return isModified;
        }

        private bool WriteNumberValue(long value, int offset, int size, Allocation.ByteOrder byteOrder, List<byte> memoryBankData)
        {
            bool isModified = false;
            int shift = (size - 1) * 8;
            for (int i = 0; i < size; i++)
            {
                byte newValue = (byte)((value >> shift) & 0xFF);
                if (memoryBankData[offset + i] != newValue)
                {
                    memoryBankData[offset + i] = newValue;
                    isModified = true;
                }
                shift -= 8;
            }
            return isModified;
        }

        private bool WriteBitValue(long value, int byteOffset, int bitOffset, int bitCount, List<byte> memoryBankData)
        {
            int bitMask = ~(((1 << bitCount) - 1) << bitOffset);
            byte newValue = (byte)((memoryBankData[byteOffset] & bitMask) | ((byte)value << bitOffset));
            if (memoryBankData[byteOffset] != newValue)
            {
                memoryBankData[byteOffset] = newValue;
                return true;
            }
            return false;
        }

        private void ReadProperty(EcgProperty property)
        {
            if (property.Allocation != null)
            {
                MemoryBank mb = GetMemoryBank(property);
                ReadPropertyValue(property, mb.Data.ToList());
            }
        }

        private void ReadPropertyValue(EcgProperty property, List<byte> memoryBankData)
        {
            if (property.Allocation.BitCount == 0)
            {
                if (property.Value.ProgrammingValue.IsArray)
                    property.Value.ProgrammingValue = ReadArrayValue(property.Allocation.ByteOffset, property.Allocation.ByteCount, property.Allocation.NetworkByteOrder, memoryBankData);
                else
                    property.Value.ProgrammingValue = ReadNumberValue(property.Allocation.ByteOffset, property.Allocation.ByteCount, property.Allocation.NetworkByteOrder, memoryBankData);
            }
            else
                property.Value.ProgrammingValue = ReadBitValue(property.Allocation.ByteOffset, property.Allocation.BitOffset, property.Allocation.BitCount, memoryBankData);
        }

        private ProgrammingValue ReadArrayValue(int offset, int size, Allocation.ByteOrder byteOrder, List<byte> memoryBankData)
        {
            if (offset + size > memoryBankData.Count)
            {
                Logger.Error("ProgrammingService.ReadArrayValue offset out of range,");
                return new ProgrammingValue(new byte[0]);
            }
            return new ProgrammingValue(memoryBankData.GetRange(offset, size).ToArray());
        }

        private long ReadNumberValue(int offset, int size, Allocation.ByteOrder byteOrder, List<byte> memoryBankData)
        {
            if (offset + size > memoryBankData.Count)
            {
                Logger.Error("ProgrammingService.ReadNumberValue offset out of range,");
                return 0;
            }

            long value = 0;
            int shift = (size - 1) * 8;

            if (byteOrder == Allocation.ByteOrder.Lsb)
            {
                for (int i = (size - 1); i >= 0; i--)
                {
                    value += memoryBankData[offset + i] << shift;
                    shift -= 8;
                }
            }
            else
            {
                for (int i = 0; i < size; i++)
                {
                    value += memoryBankData[offset + i] << shift;
                    shift -= 8;
                }
            }            
            return value;
        }

        private long ReadBitValue(int byteOffset, int bitOffset, int bitCount, List<byte> memoryBankData)
        {
            int bitMask = (1 << bitCount) - 1;
            return (memoryBankData[byteOffset] >> bitOffset) & bitMask;
        }

        private MemoryBank GetMemoryBank(EcgProperty property)
        {
            return _nfc2Tag.MemoryBanks[property.Allocation.MemoryBank];
        }

        public Nfc2Tag CopyDevice()
        {
            if (IsTagValid(_nfc2Tag))
                return _nfc2Tag;
            return null;
        }

        public bool IsSameDevice()
        {
            Logger.Trace("IsSameDevice started");
            ProgressChange(this, 5);
            Nfc2Tag destTag = new Nfc2Tag();
            LowLevelOperations lowLevelOperations = new LowLevelOperations(_reader);
            destTag.NfcTagSerial = _reader.NfcTagSerial;
            lowLevelOperations.ReadDeviceIdentificationRegisters(destTag);
            if (destTag.DeviceIdentificationRegisters.CrcIsCorrect)
            {
                if (_nfc2Tag.DeviceIdentificationRegisters.Gtin == destTag.DeviceIdentificationRegisters.Gtin
                        && _nfc2Tag.NfcTagSerial == destTag.NfcTagSerial)
                {
                    return true;
                }
            }
            return false;
        }

        public bool IsSameFamilyDevice(uint currentPassword)
        {
            Logger.Trace("IsSameFamilyDevice started");
            ProgressChange(this, 5);

            // Read Config Lock
            Nfc2Tag destTag = ReadDeviceIdentification();
            var masterPasswordDest = ReadMasterPassword(destTag);
            var servicePasswordDest = ReadServicePassword(destTag);
            var servicePasswordDecryptedArray = DecryptPasswordForCopy(servicePasswordDest);
            // In some architecture the bytes are read in LittleEndian
            if (BitConverter.IsLittleEndian && servicePasswordDecryptedArray != null)
            {
                Array.Reverse(servicePasswordDecryptedArray, 0, 4);
            }
            var servicePasswordDecrypted = servicePasswordDecryptedArray == null ? 0 : BitConverter.ToUInt32(servicePasswordDecryptedArray, 0);

            //Read Tuning Factor
            IConfiguration configuration = _configurationService.GetCurrentConfiguration();
            EcgProperty tuningFactorFeatureDest;
            IEnumerable<EcgProperty> list = configuration.PropertyDictonary.Where(
                    c => (c.Key == "TF-0:Enable")).Select(c => c.Value);
            // Needed to retrieve the correct value
            GetPropertyValues(list);
            tuningFactorFeatureDest = list.First();
            EcgProperty operatingCurrentFeatureDest = configuration.PropertyDictonary.Where(
                    c => (c.Key == "OTConfig-2:DefaultOpCurrent") || (c.Key == "PwmConfig-0:DefaultOpCurrent") || (c.Key == "OTConfig-3:DefaultOpCurrent")).Select(c => c.Value).First();

            //Read Permissions
            var permissionUserDest = ReadPermissionUser(destTag);
            var permissionServiceDest = ReadPermissionService(destTag);            
            ProtectionLock tuningFactorFeatureLock = GetTuningFactorFamilyProtectionLock(configuration, (int)permissionUserDest, (int)permissionServiceDest);

            if (destTag.DeviceIdentificationRegisters.CrcIsCorrect
                && _nfc2Tag.DeviceIdentificationRegisters.Gtin == destTag.DeviceIdentificationRegisters.Gtin)
            {
                if (masterPasswordDest == 0)
                {
                    WriteFamilyProperty(tuningFactorFeatureDest);
                    WriteFamilyProperty(operatingCurrentFeatureDest);
                    return true;
                }

                switch (tuningFactorFeatureLock)
                {
                    case ProtectionLock.Master:
                        return false;
                    case ProtectionLock.Service:
                        if(currentPassword == servicePasswordDecrypted)
                        {
                            WriteFamilyProperty(tuningFactorFeatureDest);
                            WriteFamilyProperty(operatingCurrentFeatureDest);
                        }
                        return currentPassword == servicePasswordDecrypted;
                    case ProtectionLock.None:
                        WriteFamilyProperty(tuningFactorFeatureDest);
                        WriteFamilyProperty(operatingCurrentFeatureDest);
                        return true;
                    default:
                        return false;
                }
            }
            return false;
        }

        private void WriteFamilyProperty(EcgProperty property)
        {
            MemoryBank mb = GetMemoryBank(property);
            mb.IsModified = true;
        }

        private ProtectionLock GetTuningFactorFamilyProtectionLock(IConfiguration configuration, int permissionsUser, int permissionsService)
        {
            Permission tuningFactorFeaturePermission;

            if (configuration.PermissionsDictonary.TryGetValue("Tuning Factor: Level", out tuningFactorFeaturePermission))
            {              
                int permissionUserBit = permissionsUser & (1 << tuningFactorFeaturePermission.Value);
                if (permissionUserBit > 0)
                {
                    return ProtectionLock.None;
                }

                int permissionServiceBit = permissionsService & (1 << tuningFactorFeaturePermission.Value);
                if (permissionServiceBit > 0)
                {
                    return ProtectionLock.Service;
                }
                return ProtectionLock.Master;
            }
            else
            {
                return ProtectionLock.None;
            }
        }

        public bool TuningFactorFeatureIsEnabled(EcgProperty tuningFactorFeatureDest, Dictionary<String, EcgProperty>  PropertyDictonary)
        {
            if(tuningFactorFeatureDest != null)
            {
                return tuningFactorFeatureDest.BoolValue;
            }
            else
            {
                EcgProperty tuningFactor = PropertyDictonary.Where(
                    c => (c.Key == "TF-0:TuningFactor") || (c.Key == "PwmConfig-0:TuningFactor")).Select(c => c.Value).First();
                EcgProperty maximumTuningFactor = PropertyDictonary.Where(
                    c => (c.Key == "TF-0:MaximumTuningFactor") || (c.Key == "PwmConfig-0:MaximumTuningFactor")).Select(c => c.Value).First();
                EcgProperty minimumTuningFactor = PropertyDictonary.Where(
                    c => (c.Key == "TF-0:MinimumTuningFactor") || (c.Key == "PwmConfig-0:MinimumTuningFactor")).Select(c => c.Value).First();

                if (tuningFactor.IntValue == 0 || (maximumTuningFactor.IntValue == maximumTuningFactor.IntValue))
                {
                    return false;
                }

                return true;
            }
        }

        public bool PasteDevice(Nfc2Tag sourceTag)
        {
            Logger.Trace("PasteDevice started");
            ProgressChange(this, 5);
            Nfc2Tag destTag = ReadDeviceIdentification();
            if (IsPasteOperationPossible(sourceTag, destTag))
            {
                var masterPasswordSource = ReadMasterPassword(sourceTag);
                var masterPasswordDest = ReadMasterPassword(destTag);
                // Source is not protected
                //if (masterPasswordSource == 0)
                //{
                //    throw new OperationFailedException("PasteDevice: Master passwords is not setted!", ErrorCodes.MasterKeysNotSetted);
                //}
                //// Both are protected
                //else
                //{
                // Are the same password or the destination is not protected
                if (masterPasswordSource == masterPasswordDest || masterPasswordDest == 0)
                {
                    var servicePasswordSource = ReadServicePassword(sourceTag);
                    //var servicePasswordPaste = ReadServicePassword(destTag);

                    var masterPasswordDecrypted = DecryptPasswordForCopy(masterPasswordSource);
                    var servicePasswordDecrypted = DecryptPasswordForCopy(servicePasswordSource);
                    var oldMasterPassword = BitConverter.GetBytes(masterPasswordDest);
                    var masterPasswordEncrypted = BitConverter.GetBytes(masterPasswordSource);
                    var servicePasswordEncrypted = BitConverter.GetBytes(servicePasswordSource);

                    CopyMemoryBanks(sourceTag, destTag, oldMasterPassword, masterPasswordDecrypted, servicePasswordDecrypted, masterPasswordEncrypted, servicePasswordEncrypted);

                    Logger.Trace("PasteDevice success");
                    return true;
                }
                // Are not the same password
                else
                {
                    throw new OperationFailedException("PasteDevice: Master passwords are not the same!", ErrorCodes.MasterKeysNotMatching);
                }
                //}
            }
            return false;
        }

        private byte[] DecryptPasswordForCopy(uint passwordEcryptedUint)
        {
            // if the password is zero return null
            if (passwordEcryptedUint == 0u)
                return null;

            // Decrypt the password
            var passwordDecryptedByte = Pin.DecryptMsk(passwordEcryptedUint);
            // Reverse it
            Array.Reverse(passwordDecryptedByte);
            // Return it
            return passwordDecryptedByte;
        }

        private int GetMemoryBankIndex(Nfc2Tag tag, int id)
        {
            for (int i = 0; i < tag.TableOfContent.ContentTable.Count; i++)
            {
                if (tag.TableOfContent.ContentTable[i].MpcId == id)
                    return i;
            }
            return -1;
        }

        private uint ReadMasterPassword(Nfc2Tag tag)
        {
            uint masterPassword = 0;

            int masterPasswordOffset = 0;
            int pageIndex = GetMemoryBankIndex(tag, 29);    // use bio page if exists
            if (pageIndex < 0)
            {
                // in case of older devices biopage does not exist, use MSK page instead
                pageIndex = GetMemoryBankIndex(tag, 27);

                //in case of ot fit drivers, use PwmConfig page instead
                if (pageIndex < 0)
                {
                    pageIndex = GetMemoryBankIndex(tag, 50);
                }
            }
            bool memoryBanksWasNull = false;
            if (tag.MemoryBanks == null)
            {
                memoryBanksWasNull = true;
                CreateDestMemoryBankList(tag, tag.TableOfContent.ContentTable.Count);
                LowLevelOperations lowLevelOperations = new LowLevelOperations(_reader);
                lowLevelOperations.ReadMemoryBank(tag, pageIndex);
            }
            masterPassword = tag.MemoryBanks[pageIndex].Data.GetUInt32(masterPasswordOffset);
            if (memoryBanksWasNull)
            {
                tag.MemoryBanks = null;
            }

            return masterPassword;
        }

        private uint ReadServicePassword(Nfc2Tag tag)
        {
            uint servicePassword = 0;

            int servicePasswordOffset = 4;
            int pageIndex = GetMemoryBankIndex(tag, 29);
            if (pageIndex < 0)
            {
                // in case of older devices biopage does not exist, use MSK page instead
                pageIndex = GetMemoryBankIndex(tag, 27);
                servicePasswordOffset = 12;

                //in case of ot fit drivers, use PwmConfig page instead
                if (pageIndex < 0)
                {
                    pageIndex = GetMemoryBankIndex(tag, 50);
                    servicePasswordOffset = 4;
                }
            }
            bool memoryBanksWasNull = false;
            if (tag.MemoryBanks == null)
            {
                memoryBanksWasNull = true;
                CreateDestMemoryBankList(tag, tag.TableOfContent.ContentTable.Count);
                LowLevelOperations lowLevelOperations = new LowLevelOperations(_reader);
                lowLevelOperations.ReadMemoryBank(tag, pageIndex);
            }
            servicePassword = tag.MemoryBanks[pageIndex].Data.GetUInt32(servicePasswordOffset);
            if (memoryBanksWasNull)
            {
                tag.MemoryBanks = null;
            }

            return servicePassword;
        }

        private uint ReadPermissionUser(Nfc2Tag tag)
        {
            uint permissionUser = 0;

            int permissionUserOffset = 20;
            int pageIndex = GetMemoryBankIndex(tag, 27);

            //in case of ot fit drivers, use PwmConfig page instead
            if (pageIndex < 0)
            {
                pageIndex = GetMemoryBankIndex(tag, 50);
                permissionUserOffset = 8;
            }

            bool memoryBanksWasNull = false;
            if (tag.MemoryBanks == null)
            {
                memoryBanksWasNull = true;
                CreateDestMemoryBankList(tag, tag.TableOfContent.ContentTable.Count);
                LowLevelOperations lowLevelOperations = new LowLevelOperations(_reader);
                lowLevelOperations.ReadMemoryBank(tag, pageIndex);
            }
            permissionUser = tag.MemoryBanks[pageIndex].Data.GetUInt32(permissionUserOffset);
            if (memoryBanksWasNull)
            {
                tag.MemoryBanks = null;
            }

            return permissionUser;
        }

        private uint ReadPermissionService(Nfc2Tag tag)
        {
            uint permissionService = 0;

            int permissionServiceOffset = 24;
            int pageIndex = GetMemoryBankIndex(tag, 27);

            //in case of ot fit drivers, use PwmConfig page instead
            if (pageIndex < 0)
            {
                pageIndex = GetMemoryBankIndex(tag, 50);
                permissionServiceOffset = 12;
            }

            bool memoryBanksWasNull = false;
            if (tag.MemoryBanks == null)
            {
                memoryBanksWasNull = true;
                CreateDestMemoryBankList(tag, tag.TableOfContent.ContentTable.Count);
                LowLevelOperations lowLevelOperations = new LowLevelOperations(_reader);
                lowLevelOperations.ReadMemoryBank(tag, pageIndex);
            }
            permissionService = tag.MemoryBanks[pageIndex].Data.GetUInt32(permissionServiceOffset);
            if (memoryBanksWasNull)
            {
                tag.MemoryBanks = null;
            }

            return permissionService;
        }

        private Nfc2Tag ReadDeviceIdentification()
        {

            Nfc2Tag tag = new Nfc2Tag();
            LowLevelOperations lowLevelOperations = new LowLevelOperations(_reader);
            ProgressChange(this, 10);
            bool result = lowLevelOperations.ReadDeviceIdentificationRegisters(tag);
            if (!result)
            {
                tag.TableOfContent = new TableOfContent();
                throw new OperationFailedException("ReadDeviceIdentification failed.", ErrorCodes.ReadingNfcTagFailed);
            }
            ProgressChange(this, 15);
            result = lowLevelOperations.ReadTableOfContent(tag);
            if (!result)
            {
                throw new OperationFailedException("ReadDeviceIdentification failed.", ErrorCodes.ReadingNfcTagFailed);
            }
            ProgressChange(this, 30);
            if (tag.DeviceIdentificationRegisters.CrcIsCorrect && tag.TableOfContent.CrcIsCorrect)
                return tag;
            throw new OperationFailedException("ReadDeviceIdentification failed.", ErrorCodes.ReadingNfcTagFailed);
        }

        private bool IsPasteOperationPossible(Nfc2Tag sourceTag, Nfc2Tag destTag)
        {
            Logger.Trace("IsPasteOperationPossible started");
            if (destTag.DeviceIdentificationRegisters.Gtin != sourceTag.DeviceIdentificationRegisters.Gtin)
            {
                Logger.Error("IsPasteOperationPossible Gtin not matching");
                return false;
            }
            if (destTag.DeviceIdentificationRegisters.FwMajorVersion != sourceTag.DeviceIdentificationRegisters.FwMajorVersion)
            {
                Logger.Error("IsPasteOperationPossible FW version not matching");
                return false;
            }

            return true;
        }

        private void CopyMemoryBanks(Nfc2Tag sourceTag, Nfc2Tag destTag, byte[] oldMasterPassword, byte[] masterPassword = null, byte[] servicePassword = null, byte[] masterPasswordEncrypted = null, byte[] servicePasswordEncrypted = null)
        {
            Logger.Trace("CopyMemoryBanks started");
            List<int> memoryBankIndexList = GetMemoryBanksToCopy(sourceTag, destTag);
            CopyMemoryBanks(sourceTag, destTag, memoryBankIndexList, oldMasterPassword, masterPassword, servicePassword, masterPasswordEncrypted, servicePasswordEncrypted);
        }

        private List<int> GetMemoryBanksToCopy(Nfc2Tag sourceTag, Nfc2Tag destTag)
        {
            List<int> memoryBankIndexList = new List<int>(10);
            for (int i = 0; i < sourceTag.TableOfContent.ContentTable.Count; i++)
            {
                if (IsMemoryBankCopyPossible(sourceTag.TableOfContent.ContentTable[i], destTag.TableOfContent.ContentTable[i]))
                    memoryBankIndexList.Add(i);
            }
            return memoryBankIndexList;
        }

        private void CopyMemoryBanks(Nfc2Tag sourceTag, Nfc2Tag destTag, List<int> memoryBankIndexList, byte[] oldMasterPassword, byte[] masterPassword, byte[] servicePassword, byte[] masterPasswordEncrypted, byte[] servicePasswordEncrypted)
        {
            CreateDestMemoryBankList(destTag, sourceTag.MemoryBanks.Count);
            foreach (var mbIndex in memoryBankIndexList)
            {
                destTag.MemoryBanks[mbIndex] = sourceTag.MemoryBanks[mbIndex].Clone();
            }

            int pageIndex = RetrievePageIndex(destTag);

            if (masterPassword != null)
            {
                // In some architecture the bytes are read in LittleEndian
                if (BitConverter.IsLittleEndian)
                {
                    Array.Reverse(masterPasswordEncrypted, 0, 4);
                }

                // Set the Master Password, in case of ot fit drivers service password need to be encrypted
                destTag.MemoryBanks[pageIndex].Data[0] = pageIndex == 2 ? masterPasswordEncrypted[0] : oldMasterPassword[0];
                destTag.MemoryBanks[pageIndex].Data[1] = pageIndex == 2 ? masterPasswordEncrypted[1] : oldMasterPassword[1];
                destTag.MemoryBanks[pageIndex].Data[2] = pageIndex == 2 ? masterPasswordEncrypted[2] : oldMasterPassword[2];
                destTag.MemoryBanks[pageIndex].Data[3] = pageIndex == 2 ? masterPasswordEncrypted[3] : oldMasterPassword[3];

                //ot fit drivers doesn't have New Master Password and Master Password Confirmation
                if (pageIndex != 2)
                {
                    // Set the New Master Password
                    destTag.MemoryBanks[pageIndex].Data[4] = masterPassword[0];
                    destTag.MemoryBanks[pageIndex].Data[5] = masterPassword[1];
                    destTag.MemoryBanks[pageIndex].Data[6] = masterPassword[2];
                    destTag.MemoryBanks[pageIndex].Data[7] = masterPassword[3];

                    // Set the Master Password Confirmation
                    //destTag.MemoryBanks[0].Data[8] = 0xFF;
                    destTag.MemoryBanks[pageIndex].Data[8] = 0x00;
                }                
            }
            if (servicePassword != null)
            {
                int servicePasswordOffset = pageIndex == 2 ? 4 : 12;

                // In some architecture the bytes are read in LittleEndian
                if (BitConverter.IsLittleEndian)
                {
                    Array.Reverse(servicePasswordEncrypted, 0, 4);
                }

                // Set the Service Password, in case of ot fit drivers service password need to be encrypted
                destTag.MemoryBanks[pageIndex].Data[servicePasswordOffset] = pageIndex == 2 ? servicePasswordEncrypted[0] : servicePassword[0];
                destTag.MemoryBanks[pageIndex].Data[servicePasswordOffset + 1] = pageIndex == 2 ? servicePasswordEncrypted[1] : servicePassword[1];
                destTag.MemoryBanks[pageIndex].Data[servicePasswordOffset + 2] = pageIndex == 2 ? servicePasswordEncrypted[2] : servicePassword[2];
                destTag.MemoryBanks[pageIndex].Data[servicePasswordOffset + 3] = pageIndex == 2 ? servicePasswordEncrypted[3] : servicePassword[3];

                //ot fit drivers doesn't have Service Password Confirmation
                if (pageIndex != 2)
                {
                    // Set the Service Password Confirmation
                    //destTag.MemoryBanks[0].Data[16] = 0xFF;
                    destTag.MemoryBanks[0].Data[16] = 0x01;
                }                
            }

            ProgrammDevice(destTag, memoryBankIndexList, 30);
        }

        private int RetrievePageIndex(Nfc2Tag tag)
        {
            int pageIndex = GetMemoryBankIndex(tag, 27);
            if (pageIndex < 0)
            {
                //in case of ot fit drivers, use PwmConfig page instead
                if (pageIndex < 0)
                {
                    pageIndex = GetMemoryBankIndex(tag, 50);
                }
            }
            return pageIndex;
        }

        private void CreateDestMemoryBankList(Nfc2Tag destTag, int memoryBanksCount)
        {
            if (destTag.MemoryBanks == null)
            {
                destTag.MemoryBanks = new List<MemoryBank>(memoryBanksCount);
                for (int i = 0; i < memoryBanksCount; i++)
                    destTag.MemoryBanks.Add(null);
            }
        }

        private bool IsMemoryBankCopyPossible(TableOfContent.TocEntry sourceTocEntry, TableOfContent.TocEntry destTocEntry)
        {
            // exclude non copyable mememory pages
            if (_banksIdToExclude.Contains(sourceTocEntry.MpcId))
                return false;

            int sourceAttribute = sourceTocEntry.MbAttribut & 0x7;
            int destAttribute = destTocEntry.MbAttribut & 0x7;
            if ((sourceAttribute != 0) || (destAttribute != 0))
                return false;
            if (sourceTocEntry.MpcId != destTocEntry.MpcId)
                return false;
            if (sourceTocEntry.MbLength != destTocEntry.MbLength)
                return false;

            return true;
        }
    }
}
