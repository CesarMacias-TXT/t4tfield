using System;
using System.Collections.Generic;
using Osram.TFTBackend.BaseTypes;
using Osram.TFTBackend.ConfigurationService.DataModel;
using Osram.TFTBackend.DataModel.Feature.Helper;
using Osram.TFTBackend.Nfc2.Tag;
using System.Threading;
using System.Threading.Tasks;

namespace Osram.TFTBackend.ProgrammingService.Contracts
{
    /// <summary>
    /// Handles programming of an ECG device. It uses the Gtin to identify an ECG. 
    /// Reading and writing is done in device idependent way using meomory locations.
    /// </summary>
    public interface IProgrammingService
    {
        /// <summary>
        /// Reads the device and returns the EcgSelector for identification.
        /// </summary>
        /// <returns>EcgSelector of the device, in case of an error the EcgSelector is 0</returns>
        EcgSelector ReadDevice(CancellationToken cancellationToken);

        /// <summary>
        /// Extracts the values for the properties from the previous read device.
        /// Input is a list of InMemoryProperties with the memory locations and value size.
        /// Output is the same list with filled values.
        /// </summary>
        /// <param name="properties">List of memory locations</param>
        void GetPropertyValues(IEnumerable<EcgProperty> properties);

        /// <summary>
        /// Applies a list of memory changes to the device.
        /// </summary>
        /// <param name="properties">A list of InMemoryProperties with the memory locations and value values.</param>
        void ProgramConfiguration(IEnumerable<EcgProperty> properties, string type = "");

        /// <summary>
        /// Gets a raw copy of the current read device.
        /// </summary>
        /// <returns>Returns the tag data or null if data is invalid.</returns>
        Nfc2Tag CopyDevice();

        /// <summary>
        /// Copies the memory banks from the source tag to another device.
        /// </summary>
        /// <param name="sourceTag">A valid source tag</param>
        /// <returns>Returns true if the operation succeeded or false if the devices are incompatible</returns>
        bool PasteDevice(Nfc2Tag sourceTag);

        /// <summary>
        /// Check if the actual ecg is the same as the read.
        /// </summary>
        /// <param name="sourceTag">A valid source tag</param>
        /// <returns>Returns true if the operation succeeded or false if the devices are incompatible</returns>
        bool IsSameDevice();

        /// <summary>
        /// Check if the actual ecg is from the same family as the read.
        /// </summary>
        /// <param name="sourceTag">A valid source tag</param>
        /// <returns>Returns true if the operation succeeded or false if the devices are incompatible</returns>
        bool IsSameFamilyDevice(uint currentPassword);

        #region Events

        /// <summary>
        /// reading or writing progress
        /// </summary>
        event EventHandler<int> ProgressChange;

        #endregion
    }


}