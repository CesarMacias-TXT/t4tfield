﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Osram.TFTBackend.InitalizeDatabaseService.Contracts
{
    public interface IInitalizeDatabaseService
    {
        /// <summary>
        /// Initalizes the configuration database with configurations shipped with the app.
        /// </summary>
        Task Initialize();
    }
}
