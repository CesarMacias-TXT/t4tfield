﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using MvvmCross;
using Osram.TFTBackend.FileService.Contracts;
using Osram.TFTBackend.ImportService.Contracts;
using Osram.TFTBackend.InitalizeDatabaseService.Contracts;
using Osram.TFTBackend.PersistenceService;
using Osram.TFTBackend.RestService.Contracts;
using Osram.TFTBackend.Services.Contracts;
using Plugin.Settings.Abstractions;

namespace Osram.TFTBackend.InitalizeDatabaseService
{
    /// <summary>
    /// Is used to initialize the database, e.g. creating and populating the database with the configurations from the database.
    /// It will also handle updating of the shipped configurations or even migrating to a new database schema.
    /// It uses a database version stored in the database and an app version to decide what to do by comparing the two versions.
    /// </summary>
    public class InitalizeDatabaseService : IInitalizeDatabaseService
    {
        private const int AppVersion = 13;
        public const string IsOnlineServicesActive = "IsOnlineServicesActive";
        public const string LastUpdateOnlineServices = "LastUpdateOnlineServices";

        private enum DatabaseActions
        {
            Nothing,
            CreateNew,
            UpdateConfigurations,
            MigrateDatabase
        }

        private IImportService _importService;
        private IResourceService _resourceService;
        private IFileService _fileService;
        private IDatabaseVersionDataStore _databaseVersionDataStore;
        private IRestService _restService;
        private readonly ISettings _settings;

        public InitalizeDatabaseService(IImportService importService, IResourceService resourceService, IFileService fileService, IRestService restService, IDatabaseVersionDataStore databaseVersionDataStore, ISettings settings)
        {
            _importService = importService;
            _resourceService = resourceService;
            _fileService = fileService;
            _restService = restService;
            _databaseVersionDataStore = databaseVersionDataStore;
            _settings = settings;
        }

        public async Task Initialize()
        {
            Logger.TaggedTrace("InitalizeDatabaseService", "Initialize database");
            DatabaseActions action = await CheckDatabase();
            Logger.TaggedTrace("InitalizeDatabaseService", $"Database action = {action}");
            switch (action)
            {
                case DatabaseActions.CreateNew:
                    await CreateNewDatabase();
                    return;
                case DatabaseActions.UpdateConfigurations:
                    await CreateNewDatabase();
                    return;
                default:
                    await RestServiceCalls();
                    return;
            }
        }

        private async Task RestServiceCalls()
        {
            if (_settings.GetValueOrDefault(IsOnlineServicesActive, false))
            {
                await _restService.PostAsyncUserData();
                await _restService.ImportNewConfigurations();
                _settings.AddOrUpdateValue(LastUpdateOnlineServices, DateTime.Now.ToString());
            }
        }

        private async Task<DatabaseActions> CheckDatabase()
        {
            int databaseVersion = await _databaseVersionDataStore.GetVersion();
            Logger.TaggedTrace("InitalizeDatabaseService", $"Database version = {databaseVersion} App version = {AppVersion}");
            if (databaseVersion < 0)
                return DatabaseActions.CreateNew;
            if (AppVersion > databaseVersion)
                return DatabaseActions.UpdateConfigurations;
            return DatabaseActions.Nothing;
        }

        private async Task CreateNewDatabase()
        {
            Logger.TaggedTrace("InitalizeDatabaseService", "Create database");
            _fileService.DeleteAllConfigurationFiles(_databaseVersionDataStore.DatabaseDirectory);
            await Mvx.IoCProvider.Resolve<IConfigurationDataStore>().RemoveAllAsync();
            await _databaseVersionDataStore.SetVersion(AppVersion);
            //await ImportInitalConfigurations();

            await RestServiceCalls();
        }

        private async Task ImportInitalConfigurations()
        {
            IEnumerable<string> configurations = GetAllInitialConfigurations();
            foreach (var resourceName in configurations)
            {
                await ImportConfiguration(resourceName);
            }
        }

        private IEnumerable<string> GetAllInitialConfigurations()
        {
            var resources = _resourceService.GetManifestResourceNames();
            return resources.Where(resource => resource.Contains(".Resources.InitialConfigurations."));
        }

        private async Task ImportConfiguration(string resourceName)
        {
            Logger.TaggedTrace("InitalizeDatabaseService", $"Import configuration {resourceName}");
            Stream stream = _resourceService.GetManifestResourceStream(resourceName);
            await _importService.Import(ExtractName(resourceName), stream);
        }

        private string ExtractName(string resourceName)
        {
            string[] parts = resourceName.Split('.');
            if (parts.Length >= 2)
                return $"{parts[parts.Length-2]}.{parts[parts.Length - 1]}";
            return resourceName;
        }
    }
}
