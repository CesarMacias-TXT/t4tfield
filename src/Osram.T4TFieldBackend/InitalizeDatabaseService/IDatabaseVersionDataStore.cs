﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Osram.TFTBackend.DatabaseService.Contracts;
using SQLite;

namespace Osram.TFTBackend.InitalizeDatabaseService
{
    public class DatabaseVersion
    {
        [PrimaryKey]
        public int Id { get; set; }
        public int Version { get; set; }
    }

    /// <summary>
    /// Interface to get and set the database version.
    /// Used to check for updating / migration of the database on startup.
    /// </summary>
    public interface IDatabaseVersionDataStore : IDataStore<DatabaseVersion>
    {
        Task<int> GetVersion();
        Task SetVersion(int version);
    }
}
