﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Osram.TFTBackend.DatabaseService;
using Osram.TFTBackend.DatabaseService.Contracts;

namespace Osram.TFTBackend.InitalizeDatabaseService
{
    public class DatabaseVersionDataStore : DataStoreBase<DatabaseVersion>, IDatabaseVersionDataStore
    {
        private DatabaseVersion _data = new DatabaseVersion() {Id = 1};

        public DatabaseVersionDataStore(IConnectionProvider connectionProvider) : base(connectionProvider)
        {
        }

        public override async Task<bool> ExistsAsync(DatabaseVersion entity)
        {
            return (await LoadAsync(entity.Id)) != null;
        }

        public async Task<int> GetVersion()
        {
            try
            {
                DatabaseVersion data = await Connection.GetAsync<DatabaseVersion>(_data.Id);
                return data?.Version ?? -1;
            }
            catch (Exception e)
            {
                Logger.Exception("DatabaseVersionDataStore.GetVersion", e);
                return -1;
            }
        }

        public async Task SetVersion(int version)
        {
            try
            {
                _data.Version = version;
                await SaveOrUpdateAsync(_data);
            }
            catch (Exception e)
            {
                Logger.Exception("DatabaseVersionDataStore.SetVersion", e);
            }
        }
    }
}
