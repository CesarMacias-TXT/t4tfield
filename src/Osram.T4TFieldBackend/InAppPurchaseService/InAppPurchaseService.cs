﻿using Newtonsoft.Json;
using Osram.TFTBackend.InAppPurchaseService.Contracts;
using Osram.TFTBackend.InAppPurchaseService.Data;
using Plugin.InAppBilling;
using Plugin.InAppBilling.Abstractions;
using Plugin.Settings.Abstractions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Osram.TFTBackend.InAppPurchaseService
{
    public class InAppPurchaseService : IInAppPurchaseService
    {

        #region CONSTANTS

        private readonly ISettings _settings;
        private readonly IInAppBillingVerifyPurchase _verifyPurchase;

        private const string _payload = "OSRAM_T4TField";

        private const string _subscriptionsSaved = "SUB_SAVED";
        private const string _inAppSaved = "INAPP_SAVED";

        private readonly string[] _subscriptionIds = new string[] { "s_1_week", "s_1_weeks", "s_1_month", "s_1_months", "s_3_month", "s_3_months", "s_6_month", "s_6_months", "s_1_year", "s_1_years" };
        private readonly string[] _inAppIds = new string[] { "i_5_credits", "i_10_credits", "i_15_credits", "i_20_credits", "i_25_credits", "i_30_credits", "i_35_credits", "i_40_credits", "i_45_credits", "i_50_credits", "i_75_credits", "i_100_credits" };

        #endregion CONSTANTS

        #region VARIABLES

        private IEnumerable<BillingProduct> _availableSubscriptions;
        public IEnumerable<BillingProduct> GetAvailableSubscriptions() => _availableSubscriptions;

        private IEnumerable<BillingProduct> _availableInApps;
        public IEnumerable<BillingProduct> GetAvailableInApps() => _availableInApps;

        private List<BillingPurchase> _allPurchasedSubscriptions;
        public IEnumerable<BillingPurchase> GetActivePurchasedSubscriptions()
        {
            List<BillingPurchase> purchased = new List<BillingPurchase>();

            foreach (var p in _allPurchasedSubscriptions)
            {
                if (p.InAppBillingPurchase.State != PurchaseState.Purchased
                    && p.InAppBillingPurchase.State != PurchaseState.Restored
                    && p.InAppBillingPurchase.State != PurchaseState.PaymentPending
                    && p.InAppBillingPurchase.State != PurchaseState.FreeTrial)
                {
                    continue;
                }

                if (p.InAppBillingPurchase.ConsumptionState == ConsumptionState.Consumed)
                {
                    continue;
                }

                purchased.Add(p);
            }
            return purchased;
        }
        private List<BillingPurchase> _allPurchasedInApps;
        public IEnumerable<BillingPurchase> GetActivePurchasedInApps()
        {
            List<BillingPurchase> purchased = new List<BillingPurchase>();

            foreach (var p in _allPurchasedInApps)
            {
                if (p.InAppBillingPurchase.State != PurchaseState.Purchased
                    && p.InAppBillingPurchase.State != PurchaseState.Restored
                    && p.InAppBillingPurchase.State != PurchaseState.PaymentPending
                    && p.InAppBillingPurchase.State != PurchaseState.FreeTrial)
                {
                    continue;
                }

                if (p.InAppBillingPurchase.ConsumptionState == ConsumptionState.Consumed)
                {
                    continue;
                }

                purchased.Add(p);
            }

            return purchased;
        }
        #endregion VARIABLES

        public InAppPurchaseService(ISettings settings, IInAppBillingVerifyPurchase verifyPurchase)
        {
            _verifyPurchase = verifyPurchase;
            _settings = settings;

            // Get the saved subscriptions from JSON Settings
            RestorePurchaseFromLocal(ItemType.Subscription);
            RestorePurchaseFromLocal(ItemType.InAppPurchase);
        }

        public async Task RefreshProductsAsync()
        {
            _availableSubscriptions = await GetProductsAsync(ItemType.Subscription, _subscriptionIds);
            _availableInApps = await GetProductsAsync(ItemType.InAppPurchase, _inAppIds);
        }

        public async Task RestorePurchasesAsync()
        {
            _allPurchasedSubscriptions = (await GetPurchasesAsync(ItemType.Subscription)).ToList();
            WritePurchaseToLocal(ItemType.Subscription);

            var onlinePurchases = (await GetPurchasesAsync(ItemType.InAppPurchase)).ToList();
            await ManageLocalPurchasesAsync(onlinePurchases);
            WritePurchaseToLocal(ItemType.InAppPurchase);
        }

        public async Task<bool> MakePurchaseAsync(string productId, ItemType itemType)
        {
            if (!CrossInAppBilling.IsSupported)
            {
                Logger.Trace($"InAppPurchaseService.MakePurchaseAsync CrossInAppBilling Is NOT Supported");
                return false;
            }

            var billing = CrossInAppBilling.Current;
            try
            {
                var connected = await billing.ConnectAsync(itemType);
                Logger.Trace($"InAppPurchaseService.MakePurchaseAsync ConnectAsync={connected}");

                if (!connected)
                {
                    //we are offline or can't connect
                    throw new InAppBillingPurchaseException(PurchaseError.ServiceUnavailable);
                }

                //check purchases
                var purchase = await billing.PurchaseAsync(productId, itemType, _payload, _verifyPurchase);
                Logger.Trace($"InAppPurchaseService.MakePurchaseAsync purchase={purchase}");


                //possibility that a null came through.
                if (purchase == null)
                {
                    if (itemType == ItemType.InAppPurchase)
                    {
                        var onlinePurchases = (await GetPurchasesAsync(ItemType.InAppPurchase)).ToList();
                        await ManageLocalPurchasesAsync(onlinePurchases);

                        WritePurchaseToLocal(itemType);
                    }
                    else if (itemType == ItemType.Subscription)
                    {
                        _allPurchasedSubscriptions = (await GetPurchasesAsync(ItemType.Subscription)).ToList();
                        WritePurchaseToLocal(ItemType.Subscription);

                    }

                    return true;
                }
                else
                {
                    //purchased!
                    if (itemType == ItemType.Subscription)
                    {
                        _allPurchasedSubscriptions.Add(new BillingPurchase()
                        {
                            InAppBillingPurchase = purchase,
                            ItemType = itemType,
                        });
                    }
                    else if (itemType == ItemType.InAppPurchase)
                    {
                        _allPurchasedInApps.Add(new BillingPurchase()
                        {
                            InAppBillingPurchase = purchase,
                            ItemType = itemType,
                            Credits = GetCredits(purchase.ProductId),
                            AvailableCredits = GetCredits(purchase.ProductId),
                        });
                    }

                    WritePurchaseToLocal(itemType);

                    return true;
                }
            }
            catch (InAppBillingPurchaseException purchaseEx)
            {
                Logger.TaggedException("InAppPurchaseService.MakePurchaseAsync", "Error during Purchase", purchaseEx);

                throw purchaseEx;
            }
            catch (Exception ex)
            {
                Logger.TaggedException("InAppPurchaseService.MakePurchaseAsync", "Generic Error during Purchase", ex);

                throw ex;
            }
            finally
            {
                await billing.DisconnectAsync();
            }
        }

        private int GetCredits(string productId)
        {
            // i_(x)_credits
            var tokens = productId.Split('_');
            return int.Parse(tokens[1]);
        }

        public async Task<IEnumerable<BillingProduct>> GetProductsAsync(ItemType itemType, params string[] productIds)
        {
            var billing = CrossInAppBilling.Current;

            try
            {
                var connected = await billing.ConnectAsync(itemType);
                Logger.Trace($"InAppPurchaseService.GetProductsAsync ConnectAsync={connected}");

                if (!connected)
                {
                    throw new InAppBillingPurchaseException(PurchaseError.ServiceUnavailable);
                }

                var products = await billing.GetProductInfoAsync(itemType, productIds);
                if (products == null)
                {
                    products = new List<InAppBillingProduct>();
                }

                if (itemType == ItemType.InAppPurchase)
                {
                    products = products.OrderBy(p => Array.IndexOf(_inAppIds, p.ProductId));
                }
                else if (itemType == ItemType.Subscription)
                {
                    products = products.OrderBy(p => Array.IndexOf(_subscriptionIds, p.ProductId));
                }


                return products.Select(p => new BillingProduct() { InAppBillingProduct = p, ItemType = itemType });
            }
            catch (InAppBillingPurchaseException ex)
            {
                Logger.TaggedException("InAppPurchaseService.GetProductsAsync", "Error during Get Products", ex);

                throw ex;
            }
            catch (Exception ex)
            {
                Logger.TaggedException("InAppPurchaseService.GetProductsAsync", "GEneric Error during Get Products", ex);

                throw ex;
            }
            finally
            {
                await billing.DisconnectAsync();
            }
        }

        public async Task<IEnumerable<BillingPurchase>> GetPurchasesAsync(ItemType itemType)
        {
            var billing = CrossInAppBilling.Current;

            try
            {
                var connected = await billing.ConnectAsync(itemType);
                Logger.Trace($"InAppPurchaseService.GetPurchasesAsync ConnectAsync={connected}");

                if (!connected)
                {
                    throw new InAppBillingPurchaseException(PurchaseError.ServiceUnavailable);
                }

                var purchases = await billing.GetPurchasesAsync(itemType);
                if (purchases == null)
                {
                    purchases = new List<InAppBillingPurchase>();
                }

                if (itemType == ItemType.Subscription)
                {
                    return purchases.Select(p => new BillingPurchase()
                    {
                        InAppBillingPurchase = p,
                        ItemType = itemType,
                    });
                }
                else if (itemType == ItemType.InAppPurchase)
                {
                    return purchases.Select(p => new BillingPurchase()
                    {
                        InAppBillingPurchase = p,
                        ItemType = itemType,
                        Credits = GetCredits(p.ProductId),
                        AvailableCredits = GetCredits(p.ProductId),
                    });
                }
                else
                {
                    return purchases.Select(p => new BillingPurchase());
                }
            }
            catch (InAppBillingPurchaseException ex)
            {
                Logger.TaggedException("InAppPurchaseService.GetPurchasesAsync", "Error during Get Purchases", ex);

                throw ex;
            }
            catch (Exception ex)
            {
                Logger.TaggedException("InAppPurchaseService.GetPurchasesAsync", "Generic Error during Get Purchases", ex);

                throw ex;
            }
            finally
            {
                await billing.DisconnectAsync();
            }
        }

        public async Task<BillingPurchase> ConsumePurchaseAsync()
        {
            // In case of subscriptions all programmings are inside this premium package
            if (GetActivePurchasedSubscriptions().Count() > 0)
            {
                return await Task.FromResult<BillingPurchase>(null);
            }
            // In case of in app we have to remove one credit for each use
            else if (GetActivePurchasedInApps().Count() > 0)
            {
                // User can buy different packages at same time
                foreach (var purchaseInApp in GetActivePurchasedInApps())
                {
                    if (purchaseInApp.InAppBillingPurchase.ConsumptionState == ConsumptionState.NoYetConsumed)
                    {
                        if (purchaseInApp.AvailableCredits > 0)
                        {
                            purchaseInApp.AvailableCredits--;
                            WritePurchaseToLocal(ItemType.InAppPurchase);

                            if (purchaseInApp.AvailableCredits <= 0)
                            {
                                try
                                {
                                    var consumedPurchase = await MarkAsConsumedAsync(purchaseInApp.InAppBillingPurchase);

                                    if (consumedPurchase != null)
                                    {
                                        purchaseInApp.InAppBillingPurchase.ConsumptionState = ConsumptionState.Consumed;
                                        WritePurchaseToLocal(ItemType.InAppPurchase);
                                    }
                                }
                                catch (Exception ex)
                                {
                                    Logger.TaggedException("InAppPurchaseService.ConsumePurchaseAsync", "Error during ConsumePurchaseAsync", ex);
                                }
                            }

                            return await Task.FromResult<BillingPurchase>(null);
                        }
                    }
                }
            }

            return await Task.FromResult<BillingPurchase>(null);
        }

        public async Task<bool> CanConsumePurchaseAsync()
        {
            // In case of subscriptions all programmings are allowed
            if (GetActivePurchasedSubscriptions().Count() > 0)
            {
                return await Task.FromResult(true);
            }
            // In case of in apps we need to check if the token are available
            else if (GetActivePurchasedInApps().Count() > 0)
            {
                foreach (var purchaseInApp in GetActivePurchasedInApps())
                {
                    if (purchaseInApp.InAppBillingPurchase.ConsumptionState == ConsumptionState.NoYetConsumed && purchaseInApp.AvailableCredits <= 0)
                    {
                        try
                        {
                            var consumedPurchase = await MarkAsConsumedAsync(purchaseInApp.InAppBillingPurchase);

                            if (consumedPurchase != null)
                            {
                                purchaseInApp.InAppBillingPurchase.ConsumptionState = ConsumptionState.Consumed;
                                WritePurchaseToLocal(ItemType.InAppPurchase);
                            }
                        }
                        catch (Exception ex)
                        {
                            Logger.TaggedException("InAppPurchaseService.ConsumePurchaseAsync", "Error during ConsumePurchaseAsync", ex);
                        }
                    }

                    if (purchaseInApp.InAppBillingPurchase.ConsumptionState == ConsumptionState.NoYetConsumed && purchaseInApp.AvailableCredits > 0)
                    {
                        return await Task.FromResult(true);
                    }
                }
            }

            return await Task.FromResult(false);
        }

        private async Task ManageLocalPurchasesAsync(List<BillingPurchase> onlinePurchases)
        {
            // Check if something online is not in local list
            foreach (var onlinePurchase in onlinePurchases)
            {
                var found = false;

                foreach (var localPurchase in GetActivePurchasedInApps())
                {
                    if (onlinePurchase.InAppBillingPurchase.Id == localPurchase.InAppBillingPurchase.Id)
                    {
                        found = true;

                        // Mark as consumed if  not for the moment
                        if (localPurchase.InAppBillingPurchase.ConsumptionState == ConsumptionState.NoYetConsumed && localPurchase.AvailableCredits <= 0)
                        {
                            try
                            {
                                var consumedPurchase = await MarkAsConsumedAsync(localPurchase.InAppBillingPurchase);

                                if (consumedPurchase != null)
                                {
                                    localPurchase.InAppBillingPurchase.ConsumptionState = ConsumptionState.Consumed;
                                    WritePurchaseToLocal(ItemType.InAppPurchase);
                                }
                            }
                            catch (Exception ex)
                            {
                                Logger.TaggedException("InAppPurchaseService.ManageLocalPurchasesAsync", "Error during ConsumePurchaseAsync", ex);
                            }
                        }

                        break;
                    }
                }

                // The local list is not updated, add the online one
                if (!found)
                {
                    _allPurchasedInApps.Add(onlinePurchase);
                }
            }

            foreach (var localPurchase in GetActivePurchasedInApps())
            {
                var found = false;

                foreach (var onlinePurchase in onlinePurchases)
                {
                    if (localPurchase.InAppBillingPurchase.Id == onlinePurchase.InAppBillingPurchase.Id)
                    {
                        found = true;
                        break;
                    }
                }

                // The local list is not updated
                if (!found)
                {
                    if (Plugin.DeviceInfo.CrossDeviceInfo.Current.Platform == Plugin.DeviceInfo.Abstractions.Platform.Android)
                    {

                        _allPurchasedInApps.Remove(localPurchase);
                    }
                }
            }
        }

        private async Task<InAppBillingPurchase> MarkAsConsumedAsync(InAppBillingPurchase inAppBillingPurchase)
        {
            var billing = CrossInAppBilling.Current;

            try
            {
                var connected = await billing.ConnectAsync(ItemType.InAppPurchase);

                if (!connected)
                {
                    throw new InAppBillingPurchaseException(PurchaseError.ServiceUnavailable);
                }

                return await billing.ConsumePurchaseAsync(inAppBillingPurchase.ProductId, inAppBillingPurchase.PurchaseToken);

            }
            catch (InAppBillingPurchaseException purchaseEx)
            {
                Logger.TaggedException("InAppPurchaseService.MarkAsConsumedAsync", "Error during MarkAsConsumedAsync", purchaseEx);
                throw purchaseEx;
            }
            catch (Exception ex)
            {
                Logger.TaggedException("InAppPurchaseService.MarkAsConsumedAsync", "Error during MarkAsConsumedAsync", ex);
                throw ex;
            }
            finally
            {
                await billing.DisconnectAsync();
            }
        }

        #region LOCAL PURCHASE

        private void RestorePurchaseFromLocal(ItemType itemType)
        {
            if (itemType == ItemType.Subscription)
            {
                var allPurchasedSubscriptionsString = _settings.GetValueOrDefault(_subscriptionsSaved, null);
                if (!string.IsNullOrEmpty(allPurchasedSubscriptionsString))
                {
                    _allPurchasedSubscriptions = JsonConvert.DeserializeObject<List<BillingPurchase>>(allPurchasedSubscriptionsString);
                }
                else
                {
                    _allPurchasedSubscriptions = new List<BillingPurchase>();
                }
            }
            else if (itemType == ItemType.InAppPurchase)
            {
                var allPurchasedInAppsString = _settings.GetValueOrDefault(_inAppSaved, null);
                if (!string.IsNullOrEmpty(allPurchasedInAppsString))
                {
                    _allPurchasedInApps = JsonConvert.DeserializeObject<List<BillingPurchase>>(allPurchasedInAppsString);
                }
                else
                {
                    _allPurchasedInApps = new List<BillingPurchase>();
                }
            }
        }

        private void WritePurchaseToLocal(ItemType itemType)
        {
            if (itemType == ItemType.Subscription)
            {
                _settings.AddOrUpdateValue(_subscriptionsSaved, JsonConvert.SerializeObject(_allPurchasedSubscriptions));
            }
            else if (itemType == ItemType.InAppPurchase)
            {
                _settings.AddOrUpdateValue(_inAppSaved, JsonConvert.SerializeObject(_allPurchasedInApps));
            }
        }

        #endregion LOCAL PURCHASE
    }
}
