﻿using Plugin.InAppBilling.Abstractions;

namespace Osram.TFTBackend.InAppPurchaseService.Contracts
{
    public interface IInAppVerifyPurchaseService : IInAppBillingVerifyPurchase
    {

    }
}
