﻿using Osram.TFTBackend.InAppPurchaseService.Data;
using Plugin.InAppBilling.Abstractions;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Osram.TFTBackend.InAppPurchaseService.Contracts
{
    public interface IInAppPurchaseService
    {
        IEnumerable<BillingProduct> GetAvailableSubscriptions();
        IEnumerable<BillingProduct> GetAvailableInApps();

        IEnumerable<BillingPurchase> GetActivePurchasedSubscriptions();
        IEnumerable<BillingPurchase> GetActivePurchasedInApps();


        /// <summary>
        /// Get all products information for a specifiy product type.
        /// </summary>
        /// <param name="itemType">Type of product offering</param>
        /// <returns>List of products</returns>
        Task<IEnumerable<BillingProduct>> GetProductsAsync(ItemType itemType, params string[] productIds);

        /// <summary>
        /// Get all purchased product for a specifiy product type.
        /// </summary>
        /// <param name="itemType">Type of product</param>
        /// <returns>The current purchases</returns>
        Task<IEnumerable<BillingPurchase>> GetPurchasesAsync(ItemType itemType);

        /// <summary>
        /// Purchase a specific product or subscription
        /// </summary>
        /// <param name="productId">Sku or ID of product</param>
        /// <param name="itemType">Type of product being requested</param>
        /// <returns>Purchase details</returns>
        Task<bool> MakePurchaseAsync(string productId, ItemType itemType);

        /// <summary>
        /// Consume a purchase
        /// </summary>
        /// <returns>If consumed successful</returns>
        Task<BillingPurchase> ConsumePurchaseAsync();

        /// <summary>
        /// Check if a user can use the service
        /// </summary>
        /// <returns>true if the user has a correct premium package</returns>
        Task<bool> CanConsumePurchaseAsync();

        /// <summary>
        /// Refresh products information 
        /// </summary>
        Task RefreshProductsAsync();

        /// <summary>
        /// Restore the Purchases from the store
        /// </summary>
        Task RestorePurchasesAsync();
    }
}
