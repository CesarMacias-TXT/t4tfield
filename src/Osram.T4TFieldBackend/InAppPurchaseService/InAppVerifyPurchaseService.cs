﻿using Osram.TFTBackend.InAppPurchaseService.Contracts;
using System.Threading.Tasks;

namespace Osram.TFTBackend.InAppPurchaseService
{
    public partial class InAppVerifyPurchaseService : IInAppVerifyPurchaseService
    {
        const string key1 = @"XOR_key1";
        const string key2 = @"XOR_key2";
        const string key3 = @"XOR_key3";

        public Task<bool> VerifyPurchase(string signedData, string signature, string productId = null, string transactionId = null)
        {

#if __ANDROID__
            //var key1Transform = Plugin.InAppBilling.InAppBillingImplementation.InAppBillingSecurity.TransformString(key1, 1);
            //var key2Transform = Plugin.InAppBilling.InAppBillingImplementation.InAppBillingSecurity.TransformString(key2, 2);
            //var key3Transform = Plugin.InAppBilling.InAppBillingImplementation.InAppBillingSecurity.TransformString(key3, 3);

            //return Task.FromResult(Plugin.InAppBilling.InAppBillingImplementation.InAppBillingSecurity.VerifyPurchase(key1Transform + key2Transform + key3Transform, signedData, signature));
            return Task.FromResult(true);
#else
            return Task.FromResult(true);
#endif
        }
    }
}
