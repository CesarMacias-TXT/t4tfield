﻿using Plugin.InAppBilling.Abstractions;

namespace Osram.TFTBackend.InAppPurchaseService.Data
{
    public class BillingProduct
    {
        public InAppBillingProduct InAppBillingProduct { get; set; }
        public ItemType ItemType { get; set; }
    }
}
