﻿using Plugin.InAppBilling.Abstractions;
using System;

namespace Osram.TFTBackend.InAppPurchaseService.Data
{
    public partial class BillingPurchase
    {
        public InAppBillingPurchase InAppBillingPurchase { get; set; }
        public ItemType ItemType { get; set; }
    }

    #region CONSUMABLE

    public partial class BillingPurchase
    {

        public int Credits { get; set; }
        public int AvailableCredits { get; set; }
    }

    #endregion CONSUMABLE

    #region SUBSCRIPTION
    public partial class BillingPurchase
    {

        //
        //  Number of Period Depending by Unit
        //
        private int? subscriptionQuantity;
        public int SubscriptionQuantity {
            get {
                if (subscriptionQuantity == null)
                {
                    // s_x_(days/weeks/months/years)
                    var tokens = InAppBillingPurchase.ProductId.Split('_');
                    subscriptionQuantity = int.Parse(tokens[1]);
                }
                return (int)subscriptionQuantity;
            }
            set {
                subscriptionQuantity = value;
            }
        }

        //
        //  Unit of time like days, weeks, months, yers
        //
        private InAppSubscriptionUnitEnum subscriptionUnit;
        public InAppSubscriptionUnitEnum SubscriptionUnit {
            get {
                if (subscriptionUnit == InAppSubscriptionUnitEnum.NONE)
                {
                    // s_x_(days/weeks/months/years)
                    var tokens = InAppBillingPurchase.ProductId.Split('_');
                    if (tokens[2] == "day" || tokens[2] == "days")
                    {
                        subscriptionUnit = InAppSubscriptionUnitEnum.DAYS;
                    }
                    else if (tokens[2] == "week" || tokens[2] == "weeks")
                    {
                        subscriptionUnit = InAppSubscriptionUnitEnum.WEEKS;
                    }
                    else if (tokens[2] == "month" || tokens[2] == "months")
                    {
                        subscriptionUnit = InAppSubscriptionUnitEnum.MONTHS;
                    }
                    else if (tokens[2] == "year" || tokens[2] == "years")
                    {
                        subscriptionUnit = InAppSubscriptionUnitEnum.YEARS;
                    }
                }
                return subscriptionUnit;
            }
            set {
                subscriptionUnit = value;
            }
        }

        public DateTime GetEstimatedExpirationDate()
        {

            DateTime end = InAppBillingPurchase.TransactionDateUtc.AddDays(0d);

            switch (SubscriptionUnit)
            {
                case InAppSubscriptionUnitEnum.DAYS:
                    end = InAppBillingPurchase.TransactionDateUtc.AddDays(SubscriptionQuantity);
                    break;
                case InAppSubscriptionUnitEnum.WEEKS:
                    end = InAppBillingPurchase.TransactionDateUtc.AddDays(SubscriptionQuantity * 7);
                    break;
                case InAppSubscriptionUnitEnum.MONTHS:
                    end = InAppBillingPurchase.TransactionDateUtc.AddMonths(SubscriptionQuantity);
                    break;
                case InAppSubscriptionUnitEnum.YEARS:
                    end = InAppBillingPurchase.TransactionDateUtc.AddYears(SubscriptionQuantity);
                    break;
            }

            return end;
        }
    }

    public enum InAppSubscriptionUnitEnum
    {
        NONE,
        DAYS,
        WEEKS,
        MONTHS,
        YEARS
    }

    #endregion SUBSCRIPTION
}
