﻿using Osram.TFTBackend.InAppPurchaseService.Contracts;
using Osram.TFTBackend.InAppPurchaseService.Data;
using Plugin.InAppBilling.Abstractions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Osram.TFTBackend.InAppPurchaseService.Fake
{
    public class FakeInAppPurchaseService : IInAppPurchaseService
    {
        private const string _payload = "OSRAM_T4TField";
        private readonly IInAppBillingVerifyPurchase _verifyPurchase;


        private IEnumerable<BillingProduct> availableSubscriptions;
        public IEnumerable<BillingProduct> GetAvailableSubscriptions() => availableSubscriptions;

        private IEnumerable<BillingProduct> availableInApps;
        public IEnumerable<BillingProduct> GetAvailableInApps() => availableInApps;

        private IEnumerable<BillingPurchase> allPurchasedSubscriptions;
        private IEnumerable<BillingPurchase> allPurchasedInApps;

        public IEnumerable<BillingPurchase> GetActivePurchasedSubscriptions()
        {
            List<BillingPurchase> purchased = new List<BillingPurchase>();

            foreach (var p in allPurchasedSubscriptions)
            {
                if (p.InAppBillingPurchase.State != PurchaseState.Purchased && p.InAppBillingPurchase.State != PurchaseState.Restored)
                {
                    continue;
                }

                if (p.InAppBillingPurchase.ConsumptionState == ConsumptionState.Consumed)
                {
                    continue;
                }

                var tokens = p.InAppBillingPurchase.ProductId.Split('_'); // s_x_(days/weeks/months/years)

                DateTime expire = p.InAppBillingPurchase.TransactionDateUtc.AddDays(0d);
                if (tokens[2] == "days")
                {
                    expire = p.InAppBillingPurchase.TransactionDateUtc.AddDays(double.Parse(tokens[1]));
                }
                else if (tokens[2] == "weeks")
                {
                    expire = p.InAppBillingPurchase.TransactionDateUtc.AddDays(double.Parse(tokens[1]) * 7);
                }
                else if (tokens[2] == "months")
                {
                    expire = p.InAppBillingPurchase.TransactionDateUtc.AddMonths(int.Parse(tokens[1]));
                }
                else if (tokens[2] == "years")
                {
                    expire = p.InAppBillingPurchase.TransactionDateUtc.AddYears(int.Parse(tokens[1]));
                }

                if (expire < DateTime.UtcNow)
                {
                    continue;
                }

                purchased.Add(p);
            }

            return purchased;
        }
        public IEnumerable<BillingPurchase> GetActivePurchasedInApps()
        {
            List<BillingPurchase> purchased = new List<BillingPurchase>();

            foreach (var p in allPurchasedSubscriptions)
            {
                if (p.InAppBillingPurchase.State != PurchaseState.Purchased && p.InAppBillingPurchase.State != PurchaseState.Restored)
                {
                    continue;
                }

                if (p.InAppBillingPurchase.ConsumptionState == ConsumptionState.Consumed)
                {
                    continue;
                }

                purchased.Add(p);
            }

            return purchased;
        }

        public FakeInAppPurchaseService(IInAppBillingVerifyPurchase verifyPurchase)
        {
            _verifyPurchase = verifyPurchase;

            RestorePurchasesAsync().Wait();
        }

        public async Task RefreshProductsAsync()
        {
            availableSubscriptions = await GetProductsAsync(ItemType.Subscription);
            availableInApps = await GetProductsAsync(ItemType.InAppPurchase);
        }

        public async Task RestorePurchasesAsync()
        {
            allPurchasedSubscriptions = await GetPurchasesAsync(ItemType.Subscription);
            allPurchasedInApps = await GetPurchasesAsync(ItemType.InAppPurchase);
        }

        public async Task<bool> MakePurchaseAsync(string productId, ItemType itemType)
        {
            return await Task.FromResult(true);
        }


        public async Task<IEnumerable<BillingProduct>> GetProductsAsync(ItemType itemType, params string[] productIds)
        {
            List<BillingProduct> products = new List<BillingProduct>();

            if (itemType == ItemType.Subscription)
            {
                products.Add(
                    new BillingProduct()
                    {
                        InAppBillingProduct = new InAppBillingProduct()
                        {
                            ProductId = "s_1_weeks",
                            Name = "Unlimited Programmings",
                            Description = "Weekly Subscription",
                            LocalizedPrice = "14,99 €",
                            CurrencyCode = "EUR",
                        },
                        ItemType = itemType,
                    }
                );
                products.Add(
                    new BillingProduct()
                    {
                        InAppBillingProduct = new InAppBillingProduct()
                        {
                            ProductId = "s_1_months",
                            Name = "Unlimited Programmings",
                            Description = "Monthly Subscription",
                            LocalizedPrice = "24,99 €",
                            CurrencyCode = "EUR",
                        },
                        ItemType = itemType,
                    }
                );
            }
            else
            {
                products.Add(
                    new BillingProduct()
                    {
                        InAppBillingProduct = new InAppBillingProduct()
                        {
                            ProductId = "i_10_credits",
                            Name = "10 Programmings",
                            Description = "One time payment",
                            LocalizedPrice = "4,99 €",
                            CurrencyCode = "EUR",
                        },
                        ItemType = itemType,
                    }
                );
            }


            return await Task.FromResult(products);
        }

        public async Task<IEnumerable<BillingPurchase>> GetPurchasesAsync(ItemType itemType)
        {
            List<BillingPurchase> products = new List<BillingPurchase>();

            if (itemType == ItemType.Subscription)
            {
                products.Add(
                    new BillingPurchase()
                    {
                        InAppBillingPurchase = new InAppBillingPurchase()
                        {
                            Id = "1",
                            ProductId = "s_1_weeks",
                            TransactionDateUtc = DateTime.Now.AddDays(-100),
                            AutoRenewing = true,
                            PurchaseToken = "1",
                            State = PurchaseState.Purchased,
                            ConsumptionState = ConsumptionState.Consumed,
                        },
                        ItemType = itemType,
                    }
                );
                products.Add(
                    new BillingPurchase()
                    {
                        InAppBillingPurchase = new InAppBillingPurchase()
                        {
                            Id = "2",
                            ProductId = "s_1_months",
                            TransactionDateUtc = DateTime.Now.AddDays(-20),
                            AutoRenewing = true,
                            PurchaseToken = "2",
                            State = PurchaseState.Purchased,
                            ConsumptionState = ConsumptionState.NoYetConsumed,
                        },
                        ItemType = itemType,
                    }
                );
            }
            else
            {

            }


            return await Task.FromResult(products);
        }

        public async Task<BillingPurchase> ConsumePurchaseAsync()
        {
            // In case of subscriptions all programmings are inside this premium package
            if (GetActivePurchasedSubscriptions().Count() > 0)
            {
                return await Task.FromResult<BillingPurchase>(null);
            }
            // In case of subscriptions all programmings are inside this premium package
            else if (GetActivePurchasedInApps().Count() > 0)
            {

                return await Task.FromResult<BillingPurchase>(null);
            }

            return await Task.FromResult<BillingPurchase>(null);
        }

        public async Task<bool> CanConsumePurchaseAsync()
        {
            // In case of subscriptions all programmings are allowed
            if (GetActivePurchasedSubscriptions().Count() > 0)
            {
                return await Task.FromResult(true);
            }
            // In case of in apps we need to check if thetoken are available
            else if (GetActivePurchasedInApps().Count() > 0)
            {
                foreach (var purchase in GetActivePurchasedInApps())
                {
                    if (purchase.InAppBillingPurchase.ConsumptionState == ConsumptionState.NoYetConsumed)
                    {
                        return await Task.FromResult(true);
                    }
                }
            }

            return await Task.FromResult(false);
        }
    }
}
