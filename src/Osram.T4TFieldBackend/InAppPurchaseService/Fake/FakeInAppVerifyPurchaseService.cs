﻿using Osram.TFTBackend.InAppPurchaseService.Contracts;
using System.Threading.Tasks;

namespace Osram.TFTBackend.InAppPurchaseService.Fake
{
    public partial class FakeInAppVerifyPurchaseService : IInAppVerifyPurchaseService
    {

        public Task<bool> VerifyPurchase(string signedData, string signature, string productId = null, string transactionId = null)
        {
            return Task.FromResult(true);
        }
    }
}
