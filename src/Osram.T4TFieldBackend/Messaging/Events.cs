﻿using MvvmCross.Plugin.Messenger;
using Osram.TFTBackend.ConfigurationService.Contracts;
using Osram.TFTBackend.NfcService.NfcDataTypes;

namespace Osram.TFTBackend.Messaging
{
    public class Events
    {     
        public class OnNfcReaderChangedEvent : MvxMessage
        {
            public NfcReaderType ReaderTypeSelected;

            public OnNfcReaderChangedEvent(object sender, NfcReaderType type) : base(sender)
            {
                ReaderTypeSelected = type;
            }
        }

        public class OnNfcReaderUpdateEvent : MvxMessage
        {
            public OnNfcReaderUpdateEvent(object sender) : base(sender)
            {
            }
        }

        public class OnConfigurationPropertiesReadEvent : MvxMessage
        {
            public OnConfigurationPropertiesReadEvent(object sender) : base(sender)
            {
            }
        }

        public class ConfigurationChangedEvent : MvxMessage
        {
            public IConfiguration Configuration;

            public ConfigurationChangedEvent(object sender, IConfiguration conf) : base(sender)
            {
                Configuration = conf;
            }
        }

        public class ConfigurationResetEvent : MvxMessage
        {
            public bool ConfigurationResetNeeded;

            public ConfigurationResetEvent(object sender, bool ResetNeeded) : base(sender)
            {
                ConfigurationResetNeeded = ResetNeeded;
            }
        }

        public class AstroDimFeatureChangedEvent : MvxMessage
        {
            public AstroDimFeatureChangedEvent(object sender) : base(sender)
            {
            }
        }

        public class OsrtupImportFinished : MvxMessage
        {
            public OsrtupImportFinished(object sender) : base(sender) { }
        }
    }
}
