﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace Osram.TFTBackend.LocationService
{

    public class AstroBasedUtils
    {

        public const string StrTimePM = "PM";
        public const string StrTimeAM = "AM";
        private static List<DateTime> dateListOfYear;

        /// <summary>
        /// Method used to calculate the sun rise based on the location details.
        /// Location details shall include the latitude and longitude of the location.
        /// </summary>
        /// <param name="today">Date time for the current day.</param>
        /// <param name="timeOffset">Offset value of the selected location.</param>
        /// <param name="position">Geographical position represents the selected location latitude and longitude value.</param>
        /// <returns>Returns the sun rise and sun set date time for the selected location.</returns>
        public static SunDawn CalcSunrise(DateTime today, double timeOffset, GeographicalPosition position)
        {
            return new SunDawn
            {
                Rise = SunRise(today.Year, today.Month, today.Day, position.Latitude, position.Longitude, timeOffset),
                Set = SunSet(today.Year, today.Month, today.Day, position.Latitude, position.Longitude, timeOffset)
            };
        }

        /// <summary>
        /// Method used to get the date time of the specified year, month, day.
        /// </summary>
        /// <param name="year">Selected year</param>
        /// <param name="month">Selected month</param>
        /// <param name="day">Selected day</param>
        /// <returns>Returns the date and time in the specified year.</returns>
        private static int DayOfYear(int year, int month, int day)
        {
            return new DateTime(year, month, day).DayOfYear;
        }

        /// <summary>
        /// Calculated the grad value.
        /// </summary>
        /// <param name="rad">Rad value.</param>
        ///<returns>Calculated value.</returns>
        private static double Grad(double rad)
        {
            return rad * 180.0 / 3.1415926536;
        }

        /// <summary>
        /// Calculates the rad value
        /// </summary>
        /// <param name="grad">Grad value</param>
        /// <returns>Calculated value.</returns>
        private static double Rad(double grad)
        {
            return grad * 3.1415926536 / 180.0;
        }

        /// <summary>
        /// Method used to calculate the sun position based on the specified parameters.
        /// </summary>
        /// <param name="year">Selected year.</param>
        /// <param name="month">Selected month.</param>
        /// <param name="day">Selected day.</param>
        /// <param name="hour">Hours value.</param>
        /// <param name="min">Minutes value.</param>
        /// <param name="sec">Seconds value.</param>
        /// <param name="lat">Selected location latitude value.</param>
        /// <param name="lon">Selected location longitude value.</param>
        /// <param name="timezone">Time zone.</param>
        /// <returns>Returns the Sun position value based selected location parameters and the date time parameters.</returns>
        private static double SunPosition(int year, int month, int day, int hour, int min, int sec, double lat, double lon, double timezone)
        {
            double num = (double)new DateTime(year, 12, 31).DayOfYear;
            double num2 = (double)DayOfYear(year, month, day);
            double num3 = (double)hour + 0.016666666666666666 * (double)min + 0.00027777777777777778 * (double)sec - timezone + 1.0;
            num3 -= 4.0 * (15.0 - lon) / 60.0;
            num2 = (double)((int)(num2 * 360.0 / num + num3 / 24.0));
            double grad = 0.3948 - 23.2559 * Math.Cos(Rad(num2 + 9.1)) - 0.3915 * Math.Cos(Rad(2.0 * num2 + 5.4)) - 0.1764 * Math.Cos(Rad(3.0 * num2 + 26.0));
            double num4 = 0.0066 + 7.3525 * Math.Cos(Rad(num2 + 85.9)) + 9.9359 * Math.Cos(Rad(2.0 * num2 + 108.9)) + 0.3387 * Math.Cos(Rad(3.0 * num2 + 105.2));
            double num5 = num3 + num4 / 60.0;
            double grad2 = (12.0 - num5) * 15.0;
            double num6 = Math.Cos(Rad(grad2)) * Math.Cos(Rad(lat)) * Math.Cos(Rad(grad)) + Math.Sin(Rad(lat)) * Math.Sin(Rad(grad));
            num6 = (num6 > 1.0) ? 1.0 : ((num6 < -1.0) ? -1.0 : num6);
            double num7 = Grad(Math.Asin(num6));
            double num8 = (Math.Sin(Rad(num7)) * Math.Sin(Rad(lat)) - Math.Sin(Rad(grad))) / (Math.Cos(Rad(num7)) * Math.Cos(Rad(lat)));
            num8 = (num8 > 1.0) ? 1.0 : ((num8 < -1.0) ? -1.0 : num8);
            double num9 = Grad(Math.Acos(num8));
            if (num5 > 12.0 || num5 < 0.0)
            {
                num9 = 180.0 + num9;
            }
            else
            {
                num9 = 180.0 - num9;
            }

            return Math.Round(num7 * 1000.0) / 1000.0;
        }

        /// <summary>
        /// Get the Sun rise value based on location parameters and the date and time parameters.
        /// </summary>
        /// <param name="year">Selected year.</param>
        /// <param name="month">Selected month.</param>
        /// <param name="day">Selected day.</param>
        /// <param name="lat">Selected Location latitude value.</param>
        /// <param name="lon">Selected Location longitude value.</param>
        /// <param name="timezone">Time zone</param>
        /// <returns>Sunrise value of the specified location.</returns>
        private static double SunRise(int year, int month, int day, double lat, double lon, double timezone)
        {
            double num = 900.0;
            double num2 = 0.0 - num;
            double num3 = 86400.0;
            int hour;
            int min;
            int sec;
            do
            {
                num2 += num;
                hour = Convert.ToInt32(Math.Truncate(num2 / 3600.0));
                min = Convert.ToInt32(Math.Truncate(num2 % 3600.0 / 60.0));
                sec = Convert.ToInt32(Math.Truncate(num2)) % 60;
            }
            while (SunPosition(year, month, day, hour, min, sec, lat, lon, timezone) > 0.0 && num2 > num3);
            double num4 = num2 - num;
            do
            {
                num4 += num;
                hour = Convert.ToInt32(Math.Truncate(num4 / 3600.0));
                min = Convert.ToInt32(Math.Truncate(num4 % 3600.0 / 60.0));
                sec = Convert.ToInt32(Math.Truncate(num4)) % 60;
            }
            while ((SunPosition(year, month, day, hour, min, sec, lat, lon, timezone) < 0.0 || num4 < num2) && num4 < num3);
            if (num2 <= num3 && num4 <= num3)
            {
                num = num4 - num2;
                num2 += num / 2.0;
                while (num > 1.0)
                {
                    num /= 2.0;
                    hour = Convert.ToInt32(Math.Truncate(num2 / 3600.0));
                    min = Convert.ToInt32(Math.Truncate(num2 % 3600.0 / 60.0));
                    sec = Convert.ToInt32(Math.Truncate(num2)) % 60;
                    if (SunPosition(year, month, day, hour, min, sec, lat, lon, timezone) > 0.0)
                    {
                        num2 -= num;
                    }
                    else
                    {
                        num2 += num;
                    }
                }

                return num2 / 3600.0;
            }

            return double.NaN;
        }

        /// <summary>
        /// Method used to calculate the sun set based on the location details.
        /// Location details shall include the latitude and longitude of the location.
        /// </summary>
        /// <param name="year">Selected year</param>
        /// <param name="month">Selected month</param>
        /// <param name="day">Selected day</param>
        /// <param name="lat">Location latitude value</param>
        /// <param name="lon">Location longitude value</param>
        /// <param name="timezone">Time zone</param>
        /// <returns>SunSet value of the specified location.</returns>
        private static double SunSet(int year, int month, int day, double lat, double lon, double timezone)
        {
            double num = 900.0;
            double num2 = 0.0;
            double num3 = 86400.0 + num;
            int hour;
            int min;
            int sec;
            do
            {
                num3 -= num;
                hour = Convert.ToInt32(Math.Truncate(num3 / 3600.0));
                min = Convert.ToInt32(Math.Truncate(num3 % 3600.0 / 60.0));
                sec = Convert.ToInt32(Math.Truncate(num3)) % 60;
            }
            while (SunPosition(year, month, day, hour, min, sec, lat, lon, timezone) > 0.0 && num3 > num2);
            double num4 = num3 + num;
            do
            {
                num4 -= num;
                hour = Convert.ToInt32(Math.Truncate(num4 / 3600.0));
                min = Convert.ToInt32(Math.Truncate(num4 % 3600.0 / 60.0));
                sec = Convert.ToInt32(Math.Truncate(num4)) % 60;
            }
            while ((SunPosition(year, month, day, hour, min, sec, lat, lon, timezone) < 0.0 || num4 > num3) && num4 > num2);
            if (num4 >= num2 && num3 >= num2)
            {
                num = num3 - num4;
                num4 += num / 2.0;
                while (num > 1.0)
                {
                    num /= 2.0;
                    hour = Convert.ToInt32(Math.Truncate(num4 / 3600.0));
                    min = Convert.ToInt32(Math.Truncate(num4 % 3600.0 / 60.0));
                    sec = Convert.ToInt32(Math.Truncate(num4)) % 60;
                    if (SunPosition(year, month, day, hour, min, sec, lat, lon, timezone) > 0.0)
                    {
                        num4 += num;
                    }
                    else
                    {
                        num4 -= num;
                    }
                }

                return num4 / 3600.0;
            }

            return double.NaN;
        }

        /// <summary>
        /// Get sun location based on the schedule date with the location parameters.
        /// </summary>
        /// <param name="scheduleDate">Selected date.</param>
        /// <param name="sunlocationCurrentDay">Sun set location current date.</param>
        /// <param name="sunlocationNextDay">Sun rise location next date.</param>
        /// <param name="selectedOffset">Selected location Offset.</param>
        /// <param name="selectedLatitude">Selected location Latitude Value.</param>
        /// <param name="selectedLongitude">Selected location Longitude value.</param>
        public static void GetSunLocation(DateTime scheduleDate, out SunDawn sunlocationCurrentDay, out SunDawn sunlocationNextDay, double selectedOffset, double selectedLatitude, double selectedLongitude)
        {
            DateTime tmpnextDate = scheduleDate.AddDays(1);
            sunlocationCurrentDay = AstroBasedUtils.CalcSunrise(scheduleDate, selectedOffset, new GeographicalPosition(selectedLatitude, selectedLongitude));
            sunlocationNextDay = AstroBasedUtils.CalcSunrise(tmpnextDate, selectedOffset, new GeographicalPosition(selectedLatitude, selectedLongitude));
        }

        /// <summary>
        /// Calculates the date time based on date and sunrise/sunset time in AstroDIM
        /// </summary>
        /// <param name="date">The date.</param>
        /// <param name="time">The time.</param>
        /// <returns>Returns converted date time value.</returns>
        public static DateTime DoubleTimeToDate(DateTime date, double time)
        {
            DateTime returnValue = DateTime.Now;
            TimeSpan tmpTime = TimeSpan.FromHours(time);
            try
            {
                returnValue = new DateTime(date.Year, date.Month, date.Day, tmpTime.Hours, tmpTime.Minutes, tmpTime.Seconds);
            }
            catch (ArgumentOutOfRangeException)
            {
            }

            return returnValue;
        }

        /// <summary>
        /// Calculates the schedule values for AstroDIM.
        /// </summary>
        /// <param name="value">The current value.</param>
        /// <param name="is4Dim">If set to <c>true</c> [is 4 dim] else false.</param>
        /// <param name="sunSet">The sun set time.</param>
        /// <param name="sunRise">The sun rise time.</param>
        /// <param name="dimlength1">The dim length1.</param>
        /// <param name="dimlength2">The dim length2.</param>
        /// <param name="dimShift1">The dim shift1.</param>
        /// <param name="dimShift2">The dim shift2.</param>
        /// <param name="virtualMidnight">The virtual midnight.</param>
        /// <param name="duration23">The duration 2 to 3.</param>
        /// <param name="duration34">The duration 3 to 4.</param>
        /// <param name="duration45">The duration 4 to 5.</param>
        /// <param name="dimStartTimeSpan">The dim start time span.</param>
        /// <param name="sr2">The Sun Rises time 2.</param>
        /// <param name="sr3">The Sun Rises time 3.</param>
        /// <param name="sr4">The Sun Rises time 4.</param>
        /// <param name="sr5">The Sun Rises time 5.</param>
        public static void CalculateScheduleValues(
            DateTime value,
            bool is4Dim,
            DateTime sunSet,
            DateTime sunRise,
            TimeSpan dimlength1,
            TimeSpan dimlength2,
            TimeSpan dimShift1,
            TimeSpan dimShift2,
            DateTime virtualMidnight,
            TimeSpan duration23,
            TimeSpan duration34,
            TimeSpan duration45,
            TimeSpan dimStartTimeSpan,
            out DateTime sr2,
            out DateTime sr3,
            out DateTime sr4,
            out DateTime sr5)
        {
            sr2 = new DateTime();
            sr3 = new DateTime();
            sr4 = new DateTime();
            sr5 = new DateTime();
            if (is4Dim)
            {
                DateTime time2 = virtualMidnight.Subtract(dimStartTimeSpan);//vm -dst;
                DateTime time3 = time2.Add(duration23);//T2 +dd1;
                DateTime time4 = time3.Add(duration34);//T3 +dd2;

                sr2 = AdjustSrValue(sunSet, sunRise, time2);                   //vm -dst;
                sr3 = AdjustSrValue(sunSet, sunRise, time2.Add(duration23));   //ts2+dd1;
                sr4 = AdjustSrValue(sunSet, sunRise, time3.Add(duration34));   //ts3+dd2;
                sr5 = AdjustSrValue(sunSet, sunRise, time4.Add(duration45));   //ts4+dd3;
            }
            else
            {
                TimeSpan halfDL1 = new TimeSpan(dimlength1.Ticks / 2);
                TimeSpan halfDL2 = new TimeSpan(dimlength2.Ticks / 2);

                sr2 = AdjustSrValue(sunSet, sunRise, virtualMidnight.Add(dimShift1).Subtract(halfDL1));
                sr3 = AdjustSrValue(sunSet, sunRise, virtualMidnight.Add(dimShift2).Subtract(halfDL2));
                sr4 = AdjustSrValue(sunSet, sunRise, virtualMidnight.Add(dimShift2).Add(halfDL2));
                sr5 = AdjustSrValue(sunSet, sunRise, virtualMidnight.Add(dimShift1).Add(halfDL1));
            }
        }

        /// <summary>
        /// Adjusts the sun rise value for AstroDIM.
        /// </summary>
        /// <param name="sunSet">The sun set value.</param>
        /// <param name="sunRise">The sun rise.</param>
        /// <param name="sunRiseValue">The sun rise value.</param>
        /// <returns>Returns adjusted date time value.</returns>
        public static DateTime AdjustSrValue(DateTime sunSet, DateTime sunRise, DateTime sunRiseValue)
        {
            DateTime returnValue = new DateTime();
            TimeSpan sr = new TimeSpan(sunRiseValue.Hour, sunRiseValue.Minute, sunRiseValue.Second);

            TimeSpan todayMinValue = new TimeSpan(sunSet.Hour, sunSet.Minute, sunSet.Second);
            TimeSpan tomorrowMaxValue = new TimeSpan(sunRise.Hour, sunRise.Minute, sunRise.Second);

            if (sr < todayMinValue && sunSet.Day == sunRiseValue.Day)
            {
                returnValue = new DateTime(sunRiseValue.Year, sunRiseValue.Month, sunRiseValue.Day, sunSet.Hour, sunSet.Minute, sunSet.Second);
            }
            else if (sr > tomorrowMaxValue && sunRise.Day == sunRiseValue.Day)
            {
                returnValue = new DateTime(sunRiseValue.Year, sunRiseValue.Month, sunRiseValue.Day, sunRise.Hour, sunRise.Minute, sunRise.Second);
            }
            else if (sunRise < sunRiseValue)
            {
                returnValue = sunRise;
            }
            else
            {
                returnValue = sunRiseValue;
            }

            return returnValue;
        }

        /// <summary>
        /// Gets date list of a year to be displayed in AstroIDM Schedule.
        /// </summary>
        /// <param name="year">The particular year for which date list is to be calculated.</param>
        /// <returns>List of available dates for a particular year.</returns>
        public static List<DateTime> GetDateListOfYear(int year)
        {
            if (dateListOfYear == null)
            {
                dateListOfYear = new List<DateTime>();
            }
            else if (dateListOfYear[0].Year == year)
            {
                return dateListOfYear;
            }

            if (year > 1900)
            {
                for (int month = 1; month <= 12; month++)
                {
                    dateListOfYear.AddRange(
                        Enumerable.Range(1, DateTime.DaysInMonth(year, month))
                        .Select(day => new DateTime(year, month, day))
                        .ToList());
                }
            }

            return dateListOfYear;
        }

        /// <summary>
        /// Get time as double value from timestamp format.
        /// </summary>
        /// <param name="durationNP">Time span.</param>
        /// <returns>Time in double.</returns>
        public static double GetTimeInDoubleValue(TimeSpan durationNP)
        {
            double dimLength1 = durationNP.Hours + ((durationNP.Minutes * 100) / 60) * .01;
            return dimLength1;
        }

        /// <summary>
        /// Methods to map fade time value to error validation value .
        /// </summary>
        /// <param name="input">fade time value.</param>
        /// <returns>fade time value mapped with error validation value</returns>
        public static double AstroFadeTimeToDouble(string input)
        {
            decimal tmpMin = 0, tmpSec = 0;
            double returnValue = 0, tmpInput;
            if (input != null)
            {
                string[] htime = input.Split(':');
                if (input.ToUpper() == "OFF")
                {
                    returnValue = 0;
                }
                else if (input.ToUpper() == "OO")
                {
                    returnValue = 25;
                }
                else
                {
                    decimal.TryParse(htime[0], out tmpMin);
                    decimal.TryParse(htime[1], out tmpSec);
                    tmpInput = Convert.ToDouble(tmpMin * 60) + Convert.ToDouble(tmpSec);
                    returnValue = 0.00028 * tmpInput;
                }
            }

            return returnValue;
        }
    }

    /// <summary>
    ///Geographical Position class shall represents the latitude and longitude properties of the selected location.
    /// </summary>
    public partial class GeographicalPosition
    {
        /// <summary>
        /// The latitude of the selected location.
        /// </summary>
        private double mlatitude;

        /// <summary>
        /// The longitude of the selected location.
        /// </summary>
        private double mlongitude;

        public GeographicalPosition(double latitude, double longitude)
        {
            mlatitude = latitude;
            mlongitude = longitude;
        }

        /// <summary>
        /// Gets or sets the latitude of the location
        /// </summary>
        /// <value>
        /// The latitude.
        /// </value>
        public double Latitude
        {
            get
            {
                return mlatitude;
            }

            set
            {
                mlatitude = value;
            }
        }

        /// <summary>
        /// Gets or sets the longitude of the location.
        /// </summary>
        /// <value>
        /// The longitude.
        /// </value>
        public double Longitude
        {
            get
            {
                return mlongitude;
            }

            set
            {
                mlongitude = value;
            }
        }
    }

    /// <summary>
    /// This class represents the sun rise and sun set value of the location.
    /// </summary>
    public class SunDawn
    {
        private double msunDown;
        private double msunRise;

        /// <summary>
        /// Gets or sets the sun rise value.
        /// </summary>
        /// <value>
        /// The sun rice value.
        /// </value>
        public double Rise
        {
            get
            {
                return msunRise;
            }

            set
            {
                msunRise = value;
            }
        }

        /// <summary>
        /// Gets or sets the sun set value.
        /// </summary>
        /// <value>
        /// The sun set value.
        /// </value>
        public double Set
        {
            get
            {
                return msunDown;
            }

            set
            {
                msunDown = value;
            }
        }
    }



    /// <summary>
    /// Class entity of Astro Schedules
    /// </summary>
    public class AstroSchedules
    {
        #region Fields

        private double astroTime;
        private double astroMidTime;
        private DateTime astroDate;
        private DateTime dtscheduleSunRise;
        private DateTime dtscheduleSunSet;
        private DateTime dtscheduleTimeSR2;
        private DateTime dtscheduleTimeSR3;
        private DateTime dtscheduleTimeSR4;
        private DateTime dtscheduleTimeSR5;
        private bool isEditable;

        #endregion

        #region Constructors

        public AstroSchedules()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="AstroSchedules"/> class.
        /// </summary>
        /// <param name="date">The date.</param>
        /// <param name="astroTime">The astro time.</param>
        /// <param name="astroMidTime">The astro mid time.</param>
        /// <param name="sunSet">The sun set.</param>
        /// <param name="sunRise">The sun rise.</param>
        /// <param name="sR2">The s r2.</param>
        /// <param name="sR3">The s r3.</param>
        /// <param name="sR4">The s r4.</param>
        /// <param name="sR5">The s r5.</param>
        /// <param name="isEditable">if set to <c>true</c> [is editable].</param>
        /// <param name="isVertexVisible">The is vertex visible.</param>
        public AstroSchedules(DateTime date, double astroTime, double astroMidTime, DateTime sunSet, DateTime sunRise, DateTime sR2, DateTime sR3, DateTime sR4, DateTime sR5, bool isEditable)
        {
            AstroDate = date;
            AstroTime = astroTime;
            AstroMidTime = astroMidTime;
            DTScheduleSunSet = sunSet;
            DTScheduleSunRise = sunRise;
            DTScheduleTimeSR2 = sR2;
            DTScheduleTimeSR3 = sR3;
            DTScheduleTimeSR4 = sR4;
            DTScheduleTimeSR5 = sR5;
            IsEditable = isEditable;
        }

        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets the astro date.
        /// </summary>
        /// <value>
        /// The astro date.
        /// </value>
        public DateTime AstroDate
        {
            get
            {
                return astroDate;
            }

            set
            {
                astroDate = value;

            }
        }

        /// <summary>
        /// Gets or sets the astro time.
        /// </summary>
        /// <value>
        /// The astro time.
        /// </value>
        public double AstroTime
        {
            get
            {
                return astroTime;
            }

            set
            {
                astroTime = value;

            }
        }

        /// <summary>
        /// Gets or sets the astro mid time.
        /// </summary>
        /// <value>
        /// The astro mid time.
        /// </value>
        public double AstroMidTime
        {
            get
            {
                return astroMidTime;
            }

            set
            {
                astroMidTime = value;

            }
        }

        /// <summary>
        /// Gets or sets the schedule sun rise.
        /// </summary>
        /// <value>
        /// The schedule sun rise.
        /// </value>
        public DateTime DTScheduleSunRise
        {
            get
            {
                return dtscheduleSunRise;
            }

            set
            {
                dtscheduleSunRise = value;

            }
        }

        /// <summary>
        /// Gets or sets the schedule sun set.
        /// </summary>
        /// <value>
        /// The schedule sun set.
        /// </value>
        public DateTime DTScheduleSunSet
        {
            get
            {
                return dtscheduleSunSet;
            }

            set
            {
                dtscheduleSunSet = value;

            }
        }

        /// <summary>
        /// Gets or sets the schedule time 2 sun rise.
        /// </summary>
        /// <value>
        /// The schedule time 2 sun rise.
        /// </value>
        public DateTime DTScheduleTimeSR2
        {
            get
            {
                return dtscheduleTimeSR2;
            }

            set
            {
                dtscheduleTimeSR2 = value;

            }
        }

        /// <summary>
        /// Gets or sets the schedule time 3 sun rise.
        /// </summary>
        /// <value>
        /// The  schedule time 3 sun rise.
        /// </value>
        public DateTime DTScheduleTimeSR3
        {
            get
            {
                return dtscheduleTimeSR3;
            }

            set
            {
                dtscheduleTimeSR3 = value;

            }
        }

        /// <summary>
        /// Gets or sets the schedule time 4 sun rise.
        /// </summary>
        /// <value>
        /// The schedule time 4 sun rise.
        /// </value>
        public DateTime DTScheduleTimeSR4
        {
            get
            {
                return dtscheduleTimeSR4;
            }

            set
            {
                dtscheduleTimeSR4 = value;

            }
        }

        /// <summary>
        /// Gets or sets the schedule time 5 sun rise.
        /// </summary>
        /// <value>
        /// The schedule time 5 sun rise.
        /// </value>
        public DateTime DTScheduleTimeSR5
        {
            get
            {
                return dtscheduleTimeSR5;
            }

            set
            {
                dtscheduleTimeSR5 = value;

            }
        }

        /// <summary>
        /// Gets the y axis maximum value.
        /// </summary>
        /// <value>
        /// The y axis maximum value.
        /// </value>
        public double YAxisMaxValue
        {
            get
            {
                return 25;
            }
        }

        /// <summary>
        /// Gets the y axis minimum value.
        /// </summary>
        /// <value>
        /// The y axis minimum value.
        /// </value>
        public double YAxisMinValue
        {
            get
            {
                return 0;
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether this instance is editable.
        /// </summary>
        /// <value>
        /// <c>true</c> if this instance is editable; otherwise, <c>false</c>.
        /// </value>
        public bool IsEditable
        {
            get
            {
                return isEditable;
            }

            set
            {
                isEditable = value;

            }
        }



        #endregion
    }


    /// <summary>
    /// Represents object for validation errors.
    /// </summary>
    public class ErrorValidationValue
    {
        /// <summary>
        /// Gets or sets the error identifier.
        /// </summary>
        /// <value>
        /// The error identifier.
        /// </value>
        public int ErrorId { get; set; }


        /// <summary>
        /// Gets or sets a value indicating whether this instance is todays value.
        /// </summary>
        /// <value>
        /// <c>true</c> if this instance is todays value; otherwise, <c>false</c>.
        /// </value>
        public bool IsTodaysValue { get; set; }

        /// <summary>
        /// Gets or sets the output level.
        /// </summary>
        /// <value>
        /// The output level.
        /// </value>
        public double OutputLevel { get; set; }

        /// <summary>
        /// Gets or sets the scale value of time.
        /// </summary>
        /// <value>
        /// The scale value of time.
        /// </value>
        public double ScaleValueOfTime { get; set; }

        /// <summary>
        /// Gets or sets the time no value.
        /// </summary>
        /// <value>
        /// The time no value.
        /// </value>
        public double OperatingTime { get; set; }

        /// <summary>
        /// Gets or sets the value.
        /// </summary>
        /// <value>
        /// The value.
        /// </value>
        public DateTime Value { get; set; }
    }

}
