﻿using Plugin.Geolocator;
using Plugin.Geolocator.Abstractions;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Osram.TFTBackend.LocationService.Contracts;
using static Osram.TFTBackend.LocationService.EcgLocation;

namespace Osram.TFTBackend.LocationService
{
    public class GeoLocationService : ILocationService
    {
        IGeolocator _locator;
        EventHandler<PositionEventArgs> handler = null;

        public GeoLocationService()
        {
            _locator = CrossGeolocator.Current;
        }

        public async Task<Position> GetCurrentLocationAsync(int timeoutInMilliSeconds = 10000, double desiredAccuracy = 5000)
        {
            var tcs = new TaskCompletionSource<Position>();
            Position result = new Position();
            _locator.DesiredAccuracy = desiredAccuracy;

            if (!_locator.IsGeolocationAvailable)
            {
                Logger.TaggedError("GeolocatorPlugin", "Geolocation is not available on this device");
                return null;
            }
            if (!_locator.IsGeolocationEnabled)
            {
                Logger.TaggedError("GeolocatorPlugin", "Geolocation is not enabled on this device");
                return null;
            }

            try
            {
                // workaround for bug in Android/Xamarin -> GetPositionAsync always cancelling task (https://github.com/xamarin/Xamarin.Mobile/issues/16)
                if (!_locator.IsListening)
                {
                    await _locator.StartListeningAsync(new TimeSpan(1 * TimeSpan.TicksPerMillisecond), 1);
                }

                handler = async (object sender, PositionEventArgs e) =>
              {
                  result.Timestamp = e.Position.Timestamp;
                  result.Latitude = e.Position.Latitude;
                  result.Longitude = e.Position.Longitude;
                  result.Accuracy = e.Position.Accuracy;

                  _locator.PositionChanged -= handler;
                  tcs.SetResult(result);
                  await _locator.StopListeningAsync();
              };
                _locator.PositionChanged += handler;

                await _locator.GetPositionAsync(new TimeSpan(timeoutInMilliSeconds * TimeSpan.TicksPerMillisecond)).ContinueWith(t => { });

            }
            catch (Exception e)
            {
                Logger.Trace($"Unable to get location, may need to increase timeout. Exception: {e.Message}");
                tcs.SetException(e);
            }

            return tcs.Task.Result;
        }
      
        public List<Location> GetLocations()
        {
            return Locations;
        }

        public string GetTimeZoneUtcString(TimeSpan offset)
        {
            string utcValue = "UTC";
            if (offset.Hours == 0 && offset.Minutes == 0)
            {
                return utcValue + "+00:00";
            }

            return utcValue + ((offset < TimeSpan.Zero) ? "-" : "+") + offset.ToString(@"hh\:mm");
        }

        public bool IsLocationAvailable => _locator.IsGeolocationAvailable && _locator.IsGeolocationEnabled;
    }
}
