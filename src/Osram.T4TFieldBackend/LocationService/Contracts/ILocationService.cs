﻿using Plugin.Geolocator.Abstractions;
using System;
using System.Threading.Tasks;

namespace Osram.TFTBackend.LocationService.Contracts
{
    public interface ILocationService
    {
        /// <summary>
        /// Gets the location async.
        /// </summary>
        /// <returns>The location async.</returns>
        Task<Position> GetCurrentLocationAsync(int timeoutInMilliSeconds = 10000, double desiredAccuracyInMeters = 5000);

        /// <summary>
        /// Gets the utc zone string from TimeSpan offset in standard format (ex. UTC+02:00)
        /// </summary>
        /// <param name="offset"></param>
        /// <returns></returns>
        string GetTimeZoneUtcString(TimeSpan offset);

        /// <summary>
        /// Gets a value indicating whether is location available.
        /// </summary>
        /// <value><c>true</c> if is location available; otherwise, <c>false</c>.</value>
        bool IsLocationAvailable { get; }

    }
}
