﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Osram.TFTBackend.LocationService
{
    public static class EcgLocation
    {
        public static List<Location> Locations = new List<Location>
        {
            { new Location() {Name="Default", Latitude=0, Longitude=0, Offset=0} },
            // { new Location() {Name="Customized", Latitude=0, Longitude=0, Offset=0} },
            { new Location() {Name="Amsterdam", Latitude=419, Longitude=39, Offset=4} },
            { new Location() {Name="Ankara", Latitude=319, Longitude=263, Offset=8} },
            { new Location() {Name="Andorra", Latitude=340, Longitude=12, Offset=4} },
            { new Location() {Name="Athens", Latitude=304, Longitude=190, Offset=8} },
            { new Location() {Name="Atlanta, GA", Latitude=270, Longitude=64860, Offset=108} },
            { new Location() {Name="Austin, TX", Latitude=242, Longitude=64753, Offset=104} },
            { new Location() {Name="Baku", Latitude=323, Longitude=399, Offset=16} },
            { new Location() {Name="Belgrade", Latitude=359, Longitude=164, Offset=4} },
            { new Location() {Name="Berlin", Latitude=420, Longitude=107, Offset=4} },
            { new Location() {Name="Bern", Latitude=376, Longitude=60, Offset=4} },
            { new Location() {Name="Bogota", Latitude=36, Longitude=64943, Offset=108} },
            { new Location() {Name="Brasilia", Latitude=65409, Longitude=65153, Offset=116} },
            { new Location() {Name="Bratislava", Latitude=385, Longitude=137, Offset=4} },
            { new Location() {Name="Brussels", Latitude=407, Longitude=35, Offset=4} },
            { new Location() {Name="Bucharest", Latitude=355, Longitude=209, Offset=8} },
            { new Location() {Name="Budapest", Latitude=380, Longitude=152, Offset=4} },
            { new Location() {Name="Buenos Aires", Latitude=65259, Longitude=65068, Offset=116} },
            { new Location() {Name="Cape Town", Latitude=65265, Longitude=147, Offset=8} },
            { new Location() {Name="Cheyenne, WY", Latitude=329, Longitude=64697, Offset=100} },
            { new Location() {Name="Chicago, IL", Latitude=215, Longitude=64777, Offset=108} },
            { new Location() {Name="Chisinau", Latitude=376, Longitude=231, Offset=8} },
            { new Location() {Name="Copenhagen", Latitude=446, Longitude=101, Offset=4} },
            { new Location() {Name="Dubai", Latitude=202, Longitude=442, Offset=16} },
            { new Location() {Name="Dublin", Latitude=427, Longitude=65486, Offset=0} },
            { new Location() {Name="Geneva", Latitude=370, Longitude=49, Offset=4} },
            { new Location() {Name="Helsinki", Latitude=481, Longitude=200, Offset=8} },
            { new Location() {Name="Istanbul", Latitude=328, Longitude=232, Offset=8} },
            { new Location() {Name="Kiev", Latitude=403, Longitude=244, Offset=8} },
            { new Location() {Name="Lima", Latitude=65439, Longitude=64919, Offset=108} },
            { new Location() {Name="Lisbon", Latitude=310, Longitude=65463, Offset=0} },
            { new Location() {Name="Ljubljana", Latitude=369, Longitude=116, Offset=4} },
            { new Location() {Name="London", Latitude=412, Longitude=65535, Offset=0} },
            { new Location() {Name="Luxembourg", Latitude=397, Longitude=49, Offset=4} },
            { new Location() {Name="Lyon", Latitude=366, Longitude=39, Offset=4} },
            { new Location() {Name="Madrid", Latitude=323, Longitude=65506, Offset=4} },
            { new Location() {Name="Marseilles", Latitude=346, Longitude=43, Offset=4} },
            { new Location() {Name="Melbourne", Latitude=65234, Longitude=1160, Offset=40} },
            { new Location() {Name="Minsk", Latitude=431, Longitude=220, Offset=8} },
            { new Location() {Name="Monaco", Latitude=350, Longitude=59, Offset=4} },
            { new Location() {Name="Moscow", Latitude=446, Longitude=300, Offset=16} },
            { new Location() {Name="Munich", Latitude=385, Longitude=93, Offset=4} },
            { new Location() {Name="New Delhi", Latitude=229, Longitude=618, Offset=22} },
            { new Location() {Name="New York, NY", Latitude=325, Longitude=64943, Offset=108} },
            { new Location() {Name="Nicosia", Latitude=281, Longitude=267, Offset=8} },
            { new Location() {Name="Olympia, WA", Latitude=376, Longitude=64552, Offset=96} },
            { new Location() {Name="Oslo", Latitude=479, Longitude=86, Offset=4} },
            { new Location() {Name="Paris", Latitude=391, Longitude=19, Offset=4} },
            { new Location() {Name="Phoenix, AZ", Latitude=267, Longitude=64640, Offset=100} },
            { new Location() {Name="Prague", Latitude=401, Longitude=115, Offset=4} },
            { new Location() {Name="Reykjavik", Latitude=513, Longitude=65360, Offset=0} },
            { new Location() {Name="Riga", Latitude=455, Longitude=193, Offset=8} },
            { new Location() {Name="Riyadh", Latitude=197, Longitude=373, Offset=12} },
            { new Location() {Name="Rome", Latitude=335, Longitude=100, Offset=4} },
            { new Location() {Name="San Marino", Latitude=351, Longitude=99, Offset=4} },
            { new Location() {Name="Salvador da Bahia", Latitude=65432, Longitude=65228, Offset=116} },
            { new Location() {Name="San Diego, CA", Latitude=261, Longitude=64599, Offset=96} },
            { new Location() {Name="Sao Paulo", Latitude=65348, Longitude=65163, Offset=116} },
            { new Location() {Name="Sarajevo", Latitude=351, Longitude=147, Offset=4} },
            { new Location() {Name="Shanghai", Latitude=250, Longitude=972, Offset=32} },
            { new Location() {Name="Skopja", Latitude=336, Longitude=172, Offset=4} },
            { new Location() {Name="Sofia", Latitude=341, Longitude=186, Offset=8} },
            { new Location() {Name="Stockholm", Latitude=475, Longitude=145, Offset=4} },
            { new Location() {Name="Sydney", Latitude=65265, Longitude=1209, Offset=40} },
            { new Location() {Name="Tallinn", Latitude=475, Longitude=198, Offset=8} },
            { new Location() {Name="Tbilisi", Latitude=334, Longitude=358, Offset=16} },
            { new Location() {Name="Tel Aviv", Latitude=257, Longitude=278, Offset=12} },
            { new Location() {Name="Tirana", Latitude=331, Longitude=159, Offset=4} },
            { new Location() {Name="Vaduz", Latitude=377, Longitude=76, Offset=4} },
            { new Location() {Name="Valletta", Latitude=287, Longitude=116, Offset=4} },
            { new Location() {Name="Vienna", Latitude=386, Longitude=131, Offset=4} },
            { new Location() {Name="Vilnius", Latitude=437, Longitude=203, Offset=8} },
            { new Location() {Name="Warsaw", Latitude=418, Longitude=168, Offset=4} },
            { new Location() {Name="Yerevan", Latitude=321, Longitude=356, Offset=16} },
            { new Location() {Name="Zagreb", Latitude=366, Longitude=128, Offset=4} },
        };

        /// <summary>
        ///  Get Selected location from location list.
        /// </summary>
        /// <param name="latitude">Latitude value.</param>
        /// <param name="longitude">Longitude value.</param>
        /// <param name="offset">Offset value.</param>
        /// <returns>Selected Location.</returns>
        public static Location GetSelectedLocation(int latitude, int longitude, int offset)
        {
            if (Locations != null)
            {
                Location loc = Locations.Where(x => x.Latitude == latitude && x.Longitude == longitude && x.Offset == offset).FirstOrDefault();
                if(loc != null)
                {
                    return loc;
                }
                else
                {
                    return new Location { Name = "Customized", Latitude = (ushort)latitude, Longitude = (ushort)longitude, Offset = offset };
                }
            }
            return null;
        }


        public class Location
        {
            public Location()
            {

            }

            public string Name { get; set; }

            public ushort Latitude { get; set; }
            public ushort Longitude { get; set; }

            public double LatitudeValue { get { return (double)((short)Latitude) / 8.0;  } }
            public double LongitudeValue { get { return (double)((short)Longitude) / 8.0; } }
            

            private double offset;
            public double Offset
            {
                get { return offset; }
                set { offset = GetConvertedOffset(value); }
            }

            /// <summary>
            /// Get the actual offset.
            /// </summary>
            /// <param name="value"> Offset value.</param>
            /// <returns>Converted value.</returns>
            private double GetConvertedOffset(double value)
            {
                if (value <= 64)
                {
                    value = value / 4;
                }
                else
                {
                    value = (value - 128) / 4;
                }

                return value;
            }

            /// <summary>
            /// Gets the UTC from offset.
            /// </summary>
            /// <param name="offset">The offset.</param>
            /// <returns>Returns the universal time code from the offset.</returns>
            public string GetUTCFromOffset(double offset)
            {
                string utcCode = "UTC+00:00";

                string hourValue;
                string minutesValue = "0";
                string[] tmpValue = offset.ToString().Split('.');

                hourValue = Convert.ToInt32(tmpValue[0]).ToString();

                if (Convert.ToDouble(tmpValue[0]) < 0)
                {
                    hourValue = (-Convert.ToInt32(tmpValue[0])).ToString();
                }

                if (tmpValue.Count() == 2)
                {
                    minutesValue = (Convert.ToInt32(tmpValue[1].PadRight(2, '0')) * .6).ToString();
                }

                string time = string.Format("{0}:{1}", hourValue.PadLeft(2, '0'), minutesValue.PadRight(2, '0'));

                if (offset >= 0)
                {
                    utcCode = "UTC+" + time;
                }
                else if (offset < 0)
                {
                    utcCode = "UTC-" + time;
                }

                return utcCode;
            }

            public bool CoordinatesEquals(Location loc)
            {
                bool flag = true;
                flag &= Longitude == loc.Longitude;
                flag &= Latitude == loc.Latitude;
                flag &= Offset == loc.Offset;

                return flag;
            }
        }
    }
}
