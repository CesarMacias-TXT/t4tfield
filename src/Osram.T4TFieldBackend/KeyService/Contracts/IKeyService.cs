﻿
using Osram.TFTBackend.DataModel.Feature.Contracts;

namespace Osram.TFTBackend.KeyService.Contracts
{
    public interface IKeyService
    {
        bool IsDevicePasswordProtected { get; }

        bool ValidateMasterPassword(uint masterPassword);

        bool ValidateServicePassword(uint servicePassword);

        byte[] RetrieveMasterPassword(uint encryptedMasterPassword);

        byte[] RetrieveServicePassword(uint encryptedServicePassword);

        void ManageMasterPasswordForDeviceProgramming(uint oldPassword, uint newPassword);

        void ManageServicePasswordForDeviceProgramming(uint newPassword);

        void SetServicePasswordForDeviceProgramming(uint servicePassword);

        bool HasMasterPassword();

        bool HasServicePassword();

        ProtectionLock GetProtectionLock(int permissionIndex);
    }
}
