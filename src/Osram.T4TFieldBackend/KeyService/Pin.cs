﻿
using System;
using System.Collections.Generic;
using System.Linq;

namespace Osram.TFTBackend.KeyService
{
    public static class Pin
    {
        private static IList<uint> _crcTable;
        private static IList<uint> _crcReverseTable;

        public static uint EncryptMsk(uint cleartext)
        {
            return Crc32(Crc32(Crc32(uint.MaxValue, 3355205504U), cleartext), 102957826U);
        }

        /// <summary>
        ///        // 0. create both the 256 element long CRC and reverse CRC tables
        ///        // 1. fix_crc_pos is called twice
        ///        // 2. first call takes the crcresult parameter which is the encrypted MSK (passed through the swapint32 function first)
        ///        // 3. 
        /// </summary>
        /// <param name="encryptedMsk"></param>
        /// <returns></returns>
        public static byte[] DecryptMsk(uint encryptedMsk)
        {
            if(_crcTable == null)
                InitCrcTable();

            if (_crcReverseTable == null)
                InitCrcReverseTable();

            // apply first a byte-swapping algorithm
            var encryptedMskSwapped = SwapInt32(encryptedMsk);
            var encryptedMskAsBytes = BitConverter.GetBytes(encryptedMskSwapped);

            var result = fix_crc_pos(
                4,
                fix_crc_pos(4, encryptedMskAsBytes, 0, SwapInt32(0x06230302)),
                0, 
                SwapInt32(0x9caa4835)
                );

            // do another series of rocade with the result :
            // 0. take a deep breath
            // 1. convert the current byte-array in the buffer to an UInt32
            // 2. perform the swap-bytes function on this UInt32
            // 3. return the resulting UInt32 as a byte array
            // 4. don't forget to breath
            var adjustedResult = BitConverter.GetBytes(SwapInt32(BitConverter.ToUInt32(result, 0)));

            return adjustedResult; 
        }

        internal static uint[] StaticCrcTable = new uint[]
        {
            0x00000000, 0x04C11DB7, 0x09823B6E, 0x0D4326D9,
            0x130476DC, 0x17C56B6B, 0x1A864DB2, 0x1E475005,
            0x2608EDB8, 0x22C9F00F, 0x2F8AD6D6, 0x2B4BCB61,
            0x350C9B64, 0x31CD86D3, 0x3C8EA00A, 0x384FBDBD
        };

        internal static uint[] CrcTable => _crcTable?.ToArray();
        internal static uint[] CrcReverseTable => _crcReverseTable?.ToArray();

        internal static uint Crc32(uint crcState, uint data)
        {
            uint num1 = crcState ^ data;
            uint num2 = num1 << 4 ^ StaticCrcTable[(int) (num1 >> 28)];
            uint num3 = num2 << 4 ^ StaticCrcTable[(int) (num2 >> 28)];
            uint num4 = num3 << 4 ^ StaticCrcTable[(int) (num3 >> 28)];
            uint num5 = num4 << 4 ^ StaticCrcTable[(int) (num4 >> 28)];
            uint num6 = num5 << 4 ^ StaticCrcTable[(int) (num5 >> 28)];
            uint num7 = num6 << 4 ^ StaticCrcTable[(int) (num6 >> 28)];
            uint num8 = num7 << 4 ^ StaticCrcTable[(int) (num7 >> 28)];
            return num8 << 4 ^ StaticCrcTable[(int) (num8 >> 28)];
        }



        private const uint CRCPOLY = 0xEDB88320;
        private const uint INITXOR = 0xFFFFFFFF;
        private const uint FINALXOR = 0x00000000;


        private static byte SwapByte(byte x)
        {
            x = (byte) ((0x55 & x) << 1 | (0xAA & x) >> 1);
            x = (byte) ((0x33 & x) << 2 | (0xCC & x) >> 2);
            x = (byte) ((0x0F & x) << 4 | (0xF0 & x) >> 4);
            return x;
        }

        private static uint SwapInt32(uint theValue)
        {
            var resultAsBytes = new byte[4];

            var theValueAsBytes = BitConverter.GetBytes(theValue);
            for (int i = 0; i < 4; i++)
            {
                resultAsBytes[i] = SwapByte(theValueAsBytes[3 - i]);
            }

            return BitConverter.ToUInt32(resultAsBytes, 0);
        }


        /// <summary>
        /// Create and initialize the CRC table with 256 32-bit entries
        /// </summary>
        private static void InitCrcTable()
        {
            if (_crcTable == null)
            {
                _crcTable = new List<uint>();
            }

            for (uint n = 0; n < 256; n++)
            {
                uint c = n ;
                for (uint k = 0; k < 8; k++)
                {
                    if ((c & 1) != 0)
                    {
                        c = CRCPOLY ^ (c >> 1);
                    }
                    else
                    {
                        c = c >> 1;
                    }
                }

                _crcTable.Add(c);
            }
        }

        /// <summary>
        /// Create the reverse CRC table with 256 32-bit entries
        /// </summary>
        /// <returns></returns>
        private static void InitCrcReverseTable()
        {
            if (_crcReverseTable == null)
            {
                _crcReverseTable = new List<uint>();
            }

            for (uint n = 0; n < 256; n++)
            {
                uint c = n << 3 * 8;
                for (uint k = 0; k < 8; k++)
                {
                    if ((c & 0x80000000) != 0)
                    {
                        c = (((c ^ CRCPOLY) << 1) & 0xffffffff) | 1;
                    }
                    else
                    {
                        c = (c << 1) & 0xffffffff;
                    }
                }

                _crcReverseTable.Add(c);
            }
        }

        /// <summary>
        /// Changes the 4 bytes of the given buffer at position fix_pos so that
        /// it afterwards will compute to the given tcrcreg using the given crc_table.
        /// A reversed crc table must be provided.
        /// </summary>
        private static byte[] fix_crc_pos(uint length, byte[] tcrcreg, uint fixPos, uint initialXorValue)
        {

            var buffer = new byte[4];
            uint tcrcregValue = BitConverter.ToUInt32(tcrcreg, 0);

            // make sure fix_pos is within 0..(length-1)
            fixPos = fixPos % length;

            // calculate crc register at position fix_pos; this is essentially crc32()
            uint crcreg = initialXorValue;
            for (uint i = 0; i < fixPos; i++)
            {
                crcreg = (crcreg >> 8) ^ StaticCrcTable[((crcreg ^ buffer[i]) & 0xFF)];
            }

            // inject crcreg as new content
            for (uint i = 0; i < 4; i++)
            {
                buffer[fixPos + i] = (byte) ((crcreg >> (int) (i * 8)) & 0xFF);
            }

            // calculate crc backwards to fix_pos, beginning at the end
            tcrcregValue ^= FINALXOR;

            for (uint i = length - 1; i < fixPos - 1; i--)
            {
                tcrcregValue = ((tcrcregValue << 8) & 0xFFFFFFFF) ^ CrcReverseTable[tcrcregValue >> 3 * 8] ^ buffer[i];
            }

            // inject the new content
            for (int i = 0; i < 4; i++)
            {
                buffer[fixPos + i] = (byte) ((tcrcregValue >> i * 8) & 0xFF);
            }

            return buffer;
        }
    }
}
