﻿using Osram.TFTBackend.DataModel.Data.Contracts;
using Osram.TFTBackend.DataModel.Feature.Contracts;
using Osram.TFTBackend.KeyService.Contracts;
using System;

namespace Osram.TFTBackend.KeyService
{
    public class KeyService : IKeyService
    {
        private readonly IMasterAndServiceKeyData _masterAndServiceKeyData;

        public KeyService(IMasterAndServiceKeyData masterAndServiceKeyData)
        {
            _masterAndServiceKeyData = masterAndServiceKeyData;
        }

        public bool IsDevicePasswordProtected => HasPassword();

        public bool ValidateMasterPassword(uint masterPassword)
        {
            return ValidatePassword(masterPassword, _masterAndServiceKeyData.EncryptedMasterPassword);
        }

        public bool ValidateServicePassword(uint servicePassword)
        {
            return ValidatePassword(servicePassword, _masterAndServiceKeyData.EncryptedServicePassword);
        }

        public byte[] RetrieveMasterPassword(uint encryptedMasterPassword)
        {
            return Pin.DecryptMsk(encryptedMasterPassword);
        }

        public byte[] RetrieveServicePassword(uint encryptedServicePassword)
        {
            return Pin.DecryptMsk(encryptedServicePassword);
        }

        /// <summary>
        /// Check if master password is the same in the config and in the ECG
        /// - if yes, then validate password from config
        /// - if not, initiate change password
        /// </summary>
        /// <param name="oldPassword"></param>
        /// <param name="newPassword"></param>
        /// <returns></returns>
        public void ManageMasterPasswordForDeviceProgramming(uint oldPassword, uint newPassword)
        {
            // validate password
            uint validateNewPassword = newPassword;
            //in case of ot fit drivers, bio page not exist
            if (_masterAndServiceKeyData.MasterPassword == _masterAndServiceKeyData.EncryptedMasterPassword)
            {
                validateNewPassword = Pin.EncryptMsk(newPassword);
            }

            if (ValidatePassword(validateNewPassword, _masterAndServiceKeyData.EncryptedMasterPassword))
            {
                _masterAndServiceKeyData.MasterPassword = validateNewPassword;
                _masterAndServiceKeyData.NewMasterPassword = 0xFFFFFFFF;
                _masterAndServiceKeyData.NewMasterPasswordConfirmation = 0xFF;
            }
            else
            {
                // change password
                //in case of ot fit drivers, bio page not exist
                if (_masterAndServiceKeyData.MasterPassword == _masterAndServiceKeyData.EncryptedMasterPassword)
                {
                    _masterAndServiceKeyData.MasterPassword = validateNewPassword;
                }
                else
                {
                    _masterAndServiceKeyData.MasterPassword = oldPassword;
                    _masterAndServiceKeyData.NewMasterPassword = validateNewPassword;
                    //_masterAndServiceKeyData.NewMasterPasswordConfirmation = GetConfirmationByteOf(newPassword);
                    _masterAndServiceKeyData.NewMasterPasswordConfirmation = 0x00;
                }                
            }
        }

        public void ManageServicePasswordForDeviceProgramming(uint newPassword)
        {
            //in case of ot fit drivers, bio page not exist
            if (_masterAndServiceKeyData.ServicePassword == _masterAndServiceKeyData.EncryptedServicePassword)
            {
                _masterAndServiceKeyData.ServicePassword = Pin.EncryptMsk(newPassword);
            }
            else
            {
                _masterAndServiceKeyData.ServicePassword = newPassword;
                _masterAndServiceKeyData.ServicePasswordConfirmation = GetConfirmationByteOf(newPassword);
            }            
        }

        /// <summary>
        /// Set the PRR bit for MSK page
        /// </summary>
        /// <param name="servicePassword"></param>
        /// <returns></returns>
        public void SetServicePasswordForDeviceProgramming(uint servicePassword)
        {
            //in case of ot fit drivers, bio page not exist
            if (_masterAndServiceKeyData.ServicePassword == _masterAndServiceKeyData.EncryptedServicePassword)
            {
                _masterAndServiceKeyData.ServicePassword = Pin.EncryptMsk(servicePassword);
            }
            else
            {
                _masterAndServiceKeyData.ServicePassword = servicePassword;
            }
            //_masterAndServiceKeyData.ServicePasswordConfirmation = 0xFF;
            _masterAndServiceKeyData.ServicePasswordConfirmation = 0x01;
        }

        public int GetConfirmationByteOf(uint password)
        {
            int result = 0;

            // calculate sum of all bytes
            var bytes = BitConverter.GetBytes(password);
            foreach (var b in bytes)
            {
                result += b;
            }

            // divide the sum by 2
            result = result / 2;

            return result;
        }

        private bool ValidatePassword(uint password, uint configuredPassword)
        {
            if (configuredPassword == Pin.EncryptMsk(password))
            {
                return true;
            }

            return false;
        }

        private bool HasPassword()
        {
            if (_masterAndServiceKeyData.MasterPassword != 0)
                return true;

            return false;
        }

        public bool HasMasterPassword()
        {
            return HasPassword();
        }

        public bool HasServicePassword()
        {
            if (_masterAndServiceKeyData.ServicePassword != 0)
                return true;

            return false;
        }

        public ProtectionLock GetProtectionLock(int permissionIndex)
        {
            int permissionUserBit = _masterAndServiceKeyData.PermissionsUser & (1 << permissionIndex);
            if (permissionUserBit > 0)
                return ProtectionLock.None;

            int permissionServiceBit = _masterAndServiceKeyData.PermissionsService & (1 << permissionIndex);
            if (permissionServiceBit > 0)
                return ProtectionLock.Service;

            return ProtectionLock.Master;
        }
    }
}
