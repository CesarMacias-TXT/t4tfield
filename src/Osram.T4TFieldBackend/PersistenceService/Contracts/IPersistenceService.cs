﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using Osram.TFTBackend.BaseTypes;
using Osram.TFTBackend.ConfigurationService.DataModel;

namespace Osram.TFTBackend.PersistenceService.Contracts
{
    /// <summary>
    /// The storage service for ECG configurations. Each configuration is stored as a metadata database entry and
    /// the xml and picture as files using the metadata database Id as filename.
    /// </summary>
    public interface IPersistenceService
    {
        /// <summary>
        /// Selects possible configurations based on a given EcgSelector.
        /// EcgDeviceInfo is the header/metada of the device as-is represented in the DB
        /// </summary>
        /// <param name="selector"></param>
        /// <returns></returns>
        Task<EcgDeviceInfo> SelectDevice(EcgSelector selector);

        /// <summary>
        /// Returns a list of ostrup configurations.
        /// </summary>
        /// <returns></returns>
        Task<List<EcgDeviceInfo>> SelectGetAvailableOstrupConfigurations();

        /// <summary>
        /// Returns a device configuration (xml and pic) for the given database id.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        EcgTuple GetDeviceConfiguration(int id);

        /// <summary>
        /// Inserts a new device configuration info header in the database.
        /// </summary>
        /// <param name="fileName">The original file name (without the path)</param>
        /// <param name="xmlStream">A seekable stream of the xml configuration</param>
        /// <param name="pictureStream">The picture stream</param>
        /// <returns></returns>
        Task ImportConfiguration(String fileName, Stream xmlStream, Stream pictureStream);

        /// <summary>
        /// Deletes a configuration with the given id. It deletes the xml file, the picture file
        /// ad as well the database metadata.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        Task DeleteConfiguration(int id);

    }
}
