﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Osram.TFTBackend.BaseTypes;
using Osram.TFTBackend.DatabaseService.Contracts;

namespace Osram.TFTBackend.PersistenceService
{
    public interface IConfigurationDataStore : IDataStore<EcgDeviceInfo>
    {
        /// <summary>
        /// Returns true if the database is empty and needs to be prepopulated.
        /// </summary>
        Task<bool> IsDatabasePrePopulated();

        /// <summary>
        /// Returns a list of device infos matching a given Gtin.
        /// </summary>
        /// <param name="gtin">The selection criteria</param>
        Task<EcgDeviceInfo> LoadAllConfigurationsWithGtin(EcgSelector selector);


        /// <summary>
        /// Returns a list of device names.
        /// </summary>
        Task<List<string>> LoadAllConfigurationsNames();

        /// <summary>
        /// Inserts a new device info in the database and returns the id.
        /// </summary>
        /// <param name="entity"></param>
        /// <returns>The new assigend id</returns>
        Task<int> InsertConfiguration(EcgDeviceInfo entity);

        /// <summary>
        /// Creates a file name based of the given id and extension. Path will be the same as the database.
        /// </summary>
        /// <param name="id"></param>
        /// <param name="extension"></param>
        /// <returns></returns>
        String GetFileName(int id, String extension);

        /// <summary>
        /// Returns a list of ostrup configurations.
        /// </summary>
        /// <returns></returns>
        Task<List<EcgDeviceInfo>> LoadAllOstrupConfigurations();

        /// <summary>
        /// Checks if en entry with the given hash exists.
        /// </summary>
        /// <param name="hash"></param>
        /// <returns></returns>
        Task<bool> IsHashExisting(String hash);
    }
}
