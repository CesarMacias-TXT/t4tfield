﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Osram.TFTBackend.BaseTypes;
using Osram.TFTBackend.DatabaseService;
using Osram.TFTBackend.DatabaseService.Contracts;

namespace Osram.TFTBackend.PersistenceService
{
    public class ConfigurationDataStore : DataStoreBase<EcgDeviceInfo>, IConfigurationDataStore
    {
        public ConfigurationDataStore(IConnectionProvider connectionProvider) : base(connectionProvider)
        {
        }

        public override async Task<bool> ExistsAsync(EcgDeviceInfo entity)
        {
            return (await LoadAsync(entity.Id)) != null;
        }

        public async Task<EcgDeviceInfo> LoadAllConfigurationsWithGtin(EcgSelector selector)
        {
            var all = await Connection.Table<EcgDeviceInfo>().Where(deviceInfo => deviceInfo.IsDdFile).ToListAsync();
            var matches = all.Where(deviceInfo => selector.EqualsWithoutHwVersion(new EcgSelector(deviceInfo.Selector)));
            return matches.OrderBy(deviceInfo => selector.GetHwVersionDifference(new EcgSelector(deviceInfo.Selector))).FirstOrDefault();
        }

        public async Task<List<string>> LoadAllConfigurationsNames()
        {
            var allConfigurations = await Connection.Table<EcgDeviceInfo>().ToListAsync();
            List<string> configurationsNames = new List<string>();

            foreach (var configuration in allConfigurations)
            {
                configurationsNames.Add(configuration.ToString());
            }

            return configurationsNames;
        }

        public async Task<bool> IsDatabasePrePopulated()
        {
            int count = await CountAllAsync();
            return count > 0;
        }

        public async Task<int> InsertConfiguration(EcgDeviceInfo entity)
        {
            await InsertAsync(entity);
            return entity.Id;
        }

        public string GetFileName(int id, String extension)
        {
            return Path.Combine(DatabaseDirectory, $"{id}.{extension}");
        }

        public async Task<List<EcgDeviceInfo>> LoadAllOstrupConfigurations()
        {
            var list = await Connection.Table<EcgDeviceInfo>().ToListAsync();
            return list.Where(deviceInfo => deviceInfo.FileName.EndsWith(".osrtup", StringComparison.OrdinalIgnoreCase)).ToList();
        }

        public async Task<bool> IsHashExisting(String hash)
        {
            return await Connection.Table<EcgDeviceInfo>().Where(deviceInfo => deviceInfo.Md5Hash == hash).CountAsync() > 0;
        }
    }
}
