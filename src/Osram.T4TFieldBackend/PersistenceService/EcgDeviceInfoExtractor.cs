﻿using Osram.TFTBackend.BaseTypes;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Linq;

namespace Osram.TFTBackend.PersistenceService
{
    /// <summary>
    /// Extracts the device meta info from a configuration xml
    /// </summary>
    public class EcgDeviceInfoExtractor
    {
        public EcgDeviceInfo Extract(Stream xmlStream)
        {
            EcgDeviceInfo info = new EcgDeviceInfo();

            try
            {
                string xmlAsString = GetContentOfStream(xmlStream);
                xmlAsString = xmlAsString.Replace("<?xml version=\"1.0\" encoding=\"utf-16\"?>", "<?xml version=\"1.0\" encoding=\"utf-8\"?>");
                byte[] byteArray = Encoding.UTF8.GetBytes(xmlAsString);
                MemoryStream correctedXmlStream = new MemoryStream(byteArray);

                // clear the source xml stream
                xmlStream.SetLength(0);

                // copy the corrected xml content into the same xml stream reference
                correctedXmlStream.CopyTo(xmlStream);
                
                string anotherXmlAsString = GetContentOfStream(xmlStream);

                xmlStream.Position = 0;
                XDocument xdocument = XDocument.Load(xmlStream);
                info.BasicCode = xdocument.Root.Descendants("BasicCode").FirstOrDefault()?.Value;
                info.Gtin = Int64.Parse(xdocument.Root.Descendants("GTIN").FirstOrDefault()?.Value);
                info.Description = xdocument.Root.Descendants("DeviceDescription").FirstOrDefault()?.Value;
                byte firmwareVersion = Byte.Parse(xdocument.Root.Descendants("FW_Major").FirstOrDefault()?.Value);
                byte hardwareVersion = Byte.Parse(xdocument.Root.Descendants("HW_Major").FirstOrDefault()?.Value);
                info.Selector = new EcgSelector(info.Gtin, firmwareVersion, hardwareVersion).Value;
            }
            catch (XmlException e)
            {
                Logger.Trace(e.Message);
                //throw;
            }
            
            return info;
        }

        private string GetContentOfStream(Stream memoryStream)
        {
            memoryStream.Position = 0;
            var sr = new StreamReader(memoryStream);
            return sr.ReadToEnd();
        }
    }
}
