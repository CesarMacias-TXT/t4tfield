﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using Osram.TFTBackend.BaseTypes;
using Osram.TFTBackend.BaseTypes.Exceptions;
using Osram.TFTBackend.ConfigurationService.DataModel;
using Osram.TFTBackend.CryptographyService.Contracts;
using Osram.TFTBackend.FileService.Contracts;
using Osram.TFTBackend.PersistenceService.Contracts;

namespace Osram.TFTBackend.PersistenceService
{
    /// <summary>
    /// Gives access to the ECGs configuration files. The configurations and corresponding pictures are stored
    /// as files plus some metadata which are stored in a database.
    /// The metadata is used for fast quering and filtering, without loading and accessing xml files.
    /// To avoid file name collisions when importing a new configuration the xml and picture are stored with 
    /// the unique database id from the metadata.
    /// </summary>
    public class PersistenceService : IPersistenceService
    {
        private IConfigurationDataStore _configurationDataStore;
        private IFileService _fileService;
        private ICryptographyService _cryptographyService;

        public PersistenceService(IConfigurationDataStore configurationDataStore, IFileService fileService, ICryptographyService cryptographyService)
        {
            _configurationDataStore = configurationDataStore;
            _fileService = fileService;
            _cryptographyService = cryptographyService;
        }

        public async Task<EcgDeviceInfo> SelectDevice(EcgSelector selector)
        {
            Logger.Trace($"PersistenceService.SelectDevice EcgSelector = {selector}");
            return await _configurationDataStore.LoadAllConfigurationsWithGtin(selector);
        }

        public EcgTuple GetDeviceConfiguration(int id)
        {
            EcgTuple ecgTuple = new EcgTuple();
            ecgTuple.XmlConfiguration = _fileService.ReadTextFile(_configurationDataStore.GetFileName(id, "xml"));
            ecgTuple.ThePicture = _fileService.ReadBinaryFile(_configurationDataStore.GetFileName(id, "png"));
            return ecgTuple;
        }

        public async Task ImportConfiguration(string fileName, Stream xmlStream, Stream pictureStream)
        {
            Logger.Trace($"PersistenceService.ImportConfiguration: importing {fileName} ");
            string hash = CalculateHash(xmlStream);
            if (await _configurationDataStore.IsHashExisting(hash))
            {
                throw new OperationFailedException($"PersistenceService.ImportConfiguration: {fileName} already exists.", 1);
            }
            xmlStream.Seek(0, SeekOrigin.Begin);
            EcgDeviceInfo info = GetEcgDeviceInfo(xmlStream, fileName, hash);
            int id = await _configurationDataStore.InsertConfiguration(info);
            xmlStream.Seek(0, SeekOrigin.Begin);
            _fileService.CopyStreamToFile(xmlStream, _configurationDataStore.GetFileName(id, "xml"));
            _fileService.CopyStreamToFile(pictureStream, _configurationDataStore.GetFileName(id, "png"));
            Logger.Trace($"PersistenceService.ImportConfiguration: importing {fileName} done.");
        }

        public async Task DeleteConfiguration(int id)
        {
            Logger.Trace($"PersistenceService.DeleteConfiguration: deleting entry with id = {id} ");
            EcgDeviceInfo info = await _configurationDataStore.LoadAsync(id);
            if (info != null)
            {
                string xmlFileName = _configurationDataStore.GetFileName(id, "xml");
                _fileService.DeleteFile(xmlFileName);
                string pngFileName = _configurationDataStore.GetFileName(id, "png");
                _fileService.DeleteFile(pngFileName);
                await _configurationDataStore.RemoveAsync(info);
                Logger.Trace($"PersistenceService.DeleteConfiguration: deleted entry {info.FileName} ");
                return;
            }
            Logger.Trace($"PersistenceService.DeleteConfiguration: entry with id = {id} not found");
        }

        private EcgDeviceInfo GetEcgDeviceInfo(Stream xmlStream, string fileName, string hash)
        {
            EcgDeviceInfoExtractor extractor = new EcgDeviceInfoExtractor();
            EcgDeviceInfo ecgDeviceInfo = extractor.Extract(xmlStream);
            ecgDeviceInfo.FileName = fileName;
            ecgDeviceInfo.IsDdFile = fileName.EndsWith(".osrtud", StringComparison.OrdinalIgnoreCase) || fileName.EndsWith(".xml", StringComparison.OrdinalIgnoreCase);
            ecgDeviceInfo.Date = DateTime.Now;
            ecgDeviceInfo.Md5Hash = hash;
            return ecgDeviceInfo;
        }

        private string CalculateHash(Stream xmlStream)
        {
            xmlStream.Seek(0, SeekOrigin.Begin);
            return _cryptographyService.ComputeMD5(xmlStream);
        }

        public async Task<List<EcgDeviceInfo>> SelectGetAvailableOstrupConfigurations()
        {
            Logger.Trace("PersistenceService.SelectGetAvailableOstrupConfigurations");
            return await _configurationDataStore.LoadAllOstrupConfigurations();
        }
    }
}
