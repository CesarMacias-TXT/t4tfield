﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SQLite;
using Osram.TFTBackend.ConfigurationService.DataModel;

namespace Osram.TFTBackend.PersistenceService
{
    [Table("DeviceInfo")]
    public class EcgDeviceInfo
    {
        [PrimaryKey, AutoIncrement]
        public int Id { get; set; } = -1;

        public long Gtin { get; set; }

        public string BasicCode { get; set; }

        public string Description { get; set; }

        /// <summary>
        /// Combination from Gtin, firmware and hardware version, unique to select the correct xml
        /// </summary>
        public long Selector { get; set; }

        public bool IsDdFile { get; set; }

        public string FileName { get; set; }

        public DateTime Date { get; set; }

        /// <summary>
        /// MD5 hash of the xml configuration file, used to detect duplicates
        /// </summary>
        public string Md5Hash { get; set; }

        public override string ToString()
        {
            return FileName;
        }
    }
}
