﻿namespace Osram.TFTBackend
{
    public static class ErrorCodes
    {
        public const int Ok = 0;
        public const int ReadingNfcTagFailed = -1;
        public const int MasterKeysNotMatching = -10;
        public const int DeviceNotSupported = -11;
        public const int MasterKeysNotSetted = -12;

        public const int NoSourceTag = -20;
        public const int IncompatibleDevices = -21;
        public const int DifferentDevice = -22;
        public const int DifferentFamilyDevice = -23;
        public const int RFPasswordFailed = -99;
    }
}
