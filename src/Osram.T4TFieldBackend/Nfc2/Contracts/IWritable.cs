﻿using Osram.TFTBackend.NfcService.NfcDataTypes;

namespace Osram.TFTBackend.Nfc2.Contracts
{
    public interface IWritable
    {
        bool IsModified { get; }
        /// <summary>
        /// Returns the byte array of the interpreted memory area.
        /// </summary>
        /// <returns></returns>
        ByteArray GetMemoryArea();
    }
}
