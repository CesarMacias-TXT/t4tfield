﻿using Osram.TFTBackend.NfcService.NfcDataTypes;

namespace Osram.TFTBackend.Nfc2
{
    /// <summary>
    /// Validates memory areas with a CRC 16. Expects a memory area with an appended 16 bit CRC.
    /// CRC used is CRC 16 CCITT False (Polynomial: 0x1021, Initial value: 0xFFFF)
    /// </summary>
    public class CrcValidator
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger();

        /// <summary>
        /// The CRC from the memory area.
        /// </summary>
        public ushort Crc { get; }
        /// <summary>
        /// The calculated CRC, excluding the last 2 CRC bytes
        /// </summary>
        public ushort CalculatedCrc { get; set; }
        /// <summary>
        /// Verifies if the CRCs are the same.
        /// </summary>
        public bool CrcIsCorrect => Crc == CalculatedCrc;

        public CrcValidator()
        {
            Crc = 0;
            CalculatedCrc = 1;
        }

        /// <summary>
        /// Creates a new CrcValidator object and checks the CRC.
        /// </summary>
        /// <param name="data">The memory area including a trailing 16 bit CRC</param>
        public CrcValidator(ByteArray data)
        {
            if (data.Count >= 2)
            {
                Crc = data.GetUshort(data.Count - 2);
                CalculatedCrc = CalcCrc16CcittFalse(data, data.Count - 2);
                if (!CrcIsCorrect)
                    log.Error($"Memory area with invalid CRC. Crc=0x{Crc:X4} expected CRC={CalculatedCrc:X4}");
            }
        }

        /// <summary>
        /// Calculates a CRC 16 CCITT False (Polynomial: 0x1021, Initial value: 0xFFFF).
        /// </summary>
        /// <param name="data">The memory area</param>
        /// <param name="len">The byte count used for CRC calculation</param>
        /// <returns></returns>
        public static ushort CalcCrc16CcittFalse(ByteArray data, int len)
        {
            ushort Polynominal = 0x1021;
            ushort InitValue = 0xFFFF;

            ushort i, j, index = 0;
            ushort CRC = InitValue;
            for (i = 0; i < len; i++)
            {
                ushort short_c = (ushort)(0x00ff & (ushort)data[index]);
                ushort tmp = (ushort)((CRC >> 8) ^ short_c);
                ushort Remainder = (ushort)(tmp << 8);
                for (j = 0; j < 8; j++)
                {

                    if ((Remainder & 0x8000) != 0)
                    {
                        Remainder = (ushort)((Remainder << 1) ^ Polynominal);
                    }
                    else
                    {
                        Remainder = (ushort)(Remainder << 1);
                    }
                }
                CRC = (ushort)((CRC << 8) ^ Remainder);
                index++;
            }
            return CRC;
        }
    }
}
