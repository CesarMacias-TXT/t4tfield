﻿using System;

namespace Osram.TFTBackend.Nfc2.Tag.Services.Contracts
{
    public interface ICurrentTimeService
    {
        DateTime GetCurrentTime();
    }
}
