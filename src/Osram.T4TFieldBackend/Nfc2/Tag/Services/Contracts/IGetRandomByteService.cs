﻿namespace Osram.TFTBackend.Nfc2.Tag.Services.Contracts
{
    public interface IGetRandomByteService
    {
        byte GetRandomByte();
    }
}