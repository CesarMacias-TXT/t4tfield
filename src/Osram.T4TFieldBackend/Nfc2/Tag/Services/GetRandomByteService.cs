﻿using System;
using Osram.TFTBackend.Nfc2.Tag.Services.Contracts;

namespace Osram.TFTBackend.Nfc2.Tag.Services
{
    public class GetRandomByteService : IGetRandomByteService
    {
        private readonly Random _rand = new Random();

        public byte GetRandomByte()
        {
            return (byte)_rand.Next(1, 256);
        }
    }
}
