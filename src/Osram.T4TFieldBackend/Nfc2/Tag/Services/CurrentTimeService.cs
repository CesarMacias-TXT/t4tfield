﻿using System;
using Osram.TFTBackend.Nfc2.Tag.Services.Contracts;

namespace Osram.TFTBackend.Nfc2.Tag.Services
{
    public class CurrentTimeService : ICurrentTimeService
    {
        public DateTime GetCurrentTime()
        {
            return DateTime.Now;
        }
    }
}
