﻿using System.Collections.Generic;
using Osram.TFTBackend.Nfc2.TagStructures;

namespace Osram.TFTBackend.Nfc2.Tag
{
    /// <summary>
    /// Provides a decoded memory structure of a NFC tag according to the NFC-2 Interface of OSRAM ECGs.
    /// It covers only the NFC memory structures definded for the NFC tag. Memory banks are not decoded
    /// but keept as byte array.
    /// </summary>
    public class Nfc2Tag
    {
        public string NfcTagSerial { get; set; }

        public DeviceIdentificationRegisters DeviceIdentificationRegisters { get; set; }

        public TableOfContent TableOfContent { get; set; }

        public StatusRegister StatusRegister { get; set; }

        public ControlRegister ControlRegister { get; set; }

        public List<MemoryBank> MemoryBanks { get; set; }

    }
}
