﻿namespace Osram.TFTBackend.Nfc2.Tag
{
    public static class MemoryConstants
    {
        public static int TagPageSize = 128;
        public static int TagSize = 2048;

        public static int DeviceIdentificationRegistersStartAddress = 0x00;
        public static int DeviceIdentificationRegistersSize = 20;
        public static int TocStartAddress = 0x14;
        public static int TocEntrySize = 6;
        public static int StatusRegisterSize = 4;
        public static int ControlRegisterSize = 11;
    }
}
