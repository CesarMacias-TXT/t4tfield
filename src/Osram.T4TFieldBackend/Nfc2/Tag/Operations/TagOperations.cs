﻿using System;
using System.Collections.Generic;
using Osram.TFTBackend.Nfc2.TagStructures;
using Osram.TFTBackend.NfcService.Contracts;
using Osram.TFTBackend.NfcService.NfcDataTypes;
using Osram.TFTBackend.NfcService.Exceptions;
using System.Threading;

namespace Osram.TFTBackend.Nfc2.Tag.Operations
{
    public class TagOperations
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger();

        private INfcReader _reader;
        private LowLevelOperations _lowLevelOperations;
        private EventHandler<int> _progressEvent;
        private const int ReadMemoryBanksProgressValue = 50;

        public TagOperations(INfcReader reader, EventHandler<int> progressEvent)
        {
            _reader = reader;
            _lowLevelOperations = new LowLevelOperations(reader);
            _progressEvent = progressEvent;
        }

        /// <summary>
        /// Reads the tag and fills the tag structures from the NFC memory.
        /// </summary>
        /// <param name="tag"></param>
        public bool ReadTag(Nfc2Tag tag, CancellationToken cancellationToken)
        {
            bool resultValue = true;

            log.Info("Reading the tag ...");

            try
            {
                tag.NfcTagSerial = _reader.NfcTagSerial;

                cancellationToken.ThrowIfCancellationRequested();

                _progressEvent(this, 10);
                resultValue = _lowLevelOperations.ReadDeviceIdentificationRegisters(tag);
                if (!resultValue)
                {
                    throw new NfcReaderException("DeviceIdentificationRegisters");
                }

                cancellationToken.ThrowIfCancellationRequested();

                _progressEvent(this, 20);
                resultValue = _lowLevelOperations.ReadTableOfContent(tag);
                if (!resultValue)
                {
                    throw new NfcReaderException("TableOfContent");
                }

                cancellationToken.ThrowIfCancellationRequested();

                _progressEvent(this, 30);
                resultValue = _lowLevelOperations.ReadStatusRegister(tag);
                if (!resultValue)
                {
                    throw new NfcReaderException("StatusRegister");
                }

                cancellationToken.ThrowIfCancellationRequested();

                _progressEvent(this, 40);
                resultValue = _lowLevelOperations.ReadControlRegister(tag);
                if (!resultValue)
                {
                    throw new NfcReaderException("ControlRegister");
                }

                cancellationToken.ThrowIfCancellationRequested();

                _progressEvent(this, ReadMemoryBanksProgressValue);
                resultValue = ReadAllMemoryBanks(tag, cancellationToken);
                if (!resultValue)
                {
                    throw new NfcReaderException("MemoryBanks");
                }

                _progressEvent(this, 100);
                log.Info("Reading the tag success");
            }
            catch (NfcReaderException e)
            {
                log.Error(e);
                switch(e.Message)
                {
                    case "DeviceIdentificationRegisters":
                        tag.DeviceIdentificationRegisters = new DeviceIdentificationRegisters();
                        goto case "TableOfContent";
                    case "TableOfContent":
                        tag.TableOfContent = new TableOfContent();
                        goto case "StatusRegister";
                    case "StatusRegister":
                        tag.StatusRegister = new StatusRegister();
                        goto case "ControlRegister";
                    case "ControlRegister":
                        tag.ControlRegister = new ControlRegister();
                        goto case "MemoryBanks";
                    case "MemoryBanks":
                        List<MemoryBank> memoryBanks;
                        if (tag.TableOfContent.CrcIsCorrect)
                        {
                            memoryBanks = new List<MemoryBank>(tag.TableOfContent.ContentTable.Count);
                            foreach (var tocEntry in tag.TableOfContent.ContentTable)
                            {
                                memoryBanks.Add(new MemoryBank());
                            }
                        }
                        else
                        {
                            memoryBanks = new List<MemoryBank>(0);
                        }
                        tag.MemoryBanks = memoryBanks;
                        break;
                }
                log.Info("Reading the tag error");
            }

            log.Info("Reading the tag done");
            return resultValue;
        }

        /// <summary>
        /// Tries to erase the tag by overwriting the tag memory with 0xFFs.
        /// May not work on write protected pages.
        /// </summary>
        /// <param name="tag"></param>
        public void EraseTag(Nfc2Tag tag)
        {
            log.Info("Erasing the tag ...");
            // write page wise as it allows to continue after exceptions from a write protected page
            int memoryPages = MemoryConstants.TagSize / MemoryConstants.TagPageSize;
            ByteArray tagMemory = new ByteArray(0xFF, MemoryConstants.TagPageSize);
            for (int i = 0; i < memoryPages; i++)
            {
                try
                {
                    _reader.Write(i * MemoryConstants.TagPageSize, tagMemory);
                }
                catch (Exception e)
                {
                    log.Error(e);
                }
            }
            log.Info("Erasing the tag done");
        }

        private bool ReadAllMemoryBanks(Nfc2Tag tag, CancellationToken cancellationToken)
        {
            bool hasError = false;
            List<MemoryBank> memoryBanks;

            if (tag.TableOfContent.CrcIsCorrect)
            {
                memoryBanks = new List<MemoryBank>(tag.TableOfContent.ContentTable.Count);
                log.Info($"Reading all {tag.TableOfContent.ContentTable.Count} memory banks");
                int increment = (int)Math.Ceiling((decimal)ReadMemoryBanksProgressValue / tag.TableOfContent.ContentTable.Count);
                int progress = ReadMemoryBanksProgressValue;
                foreach (var tocEntry in tag.TableOfContent.ContentTable)
                {
                    cancellationToken.ThrowIfCancellationRequested();

                    log.Info($"Reading {tocEntry}");
                    if (!hasError)
                    {
                        var memoryBank = _lowLevelOperations.ReadMemoryBank(tocEntry);
                        if (!memoryBank.IsDataEmpty() || ((tocEntry.MbAttribut & 0xF) == 2) || 
                            (tocEntry.MbBaseAddress >= tag.DeviceIdentificationRegisters.ProtectedMemoryAreaStartAddress))    //skip read protected memory banks
                        {
                            _progressEvent(this, progress += increment);
                        } else
                        {
                            hasError = true;
                        }
                        memoryBanks.Add(memoryBank);
                    }
                    else
                    {
                        memoryBanks.Add(new MemoryBank());
                    }
                }
            }
            else
            {
                log.Error("Error reading all memory banks, table of content is CRC invalid!");
                memoryBanks = new List<MemoryBank>(0);
            }

            tag.MemoryBanks = memoryBanks;
            return !hasError;
        }

    }
}
