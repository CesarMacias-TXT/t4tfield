﻿using System;
using System.Collections.Generic;
using Osram.TFTBackend.BaseTypes.Exceptions;
using Osram.TFTBackend.Nfc2.Tag.Services.Contracts;
using Osram.TFTBackend.Nfc2.TagStructures;
using Osram.TFTBackend.NfcService.Contracts;
using Osram.TFTBackend.NfcService.Exceptions;

namespace Osram.TFTBackend.Nfc2.Tag.Operations
{
    /// <summary>
    /// Provides the NFC-2 interface operations for OSRAM ECGs.
    /// </summary>
    public class TagEcgOperations
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger();

        private INfcReader _reader;
        private ICurrentTimeService _timeService;
        private IGetRandomByteService _randomByteService;
        private LowLevelOperations _lowLevelOperations;
        private EventHandler<int> _progressEvent;
        private int _progress;

        private const int ShortTimeout = 5000;
        private const int LongTimeout = 9000;

        /// <summary>
        /// Test requirement. If set forces writing of memory banks with a CRC error in the first try to program the meory banks.
        /// </summary>
        public bool ForceProgrammingWithOneTimeCrcError { get; set; }

        public TagEcgOperations(INfcReader reader, EventHandler<int> progressEvent, int firstProgress, ICurrentTimeService timeService, IGetRandomByteService randomByteService)
        {
            _reader = reader;
            _timeService = timeService;
            _randomByteService = randomByteService;
            _lowLevelOperations = new LowLevelOperations(reader);
            _progressEvent = progressEvent;
            _progress = firstProgress;
        }

        /// <summary>
        /// Forces an update request if the ECG is online and reads the given memory banks.
        /// </summary>
        /// <param name="tag"></param>
        /// <param name="memoryBankIndices">Array of memory bank indices.</param>
        public void UpdateMemoryBanks(Nfc2Tag tag, int[] memoryBankIndices)
        {
            try
            {
                log.Info("UpdateMemoryBanks");
                if (IsEcgOnline(tag))
                {
                    ForceMemoryBankUpdates(tag, GetUpdateBitField(memoryBankIndices));
                }
                foreach (var memoryBankIndex in memoryBankIndices)
                {
                    _lowLevelOperations.ReadMemoryBank(tag, memoryBankIndex);
                }
                log.Info("UpdateMemoryBanks done");
            }
            catch (Exception e)
            {
                log.Error("Updating memory bank request failed.");
                log.Error(e);
                throw;
            }
            //TODO clean up in error case???
        }

        /// <summary>
        /// Programms a set of memory banks into the ECG. In case of the ECG is online it makes a programming
        /// request including memory locking. If the ECG is offline it just writes the memory banks to the NFC tag
        /// and sets the appropriate PRR bits.
        /// </summary>
        /// <param name="tag">The NFC tag data structure</param>
        /// <param name="memoryBankIndices">Array of memory bank toc indices.</param>
        /// <exception cref="NfcReaderException">Thrown in case of reader or protocol error</exception>
        /// <exception cref="TimeoutException">Thrown in case of a polling timeout.</exception>
        public void ProgramMemoryBanks(Nfc2Tag tag, int[] memoryBankIndices)
        {
            try
            {
                log.Info("ProgramMemoryBanks");
                _progressEvent(this, _progress);

                if (IsEcgOnline(tag))
                    OnlineProgramming(tag, memoryBankIndices);
                else
                    OfflineProgramming(tag, memoryBankIndices);

                _progressEvent(this, 100);
                log.Info("ProgramMemoryBanks done");
            }
            catch (RFPasswordFailedException rfPasswordFailedException)
            {
                ForceProgrammingWithOneTimeCrcError = false;
                log.Error("Present rf password request failed.");
                log.Error(rfPasswordFailedException);
                throw rfPasswordFailedException;
            }
            catch (Exception e)
            {
                ForceProgrammingWithOneTimeCrcError = false;
                log.Error("Programming memory bank request failed.");
                log.Error(e);
                throw;
            }
        }

        private void OfflineProgramming(Nfc2Tag tag, int[] memoryBankIndices)
        {
            UInt32 prr = ReadPrrRegister(tag);
            _progressEvent(this, _progress += 10);
            WriteMemoryBanks(tag, memoryBankIndices);
            _progressEvent(this, 90);
            SetPrrBitmask(tag, memoryBankIndices, prr);
        }

        private UInt32 ReadPrrRegister(Nfc2Tag tag)
        {
            _lowLevelOperations.ReadControlRegister(tag);
            if (!tag.ControlRegister.CrcIsCorrect)
                throw new NfcReaderException("Error reading control registers");
            return tag.ControlRegister.Prr;
        }

        private void OnlineProgramming(Nfc2Tag tag, int[] memoryBankIndices)
        {
            WaitUntilReadyForDataTransfer(tag);
            _progressEvent(this, _progress += 5);
            DoOnlineProgramming(tag, memoryBankIndices);
            ForceProgrammingWithOneTimeCrcError = false;    // do not write mb's wit crc errors in the repetition
            _progressEvent(this, 90);
            RepeatProgrammingInCaseOfError(tag);
        }

        private void DoOnlineProgramming(Nfc2Tag tag, int[] memoryBankIndices)
        {
            WaitForProgrammingPermission(tag);
            _progressEvent(this, _progress += 5);
            WriteMemoryBanks(tag, memoryBankIndices);
            SetPrrBitmask(tag, memoryBankIndices, 0);
            WaitForEndOfProgrammingMemoryBanks(tag, LongTimeout);
        }

        private void RepeatProgrammingInCaseOfError(Nfc2Tag tag)
        {
            if (tag.ControlRegister.Prr != 0)
            {
                int[] memoryBankIndices = GetMemoryIndicesFromBitMask(tag.ControlRegister.Prr);
                DoOnlineProgramming(tag, memoryBankIndices);
            }
            if (tag.ControlRegister.Prr != 0)
            {
                _lowLevelOperations.ReadStatusRegister(tag);
                throw new NfcReaderException($"Error programming memory banks. Prr: 0x{tag.ControlRegister.Prr:X4}");
            }
        }

        private void WriteMemoryBanks(Nfc2Tag tag, int[] memoryBankIndices)
        {
            log.Info("Write memory banks.");
            if (memoryBankIndices.Length > 0)
            {
                int increment = (int)Math.Round((decimal)(90 - _progress) / memoryBankIndices.Length);
                foreach (var memoryBankIndex in memoryBankIndices)
                {
                    if (ForceProgrammingWithOneTimeCrcError)
                        _lowLevelOperations.WriteMemoryBankWithCrc(tag, memoryBankIndex, 0);
                    else
                        _lowLevelOperations.WriteMemoryBank(tag, memoryBankIndex);

                    _progressEvent(this, _progress += increment);
                }
            } else
            {
                _progress = 90;
                _progressEvent(this, _progress);
            }
        }

        private void SetPrrBitmask(Nfc2Tag tag, int[] memoryBankIndices, UInt32 initialPrr)
        {
            UInt32 memoryBanks = GetUpdateBitField(memoryBankIndices);
            log.Info($"Write memory banks bit mask to PRR. Memory bank bit field = 0x{memoryBanks:X8}");
            tag.ControlRegister.Prr = initialPrr | memoryBanks;
            _lowLevelOperations.WriteControlRegister(tag);
        }

        /// <summary>
        /// Waits until the MLR is set to 0 or the timeout.
        /// </summary>
        /// <param name="tag"></param>
        /// <param name="timeout"></param>
        private void WaitForEndOfProgrammingMemoryBanks(Nfc2Tag tag, int timeout)
        {
            log.Info("Poll for end of programming (MLR = 0)");
            PollControlRegisters(tag, reg => reg.Mlr == 0, timeout);
        }

        private bool IsEcgOnline(Nfc2Tag tag)
        {
            _lowLevelOperations.ReadStatusRegister(tag);
            if (!tag.StatusRegister.CrcIsCorrect)
                throw new NfcReaderException("Read Status register failed.");
            return tag.StatusRegister.Ecg == StatusRegister.EcgAccess.EcgOn;
        }

        private UInt32 GetUpdateBitField(int[] memoryBankIndices)
        {
            UInt32 bitField = 0;
            foreach (var memoryBankIndex in memoryBankIndices)
            {
                bitField |= (UInt32)(1 << memoryBankIndex);
            }
            return bitField;
        }

        private int[] GetMemoryIndicesFromBitMask(uint bitMask)
        {
            List<int> indices = new List<int>(32);
            for (int i = 0; i < 32; i++)
            {
                if ((bitMask & 0x1) == 1)
                    indices.Add(i);
                bitMask = bitMask >> 1;
            }
            return indices.ToArray();
        }

        private void ForceMemoryBankUpdates(Nfc2Tag tag, UInt32 memoryBanks)
        {
            log.Info($"Request memory bank updates. Memory bank bit field = 0x{memoryBanks:X8}");
            log.Info("Wait for Urr = 0");
            if (!PollControlRegisters(tag, register => register.Urr == 0, ShortTimeout))
            {
                throw new TimeoutException("Timeout waiting for Urr = 0");
            }
            log.Info("Write Urr with the bits set for the requested memory banks");
            tag.ControlRegister.Urr = memoryBanks;
            _lowLevelOperations.WriteControlRegister(tag);
            log.Info("Wait for Urr = 0 after MCU update");
            if (!PollControlRegisters(tag, register => register.Urr == 0, LongTimeout))
            {
                throw new TimeoutException("Timeout waiting for Urr = 0 after MCU update");
            }
            log.Info("Update request succesful");
        }

        private void WaitUntilReadyForDataTransfer(Nfc2Tag tag)
        {
            log.Info("Wait for Prr = Urr = Mlr = 0");
            if (!PollControlRegisters(tag, register => (register.Urr == 0) && (register.Prr == 0) && (register.Mlr == 0), ShortTimeout))
            {
                throw new TimeoutException("Timeout waiting for Prr = Urr = Mlr = 0");
            }
        }

        private void WaitForProgrammingPermission(Nfc2Tag tag)
        {
            log.Info("Request write memory bank permission. ");
            byte challenge = GetChallengeValue();
            log.Info($"Write random to MLR = {challenge:X2}");
            tag.ControlRegister.Mlr = challenge;
            _lowLevelOperations.WriteControlRegister(tag);
            byte expectedReturnChallenge = (byte)((challenge + 127) & 0xFF);
            log.Info($"Wait for MLR = {expectedReturnChallenge:X2}");
            if (!PollControlRegisters(tag, register => register.Mlr == expectedReturnChallenge, ShortTimeout))
            {
                throw new TimeoutException($"Timeout waiting for Mlr = {expectedReturnChallenge:X2}");
            }
            log.Info("Write memory bank permission granted.");
        }

        private byte GetChallengeValue()
        {
            byte challenge = _randomByteService.GetRandomByte();
            if (challenge == 129)       // do not return 129 as challenge as the ECG adds 127 and then it returns 0 indicating end of operation
                challenge++;
            return challenge;
        }

        /// <summary>
        /// Reads the control register until the endCondition is met.
        /// </summary>
        /// <param name="tag">The Nfc tag</param>
        /// <param name="endCondition">Check for the end condition</param>
        /// <param name="timeout">The timeout in ms</param>
        /// <returns></returns>
        private bool PollControlRegisters(Nfc2Tag tag, Func<ControlRegister, bool> endCondition, int timeout)
        {
            DateTime startTime = _timeService.GetCurrentTime();
            do
            {
                _lowLevelOperations.ReadControlRegister(tag);
                if (tag.ControlRegister.CrcIsCorrect && endCondition(tag.ControlRegister))
                    return true;
            } while ((_timeService.GetCurrentTime() - startTime).TotalMilliseconds < timeout);
            return false;
        }
    }
}
