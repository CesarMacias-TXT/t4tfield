﻿using System;
using Osram.TFTBackend.Nfc2.TagStructures;
using Osram.TFTBackend.NfcService.Contracts;
using Osram.TFTBackend.NfcService.NfcDataTypes;

namespace Osram.TFTBackend.Nfc2.Tag.Operations
{
    /// <summary>
    /// Read and write operations for the individual tag structures.
    /// </summary>
    public class LowLevelOperations
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger();

        private INfcReader _reader;

        public LowLevelOperations(INfcReader reader)
        {
            _reader = reader;
        }

        #region Read operations
        public bool ReadDeviceIdentificationRegisters(Nfc2Tag tag)
        {
            try
            {
                log.Info("Read device identification registers");
                ByteArray data = _reader.Read(MemoryConstants.DeviceIdentificationRegistersStartAddress,
                    MemoryConstants.DeviceIdentificationRegistersSize);
                if (data.Count == MemoryConstants.DeviceIdentificationRegistersSize)
                {
                    tag.DeviceIdentificationRegisters = new DeviceIdentificationRegisters(data);
                    return true;
                }
            }
            catch (Exception e)
            {
                log.Error(e);
            }
            tag.DeviceIdentificationRegisters = new DeviceIdentificationRegisters();
            return false;
        }

        public bool ReadTableOfContent(Nfc2Tag tag)
        {
            try
            {
                log.Info("Read table of content");
                int memoryBankCount = tag.DeviceIdentificationRegisters.MemoryBankCount;
                int memorySize = memoryBankCount * MemoryConstants.TocEntrySize + 2;     // add two CRC bytes
                if (IsMemoryValid(MemoryConstants.TocStartAddress, memorySize))
                {
                    ByteArray data = _reader.Read(MemoryConstants.TocStartAddress, memorySize);
                    if (data.Count == memorySize)
                    {
                        tag.TableOfContent = new TableOfContent(data);
                        return true;
                    }
                }
            }
            catch (Exception e)
            {
                log.Error(e);
            }
            tag.TableOfContent = new TableOfContent();
            return false;
        }

        public bool ReadStatusRegister(Nfc2Tag tag)
        {
            try
            {
                log.Info("Read status registers");
                int statusRegisterStartAddress = tag.DeviceIdentificationRegisters.StatusRegisterStartAddress;
                if (IsMemoryValid(statusRegisterStartAddress, MemoryConstants.StatusRegisterSize))
                {
                    ByteArray data = _reader.Read(statusRegisterStartAddress, MemoryConstants.StatusRegisterSize);
                    if (data.Count == MemoryConstants.StatusRegisterSize)
                    {
                        tag.StatusRegister = new StatusRegister(data);
                        return true;
                    }
                }
            }
            catch (Exception e)
            {
                log.Error(e);
            }
            tag.StatusRegister = new StatusRegister();
            return false;
        }

        public bool ReadControlRegister(Nfc2Tag tag)
        {
            try
            {
                log.Info("Read control registers");
                int controlRegisterStartAddress = tag.DeviceIdentificationRegisters.ControlRegisterStartAddress;
                if (IsMemoryValid(controlRegisterStartAddress, MemoryConstants.ControlRegisterSize))
                {
                    ByteArray data = _reader.Read(controlRegisterStartAddress, MemoryConstants.ControlRegisterSize);
                    if (data.Count == MemoryConstants.ControlRegisterSize)
                    {
                        tag.ControlRegister = new ControlRegister(data);
                        return true;
                    }
                }
            }
            catch (Exception e)
            {
                log.Error(e);
            }
            tag.ControlRegister = new ControlRegister();
            return false;
        }

        public void ReadMemoryBank(Nfc2Tag tag, int tocIndex)
        {
            log.Info($"Read memory bank with index = {tocIndex}");
            if ((tocIndex < 0) || (tocIndex >= tag.TableOfContent.ContentTable.Count))
            {
                log.Error("ReadMemoryBank: Index out of range");
                return;
            }
            TableOfContent.TocEntry tocEntry = tag.TableOfContent.ContentTable[tocIndex];
            tag.MemoryBanks[tocIndex] = ReadMemoryBank(tocEntry);
        }

        public MemoryBank ReadMemoryBank(TableOfContent.TocEntry tocEntry)
        {
            try
            {
                if (IsNoCrcMemoryBank(tocEntry))
                {
                    ByteArray data = ReadMemoryBankData(tocEntry.MbBaseAddress, tocEntry.MbLength);
                    if (data != null)
                    {
                        ushort crc = CrcValidator.CalcCrc16CcittFalse(data, data.Count);
                        data.AddUshort(crc);
                        return new MemoryBank(data);
                    }
                }
                else
                {
                    ByteArray data = ReadMemoryBankData(tocEntry.MbBaseAddress, tocEntry.MbLength + 2); // read the 2 crc bytes too
                    if (data != null)
                        return new MemoryBank(data);
                }
            }
            catch (Exception e)
            {
                log.Error(e);
            }
            return new MemoryBank();
        }

        private ByteArray ReadMemoryBankData(int startAddress, int len)
        {
            if (IsMemoryValid(startAddress, len))
            {
                ByteArray data = _reader.Read(startAddress, len);
                if (data.Count == len)
                    return data;
            }
            return null;
        }

        private bool IsNoCrcMemoryBank(TableOfContent.TocEntry tocEntry)
        {
            return (tocEntry.MbAttribut & 0x10) == 0x10;
        }
        #endregion

        #region Write operations

        public void WriteControlRegister(Nfc2Tag tag)
        {
            log.Info("Write control registers");
            ByteArray data = tag.ControlRegister.GetMemoryArea();
            _reader.Write(tag.DeviceIdentificationRegisters.ControlRegisterStartAddress, data);
            tag.ControlRegister = new ControlRegister(data);
            tag.ControlRegister.IsModified = false;
        }

        public void WriteControlRegisterWithCrc(Nfc2Tag tag, ushort crc)
        {
            log.Info("Write control registers with given crc");
            ByteArray data = tag.ControlRegister.GetMemoryArea();
            data.SetUshort(data.Count - 2, crc);
            _reader.Write(tag.DeviceIdentificationRegisters.ControlRegisterStartAddress, data);
            tag.ControlRegister = new ControlRegister(data);
            tag.ControlRegister.IsModified = false;
        }

        public void WriteMemoryBank(Nfc2Tag tag, int tocIndex)
        {
            log.Info($"Write memory bank index = {tocIndex}");
            if ((tocIndex < 0) || (tocIndex >= tag.TableOfContent.ContentTable.Count))
            {
                log.Error("WriteMemoryBank: Index out of range");
                return;
            }
            TableOfContent.TocEntry tocEntry = tag.TableOfContent.ContentTable[tocIndex];
            WriteMemoryBank(tocEntry, tag.MemoryBanks[tocIndex]);
        }

        public void WriteMemoryBankWithCrc(Nfc2Tag tag, int tocIndex, ushort crc)
        {
            log.Info($"Write memory bank with CRC index = {tocIndex}");
            if ((tocIndex < 0) || (tocIndex >= tag.TableOfContent.ContentTable.Count))
            {
                log.Error("WriteMemoryBank: Index out of range");
                return;
            }
            TableOfContent.TocEntry tocEntry = tag.TableOfContent.ContentTable[tocIndex];
            WriteMemoryBankWithCrc(tocEntry, tag.MemoryBanks[tocIndex], crc);
        }

        #endregion

        private void WriteMemoryBank(TableOfContent.TocEntry tocEntry, MemoryBank memoryBank)
        {
            if (tocEntry.MbLength != memoryBank.Data.Count)
                return;
            if (IsNoCrcMemoryBank(tocEntry))
            {
                ByteArray data = memoryBank.GetMemoryArea();
                WriteMemoryBankData(tocEntry.MbBaseAddress, data.Left(data.Count - 2)); // remove CRC
            }
            else
            {
                WriteMemoryBankData(tocEntry.MbBaseAddress, memoryBank.GetMemoryArea());
            }
                memoryBank.IsModified = false;
            }

        private void WriteMemoryBankData(int startAddress, ByteArray data)
        {
            if (IsMemoryValid(startAddress, data.Count))
            {
                _reader.Write(startAddress, data);
            }
        }

        private void WriteMemoryBankWithCrc(TableOfContent.TocEntry tocEntry, MemoryBank memoryBank, ushort crc)
        {
            if (tocEntry.MbLength != memoryBank.Data.Count)
                return;
            if (IsNoCrcMemoryBank(tocEntry))
            {
                WriteMemoryBank(tocEntry, memoryBank);
            }
            else
            {
                ByteArray data = memoryBank.GetMemoryArea();
                data.SetUshort(data.Count - 2, crc);
                WriteMemoryBankData(tocEntry.MbBaseAddress, data);
                memoryBank.IsModified = false;
            }
        }

        private bool IsMemoryValid(int startAddress, int memorySize)
        {
            if (startAddress + memorySize < MemoryConstants.TagSize)
                return true;
            log.Error($"Invalid memory access: Address = 0x{startAddress} Len = {memorySize}");
            return false;
        }
    }
}
