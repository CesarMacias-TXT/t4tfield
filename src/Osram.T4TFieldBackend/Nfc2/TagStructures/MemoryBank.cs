﻿using Osram.TFTBackend.Nfc2.Contracts;
using Osram.TFTBackend.NfcService.NfcDataTypes;

namespace Osram.TFTBackend.Nfc2.TagStructures
{
    public class MemoryBank : CrcValidator, IWritable
    {
        public bool IsModified { get; set; }

        private ByteArray _data;

        public ByteArray Data
        {
            get { return _data; }
            set
            {
                if (_data != value)
                {
                    IsModified = true;
                    _data = value;
                }
            }
        }

        public MemoryBank()
        {
            Data = new ByteArray();
            IsModified = false;
        }

        public MemoryBank(ByteArray data) : base(data)
        {
            Data = data.Left(data.Count - 2);
            IsModified = false;
        }

        public ByteArray GetMemoryArea()
        {
            ByteArray data = new ByteArray(Data);
            ushort crc = CalcCrc16CcittFalse(data, data.Count);
            data.AddUshort(crc);
            return data;
        }

        public MemoryBank Clone()
        {
            return new MemoryBank()
            {
                Data = new ByteArray(this.Data)
            };
        }

        public bool IsDataEmpty() => _data.Count > 0 ? false : true;
    }
}
