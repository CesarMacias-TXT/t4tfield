﻿using Osram.TFTBackend.NfcService.NfcDataTypes;

namespace Osram.TFTBackend.Nfc2.TagStructures
{
    public class DeviceIdentificationRegisters : CrcValidator
    {
        public byte ManufacturerCode { get; private set; }
        public long Gtin { get; private set; }
        public byte FwMajorVersion { get; private set; }
        public byte FwMinorVersion { get; private set; }
        public byte HwMajorVersion { get; private set; }
        public byte NfcMajorVersion { get; private set; }
        public byte MemoryBankCount { get; private set; }
        public ushort StatusRegisterStartAddress { get; private set; }
        public ushort ControlRegisterStartAddress { get; private set; }
        public ushort ProtectedMemoryAreaStartAddress { get; private set; }

        public DeviceIdentificationRegisters() : base()
        {
        }

        public DeviceIdentificationRegisters(ByteArray data) : base(data)
        {
            InterpretData(data);
        }

        private void InterpretData(ByteArray data)
        {
            ManufacturerCode = data[0];
            Gtin = GetGtin(data.Mid(1, 6));
            FwMajorVersion = data[7];
            FwMinorVersion = data[8];
            HwMajorVersion = data[9];
            NfcMajorVersion = data[0x0A];
            MemoryBankCount = data[0x0B];
            StatusRegisterStartAddress = data.GetUshort(0x0C);
            ControlRegisterStartAddress = data.GetUshort(0x0E);
            ProtectedMemoryAreaStartAddress = data.GetUshort(0x10);
        }

        private long GetGtin(ByteArray data)
        {
            // MSB first
            long gtin = 0;
            for (int i = 0; i < data.Count; i++)
            {
                gtin = gtin * 0x100 + data[i];
            }
            return gtin;
        }
    }
}