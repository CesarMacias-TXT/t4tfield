﻿using System;
using Osram.TFTBackend.Nfc2.Contracts;
using Osram.TFTBackend.NfcService.NfcDataTypes;

namespace Osram.TFTBackend.Nfc2.TagStructures
{
    public class ControlRegister : CrcValidator, IWritable
    {
        private UInt32 _prr;
        /// <summary>
        /// Programming request register
        /// </summary>
        public UInt32 Prr
        {
            get { return _prr; }
            set
            {
                if (_prr != value)
                {
                    IsModified = true;
                    _prr = value;
                }
            }
        }

        private UInt32 _urr;
        /// <summary>
        /// Update request register
        /// </summary>
        public UInt32 Urr
        {
            get { return _urr; }
            set
            {
                if (_urr != value)
                {
                    IsModified = true;
                    _urr = value;
                }
            }
        }

        private byte _mlr;
        /// <summary>
        /// Memory lock register
        /// </summary>
        public byte Mlr
        {
            get { return _mlr; }
            set
            {
                if (_mlr != value)
                {
                    IsModified = true;
                    _mlr = value;
                }
            }
        }

        public bool IsModified { get; set; }

        public ControlRegister() : base()
        {
        }

        public ControlRegister(ByteArray data) : base(data)
        {
            InterpretData(data);
        }

        private void InterpretData(ByteArray data)
        {
            Prr = data.GetUInt32(0);
            Urr = data.GetUInt32(4);
            Mlr = data[8];
        }

        public ByteArray GetMemoryArea()
        {
            ByteArray data = new ByteArray();
            data.AddUInt32(Prr);
            data.AddUInt32(Urr);
            data.Add(Mlr);
            CalculatedCrc = CalcCrc16CcittFalse(data, data.Count);
            data.AddUshort(CalculatedCrc);
            return data;
        }
    }
}
