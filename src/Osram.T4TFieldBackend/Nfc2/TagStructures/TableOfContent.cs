﻿using System.Collections.Generic;
using Osram.TFTBackend.Nfc2.Tag;
using Osram.TFTBackend.NfcService.NfcDataTypes;

namespace Osram.TFTBackend.Nfc2.TagStructures
{
    public class TableOfContent : CrcValidator
    {
        public class TocEntry
        {
            public byte MbAttribut { get; set; }
            public byte MpcId { get; set; }
            public byte MpcVersion { get; set; }
            public byte MbLength { get; set; }
            public ushort MbBaseAddress { get; set; }

            public override string ToString()
            {
                return $"Memory bank Attribut={MbAttribut} Id={MpcId} Version={MpcVersion} Length={MbLength}, Address={MbBaseAddress:X4}";
            }
        }

        private List<TocEntry> _contentTable;
        public List<TocEntry> ContentTable => _contentTable;

        public TableOfContent()
        {
            _contentTable = new List<TocEntry>();
        }

        public TableOfContent(ByteArray data) : base(data)
        {
            InterpretData(data);
        }

        private void InterpretData(ByteArray data)
        {
            int tocSize = data.Count / MemoryConstants.TocEntrySize;
            _contentTable = new List<TocEntry>(tocSize);
            for (int i = 0; i < tocSize; i++)
            {
                _contentTable.Add(GetTocEntry(data, i * MemoryConstants.TocEntrySize));
            }
        }

        private TocEntry GetTocEntry(ByteArray data, int startIndex)
        {
            TocEntry entry = new TocEntry();
            entry.MbAttribut = data[startIndex + 0];
            entry.MpcId = data[startIndex + 1];
            entry.MpcVersion = data[startIndex + 2];
            entry.MbLength = data[startIndex + 3];
            entry.MbBaseAddress = data.GetUshort(startIndex + 4);
            return entry;
        }
    }
}
