﻿using Osram.TFTBackend.NfcService.NfcDataTypes;

namespace Osram.TFTBackend.Nfc2.TagStructures
{
    public class StatusRegister : CrcValidator
    {
        public enum EcgAccess
        {
            NotRead,
            EcgOff,
            EcgOn,
            InvalidState
        }

        public EcgAccess Ecg { get; private set; }
        public byte Error { get; private set; }
        public byte Reserved { get; private set; }

        public StatusRegister() : base()
        {
        }

        public StatusRegister(ByteArray data) : base(data)
        {
            InterpretData(data);
        }

        private void InterpretData(ByteArray data)
        {
            Ecg = GetStatusEcg(data[0]);
            Error = GetStatusError(data[0]);
            Reserved = data[1];
        }

        private EcgAccess GetStatusEcg(byte status)
        {
            if ((status & 0xF0) == 0xA0)
                return EcgAccess.EcgOn;
            if ((status & 0xF0) == 0x00)
                return EcgAccess.EcgOff;
            return EcgAccess.InvalidState;
        }

        private byte GetStatusError(byte status)
        {
            return (byte)(status & 0x0F);
        }

    }
}
