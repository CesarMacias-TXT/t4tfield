﻿using System.IO;

namespace Osram.TFTBackend.CompressionService.Contracts
{
    public interface ICompressionService
    {
        /// <summary>
        /// Decompress will extract and assign streams-to-files to individual files of interest (xml configuration and device picture) contained in the archive.
        /// </summary>
        /// <param name="streamToDecompress"></param>
        /// <returns>A boolean to indicate if the underlying individual streams-to-files contained in the compressed stream were successfully assigned.</returns>
        bool Decompress(Stream streamToDecompress);

        Stream Decompress(string fileName);

        void Compress(Stream inputStream);

        void Compress(string bigString);

        void Compress(byte[] bigBlob);

        Stream DecompressedXmlStream { get; set; }
        Stream DecompressedPictureStream { get; set; }
    }
}