﻿using System;
using System.IO;
using ICSharpCode.SharpZipLib.Zip;
using MvvmCross;
using MvvmCross.Base;
using MvvmCross.Plugin.File;
using Osram.TFTBackend.CompressionService.Contracts;
using ZipFile = ICSharpCode.SharpZipLib.Zip.ZipFile;

namespace Osram.TFTBackend.CompressionService
{
    public class CompressionService : ICompressionService
    {
        private IMvxFileStore _fileStore;
        
        public CompressionService()
        {
            _fileStore = Mvx.IoCProvider.Resolve<IMvxFileStore>();
            InitializeDecompressed();
        }

        public void Import(Stream configurationFile)
        {
            _fileStore = Mvx.IoCProvider.Resolve<IMvxFileStore>();
            InitializeDecompressed();
        }

        private void InitializeDecompressed()
        {
            DecompressedXmlStream = Stream.Null;
            DecompressedPictureStream = Stream.Null;
        }

        public bool Decompress(Stream streamToDecompress)
        {
            InitializeDecompressed();

            try
            {
                MemoryStream streamToDecompressInMemory = new MemoryStream();
                streamToDecompress.CopyTo(streamToDecompressInMemory);

                ZipFile zf = new ZipFile(streamToDecompressInMemory);
                foreach (ZipEntry zipEntry in zf)
                {
                    if (!zipEntry.IsFile)
                    {
                        continue; // ignore directories
                    }

                    string entryFileName = zipEntry.Name;

                    Stream zipStream = zf.GetInputStream(zipEntry);

                    // File not utilizable
                    if (entryFileName.Contains("LuminaireResults")
                            || entryFileName.Contains("[Content_Types]")
                            || entryFileName.Contains("LuminaireDemoImage")
                            || entryFileName.Contains("report"))
                    {
                        continue;
                    }

                    if (entryFileName.Contains(".xml"))
                    {
                        DecompressedXmlStream = zipStream;
                    }
                    else if (entryFileName.Contains(".png"))
                    {
                        DecompressedPictureStream = zipStream;
                    }
                    /*
                    // TODO: review logic of finding the 'right' XML file in the zipped archive
                    if (entryFileName.Contains("LuminarieProduction"))
                    {
                        DecompressedXmlStream = zipStream;
                    }
                    else
                    {
                        if(!entryFileName.Contains("LuminaireResults") && !entryFileName.Contains("[Content_Types]"))
                        {
                            if (entryFileName.Contains("png") && !entryFileName.Contains("LuminaireDemoImage"))
                            {
                                DecompressedPictureStream = zipStream;
                            }
                            else
                            {
                                DecompressedXmlStream = zipStream;
                            }
                        }
                    }
                    */
                }
                if (DecompressedXmlStream != Stream.Null && DecompressedPictureStream != Stream.Null)
                {
                    // reading the XmlStream will move the position to the end
                    // and there's no way to reset it since it's a non-seekable stream
                    //CheckAndCorrectXmlEncoding(DecompressedXmlStream);
                    return true;
                }
                    
            }
            catch (Exception e)
            {
                Logger.Trace(e.Message);
                throw;
            }

            return false;
        }

        public Stream Decompress(string fileName)
        {
            if (_fileStore != null)
            {
                string nativePath = _fileStore.NativePath("Files");
                
                var fullPath = _fileStore.PathCombine(nativePath, fileName);
                var pathInAssets = _fileStore.PathCombine("Files", fileName);

                // TODO: instead of opening a direct stream to the file, must still load it from Assets
                // var fileStream = new FileStream(fullPath, FileMode.Open, FileAccess.Read);
                if (!_fileStore.TryReadBinaryFile(fullPath, LoadFrom))
                {
                    var resourceLoader = Mvx.IoCProvider.Resolve<IMvxResourceLoader>();
                    if (resourceLoader.ResourceExists(pathInAssets))
                    {
                        resourceLoader.GetResourceStream(pathInAssets, (streamToAssetFile) => LoadFrom(streamToAssetFile));
                    }
                }
                return DecompressedXmlStream;
            }

            return Stream.Null;
        }

        private bool LoadFrom(Stream streamToAssetFile)
        {
            var result = false;
            try
            {
                // TODO: stream from asset file is not seekable, must copy out first;
                // TODO: alternatively, try using ZipInputStream, it can read from unseekable input streams (like web download streams)
                MemoryStream assetFileInMemory = new MemoryStream();
                streamToAssetFile.CopyTo(assetFileInMemory);

                ZipFile zf = new ZipFile(assetFileInMemory);
                foreach (ZipEntry zipEntry in zf)
                {
                    if (!zipEntry.IsFile)
                    {
                        continue; // ignore directories
                    }

                    string entryFileName = zipEntry.Name;

                    Stream zipStream = zf.GetInputStream(zipEntry);


                    //var decompressedStream = new MemoryStream();

                    //var decompressor = new DeflateStream(zipStream, CompressionMode.Decompress);
                    //decompressor.CopyTo(decompressedStream);

                   // _decompressedXmlStream = decompressedStream;
                    DecompressedXmlStream = zipStream;
                    result = true;
                }
            }

            catch (Exception e)
            {
                Logger.Trace(e.Message);
                throw;
            }
            return result;
        }

        public void Compress(Stream inputStream)
        {
            throw new NotImplementedException();
        }

        public void Compress(string bigString)
        {
            throw new NotImplementedException();
        }

        public void Compress(byte[] bigBlob)
        {
            throw new NotImplementedException();
        }

        public Stream DecompressedXmlStream { get; set; }
        public Stream DecompressedPictureStream { get; set; }
    }
}
