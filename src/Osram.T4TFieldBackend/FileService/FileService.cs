﻿using System;
using System.IO;
using System.Linq;
using MvvmCross.Base;
using MvvmCross.Plugin.File;
using Osram.TFTBackend.FileService.Contracts;

namespace Osram.TFTBackend.FileService
{
    public class FileService : IFileService
    {
        private IMvxFileStore _fileStore;
        private IMvxResourceLoader _resourceLoader;
        public FileService(IMvxFileStore fileStore, IMvxResourceLoader resourceLoader)
        {
            _fileStore = fileStore;
            _resourceLoader = resourceLoader;
        }
        
        /// <summary>
        /// The buffer size; 4K is optimal.
        /// </summary>
        private const long Buffersize = 4096;
        public void CopyFile(Stream inputStream, Stream outputStream)
        {
            inputStream.CopyTo(outputStream);
        }

        public string CombinePath(params string[] values)
        {
            if (values.Length == 0)
            {
                return "";
            }
            string returnValue = values[0];
            for (int i = 1; i < values.Length; i++)
            {
                returnValue = _fileStore.PathCombine(returnValue, values[i]);
            }
            return returnValue;
        }

        public void EnsureFolderExists(string resourceName)
        {
            _fileStore.EnsureFolderExists(resourceName);
        }

        public void CopyResource(string resourceName, Stream outStream)
        {
            if (_resourceLoader.ResourceExists(resourceName))
            {
                _resourceLoader.GetResourceStream(resourceName, (inputStream) => CopyFile(inputStream, outStream));
            }
        }

        public void CopyResource(string resourceName, string fileName)
        {
            if (_resourceLoader.ResourceExists(resourceName))
            {
                Stream outStream = _fileStore.OpenWrite(fileName);
                _resourceLoader.GetResourceStream(resourceName, (inputStream) => CopyFile(inputStream, outStream));
                outStream.Dispose();
            }
        }

        public string ReadTextFile(string fileName)
        {
            if (!_fileStore.Exists(fileName))
            {
                Logger.Error($"FileService.ReadTextFile: File {fileName} does not exist!");
                return string.Empty;
            }
            string content;
            if (_fileStore.TryReadTextFile(fileName, out content))
            {
                return content;
            }
            Logger.Error($"FileService.ReadTextFile: Error reading file {fileName}!");
            return string.Empty;
        }

        public byte[] ReadBinaryFile(string fileName)
        {
            if (!_fileStore.Exists(fileName))
            {
                Logger.Error($"FileService.ReadBinaryFile: File {fileName} does not exist!");
                return new byte[0];
            }
            byte[] content;
            if (_fileStore.TryReadBinaryFile(fileName, out content))
            {
                return content;
            }
            Logger.Error($"FileService.ReadBinaryFile: Error reading file {fileName}!");
            return new byte[0];
        }

        public void CopyStreamToFile(Stream inStream, string fileName)
        {
            Stream outStream = _fileStore.OpenWrite(fileName);
            CopyFile(inStream, outStream);
            outStream.Dispose();
        }

        public Stream GetStreamFromFile(string fileName)
        {
            return _fileStore.OpenRead(fileName);
        }

        public void DeleteAllConfigurationFiles(string folderPath)
        {
            var files = _fileStore.GetFilesIn(folderPath).Where(file => (file.EndsWith(".xml", StringComparison.CurrentCultureIgnoreCase) 
                                                                        || file.EndsWith(".png", StringComparison.CurrentCultureIgnoreCase)));
            foreach (var file in files)
            {
                _fileStore.DeleteFile(file);
            }
        }

        public void DeleteFile(string fileName)
        {
            _fileStore.DeleteFile(fileName);
        }
    }
}
