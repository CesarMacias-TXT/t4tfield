﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Osram.TFTBackend.FileService.Contracts
{
    public interface IFileService
    {
        /// <summary>
        /// Copies an instream to an outstream.
        /// </summary>
        /// <param name="inStream">The stream to be copied</param>
        /// <param name="outStream">The stream to be written</param>
        void CopyFile(Stream inStream, Stream outStream);

        /// <summary>
        /// Copies a resource asset to a stream.
        /// </summary>
        /// <param name="resourceName">The resource name</param>
        /// <param name="outStream">The stream to be written</param>
        void CopyResource(String resourceName, Stream outStream);

        /// <summary>
        /// Copies a resource to a file location.
        /// </summary>
        /// <param name="resourceName"></param>
        /// <param name="fileName"></param>
        void CopyResource(String resourceName, String fileName);

        /// <summary>
        /// Copies a stream to a file.
        /// </summary>
        /// <param name="inStream"></param>
        /// <param name="fileName"></param>
        void CopyStreamToFile(Stream inStream, String fileName);

        /// <summary>
        /// Get a stream from a file.
        /// </summary>
        /// <param name="fileName"></param>
        Stream GetStreamFromFile(String fileName);

        /// <summary>
        /// Reads a text file and returns the file content.
        /// </summary>
        /// <param name="fileName"></param>
        /// <returns></returns>
        String ReadTextFile(String fileName);

        /// <summary>
        /// Reads a binary file and returns the file content.
        /// </summary>
        /// <param name="fileName"></param>
        /// <returns></returns>
        byte[] ReadBinaryFile(String fileName);

        /// <summary>
        /// Delete all files in the given directory.
        /// </summary>
        /// <param name="folderPath"></param>
        void DeleteAllConfigurationFiles(String folderPath);

        /// <summary>
        /// Deletes a single file.
        /// </summary>
        /// <param name="fileName">The filename and path of the file to be deleted.</param>
        void DeleteFile(String fileName);

        /// <summary>
        /// Combine multiple string to a Path
        /// </summary>
        /// <param name="values">The values to combine</param>
        string CombinePath(params string[] values);

        /// <summary>
        /// Check if folder exist, if not, creates the folder
        /// </summary>
        /// <param name="resourceName">The folder to check</param>
        void EnsureFolderExists(string resourceName);
    }
}
