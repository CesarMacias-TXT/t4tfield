﻿using Plugin.BLE.Abstractions.Contracts;
using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Osram.TFTBackend.BluetoothService.Exceptions;
using Osram.TFTBackend.NfcService.Contracts;
using Osram.TFTBackend.NfcService.ISOStandard;
using Osram.TFTBackend.NfcService.NfcDataTypes;
using Plugin.BLE.Abstractions.EventArgs;
using System.Collections;
using Plugin.BLE.Abstractions;
using Osram.TFTBackend.BluetoothService.Contracts;
using Osram.TFTBackend.Tertium;
using Plugin.BLE.Abstractions.Exceptions;

namespace Osram.TFTBackend.BluetoothService
{
    public class BluetoothNfcReader : IBluetoothReader, INfcCommunication
    {
        private readonly Plugin.BLE.Abstractions.Contracts.IAdapter _adapter;
        private readonly IBluetoothLE _bluetoothLe;
        private string tagId;

        private TertiumLib lib;
        private TimeSpan waitTimeForScan = new TimeSpan(0, 0, 20);
        private TimeSpan waitTimeForBluetoothResponse = new TimeSpan(0, 0, 2);
        private int RetryBluetoothTimeout = 2;

        IResponseQueue _responseQueue;
        IService BTservice;
        ICharacteristic WriteCharacteristic;
        ICharacteristic ReadCharacteristic;
        private AutoResetEvent resetEvent = new AutoResetEvent(false);

        public BluetoothNfcReader(IBluetoothLE ble, IResponseQueue response)
        {
            _bluetoothLe = ble;
            _responseQueue = response;
            _adapter = ble.Adapter;

            _adapter.DeviceDiscovered += (sender, args) => { RaiseDeviceDiscovered(args.Device); };
            _adapter.DeviceDisconnected += (sender, args) => { OnDeviceDisconnected(args.Device); };
            _adapter.DeviceConnected += (sender, args) => { OnDeviceConnected(args.Device); };
            _adapter.DeviceConnectionLost += (sender, args) => { OnDeviceConnectionLost(args.Device); };
            _adapter.ScanTimeoutElapsed += (sender, args) => { OnScanTimeoutElapsed(); };

            // _adapter.ScanTimeoutElapsed += (sender, args) => { PublishScanTimeoutElapsed(true); };
            lib = new TertiumLib();

            _bluetoothLe.StateChanged += _bluetoothLe_StateChanged;
        }

        private void _bluetoothLe_StateChanged(object sender, BluetoothStateChangedArgs e)
        {
            Logger.TaggedTrace("BluetoothNfcReader", "_bluetoothLe_StateChanged", e.NewState);
            if (e.NewState == BluetoothState.On)
            {
                AdapterConnected?.Invoke(this, null);
            }
            else if (e.NewState == BluetoothState.Off)
            {
                AdapterDisconnected?.Invoke(this, null);
            }
        }

        #region [ Events ]

        public event EventHandler<IDevice> DeviceDiscovered;
        public event EventHandler<IDevice> DeviceDisconnected;
        public event EventHandler<IDevice> DeviceConnected;
        public event EventHandler<IDevice> DeviceLost;
        public event EventHandler<IDevice> DeviceScanTimeoutElapsed;
        public event EventHandler<IDevice> DeviceConnectionTimeoutElapsed;
        public event EventHandler<EventArgs> AdapterConnected;
        public event EventHandler<EventArgs> AdapterDisconnected;
        public event EventHandler<string> TagDetected;
        public event EventHandler<EventArgs> TagUndetected;

        #endregion [ Events ]

        public bool IsDeviceScanning => _adapter.IsScanning;

        //   public bool IsDeviceConnected = false;

        public IDevice ConnectedDevice { get; set; }

        private void RaiseDeviceDiscovered(IDevice device)
        {
            Logger.TaggedTrace("BluetoothNfcReader " +  " RaiseDeviceDiscovered ", "");
            DeviceDiscovered?.Invoke(this, device);
        }

        private void OnDeviceDisconnected(IDevice device)
        {
            Logger.TaggedTrace("BluetoothNfcReader " +  " OnDeviceDisconnected " , "");
            DeviceDisconnected?.Invoke(this, device);
            IsDeviceConnected = false;
            ConnectedDevice = null;
        }

        private async void OnDeviceConnected(IDevice device)
        {
            Logger.TaggedTrace("BluetoothNfcReader " + " OnDeviceConnected " , "");
            ConnectedDevice = device;
            try
            {
                var res = device.UpdateConnectionInterval(ConnectionInterval.High);
            }
            catch(Exception e)
            {
                Logger.TaggedTrace("BluetoothNfcReader " + e.Message + " ", ""); 
            }
            IsDeviceConnected = true;
            DeviceConnected?.Invoke(this, device);
            await initBTService(device);
        }

        private async Task initBTService(IDevice device)
        {
            //service "175f8f23-a570-49bd-9627-815a6a27de2a"
            // write  "1cce1ea8-bd34-4813-a00a-c76e028fadcb"
            // read "cacc07ff-ffff-4c48-8fae-a9ef71b75e26"
            try
            {
                BTservice = await device.GetServiceAsync(new Guid("175f8f23-a570-49bd-9627-815a6a27de2a"));

                WriteCharacteristic = await BTservice.GetCharacteristicAsync(new Guid("1cce1ea8-bd34-4813-a00a-c76e028fadcb"));
                ReadCharacteristic = await BTservice.GetCharacteristicAsync(new Guid("cacc07ff-ffff-4c48-8fae-a9ef71b75e26"));

                await ReadCharacteristic.StartUpdatesAsync();
                ReadCharacteristic.ValueUpdated -= onReadValueUpdated;
                ReadCharacteristic.ValueUpdated += onReadValueUpdated;

            }
            catch (Exception ex)
            {
                Logger.TaggedError("BluetoothNfcReader ", ex.Message + " " , ex);
            }
        }

        private void onReadValueUpdated(object sender, CharacteristicUpdatedEventArgs ev)
        {
            Logger.TaggedTrace("BluetoothNfcReader " + "Read value received ", "");
            var resp = ev.Characteristic.Value;
            //add value received to the queue
            _responseQueue.Add(resp);
            //check if response is completed or it's only a chunk
            if (lib.IsResponseCompleted(resp))
            {
                _responseQueue.IsCompleted = true;
                resetEvent?.Set();
            }
        }

        private void OnDeviceConnectionLost(IDevice device)
        {
            Logger.TaggedTrace("BluetoothNfcReader " + " OnDeviceConnectionLost", "");
            IsDeviceConnected = false;
            ConnectedDevice = null;
            DeviceLost?.Invoke(this, device);
        }

        private void OnScanTimeoutElapsed()
        {
            Logger.TaggedTrace("BluetoothNfcReader " + "OnScanTimeoutElapsed", "");
            DeviceScanTimeoutElapsed?.Invoke(this, null);
        }

        #region Ble Start/Stop

        public async Task StartScanningForDevicesAsync()
        {
            Logger.TaggedTrace("BluetoothNfcReader", "StartScanningForDevicesAsync");
            await _adapter.StartScanningForDevicesAsync(null, (dev) => FilterTertiumDevices(dev));
        }

        public async Task StopScanningForDevicesAsync()
        {
            Logger.TaggedTrace("BluetoothNfcReader", "StopScanningForDevicesAsync");
            await _adapter.StopScanningForDevicesAsync();
        }

        private bool FilterTertiumDevices(IDevice dev)
        {
            return dev.AdvertisementRecords.Any(r => r.Type == AdvertisementRecordType.UuidsComplete128Bit &&
            StructuralComparisons.StructuralEqualityComparer.Equals(r.Data, lib.GetFilterTertiumUID()));
        }

        #endregion Ble Start/Stop


        #region BLE connect/disconect

        public async Task ConnectDeviceAsync(IDevice device)
        {
            Logger.TaggedTrace("BluetoothNfcReader " +  "ConnectDeviceAsync", "");
            await StopScanningForDevicesAsync();
            await DisconnectDeviceAsync(ConnectedDevice);
            try
            {
                //Already connected to BLE
                if (IsConnectedDevice(device))
                {
                    OnDeviceConnected(device);
                }
                else
                {
                    // At the moment the library is no supporting the cancellationToken in the right way,
                    // the OperationCanceledException is launched correctly but the _adapter is no longer awailable
                    // until the internal timer of 30 seconds of ConnectToDeviceAsync is finished
                    //CancellationToken cancellationToken = new CancellationTokenSource(waitTimeForBluetoothResponse).Token;
                    await _adapter.ConnectToDeviceAsync(device);//, cancellationToken: cancellationToken);
                }
            }
            catch (TimeoutException)
            {
                Logger.TaggedTrace("BluetoothNfcReader " +  "ConnectDeviceAsync timeout", "");
                DeviceConnectionTimeoutElapsed?.Invoke(this, device);
            }
            catch (DeviceConnectionException)
            {
                Logger.TaggedTrace("BluetoothNfcReader " + "ConnectDeviceAsync DeviceConnectionException", "");
                DeviceConnectionTimeoutElapsed?.Invoke(this, device);
            }
            //catch (OperationCanceledException)
            //{
            //    Logger.TaggedTrace("BluetoothNfcReader", "ConnectDeviceAsync OperationCanceledException");
            //    await _adapter.DisconnectDeviceAsync(device);
            //    DeviceConnectionTimeoutElapsed?.Invoke(this, device);
            //}
            catch (Exception exception)
            {
                Logger.TaggedError("BluetoothNfcReader " + exception.Message, "" );
            }
        }


        public async Task DisconnectDeviceAsync(IDevice device)
        {
            Logger.TaggedTrace("BluetoothNfcReader " + "DisconnectDeviceAsync", "");
            await StopScanningForDevicesAsync();
            try
            {
                if (device != null)
                    await _adapter.DisconnectDeviceAsync(device);
            }
            catch (Exception exception)
            {
                Logger.TaggedError("BluetoothNfcReader " + exception.Message, "");
            }
        }



        public async Task ConnectToKnownDeviceAsync(Guid guid)
        {
            Logger.TaggedTrace("BluetoothNfcReader " + "ConnectToKnownDeviceAsync" + " " , "");
            await StopScanningForDevicesAsync();
            try
            {
                await _adapter.ConnectToKnownDeviceAsync(guid);
            }
            catch (Exception exception)
            {
                Logger.TaggedError("BluetoothNfcReader " + exception.Message + " " , "" );
            }
        }




        #endregion Ble Connect/disconnect

        public string DeviceId => ConnectedDevice?.Id.ToString();

        public bool IsAvailable => _bluetoothLe.IsAvailable;

        public bool IsEnabled => _bluetoothLe.IsOn;

        public string ReaderName => ConnectedDevice?.Name;

        public NfcReaderType ReaderType { get; } = NfcReaderType.Bluetooth;

        public int RetryCount { get; set; }

        public bool IsDeviceConnected { get; set; }

        //TODO
        public bool Connect()
        {
            return true;
        }

        public async Task DetectTag()
        {
            Logger.TaggedTrace("BluetoothNfcReader" + " DetectTag " , "");
            if (Connect())
            {
                await Task.Run( () => ScanTagAsync());
            }
            await Task.FromResult(false);
        }

        public void Disconnect()
        {
            //do nothing? or disconnect the tertium device?
        }

        public ByteArray SendCommand(ByteArray command)
        {
            Logger.TaggedTrace("BluetoothNfcReader"+ $" BluetoothNfcReader.SendCommand Cmd={command} ", "");

            ByteArray response;
            int count = RetryCount;
            Exception lastException = null;
            do
            {
                count--;
                try
                {
                    //send the string through BL
                    response = SendBluetoothMessage(lib.CreateTertiumCommandFromIsoStandard(command));
                    if ((response[0] & 0x01) != (int)ISO15693_Utils.ReturnCode.Success)
                    {
                        Logger.TaggedError("BluetoothNfcReader", $"BluetoothNfcReader SendCommand failed with an error {response[0]:X}", command);
                        continue;
                    }
                    return response;
                }
                catch (Exception e)
                {
                    Logger.TaggedException("BluetoothNfcReader" + $"SendCommand  " + e.Message + " " , "" , e );
                    lastException = e;
                }
            } while (count > 0);

            throw new BluetoothReaderException("SendCommand fail", lastException);
        }

        private async Task ScanTagAsync()
        {
            Logger.TaggedTrace("BluetoothNfcReader" + "BluetoothNfcReader.ScanTag", "");
            int cont = 1;
            while(cont <= RetryBluetoothTimeout)
            {
                try
                {
                    bool isFound = Execute(SendInventoryCommand, 0, waitTimeForScan);
                    if (!isFound)
                    {
                        TagUndetected?.Invoke(this, new NfcEventArgs(NfcEventType.EcgNotFound));
                    }
                    return;
                }
                catch (Exception)
                {
                    Logger.TaggedTrace("BluetoothNfcReader" + "Bluetooth response time out, exit from searching", "");
                    if (cont == RetryBluetoothTimeout)
                    {
                        await DisconnectDeviceAsync(ConnectedDevice);
                        TagUndetected?.Invoke(this, new NfcEventArgs(NfcEventType.DeviceNotFound));
                    }
                }
                cont++;
            }
        }

        private bool SendInventoryCommand()
        {
            Logger.TaggedTrace("BluetoothNfcReader"+ " SendInventoryCommand", "");
            int orgRetryCount = RetryCount;
            try
            {
                RetryCount = 1;
                ByteArray resp = SendCommand(ISO15693_Utils.CreateInventoryCommand());
                tagId = BitConverter.ToString(resp.Skip(2).Reverse().ToArray());
                Logger.TaggedTrace("BluetoothNfcReader" + $" Tag id: {tagId}", "");
                // added a single read memory command, as "normally" the Inventory command succeeds and a subsequent read command fails
                SetUseProtocolExtension();
                RetryCount = orgRetryCount;
                TagDetected?.Invoke(this, tagId);
                return true;
            }
            catch (BluetoothReaderException ex)
            {
                Logger.TaggedTrace("BluetoothNfcReader" + $" BluetoothNfcReader SendInventoryCommand exception: " + ex.Message, "");

                // This is the exception when the bluetooth device is shutdown
                if (ex.Message == "SendCommand fail" && ex.InnerException?.Message == "Bluetooth response time out")
                {
                    throw ex;
                }

                RetryCount = orgRetryCount;
                return false;
            }
        }

        private void SetUseProtocolExtension()
        {
            ByteArray response = SendCommand(ISO15693_Utils.CreateGetSystemInfoCommand(false));
            int icReference = response[response.Count - 1];

            switch (icReference)
            {
                case 0x38:      // ST25DV02K-W1 (PWM) 256 bytes uses only single byte addressing
                    ISO15693_Utils.UseProtocolExtension = false;
                    break;
                case 0x4D:      // M24LR16E-R (old) 2k bytes uses 2 byte addressing
                case 0x4E:      // M24LR16E-R
                    ISO15693_Utils.UseProtocolExtension = true;
                    SendCommand(ISO15693_Utils.CreateReadSingleBlockCommand(0));
                    break;
                default:
                    Logger.Error($"Unknown chip type {icReference:X2}");
                    ISO15693_Utils.UseProtocolExtension = true;
                    SendCommand(ISO15693_Utils.CreateReadSingleBlockCommand(0));
                    break;
            }
        }

        private ByteArray SendBluetoothMessage(byte[] command)
        {
            Logger.TaggedTrace("BluetoothNfcReader" + $" BluetoothNfcReader.SendBluetoothMessage mex= {command}" , "");
            _responseQueue.ResetQueue();
            var result = SendMessageAsync(command);
            return WaitForTheBluetoothResponse();
        }

        private ByteArray WaitForTheBluetoothResponse()
        {
            //better way to wait the reading event, but problem in testing
            //more or less same performance
            //-----------------------------------------------------------------
            if (resetEvent.WaitOne(waitTimeForBluetoothResponse))
            {
                return _responseQueue.GetResponseBytes();
            }
            //-----------------------------------------------------------------
            //Logger.TaggedTrace("BluetoothNfcReader", "WaitForTheBluetoothResponse");
            //if (Execute(IsResponseReceived, 10, waitTimeForBluetoothResponse))
            //{
            //    return _responseQueue.GetResponseBytes();
            //}
            else
            {
                Logger.TaggedTrace("BluetoothNfcReader", "Bluetooth response time out");
                throw new BluetoothReaderException("Bluetooth response time out");
            }
        }

        private bool IsResponseReceived()
        {
            return _responseQueue.IsCompleted;
        }

        private async Task<bool> SendMessageAsync(byte[] command)
        {
            var results = false;
            Logger.TaggedTrace("BluetoothNfcReader" + " Write value sent" , "");
            results = await WriteCharacteristic.WriteAsync(command);
            return results;
        }

        private async Task<byte[]> sendReadMessageAsync()
        {
            byte[] results = new byte[] { };
            try
            {
                results = await ReadCharacteristic.ReadAsync();
            }
            catch (Exception e)
            {

                var ex = e.ToString();
            }
            return results;
        }

        private bool IsConnectedDevice(IDevice device)
        {
            return ConnectedDevice != null && ConnectedDevice.Id.Equals(device.Id);
        }

        /// <summary>
        /// Execute a function until succeeded or timeout expired
        /// </summary>
        /// <returns>true if function succeds, false if timeout occurs</returns>
        private bool Execute(Func<bool> operation, int millisecondsDelay, TimeSpan timeout)
        {
            CancellationTokenSource tokenSource = new CancellationTokenSource(timeout);
            bool timeOutRaised = true;
            while (!tokenSource.IsCancellationRequested)
            {
                Task.Delay(millisecondsDelay).Wait();
                if (operation.Invoke())
                {
                    tokenSource.Cancel();
                    timeOutRaised = false;
                }
            }
            tokenSource.Dispose();
            return !timeOutRaised;
        }
    }
}
