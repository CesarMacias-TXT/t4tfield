﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Osram.TFTBackend.BluetoothService.Exceptions
{
    public class BluetoothReaderException : Exception
    {
        public BluetoothReaderException() { }
        public BluetoothReaderException(string message) : base(message) { }
        public BluetoothReaderException(string message, Exception inner) : base(message, inner) { }
    }
}
