﻿using System;
using System.Threading.Tasks;
using Plugin.BLE.Abstractions.Contracts;

namespace Osram.TFTBackend.BluetoothService.Contracts
{
    public interface IBluetoothReader
    {
        /// <summary>
        /// Reader name
        /// </summary>
        string ReaderName { get;}
        /// <summary>
        /// Readers device Id.
        /// </summary>
        string DeviceId { get;}

        /// <summary>
        /// This event occurs when a new BT device is discovered.
        /// </summary>
        event EventHandler<IDevice> DeviceDiscovered;


        /// <summary>
        /// This event occurs when a new BT device is disconnected.
        /// </summary>
        event EventHandler<IDevice> DeviceDisconnected;

        /// <summary>
        /// This event occurs when a new BT device is connected.
        /// </summary>
        event EventHandler<IDevice> DeviceConnected;

        /// <summary>
        /// This event occurs when a new BT device is lost.
        /// </summary>
        event EventHandler<IDevice> DeviceLost;

        /// <summary>
        /// This event occurs when scannig timeout elapsed;
        /// </summary>
        event EventHandler<IDevice> DeviceScanTimeoutElapsed;

        /// <summary>
        /// This event occurs when connection timeout elapsed;
        /// </summary>
        event EventHandler<IDevice> DeviceConnectionTimeoutElapsed;

        /// <summary>
        /// Get the connected BT device.
        /// </summary>
        IDevice ConnectedDevice { get; }

     
        /// <summary>
        /// Start scanning for BT device available
        /// </summary>
        Task StartScanningForDevicesAsync();

        /// <summary>
        /// Connect to BT seleced device
        /// </summary>
        Task ConnectDeviceAsync(IDevice device);

        /// <summary>
        /// Connect to a know seleced device
        /// </summary>
        Task ConnectToKnownDeviceAsync(Guid guid);

          

        /// <summary>
        /// Disconect to BT seleced device
        /// </summary>
        Task DisconnectDeviceAsync(IDevice device);

        /// <summary>
        /// Indicate if bt devevice is in scanning mode
        /// </summary>
        bool IsDeviceScanning { get; }

        /// <summary>
        /// Indicate if there is a BT device connected
        /// </summary>
        bool IsDeviceConnected { get; }

        bool IsEnabled { get; }
    }
}
