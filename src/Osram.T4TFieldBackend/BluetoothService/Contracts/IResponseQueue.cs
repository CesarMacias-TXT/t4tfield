﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Osram.TFTBackend.NfcService.NfcDataTypes;

namespace Osram.TFTBackend.BluetoothService.Contracts
{
    public interface IResponseQueue
    {
        bool IsCompleted { get; set; }

        void ResetQueue();
        void Add(byte[] item);  
        ByteArray GetResponseBytes();
    }
}
