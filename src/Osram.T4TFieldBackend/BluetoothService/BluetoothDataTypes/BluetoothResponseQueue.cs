﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Osram.TFTBackend.BluetoothService.Contracts;
using Osram.TFTBackend.NfcService.NfcDataTypes;

namespace Osram.TFTBackend.BluetoothService.BluetoothDataTypes
{
    public class BluetoothResponseQueue : IResponseQueue
    {
        Queue<byte[]> responseQueue;

        public BluetoothResponseQueue()
        {
            responseQueue = new Queue<byte[]>();
        }

        public void ResetQueue()
        {
            responseQueue.Clear();
            IsCompleted = false;
        }

        public void Add(byte[] item)
        {
            responseQueue.Enqueue(item);
        }

        public bool IsCompleted { get; set; }

        public ByteArray GetResponseBytes()
        {       
            var response = new List<byte>();
            foreach (byte[] arr in responseQueue)
            {
                response.AddRange(arr);
            }

            //remove first 2 bytes -> PREFIX
            var temp = response.Skip(2);
            //remove last 2 bytes -> SUFFIX
            response = temp.Take(temp.Count() - 2).ToList();

            var asciiString = Encoding.UTF8.GetString(response.ToArray(), 0, response.Count);
            asciiString =  PadStringInCouples(asciiString);          
            return ByteArray.FromString(asciiString);
        }

        private string PadStringInCouples(string asciiString)
        {
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < asciiString.Length; i+=2)
            {
                sb.Append(asciiString.Substring(i, 2));
                sb.Append(" ");
            }
            return sb.ToString();
        }
    }
}
