﻿using MvvmCross;
using MvvmCross.IoC;
using Osram.TFTBackend.DataModel.Feature;
using Osram.TFTBackend.DataModel.Feature.Contracts;
using Osram.TFTBackend.DataModel.Feature.Helper;
using Osram.TFTBackend.KeyService.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace Osram.TFTBackend.DataModel
{
    public class FeatureManager : IFeatureManager
    {
        private List<IFeature> features = new List<IFeature>();

        public FeatureManager()
        {
            CreateAndRegisterFeatures();
        }

        private void CreateAndRegisterFeatures()
        {
            var list = typeof(FeatureManager).GetTypeInfo().Assembly.CreatableTypes().EndingWith("Feature").AsInterfaces();

            foreach (var l in list)
            {
                var feat = (IFeature)Mvx.IoCProvider.IoCConstruct(l.ImplementationType);
                InitFeatureInfo(feat);
                foreach (var s in l.ServiceTypes)
                {
                    Mvx.IoCProvider.RegisterSingleton(s, feat);
                }
                features.Add(feat);
            }
        }

        private void InitFeatureInfo(IFeature feat)
        {
            feat.Info = new FeatureInfo();
            feat.Info.FeatureType = GetFeatureType(feat.GetType());
        }

        private FeatureType GetFeatureType(Type feature)
        {
            return featureDictionary[feature];
        }

        public List<IFeatureInfo> GetAllFeaturesInfoList()
        {
            return features.Select(f => f.Info).ToList();
        }

        public IFeatureInfo GetFeatureInfoByType(FeatureType featureType)
        {
            return features.Where(feature => feature.Info.FeatureType == featureType).Select(f => f.Info).FirstOrDefault();
        }

        public void UpdateFeatureList()
        {
            Logger.TaggedTrace("FeatureManager", "UpdateFeatureList starting ...");
            IKeyService keyService = Mvx.IoCProvider.Resolve<IKeyService>();

            foreach (var feature in features)
            {
                // set the enabled
                feature.Info.IsEnabled = feature.Enabled;

                //Special handle for TuningFactorFeature due to the concurrency with OperatingCurrent
                if (feature.Name == "TuningFactorFeature")
                {
                    TuningFactorFeature tuningFactor = (TuningFactorFeature)feature;
                    if ((!feature.Enabled) && (tuningFactor.OperatingCurrentMode == OperatingCurrentMode.LEDSet2))
                    {
                        feature.Info.IsAvailable = false;
                    }
                }

                ProtectionLock actualProtectionLock = ProtectionLock.None;
                foreach (var permissionIndex in feature.PermissionIndexes)
                {
                    ProtectionLock lockTemp = keyService.GetProtectionLock(permissionIndex);
                    if (lockTemp > actualProtectionLock)
                    {
                        actualProtectionLock = lockTemp;
                    }
                }
                feature.Info.Lock = actualProtectionLock;
                Logger.TaggedTrace("FeatureManager", $" Feature: {feature.Name} Info: {feature.Info}");
            }
            Logger.TaggedTrace("FeatureManager", "UpdateFeatureList done");
        }

        private IDictionary<Type, FeatureType> featureDictionary = new Dictionary<Type, FeatureType>()
        {
            { typeof(AstroDimFeature), FeatureType.AstroDIM},
            { typeof(ConstantLumenFeature), FeatureType.ConstantLumen},
            { typeof(DriverGuardFeature), FeatureType.DriverGuard},
            { typeof(EmergencyModeFeature), FeatureType.EmergencyMode},
            { typeof(EndOfLifeFeature), FeatureType.EndOfLife},
            { typeof(LampOperatingTimeFeature), FeatureType.LampOperatingTime},
            { typeof(MainsDimFeature), FeatureType.MainsDIM},
            { typeof(OperatingCurrentFeature), FeatureType.OperatingCurrent},
            { typeof(PresenceDetectionFeature), FeatureType.PresenceDetection},
            { typeof(StepDimFeature), FeatureType.StepDIM},
            { typeof(ThermalProtectionFeature), FeatureType.ThermalProtection},
            { typeof(TuningFactorFeature), FeatureType.TuningFactor},
            { typeof(DexalPSUFeature), FeatureType.DexalPSU},
            { typeof(DimToDarkFeature), FeatureType.DimToDark},
            { typeof(SoftSwitchOffFeature), FeatureType.SoftSwitchOff},
            { typeof(TouchDimFeature), FeatureType.TouchDim},
            { typeof(CorridorFeature), FeatureType.Corridor},
            { typeof(MonitoringDataFeature), FeatureType.MonitoringData}
        };
    }
}
