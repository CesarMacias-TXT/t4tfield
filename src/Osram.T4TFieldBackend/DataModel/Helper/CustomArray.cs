﻿using MvvmCross.ViewModels;
using System;
using System.Collections;
using System.Linq;

namespace Osram.TFTBackend.DataModel.Helper
{
    public class CustomArray<T> : MvxViewModel, IEnumerable, IEnumerator
    {
        private T[] arrays;
        private bool[] isValid;
        public EventHandler<int> Changed;
        private int position = -1;
        public Type TheType;
        public int Length => arrays.Length;

        public CustomArray(T value, int count)
        {
            arrays = new T[count];
            isValid = new bool[count];
            TheType = typeof(T);
            for (int i = 0; i < arrays.Count(); i++)
            {
                arrays[i] = value;
                isValid[i] = true;
            }
        }

        public void CopyTo(CustomArray<T> arrays)
        {
            if (arrays.TheType != this.TheType)
                throw new Exception("Different array type error");

            arrays.SetOutputLevel(Element1, 1);
            arrays.SetOutputLevel(Element2, 2);
            arrays.SetOutputLevel(Element3, 3);
            arrays.SetOutputLevel(Element4, 4);
            arrays.SetOutputLevel(Element5, 5);
            arrays.SetOutputLevel(Element6, 6);
            arrays.SetOutputLevel(Element7, 7);
            arrays.SetOutputLevel(Element8, 8);
        }

        private void RaiseChangeEvent(int level)
        {
            Changed?.Invoke(this, level);
        }

        private void SetOutputLevel(T value, int indexLevel)
        {
            if (indexLevel <= arrays.Count())
            {
                arrays[indexLevel - 1] = value;
                RaiseChangeEvent(indexLevel);
            }
        }
        private T GetOutputLevel(int numberLevel)
        {
            if (0 < numberLevel &&
                numberLevel <= arrays.Count())
            {
                return arrays[numberLevel - 1];
            }
            return default(T);
        }

        public T this[int key]
        {
            get
            {
                return GetOutputLevel(key);
            }
            set
            {
                SetOutputLevel(value, key);
            }
        }

        public IEnumerator GetEnumerator()
        {
            return this;
        }

        public bool MoveNext()
        {
            position++;
            return (position < arrays.Length);
        }

        public void Reset()
        {
            position = 0;
        }

        public T Element1
        {
            get { return GetOutputLevel(1); }
            set
            {
                SetOutputLevel(value, 1);
                RaisePropertyChanged(() => Element1);
            }
        }
        public bool IsValid1 { get { return isValid[0]; } set { isValid[0] = value; RaisePropertyChanged(() => IsValid1); } }

        public T Element2
        {
            get { return GetOutputLevel(2); }
            set
            {
                SetOutputLevel(value, 2);
                RaisePropertyChanged(() => Element2);
            }
        }
        public bool IsValid2 { get { return isValid[1]; } set { isValid[1] = value; RaisePropertyChanged(() => IsValid2); } }
        public T Element3
        {
            get { return GetOutputLevel(3); }
            set
            {
                SetOutputLevel(value, 3);
                RaisePropertyChanged(() => Element3);
            }
        }
        public bool IsValid3 { get { return isValid[2]; } set { isValid[2] = value; RaisePropertyChanged(() => IsValid3); } }
        public T Element4
        {
            get { return GetOutputLevel(4); }
            set
            {
                SetOutputLevel(value, 4);
                RaisePropertyChanged(() => Element4);
            }
        }
        public bool IsValid4 { get { return isValid[3]; } set { isValid[3] = value; RaisePropertyChanged(() => IsValid4); } }
        public T Element5
        {
            get { return GetOutputLevel(5); }
            set
            {
                SetOutputLevel(value, 5);
                RaisePropertyChanged(() => Element5);
            }
        }
        public bool IsValid5 { get { return isValid[4]; } set { isValid[4] = value; RaisePropertyChanged(() => IsValid5); } }
        public T Element6
        {
            get { return GetOutputLevel(6); }
            set
            {
                SetOutputLevel(value, 6);
                RaisePropertyChanged(() => Element6);
            }
        }
        public bool IsValid6 { get { return isValid[5]; } set { isValid[5] = value; RaisePropertyChanged(() => IsValid6); } }
        public T Element7
        {
            get { return GetOutputLevel(7); }
            set
            {
                SetOutputLevel(value, 7);
                RaisePropertyChanged(() => Element7);
            }
        }
        public bool IsValid7 { get { return isValid[6]; } set { isValid[6] = value; RaisePropertyChanged(() => IsValid7); } }
        public T Element8
        {
            get { return GetOutputLevel(8); }
            set
            {
                SetOutputLevel(value, 8);
                RaisePropertyChanged(() => Element8);
            }
        }
        public bool IsValid8 { get { return isValid[7]; } set { isValid[7] = value; RaisePropertyChanged(() => IsValid8); } }

        public object Current => arrays[position];
    }
}
