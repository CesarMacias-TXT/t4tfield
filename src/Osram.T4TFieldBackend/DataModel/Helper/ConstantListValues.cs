﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Osram.TFTBackend.DataModel.Helper
{
    /// <summary>
    /// Class contains all the methods that can be used by the whole application.
    /// </summary>
    public class ConstantListValues
    {
        #region Singleton

        private static ConstantListValues instance;

        private List<KeyValuePair<int, string>> tmpSwitchOnFadeTimes;
        private List<KeyValuePair<int, string>> tmpFadeTimeRangeValues;
        private List<KeyValuePair<int, string>> tmpDaliFadeTimes;
        private List<KeyValuePair<int, string>> tmpDaliFadeRates;
        private List<KeyValuePair<int, string>> tmpTouchDimFadeTimes;
        private List<KeyValuePair<int, string>> tmpTouchDimHoldTimes1;
        private List<KeyValuePair<int, string>> tmpTouchDimHoldTimes2;
        private List<KeyValuePair<int, string>> tmpTouchDimColorRangeValues;
        private List<KeyValuePair<int, string>> tmpTouchDimStartupBehaviourValues;
        private List<decimal> astro2dimSwitchofffadetimelist = new List<decimal>();

        /// <summary>
        /// Gets the instance of this class.
        /// </summary>
        /// <value>
        /// The instance.
        /// </value>
        public static ConstantListValues Instance {
            get {
                if (instance == null)
                {
                    instance = new ConstantListValues();
                }

                return instance;
            }
        }

        #endregion

        #region Properties

        /// <summary>
        /// Gets the dali fade times for DaliSettings 4 DIM.
        /// </summary>
        /// <value>
        /// The switch on fade times.
        /// </value>
        public List<KeyValuePair<int, string>> DaliFadeTimes {
            get {
                if (tmpDaliFadeTimes == null)
                {
                    tmpDaliFadeTimes = new List<KeyValuePair<int, string>>();
                    tmpDaliFadeTimes.Add(new KeyValuePair<int, string>(0, "0"));
                    tmpDaliFadeTimes.Add(new KeyValuePair<int, string>(1, "0.7"));
                    tmpDaliFadeTimes.Add(new KeyValuePair<int, string>(2, "1.0"));
                    tmpDaliFadeTimes.Add(new KeyValuePair<int, string>(3, "1.4"));
                    tmpDaliFadeTimes.Add(new KeyValuePair<int, string>(4, "2.0"));
                    tmpDaliFadeTimes.Add(new KeyValuePair<int, string>(5, "2.8"));
                    tmpDaliFadeTimes.Add(new KeyValuePair<int, string>(6, "4.0"));
                    tmpDaliFadeTimes.Add(new KeyValuePair<int, string>(7, "5.7"));
                    tmpDaliFadeTimes.Add(new KeyValuePair<int, string>(8, "8.0"));
                    tmpDaliFadeTimes.Add(new KeyValuePair<int, string>(9, "11.3"));
                    tmpDaliFadeTimes.Add(new KeyValuePair<int, string>(10, "16"));
                    tmpDaliFadeTimes.Add(new KeyValuePair<int, string>(11, "22.6"));
                    tmpDaliFadeTimes.Add(new KeyValuePair<int, string>(12, "32"));
                    tmpDaliFadeTimes.Add(new KeyValuePair<int, string>(13, "45.3"));
                    tmpDaliFadeTimes.Add(new KeyValuePair<int, string>(14, "64"));
                    tmpDaliFadeTimes.Add(new KeyValuePair<int, string>(15, "90.5"));
                }

                return tmpDaliFadeTimes;
            }
        }

        public List<KeyValuePair<int, string>> TouchDimFadeTimes
        {
            get
            {
                if (tmpTouchDimFadeTimes == null)
                {
                    tmpTouchDimFadeTimes = new List<KeyValuePair<int, string>>();

                    tmpTouchDimFadeTimes.Add(new KeyValuePair<int, string>(0, "0"));
                    tmpTouchDimFadeTimes.Add(new KeyValuePair<int, string>(1, "0.7"));
                    tmpTouchDimFadeTimes.Add(new KeyValuePair<int, string>(2, "1.0"));
                    tmpTouchDimFadeTimes.Add(new KeyValuePair<int, string>(3, "1.4"));
                    tmpTouchDimFadeTimes.Add(new KeyValuePair<int, string>(4, "2.0"));
                    tmpTouchDimFadeTimes.Add(new KeyValuePair<int, string>(5, "2.8"));
                    tmpTouchDimFadeTimes.Add(new KeyValuePair<int, string>(6, "4.0"));
                    tmpTouchDimFadeTimes.Add(new KeyValuePair<int, string>(7, "5.7"));
                    tmpTouchDimFadeTimes.Add(new KeyValuePair<int, string>(8, "8.0"));
                    tmpTouchDimFadeTimes.Add(new KeyValuePair<int, string>(9, "11.3"));
                    tmpTouchDimFadeTimes.Add(new KeyValuePair<int, string>(10, "16"));
                    tmpTouchDimFadeTimes.Add(new KeyValuePair<int, string>(11, "22.6"));
                    tmpTouchDimFadeTimes.Add(new KeyValuePair<int, string>(12, "32"));
                    tmpTouchDimFadeTimes.Add(new KeyValuePair<int, string>(13, "45.3"));
                    tmpTouchDimFadeTimes.Add(new KeyValuePair<int, string>(14, "64"));
                    tmpTouchDimFadeTimes.Add(new KeyValuePair<int, string>(15, "90.5"));
                    tmpTouchDimFadeTimes.Add(new KeyValuePair<int, string>(16, "0.1"));
                    tmpTouchDimFadeTimes.Add(new KeyValuePair<int, string>(17, "0.2"));
                    tmpTouchDimFadeTimes.Add(new KeyValuePair<int, string>(18, "0.3"));
                    tmpTouchDimFadeTimes.Add(new KeyValuePair<int, string>(19, "0.4"));
                    tmpTouchDimFadeTimes.Add(new KeyValuePair<int, string>(20, "0.5"));
                    tmpTouchDimFadeTimes.Add(new KeyValuePair<int, string>(21, "0.6"));
                    tmpTouchDimFadeTimes.Add(new KeyValuePair<int, string>(22, ""));
                    tmpTouchDimFadeTimes.Add(new KeyValuePair<int, string>(23, "0.8"));
                    tmpTouchDimFadeTimes.Add(new KeyValuePair<int, string>(24, "0.9"));
                    tmpTouchDimFadeTimes.Add(new KeyValuePair<int, string>(25, ""));
                    tmpTouchDimFadeTimes.Add(new KeyValuePair<int, string>(26, "1.1"));
                    tmpTouchDimFadeTimes.Add(new KeyValuePair<int, string>(27, "1.2"));
                    tmpTouchDimFadeTimes.Add(new KeyValuePair<int, string>(28, "1.3"));
                    tmpTouchDimFadeTimes.Add(new KeyValuePair<int, string>(29, ""));
                    tmpTouchDimFadeTimes.Add(new KeyValuePair<int, string>(30, "1.5"));
                    tmpTouchDimFadeTimes.Add(new KeyValuePair<int, string>(31, "1.6"));
                }

                return tmpTouchDimFadeTimes;
            }
        }

        public List<KeyValuePair<int, string>> TouchDimHoldTimes1RangeValues
        {
            get
            {
                if (tmpTouchDimHoldTimes1 == null)
                {
                    tmpTouchDimHoldTimes1 = new List<KeyValuePair<int, string>>();

                    for (int i = 0; i < 255; i++)
                    {
                        decimal holdValue = ((i * 256) + 255) / 50;
                        tmpTouchDimHoldTimes1.Add(new KeyValuePair<int, string>(i, holdValue + ""));
                    }

                    tmpTouchDimHoldTimes1.Add(new KeyValuePair<int, string>(255, ""));
                }

                return tmpTouchDimHoldTimes1;
            }
        }

        public List<KeyValuePair<int, string>> TouchDimHoldTimes2RangeValues
        {
            get
            {
                if (tmpTouchDimHoldTimes2 == null)
                {
                    tmpTouchDimHoldTimes2 = new List<KeyValuePair<int, string>>();

                    tmpTouchDimHoldTimes2.Add(new KeyValuePair<int, string>(0, "0"));

                    for (int i = 1; i < 255; i++)
                    {
                        double holdValue = (((i * 256) - 1) * 5.889) / 50;
                        tmpTouchDimHoldTimes2.Add(new KeyValuePair<int, string>(i, holdValue + ""));
                    }

                    tmpTouchDimHoldTimes2.Add(new KeyValuePair<int, string>(255, ""));
                }

                return tmpTouchDimHoldTimes2;
            }
        }

        public List<KeyValuePair<int, string>> TouchDimColorRangeValues
        {
            get
            {
                if (tmpTouchDimColorRangeValues == null)
                {
                    tmpTouchDimColorRangeValues = new List<KeyValuePair<int, string>>();

                    tmpTouchDimColorRangeValues.Add(new KeyValuePair<int, string>(0, "0"));

                    for (int i = 1; i < 65534; i++)
                    {
                        double colorValue = 1000000 / i;
                        tmpTouchDimColorRangeValues.Add(new KeyValuePair<int, string>(i, colorValue + ""));
                    }

                    tmpTouchDimColorRangeValues.Add(new KeyValuePair<int, string>(65535, ""));
                }

                return tmpTouchDimColorRangeValues;
            }
        }

        public List<KeyValuePair<int, string>> TouchDimStartupBehaviourValues
        {
            get
            {
                if (tmpTouchDimStartupBehaviourValues == null)
                {
                    tmpTouchDimStartupBehaviourValues = new List<KeyValuePair<int, string>>();

                    tmpTouchDimStartupBehaviourValues.Add(new KeyValuePair<int, string>(0, "Use Level 0"));
                    tmpTouchDimStartupBehaviourValues.Add(new KeyValuePair<int, string>(1, "Use Level 1"));
                    tmpTouchDimStartupBehaviourValues.Add(new KeyValuePair<int, string>(2, "Use Level 2"));
                    tmpTouchDimStartupBehaviourValues.Add(new KeyValuePair<int, string>(255, "Stay off"));
                }

                return tmpTouchDimStartupBehaviourValues;
            }
        }        

        /// <summary>
        /// Gets the dali fade rate for DaliSettings 4 DIM.
        /// </summary>
        /// <value>
        /// The switch on fade times.
        /// </value>
        public List<KeyValuePair<int, string>> DaliFadeRates {
            get {
                if (tmpDaliFadeRates == null)
                {
                    tmpDaliFadeRates = new List<KeyValuePair<int, string>>();
                    tmpDaliFadeRates.Add(new KeyValuePair<int, string>(0, "%"));
                    tmpDaliFadeRates.Add(new KeyValuePair<int, string>(1, "358"));
                    tmpDaliFadeRates.Add(new KeyValuePair<int, string>(2, "253"));
                    tmpDaliFadeRates.Add(new KeyValuePair<int, string>(3, "179"));
                    tmpDaliFadeRates.Add(new KeyValuePair<int, string>(4, "127"));
                    tmpDaliFadeRates.Add(new KeyValuePair<int, string>(5, "89.4"));
                    tmpDaliFadeRates.Add(new KeyValuePair<int, string>(6, "63.3"));
                    tmpDaliFadeRates.Add(new KeyValuePair<int, string>(7, "44.7"));
                    tmpDaliFadeRates.Add(new KeyValuePair<int, string>(8, "31.6"));
                    tmpDaliFadeRates.Add(new KeyValuePair<int, string>(9, "22.4"));
                    tmpDaliFadeRates.Add(new KeyValuePair<int, string>(10, "15.8"));
                    tmpDaliFadeRates.Add(new KeyValuePair<int, string>(11, "11.2"));
                    tmpDaliFadeRates.Add(new KeyValuePair<int, string>(12, "7.9"));
                    tmpDaliFadeRates.Add(new KeyValuePair<int, string>(13, "5.6"));
                    tmpDaliFadeRates.Add(new KeyValuePair<int, string>(14, "4.0"));
                    tmpDaliFadeRates.Add(new KeyValuePair<int, string>(15, "2.8"));
                }

                return tmpDaliFadeRates;
            }
        }

        /// <summary>
        /// Gets the switch on fade times for StepDIM 4 DIM feature.
        /// </summary>
        /// <value>
        /// The switch on fade times.
        /// </value>
        public List<KeyValuePair<int, string>> SwitchOnFadeTimes {
            get {
                if (tmpSwitchOnFadeTimes == null)
                {
                    tmpSwitchOnFadeTimes = new List<KeyValuePair<int, string>>();
                    tmpSwitchOnFadeTimes.Add(new KeyValuePair<int, string>(0, "00:00"));
                    tmpSwitchOnFadeTimes.Add(new KeyValuePair<int, string>(15, "00:15"));
                    tmpSwitchOnFadeTimes.Add(new KeyValuePair<int, string>(30, "00:30"));
                    tmpSwitchOnFadeTimes.Add(new KeyValuePair<int, string>(45, "00:45"));
                    tmpSwitchOnFadeTimes.Add(new KeyValuePair<int, string>(60, "01:00"));
                    tmpSwitchOnFadeTimes.Add(new KeyValuePair<int, string>(120, "02:00"));
                    tmpSwitchOnFadeTimes.Add(new KeyValuePair<int, string>(300, "05:00"));
                    tmpSwitchOnFadeTimes.Add(new KeyValuePair<int, string>(600, "10:00"));
                    tmpSwitchOnFadeTimes.Add(new KeyValuePair<int, string>(900, "15:00"));
                    tmpSwitchOnFadeTimes.Add(new KeyValuePair<int, string>(1200, "20:00"));
                    tmpSwitchOnFadeTimes.Add(new KeyValuePair<int, string>(1500, "25:00"));
                    tmpSwitchOnFadeTimes.Add(new KeyValuePair<int, string>(1800, "30:00"));
                    tmpSwitchOnFadeTimes.Add(new KeyValuePair<int, string>(2100, "35:00"));
                    tmpSwitchOnFadeTimes.Add(new KeyValuePair<int, string>(2400, "40:00"));
                    tmpSwitchOnFadeTimes.Add(new KeyValuePair<int, string>(2700, "45:00"));
                    tmpSwitchOnFadeTimes.Add(new KeyValuePair<int, string>(3600, "60:00"));
                }

                return tmpSwitchOnFadeTimes;
            }
        }

        /// <summary>
        /// Gets the time of fade times for StepDIM 4 DIM feature.
        /// </summary>
        /// <value>
        /// The time of fade times.
        /// </value>
        public List<KeyValuePair<int, string>> FadeTimeRangeValues {
            get {
                if (tmpFadeTimeRangeValues == null)
                {
                    tmpFadeTimeRangeValues = new List<KeyValuePair<int, string>>();
                    tmpFadeTimeRangeValues.Add(new KeyValuePair<int, string>(0, "00:00"));
                    tmpFadeTimeRangeValues.Add(new KeyValuePair<int, string>(2, "00:02"));
                    tmpFadeTimeRangeValues.Add(new KeyValuePair<int, string>(4, "00:04"));
                    tmpFadeTimeRangeValues.Add(new KeyValuePair<int, string>(6, "00:06"));
                    tmpFadeTimeRangeValues.Add(new KeyValuePair<int, string>(8, "00:08"));
                    tmpFadeTimeRangeValues.Add(new KeyValuePair<int, string>(10, "00:10"));
                    tmpFadeTimeRangeValues.Add(new KeyValuePair<int, string>(20, "00:20"));
                    tmpFadeTimeRangeValues.Add(new KeyValuePair<int, string>(30, "00:30"));
                    tmpFadeTimeRangeValues.Add(new KeyValuePair<int, string>(40, "00:40"));
                    tmpFadeTimeRangeValues.Add(new KeyValuePair<int, string>(50, "00:50"));
                    tmpFadeTimeRangeValues.Add(new KeyValuePair<int, string>(60, "01:00"));
                    tmpFadeTimeRangeValues.Add(new KeyValuePair<int, string>(120, "02:00"));
                    tmpFadeTimeRangeValues.Add(new KeyValuePair<int, string>(180, "03:00"));
                    tmpFadeTimeRangeValues.Add(new KeyValuePair<int, string>(240, "04:00"));
                    tmpFadeTimeRangeValues.Add(new KeyValuePair<int, string>(300, "05:00"));
                    tmpFadeTimeRangeValues.Add(new KeyValuePair<int, string>(360, "06:00"));
                    tmpFadeTimeRangeValues.Add(new KeyValuePair<int, string>(480, "08:00"));
                }

                return tmpFadeTimeRangeValues;
            }
        }

        /// <summary>
        /// Astro DIM fade time values.
        /// </summary>
        /// <returns>List of Astro DIM fade time values.</returns>
        public List<KeyValuePair<int, string>> AstroDimRangeValues()
        {
            List<KeyValuePair<int, string>> returnValue = new List<KeyValuePair<int, string>>();

            //0s , 2s, 4s, 6s, 8s, 10s, 20s, 30s, 45s, 1min, 2min, 3min, 4min, 5min, 6min, 8min, Off

            returnValue.Add(new KeyValuePair<int, string>(0, "00:00"));
            returnValue.Add(new KeyValuePair<int, string>(2, "00:02"));
            returnValue.Add(new KeyValuePair<int, string>(4, "00:04"));
            returnValue.Add(new KeyValuePair<int, string>(6, "00:06"));
            returnValue.Add(new KeyValuePair<int, string>(8, "00:08"));
            returnValue.Add(new KeyValuePair<int, string>(10, "00:10"));
            returnValue.Add(new KeyValuePair<int, string>(20, "00:20"));
            returnValue.Add(new KeyValuePair<int, string>(30, "00:30"));
            returnValue.Add(new KeyValuePair<int, string>(40, "00:40"));
            returnValue.Add(new KeyValuePair<int, string>(50, "00:50"));
            returnValue.Add(new KeyValuePair<int, string>(60, "01:00"));
            returnValue.Add(new KeyValuePair<int, string>(120, "02:00"));
            returnValue.Add(new KeyValuePair<int, string>(180, "03:00"));
            returnValue.Add(new KeyValuePair<int, string>(240, "04:00"));
            returnValue.Add(new KeyValuePair<int, string>(300, "05:00"));
            returnValue.Add(new KeyValuePair<int, string>(360, "06:00"));
            returnValue.Add(new KeyValuePair<int, string>(480, "08:00"));

            return returnValue;
        }


        /// <summary>
        /// Switch Off Fade Time Range Values for AstroDIM.
        /// </summary>
        /// <returns>List of Switch Off Fade Time Range Values.</returns>
        public List<KeyValuePair<decimal, string>> AstroDimSwitchOffRangeValues()
        {
            List<KeyValuePair<decimal, string>> returnValue = new List<KeyValuePair<decimal, string>>();

            //0s (0x00 1)), 15s, 30s, 45s, 1min, 2min, 5min, 10min, 15min, 20min, 25min, 30min, 35min, 40min, 45min, 60min, Off
            //Not effect on communication as communication based on value
            returnValue.Add(new KeyValuePair<decimal, string>(255, "OFF"));
            returnValue.Add(new KeyValuePair<decimal, string>(0, "00:00"));
            returnValue.Add(new KeyValuePair<decimal, string>(15, "00:15"));
            returnValue.Add(new KeyValuePair<decimal, string>(30, "00:30"));
            returnValue.Add(new KeyValuePair<decimal, string>(45, "00:45"));
            returnValue.Add(new KeyValuePair<decimal, string>(60, "01:00"));
            returnValue.Add(new KeyValuePair<decimal, string>(120, "02:00"));
            returnValue.Add(new KeyValuePair<decimal, string>(300, "05:00"));
            returnValue.Add(new KeyValuePair<decimal, string>(600, "10:00"));
            returnValue.Add(new KeyValuePair<decimal, string>(900, "15:00"));
            returnValue.Add(new KeyValuePair<decimal, string>(1200, "20:00"));
            returnValue.Add(new KeyValuePair<decimal, string>(1500, "25:00"));
            returnValue.Add(new KeyValuePair<decimal, string>(1800, "30:00"));
            returnValue.Add(new KeyValuePair<decimal, string>(2100, "35:00"));
            returnValue.Add(new KeyValuePair<decimal, string>(2400, "40:00"));
            returnValue.Add(new KeyValuePair<decimal, string>(2700, "45:00"));
            returnValue.Add(new KeyValuePair<decimal, string>(3600, "60:00"));

            return returnValue;
        }


        /// <summary>
        /// Switch On Fade Time Range Values for Astro DIM.
        /// </summary>
        /// <returns>List of Switch Off Fade Time Range Values.</returns>
        public List<KeyValuePair<decimal, string>> AstroDimSwitchOnRangeValues()
        {
            List<KeyValuePair<decimal, string>> returnValue = new List<KeyValuePair<decimal, string>>();

            //0s (0x00 1)), 15s, 30s, 45s, 1min, 2min, 5min, 10min, 15min, 20min, 25min, 30min, 35min, 40min, 45min, 60min, Off
            returnValue.Add(new KeyValuePair<decimal, string>(0, "00:00"));
            returnValue.Add(new KeyValuePair<decimal, string>(15, "00:15"));
            returnValue.Add(new KeyValuePair<decimal, string>(30, "00:30"));
            returnValue.Add(new KeyValuePair<decimal, string>(45, "00:45"));
            returnValue.Add(new KeyValuePair<decimal, string>(60, "01:00"));
            returnValue.Add(new KeyValuePair<decimal, string>(120, "02:00"));
            returnValue.Add(new KeyValuePair<decimal, string>(300, "05:00"));
            returnValue.Add(new KeyValuePair<decimal, string>(600, "10:00"));
            returnValue.Add(new KeyValuePair<decimal, string>(900, "15:00"));
            returnValue.Add(new KeyValuePair<decimal, string>(1200, "20:00"));
            returnValue.Add(new KeyValuePair<decimal, string>(1500, "25:00"));
            returnValue.Add(new KeyValuePair<decimal, string>(1800, "30:00"));
            returnValue.Add(new KeyValuePair<decimal, string>(2100, "35:00"));
            returnValue.Add(new KeyValuePair<decimal, string>(2400, "40:00"));
            returnValue.Add(new KeyValuePair<decimal, string>(2700, "45:00"));
            returnValue.Add(new KeyValuePair<decimal, string>(3600, "60:00"));

            return returnValue;
        }

        /// <summary>
        /// Astro dim fade time list calculation.
        /// </summary>
        /// <param name="devicedescriptionFileValue">The device description file value.</param>
        /// <returns>Returns the list of astro dim fade time value.</returns>
        public List<KeyValuePair<int, string>> AstroDimFadeTime(List<KeyValuePair<int, string>> devicedescriptionFileValue)
        {
            List<KeyValuePair<int, string>> returnValue = new List<KeyValuePair<int, string>>();
            if (devicedescriptionFileValue != null)
            {
                foreach (KeyValuePair<int, string> deveicedescription in devicedescriptionFileValue)
                {
                    returnValue.Add(new KeyValuePair<int, string>(deveicedescription.Key, GetTime(deveicedescription.Value)));
                }
            }

            return returnValue;
        }



        /// <summary>
        /// Gets the time in the minutes:seconds format for astroDIM.
        /// </summary>
        /// <param name="value">The value.</param>
        /// <returns>Returns in date time for the given value.</returns>
        private string GetTime(string value)
        {
            int intermediatevalue = Convert.ToInt32(Convert.ToDouble(value));
            TimeSpan t = TimeSpan.FromSeconds(intermediatevalue);
            return new DateTime(t.Ticks).ToString("mm:ss");
        }

        #endregion
    }
}
