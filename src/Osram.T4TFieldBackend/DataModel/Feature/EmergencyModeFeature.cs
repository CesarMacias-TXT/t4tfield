﻿using MvvmCross.Plugin.Messenger;
using Osram.TFTBackend.ConfigurationService.Contracts;
using Osram.TFTBackend.ConfigurationService.DataModel;
using Osram.TFTBackend.DataModel.Feature.Contracts;
using Osram.TFTBackend.DataModel.Feature.Helper;

namespace Osram.TFTBackend.DataModel.Feature
{
    public class EmergencyModeFeature : FeatureImpl, IEmergencyModeFeature
    {
#pragma warning disable CS0649
        // <Property PgmName="Emergency-0:DCLightLevel" AccessType="RWL" Volatile="false" PermW="16">
        [EcgMapping("Emergency-0:DCLightLevel", true)]
        private IEcgPropertyValue _dcLightLevel;
        public int DCLightLevel {
            get { return _dcLightLevel.IntValue; }
            set { _dcLightLevel.IntValue = value; }
        }

        [EcgMapping("Emergency-0:ConfigLock-DaliParamLock", true)]
        private IEcgPropertyValue _lockDali;
        public bool LockDali => !(_lockDali.IntValue == _lockDali.IntOffValue);

#pragma warning restore CS0649

        protected override string EnabledPropertyMappingName => "Emergency-0:StatusInfo-DCDetectionStatus";

        protected override bool IsEnabledAdvanced()
        {
            if (_enabledProperty == null)
            {
                Info.IsAvailable = false;
                return false;
            }

            return true;
        }

        public EmergencyModeFeature(IMvxMessenger messenger, IConfigurationService configurationService) : base(messenger, configurationService)
        {
        }
    }
}