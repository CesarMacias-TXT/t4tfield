﻿using MvvmCross.Plugin.Messenger;
using Osram.TFTBackend.ConfigurationService.Contracts;
using Osram.TFTBackend.ConfigurationService.DataModel;
using Osram.TFTBackend.DataModel.Feature.Contracts;
using Osram.TFTBackend.DataModel.Feature.Helper;

namespace Osram.TFTBackend.DataModel.Feature
{
    public class DriverGuardFeature : FeatureImpl, IDriverGuardFeature
    {
#pragma warning disable CS0649
        // <Property PgmName="DG-0:PrestartDerating" AccessType="RWL" Volatile="false" PermW="11">
        [EcgMapping("DG-0:PrestartDerating", true)]
        private IEcgPropertyValue _prestartDerating;
        public int PrestartDerating
        {
            get { return (sbyte)_prestartDerating.IntValue; }
            set { _prestartDerating.IntValue = value; }
        }

        // <Property PgmName="DG-0:DeratingLevel" AccessType="RWL" Volatile="false" PermW="11">
        [EcgMapping("DG-0:DeratingLevel", true)]
        private IEcgPropertyValue _deratingLevel;
        public int DeratingLevel
        {
            get { return _deratingLevel.IntValue; }
            set { _deratingLevel.IntValue = value; }
        }

        // <Property PgmName="DG-0:PowerDeratingLevel" AccessType="RWL" Volatile="false" PermW="11">
        [EcgMapping("DG-0:PowerDeratingLevel", true)]
        private IEcgPropertyValue _powerDeratingLevel;
        public int PowerDeratingLevel
        {
            get { return _powerDeratingLevel.IntValue; }
            set { _powerDeratingLevel.IntValue = value; }
        }
#pragma warning restore CS0649

        protected override string EnabledPropertyMappingName => "DG-0:Enable";
        public DriverGuardFeature(IMvxMessenger messenger, IConfigurationService configurationService) : base(messenger, configurationService)
        {
        }
    }
}