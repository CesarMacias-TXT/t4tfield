﻿using MvvmCross.Plugin.Messenger;
using Osram.TFTBackend.ConfigurationService.Contracts;
using Osram.TFTBackend.ConfigurationService.DataModel;
using Osram.TFTBackend.DataModel.Feature.Contracts;
using Osram.TFTBackend.DataModel.Feature.Helper;

namespace Osram.TFTBackend.DataModel.Feature
{
    public class DimToDarkFeature : FeatureImpl, IDimToDarkFeature
    {
        [EcgMapping("D2D-0:Config-Enable", true, "GFM-1:D2DConfig-Enable")]
        private IEcgPropertyValue _active;
        public bool Active => !(_active.IntValue == _active.IntOffValue);

#pragma warning restore CS0649
        protected override string EnabledPropertyMappingName => "D2D-0:Config-Enable";

        protected override bool IsEnabledAdvanced()
        {
            if (_enabledProperty == null)
            {
                var propertyDictionary = _theConfigurationService.GetConfigurationPropertyDictionary();
                EcgProperty property;

                if (propertyDictionary.TryGetValue("GFM-1:D2DConfig-Enable", out property))
                {
                    if (property.IntMaxValue == property.IntMinValue)
                    {
                        Info.IsAvailable = false;
                        return false;
                    }

                    return Active;
                }

                Info.IsAvailable = false;
                return false;
            }

            return true;
        }

        public DimToDarkFeature(IMvxMessenger messenger, IConfigurationService configurationService) : base(messenger, configurationService)
        {
        }
    }
}