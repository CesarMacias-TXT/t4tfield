﻿using MvvmCross.Plugin.Messenger;
using Osram.TFTBackend.ConfigurationService.Contracts;
using Osram.TFTBackend.DataModel.Feature.Contracts;
using Osram.TFTBackend.DataModel.Feature.Helper;

namespace Osram.TFTBackend.DataModel.Feature
{
    public class DexalPSUFeature : FeatureImpl, IDexalPSUFeature
    {
#pragma warning restore CS0649

        protected override string EnabledPropertyMappingName => "DexalPSU-0:Enable";

        protected override bool IsEnabledAdvanced()
        {
            if(_enabledProperty == null)
            {
                Info.IsAvailable = false;
                return false;
            }

            return true;
        }

        public DexalPSUFeature(IMvxMessenger messenger, IConfigurationService configurationService) : base(messenger, configurationService)
        {
        }
    }
}