﻿using MvvmCross.Plugin.Messenger;
using Osram.TFTBackend.ConfigurationService.Contracts;
using Osram.TFTBackend.ConfigurationService.DataModel;
using Osram.TFTBackend.DataModel.Feature.Contracts;
using Osram.TFTBackend.DataModel.Feature.Helper;

namespace Osram.TFTBackend.DataModel.Feature
{
    public class SoftSwitchOffFeature : FeatureImpl, ISoftSwitchOffFeature
    {
#pragma warning disable CS0649
        // <Property PgmName="ThermProt-2:Command-TempMeasureEnable" AccessType="RWL" Volatile="false" PermW="10">
        [EcgMapping("GFM-1:SSOMode-Time", true)]
        private IEcgPropertyValue _fadeTime;
        public int FadeTime
        {
            get { return _fadeTime.IntValue; }
            set { _fadeTime.IntValue = (int)value; }
        }

#pragma warning restore CS0649

        protected override string EnabledPropertyMappingName => "GFM-1:SSOMode-Enable";

        public SoftSwitchOffFeature(IMvxMessenger messenger, IConfigurationService configurationService) : base(messenger, configurationService)
        {
        }
    }
}
