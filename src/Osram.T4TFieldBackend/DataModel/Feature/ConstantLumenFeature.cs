﻿using System;
using System.Collections.Generic;
using MvvmCross.Plugin.Messenger;
using Osram.TFTBackend.ConfigurationService.Contracts;
using Osram.TFTBackend.ConfigurationService.DataModel;
using Osram.TFTBackend.DataModel.Feature.Contracts;
using Osram.TFTBackend.DataModel.Feature.Helper;

namespace Osram.TFTBackend.DataModel.Feature
{
    public class ConstantLumenFeature : FeatureImpl, IConstantLumenFeature
    {
#pragma warning disable CS0649
        // <Property PgmName="OTConfig-2:DefaultOpCurrent" AccessType="RWL" Volatile="false" PermW="8">
        [EcgMapping("OTConfig-2:DefaultOpCurrent", true)]
        private IEcgPropertyValue _default;
        public int Default
        {
            get { return _default.IntValue; }
            set { _default.IntValue = value; }
        }

        [EcgMapping("ConstLum-0:Command-ClmEnable", true)]
        private IEcgPropertyValue _enable;
        public int Enable
        {
            get { return _enable.IntValue; }
            set { _enable.IntValue = value; }
        }

        #region Time
        // <Property PgmName="ConstLum-0:Time1" AccessType="RWL" Volatile="false" PermW="9">
        [EcgMapping("ConstLum-0:Time1", true)]
        private IEcgPropertyValue _time1;
        public int Time1
        {
            get { return _time1.IntValue; }
            set { _time1.IntValue = value; }
        }

        // <Property PgmName="ConstLum-0:Time2" AccessType="RWL" Volatile="false" PermW="9">
        [EcgMapping("ConstLum-0:Time2", true)]
        private IEcgPropertyValue _time2;
        public int Time2
        {
            get { return _time2.IntValue; }
            set { _time2.IntValue = value; }
        }
        // <Property PgmName="ConstLum-0:Time3" AccessType="RWL" Volatile="false" PermW="9">
        [EcgMapping("ConstLum-0:Time3", true)]
        private IEcgPropertyValue _time3;
        public int Time3
        {
            get { return _time3.IntValue; }
            set { _time3.IntValue = value; }
        }

        // <Property PgmName="ConstLum-0:Time4" AccessType="RWL" Volatile="false" PermW="9">
        [EcgMapping("ConstLum-0:Time4", true)]
        private IEcgPropertyValue _time4;
        public int Time4
        {
            get { return _time4.IntValue; }
            set { _time4.IntValue = value; }
        }

        // <Property PgmName="ConstLum-0:Time5" AccessType="RWL" Volatile="false" PermW="9">
        [EcgMapping("ConstLum-0:Time5", true)]
        private IEcgPropertyValue _time5;
        public int Time5
        {
            get { return _time5.IntValue; }
            set { _time5.IntValue = value; }
        }

        // <Property PgmName="ConstLum-0:Time6" AccessType="RWL" Volatile="false" PermW="9">
        [EcgMapping("ConstLum-0:Time6", true)]
        private IEcgPropertyValue _time6;
        public int Time6
        {
            get { return _time6.IntValue; }
            set { _time6.IntValue = value; }
        }

        // <Property PgmName="ConstLum-0:Time7" AccessType="RWL" Volatile="false" PermW="9">
        [EcgMapping("ConstLum-0:Time7", true)]
        private IEcgPropertyValue _time7;
        public int Time7
        {
            get { return _time7.IntValue; }
            set { _time7.IntValue = value; }
        }

        // <Property PgmName="ConstLum-0:Time8" AccessType="RWL" Volatile="false" PermW="9">
        [EcgMapping("ConstLum-0:Time8", true)]
        private IEcgPropertyValue _time8;
        public int Time8
        {
            get { return _time8.IntValue; }
            set { _time8.IntValue = value; }
        }
        #endregion

        #region AdjustmentLevel
        // <Property PgmName = "ConstLum-0:AdjustmentLevel1" AccessType="RWL" Volatile="false" PermW="18">
        [EcgMapping("ConstLum-0:AdjustmentLevel1", true)]
        private IEcgPropertyValue _adjustmentLevel1;
        public int AdjustmentLevel1
        {
            get { return _adjustmentLevel1.IntValue; }
            set { _adjustmentLevel1.IntValue = value; }
        }

        // <Property PgmName = "ConstLum-0:AdjustmentLevel2" AccessType="RWL" Volatile="false" PermW="18">
        [EcgMapping("ConstLum-0:AdjustmentLevel2", true)]
        private IEcgPropertyValue _adjustmentLevel2;
        public int AdjustmentLevel2
        {
            get { return _adjustmentLevel2.IntValue; }
            set { _adjustmentLevel2.IntValue = value; }
        }

        // <Property PgmName = "ConstLum-0:AdjustmentLevel3" AccessType="RWL" Volatile="false" PermW="18">
        [EcgMapping("ConstLum-0:AdjustmentLevel3", true)]
        private IEcgPropertyValue _adjustmentLevel3;
        public int AdjustmentLevel3
        {
            get { return _adjustmentLevel3.IntValue; }
            set { _adjustmentLevel3.IntValue = value; }
        }

        // <Property PgmName = "ConstLum-0:AdjustmentLevel4" AccessType="RWL" Volatile="false" PermW="18">
        [EcgMapping("ConstLum-0:AdjustmentLevel4", true)]
        private IEcgPropertyValue _adjustmentLevel4;
        public int AdjustmentLevel4 
        {
            get { return _adjustmentLevel4.IntValue; }
            set { _adjustmentLevel4.IntValue = value; }
        }

        // <Property PgmName = "ConstLum-0:AdjustmentLevel5" AccessType="RWL" Volatile="false" PermW="18">
        [EcgMapping("ConstLum-0:AdjustmentLevel5", true)]
        private IEcgPropertyValue _adjustmentLevel5;
        public int AdjustmentLevel5
        {
            get { return _adjustmentLevel5.IntValue; }
            set { _adjustmentLevel5.IntValue = value; }
        }

        // <Property PgmName = "ConstLum-0:AdjustmentLevel6" AccessType="RWL" Volatile="false" PermW="18">
        [EcgMapping("ConstLum-0:AdjustmentLevel6", true)]
        private IEcgPropertyValue _adjustmentLevel6;
        public int AdjustmentLevel6
        {
            get { return _adjustmentLevel6.IntValue; }
            set { _adjustmentLevel6.IntValue = value; }
        }

        // <Property PgmName = "ConstLum-0:AdjustmentLevel7" AccessType="RWL" Volatile="false" PermW="18">
        [EcgMapping("ConstLum-0:AdjustmentLevel7", true)]
        private IEcgPropertyValue _adjustmentLevel7;
        public int AdjustmentLevel7
        {
            get { return _adjustmentLevel7.IntValue; }
            set { _adjustmentLevel7.IntValue = value; }
        }

        // <Property PgmName = "ConstLum-0:AdjustmentLevel8" AccessType="RWL" Volatile="false" PermW="18">
        [EcgMapping("ConstLum-0:AdjustmentLevel8", true)]
        private IEcgPropertyValue _adjustmentLevel8;
        public int AdjustmentLevel8
        {
            get { return _adjustmentLevel8.IntValue; }
            set { _adjustmentLevel8.IntValue = value; }
        }
        #endregion
#pragma warning restore CS0649

        protected override string EnabledPropertyMappingName => "ConstLum-0:Command-ClmEnable";

        protected override bool IsEnabledAdvanced()
        {
            if (_enabledProperty == null)
            {
                Info.IsAvailable = false;
                return false;
            }

            return true;
        }

        public TimeOuput Times { get; set; }
        public TimeOuput TimesMin { get; set; }
        public TimeOuput TimesMax { get; set; }
        public TimeOuput TimesOff { get; set; }

        public AdjustmentLevelOutput AdjustmentLevels { get; set; }

        public AdjustmentLevelOutput AdjustmentLevelsMin { get; set; }

        public AdjustmentLevelOutput AdjustmentLevelsMax { get; set; }

        public AdjustmentLevelOutput AdjustmentLevelsOff { get; set; }

        public ConstantLumenFeature(IMvxMessenger messenger, IConfigurationService configurationService) : base(messenger, configurationService)
        {
        }

        public override void OnConfigurationChange(Dictionary<String, OperatingMode> operatingModesDictonary, Dictionary<String, EcgProperty> propertyDictonary)
        {
            Init();
        }

        public override void OnConfigurationPropertiesRead()
        {
            UpdateAfterConfigurationPropertiesRead();
        }

        private void Init()
        {
            InitTimes();
            InitAdjustmentLevels();
        }

        private void UpdateAfterConfigurationPropertiesRead()
        {
            UpdateTimes();
            UpdateAdjustmentLevels();
        }

        private void InitTimes()
        {
            Times = new TimeOuput();

            TimesMin = new TimeOuput();
            TimesMax= new TimeOuput();
            TimesOff = new TimeOuput();
        }

        private void InitAdjustmentLevels()
        {
            AdjustmentLevels = new AdjustmentLevelOutput();

            AdjustmentLevelsMin = new AdjustmentLevelOutput();
            AdjustmentLevelsMax = new AdjustmentLevelOutput();
            AdjustmentLevelsOff = new AdjustmentLevelOutput();
        }

        private void UpdateTimes()
        {
            Times.Element1 = _time1.IntValue;
            Times.Element2 = _time2.IntValue;
            Times.Element3 = _time3.IntValue;
            Times.Element4 = _time4.IntValue;
            Times.Element5 = _time5.IntValue;
            Times.Element6 = _time6.IntValue;
            Times.Element7 = _time7.IntValue;
            Times.Element8 = _time8.IntValue;

            TimesMin.Element1 = _time1.IntMinValue;
            TimesMin.Element2 = _time2.IntMinValue;
            TimesMin.Element3 = _time3.IntMinValue;
            TimesMin.Element4 = _time4.IntMinValue;
            TimesMin.Element5 = _time5.IntMinValue;
            TimesMin.Element6 = _time6.IntMinValue;
            TimesMin.Element7 = _time7.IntMinValue;
            TimesMin.Element8 = _time8.IntMinValue;

            TimesMax.Element1 = _time1.IntMaxValue;
            TimesMax.Element2 = _time2.IntMaxValue;
            TimesMax.Element3 = _time3.IntMaxValue;
            TimesMax.Element4 = _time4.IntMaxValue;
            TimesMax.Element5 = _time5.IntMaxValue;
            TimesMax.Element6 = _time6.IntMaxValue;
            TimesMax.Element7 = _time7.IntMaxValue;
            TimesMax.Element8 = _time8.IntMaxValue;

            TimesOff.Element1 = _time1.IntOffValue;
            TimesOff.Element2 = _time2.IntOffValue;
            TimesOff.Element3 = _time3.IntOffValue;
            TimesOff.Element4 = _time4.IntOffValue;
            TimesOff.Element5 = _time5.IntOffValue;
            TimesOff.Element6 = _time6.IntOffValue;
            TimesOff.Element7 = _time7.IntOffValue;
            TimesOff.Element8 = _time8.IntOffValue;
        }

        private void UpdateAdjustmentLevels()
        {
            AdjustmentLevels.Element1 = _adjustmentLevel1.IntValue;
            AdjustmentLevels.Element2 = _adjustmentLevel2.IntValue;
            AdjustmentLevels.Element3 = _adjustmentLevel3.IntValue;
            AdjustmentLevels.Element4 = _adjustmentLevel4.IntValue;
            AdjustmentLevels.Element5 = _adjustmentLevel5.IntValue;
            AdjustmentLevels.Element6 = _adjustmentLevel6.IntValue;
            AdjustmentLevels.Element7 = _adjustmentLevel7.IntValue;
            AdjustmentLevels.Element8 = _adjustmentLevel8.IntValue;

            AdjustmentLevelsMin.Element1 = _adjustmentLevel1.IntMinValue;
            AdjustmentLevelsMin.Element2 = _adjustmentLevel2.IntMinValue;
            AdjustmentLevelsMin.Element3 = _adjustmentLevel3.IntMinValue;
            AdjustmentLevelsMin.Element4 = _adjustmentLevel4.IntMinValue;
            AdjustmentLevelsMin.Element5 = _adjustmentLevel5.IntMinValue;
            AdjustmentLevelsMin.Element6 = _adjustmentLevel6.IntMinValue;
            AdjustmentLevelsMin.Element7 = _adjustmentLevel7.IntMinValue;
            AdjustmentLevelsMin.Element8 = _adjustmentLevel8.IntMinValue;

            AdjustmentLevelsMax.Element1 = _adjustmentLevel1.IntMaxValue;
            AdjustmentLevelsMax.Element2 = _adjustmentLevel2.IntMaxValue;
            AdjustmentLevelsMax.Element3 = _adjustmentLevel3.IntMaxValue;
            AdjustmentLevelsMax.Element4 = _adjustmentLevel4.IntMaxValue;
            AdjustmentLevelsMax.Element5 = _adjustmentLevel5.IntMaxValue;
            AdjustmentLevelsMax.Element6 = _adjustmentLevel6.IntMaxValue;
            AdjustmentLevelsMax.Element7 = _adjustmentLevel7.IntMaxValue;
            AdjustmentLevelsMax.Element8 = _adjustmentLevel8.IntMaxValue;

            AdjustmentLevelsOff.Element1 = _adjustmentLevel1.IntOffValue;
            AdjustmentLevelsOff.Element2 = _adjustmentLevel2.IntOffValue;
            AdjustmentLevelsOff.Element3 = _adjustmentLevel3.IntOffValue;
            AdjustmentLevelsOff.Element4 = _adjustmentLevel4.IntOffValue;
            AdjustmentLevelsOff.Element5 = _adjustmentLevel5.IntOffValue;
            AdjustmentLevelsOff.Element6 = _adjustmentLevel6.IntOffValue;
            AdjustmentLevelsOff.Element7 = _adjustmentLevel7.IntOffValue;
            AdjustmentLevelsOff.Element8 = _adjustmentLevel8.IntOffValue;
        }
    }
}