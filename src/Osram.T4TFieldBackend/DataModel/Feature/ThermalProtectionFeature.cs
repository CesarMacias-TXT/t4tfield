﻿using MvvmCross.Plugin.Messenger;
using Osram.TFTBackend.ConfigurationService.Contracts;
using Osram.TFTBackend.ConfigurationService.DataModel;
using Osram.TFTBackend.DataModel.Feature.Contracts;
using Osram.TFTBackend.DataModel.Feature.Helper;

namespace Osram.TFTBackend.DataModel.Feature
{
    public class ThermalProtectionFeature : FeatureImpl, IThermalProtectionFeature
    {
#pragma warning disable CS0649
        // <Property PgmName="ThermProt-2:Command-TempMeasureEnable" AccessType="RWL" Volatile="false" PermW="10">
        [EcgMapping("ThermProt-2:Command-TempMeasureEnable", true)]
        private IEcgPropertyValue _temperatureMeasureEnable;
        public ThermalProtectionMode ThermalProtectionMode
        {
            get { return (ThermalProtectionMode)_temperatureMeasureEnable.IntValue; }
            set { _temperatureMeasureEnable.IntValue = (int)value; }
        }

        // <Property PgmName="ThermProt-2:StartDeratingResistance" AccessType="RWL" Volatile="false" PermW="10">
        [EcgMapping("ThermProt-2:StartDeratingResistance", true)]
        private IEcgPropertyValue _startDeratingResistance;
        public int StartDeratingResistance
        {
            get { return _startDeratingResistance.IntValue; }
            set { _startDeratingResistance.IntValue = value; }
        }

        // <Property PgmName="ThermProt-2:EndDeratingResistance" AccessType="RWL" Volatile="false" PermW="10">
        [EcgMapping("ThermProt-2:EndDeratingResistance", true)]
        private IEcgPropertyValue _endDeratingResistance;
        public int EndDeratingResistance
        {
            get { return _endDeratingResistance.IntValue; }
            set { _endDeratingResistance.IntValue = value; }
        }

        // <Property PgmName="ThermProt-2:ShutOffResistance" AccessType="RWL" Volatile="false" PermW="10">
        [EcgMapping("ThermProt-2:ShutOffResistance", true)]
        private IEcgPropertyValue _shutOffResistance;
        public int ShutOffValue
        {
            get { return _shutOffResistance.IntValue; }
            set { _shutOffResistance.IntValue = value; }
        }
        public bool ShutOff => !(_shutOffResistance.IntValue == _shutOffResistance.IntOffValue);

        // <Property PgmName="ThermProt-2:FinalPowerReductionLevel" AccessType="RWL" Volatile="false" PermW="10">
        [EcgMapping("ThermProt-2:FinalPowerReductionLevel", true)]
        private IEcgPropertyValue _finalPowerReductionLevel;
        public int FinalPowerReductionLevel
        {
            get { return _finalPowerReductionLevel.IntValue; }
            set { _finalPowerReductionLevel.IntValue = value; }
        }
#pragma warning restore CS0649

        protected override string EnabledPropertyMappingName => "ThermProt-2:Command-TPMEnable";

        protected override bool IsEnabledAdvanced()
        {
            if (_enabledProperty == null)
            {
                Info.IsAvailable = false;
                return false;
            }

            return true;
        }

        public ThermalProtectionFeature(IMvxMessenger messenger, IConfigurationService configurationService) : base(messenger, configurationService)
        {
        }
    }
}