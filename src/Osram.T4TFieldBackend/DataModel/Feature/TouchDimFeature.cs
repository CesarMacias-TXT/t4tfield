﻿using MvvmCross.Plugin.Messenger;
using Osram.TFTBackend.ConfigurationService.Contracts;
using Osram.TFTBackend.ConfigurationService.DataModel;
using Osram.TFTBackend.DataModel.Feature.Contracts;
using Osram.TFTBackend.DataModel.Feature.Helper;

namespace Osram.TFTBackend.DataModel.Feature
{
    public class TouchDimFeature : FeatureImpl, ITouchDimFeature
    {
#pragma warning disable CS0649
        [EcgMapping("MainsFrequency", true)]
        private IEcgPropertyValue _frequency;
        public int Frequency
        {
            get { return _frequency.IntValue; }
            set { _frequency.IntValue = (int)value; }
        }

        [EcgMapping("TDiCorridor-1:TDConfig-ShortPushEnable", false, "TDiCorridor-5:TDConfig-ShortPushEnable")]
        private IEcgPropertyValue _switchOnOff;
        public bool SwitchOnOff => !(_switchOnOff.IntValue == _switchOnOff.IntOffValue);

        [EcgMapping("TDiCorridor-1:TDConfig-DoublPushEnable", false, "TDiCorridor-5:TDConfig-DoublPushEnable")]
        private IEcgPropertyValue _setResetLevel;
        public bool SetResetLevel => !(_setResetLevel.IntValue == _setResetLevel.IntOffValue);

        [EcgMapping("TDiCorridor-1:TDConfig-LongPushEnable", false, "TDiCorridor-5:TDConfig-LongPushEnable")]
        private IEcgPropertyValue _dimUpDown;
        public bool DimUpDown => !(_dimUpDown.IntValue == _dimUpDown.IntOffValue);

        [EcgMapping("TDiCorridor-1:TDPowerOnLevel", false, "TDiCorridor-5:TDPowerOnLevel")]
        private IEcgPropertyValue _mainsPowerLevel;
        public int MainsPowerLevel
        {
            get { return _mainsPowerLevel.IntValue; }
            set { _mainsPowerLevel.IntValue = (int)value; }
        }

        [EcgMapping("TDiCorridor-1:FadeUpTime", false, "TDiCorridor-5:FadeUpTime")]
        private IEcgPropertyValue _fadeTime1;
        public int FadeTime1
        {
            get { return _fadeTime1.IntValue; }
            set { _fadeTime1.IntValue = (int)value; }
        }

        [EcgMapping("TDiCorridor-1:TDSwitchOnLevel", false, "TDiCorridor-5:TDSwitchOnLevel")]
        private IEcgPropertyValue _operativeDaliLevel;
        public int OperativeDaliLevel
        {
            get { return _operativeDaliLevel.IntValue; }
            set { _operativeDaliLevel.IntValue = (int)value; }
        }

        [EcgMapping("TDiCorridor-1:PSTimeout", false, "TDiCorridor-5:PSTimeout")]
        private IEcgPropertyValue _holdTime2;
        public int HoldTime2
        {
            get { return _holdTime2.IntValue; }
            set { _holdTime2.IntValue = (int)value; }
        }

        [EcgMapping("TDiCorridor-1:CFFadeTime2", false, "TDiCorridor-5:CFFadeTime2")]
        private IEcgPropertyValue _fadeTime3;
        public int FadeTime3
        {
            get { return _fadeTime3.IntValue; }
            set { _fadeTime3.IntValue = (int)value; }
        }

        [EcgMapping("TDiCorridor-1:FadeDownTime", false, "TDiCorridor-5:FadeDownTime")]
        private IEcgPropertyValue _fadeTime4;
        public int FadeTime4
        {
            get { return _fadeTime4.IntValue; }
            set { _fadeTime4.IntValue = (int)value; }
        }

        [EcgMapping("", true)]
        private IEcgPropertyValue _standbyDaliLevel;
        public int StandbyDaliLevel
        {
            get { return _standbyDaliLevel.IntValue; }
            set { _standbyDaliLevel.IntValue = (int)value; }
        }

        [EcgMapping("TDiCorridor-1:TDStandByTime", false, "TDiCorridor-5:TDStandByTime")]
        private IEcgPropertyValue _holdTime5;
        public int HoldTime5
        {
            get { return _holdTime5.IntValue; }
            set { _holdTime5.IntValue = (int)value; }
        }

        [EcgMapping("TDiCorridor-1:TDConfig-LSEnable", false, "TDiCorridor-5:TDConfig-LSEnable")]
        private IEcgPropertyValue _lsActive;
        public bool LsActive => !(_lsActive.IntValue == _lsActive.IntOffValue);

        [EcgMapping("TDiCorridor-1:TDConfig-PSEnable", false, "TDiCorridor-5:TDConfig-PSEnable")]
        private IEcgPropertyValue _pdActive;
        public bool PdActive => !(_pdActive.IntValue == _pdActive.IntOffValue);

        [EcgMapping("TDiCorridor-1:TDConfig-HolidayEnable", false, "TDiCorridor-1:TDConfig-HolidayEnable")]
        private IEcgPropertyValue _pdHolidayMode;
        public bool PdHolidayMode => !(_pdHolidayMode.IntValue == _pdHolidayMode.IntOffValue);

        [EcgMapping("TDiCorridor-1:TDConfig-AutoDisPSEnable", false, "TDiCorridor-5:TDConfig-AutoDisPSEnable")]
        private IEcgPropertyValue _autoDisablePdActive;
        public bool AutoDisablePdActive => !(_autoDisablePdActive.IntValue == _autoDisablePdActive.IntOffValue);

        [EcgMapping("TDiCorridor-5:TD2PowerOnColour", false)]
        private IEcgPropertyValue _mainsPowerColour;
        public int MainsPowerColour
        {
            get { return _mainsPowerColour.IntValue; }
            set { _mainsPowerColour.IntValue = (int)value; }
        }

        [EcgMapping("TDiCorridor-5:TD2SwitchOnColour", false)]
        private IEcgPropertyValue _switchOnColour;
        public int SwitchOnColour
        {
            get { return _switchOnColour.IntValue; }
            set { _switchOnColour.IntValue = (int)value; }
        }

        [EcgMapping("TDiCorridor-5:TD2PowerOnLevel", false)]
        private IEcgPropertyValue _mainsPowerLevel2;
        public int MainsPowerLevel2
        {
            get { return _mainsPowerLevel2.IntValue; }
            set { _mainsPowerLevel2.IntValue = (int)value; }
        }

        [EcgMapping("TDiCorridor-5:TD2SwitchOnLevel", false)]
        private IEcgPropertyValue _operativeLevel;
        public int OperativeLevel
        {
            get { return _operativeLevel.IntValue; }
            set { _operativeLevel.IntValue = (int)value; }
        }

        [EcgMapping("TDiCorridor-5:Config2-ShortPushEnable", false)]
        private IEcgPropertyValue _switchOnOff2;
        public bool SwitchOnOff2 => !(_switchOnOff2.IntValue == _switchOnOff2.IntOffValue);

        [EcgMapping("TDiCorridor-5:Config2-DoublePushEnable", false)]
        private IEcgPropertyValue _setResetLevel2;
        public bool SetResetLevel2 => !(_setResetLevel2.IntValue == _setResetLevel2.IntOffValue);

        [EcgMapping("TDiCorridor-5:Config2-LongPushEnable", false)]
        private IEcgPropertyValue _dimUpDown2;
        public bool DimUpDown2 => !(_dimUpDown2.IntValue == _dimUpDown2.IntOffValue);

#pragma warning restore CS0649

        protected override string EnabledPropertyMappingName => "TDiCorridor-1:TDConfig-TDCFEnable";

        protected override bool IsEnabledAdvanced()
        {
            if (_enabledProperty == null)
            {
                Info.IsAvailable = false;
                return false;
            }

            return true;
        }

        public TouchDimFeature(IMvxMessenger messenger, IConfigurationService configurationService) : base(messenger, configurationService)
        {
        }
    }
}
