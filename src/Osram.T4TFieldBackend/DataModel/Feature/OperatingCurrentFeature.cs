﻿using MvvmCross.Plugin.Messenger;
using Osram.TFTBackend.ConfigurationService.Contracts;
using Osram.TFTBackend.ConfigurationService.DataModel;
using Osram.TFTBackend.DataModel.Feature.Contracts;
using Osram.TFTBackend.DataModel.Feature.Helper;

namespace Osram.TFTBackend.DataModel.Feature
{
    public class OperatingCurrentFeature : FeatureImpl, IOperatingCurrentFeature
    {
#pragma warning disable CS0649
        // <Property PgmName="OTConfig-2:DefaultOpCurrent" AccessType="RWL" Volatile="false" PermW="8">
        [EcgMapping("OTConfig-2:DefaultOpCurrent", true, "PwmConfig-0:DefaultOpCurrent", "OTConfig-3:DefaultOpCurrent")]
        private IEcgPropertyValue _operatingCurrentProperty;
        public int OperatingCurrent
        {
            get { return _operatingCurrentProperty.IntValue; }
            set { _operatingCurrentProperty.IntValue = value; }
        }

        // <Property PgmName="OTConfig-2:ModeSetting-EnableLEDset2Interface" AccessType="RWL" Volatile="false" PermW="8">
        [EcgMapping("OTConfig-2:ModeSetting-EnableLEDset2Interface", true, "PwmConfig-0:EnableLEDset", "OTConfig-3:ModeSetting-EnableLEDset2Interface")]
        private IEcgPropertyValue _operatingCurrentMode;
        public OperatingCurrentMode OperatingCurrentMode
        {
            get { return (OperatingCurrentMode)_operatingCurrentMode.IntValue; }
            set { _operatingCurrentMode.IntValue = (int)value; }
        }
#pragma warning restore CS0649

        public OperatingCurrentFeature(IMvxMessenger messenger, IConfigurationService configurationService) : base(messenger, configurationService)
        {
        }
    }
}