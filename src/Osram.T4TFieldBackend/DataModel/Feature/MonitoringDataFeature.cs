﻿using MvvmCross.Plugin.Messenger;
using Osram.TFTBackend.ConfigurationService.Contracts;
using Osram.TFTBackend.ConfigurationService.DataModel;
using Osram.TFTBackend.DataModel.Feature.Contracts;
using Osram.TFTBackend.DataModel.Feature.Helper;

namespace Osram.TFTBackend.DataModel.Feature
{
    public class MonitoringDataFeature : FeatureImpl, IMonitoringDataFeature
    {
#pragma warning disable CS0649
        [EcgMapping("ValData202-0:MonitoringDataContent", true)]
        private IEcgPropertyValue _activeEnergyPower;
        public ProgrammingValue ActiveEnergyPower
        {
            get { return _activeEnergyPower.ProgrammingValue; }
            set { _activeEnergyPower.ProgrammingValue = value; }
        }

        [EcgMapping("ValData203-0:MonitoringDataContent", true)]
        private IEcgPropertyValue _apparentEnergyPower;
        public ProgrammingValue ApparentEnergyPower
        {
            get { return _apparentEnergyPower.ProgrammingValue; }
            set { _apparentEnergyPower.ProgrammingValue = value; }
        }

        [EcgMapping("ValData204-0:MonitoringDataContent", true)]
        private IEcgPropertyValue _loadSideEnergyPower;
        public ProgrammingValue LoadSideEnergyPower
        {
            get { return _loadSideEnergyPower.ProgrammingValue; }
            set { _loadSideEnergyPower.ProgrammingValue = value; }
        }

        [EcgMapping("ValData205-0:MonitoringDataContent", true)]
        private IEcgPropertyValue _controlGearDiagnostics;
        public ProgrammingValue ControlGearDiagnostics
        {
            get { return _controlGearDiagnostics.ProgrammingValue; }
            set { _controlGearDiagnostics.ProgrammingValue = value; }
        }

        [EcgMapping("ValData206-0:MonitoringDataContent", true)]
        private IEcgPropertyValue _lightSourceDiagnostics;
        public ProgrammingValue LightSourceDiagnostics
        {
            get { return _lightSourceDiagnostics.ProgrammingValue; }
            set { _lightSourceDiagnostics.ProgrammingValue = value; }
        }

#pragma warning restore CS0649

        protected override string EnabledPropertyMappingName => "ValData202-0";

        public MonitoringDataFeature(IMvxMessenger messenger, IConfigurationService configurationService) : base(messenger, configurationService)
        {
        }
    }
}
