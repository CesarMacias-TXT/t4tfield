﻿using MvvmCross.Plugin.Messenger;
using Osram.TFTBackend.ConfigurationService.Contracts;
using Osram.TFTBackend.ConfigurationService.DataModel;
using Osram.TFTBackend.DataModel.Feature.Contracts;
using Osram.TFTBackend.DataModel.Feature.Helper;
using Osram.TFTBackend.DataModel.Helper;
using Osram.TFTBackend.LocationService;
using Osram.TFTBackend.Messaging;
using System;
using System.Collections.Generic;
using System.Linq;
using static Osram.TFTBackend.LocationService.EcgLocation;

namespace Osram.TFTBackend.DataModel.Feature
{
    public class AstroDimFeature : FeatureImpl, IAstroDimFeature
    {
#pragma warning disable CS0649
        [EcgMapping("Astro-2:StartupFadeTime", true)]
        private IEcgPropertyValue _switchOnFadeTimeProperty;
        public int SwitchOnFadeTime
        {
            get { return _switchOnFadeTimeProperty.IntValue; }
            set { _switchOnFadeTimeProperty.IntValue = value; }
        }

        // <Property PgmName="Astro-2:SwitchOFFFadeTime" AccessType="RWL" Volatile="false">
        // TODO: has Multiplier - represent this in the EcgMemoryCell Metadata reference that's behind this property
        [EcgMapping("Astro-2:SwitchOFFFadeTime", true)]
        private IEcgPropertyValue _switchOffFadeTimeProperty;
        public int SwitchOffFadeTime
        {
            get { return _switchOffFadeTimeProperty.IntValue; }
            set { _switchOffFadeTimeProperty.IntValue = value; }
        }

        // <Property PgmName = "Astro-2:AstroDIMFadeTime" AccessType="RWL" Volatile="false">
        // TODO: has Multiplier - represent this in the EcgMemoryCell Metadata reference that's behind this property 
        [EcgMapping("Astro-2:AstroDIMFadeTime", true)]
        private IEcgPropertyValue _astroDimFadeTimeProperty;
        public int AstroDimFadeTime
        {
            get { return _astroDimFadeTimeProperty.IntValue; }
            set { _astroDimFadeTimeProperty.IntValue = value; }
        }

        //<Property PgmName = "Astro-2:NominalLevel" AccessType="RWL" Volatile="false" PermW="18">
        [EcgMapping("Astro-2:NominalLevel", true)]
        private IEcgPropertyValue _astroDimNominalLevel;
        public int NominalLevel
        {
            get { return _astroDimNominalLevel.IntValue; }
            set { _astroDimNominalLevel.IntValue = value; }
        }

        //<Property PgmName = "Astro-2:DimStartTime" AccessType="RWL" Volatile="false" PermW="18">
        [EcgMapping("Astro-2:DimStartTime", true)]
        private IEcgPropertyValue _astroDimStartTime;
        public int DimStartTime
        {
            get { return _astroDimStartTime.IntValue; }
            set { _astroDimStartTime.IntValue = value; }
        }

        //<Property PgmName = "Astro-2:Control-AstroDimSubMode" AccessType="RWL" Volatile="false" PermW="17">
        [EcgMapping("Astro-2:Control-AstroDimSubMode", true)]
        private IEcgPropertyValue _astroDimMode;
        public AstroDimMode AstroDimMode
        {
            get { return (AstroDimMode)_astroDimMode.IntValue; }
            set { _astroDimMode.IntValue = (int)value; }
        }

        //<RatedDimmingRange Min="9.8" Max="100"></RatedDimmingRange>
        [EcgMapping("RatedDimmingRange")]
        private IEcgPropertyValue _ratedDimmingRange;
        public int MaxDimmingRange
        { get { return _ratedDimmingRange.IntMaxValue; } }
        public int MinDimmingRange
        { get { return _ratedDimmingRange.IntMinValue; } }

        #region Location
        //<Property PgmName = "Astro-2:Latitude" AccessType="RWL" Volatile="false" PermW="18">
        [EcgMapping("Astro-2:Latitude", true)]
        private IEcgPropertyValue _latitude;
        public int Latitude
        {
            get { return (int)_latitude.ProgrammingValue; }
            set { _latitude.ProgrammingValue = (int)value; }
        }

        //<Property PgmName = "Astro-2:Longitude" AccessType="RWL" Volatile="false" PermW="18">
        [EcgMapping("Astro-2:Longitude", true)]
        private IEcgPropertyValue _longitude;
        public int Longitude
        {
            get { return (int)_longitude.ProgrammingValue; }
            set { _longitude.ProgrammingValue = (int)value; }
        }

        //<Property PgmName = "Astro-2:UTCTimeShift" AccessType="RWL" Volatile="false" PermW="18">
        [EcgMapping("Astro-2:UTCTimeShift", true)]
        private IEcgPropertyValue _utcTimeShift;
        public double UtcTimeShift
        {
            get { return _utcTimeShift.IntValue; }
            set { _utcTimeShift.IntValue = (int)value; }
        }

        #endregion

        #region DimDurationsMapping
        // <Property PgmName = "Astro-2:DimDuration1" AccessType="RWL" Volatile="false">
        [EcgMapping("Astro-2:DimDuration1", true)]
        private IEcgPropertyValue _astroDimDuration1;
        // <Property PgmName = "Astro-2:DimDuration2" AccessType="RWL" Volatile="false">
        [EcgMapping("Astro-2:DimDuration2", true)]
        private IEcgPropertyValue _astroDimDuration2;
        // <Property PgmName = "Astro-2:DimDuration3" AccessType="RWL" Volatile="false">
        [EcgMapping("Astro-2:DimDuration3", true)]
        private IEcgPropertyValue _astroDimDuration3;
        #endregion

        #region DimLevelsMapping
        // <Property PgmName = "Astro-2:DimLevel1" AccessType="RWL" Volatile="false" PermW="18">
        [EcgMapping("Astro-2:DimLevel1", true)]
        private IEcgPropertyValue _astroDimLevel1;
        // <Property PgmName = "Astro-2:DimLevel2" AccessType="RWL" Volatile="false" PermW="18">
        [EcgMapping("Astro-2:DimLevel2", true)]
        private IEcgPropertyValue _astroDimLevel2;
        // <Property PgmName = "Astro-2:DimLevel3" AccessType="RWL" Volatile="false" PermW="18">
        [EcgMapping("Astro-2:DimLevel3", true)]
        private IEcgPropertyValue _astroDimLevel3;
        // <Property PgmName = "Astro-2:DimLevel4" AccessType="RWL" Volatile="false" PermW="18">
        [EcgMapping("Astro-2:DimLevel4", true)]
        private IEcgPropertyValue _astroDimLevel4;
        #endregion

#pragma warning restore CS0649

        protected override String EnabledPropertyMappingName => "Astro-2:Control-AstroEnable";

        public DimmingDuration DimmingDurations
        { get; set; }

        public OutputLevel DimmingLevels
        { get; set; }

        public OutputLevel DimmingLevelsMin
        { get; set; }

        public OutputLevel DimmingLevelsMax
        { get; set; }

        public OutputLevel DimmingLevelsOff
        { get; set; }

        private Location location;
        public Location Location
        {
            get
            {
                if (location == null)
                    location = GetLocation();
                return location;
            }
            set { location = value; }
        }

        public string OperatingModes
        { get; set; }

        /// <summary>
        /// The real midnight time value.
        /// </summary>
        private DateTime realMidnightTime = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 0, 0, 0, 0);

        public AstroDimFeature(IMvxMessenger messenger, IConfigurationService configurationService) : base(messenger, configurationService)
        {
        }

        public override void OnConfigurationChange(Dictionary<String, OperatingMode> operatingModesDictonary, Dictionary<String, EcgProperty> propertyDictonary)
        {
            Init();
        }

        public override void OnConfigurationPropertiesRead()
        {
            UpdateAfterConfigurationPropertiesRead(); 
        }

        private void Init()
        {
            //TODO to remove when retreived from xml conf
            OperatingModes = "operating";
            //-------------------------------------------

            InitDimmingDurations();
            InitDimmingLevels();
        }

        private void UpdateAfterConfigurationPropertiesRead()
        {
            UpdateDimmingDurations();
            UpdateDimmingLevels();
        }

        private void InitDimmingLevels()
        {
            DimmingLevels = new OutputLevel();
            DimmingLevels.Changed += ((s, e) => UpdateOutputValuesToDataModel(e));

            DimmingLevelsMin = new OutputLevel();
            DimmingLevelsMax = new OutputLevel();
            DimmingLevelsOff = new OutputLevel();
        }

        private void UpdateDimmingLevels()
        {
            DimmingLevels.Element1 = _astroDimNominalLevel.IntValue;
            DimmingLevels.Element2 = _astroDimLevel1.IntValue;
            DimmingLevels.Element3 = _astroDimLevel2.IntValue;
            DimmingLevels.Element4 = _astroDimLevel3.IntValue;
            DimmingLevels.Element5 = _astroDimLevel4.IntValue;

            DimmingLevelsMin.Element1 = _astroDimNominalLevel.IntMinValue;
            DimmingLevelsMin.Element2 = _astroDimLevel1.IntMinValue;
            DimmingLevelsMin.Element3 = _astroDimLevel2.IntMinValue;
            DimmingLevelsMin.Element4 = _astroDimLevel3.IntMinValue;
            DimmingLevelsMin.Element5 = _astroDimLevel4.IntMinValue;

            DimmingLevelsMax.Element1 = _astroDimNominalLevel.IntMaxValue;
            DimmingLevelsMax.Element2 = _astroDimLevel1.IntMaxValue;
            DimmingLevelsMax.Element3 = _astroDimLevel2.IntMaxValue;
            DimmingLevelsMax.Element4 = _astroDimLevel3.IntMaxValue;
            DimmingLevelsMax.Element5 = _astroDimLevel4.IntMaxValue;

            DimmingLevelsOff.Element1 = _astroDimNominalLevel.IntOffValue;
            DimmingLevelsOff.Element2 = _astroDimLevel1.IntOffValue;
            DimmingLevelsOff.Element3 = _astroDimLevel2.IntOffValue;
            DimmingLevelsOff.Element4 = _astroDimLevel3.IntOffValue;
            DimmingLevelsOff.Element5 = _astroDimLevel4.IntOffValue;
        }

        private void InitDimmingDurations()
        {
            DimmingDurations = new DimmingDuration();
        }

        private void UpdateDimmingDurations()
        {
            DimmingDurations.Element1 = _astroDimDuration1.IntValue;
            DimmingDurations.Element2 = _astroDimDuration2.IntValue;
            DimmingDurations.Element3 = _astroDimDuration3.IntValue;
        }

        /// <summary>
        /// Gets Minimum Time Differences REQUIRED BETWEEN TWO STAMPS.
        /// </summary>
        /// <param name="is4DimDevice">Device name.</param>
        /// <param name="astroFadeTime">Astro Dim FadeTime.</param>
        /// <returns>Minimum Required Time Difference.</returns>
        private TimeSpan GetMinimumDiffrencesInTime(bool is4DimDevice, long astroFadeTime)
        {
            TimeSpan astroTimeStamp = new TimeSpan(0, 0, 0, 0, 0);
            TimeSpan minStep = new TimeSpan(0, 0, 0, 0, 0); //minutes
            TimeSpan requiredTimeStamp = new TimeSpan(0, 0, 0, 0, 0);

            if (is4DimDevice)
            {
                var AstroDimFadeTime = ConstantListValues.Instance.AstroDimRangeValues();
                //4Dim
                minStep = new TimeSpan(0, 0, 1, 0, 0);
                if (AstroDimFadeTime.Any(x => x.Key == astroFadeTime))
                {
                    string strAstroFadeTime = AstroDimFadeTime.Where(x => x.Key == astroFadeTime).FirstOrDefault().Value;
                    if (!strAstroFadeTime.Equals("OFF"))
                    {
                        int astrdodimSec = Convert.ToInt16(strAstroFadeTime.Split(':')[1]);
                        int astrdodimMinutes = astrdodimSec > 0 ? Convert.ToInt16(strAstroFadeTime.Split(':')[0]) + 1 : Convert.ToInt16(strAstroFadeTime.Split(':')[0]);
                        astroTimeStamp = new TimeSpan(0, 0, astrdodimMinutes, 0, 0);
                    }
                }
            }

            if (minStep < astroTimeStamp)
            {
                requiredTimeStamp = astroTimeStamp;
            }
            else
            {
                requiredTimeStamp = minStep;
            }

            return requiredTimeStamp;
        }

        public TimeValue GetDefaultReferenceScheduleTime()
        {
            DateTime virtualMidnight;
            if (AstroDimMode == AstroDimMode.TimeBased)
            {
                virtualMidnight = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 0, 0, 0, 0);
            }
            else
            {
                virtualMidnight = GetVirtualMidnight(CalculateMidnightShift(Location.Offset, Location.LatitudeValue, Location.LongitudeValue));
            }

            TimeSpan dimStartTime = _astroDimStartTime.IntDefaultValue != -1 ? new TimeSpan(0, 0, _astroDimStartTime.IntDefaultValue, 0, 0) : new TimeSpan(0, 0, 0, 0, 0);
            TimeSpan dimDuration1 = DimmingDurations.Element1 != -1 ? new TimeSpan(0, 0, (int)_astroDimDuration1.IntDefaultValue, 0, 0) : MinimumDifferencesInTime;
            TimeSpan dimDuration2 = DimmingDurations.Element2 != -1 ? new TimeSpan(0, 0, (int)_astroDimDuration2.IntDefaultValue, 0, 0) : MinimumDifferencesInTime;
            TimeSpan dimDuration3 = DimmingDurations.Element3 != -1 ? new TimeSpan(0, 0, (int)_astroDimDuration3.IntDefaultValue, 0, 0) : MinimumDifferencesInTime;

            TimeValue time = new TimeValue();

            time.Element1 = virtualMidnight;
            if (AstroDimMode == AstroDimMode.TimeBased)
            {
                time.Element2 = virtualMidnight.Add(dimStartTime);////vm +dst: 00:00 + DST ;
            }
            else
            {
                time.Element2 = virtualMidnight.Subtract(dimStartTime);////vm -dst;
            }
            time.Element3 = time.Element2.Add(dimDuration1);    ////T2 +dd1;
            time.Element4 = time.Element3.Add(dimDuration2);    ////T3 +dd2;
            time.Element5 = time.Element4.Add(dimDuration3);    ////T4 +dd2;
            time.Element6 = virtualMidnight.AddDays(1);
            return time;
        }

        public TimeValue GetTimeBasedReferenceScheduleTime()
        {
            TimeSpan dimStartTime = DimStartTime != -1 ? new TimeSpan(0, 0, DimStartTime, 0, 0) : new TimeSpan(0, 0, 0, 0, 0);
            TimeSpan dimDuration1 = DimmingDurations.Element1 != -1 ? new TimeSpan(0, 0, DimmingDurations.Element1, 0, 0) : MinimumDifferencesInTime;
            TimeSpan dimDuration2 = DimmingDurations.Element2 != -1 ? new TimeSpan(0, 0, DimmingDurations.Element2, 0, 0) : MinimumDifferencesInTime;
            TimeSpan dimDuration3 = DimmingDurations.Element3 != -1 ? new TimeSpan(0, 0, DimmingDurations.Element3, 0, 0) : MinimumDifferencesInTime;

            DateTime virtualMidnight = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 0, 0, 0, 0);


            TimeValue time = new TimeValue();
            time.Element1 = virtualMidnight;
            time.Element2 = virtualMidnight.Add(dimStartTime);////vm +dst: 00:00 + DST ;
            time.Element3 = time.Element2.Add(dimDuration1);    ////T2 +dd1;
            time.Element4 = time.Element3.Add(dimDuration2);    ////T3 +dd2;
            time.Element5 = time.Element4.Add(dimDuration3);    ////T4 +dd2;
            time.Element6 = virtualMidnight.AddDays(1);

            return time;
        }

        public TimeValue GetAstroBasedReferenceScheduleTime()
        {
            TimeSpan dimStartTime = DimStartTime != -1 ? new TimeSpan(0, 0, DimStartTime, 0, 0) : new TimeSpan(0, 0, 0, 0, 0);
            TimeSpan dimDuration1 = DimmingDurations.Element1 != -1 ? new TimeSpan(0, 0, DimmingDurations.Element1, 0, 0) : MinimumDifferencesInTime;
            TimeSpan dimDuration2 = DimmingDurations.Element2 != -1 ? new TimeSpan(0, 0, DimmingDurations.Element2, 0, 0) : MinimumDifferencesInTime;
            TimeSpan dimDuration3 = DimmingDurations.Element3 != -1 ? new TimeSpan(0, 0, DimmingDurations.Element3, 0, 0) : MinimumDifferencesInTime;

            DateTime virtualMidnight = GetVirtualMidnight(CalculateMidnightShift(Location.Offset, Location.LatitudeValue, Location.LongitudeValue));

            TimeValue time = new TimeValue();
            time.Element1 = virtualMidnight;
            time.Element2 = virtualMidnight.Subtract(dimStartTime);////vm -dst;
            time.Element3 = time.Element2.Add(dimDuration1);    ////T2 +dd1;
            time.Element4 = time.Element3.Add(dimDuration2);    ////T3 +dd2;
            time.Element5 = time.Element4.Add(dimDuration3);    ////T4 +dd2;
            //time.Element6 = virtualMidnight.AddDays(1);

            return time;
        }


        /// <summary>
        /// Gets the virtual midnight.
        /// </summary>
        /// <param name="midNightShift">The mid night shift.</param>
        /// <returns>Returns the virtual mid night time.</returns>
        public DateTime GetVirtualMidnight(int midNightShift)
        {
            DateTime realMidnigt = new DateTime(DateTime.Now.AddDays(1).Year, DateTime.Now.AddDays(1).Month, DateTime.Now.AddDays(1).Day, 0, 0, 0, 0);
            TimeSpan midNightSpan = new TimeSpan(0, 0, midNightShift, 0, 0);
            DateTime virtualMidnight = realMidnigt.Add(midNightSpan);
            return virtualMidnight;
        }

        /// <summary>
        ///  Gets Minimum Differences required between two time stamps.
        /// </summary>
        /// <value>Returns Minimum Differences In Time.</value>
        public TimeSpan MinimumDifferencesInTime
        {
            get
            {
                return GetMinimumDiffrencesInTime(true, AstroDimFadeTime);
            }
        }

        public Location GetLocation()
        {
            return EcgLocation.GetSelectedLocation(Latitude, Longitude, _utcTimeShift.IntValue);
        }

        /// <summary>
        /// Calculate average midnight shift for the selected location.
        /// selection location shall have latitude, longitude and offset parameters.
        /// </summary>
        /// <param name="selectedOffset">Selected location Offset.</param>
        /// <param name="selectedLatitude">Selected location Latitude Value.</param>
        /// <param name="selectedLongitude">Selected location Longitude.</param>
        /// <returns>Returns the average midnight value.</returns>
        public int CalculateMidnightShift(double selectedOffset, double selectedLatitude, double selectedLongitude)
        {
            double sunset = 0;
            double sunrise = 0;
            double accumulatedShift = 0;
            DateTime dateTimeRepeat = new DateTime(DateTime.Now.Year, 1, 1);
            for (int i = 1; i <= 366; i++)
            {
                SunDawn sunlocationCurrentDay;
                SunDawn sunlocationNextDay;
                AstroBasedUtils.GetSunLocation(dateTimeRepeat, out sunlocationCurrentDay, out sunlocationNextDay, selectedOffset, selectedLatitude, selectedLongitude);
                if (sunlocationCurrentDay != null && sunlocationNextDay != null)
                {
                    sunset = sunlocationCurrentDay.Set;
                    sunrise = sunlocationNextDay.Rise;
                }

                dateTimeRepeat = dateTimeRepeat.AddDays(1);
                double nightLength = 24 + sunrise - sunset;
                double virtualMidnight = sunset + (nightLength / 2);
                double midnightshift = virtualMidnight - 24;
                accumulatedShift += midnightshift;
            }

            return Convert.ToInt32(accumulatedShift * 60 / 366);
        }

        private void UpdateOutputValuesToDataModel(int level)
        {
            switch (level)
            {
                case 1:
                    NominalLevel = DimmingLevels.Element1;
                    break;
                case 2:
                    _astroDimLevel1.IntValue = DimmingLevels.Element2;
                    break;
                case 3:
                    _astroDimLevel2.IntValue = DimmingLevels.Element3;
                    break;
                case 4:
                    _astroDimLevel3.IntValue = DimmingLevels.Element4;
                    break;
                case 5:
                    _astroDimLevel4.IntValue = DimmingLevels.Element5;
                    break;
            }
        }

        /// <summary>
        /// Updates all Time based dim durations to respective data model parameters.
        /// </summary>
        public void UpdateDimDurationToDataModel(TimeValue time)
        {

            if (DimStartTime != -1 && time.Element2 != DateTime.MinValue)
            {
                if (AstroDimMode == AstroDimMode.TimeBased)
                    DimStartTime = GetTimeBasedDimStartTimeInMinutes(time.Element2);
                else
                    DimStartTime = GetDimStartTimeInMinutes(time.Element2);
            }

            if (_astroDimDuration1.IntValue != -1 && time.Element3 != DateTime.MinValue)
            {
                _astroDimDuration1.IntValue = Convert.ToInt32(time.Element3.Subtract(
                     time.Element2).TotalMinutes);
                DimmingDurations.Element1 = _astroDimDuration1.IntValue;
            }

            if (_astroDimDuration2.IntValue != -1 && time.Element4 != DateTime.MinValue)
            {
                _astroDimDuration2.IntValue = Convert.ToInt32(time.Element4.Subtract(
                   time.Element3).TotalMinutes);
                DimmingDurations.Element2 = _astroDimDuration2.IntValue;
            }

            if (_astroDimDuration3.IntValue != -1 && time.Element5 != DateTime.MinValue)
            {
                _astroDimDuration3.IntValue = Convert.ToInt32(time.Element5.Subtract(
                      time.Element4).TotalMinutes);
                DimmingDurations.Element3 = _astroDimDuration3.IntValue;
            }

            //publish mex for updating scheduler
            _messenger.Publish(new Events.AstroDimFeatureChangedEvent(this));
        }

        /// <summary>
        /// Gets DimStartTime value in minutes to be updated to data model.
        /// </summary>
        /// <param name="time2">Reference schedule Time value in time based mode.</param>
        /// <returns>Dim startTime in minutes.</returns>
        private int GetTimeBasedDimStartTimeInMinutes(DateTime time2)
        {
            TimeSpan dimStartTimeSpan = new TimeSpan(0, 0, 0);
            DateTime timeLevel2 = time2;
            dimStartTimeSpan = timeLevel2 - realMidnightTime;
            return Convert.ToInt32(dimStartTimeSpan.TotalMinutes);
        }

        /// <summary>
        /// Gets DimStartTime value change in minutes in astro based mode.
        /// </summary>
        /// <param name="time2">Reference schedule Time value.</param>
        /// <returns>Dim startTime in minutes.</returns>
        public int GetDimStartTimeInMinutes(DateTime time2)
        {
            TimeSpan dimStartTimeSpan = new TimeSpan(0, 0, 0);
            DateTime timeLevel2 = time2;
            DateTime virtualMidnight = GetVirtualMidnight(CalculateMidnightShift(Location.Offset, Location.LatitudeValue, Location.LongitudeValue));
            dimStartTimeSpan = virtualMidnight - timeLevel2;
            return Convert.ToInt32(dimStartTimeSpan.TotalMinutes);
        }

        /// <summary>
        /// Validates the range of output levels of astro Dim reference schedule group.
        /// </summary>
        /// <param name="myDouble">Value.</param>
        /// <param name="is4DIMDevice">Device type.</param>
        /// <param name="offSupported">Off value supported.</param>
        /// <param name="offValue">Off value.</param>
        /// <param name="minLevel">Output Level Min Value.</param>
        /// <param name="outputLevelMaxValue">Output Level Max Value.</param>
        /// <returns>Validated value.</returns>
        public double ValidateRangeOutputLevel(int level)
        {
            double myDouble = DimmingLevels[level];
            bool is4DIMDevice = true;
            int offValue = DimmingLevelsOff[level];
            double minLevel = DimmingLevelsMin[level];
            double outputLevelMaxValue = DimmingLevelsMax[level];
            //TODO check the not defined value in config -> default isn't good 
            //bool offSupported = offValue != default(int) ? true : false;
            bool offSupported = true;

            List<double> lstdb = new List<double>();

            if (is4DIMDevice)
            {
                myDouble = Math.Round(myDouble);
                lstdb = Get4DimList(offSupported, offValue, minLevel, outputLevelMaxValue);
            }
            //else
            //{
            //    myDouble = Math.Truncate(myDouble * 10) / 10;
            //    lstdb = Get3DimList(offSupported, offValue, minLevel, outputLevelMaxValue);
            //}

            //Find the closest one
            myDouble = lstdb.OrderBy(item => Math.Abs(myDouble - item)).First();
            return myDouble;
        }

        /// <summary>
        /// Method to get List of supported 4Dim output values.
        /// </summary>
        /// <param name="offSupported">Off value supported.</param>
        /// <param name="offValue">Off value.</param>
        /// <param name="outputLevelMinValue">Output Level Min Value.</param>
        /// <param name="outputLevelMaxValue">Output Level Max Value.</param>
        /// <returns>List of supported dim levels.</returns>
        private List<double> Get4DimList(bool offSupported, int offValue, double outputLevelMinValue, double outputLevelMaxValue)
        {
            List<double> lstdb = new List<double>();
            if (outputLevelMinValue < outputLevelMaxValue && outputLevelMinValue != outputLevelMaxValue)
            {
                lstdb = Device4Dim.Where(x => x >= outputLevelMinValue && x <= outputLevelMaxValue).ToList();
            }
            else
            {
                lstdb = new List<double> { outputLevelMinValue };
            }

            if (offSupported && lstdb.Count > 0)
            {
                if (offValue < outputLevelMinValue && offValue != outputLevelMaxValue)
                {
                    lstdb.Insert(0, offValue);
                }
                else if (offValue > outputLevelMaxValue && offValue != outputLevelMaxValue)
                {
                    lstdb.Add(offValue);
                }
            }

            return lstdb;
        }

        private List<double> device4Dim;
        /// <summary>
        /// Gets the 4DIM device values list.
        /// </summary>
        /// <value>
        /// The values list.
        /// </value>
        public List<double> Device4Dim
        {
            get
            {
                if (device4Dim == null)
                {
                    device4Dim = new List<double>()
                    {
                            0   ,10,
                            11  ,12 ,13 ,14 ,15 ,16 ,17 ,18 ,19 ,20,
                            21  ,22 ,23 ,24 ,25 ,26 ,27 ,28 ,29 ,30,
                            31  ,32 ,33 ,34 ,35 ,36 ,37 ,38 ,39 ,40,
                            41  ,42 ,43 ,44 ,45 ,46 ,47 ,48 ,49 ,50,
                            51  ,52 ,53 ,54 ,55 ,56 ,57 ,58 ,59 ,60,
                            61  ,62 ,63 ,64 ,65 ,66 ,67 ,68 ,69 ,70,
                            71  ,72 ,73 ,74 ,75 ,76 ,77 ,78 ,79 ,80,
                            81  ,82 ,83 ,84 ,85 ,86 ,87 ,88 ,89 ,90,
                            91  ,92 ,93 ,94 ,95 ,96 ,97 ,98 ,99 ,100
                    };
                }

                return device4Dim;
            }
        }
    }
}