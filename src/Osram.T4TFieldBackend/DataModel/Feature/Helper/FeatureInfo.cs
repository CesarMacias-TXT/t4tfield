﻿using Osram.TFTBackend.DataModel.Feature.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Osram.TFTBackend.DataModel.Feature.Helper
{
    class FeatureInfo : IFeatureInfo
    {
        public bool IsEnabled { get; set; }
        public bool IsAvailable { get; set; }
        public ProtectionLock Lock { get; set; }
        public FeatureStatus Status { get; set; }
        public FeatureType FeatureType { get; set; }

        public override string ToString()
        {
            return $"FeatureInfo: Enabled={IsEnabled} Available={IsAvailable} Lock={Lock} Status={Status}";
        }
    }
}
