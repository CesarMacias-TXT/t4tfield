﻿using System;

namespace Osram.TFTBackend.DataModel.Feature.Helper
{
    /// <summary>
    /// A custom attribute used to map feature properies to the corresponding xml properties.
    /// The name will be the property name from the xml. The optional parameter IsPermissionRelevant should be set to true
    /// when this property can be changed in the feature.
    /// </summary>
    [AttributeUsage(AttributeTargets.Field)]
    public class EcgMappingAttribute : Attribute
    {
        public EcgMappingAttribute(string name, bool isPermissionRelevant = false)
        {
            Name = name;
            AlternativeNames = null;
            IsPermissionRelevant = isPermissionRelevant;
        }

        public EcgMappingAttribute(string name, bool isPermissionRelevant = false, params string[] alternativeNames)
        {
            Name = name;
            AlternativeNames = alternativeNames;
            IsPermissionRelevant = isPermissionRelevant;
        }

        public String Name { get; }

        public string[] AlternativeNames { get; }

        public bool IsPermissionRelevant { get; }
    }
}
