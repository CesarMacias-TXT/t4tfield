using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using MvvmCross.Plugin.Messenger;
using Osram.TFTBackend.ConfigurationService.Contracts;
using Osram.TFTBackend.ConfigurationService.DataModel;
using Osram.TFTBackend.Messaging;
using Osram.TFTBackend.DataModel.Feature.Contracts;

namespace Osram.TFTBackend.DataModel.Feature.Helper
{
    public class FeatureImpl
    {
        protected readonly IMvxMessenger _messenger;
        private readonly MvxSubscriptionToken _tokenConfigurationChanged;
        private readonly MvxSubscriptionToken _tokenConfigurationPropertiesRead;
        protected IConfigurationService _theConfigurationService;

        public string Name { get; }
        public FeatureType FeatureType { get; set; }

        public IFeatureInfo Info { get; set; }

        protected virtual string EnabledPropertyMappingName => string.Empty;

        protected IEcgPropertyValue _enabledProperty;
        public bool Enabled => _enabledProperty == null ? IsEnabledAdvanced() : _enabledProperty.BoolValue && IsEnabledAdvanced();

        protected virtual bool IsEnabledAdvanced() => true;


        public HashSet<int> PermissionIndexes { get; private set; }

        public FeatureImpl(IMvxMessenger messenger, IConfigurationService configurationService)
        {
            _messenger = messenger;
            _theConfigurationService = configurationService;
            Name = GetType().Name;
            _tokenConfigurationChanged = _messenger.Subscribe<Events.ConfigurationChangedEvent>(OnConfigurationChangedEvent);
            _tokenConfigurationPropertiesRead = _messenger.Subscribe<Events.OnConfigurationPropertiesReadEvent>(OnConfigurationPropertiesReadEvent);

            PermissionIndexes = new HashSet<int>();

            Logger.TaggedTrace("Feature", $"Feature {Name} constructor");
        }

        private IEnumerable<FieldInfo> GetEcgMappingFields()
        {
            return GetType().GetRuntimeFields().Where(prop => prop.IsPrivate && prop.GetCustomAttributes(typeof(EcgMappingAttribute), true).Count() != 0);
        }

        private void MapEcgFeatureFields(IEnumerable<FieldInfo> featureFields, Dictionary<string, EcgProperty> propertyDictionary)
        {
            if (Info != null)
            {
                Info.IsAvailable = true;
            }

            foreach (var field in featureFields)
            {
                EcgMappingAttribute fieldAttribute = field.GetCustomAttribute<EcgMappingAttribute>();
                IEcgPropertyValue property = MapField(propertyDictionary, fieldAttribute);
                field.SetValue(this, property);
            }
        }

        private string GetMemoryBankName(string propertyMappingName)
        {
            return propertyMappingName.Substring(0, propertyMappingName.IndexOf(':'));
        }

        private IEcgPropertyValue MapField(Dictionary<string, EcgProperty> propertyDictionary, EcgMappingAttribute fieldAttribute)
        {
            EcgProperty property;
            if (propertyDictionary.TryGetValue(fieldAttribute.Name, out property))
            {
                if (fieldAttribute.IsPermissionRelevant && !property.ReadOnly)
                {
                    PermissionIndexes.Add(property.PermissionIndex);
                }
                return property;
            }
            else if (fieldAttribute.AlternativeNames != null)
            {   
                foreach(var alternativeName in fieldAttribute.AlternativeNames)
                {
                    if (propertyDictionary.TryGetValue(alternativeName, out property))
                    {
                        if (fieldAttribute.IsPermissionRelevant && !property.ReadOnly)
                        {
                            PermissionIndexes.Add(property.PermissionIndex);
                        }
                        return property;
                    }
                }                
            }
            if(Name == "AstroDimFeature")
            {
                Info.IsAvailable = false;
            }
            //if (Info != null)
            //{
            //    Info.IsAvailable = false;
            //}
            return NotFoundEcgProperty.Property;
        }

        public virtual void OnConfigurationChange(Dictionary<string, OperatingMode> operatingModesDictonary, Dictionary<string, EcgProperty> propertyDictonary) { }

        public virtual void OnConfigurationPropertiesRead() { }

        private void OnConfigurationPropertiesReadEvent(Events.OnConfigurationPropertiesReadEvent propertiesEvent)
        {
            OnConfigurationPropertiesRead();
        }

        private void OnConfigurationChangedEvent(Events.ConfigurationChangedEvent configurationEvent)
        {
            Logger.TaggedTrace("Feature", $"Feature {Name} ConfigurationChangedEvent");
            var propertyDictionary = _theConfigurationService.GetConfigurationPropertyDictionary();
            var fieldsToBeMapped = GetEcgMappingFields();
            MapEcgFeatureFields(fieldsToBeMapped, propertyDictionary);
            MapEnabledProperty(propertyDictionary);
            OnConfigurationChange(_theConfigurationService.GetOperatingModesDictonary(), propertyDictionary);
        }

        private void MapEnabledProperty(Dictionary<string, EcgProperty> propertyDictionary)
        {
            EcgProperty property;
            if (propertyDictionary.TryGetValue(EnabledPropertyMappingName, out property))
            {
                _enabledProperty = property;
                //UpdateFeatureInfo(isEnabled: Enabled);
                return;
            }
            _enabledProperty = null;
        }
    }
}