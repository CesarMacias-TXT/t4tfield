﻿using System;
using MvvmCross.Plugin.Messenger;
using Osram.TFTBackend.ConfigurationService.Contracts;
using Osram.TFTBackend.ConfigurationService.DataModel;
using Osram.TFTBackend.DataModel.Feature.Contracts;
using Osram.TFTBackend.DataModel.Feature.Helper;

namespace Osram.TFTBackend.DataModel.Feature
{
    public class CorridorFeature : FeatureImpl, ICorridorFeature
    {
#pragma warning disable CS0649
        [EcgMapping("MainsFrequency", true)]
        private IEcgPropertyValue _frequency;
        public int Frequency
        {
            get { return _frequency.IntValue; }
            set { _frequency.IntValue = (int)value; }
        }

        [EcgMapping("TDiCorridor-1:TDConfig-ShortPushEnable", false, "TDiCorridor-5:TDConfig-ShortPushEnable")]
        private IEcgPropertyValue _switchOnOff;
        public bool SwitchOnOff => !(_switchOnOff.IntValue == _switchOnOff.IntOffValue);

        [EcgMapping("TDiCorridor-1:TDConfig-DoublPushEnable", false, "TDiCorridor-5:TDConfig-DoublPushEnable")]
        private IEcgPropertyValue _setResetLevel;
        public bool SetResetLevel => !(_setResetLevel.IntValue == _setResetLevel.IntOffValue);

        [EcgMapping("TDiCorridor-1:TDConfig-LongPushEnable", false, "TDiCorridor-5:TDConfig-LongPushEnable")]
        private IEcgPropertyValue _dimUpDown;
        public bool DimUpDown => !(_dimUpDown.IntValue == _dimUpDown.IntOffValue);

        [EcgMapping("TDiCorridor-1:FadeUpTime", false, "TDiCorridor-5:FadeUpTime")]
        private IEcgPropertyValue _fadeTime1;
        public int FadeTime1
        {
            get { return _fadeTime1.IntValue; }
            set { _fadeTime1.IntValue = (int)value; }
        }

        [EcgMapping("TDiCorridor-1:CFHoldTime0", false, "TDiCorridor-5:CFHoldTime0")]
        private IEcgPropertyValue _holdTime2;
        public int HoldTime2
        {
            get { return _holdTime2.IntValue; }
            set { _holdTime2.IntValue = (int)value; }
        }

        [EcgMapping("TDiCorridor-1:TDSwitchOnLevel", false, "TDiCorridor-5:TDSwitchOnLevel")]
        private IEcgPropertyValue _operativeDaliLevel;
        public int OperativeDaliLevel
        {
            get { return _operativeDaliLevel.IntValue; }
            set { _operativeDaliLevel.IntValue = (int)value; }
        }

        [EcgMapping("TDiCorridor-1:FadeDownTime", false, "TDiCorridor-5:FadeDownTime")]
        private IEcgPropertyValue _fadeTime3;
        public int FadeTime3
        {
            get { return _fadeTime3.IntValue; }
            set { _fadeTime3.IntValue = (int)value; }
        }

        [EcgMapping("TDiCorridor-1:CFHoldTime1", false, "TDiCorridor-5:CFHoldTime1")]
        private IEcgPropertyValue _holdTime4;
        public int HoldTime4
        {
            get { return _holdTime4.IntValue; }
            set { _holdTime4.IntValue = (int)value; }
        }

        [EcgMapping("TDiCorridor-1:CFLevel0", false, "TDiCorridor-5:CFLevel0")]
        private IEcgPropertyValue _standbyDaliLevel;
        public int StandbyDaliLevel
        {
            get { return _standbyDaliLevel.IntValue; }
            set { _standbyDaliLevel.IntValue = (int)value; }
        }

        [EcgMapping("TDiCorridor-1:CFFadeTime2", false, "TDiCorridor-5:CFFadeTime2")]
        private IEcgPropertyValue _fadeTime5;
        public int FadeTime5
        {
            get { return _fadeTime5.IntValue; }
            set { _fadeTime5.IntValue = (int)value; }
        }

        [EcgMapping("TDiCorridor-1:CFHoldTime2", false, "TDiCorridor-1:CFHoldTime2")]
        private IEcgPropertyValue _holdTime6;
        public int HoldTime6
        {
            get { return _holdTime6.IntValue; }
            set { _holdTime6.IntValue = (int)value; }
        }

        [EcgMapping("TDiCorridor-1:CFLevel2", false, "TDiCorridor-5:CFLevel2")]
        private IEcgPropertyValue _standby2DaliLevel;
        public int Standby2DaliLevel
        {
            get { return _standby2DaliLevel.IntValue; }
            set { _standby2DaliLevel.IntValue = (int)value; }
        }

        [EcgMapping("TDiCorridor-1:CFPowerOnBehavior", false, "TDiCorridor-5:CFPowerOnBehavior")]
        private IEcgPropertyValue _startupBehaviour;
        public int StartupBehaviour
        {
            get { return _startupBehaviour.IntValue; }
            set { _startupBehaviour.IntValue = (int)value; }
        }

#pragma warning restore CS0649

        protected override String EnabledPropertyMappingName => "TDiCorridor-1:TDConfig-TDCFEnable";

        protected override bool IsEnabledAdvanced()
        {
            if (_enabledProperty == null)
            {
                Info.IsAvailable = false;
                return false;
            }

            return true;
        }

        public CorridorFeature(IMvxMessenger messenger, IConfigurationService configurationService) : base(messenger, configurationService)
        {
        }
    }
}
