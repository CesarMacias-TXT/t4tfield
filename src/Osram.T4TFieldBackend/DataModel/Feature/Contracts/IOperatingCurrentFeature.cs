﻿namespace Osram.TFTBackend.DataModel.Feature.Contracts
{
    public interface IOperatingCurrentFeature : IFeature
    {
        OperatingCurrentMode OperatingCurrentMode { get; set; }

        int OperatingCurrent { get; set; }
    }

    public enum OperatingCurrentMode
    {
        FixedCurrent = 0,
        LEDSet2 = 1
    }
}
