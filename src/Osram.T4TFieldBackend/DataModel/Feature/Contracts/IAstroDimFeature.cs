﻿using System;
using Osram.TFTBackend.LocationService;
using Osram.TFTBackend.DataModel.Helper;

namespace Osram.TFTBackend.DataModel.Feature.Contracts
{
    public interface IAstroDimFeature : IFeature
    {
        AstroDimMode AstroDimMode { get; set; }

        string OperatingModes { get; set; }

        int SwitchOnFadeTime { get; set; }
        int SwitchOffFadeTime { get; set; }
        int AstroDimFadeTime { get; set; }

        int NominalLevel { get; set; }

        //all dimming levels
        OutputLevel DimmingLevels { get; set; }
        OutputLevel DimmingLevelsMin { get; set; }
        OutputLevel DimmingLevelsMax { get; set; }
        OutputLevel DimmingLevelsOff { get; set; }

        DimmingDuration DimmingDurations { get; set; }

        int DimStartTime { get;}
        int MaxDimmingRange { get;}
        int MinDimmingRange { get; }

        int Latitude { get; set; }
        int Longitude { get; set; }
        double UtcTimeShift { get; set; }

        TimeSpan MinimumDifferencesInTime { get; }

        EcgLocation.Location Location { get; set; }

        TimeValue GetDefaultReferenceScheduleTime();

        TimeValue GetTimeBasedReferenceScheduleTime();

        TimeValue GetAstroBasedReferenceScheduleTime();

        void UpdateDimDurationToDataModel(TimeValue referenceScheduleTimeBasedTime);

        double ValidateRangeOutputLevel(int level);
    }

    public enum AstroDimMode
    {
        AstroBased = 0,
        TimeBased = 1     
    }

    /// <summary>
    ///  Output level (may used not every levels)
    /// </summary>
    public class OutputLevel : CustomArray<int>
    {
        public OutputLevel() : base(-1, 5)
        { }
    }

    /// <summary>
    /// Time level in Reference Schedule value
    /// </summary>
    public class TimeValue : CustomArray<DateTime>
    {
        public TimeValue() : base(DateTime.MinValue, 6)
        {

        }
    }

    /// <summary>
    ///  Dimming Duration (may used not every durations)
    /// </summary>
    public class DimmingDuration : CustomArray<int>
    {
        public DimmingDuration() : base(-1, 5)
        { }
    }
}
