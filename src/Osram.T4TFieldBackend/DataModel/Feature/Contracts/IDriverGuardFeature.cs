﻿namespace Osram.TFTBackend.DataModel.Feature.Contracts
{
    public interface IDriverGuardFeature : IFeature
    {
        int PrestartDerating { get; set; }

        int DeratingLevel { get; set; }

        int PowerDeratingLevel { get; set; }
    }
}
