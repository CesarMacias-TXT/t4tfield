﻿namespace Osram.TFTBackend.DataModel.Feature.Contracts
{
    public interface ISoftSwitchOffFeature : IFeature
    {
        int FadeTime { get; set;  }

    }
}
