﻿namespace Osram.TFTBackend.DataModel.Feature.Contracts
{
    public interface IEndOfLifeFeature : IFeature
    {
        int EndOfLifeTime { get; set; }
    }
}
