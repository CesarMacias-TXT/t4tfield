﻿namespace Osram.TFTBackend.DataModel.Feature.Contracts
{
    public interface ICorridorFeature : IFeature
    {
        int Frequency { get; set; }
        bool SwitchOnOff { get; }
        bool SetResetLevel { get; }
        bool DimUpDown { get; }
        int FadeTime1 { get; set; }
        int HoldTime2 { get; set; }
        int OperativeDaliLevel { get; set; }
        int FadeTime3 { get; set; }
        int HoldTime4 { get; set; }
        int StandbyDaliLevel { get; set; }
        int FadeTime5 { get; set; }
        int HoldTime6 { get; set;  }
        int Standby2DaliLevel { get; set; }
        int StartupBehaviour { get; set; }

    }
}
