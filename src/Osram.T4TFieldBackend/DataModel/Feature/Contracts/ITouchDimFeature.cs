﻿namespace Osram.TFTBackend.DataModel.Feature.Contracts
{
    public interface ITouchDimFeature : IFeature
    {
        int Frequency { get; set; }
        bool SwitchOnOff { get; }
        bool SetResetLevel { get; }        
        bool DimUpDown { get; }
        bool SwitchOnOff2 { get; }
        bool SetResetLevel2 { get; }
        bool DimUpDown2 { get; }
        int MainsPowerLevel { get; set; }
        int FadeTime1 { get; set; }
        int OperativeDaliLevel { get; set; }
        int HoldTime2 { get; set; }
        int FadeTime3 { get; set; }
        int FadeTime4 { get; set; }
        int StandbyDaliLevel { get; set; }
        int HoldTime5 { get; set; }
        bool LsActive { get; }
        bool PdActive { get; }
        bool PdHolidayMode { get; }
        bool AutoDisablePdActive { get; }
        int MainsPowerColour { get; set; }
        int SwitchOnColour { get; set; }
        int MainsPowerLevel2 { get; set; }
        int OperativeLevel { get; set; }

    }
}
