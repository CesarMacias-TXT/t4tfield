﻿namespace Osram.TFTBackend.DataModel.Feature.Contracts
{
    public interface IThermalProtectionFeature : IFeature
    {
        ThermalProtectionMode ThermalProtectionMode { get; set; }
        
        int StartDeratingResistance { get; set; }
        int EndDeratingResistance { get; set; }
        bool ShutOff { get; }
        int ShutOffValue { get; set; }
        int FinalPowerReductionLevel { get; set; }
    }

    public enum ThermalProtectionMode
    {
        ResistorBased = 0,
        TemperatureBased = 1
    }
}
