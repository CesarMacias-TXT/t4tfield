﻿namespace Osram.TFTBackend.DataModel.Feature.Contracts
{
    public interface IDimToDarkFeature : IFeature
    {
        bool Active { get; }
    }
}
