﻿namespace Osram.TFTBackend.DataModel.Feature.Contracts
{
    public interface IMainsDimFeature : IFeature
    {
        int MainsVoltage { get; set; }

        int StartVoltage { get; set; }

        int StartLevel { get; set; }

        int StopVoltage { get; set; }

        int StopLevel { get; set; }
    }
}
