﻿namespace Osram.TFTBackend.DataModel.Feature.Contracts
{
    public interface ITuningFactorFeature : IFeature
    {
        int MaximumTuningFactor { get; set;  }
        int MinimumTuningFactor { get; set; }
        int TuningFactor { get; set; }
        int ReferenceLumenOutput { get; set; }
        int PwmPulsWidth { get; set; }
    }
}
