﻿namespace Osram.TFTBackend.DataModel.Feature.Contracts
{
    public interface IEmergencyModeFeature : IFeature
    {
        int DCLightLevel { get; set; }
        bool LockDali { get; }
    }
}
