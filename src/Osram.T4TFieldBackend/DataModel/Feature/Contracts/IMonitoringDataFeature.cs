﻿using Osram.TFTBackend.ConfigurationService.DataModel;

namespace Osram.TFTBackend.DataModel.Feature.Contracts
{
    public interface IMonitoringDataFeature : IFeature
    {
        ProgrammingValue ActiveEnergyPower { get; set;  }
        ProgrammingValue ApparentEnergyPower { get; set;  }
        ProgrammingValue LoadSideEnergyPower { get; set;  }
        ProgrammingValue ControlGearDiagnostics { get; set;  }
        ProgrammingValue LightSourceDiagnostics { get; set;  }

    }
}
