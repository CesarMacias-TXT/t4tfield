﻿namespace Osram.TFTBackend.DataModel.Feature.Contracts
{
    public interface ILampOperatingTimeFeature : IFeature
    {
        int LampOperationCounter { get; set; }
    }
}
