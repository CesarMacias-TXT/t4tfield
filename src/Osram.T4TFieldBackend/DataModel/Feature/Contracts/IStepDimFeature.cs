﻿namespace Osram.TFTBackend.DataModel.Feature.Contracts
{
    public interface IStepDimFeature : IFeature
    {
        int SdInputStatus { get; set; }

        int SdLevel { get; set; }

        int StartFadeTime { get; set; }

        int EndFadeTime { get; set; }

        int HoldTime { get; set; }

        int NominalLevel { get; set; }

        int StartupFadeTime { get; set; }
    }
}
