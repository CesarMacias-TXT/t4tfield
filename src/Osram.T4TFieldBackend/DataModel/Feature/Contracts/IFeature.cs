﻿
using System.Collections.Generic;

namespace Osram.TFTBackend.DataModel.Feature.Contracts
{
    public interface IFeature
    {
        /// <summary>
        /// The name as an identifier for this Feature.
        /// </summary>
        string Name { get; }

        bool Enabled { get; }

        HashSet<int> PermissionIndexes { get; }

        IFeatureInfo Info { get; set; }
    }
}