﻿

namespace Osram.TFTBackend.DataModel.Feature.Contracts
{
    public interface IFeatureInfo
    {
        /// <summary>
        /// Check if feature is enabled
        /// false => only read mode
        /// </summary>
        bool IsEnabled { get; set; }

        /// <summary>
        /// Check if feature is supported by the current ECG
        /// false => feature not present
        /// </summary>
        bool IsAvailable { get; set; }

        /// <summary>
        /// Information about key protection level
        /// None, Service, Master
        /// </summary>
        ProtectionLock Lock { get; set; }

        /// <summary>
        /// Check if feature has warnings or errors...
        /// </summary>
        //TOCHECK
        //are expected multiple status at the same time? (warning + error)
        FeatureStatus Status { get; set; }


        FeatureType FeatureType { get; set; }
    }


    public enum ProtectionLock
    {
        None,
        Service,
        Master
    }
    public enum FeatureStatus
    {
        Ok,
        Warning,
        Error
    }

    /// <summary>
    /// Represents the list of all the features can be handling by T4T F app.
    /// </summary>
    public enum FeatureType
    {
        Default,

        /// <summary>
        /// The astro dim
        /// </summary>
        AstroDIM,

        /// <summary>
        /// The constant lumen
        /// </summary>
        ConstantLumen,

        /// <summary>
        /// The corridor
        /// </summary>
        Corridor,

        /// <summary>
        /// The DexalPSU
        /// </summary>
        DexalPSU,

        /// <summary>
        /// The DimToDark
        /// </summary>
        DimToDark,

        /// <summary>
        /// The driver guard
        /// </summary>
        DriverGuard,

        /// <summary>
        /// The emergency mode
        /// </summary>
        EmergencyMode,

        /// <summary>
        /// The end of life
        /// </summary>
        EndOfLife,

        /// <summary>
        /// The lamp operating time
        /// </summary>
        LampOperatingTime,

        /// <summary>
        /// The mains dim
        /// </summary>
        MainsDIM,

        /// <summary>
        /// The monitoring data
        /// </summary>
        MonitoringData,        

        /// <summary>
        /// The operating current
        /// </summary>
        OperatingCurrent,

        /// <summary>
        /// The presence detection
        /// </summary>
        PresenceDetection,

        /// <summary>
        /// The soft switch off
        /// </summary>
        SoftSwitchOff,

        /// <summary>
        /// The step dim
        /// </summary>
        StepDIM,

        /// <summary>
        /// The thermal protection
        /// </summary>
        ThermalProtection,

        /// <summary>
        /// The touch dim
        /// </summary>
        TouchDim,

        /// <summary>
        /// The tuning factor
        /// </summary>
        TuningFactor
    }
}
