﻿using Osram.TFTBackend.DataModel.Helper;

namespace Osram.TFTBackend.DataModel.Feature.Contracts
{
    public interface IConstantLumenFeature : IFeature
    {
        TimeOuput Times { get; set; }
        TimeOuput TimesMin { get; set; }
        TimeOuput TimesMax { get; set; }
        TimeOuput TimesOff { get; set; }

        AdjustmentLevelOutput AdjustmentLevels { get; set; }
        AdjustmentLevelOutput AdjustmentLevelsMin { get; set; }
        AdjustmentLevelOutput AdjustmentLevelsMax { get; set; }
        AdjustmentLevelOutput AdjustmentLevelsOff { get; set; }

        int Enable { get; set; }
        int Time1 { get; set; }
        int Time2 { get; set; }
        int Time3 { get; set; }
        int Time4 { get; set; }
        int Time5 { get; set; }
        int Time6 { get; set; }
        int Time7 { get; set; }
        int Time8 { get; set; }

        int AdjustmentLevel1 { get; set; }
        int AdjustmentLevel2 { get; set; }
        int AdjustmentLevel3 { get; set; }
        int AdjustmentLevel4 { get; set; }
        int AdjustmentLevel5 { get; set; }
        int AdjustmentLevel6 { get; set; }
        int AdjustmentLevel7 { get; set; }
        int AdjustmentLevel8 { get; set; }
    }

    public class TimeOuput : CustomArray<int>
    {
        public TimeOuput() : base(-1, 8)
        { }
    }

    public class AdjustmentLevelOutput : CustomArray<int>
    {
        public AdjustmentLevelOutput() : base(-1, 8)
        { }
    }
}
