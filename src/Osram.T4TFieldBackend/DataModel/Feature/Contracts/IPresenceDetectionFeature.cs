﻿namespace Osram.TFTBackend.DataModel.Feature.Contracts
{
    public interface IPresenceDetectionFeature : IFeature
    {
        int PdInputStatus { get; set; }

        int PdLevel { get; set; }

        int StartFadeTime { get; set; }

        int EndFadeTime { get; set; }

        int HoldTime { get; set; }
    }
}
