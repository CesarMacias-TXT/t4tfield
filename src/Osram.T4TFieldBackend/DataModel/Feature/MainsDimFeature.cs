﻿using MvvmCross.Plugin.Messenger;
using Osram.TFTBackend.ConfigurationService.Contracts;
using Osram.TFTBackend.ConfigurationService.DataModel;
using Osram.TFTBackend.DataModel.Feature.Contracts;
using Osram.TFTBackend.DataModel.Feature.Helper;

namespace Osram.TFTBackend.DataModel.Feature
{
    public class MainsDimFeature : FeatureImpl, IMainsDimFeature
    {
#pragma warning disable CS0649
        // <Property PgmName="MD-0:MainsVoltage" AccessType="R" Volatile="true" PermW="0">
        [EcgMapping("MD-0:MainsVoltage", true)]
        private IEcgPropertyValue _mainsVoltage;
        public int MainsVoltage {
            get { return _mainsVoltage.IntValue; }
            set { _mainsVoltage.IntValue = value; }
        }

        // <Property PgmName="MD-0:StartVoltage" AccessType="RWL" Volatile="false" PermW="18">
        [EcgMapping("MD-0:StartVoltage", true)]
        private IEcgPropertyValue _startVoltage;
        public int StartVoltage {
            get { return _startVoltage.IntValue; }
            set { _startVoltage.IntValue = value; }
        }

        // <Property PgmName="MD-0:StartLevel" AccessType="RWL" Volatile="false" PermW="18">
        [EcgMapping("MD-0:StartLevel", true)]
        private IEcgPropertyValue _startLevel;
        public int StartLevel {
            get { return _startLevel.IntValue; }
            set { _startLevel.IntValue = value; }
        }

        // <Property PgmName="MD-0:StopVoltage" AccessType="RWL" Volatile="false" PermW="18">
        [EcgMapping("MD-0:StopVoltage", true)]
        private IEcgPropertyValue _stopVoltage;
        public int StopVoltage {
            get { return _stopVoltage.IntValue; }
            set { _stopVoltage.IntValue = value; }
        }

        // <Property PgmName="MD:StopLevel" AccessType="RWL" Volatile="false" PermW="18">
        [EcgMapping("MD:StopLevel", true)]
        private IEcgPropertyValue _stopLevel;
        public int StopLevel {
            get { return _stopLevel.IntValue; }
            set { _stopLevel.IntValue = value; }
        }
#pragma warning restore CS0649

        protected override string EnabledPropertyMappingName => "MD-0:Control-MDEnable";
        public MainsDimFeature(IMvxMessenger messenger, IConfigurationService configurationService) : base(messenger, configurationService)
        {
        }
    }
}