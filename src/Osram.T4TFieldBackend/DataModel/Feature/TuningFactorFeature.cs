﻿using MvvmCross.Plugin.Messenger;
using Osram.TFTBackend.ConfigurationService.Contracts;
using Osram.TFTBackend.ConfigurationService.DataModel;
using Osram.TFTBackend.DataModel.Feature.Contracts;
using Osram.TFTBackend.DataModel.Feature.Helper;

namespace Osram.TFTBackend.DataModel.Feature
{
    public class TuningFactorFeature : FeatureImpl, ITuningFactorFeature
    {
#pragma warning disable CS0649
        [EcgMapping("TF-0:MaximumTuningFactor", false, "PwmConfig-0:MaximumTuningFactor")]
        private IEcgPropertyValue _maximumTuningFactorProperty;
        public int MaximumTuningFactor
        {
            get { return _maximumTuningFactorProperty.IntValue; }
            set { _maximumTuningFactorProperty.IntValue = value; }
        }

        [EcgMapping("TF-0:MinimumTuningFactor", false, "PwmConfig-0:MinimumTuningFactor")]
        private IEcgPropertyValue _minimumTuningFactorProperty;
        public int MinimumTuningFactor
        {
            get { return _minimumTuningFactorProperty.IntValue; }
            set { _minimumTuningFactorProperty.IntValue = value; }
        }

        [EcgMapping("TF-0:TuningFactor", true, "PwmConfig-0:TuningFactor")]
        private IEcgPropertyValue _tuningFactorProperty;
        public int TuningFactor
        {
            get { return _tuningFactorProperty.IntValue; }
            set { _tuningFactorProperty.IntValue = value; }
        }

        [EcgMapping("TF-0:ReferenceLumenOutput")]
        private IEcgPropertyValue _referenceLumenOutputProperty;
        public int ReferenceLumenOutput
        {
            get { return _referenceLumenOutputProperty.IntValue; }
            set { _referenceLumenOutputProperty.IntValue = value; }
        }

        [EcgMapping("PwmOut-0:PwmPulsWidth")]
        private IEcgPropertyValue _pwmPulsWidth;
        public int PwmPulsWidth
        {
            get { return _pwmPulsWidth.IntValue; }
            set { _pwmPulsWidth.IntValue = value; }
        }

        [EcgMapping("OTConfig-2:ModeSetting-EnableLEDset2Interface", false, "PwmConfig-0:EnableLEDset", "OTConfig-3:ModeSetting-EnableLEDset2Interface")]
        private IEcgPropertyValue _operatingCurrentMode;
        public OperatingCurrentMode OperatingCurrentMode
        {
            get { return (OperatingCurrentMode)_operatingCurrentMode.IntValue; }
            set { _operatingCurrentMode.IntValue = (int)value; }
        }

#pragma warning restore CS0649

        protected override string EnabledPropertyMappingName => "TF-0:Enable";

        protected override bool IsEnabledAdvanced()
        {
            if (TuningFactor == 0 || (MaximumTuningFactor == MinimumTuningFactor))
            {
                return false;
            }

            return true;
        }

        public TuningFactorFeature(IMvxMessenger messenger, IConfigurationService configurationService) : base(messenger, configurationService)
        {
            //if (ECG != null)
            //{
            //    //TODO: HACK to get tuning values correct
            //    _tuningFactorProperty.EcgMemoryCell.SetStartLocation(1, 0);
            //    _theConfigurationService.AddUsedMemoryCell(_tuningFactorProperty.EcgMemoryCell);
            //    _minimumTuningFactorProperty.EcgMemoryCell.SetStartLocation(2, 0);
            //    _theConfigurationService.AddUsedMemoryCell(_minimumTuningFactorProperty.EcgMemoryCell);
            //    _maximumTuningFactorProperty.EcgMemoryCell.SetStartLocation(3, 0);
            //    _theConfigurationService.AddUsedMemoryCell(_maximumTuningFactorProperty.EcgMemoryCell);
            //    EcgMemoryBank bank = new EcgMemoryBank(10, "TF-0", new EcgMemoryCell[3] { _tuningFactorProperty.EcgMemoryCell, _minimumTuningFactorProperty.EcgMemoryCell, _maximumTuningFactorProperty.EcgMemoryCell });
            //}
        }
    }
}