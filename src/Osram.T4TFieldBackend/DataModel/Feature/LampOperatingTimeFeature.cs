﻿using MvvmCross.Plugin.Messenger;
using Osram.TFTBackend.ConfigurationService.Contracts;
using Osram.TFTBackend.ConfigurationService.DataModel;
using Osram.TFTBackend.DataModel.Feature.Contracts;
using Osram.TFTBackend.DataModel.Feature.Helper;

namespace Osram.TFTBackend.DataModel.Feature
{
    public class LampOperatingTimeFeature : FeatureImpl, ILampOperatingTimeFeature
    {
#pragma warning disable CS0649
        // <Property PgmName="Info-0:LampOperationCounter" AccessType="RWL" Volatile="true" PermW="15">
        [EcgMapping("Info-0:LampOperationCounter", true)]
        private IEcgPropertyValue _lampOperationCounter;
        public int LampOperationCounter
        {
            get { return _lampOperationCounter.IntValue; }
            set { _lampOperationCounter.IntValue = value; }
        }
#pragma warning restore CS0649

        protected override string EnabledPropertyMappingName => "Info-0:LampOperationCounter";

        protected override bool IsEnabledAdvanced()
        {
            if (_enabledProperty == null)
            {
                Info.IsAvailable = false;
                return false;
            }

            return true;
        }

        public LampOperatingTimeFeature(IMvxMessenger messenger, IConfigurationService configurationService) : base(messenger, configurationService)
        {
        }
    }
}