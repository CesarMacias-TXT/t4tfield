﻿using MvvmCross.Plugin.Messenger;
using Osram.TFTBackend.ConfigurationService.Contracts;
using Osram.TFTBackend.ConfigurationService.DataModel;
using Osram.TFTBackend.DataModel.Feature.Contracts;
using Osram.TFTBackend.DataModel.Feature.Helper;

namespace Osram.TFTBackend.DataModel.Feature
{
    public class EndOfLifeFeature : FeatureImpl, IEndOfLifeFeature
    {
#pragma warning disable CS0649
        //<Property PgmName="_default" AccessType="RWL" Volatile="false" PermW="8">
        [EcgMapping("EOL-0:EOLTime", true)]
        private IEcgPropertyValue _endOfLifeTime;
        public int EndOfLifeTime
        {
            get { return _endOfLifeTime.IntValue; }
            set { _endOfLifeTime.IntValue = value; }
        }
#pragma warning restore CS0649

        protected override string EnabledPropertyMappingName => "EOL-0:Command-EOLEnable";

        protected override bool IsEnabledAdvanced()
        {
            if (_enabledProperty == null)
            {
                Info.IsAvailable = false;
                return false;
            }

            return true;
        }

        public EndOfLifeFeature(IMvxMessenger messenger, IConfigurationService configurationService) : base(messenger, configurationService)
        {
        }
    }
}