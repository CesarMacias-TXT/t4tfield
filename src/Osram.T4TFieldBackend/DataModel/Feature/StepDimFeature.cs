﻿using MvvmCross.Plugin.Messenger;
using Osram.TFTBackend.ConfigurationService.Contracts;
using Osram.TFTBackend.ConfigurationService.DataModel;
using Osram.TFTBackend.DataModel.Feature.Contracts;
using Osram.TFTBackend.DataModel.Feature.Helper;

namespace Osram.TFTBackend.DataModel.Feature
{
    public class StepDimFeature : FeatureImpl, IStepDimFeature
    {
#pragma warning disable CS0649
        // <Property PgmName="SD-1:Status-SDInput" AccessType="R" Volatile="true" PermW="0">
        [EcgMapping("SD-1:Status-SDInput", true)]
        private IEcgPropertyValue _sdInputStatus;
        public int SdInputStatus {
            get { return _sdInputStatus.IntValue; }
            set { _sdInputStatus.IntValue = value; }
        }

        // <Property PgmName="SD-1:SDLevel" AccessType="RWL" Volatile="false" PermW="18">
        [EcgMapping("SD-1:SDLevel", true)]
        private IEcgPropertyValue _sdLevel;
        public int SdLevel {
            get { return _sdLevel.IntValue; }
            set { _sdLevel.IntValue = value; }
        }

        // <Property PgmName="SD-1:StartFadeTime" AccessType="RWL" Volatile="false" PermW="18">
        [EcgMapping("SD-1:StartFadeTime", true)]
        private IEcgPropertyValue _startFadeTime;
        public int StartFadeTime {
            get { return _startFadeTime.IntValue; }
            set { _startFadeTime.IntValue = value; }
        }

        // <Property PgmName="SD-1:EndFadeTime" AccessType="RWL" Volatile="false" PermW="18">
        [EcgMapping("SD-1:EndFadeTime", true)]
        private IEcgPropertyValue _endFadeTime;
        public int EndFadeTime {
            get { return _endFadeTime.IntValue; }
            set { _endFadeTime.IntValue = value; }
        }

        // <Property PgmName="SD-1:HoldTime" AccessType="RWL" Volatile="false" PermW="18">
        [EcgMapping("SD-1:HoldTime", true)]
        private IEcgPropertyValue _holdTime;
        public int HoldTime {
            get { return _holdTime.IntValue; }
            set { _holdTime.IntValue = value; }
        }

        // <Property PgmName="SD-1:Nominallevel" AccessType="RWL" Volatile="false" PermW="18">
        [EcgMapping("SD-1:Nominallevel", true)]
        private IEcgPropertyValue _nominalLevel;
        public int NominalLevel {
            get { return _nominalLevel.IntValue; }
            set { _nominalLevel.IntValue = value; }
        }

        // <Property PgmName="SD-1:StartupFadeTime" AccessType="RWL" Volatile="false" PermW="18">
        [EcgMapping("SD-1:StartupFadeTime", true)]
        private IEcgPropertyValue _startupFadeTime;
        public int StartupFadeTime {
            get { return _startupFadeTime.IntValue; }
            set { _startupFadeTime.IntValue = value; }
        }
#pragma warning restore CS0649

        protected override string EnabledPropertyMappingName => "SD-1:Control-SDEnable";
        public StepDimFeature(IMvxMessenger messenger, IConfigurationService configurationService) : base(messenger, configurationService)
        {
        }
    }
}