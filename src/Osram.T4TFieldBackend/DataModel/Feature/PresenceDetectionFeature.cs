﻿using MvvmCross.Plugin.Messenger;
using Osram.TFTBackend.ConfigurationService.Contracts;
using Osram.TFTBackend.ConfigurationService.DataModel;
using Osram.TFTBackend.DataModel.Feature.Contracts;
using Osram.TFTBackend.DataModel.Feature.Helper;

namespace Osram.TFTBackend.DataModel.Feature
{
    public class PresenceDetectionFeature : FeatureImpl, IPresenceDetectionFeature
    {
#pragma warning disable CS0649
        // <Property PgmName="PD-0:Status-PDInput" AccessType="R" Volatile="true" PermW="0">
        [EcgMapping("PD-0:Status-PDInput", true)]
        private IEcgPropertyValue _pdInputStatus;
        public int PdInputStatus {
            get { return _pdInputStatus.IntValue; }
            set { _pdInputStatus.IntValue = value; }
        }

        // <Property PgmName="PD-0:PDLevel" AccessType="RWL" Volatile="false" PermW="18">
        [EcgMapping("PD-0:PDLevel", true)]
        private IEcgPropertyValue _pdLevel;
        public int PdLevel {
            get { return _pdLevel.IntValue; }
            set { _pdLevel.IntValue = value; }
        }

        // <Property PgmName="PD-0:StartFadeTime" AccessType="RWL" Volatile="false" PermW="18">
        [EcgMapping("PD-0:StartFadeTime", true)]
        private IEcgPropertyValue _startFadeTime;
        public int StartFadeTime {
            get { return _startFadeTime.IntValue; }
            set { _startFadeTime.IntValue = value; }
        }

        // <Property PgmName="PD-0:EndFadeTime" AccessType="RWL" Volatile="false" PermW="18">
        [EcgMapping("PD-0:EndFadeTime", true)]
        private IEcgPropertyValue _endFadeTime;
        public int EndFadeTime {
            get { return _endFadeTime.IntValue; }
            set { _endFadeTime.IntValue = value; }
        }

        // <Property PgmName="PD-0:HoldTime" AccessType="RWL" Volatile="false" PermW="18">
        [EcgMapping("PD-0:HoldTime", true)]
        private IEcgPropertyValue _holdTime;
        public int HoldTime {
            get { return _holdTime.IntValue; }
            set { _holdTime.IntValue = value; }
        }
#pragma warning restore CS0649

        protected override string EnabledPropertyMappingName => "PD-0:Control-PDEnable";
        public PresenceDetectionFeature(IMvxMessenger messenger, IConfigurationService configurationService) : base(messenger, configurationService)
        {
        }
    }
}