﻿using Osram.TFTBackend.ConfigurationService.DataModel;
using Osram.TFTBackend.DataModel.Feature.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Osram.TFTBackend.DataModel
{
    public interface IFeatureManager
    {
        List<IFeatureInfo> GetAllFeaturesInfoList();

        IFeatureInfo GetFeatureInfoByType(FeatureType featureType);

        void UpdateFeatureList();
    }
}
