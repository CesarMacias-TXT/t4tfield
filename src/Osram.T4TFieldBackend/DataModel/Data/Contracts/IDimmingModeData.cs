﻿using System.Collections.Generic;

namespace Osram.TFTBackend.DataModel.Data.Contracts
{
    public interface IDimmingModeData
    {

        /// <summary>
        /// Gets or sets the maximum dimming range value.
        /// </summary>
        /// <value>
        /// The maximum dimming range.
        /// </value>
        double MaxDimmingRange { get; set; }

        /// <summary>
        /// Gets or sets the minimum dimming range value.
        /// </summary>
        /// <value>
        /// The minimum dimming range.
        /// </value>
        double MinDimmingRange { get; set; }

        int SelectedDimmingMode { get; set; }

        void Init();

        List<DimmingMode> ModesCollection { get; set; }
    }


    //TODO insert the right number value foreach type
    public enum DimmingMode
    {
        DimModeAstroDIM,
        DimModeAstroStepDIM,
        DimModeStepDIM,
        DimModeAstroPD,
        DimModeMainsDIM,
        DimModeTDDali,
        DimModeDali,
        DimModeZeroToTen,
        DimModeTD,
        DimModeCF,
        None
    }

}
