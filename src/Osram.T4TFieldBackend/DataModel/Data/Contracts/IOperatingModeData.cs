﻿using Osram.TFTBackend.ConfigurationService.DataModel;

namespace Osram.TFTBackend.DataModel.Data.Contracts
{
    public interface IOperatingModeData
    {
        /// <summary>
        /// Init Method
        /// </summary>
        void Init();

        /// <summary>
        /// Gets the current Operating Mode
        /// </summary>
        OperatingMode GetCurrentOperatingMode();

        /// <summary>
        /// Gets the current Operating Mode Enum
        /// </summary>
        OperatingModeEnum GetCurrentOperatingModeEnum();

        /// <summary>
        /// Gets the current Operating Mode for the report
        /// </summary>
        OperatingModeReportEnum GetCurrentReportOperatingModeEnum();

        /// <summary>
        /// Gets the current Operating Mode Name for the report
        /// </summary>
        string GetCurrentReportOperatingModeName();

        /// <summary>
        /// Set all the mode properties to the values defined in the mode
        /// </summary>
        /// <value>
        /// The operating mode
        /// </value>
        void SetOperatingMode(OperatingMode operatingMode);

        /// <summary>
        /// Set all the mode properties to the values defined in the mode
        /// </summary>
        /// <value>
        /// The operating mode enum
        /// </value>
        void SetOperatingModeEnum(OperatingModeEnum operatingModeEnum);
    }

    public enum OperatingModeEnum
    {
        None,

        OnOff,
        AstroDim
    }

    public enum OperatingModeReportEnum
    {
        None,

        OnOff,
        AstroDim,

        StepDim_AstroDim_Dali_WiringSelection,
        AstroDim_Dali,
        AstroDim_PresenceDetection_Dali,
        StepDim_Dali,
        StepDimInverse_Dali,
        Autodetect_OnOff_StepDimInverse_Dali,
        Autodetect_AstroDim_StepDimInverse_Dali,
        MainsDim_Dali,
        Dali,
        Dali_SSO,
        Dali2_TF,
        Dali2_Only,
        Dali2_SSO,        
        TouchDim,
        Corridor,
        DaliTD,
        Dali_Only,
        Dali_EO,
        TD,
        CF
    }
}
