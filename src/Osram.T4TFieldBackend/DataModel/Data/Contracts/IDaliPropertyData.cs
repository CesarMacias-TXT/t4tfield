﻿
using Osram.TFTBackend.DataModel.Feature.Contracts;

namespace Osram.TFTBackend.DataModel.Data.Contracts
{
    public interface IDaliPropertyData
    {
        int PowerOnLevel { get; set; }
        int SystemFailureLevel { get; set; }
        int MinLevel { get; set; }
        int MaxLevel { get; set; }
        int FadeRate { get; set; }
        int FadeTime { get; set; }
        int ExtendedFadeTimeBase { get; set; }
        int ExtendedFadeTimeMultiplier { get; set; }
        int OperatingMode { get; set; }
        int DimmingCurve { get; set; }
        int FastFadeTime { get; set; }
        int Address { get; set; }

        bool IsAvailable { get; }
    }
}
