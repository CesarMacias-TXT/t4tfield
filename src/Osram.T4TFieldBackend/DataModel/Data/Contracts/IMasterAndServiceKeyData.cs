﻿
namespace Osram.TFTBackend.DataModel.Data.Contracts
{
    public interface IMasterAndServiceKeyData
    {
        uint MasterPassword { get; set; }
        uint EncryptedMasterPassword { get; }
        uint NewMasterPassword { get; set; }
        int NewMasterPasswordConfirmation { get; set; }
        int MasterPasswordStatusInformation { get; set; }
        int MasterPasswordUnlockCounter { get; set; }
        int MasterPasswordErrorCounter { get; set; }

        uint ServicePassword { get; set; }
        uint EncryptedServicePassword { get; }
        int ServicePasswordConfirmation { get; set; }
        int ServicePasswordStatusInformation { get; set; }
        int ServicePasswordUnlockCounter { get; set; }
        int ServicePasswordErrorCounter { get; set; }

        int PermissionsUser { get; set; }

        int PermissionsService { get; set; }
    }
}
