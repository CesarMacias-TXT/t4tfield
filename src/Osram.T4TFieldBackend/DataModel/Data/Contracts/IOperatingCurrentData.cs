﻿namespace Osram.TFTBackend.DataModel.Data.Contracts
{
    public interface IOperatingCurrentData
    {
        int OperatingCurrent { get; set; }
        int PhysicalMinLevel { get; }
        int PhysicalMaxLevel { get; }
    }
}