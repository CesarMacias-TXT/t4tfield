﻿namespace Osram.TFTBackend.DataModel.Data
{
    public class DDInfoData 
    {
        public int id { get; set; }
        public string serial { get; set; }
        public string gtin { get; set; }
        public string basicCode { get; set; }
        public string naed { get; set; }
        public string modelId { get; set; }
        public string deviceTypeName { get; set; }
        public string fwVersion { get; set; }
        public string hwVersion { get; set; }
        public string deviceDescription { get; set; }
        public string interfaceType { get; set; }
        public string ecgType { get; set; }
        public string docUrl { get; set; }
        public string picRef { get; set; }
        public string xmlRef { get; set; }
        public bool scopePC { get; set; }
        public bool scopeCloud { get; set; }
        public bool scopeField { get; set; }
        public string driverType { get; set; }
    }
}
