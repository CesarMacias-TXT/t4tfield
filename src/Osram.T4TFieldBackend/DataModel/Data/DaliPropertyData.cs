﻿using MvvmCross.Plugin.Messenger;
using Osram.TFTBackend.ConfigurationService.Contracts;
using Osram.TFTBackend.ConfigurationService.DataModel;
using Osram.TFTBackend.DataModel.Data.Contracts;
using Osram.TFTBackend.DataModel.Feature.Helper;
using System.Linq;

namespace Osram.TFTBackend.DataModel.Data
{
    public class DaliPropertyData : FeatureImpl, IDaliPropertyData
    {
#pragma warning disable CS0649
        // <Property PgmName="PowerOnLevel" AccessType="RW" Volatile="false">
        [EcgMapping("PowerOnLevel")]
        private IEcgPropertyValue _powerOnLevel;
        public int PowerOnLevel {
            get { return _powerOnLevel.IntValue; }
            set { _powerOnLevel.IntValue = value; }
        }

        // <Property PgmName = "SystemFailureLevel" AccessType="RW" Volatile="false">
        [EcgMapping("SystemFailureLevel")]
        private IEcgPropertyValue _systemFailureLevel;
        public int SystemFailureLevel {
            get { return _systemFailureLevel.IntValue; }
            set { _systemFailureLevel.IntValue = value; }
        }

        // <Property PgmName="MinLevel" AccessType="RW" Volatile="false">
        [EcgMapping("MinLevel")]
        private IEcgPropertyValue _minLevel;
        public int MinLevel {
            get { return _minLevel.IntValue; }
            set { _minLevel.IntValue = value; }
        }

        // <Property PgmName="MaxLevel" AccessType="RW" Volatile="false">
        [EcgMapping("MaxLevel")]
        private IEcgPropertyValue _maxLevel;
        public int MaxLevel {
            get { return _maxLevel.IntValue; }
            set { _maxLevel.IntValue = value; }
        }

        // <Property PgmName="FadeRate" AccessType="RW" Volatile="false">
        [EcgMapping("FadeRate")]
        private IEcgPropertyValue _fadeRate;
        public int FadeRate {
            get { return _fadeRate.IntValue; }
            set { _fadeRate.IntValue = value; }
        }

        // <Property PgmName="FadeTime" AccessType="RW" Volatile="false">
        [EcgMapping("FadeTime")]
        private IEcgPropertyValue _fadeTime;
        public int FadeTime {
            get { return _fadeTime.IntValue; }
            set { _fadeTime.IntValue = value; }
        }

        // <Property PgmName="ExtendedFadeTimeBase" AccessType="RW" Volatile="false">
        [EcgMapping("ExtendedFadeTimeBase")]
        private IEcgPropertyValue _extendedFadeTimeBase;
        public int ExtendedFadeTimeBase {
            get { return _extendedFadeTimeBase.IntValue; }
            set { _extendedFadeTimeBase.IntValue = value; }
        }

        [EcgMapping("ExtendedFadeTimeMultiplier")]
        private IEcgPropertyValue _extendedFadeTimeMultiplier;
        public int ExtendedFadeTimeMultiplier {
            get { return _extendedFadeTimeMultiplier.IntValue; }
            set { _extendedFadeTimeMultiplier.IntValue = value; }
        }

        // <Property PgmName="OperatingMode" AccessType="RW" Volatile="false" PermW="17">
        [EcgMapping("OperatingMode")]
        private IEcgPropertyValue _operatingMode;
        public int OperatingMode {
            get { return _operatingMode.IntValue; }
            set { _operatingMode.IntValue = value; }
        }

        // <Property PgmName="DimmingCurve" AccessType="RW" Volatile="false">
        [EcgMapping("DimmingCurve")]
        private IEcgPropertyValue _dimmingCurve;
        public int DimmingCurve {
            get { return _dimmingCurve.IntValue; }
            set { _dimmingCurve.IntValue = value; }
        }

        // <Property PgmName="FastFadeTime" AccessType="RW" Volatile="false">
        [EcgMapping("FastFadeTime")]
        private IEcgPropertyValue _fastFadeTime;
        public int FastFadeTime {
            get { return _fastFadeTime.IntValue; }
            set { _fastFadeTime.IntValue = value; }
        }

        // <Property PgmName="Address" AccessType="RW" Volatile="false">
        [EcgMapping("Address")]
        private IEcgPropertyValue _address;
        public int Address {
            get { return _address.IntValue; }
            set { _address.IntValue = value; }
        }
#pragma warning restore CS0649

        public DaliPropertyData(IMvxMessenger messenger, IConfigurationService configurationService) : base(messenger, configurationService)
        {
        }

        public bool IsAvailable => _theConfigurationService.GetCurrentConfiguration().InterfaceTypes.Contains("DALI2");
    }
}
