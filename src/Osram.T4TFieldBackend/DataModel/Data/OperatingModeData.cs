﻿using MvvmCross.Plugin.Messenger;
using Osram.TFTBackend.ConfigurationService.Contracts;
using Osram.TFTBackend.ConfigurationService.DataModel;
using Osram.TFTBackend.DataModel.Data.Contracts;
using Osram.TFTBackend.DataModel.Feature.Helper;
using System.Collections.Generic;
using System.Linq;

namespace Osram.TFTBackend.DataModel.Data
{
    public class OperatingModeData : FeatureImpl, IOperatingModeData
    {
        private Dictionary<string, OperatingMode> _operatingModesDictonary { get; set; }
        private Dictionary<string, EcgProperty> _propertyDictonary { get; set; }

        private static readonly Dictionary<OperatingModeEnum, string> _enumToMode1Dim = new Dictionary<OperatingModeEnum, string>
        {
            {OperatingModeEnum.None, null},
            {OperatingModeEnum.OnOff, "On/Off"},
            {OperatingModeEnum.AstroDim, "AstroDIM"},
        };

        private static readonly Dictionary<OperatingModeEnum, string> _enumToMode4Dim = new Dictionary<OperatingModeEnum, string>
        {
            {OperatingModeEnum.None, null},
            {OperatingModeEnum.OnOff, "DALI"},
            {OperatingModeEnum.AstroDim, "AstroDIM (DALI)"},
        };

        private static readonly Dictionary<OperatingModeReportEnum, string> _enumToModeReport = new Dictionary<OperatingModeReportEnum, string>
        {
            {OperatingModeReportEnum.None, null},
            {OperatingModeReportEnum.OnOff, "On/Off"},
            {OperatingModeReportEnum.AstroDim, "AstroDIM"},

            {OperatingModeReportEnum.StepDim_AstroDim_Dali_WiringSelection, "StepDIM/AstroDIM/DALI (wiring selection)"},
            {OperatingModeReportEnum.AstroDim_Dali, "AstroDIM (DALI)"},
            {OperatingModeReportEnum.AstroDim_PresenceDetection_Dali, "AstroDIM + Presence Detection (DALI)"},
            {OperatingModeReportEnum.StepDim_Dali, "StepDIM (DALI)"},
            {OperatingModeReportEnum.StepDimInverse_Dali, "StepDIM inverse (DALI)"},
            {OperatingModeReportEnum.Autodetect_OnOff_StepDimInverse_Dali, "Autodetect, On/OFF-StepDIM inverse (DALI)"},
            {OperatingModeReportEnum.Autodetect_AstroDim_StepDimInverse_Dali, "Autodetect, AstroDIM-StepDIM inverse (DALI)"},
            {OperatingModeReportEnum.MainsDim_Dali, "MainsDIM (DALI)"},
            {OperatingModeReportEnum.Dali, "DALI"},
            {OperatingModeReportEnum.Dali_SSO, "DALI & Soft Switch Off"},
            {OperatingModeReportEnum.Dali2_TF, "DALI-2 and TD Autodetect: DALI or TouchDim"},
            {OperatingModeReportEnum.Dali2_Only, "DALI-2 only"},
            {OperatingModeReportEnum.Dali2_SSO, "DALI-2 and Soft Switch Off"},
            {OperatingModeReportEnum.TouchDim, "TouchDim"},
            {OperatingModeReportEnum.Corridor, "Corridor Functionality"},
            {OperatingModeReportEnum.DaliTD, "DALI and TD"},
            {OperatingModeReportEnum.Dali_Only, "DALI only"},
            {OperatingModeReportEnum.Dali_EO, "DALI and Enhanced Options"},
            {OperatingModeReportEnum.TD, "TD"},
            {OperatingModeReportEnum.CF, "CF"},
        };

        private static readonly IList<string> _propertiesToExclude = new List<string>
        {
            "OperatingMode"
        };

        public OperatingModeData(IMvxMessenger messenger, IConfigurationService configurationService) : base(messenger, configurationService)
        {
            Init();
        }

        public void Init()
        {
        }

        public OperatingMode GetCurrentOperatingMode()
        {
            foreach (var operatingMode in _operatingModesDictonary.Values)
            {
                bool isValid = true;
                foreach (var key in operatingMode.ReferencePropertyDictonary.Keys.Except(_propertiesToExclude))
                {
                    EcgProperty ecgProperty;
                    if (_propertyDictonary.TryGetValue(key, out ecgProperty))
                    {
                        if (ecgProperty.Value.IntValue.ToString() != operatingMode.ReferencePropertyDictonary[key].ToString())
                        {
                            isValid = false;
                            break;
                        }
                    }
                }
                if (isValid)
                {
                    return operatingMode;
                }
            }

            return new OperatingMode(null);
        }

        public OperatingModeEnum GetCurrentOperatingModeEnum()
        {
            var operatingMode = GetCurrentOperatingMode();
            var entry = _enumToMode1Dim.FirstOrDefault(om => om.Value == operatingMode.Name);
            OperatingModeEnum modeEnum = entry.Key;
            if (string.IsNullOrEmpty(entry.Value))
                modeEnum = _enumToMode4Dim.FirstOrDefault(om => om.Value == operatingMode.Name).Key;
            return modeEnum;
        }

        public OperatingModeReportEnum GetCurrentReportOperatingModeEnum()
        {
            var operatingMode = GetCurrentOperatingMode();
            var entry = _enumToModeReport.FirstOrDefault(om => om.Value == operatingMode.Name);
            return entry.Key;
        }

        public string GetCurrentReportOperatingModeName()
        {
            var operatingMode = GetCurrentOperatingMode();
            var entry = _enumToModeReport.FirstOrDefault(om => om.Value == operatingMode.Name);
            return entry.Value;
        }

        public void SetOperatingMode(OperatingMode operatingMode)
        {
            foreach (var key in operatingMode.ReferencePropertyDictonary.Keys.Except(_propertiesToExclude))
            {
                EcgProperty ecgProperty;
                if (_propertyDictonary.TryGetValue(key, out ecgProperty))
                {
                    ecgProperty.Value.IntValue = int.Parse(operatingMode.ReferencePropertyDictonary[key]);
                }
            }
        }

        public void SetOperatingModeEnum(OperatingModeEnum operatingModeEnum)
        {
            if (!SetOperatingModeEnum(_enumToMode1Dim, operatingModeEnum))
                SetOperatingModeEnum(_enumToMode4Dim, operatingModeEnum);
        }

        private bool SetOperatingModeEnum(Dictionary<OperatingModeEnum, string> dictionary, OperatingModeEnum operatingModeEnum)
        {
            string name;
            if (dictionary.TryGetValue(operatingModeEnum, out name))
            {
                OperatingMode operatingMode;
                if (_operatingModesDictonary.TryGetValue(name, out operatingMode))
                {
                    SetOperatingMode(operatingMode);
                    return true;
                }
            }
            return false;
        }

        public override void OnConfigurationChange(Dictionary<string, OperatingMode> operatingModesDictonary, Dictionary<string, EcgProperty> propertyDictonary)
        {
            _operatingModesDictonary = operatingModesDictonary;
            _propertyDictonary = propertyDictonary;
        }
    }
}