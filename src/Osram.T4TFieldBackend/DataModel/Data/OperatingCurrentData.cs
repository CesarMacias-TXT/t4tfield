﻿using MvvmCross.Plugin.Messenger;
using Osram.TFTBackend.ConfigurationService.Contracts;
using Osram.TFTBackend.ConfigurationService.DataModel;
using Osram.TFTBackend.DataModel.Data.Contracts;
using Osram.TFTBackend.DataModel.Feature.Helper;

namespace Osram.TFTBackend.DataModel.Data
{
    public class OperatingCurrentData : FeatureImpl, IOperatingCurrentData
    {
#pragma warning disable CS0649
        [EcgMapping("OTConfig-2:DefaultOpCurrent", false, "PwmConfig-0:DefaultOpCurrent", "OTConfig-3:DefaultOpCurrent")]
        private IEcgPropertyValue _operatingCurrent;
        public int OperatingCurrent {
            get { return _operatingCurrent.IntValue; }
            set { _operatingCurrent.IntValue = value; }
        }

        /// <summary>
        /// Gets PhysicalMinLevel value.
        /// </summary>
        /// <value>The Physical Min Level.</value>
        public int PhysicalMinLevel {
            get {
                return GetPhysicalMinLevel();
            }
        }

        /// <summary>
        /// Gets PhysicalMaxLevel value.
        /// </summary>
        /// <value>The Physical Max Level.</value>
        public int PhysicalMaxLevel {
            get {
                return GetPhysicalMaxLevel();
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the PML value is obtained directly from device description file property or
        /// is calculated based on other parameters like minimum rated dimming level,real minimum output current etc.
        /// </summary>
        /// <value>
        /// <c>true</c> if the PML fixed; otherwise, <c>false</c>.
        /// </value>
        public bool IsPMLFixed {
            get;
            set;
        }

        private int GetPhysicalMinLevel()
        {
            return _operatingCurrent.IntMinValue;
        }

        private int GetPhysicalMaxLevel()
        {
            return _operatingCurrent.IntMaxValue;
        }
#pragma warning restore CS0649

        protected override bool IsEnabledAdvanced()
        {
            if (OperatingCurrent == 0)
            {
                return false;
            }
            return true;
        }

        public OperatingCurrentData(IMvxMessenger messenger, IConfigurationService configurationService) : base(messenger, configurationService)
        {
        }
    }
}