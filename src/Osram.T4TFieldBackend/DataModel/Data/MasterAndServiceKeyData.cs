﻿using MvvmCross.Plugin.Messenger;
using Osram.TFTBackend.ConfigurationService.Contracts;
using Osram.TFTBackend.ConfigurationService.DataModel;
using Osram.TFTBackend.DataModel.Data.Contracts;
using Osram.TFTBackend.DataModel.Feature.Contracts;
using Osram.TFTBackend.DataModel.Feature.Helper;

namespace Osram.TFTBackend.DataModel.Data
{
    public class MasterAndServiceKeyData : FeatureImpl, IMasterAndServiceKeyData
    {
#pragma warning disable CS0649
        [EcgMapping("MSK-0:MasterPwd", false, "PwmConfig-0:MasterPwd")]
        private IEcgPropertyValue _masterPassword;
        public uint MasterPassword
        {
            get { return (uint) _masterPassword.IntValue; }
            set { _masterPassword.IntValue = (int) value; }
        }

        private uint GetEncryptedMasterPassword()
        {
            if (_encryptedMasterPassword is NotFoundEcgProperty)
                return (uint)_masterPassword.IntValue;
            return (uint)_encryptedMasterPassword.IntValue;
        }

        [EcgMapping("BIO-0:MasterPwd")]
        private IEcgPropertyValue _encryptedMasterPassword;
        public uint EncryptedMasterPassword
        {
            get { return GetEncryptedMasterPassword(); }
        }

        [EcgMapping("MSK-0:NewMasterPwd")]
        private IEcgPropertyValue _newMasterPassword;
        public uint NewMasterPassword
        {
            get { return (uint) _newMasterPassword.IntValue; }
            set { _newMasterPassword.IntValue = (int) value; }
        }

        [EcgMapping("MSK-0:MasterPwdConfirm")]
        private IEcgPropertyValue _newMasterPasswordConfirmation;
        public int NewMasterPasswordConfirmation
        {
            get { return _newMasterPasswordConfirmation.IntValue; }
            set { _newMasterPasswordConfirmation.IntValue = value; }
        }

        [EcgMapping("MSK-0:MasterPwdStatusInfo")]
        private IEcgPropertyValue _masterPasswordStatusInformation;
        public int MasterPasswordStatusInformation
        {
            get { return _masterPasswordStatusInformation.IntValue; }
            set { _masterPasswordStatusInformation.IntValue = value; }
        }

        [EcgMapping("MSK-0:MasterPwdUnlockCounter")]
        private IEcgPropertyValue _masterPasswordUnlockCounter;
        public int MasterPasswordUnlockCounter
        {
            get { return _masterPasswordUnlockCounter.IntValue; }
            set { _masterPasswordUnlockCounter.IntValue = value; }
        }

        [EcgMapping("MSK-0:MasterPwdErrorCounter")]
        private IEcgPropertyValue _masterPasswordErrorCounter;
        public int MasterPasswordErrorCounter
        {
            get { return _masterPasswordErrorCounter.IntValue; }
            set { _masterPasswordErrorCounter.IntValue = value; }
        }

        [EcgMapping("MSK-0:ServicePwd", false, "PwmConfig-0:ServicePwd")]
        private IEcgPropertyValue _servicePassword;
        public uint ServicePassword
        {
            get { return (uint) _servicePassword.IntValue; }
            set { _servicePassword.IntValue = (int) value; }
        }

        private uint GetEncryptedServicePassword()
        {
            if (_encryptedServicePassword is NotFoundEcgProperty)
                return (uint)_servicePassword.IntValue;
            return (uint)_encryptedServicePassword.IntValue;
        }

        [EcgMapping("BIO-0:ServicePwd")]
        private IEcgPropertyValue _encryptedServicePassword;
        public uint EncryptedServicePassword
        {
            get { return GetEncryptedServicePassword(); }
        }

        [EcgMapping("MSK-0:ServicePwdConfirm")]
        private IEcgPropertyValue _servicePasswordConfirmation;
        public int ServicePasswordConfirmation
        {
            get { return _servicePasswordConfirmation.IntValue; }
            set { _servicePasswordConfirmation.IntValue = value; }
        }

        [EcgMapping("MSK-0:ServicePwdStatusInfo")]
        private IEcgPropertyValue _servicePasswordStatusInformation;
        public int ServicePasswordStatusInformation
        {
            get { return _servicePasswordStatusInformation.IntValue; }
            set { _servicePasswordStatusInformation.IntValue = value; }
        }

        [EcgMapping("MSK-0:ServicePwdUnlockCounter")]
        private IEcgPropertyValue _servicePasswordUnlockCounter;
        public int ServicePasswordUnlockCounter
        {
            get { return _servicePasswordUnlockCounter.IntValue; }
            set { _servicePasswordUnlockCounter.IntValue = value; }
        }

        [EcgMapping("MSK-0:ServicePwdErrorCounter")]
        private IEcgPropertyValue _servicePasswordErrorCounter;
        public int ServicePasswordErrorCounter
        {
            get { return _servicePasswordErrorCounter.IntValue; }
            set { _servicePasswordErrorCounter.IntValue = value; }
        }

        [EcgMapping("MSK-0:PermUser", false, "PwmConfig-0:PermUser")]
        private IEcgPropertyValue _permissionsUser;
        public int PermissionsUser
        {
            get { return _permissionsUser.IntValue; }
            set { _permissionsUser.IntValue = value; }
        }

        [EcgMapping("MSK-0:PermService", false, "PwmConfig-0:PermService")]
        private IEcgPropertyValue _permissionsService;
#pragma warning restore CS0649

        public int PermissionsService
        {
            get { return _permissionsService.IntValue; }
            set { _permissionsService.IntValue = value; }
        }

        public MasterAndServiceKeyData(IMvxMessenger messenger, IConfigurationService configurationService) : base(messenger, configurationService)
        {
        }

        public bool IsAvailable { get; set; }

        public ProtectionLock Lock { get; set; }

        public bool IsSelectedToBeProgrammed { get; set; }

        public FeatureStatus Status { get; set; }
    }
}
