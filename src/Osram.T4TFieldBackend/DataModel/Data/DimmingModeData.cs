﻿using System.Collections.Generic;
using MvvmCross.Plugin.Messenger;
using Osram.TFTBackend.ConfigurationService.Contracts;
using Osram.TFTBackend.DataModel.Feature.Helper;
using Osram.TFTBackend.DataModel.Data.Contracts;

namespace Osram.TFTBackend.DataModel.Data
{
    public class DimmingModeData : FeatureImpl, IDimmingModeData
    {
        public double MaxDimmingRange { get; set; }

        public double MinDimmingRange { get; set; }

        public List<DimmingMode> ModesCollection
        { get; set; }

        public int SelectedDimmingMode
        { get; set; }


        public DimmingModeData(IMvxMessenger messenger, IConfigurationService configurationService) : base(messenger, configurationService)
        {
            Init();
        }

        public void Init()
        {
            ModesCollection = new List<DimmingMode>();
            ModesCollection.Add(DimmingMode.DimModeAstroStepDIM);
        }
    }
}