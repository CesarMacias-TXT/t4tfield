﻿namespace Osram.TFTBackend.DataModel.Data
{
    public class DDUserData 
    {
        public string mode { get; set; }
        public string tool_name { get; set; }
        public string tool_version { get; set; }
        public string hostid { get; set; }
    }
}
