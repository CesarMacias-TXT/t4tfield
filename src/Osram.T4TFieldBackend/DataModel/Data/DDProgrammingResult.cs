﻿namespace Osram.TFTBackend.DataModel.Data
{
    public class DDProgrammingResult 
    {
        public string hostid { get; set; }
        public string companyid { get; set; }
        public string sessionid { get; set; }
        public string progdate { get; set; }
        public string prog_if { get; set; }
        public string gtin { get; set; }
        public string fw { get; set; }
        public string hw { get; set; }
        public string modelid { get; set; }
        public string serial { get; set; }
        public string config { get; set; }
        public string counter { get; set; }
        public string result { get; set; }
    }
}
