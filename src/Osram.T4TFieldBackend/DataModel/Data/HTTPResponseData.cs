﻿namespace Osram.TFTBackend.DataModel.Data
{
    public class HTTPResponseData
    {
        public string data { get; set; }
        public string key { get; set; }
    }
}
