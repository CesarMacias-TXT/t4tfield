﻿using System.Collections.Generic;

namespace Osram.TFTBackend.DataModel.Data
{
    public class DDProgrammingData 
    {
        public string hostid { get; set; }
        public string programmer_tool { get; set; }
        public string programmer_version { get; set; }
        public string companyid { get; set; }
        public string tool_name { get; set; }
        public string tool_version { get; set; }
        public string tool_id { get; set; }
        public string created { get; set; }
        public string drvcnt0 { get; set; }
        public string drvcnt1 { get; set; }
        public string modelid0 { get; set; }
        public string gtin0 { get; set; }
        public string fw0 { get; set; }
        public string gtin1 { get; set; }
        public string fw1 { get; set; }
        public Dictionary<string, string> data0 { get; set; }
        public string data1 { get; set; }
        public bool lum_locked { get; set; }
        public bool lum_single { get; set; }
        public bool disable_fam { get; set; }
        public bool verify { get; set; }
    }
}
