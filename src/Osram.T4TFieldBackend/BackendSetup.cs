﻿using MvvmCross;
using MvvmCross.IoC;
using Osram.TFTBackend.DataModel;
using Osram.TFTBackend.InitalizeDatabaseService;
using Osram.TFTBackend.InitalizeDatabaseService.Contracts;
using Osram.TFTBackend.NfcService.Contracts;
using Osram.TFTBackend.PersistenceService;
using System;
using System.Collections.Generic;
using System.Reflection;

namespace Osram.TFTBackend
{
    public static class BackendSetup
    {
        public static void InitIoC()
        {
            //TODO improve handle of InAppPurchaseService
            // register services
            CreatableTypes()
                .EndingWith("Service")
                .Except(typeof(InAppPurchaseService.InAppPurchaseService), typeof(InAppPurchaseService.InAppVerifyPurchaseService))
                //.Except(typeof(InAppPurchaseService.Fake.FakeInAppPurchaseService), typeof(InAppPurchaseService.Fake.FakeInAppVerifyPurchaseService))
                .AsInterfaces()
                .RegisterAsLazySingleton();

            var nfcReader = Mvx.IoCProvider.IoCConstruct<NfcService.NfcReader>();
            Mvx.IoCProvider.RegisterSingleton<INfcReader>(nfcReader);
            Mvx.IoCProvider.RegisterSingleton<IExtNfcReader>(nfcReader);

            // register DataStores
            Mvx.IoCProvider.LazyConstructAndRegisterSingleton<IConfigurationDataStore, ConfigurationDataStore>();
            Mvx.IoCProvider.LazyConstructAndRegisterSingleton<IDatabaseVersionDataStore, DatabaseVersionDataStore>();
            
            //register all "Data" used by Features
            CreatableTypes()
                .EndingWith("Data")
                .AsInterfaces()
                .RegisterAsSingleton();

            //register feature manager that creates and registers all the features implemented 
            Mvx.IoCProvider.ConstructAndRegisterSingleton<IFeatureManager, FeatureManager>();

            PopulateDatabase();
        }

        private static IEnumerable<Type> CreatableTypes()
        {
            return typeof(BackendSetup).GetTypeInfo().Assembly.CreatableTypes();
        }

        private static async void PopulateDatabase()
        {
            try
            {
                await Mvx.IoCProvider.Resolve<IInitalizeDatabaseService>().Initialize();
            }
            catch (Exception e)
            {
                Logger.Exception("PopulateDatabase failed", e);
            }
        }
    }
}
