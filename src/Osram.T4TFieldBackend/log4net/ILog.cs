﻿namespace Osram.TFTBackend.log4net
{
    public interface ILog
    {
        void Info(object message);
        void Error(object message);

    }
}