﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Osram.TFTBackend.log4net
{
    public class LogManager : ILog
    {
        private static ILog _logger = new LogManager();

        public void Info(object message)
        {
            Logger.Trace(message.ToString());
        }

        public void Error(object message)
        {
            Logger.Error(message.ToString());
        }

        public static ILog GetLogger()
        {
            return _logger;
        }
    }
}
