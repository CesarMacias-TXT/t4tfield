﻿using System;
using System.IO;

namespace Osram.TFTBackend.CryptographyService.Contracts
{
    public interface ICryptographyService
    {
        Stream DecryptedXmlStream { get; set; }

        Stream Encrypt(Stream streamToEncrypt);

        byte[] Encrypt(byte[] dataToEncrypt, string encryptionPassword);

        Stream Decrypt(Stream streamToDecrypt, string decryptionPassword = "");

        byte[] Decrypt(byte[] encryptedData, string encryptionPassword);

        byte[] ComputeSha1(byte[] input);

        string ComputeMD5(string input);

        byte[] ComputeMD5Hash(byte[] input);

        String ComputeMD5(Stream inputStream);
    }
}
