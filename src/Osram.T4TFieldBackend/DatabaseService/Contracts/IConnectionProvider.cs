﻿using SQLite;

namespace Osram.TFTBackend.DatabaseService.Contracts
{
    public interface IConnectionProvider
    {
        SQLiteAsyncConnection Connection { get; }

        /// <summary>
        /// The full path of the database file.
        /// </summary>
        string DbPath { get; }

        /// <summary>
        /// The directory of the database file.
        /// </summary>
        string DbDirectory { get; }

        void CloseConnection();

        void InitConnection();
    }
}
