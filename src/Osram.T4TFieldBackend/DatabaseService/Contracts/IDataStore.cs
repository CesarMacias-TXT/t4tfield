using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Osram.TFTBackend.DatabaseService.Contracts
{
    /// <summary>
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public interface IDataStore<T>
    {
        /// <summary>
        /// Returns the database directory.
        /// </summary>
        String DatabaseDirectory { get; }

        /// <summary>
        ///     Remarks: overwrites entities with same id. Please call CanSaveAsync before to check this.
        /// </summary>
        /// <param name="entity"></param>        
        /// <returns></returns>
        Task SaveOrUpdateAsync(T entity);

        /// <summary>
        ///     Load all entities
        /// </summary>
        /// <returns></returns>
        Task<List<T>> LoadAllAsync();

        /// <summary>
        ///     An entity with key
        /// </summary>
        /// <param name="someKey"></param>
        /// <returns></returns>
        Task<T> LoadAsync(object someKey);

        /// <summary>
        ///     An entity with the same key exists
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        Task<bool> ExistsAsync(T entity);

        /// <summary>
        ///     Checks if an entity can be saved with the current params set. (either for new entity or update)
        /// </summary>
        /// <param name="entity"></param>
        /// <returns>False it name already taken.</returns>
        Task<bool> CanSaveAsync(T entity);

        /// <summary>
        ///     Removes the given entity form the data store.
        /// </summary>
        /// <param name="entity"></param>
        Task RemoveAsync(T entity);

        /// <summary>
        ///     Inserts a new entity always.
        /// </summary>
        /// <param name="entity"></param>
        Task InsertAsync(T entity);

        /// <summary>
        ///     Updates an exisitng entity.
        /// </summary>
        /// <param name="entity"></param>
        Task UpdateAsync(T entity);

        /// <summary>
        /// Removes all entries async.
        /// </summary>
        /// <returns>The all async.</returns>
        Task RemoveAllAsync();

        /// <summary>
        /// Counts all async.
        /// </summary>
        /// <returns>The all async.</returns>
        Task<int> CountAllAsync();
    }
}