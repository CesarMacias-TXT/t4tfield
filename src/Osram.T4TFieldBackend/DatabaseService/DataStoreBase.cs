using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using SQLite;
using Osram.TFTBackend.DatabaseService.Contracts;

namespace Osram.TFTBackend.DatabaseService
{
    public abstract class DataStoreBase<T> : IDataStore<T> where T : class, new()
    {
        private readonly IConnectionProvider _connectionProvider;

        protected SQLiteAsyncConnection Connection => _connectionProvider.Connection;

        public String DatabaseDirectory => _connectionProvider.DbDirectory;

        protected readonly Task EnsureTableCreated;

        protected DataStoreBase(IConnectionProvider connectionProvider)
        {
            _connectionProvider = connectionProvider;
            EnsureTableCreated = Connection.CreateTableAsync<T>();
        }

        public virtual async Task<bool> CanSaveAsync(T entity)
        {
            await EnsureTableCreated;
            return !await ExistsAsync(entity);
        }

        public virtual async Task<T> LoadAsync(object someKey)
        {
            await EnsureTableCreated;
            return await Connection.FindAsync<T>(someKey);
        }

        public abstract Task<bool> ExistsAsync(T entity);

        public async Task<List<T>> LoadAllAsync()
        {
            await EnsureTableCreated;
            return await Connection.Table<T>().ToListAsync();
        }

        public virtual async Task RemoveAsync(T entity)
        {
            await EnsureTableCreated;
            await Connection.DeleteAsync(CreatePersistableAdapter(entity));
        }

        public virtual async Task SaveOrUpdateAsync(T entity)
        {
            await EnsureTableCreated;
            await Connection.InsertOrReplaceAsync(CreatePersistableAdapter(entity));
        }

        public virtual async Task InsertAsync(T entity)
        {
            await EnsureTableCreated;
            await Connection.InsertAsync(CreatePersistableAdapter(entity));
        }

        public virtual async Task UpdateAsync(T entity)
        {
            await EnsureTableCreated;
            await Connection.UpdateAsync(CreatePersistableAdapter(entity));
        }

        public async Task RemoveAllAsync()
        {
            await EnsureTableCreated;
            await Connection.DropTableAsync<T>();
            await Connection.CreateTableAsync<T>();
        }


        public async Task<int> CountAllAsync()
        {
            await EnsureTableCreated;
            return await Connection.Table<T>().CountAsync();
        }

        protected virtual T CreatePersistableAdapter(T entity)
        {
            return entity;
        }
    }
}