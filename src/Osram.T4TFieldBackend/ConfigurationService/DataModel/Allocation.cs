﻿namespace Osram.TFTBackend.ConfigurationService.DataModel
{
    public class Allocation
    {
        public ByteOrder NetworkByteOrder;
        public int MemoryBank;
        public int ByteOffset;
        public int BitOffset;
        public int ByteCount;
        public int BitCount;
        public enum ByteOrder
        {
            Lsb,
            Msb
        }
    }
}
