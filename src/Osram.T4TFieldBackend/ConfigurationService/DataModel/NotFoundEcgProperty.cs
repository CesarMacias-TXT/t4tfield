﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Osram.TFTBackend.ConfigurationService.DataModel
{
    public class NotFoundEcgProperty : IEcgPropertyValue
    {
        public int IntValue
        {
            get { return 0; }
            set {  }
        }

        public bool BoolValue
        {
            get { return false; }
            set {  }
        }

        public int IntMinValue => 0;

        public int IntMaxValue => 0;

        public int IntDefaultValue => 0;

        public int IntOffValue => 0;

        private byte _byteValue;
        public byte ByteValue
        {
            get { return _byteValue; }
            set { _byteValue = value; }
        }

        public ProgrammingValue ProgrammingValue
        {
            get { return 0; }
            set { }
        }

        public static NotFoundEcgProperty Property = new NotFoundEcgProperty();
    }
}
