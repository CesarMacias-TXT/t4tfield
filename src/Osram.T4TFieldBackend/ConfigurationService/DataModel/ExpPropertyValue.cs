﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Osram.TFTBackend.ConfigurationService.DataModel
{
    public class ExpPropertyValue : PropertyValue
    {
        private readonly int _startIndex;
        private readonly int _min;
        private readonly int _max;
        private readonly int _defaultValue;
        private readonly Dictionary<int, double> _valueMapping;
        public ExpPropertyValue(int startIndex, int min, int max, int defaultValue, long rawValue, Dictionary<int, double> valueMapping)
        {
            ProgrammingValue = rawValue;
            _startIndex = startIndex;
            _min = min;
            _max = max;
            _defaultValue = defaultValue;
            _valueMapping = valueMapping;
        }

        public override double DoubleValue
        {
            get { return _valueMapping[(int)ProgrammingValue]; }
        }
    }
}
