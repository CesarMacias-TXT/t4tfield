﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Osram.TFTBackend.ConfigurationService.DataModel
{
    public class EnumPropertyValue : PropertyValue
    {
        public sealed override int IntValue
        {
            get { return (int)ProgrammingValue; }
            set { ProgrammingValue = value; }
        }

        public override int IntMinValue { get; }

        public override int IntMaxValue { get; }

        public override int IntDefaultValue { get; }

        public EnumPropertyValue(int value, int min, int max, int defaultValue)
        {
            IntValue = value;
            IntMinValue = min;
            IntMaxValue = max;
            IntDefaultValue = defaultValue;
        }

    }
}
