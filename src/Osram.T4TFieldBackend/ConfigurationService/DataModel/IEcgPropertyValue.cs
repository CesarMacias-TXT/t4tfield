﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Osram.TFTBackend.ConfigurationService.DataModel
{
    public interface IEcgPropertyValue
    {
        /// <summary>
        /// The raw value from the xml
        /// </summary>
        ProgrammingValue ProgrammingValue { get; set; }

        /// <summary>
        /// The property value as int.
        /// </summary>
        int IntValue { get; set; }

        /// <summary>
        /// The property value interpreted as boolean.
        /// </summary>
        bool BoolValue { get; set; }

        /// <summary>
        /// The minimum value as int.
        /// </summary>
        int IntMinValue { get; }
        /// <summary>
        /// The maximum value as int.
        /// </summary>
        int IntMaxValue { get; }
        /// <summary>
        /// The default value as int.
        /// </summary>
        int IntDefaultValue { get; }

        /// <summary>
        /// The off value as int.
        /// </summary>
        int IntOffValue { get; }
    }

}
