﻿using System;

namespace Osram.TFTBackend.ConfigurationService.DataModel
{
    public class ScalePropertyValue : PropertyValue
    {
        private long _offset;
        private double _multiplier;
        private long _exponent;

        public long Value
        {
            get { return Calcvalue(ProgrammingValue); }
            set { ProgrammingValue = CalcProgrammingValue(value); }
        }

        public override int IntValue
        {
            get { return (int)Value; }
            set { Value = value; }
        }

        public override bool BoolValue
        {
            get { return Value != 0; }
            set { Value = value ? 1 : 0; }
        }

        private long _min;
        public long Min => Calcvalue(_min);

        public override int IntMinValue => (int) Min;

        private long _max;
        public long Max => Calcvalue(_max);
        public override int IntMaxValue => (int)Max;

        private long _off;
        public long Off => Calcvalue(_off);
        public override int IntOffValue => (int)Off;

        private long _default;
        public long DefaultValue => Calcvalue(_default);
        public override int IntDefaultValue => (int) DefaultValue;

        public ScalePropertyValue(long offset, double multiplier, long exponent, long min, long max, long off,
            long defaultValue, long rawValue)
        {
            _offset = offset;
            _multiplier = multiplier;
            _exponent = exponent;
            _min = min;
            _max = max;
            _off = off;
            _default = defaultValue;
            ProgrammingValue = rawValue;
        }

        private long Calcvalue(long rawValue)
        {
            if (_exponent == 0)
                return (long)Math.Round(_offset + rawValue * _multiplier);
            return (long)Math.Round(_offset + ProgrammingValue * _multiplier * (long)Math.Round(Math.Pow(10, _exponent)));
        }

        private long CalcProgrammingValue(long value)
        {
            if (_exponent == 0)
                return (long)Math.Round((value - _offset) / _multiplier);
            return (long)Math.Round((value - _offset) / _multiplier / (long)Math.Round(Math.Pow(10, _exponent)));
        }
    }
}
