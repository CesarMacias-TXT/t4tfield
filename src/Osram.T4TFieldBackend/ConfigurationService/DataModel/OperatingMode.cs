﻿using System;
using System.Collections.Generic;

namespace Osram.TFTBackend.ConfigurationService.DataModel
{
    public class OperatingMode
    {
        /// <summary>
        /// The name of the operating mode
        /// </summary>
        public String Name { get; }
        /// <summary>
        /// Dictionary conataing all reference properties for the operating mode.
        /// Key is the property name and the value is the property value to set to switch to the operating mode
        /// </summary>
        public Dictionary<String, String> ReferencePropertyDictonary { get; }

        public OperatingMode(String name)
        {
            Name = name;
            ReferencePropertyDictonary = new Dictionary<string, string>();
        }
    }
}