﻿using System;
using System.Collections.Generic;

namespace Osram.TFTBackend.ConfigurationService.DataModel
{
    public class Permission
    {
        /// <summary>
        /// The name of the permission
        /// </summary>
        public String Name { get; }
        /// <summary>
        /// The value of the permission
        /// </summary>
        public int Value { get; }

        public Permission(String name, String valueString)
        {
            Name = name;
            int value = 0;
            Int32.TryParse(valueString, out value);
            Value = value;
        }
    }
}