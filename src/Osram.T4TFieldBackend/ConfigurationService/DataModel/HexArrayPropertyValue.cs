﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Osram.TFTBackend.ConfigurationService.DataModel
{
    public class HexArrayPropertyValue : PropertyValue
    {
        public HexArrayPropertyValue(string setValue)
        {
            ProgrammingValue = new ProgrammingValue(ConvertHexStringToByteArray(setValue));
        }

        private static byte[] ConvertHexStringToByteArray(string hexString)
        {
            if (hexString.StartsWith("0x", StringComparison.OrdinalIgnoreCase))
            {
                hexString = hexString.Remove(0, 2);
            }
            if (hexString.Length % 2 != 0)
            {
                hexString = "0" + hexString;
            }

            byte[] data = new byte[hexString.Length / 2];
            for (int index = 0; index < data.Length; index++)
            {
                string byteValue = hexString.Substring(index * 2, 2);
                data[index] = GetByteFromHexString(byteValue);
            }

            return data;
        }
        private static byte GetByteFromHexString(String hexByte)
        {
            try
            {
                return Convert.ToByte(hexByte, 16);
            }
            catch (Exception)
            {
                return 0;
            }
        }
    }
}
