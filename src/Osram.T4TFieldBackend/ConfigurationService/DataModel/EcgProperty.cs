﻿using System;

namespace Osram.TFTBackend.ConfigurationService.DataModel
{
    public class EcgProperty : IEcgPropertyValue
    {
        public String PropertyName;
        public PropertyValue Value;
        public Allocation Allocation;
        public int PermissionIndex;
        public bool ReadOnly;

        public ProgrammingValue ProgrammingValue
        {
            get { return Value.ProgrammingValue; }
            set { Value.ProgrammingValue = value; }
        }

        public int IntValue
        {
            get { return Value.IntValue; }
            set { Value.IntValue = value; }
        }

        public bool BoolValue
        {
            get { return Value.BoolValue; }
            set { Value.BoolValue = value; }
        }

        public int IntMinValue => Value.IntMinValue;

        public int IntMaxValue => Value.IntMaxValue;

        public int IntDefaultValue => Value.IntDefaultValue;

        public int IntOffValue => Value.IntOffValue;
    }
}
