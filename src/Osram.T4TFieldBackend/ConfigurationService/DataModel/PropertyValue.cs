﻿using System;

namespace Osram.TFTBackend.ConfigurationService.DataModel
{
    public class PropertyValue
    {
        /// <summary>
        /// Its the raw value from the xml and is read/written to th ECG.
        /// </summary>
        public ProgrammingValue ProgrammingValue;

        public virtual int IntValue
        {
            get { return (int)ProgrammingValue; }
            set { ProgrammingValue = value; }
        }

        public virtual bool BoolValue
        {
            get { throw new NotSupportedException("Bool data type not supported"); }
            set { throw new NotSupportedException("Bool data type not supported"); }
        }

        public virtual double DoubleValue
        {
            get { throw new NotSupportedException("Double data type not supported"); }
        }

        public virtual int IntMinValue { get; }
        public virtual int IntMaxValue { get; }
        public virtual int IntDefaultValue { get; }

        public virtual int IntOffValue { get; }
    }
}
