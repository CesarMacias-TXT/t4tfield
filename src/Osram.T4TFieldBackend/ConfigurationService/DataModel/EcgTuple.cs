﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Osram.TFTBackend.ConfigurationService.DataModel
{
    public class EcgTuple
    {
        public String XmlConfiguration;

        public byte[] ThePicture;
    }
}
