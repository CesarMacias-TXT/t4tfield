﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Osram.TFTBackend.ConfigurationService.DataModel
{
    public class ProgrammingValue
    {
        private long _numberProgrammingValue;
        private byte[] _arrayProgrammingValue;

        public bool IsArray => _arrayProgrammingValue != null;

        public ProgrammingValue(long value)
        {
            _numberProgrammingValue = value;
        }

        public ProgrammingValue(byte[] value)
        {
            _arrayProgrammingValue = value;
        }

        public byte this[int index]
        {
            get { return _arrayProgrammingValue[index]; }
        }

        public static implicit operator long(ProgrammingValue value)
        {
            return value._numberProgrammingValue;
        }

        public static implicit operator ProgrammingValue(long value)
        {
            return new ProgrammingValue(value);
        }
    }
}
