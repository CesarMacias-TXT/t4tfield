﻿using System;
using System.Collections.Generic;
using Osram.TFTBackend.BaseTypes;
using Osram.TFTBackend.ConfigurationService.Contracts;

namespace Osram.TFTBackend.ConfigurationService.DataModel
{
    public class Configuration : IConfiguration
    {
        public Gtin Gtin { get; set; }
        public string FW_Major { get; set; }
        public string HW_Major { get; set; }
        public EcgSelector Selector { get; set; }
        public string LuminaireName { get; set; }
        public string BasicCode { get; set; }
        public string DeviceDescription { get; set; }
        public string ToolName { get; set; }
        public string ToolVersion { get; set; }
        public string ToolID { get; set; }
        public string ModelID { get; set; }
        public string Created { get; set; }
        public string Lum_locked { get; set; }
        public string Lum_single { get; set; }
        public string Disable_fam { get; set; }
        public string Verify { get; set; }
        public bool UnlockDriver { get; set; }
        public UInt32 ReworkKey { get; set; }
        public string Scope { get; set; }

        public IEnumerable<string> InterfaceTypes { get; set; }

        public string ImageFileName { get; set; }

        public byte[] ThePicture { get; set; }

        /// <summary>
        /// In case of a production configuratione the new master password. 
        /// Invalid password is 0xFFFFFFFF.
        /// </summary>
        public UInt32 NewMasterPassword { get; set; }

        /// <summary>
        /// The old master password of the ECG.
        /// Invalid password is 0xFFFFFFFF.
        /// In case old and new password are the same old is set to 0x00000000. We have to check old and new which is valid :-((.
        /// </summary>
        public UInt32 OldMasterPassword { get; set; }

        public Dictionary<String, EcgProperty> PropertyDictonary { get; set; }

        public Dictionary<String, Permission> PermissionsDictonary { get; set; }

        public Dictionary<String, OperatingMode> OperatingModesDictonary { get; set; }
    }
}
