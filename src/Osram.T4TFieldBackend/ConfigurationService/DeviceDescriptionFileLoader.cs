﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Xml.Linq;
using Osram.TFTBackend.BaseTypes;
using Osram.TFTBackend.ConfigurationService.DataModel;

namespace Osram.TFTBackend.ConfigurationService
{
    public class DeviceDescriptionFileLoader
    {
        public Configuration Deserialize(String xmlDeviceDescription)
        {
            Configuration configuration = new Configuration();
            using (StringReader reader = new StringReader(xmlDeviceDescription))
            {
                XDocument xdocument = XDocument.Load(reader);

                configuration.PropertyDictonary = new Dictionary<string, EcgProperty>(100);
                ReadProductionPasswords(configuration, xdocument.Root);
                XElement ecgDescription = GetEcgDescription(xdocument.Root);
                ReadPFMetaData(configuration, xdocument.Root);
                ReadMetaData(configuration, ecgDescription.Element("MetaData"));
                ReadPermissions(configuration, ecgDescription.Element("Permissions"));
                ReadOperatingModes(configuration, ecgDescription.Element("OperatingModes"));
                IEnumerable<EcgProperty> properties = ReadProperties(ecgDescription);
                foreach (var property in properties)
                {
                    Logger.Trace($"Property {property.PropertyName}");
                    configuration.PropertyDictonary.Add(property.PropertyName, property);
                }
            }

            return configuration;
        }

        private void ReadProductionPasswords(Configuration configuration, XElement rootElement)
        {
            const UInt32 invalidPassword = 0xFFFFFFFF;
            configuration.NewMasterPassword = invalidPassword;
            configuration.OldMasterPassword = invalidPassword;
            if (rootElement.Name != "LuminaireDescription")
                return;
            XElement oem = rootElement.Element("MetaData")?.Element("OEM");
            if (oem == null)
                return;
            configuration.NewMasterPassword = (UInt32)GetAttributeOrDefault(oem, "code", invalidPassword);
            XElement old = rootElement.Element("MetaData")?.Element("Rework");
            if (old == null)
                return;
            configuration.OldMasterPassword = (UInt32)GetAttributeOrDefault(old, "code", invalidPassword);
            if ((configuration.NewMasterPassword != invalidPassword) && (configuration.OldMasterPassword == invalidPassword))
            {
                configuration.OldMasterPassword = configuration.NewMasterPassword;
            }
        }

        private XElement GetEcgDescription(XElement rootElement)
        {
            // we have to handle luminaire and description files which differ in structure
            XElement descriptionsElement = rootElement.Element("ECGDescriptions");
            if (descriptionsElement != null)
            {
                return descriptionsElement.Descendants("ECGDescription").FirstOrDefault();
            }
            return rootElement;
        }

        private void ReadMetaData(Configuration configuration, XElement metadata)
        {
            configuration.LuminaireName = metadata.Element("DeviceTypeName")?.Value;
            configuration.Gtin = new Gtin(Int64.Parse(metadata.Element("GTIN")?.Value));
            configuration.FW_Major = metadata.Element("FW_Major")?.Value;
            configuration.HW_Major = metadata.Element("HW_Major")?.Value;
            byte fwVersion = Byte.Parse(metadata.Element("FW_Major")?.Value);
            byte hwVersion = Byte.Parse(metadata.Element("HW_Major")?.Value);
            configuration.Selector = new EcgSelector(configuration.Gtin.GtinValue, fwVersion, hwVersion);
            configuration.BasicCode = metadata.Element("BasicCode")?.Value;
            configuration.DeviceDescription = metadata.Element("DeviceDescription")?.Value;
            configuration.InterfaceTypes = metadata.Elements("InterfaceType").Select(x => x?.Value).Where(x => !string.IsNullOrEmpty(x)).ToList();
            configuration.ImageFileName = metadata.Element("ImageFileName")?.Value;
            EcgProperty ratedDimmingRange = ReadRatedDimmingRange(metadata.Element("RatedDimmingRange"));
            configuration.PropertyDictonary.Add(ratedDimmingRange.PropertyName, ratedDimmingRange);
            configuration.ModelID = metadata.Element("ModelID")?.Value;
            configuration.Scope = metadata.Element("Scope")?.Value;            
        }

        private void ReadPFMetaData(Configuration configuration, XElement rootElement)
        {
            if (rootElement.Name != "LuminaireDescription")
                return;

            configuration.ToolName = GetAttributeOrDefault(rootElement, "ToolName", "");
            configuration.ToolVersion = GetAttributeOrDefault(rootElement, "ToolVersion", "");
            configuration.ToolID = GetAttributeOrDefault(rootElement, "ToolID", "");
            configuration.Created = GetAttributeOrDefault(rootElement, "Created", "");

            XElement metadata = rootElement.Element("MetaData");
            if (metadata == null)
                return;
            configuration.Lum_locked = metadata.Element("LockSettings")?.Value;
            configuration.Lum_single = metadata.Element("IsEnableSingleProgramming")?.Value;
            configuration.Disable_fam = metadata.Element("FamilyProgramming")?.Value == "true" ? "false" : "true";
            configuration.Verify = metadata.Element("VerifyParameters")?.Value;            
            configuration.UnlockDriver = Convert.ToBoolean(metadata.Element("Rework")?.Attribute("selected").Value);
            configuration.ReworkKey = Convert.ToUInt32(metadata.Element("Rework")?.Attribute("code").Value.Substring(2, 8));
        }

        private EcgProperty ReadRatedDimmingRange(XElement element)
        {
            int min = 0;
            int max = 100;
            if (element != null)
            {
                double minD = GetAttributeOrDefault(element, "Min", 0.0);
                min = (int)Math.Round(minD);
                double maxD = GetAttributeOrDefault(element, "Max", 100.0);
                max = (int)Math.Round(maxD);
            }
            EcgProperty ratedDimmingRange = new EcgProperty();
            ratedDimmingRange.PropertyName = "RatedDimmingRange";
            ratedDimmingRange.Value = new ScalePropertyValue(0, 1, 0, min, max, 0, 0, 0);
            return ratedDimmingRange;
        }

        private void ReadPermissions(Configuration configuration, XElement permissions)
        {
            Logger.Trace("ReadPermissions:");
            configuration.PermissionsDictonary = new Dictionary<String, Permission>(5);

            if (permissions == null)
                return;

            foreach (var permissionElement in permissions.Elements())
            {
                String name = permissionElement.Value;
                String value = permissionElement.Attribute("Value").Value;
                Permission permission = new Permission(name, value);
                configuration.PermissionsDictonary.Add(name, permission);
                Logger.Trace($"Permission: {name} - {value}");
            }
        }

        private void ReadOperatingModes(Configuration configuration, XElement operatingModes)
        {
            Logger.Trace("ReadOperatingModes:");
            configuration.OperatingModesDictonary = new Dictionary<String, OperatingMode>(5);

            if (operatingModes == null)
                return;

            foreach (var operatingModeElement in operatingModes.Elements())
            {
                // in the xml configuration can be empty Mode elements!!! skip these
                XAttribute nameAttribute = operatingModeElement.Attribute("Name");
                if (nameAttribute != null)
                {
                    String name = nameAttribute.Value;
                    OperatingMode operatingMode = new OperatingMode(name);
                    ReadOperatingModeReferenceProperties(operatingMode, operatingModeElement);
                    configuration.OperatingModesDictonary.Add(name, operatingMode);
                    Logger.Trace($"Operating Mode: {name}");
                }
            }
        }

        private void ReadOperatingModeReferenceProperties(OperatingMode operatingMode, XElement operatingModeElement)
        {
            foreach (var settingElement in operatingModeElement.Elements())
            {
                String referencePropertyName = settingElement.Attribute("Ref").Value;
                String propertyValue = settingElement.Attribute("Value").Value;
                operatingMode.ReferencePropertyDictonary.Add(referencePropertyName, propertyValue);
            }
        }

        private IEnumerable<EcgProperty> ReadProperties(XElement description)
        {
            var query = from nfcAllocation in description.Descendants("Allocation")
                        where (String)nfcAllocation.Attribute("Interface") == "NFC"
                        select nfcAllocation;
            foreach (var nfcAllocation in query)
            {
                EcgProperty ecgProperty = new EcgProperty();
                try
                {
                    ecgProperty.Allocation = ReadAllocation(nfcAllocation);
                    var prop = nfcAllocation.Parent;
                    ecgProperty.PropertyName = prop.Attribute("PgmName").Value;
                    ecgProperty.ReadOnly = prop.Attribute("AccessType").Value.IndexOf("W", StringComparison.OrdinalIgnoreCase) < 0;
                    ecgProperty.PermissionIndex = (int)GetAttributeOrDefault(prop, "PermW", 0);
                    ecgProperty.Value = ReadFormat(prop.Element("Format"));
                }
                catch (Exception e)
                {
                    Logger.Error("ReadProperties: Failed to deserialize property", e);
                    continue;
                }
                yield return ecgProperty;
            }
        }

        private PropertyValue ReadFormat(XElement format)
        {
            XElement valueElement = format.Elements().First();
            if (valueElement.Name == "Scale")
            {
                return ReadScaleProperty(valueElement);
            }
            if ((valueElement.Name == "EnumValues") || (valueElement.Name == "Enum"))
            {
                return ReadEnumValuesProperty(valueElement);
            }
            if (valueElement.Name == "ExpValues")
            {
                return ReadExpValuesProperty(valueElement);
            }
            if (valueElement.Name == "HexBytes")
            {
                return ReadHexValuesProperty(valueElement);
            }
            throw new NotImplementedException($"Implement {valueElement.Name} format type here");
        }

        private PropertyValue ReadHexValuesProperty(XElement valueElement)
        {
            String defaultValue = GetAttributeOrDefault(valueElement, "Default", "");
            return new HexArrayPropertyValue(GetAttributeOrDefault(valueElement, "SetValue", defaultValue));
        }

        private ScalePropertyValue ReadScaleProperty(XElement valueElement)
        {
            long min = GetAttributeOrDefault(valueElement, "Min", 0);
            long defaultValue = GetAttributeOrDefault(valueElement, "Default", 0);
            return new ScalePropertyValue(
                GetAttributeOrDefault(valueElement, "Offset", 0),
                GetAttributeOrDefault(valueElement, "Multiplier", 1.0),
                GetAttributeOrDefault(valueElement, "Exponent", 0),
                min,
                GetAttributeOrDefault(valueElement, "Max", 0),
                GetAttributeOrDefault(valueElement, "Off", min),
                defaultValue,
                GetAttributeOrDefault(valueElement, "SetValue", defaultValue)
                );
        }

        private EnumPropertyValue ReadEnumValuesProperty(XElement valueElement)
        {
            int min = (int)GetAttributeOrDefault(valueElement, "Min", 0);
            long defaultValue = GetAttributeOrDefault(valueElement, "Default", 0);
            return new EnumPropertyValue(
                (int)GetAttributeOrDefault(valueElement, "SetValue", defaultValue),
                min,
                (int)GetAttributeOrDefault(valueElement, "Max", 0),
                (int)defaultValue
                );
        }

        private ExpPropertyValue ReadExpValuesProperty(XElement valueElement)
        {
            int startIndex = (int)GetAttributeOrDefault(valueElement, "StartIndex", 0);
            int min = (int)GetAttributeOrDefault(valueElement, "Min", 0);
            long defaultValue = GetAttributeOrDefault(valueElement, "Default", 0);
            return new ExpPropertyValue(
                startIndex,
                min,
                (int)GetAttributeOrDefault(valueElement, "Max", 0),
                (int)defaultValue,
                (int)GetAttributeOrDefault(valueElement, "SetValue", defaultValue),
                ReadExpValues(startIndex, valueElement)
                );
        }

        private Dictionary<int, double> ReadExpValues(int startIndex, XElement valueElement)
        {
            Dictionary<int, double> valueMapping = new Dictionary<int, double>(10);
            foreach (var value in valueElement.Elements())
            {
                valueMapping.Add(startIndex++, GetAttributeOrDefault(value, "Value", 0.0));
            }
            return valueMapping;
        }

        private Allocation ReadAllocation(XElement nfcAllocation)
        {
            Allocation allocation = new Allocation()
            {
                NetworkByteOrder = GetAttributeOrDefault(nfcAllocation, "NetworkByteOrder", "true") == "false" ? Allocation.ByteOrder.Lsb : Allocation.ByteOrder.Msb,
                MemoryBank = GetSubValue(nfcAllocation, "MemAddress", "Bank"),
                ByteOffset = GetSubValue(nfcAllocation, "MemAddress", "Byte"),
                BitOffset = GetSubValue(nfcAllocation, "MemAddress", "Bit"),
                ByteCount = GetSubValue(nfcAllocation, "Size", "ByteCount"),
                BitCount = GetSubValue(nfcAllocation, "Size", "BitCount"),
            };

            return allocation;
        }

        private String GetAttributeOrDefault(XElement element, String attributeName, String defaultValue)
        {
            XAttribute attribute = element.Attribute(attributeName);
            if (attribute == null)
                return defaultValue;
            return attribute.Value;
        }

        private long GetAttributeOrDefault(XElement element, String attributeName, long defaultValue)
        {
            XAttribute attribute = element.Attribute(attributeName);
            if (attribute == null)
                return defaultValue;
            String stringValue = attribute.Value.Trim();
            if (stringValue.StartsWith("0x", StringComparison.OrdinalIgnoreCase))
                return Convert.ToInt64(stringValue, 16);
            long value;
            if (!Int64.TryParse(stringValue, out value))
            {
                UInt64 unsignedValue;
                if (UInt64.TryParse(stringValue, out unsignedValue))
                {
                    value = (long)unsignedValue;
                }
                else
                    value = 0;
            }
            return value;
        }

        private double GetAttributeOrDefault(XElement element, String attributeName, double defaultValue)
        {
            XAttribute attribute = element.Attribute(attributeName);
            if (attribute == null)
                return defaultValue;
            return Double.Parse(attribute.Value, CultureInfo.InvariantCulture);
        }

        private int GetSubValue(XElement element, String subElementName, String attributeName = null, int defaultValue = 0)
        {
            if (attributeName != null)
            {
                String value = element.Element(subElementName)?.Attribute(attributeName)?.Value;
                if (String.IsNullOrWhiteSpace(value))
                    return defaultValue;
                return Int32.Parse(value);
            }
            String elementValue = element.Element(subElementName)?.Value;
            if (String.IsNullOrWhiteSpace(elementValue))
                return defaultValue;
            return Int32.Parse(elementValue);
        }
    }
}
