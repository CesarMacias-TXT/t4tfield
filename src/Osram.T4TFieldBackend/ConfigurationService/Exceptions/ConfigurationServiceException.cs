﻿using System;

namespace Osram.TFTBackend.ConfigurationService.Exceptions
{
    public class ConfigurationServiceException : Exception
    {
        public const string Tag = "ConfigurationServiceException";

        public void Trace(string message, params object[] args)
        {
            Logger.TaggedTrace(Tag, message, args);
        }
    }
}
