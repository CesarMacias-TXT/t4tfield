﻿using System;
using System.Collections.Generic;
using MvvmCross.Plugin.Messenger;
using Osram.TFTBackend.ConfigurationService.Contracts;
using Osram.TFTBackend.ConfigurationService.DataModel;
using Osram.TFTBackend.Messaging;

namespace Osram.TFTBackend.ConfigurationService
{
    public class ConfigurationService : IConfigurationService
    {
        private IConfiguration _theCurrentIConfiguration;
        private readonly IMvxMessenger _messenger;
        private readonly Dictionary<String, EcgProperty> _emptyEcgPropertyDictionary = new Dictionary<string, EcgProperty>();
        private readonly Dictionary<String, OperatingMode> _emptyOperatingModeDictionary = new Dictionary<string, OperatingMode>();

        public ConfigurationService(IMvxMessenger messenger)
        {
            _messenger = messenger;
        }

        public IConfiguration SelectConfiguration(EcgTuple ecg)
        {
            _theCurrentIConfiguration = null;
            if (ecg != null)
            {
                _theCurrentIConfiguration = DeserializeConfigurationXml(ecg.XmlConfiguration);
                if(_theCurrentIConfiguration != null)
                    _theCurrentIConfiguration.ThePicture = ecg.ThePicture;
            }
            _messenger.Publish(new Events.ConfigurationChangedEvent(this, _theCurrentIConfiguration));
            return _theCurrentIConfiguration;
        }

        public void ResetConfiguration()
        {
            _theCurrentIConfiguration = null;
            _messenger.Publish(new Events.ConfigurationResetEvent(this, true));
        }

        public IConfiguration GetCurrentConfiguration()
        {
            return _theCurrentIConfiguration;
        }

        public IEnumerable<EcgProperty> GetConfigurationProperties()
        {
            return _theCurrentIConfiguration?.PropertyDictonary.Values;
        }

        public Dictionary<string, EcgProperty> GetConfigurationPropertyDictionary()
        {
            return _theCurrentIConfiguration != null ? _theCurrentIConfiguration.PropertyDictonary : _emptyEcgPropertyDictionary;
        }

        private Configuration DeserializeConfigurationXml(string xml)
        {
            try
            {
                DeviceDescriptionFileLoader loader = new DeviceDescriptionFileLoader();
                return loader.Deserialize(xml);
            }
            catch (Exception e)
            {
                Logger.Exception("Deserializing xml failed.", e);
            }
            return null;
        }

        public Dictionary<string, OperatingMode> GetOperatingModesDictonary()
        {
            return _theCurrentIConfiguration != null ? _theCurrentIConfiguration.OperatingModesDictonary : _emptyOperatingModeDictionary;
        }
    }
}
