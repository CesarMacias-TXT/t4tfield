﻿using System;
using Osram.TFTBackend.BaseTypes;
using System.Collections.Generic;
using Osram.TFTBackend.ConfigurationService.DataModel;

namespace Osram.TFTBackend.ConfigurationService.Contracts
{
    public interface IConfiguration
    {
        Gtin Gtin { get; set; }
        string FW_Major { get; set; }
        string HW_Major { get; set; }
        EcgSelector Selector { get; set; }
        string LuminaireName { get; set; }
        string BasicCode { get; set; }
        string DeviceDescription { get; set; }
        string ToolName { get; set; }
        string ToolVersion { get; set; }
        string ToolID { get; set; }
        string Created { get; set; }
        string ModelID { get; set; }
        string Lum_locked { get; set; }
        string Lum_single { get; set; }
        string Disable_fam { get; set; }
        string Verify { get; set; }
        bool UnlockDriver { get; set; }
        UInt32 ReworkKey { get; set; }
        string Scope { get; set; }

        IEnumerable<string> InterfaceTypes { get; set; }

        string ImageFileName { get; set; }

        byte[] ThePicture { get; set; }

        /// <summary>
        /// In case of a production configuratione the new master password. 
        /// Invalid password is 0xFFFFFFFF.
        /// </summary>
        UInt32 NewMasterPassword { get; set; }

        /// <summary>
        /// The old master password of the ECG.
        /// Invalid password is 0xFFFFFFFF.
        /// In case old and new password are the same old is set to 0x00000000. We have to check old and new which is valid :-((.
        /// </summary>
        UInt32 OldMasterPassword { get; set; }

        Dictionary<String, EcgProperty> PropertyDictonary { get; set; }

        Dictionary<String, Permission> PermissionsDictonary { get; set; }

        Dictionary<String, OperatingMode> OperatingModesDictonary { get; set; }
    }
}
