﻿using System;
using System.Collections.Generic;
using Osram.TFTBackend.BaseTypes;
using Osram.TFTBackend.ConfigurationService.DataModel;

namespace Osram.TFTBackend.ConfigurationService.Contracts
{
    public interface IConfigurationService
    {
        IConfiguration SelectConfiguration(EcgTuple ecg);
        IConfiguration GetCurrentConfiguration();

        void ResetConfiguration();

        IEnumerable<EcgProperty> GetConfigurationProperties();

        Dictionary<String, EcgProperty> GetConfigurationPropertyDictionary();

        /// <summary>
        /// Returns the current operating modes dictionary. Key is the operation mode name.
        /// </summary>
        /// <returns></returns>
        Dictionary<String, OperatingMode> GetOperatingModesDictonary();
    }
}
