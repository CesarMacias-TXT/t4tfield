﻿using Osram.TFTBackend.ConfigurationService.Contracts;
using Osram.TFTBackend.ConfigurationService.DataModel;
using Osram.TFTBackend.DataModel.Data;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace Osram.TFTBackend.RestService.Contracts
{
    public interface IRestService
    {
        Task<string> GetStringAsync(string url);

        Task<Stream> GetStreamAsync(string url);

        Task<HttpResponseMessage> PostAsync(string url, Object content);

        Task<List<DDInfoData>> GetAsyncDDInfoDatas(string url);

        Task<List<DDInfoData>> GetAsyncNewDriversAvailable(string url, List<string> configurations);

        Task<Stream> GetAsyncXml(string url);    
        
        Task<Stream> GetAsyncPicture(string url);

        Task PostAsyncUserData();

        Task PostAsyncProgrammingDataLocal(IConfiguration configuration, List<EcgProperty> properties);

        Task PostAsyncProgrammingData(IConfiguration configuration, List<EcgProperty> properties);

        Task PostAsyncProgrammingResult(IConfiguration configuration, string serialNumber, string result);

        Task ImportNewConfigurations();
    }
}
