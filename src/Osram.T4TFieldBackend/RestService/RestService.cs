﻿using Newtonsoft.Json;
using Osram.TFTBackend.ConfigurationService.Contracts;
using Osram.TFTBackend.ConfigurationService.DataModel;
using Osram.TFTBackend.DataModel.Data;
using Osram.TFTBackend.ImportService.Contracts;
using Osram.TFTBackend.PersistenceService;
using Osram.TFTBackend.RestService.Contracts;
using Plugin.Settings.Abstractions;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Essentials;

namespace Osram.TFTBackend.RestService
{
    public class RestService : IRestService
    {
        //TEST
        private const string Url = "https://tuner4tronic.com/ddstore/api/v1/";
        private const string DriverDataUrl = "https://www.tuner4tronic.com/driverdata/api/v1/";
        //private const string DriverDataUrl = "https://staging.tuner4tronic.com/driverdata/api/v1/";

        private string CompanyID = "";
        private string SessionID = "";
        private int Counter;
        private string HostID = "";
        private string API_KEY = "";

        private readonly ISettings _settings;
        private IImportService _importService;
        private IConfigurationDataStore _configurationDataStore;

        HttpClient _client;

        public RestService(ISettings settings, IImportService importService, IConfigurationDataStore configurationDataStore, IT4THttpClient t4THttpClient)
        {
            _settings = settings;
            _importService = importService;
            _configurationDataStore = configurationDataStore;

            // Generate an uuid for the calls
            byte[] hashID = Encoding.UTF8.GetBytes(Plugin.DeviceInfo.CrossDeviceInfo.Current.Id);
            HostID = (new Guid(hashID.Take(16).ToArray())).ToString();

            _client = t4THttpClient as HttpClient;
            _client.DefaultRequestHeaders.Add("Accept", "application/json");
        }

        public async Task<string> GetStringAsync(string url)
        {
            try
            {
                var current = Connectivity.NetworkAccess;

                if (current == NetworkAccess.Internet)
                {
                    // Connection to internet is available
                    var response = await _client.GetStringAsync(Url + url);

                    return response;
                }
                else
                {
                    // TODO add _userDialog, connection needed
                    return null;
                }
            }
            catch (WebException e)
            {
                throw new Exception("WebException in RestService.GetStreamAsync", e);
            }
            catch (Exception e)
            {
                throw new Exception("Exception in RestService.GetStringAsync", e);
            }
        }

        public async Task<Stream> GetStreamAsync(string url)
        {
            try
            {
                var current = Connectivity.NetworkAccess;

                if (current == NetworkAccess.Internet)
                {
                    // Connection to internet is available
                    Stream response = await _client.GetStreamAsync(Url + url);

                    return response;
                }
                else
                {
                    return null;
                }
            }
            catch (WebException e)
            {
                throw new Exception("WebException in RestService.GetStreamAsync", e);
            }
            catch (Exception e)
            {
                throw new Exception("Exception in RestService.GetStreamAsync", e);
            }
        }

        public async Task<HttpResponseMessage> PostAsync(string url, Object content)
        {
            try
            {
                var current = Connectivity.NetworkAccess;

                if (current == NetworkAccess.Internet)
                {
                    // Connection to internet is available
                    var json = JsonConvert.SerializeObject(content);
                    var stringContent = new StringContent(json, Encoding.UTF8, "application/json");
                    stringContent.Headers.Add("X-API-KEY", API_KEY);

                    var response = await _client.PostAsync(DriverDataUrl + url, stringContent);

                    stringContent.Dispose();

                    return response;
                }
                else
                {
                    return null;
                }
            }
            catch (Exception e)
            {
                throw new Exception("Exception in RestService.PostAsync", e);
            }
        }

        public async Task<List<DDInfoData>> GetAsyncDDInfoDatas(string url)
        {
            string content = await GetStringAsync(url);
            List<DDInfoData> ddInfos = new List<DDInfoData>();

            if(content != null)
            {
                ddInfos = JsonConvert.DeserializeObject<List<DDInfoData>>(content.ToString());
            }

            return ddInfos;
        }

        public async Task<List<DDInfoData>> GetAsyncNewDriversAvailable(string url, List<string> configurations)
        {
            List<DDInfoData> ddInfos = await GetAsyncDDInfoDatas(url);

            List<string> configurationsNames = configurations.Select(x => x.Replace(".osrtud", ".xml")).ToList();

            ddInfos.RemoveAll(d => configurationsNames.Contains(d.xmlRef));

            return ddInfos;
        }

        public async Task<Stream> GetAsyncXml(string xmlRef)
        {
            Stream response = await GetStreamAsync("ddfile/" + xmlRef);

            return response;
        }

        public async Task<Stream> GetAsyncPicture(string xmlRef)
        {
            Stream response = await GetStreamAsync("pictures/" + xmlRef);

            return response;
        }

        public async Task PostAsyncUserData()
        {
            DDUserData userData = new DDUserData();
            userData.mode = "0";
            userData.tool_name = "T4T-F";
            userData.tool_version = "2.1.8";
            userData.hostid = HostID;

            //the user data need an 'fake' api key
            API_KEY = HostID;

            var response = await PostAsync("user_data", userData);
            var result = response.Content.ReadAsStringAsync().Result;

            CompanyID = JsonConvert.DeserializeObject<HTTPResponseData>(result).data;
            API_KEY = JsonConvert.DeserializeObject<HTTPResponseData>(result).key;
            Counter = 0;

            response.Dispose();
        }

        public async Task PostAsyncProgrammingDataLocal(IConfiguration configuration, List<EcgProperty> writableProperties)
        {
            DDProgrammingData programmmingData = new DDProgrammingData();

            programmmingData.hostid = HostID;
            programmmingData.programmer_tool = "T4T-F";
            programmmingData.programmer_version = "2.1.8";
            programmmingData.companyid = CompanyID;
            programmmingData.tool_name = "T4T-F";
            programmmingData.tool_version = "2.1.8";
            programmmingData.tool_id = HostID;
            programmmingData.created = DateTime.UtcNow.ToString();
            programmmingData.drvcnt0 = "1";
            programmmingData.drvcnt1 = "0";
            programmmingData.modelid0 = configuration.ModelID;
            programmmingData.gtin0 = "" + configuration.Gtin.GtinValue;
            programmmingData.fw0 = configuration.FW_Major;
            programmmingData.data0 = RetrieveProgrammingData(writableProperties);
            //only '0' properties have to set
            programmmingData.gtin1 = "";
            programmmingData.fw1 = "";
            programmmingData.data1 = "";
            programmmingData.lum_locked = false;
            programmmingData.lum_single = true;
            programmmingData.disable_fam = true;
            programmmingData.verify = false;
            
            var response = await PostAsync("prog_data", programmmingData);
            var result = response.Content.ReadAsStringAsync().Result;

            if (response.StatusCode == HttpStatusCode.OK)
            {
                SessionID = JsonConvert.DeserializeObject<HTTPResponseData>(result).data;
            }
            else
            {
                SessionID = "";
            }

            response.Dispose();
        }

        public async Task PostAsyncProgrammingData(IConfiguration configuration, List<EcgProperty> properties)
        {
            DDProgrammingData programmmingData = new DDProgrammingData();

            programmmingData.hostid = HostID;
            programmmingData.programmer_tool = "T4T-F";            
            programmmingData.programmer_version = "2.1.8";
            programmmingData.companyid = CompanyID;
            programmmingData.tool_name = configuration.ToolName;
            programmmingData.tool_version = configuration.ToolVersion;
            programmmingData.tool_id = configuration.ToolID;
            programmmingData.created = configuration.Created;
            programmmingData.drvcnt0 = "1";
            programmmingData.drvcnt1 = "0";
            programmmingData.modelid0 = configuration.ModelID;
            programmmingData.gtin0 = "" + configuration.Gtin.GtinValue;
            programmmingData.fw0 = configuration.FW_Major;
            programmmingData.data0 = RetrieveProgrammingData(properties);
            //only set '0' properties            
            programmmingData.gtin1 = "";
            programmmingData.fw1 = "";
            programmmingData.data1 = "";
            programmmingData.lum_locked = Convert.ToBoolean(configuration.Lum_locked);
            programmmingData.lum_single = Convert.ToBoolean(configuration.Lum_single);
            programmmingData.disable_fam = Convert.ToBoolean(configuration.Disable_fam);
            programmmingData.verify = Convert.ToBoolean(configuration.Verify);

            var response = await PostAsync("prog_data", programmmingData);
            var result = response.Content.ReadAsStringAsync().Result;

            if (response.StatusCode == HttpStatusCode.OK)
            {
                SessionID = JsonConvert.DeserializeObject<HTTPResponseData>(result).data;
            }
            else
            {
                SessionID = null;
            }

            response.Dispose();
        }

        public async Task PostAsyncProgrammingResult(IConfiguration configuration, string serialNumber, string result)
        {
            DDProgrammingResult programmmingResult = new DDProgrammingResult();

            programmmingResult.hostid = HostID;
            programmmingResult.companyid = CompanyID;
            programmmingResult.sessionid = SessionID;
            programmmingResult.progdate = DateTime.UtcNow.ToString();
            programmmingResult.prog_if = "4";
            programmmingResult.gtin = "" + configuration.Gtin.GtinValue;
            programmmingResult.fw = configuration.FW_Major;
            programmmingResult.hw = configuration.HW_Major;
            programmmingResult.modelid = configuration.ModelID;
            programmmingResult.serial = serialNumber;
            programmmingResult.config = "0";
            programmmingResult.counter = "" + Counter++;
            programmmingResult.result = result;            

            var response = await PostAsync("prog_result", programmmingResult);

            response.Dispose();
        }

        public async Task ImportNewConfigurations()
        {
            List<string> allConfigurations = await _configurationDataStore.LoadAllConfigurationsNames();
            //List<DDInfoData> newConfigurationsAvailable = await GetAsyncNewDriversAvailable("ddinfos?offset=-1&scope=field", allConfigurations);
            List<DDInfoData> newConfigurationsAvailable = await GetAsyncNewDriversAvailable("ddinfos?offset=-1", allConfigurations);

            foreach (var configuration in newConfigurationsAvailable)
            {
                Stream xmlStream = await GetAsyncXml(configuration.xmlRef);

                Stream pictureStream = await GetAsyncPicture(configuration.picRef);

                await Task.Run(() => _importService.ImportFromDDStore(configuration.xmlRef, xmlStream, pictureStream));
                //await _importService.ImportFromDDStore(configuration.xmlRef, xmlStream, pictureStream);

                xmlStream.Dispose();
                pictureStream.Dispose();
            }

            _settings.AddOrUpdateValue(InitalizeDatabaseService.InitalizeDatabaseService.LastUpdateOnlineServices, DateTime.UtcNow.ToString());
        }

        public Dictionary<string, string> RetrieveProgrammingData(List<EcgProperty> properties)
        {
            Dictionary<string, string> programmingData = new Dictionary<string, string>();

            foreach (var property in properties)
            {
                programmingData.Add(property.PropertyName, "" + property.Value.IntValue);
            }

            return programmingData;
        }
    }
}
