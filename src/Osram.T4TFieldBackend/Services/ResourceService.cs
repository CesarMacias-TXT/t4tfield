﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using Osram.TFTBackend.Services.Contracts;

namespace Osram.TFTBackend.Services
{
    public class ResourceService : IResourceService
    {
        private Assembly _assembly;
        private Assembly Assembly => _assembly ?? (_assembly = GetType().GetTypeInfo().Assembly);

        public String[] GetManifestResourceNames()
        {
            return Assembly.GetManifestResourceNames();
        }

        public Stream GetManifestResourceStream(string resourceName)
        {
            return Assembly.GetManifestResourceStream(resourceName);
        }
    }
}
