﻿using System;
using System.IO;

namespace Osram.TFTBackend.Services.Contracts
{
    public interface IResourceService
    {
        /// <summary>
        /// Returns the names of all the resources in this assembly.
        /// </summary>
        /// <returns>An array that contains the names of all the resources.</returns>
        String[] GetManifestResourceNames();

        /// <summary>
        /// Loads the specified manifest resource from this assembly.
        /// </summary>
        /// <param name="resourceName">The case-sensitive name of the manifest resource being requested.</param>
        /// <returns>The manifest resource; or null if no resources were specified during compilation or if the resource is not visible to the caller.</returns>
        Stream GetManifestResourceStream(String resourceName);
    }
}