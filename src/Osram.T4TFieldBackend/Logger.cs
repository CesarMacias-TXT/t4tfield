﻿using System;
using MvvmCross;
using MvvmCross.Logging;

namespace Osram.TFTBackend
{
    public class Logger : IMvxLog
    {
        private const string AppTag = "T4TField";
        private const string AppTagExc = "AppTagExc";

        public static void TaggedTrace(string tag, string message, params object[] args)
        {
            Mvx.IoCProvider.Resolve<IMvxLog>().Trace(tag, message, args);
        }

        public static void TaggedWarning(string tag, string message, params object[] args)
        {
            Mvx.IoCProvider.Resolve<IMvxLog>().Warn(tag, message, args);
        }

        public static void TaggedError(string tag, string message, params object[] args)
        {
            Mvx.IoCProvider.Resolve<IMvxLog>().Error(tag, message , args);
        }

        public static void TaggedException(string tag, string message, Exception exception, params object[] args)
        {
            TaggedError(tag, $"Message: {message}, Exception : {exception.GetBaseException()}", args);
        }

        public static void Trace(string message, params object[] args)
        {
            TaggedTrace(message, "" , args);
        }

        public static void Warning(string message, params object[] args)
        {
            TaggedWarning("AppTAg WArning" , message, args);
        }

        public static void Error(string message, params object[] args)
        {
            TaggedError("Error" + message, " ", args);
        }

        public static void Exception(string message, Exception exception, params object[] args)
        {
            TaggedException(message, "" , exception, args);
        }

        public bool Log(MvxLogLevel logLevel, Func<string> messageFunc, Exception exception = null, params object[] formatParameters)
        {
            throw new NotImplementedException();
        }

        public bool IsLogLevelEnabled(MvxLogLevel logLevel)
        {
            throw new NotImplementedException();
        }
    }
}