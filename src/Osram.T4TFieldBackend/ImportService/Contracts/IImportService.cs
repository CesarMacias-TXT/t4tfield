﻿using System;
using System.IO;
using System.Threading.Tasks;

namespace Osram.TFTBackend.ImportService.Contracts
{
    public interface IImportService
    {
        /// <summary>
        /// Import of a configuration file (osrtud, osrtul, osrtup). The file name is only for informational purposes.
        /// </summary>
        /// <param name="fileName">The file name without path.</param>
        /// <param name="configurationStream">The file itself as readonly, non seekable stream.</param>
        Task<bool> Import(String fileName, Stream configurationStream);

        Task<bool> ImportFromDDStore(String fileName, Stream xmlStream, Stream pictureStream);
    }
}
