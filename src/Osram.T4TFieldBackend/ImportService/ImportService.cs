﻿using System;
using System.IO;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Xml.Linq;
using MvvmCross.Plugin.Messenger;
using Osram.TFTBackend.BaseTypes.Exceptions;
using Osram.TFTBackend.CompressionService.Contracts;
using Osram.TFTBackend.CryptographyService.Contracts;
using Osram.TFTBackend.ImportService.Contracts;
using Osram.TFTBackend.Messaging;
using Osram.TFTBackend.PersistenceService.Contracts;

namespace Osram.TFTBackend.ImportService
{
    public class ImportService : IImportService
    {
        private readonly ICryptographyService _cryptographyService;
        private readonly ICompressionService _compressionService;
        private readonly IPersistenceService _persistenceService;
        private readonly IMvxMessenger _messenger;

        public ImportService(ICompressionService compressionService, ICryptographyService cryptographyService, IPersistenceService persistenceService, IMvxMessenger messenger)
        {
            _compressionService = compressionService;
            _cryptographyService = cryptographyService;
            _persistenceService = persistenceService;
            _messenger = messenger;
        }

        public async Task<bool> Import(string fileName, Stream configurationStream)
        {
            try
            {
                Logger.Trace($"ImportService.Import importing {fileName}");
                if (_compressionService.Decompress(configurationStream))
                {
                    // look at the CompressionService.DecompressedXmlStream
                    using (var deflatedStreamInMemory = new MemoryStream())
                    {
                        if (_compressionService.DecompressedXmlStream.CanSeek)
                        {
                            _compressionService.DecompressedXmlStream.Position = 0;
                        }
                        _compressionService.DecompressedXmlStream.CopyTo(deflatedStreamInMemory);

                        // take a quick look at the content
                        var deflatedConent = PeekAtContentOfStream(deflatedStreamInMemory);

                        // now send it to the PersistenceService
                        if (_persistenceService != null)
                        {
                            if (!string.IsNullOrEmpty(deflatedConent))
                            {
                                // call the Persistence service
                                await _persistenceService.ImportConfiguration(
                                    fileName,
                                    !IsValidXml(deflatedConent)
                                        ? _cryptographyService.Decrypt(deflatedStreamInMemory)
                                        //: _compressionService.DecompressedXmlStream,
                                        : deflatedStreamInMemory,
                                    _compressionService.DecompressedPictureStream);

                                _messenger.Publish(new Events.OsrtupImportFinished(this));
                                return true;
                            }
                        }
                    }
                }
                else
                {
                    Logger.Error($"ImportService.Import Error in decompress {fileName}");
                }
            }
            catch (OperationFailedException e)
            {
                Logger.Exception("Exception in ImportService.Import", e);
                return false;
            }
            catch (Exception e)
            {
                Logger.Exception("Exception in ImportService.Import", e);
            }
            return false;
        }

        public async Task<bool> ImportFromDDStore(string fileName, Stream xmlStream, Stream pictureStream)
        {
            try
            {
                Logger.Trace($"ImportService.ImportFromDDStore importing {fileName}");
                using (var deflatedStreamInMemory = new MemoryStream())
                {
                    if (xmlStream.CanSeek)
                    {
                        xmlStream.Position = 0;
                    }

                    xmlStream.CopyTo(deflatedStreamInMemory);

                    // take a quick look at the content
                    var deflatedContent = PeekAtContentOfStream(deflatedStreamInMemory);

                    // now send it to the PersistenceService
                    if (_persistenceService != null)
                    {
                        if (!string.IsNullOrEmpty(deflatedContent))
                        {
                            // call the Persistence service
                            await _persistenceService.ImportConfiguration(
                                fileName,
                                deflatedStreamInMemory,
                                pictureStream);

                            _messenger.Publish(new Events.OsrtupImportFinished(this));
                            return true;
                        }
                    }
                }
            }
            catch (OperationFailedException e)
            {
                Logger.Exception("Exception in ImportService.ImportFromDDStore", e);
                return false;
            }
            catch (Exception e)
            {
                Logger.Exception("Exception in ImportService.ImportFromDDStore", e);
            }
            return false;
        }

        private string PeekAtContentOfStream(MemoryStream memoryStream)
        {
            memoryStream.Position = 0;
            var sr = new StreamReader(memoryStream);
            return sr.ReadToEnd();
        }

        public static bool IsValidXml(string xmlString)
        {
            Regex tagsWithData = new Regex("<\\w+>[^<]+</\\w+>");

            //Light checking
            if (string.IsNullOrEmpty(xmlString) || tagsWithData.IsMatch(xmlString) == false)
            {
                return false;
            }

            try
            {
                var xmlDocument = XDocument.Parse(xmlString);
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }
    }
}
