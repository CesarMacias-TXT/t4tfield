﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Osram.TFTBackend.PasswordService.Contracts
{
    public interface IPasswordService
    {
        /// <summary>
        /// Inserts a new passwordEntry, no Id needs to be assigned as the database will assign one.
        /// </summary>
        /// <param name="passwordEntry"></param>
        /// <returns></returns>
        Task Insert(PasswordEntry passwordEntry);

        /// <summary>
        /// Deletes the passwordEntry identified by its database Id.
        /// </summary>
        /// <param name="passwordEntry"></param>
        /// <returns></returns>
        Task Delete(PasswordEntry passwordEntry);

        /// <summary>
        /// Updates new passwordEntry values for a given (existing) Id.
        /// </summary>
        /// <param name="passwordEntry"></param>
        /// <returns></returns>
        Task Update(PasswordEntry passwordEntry);

        /// <summary>
        /// Returns a sorted password entry list, last added passwords first
        /// </summary>
        /// <returns></returns>
        Task<List<PasswordEntry>>  Query();

        /// <summary>
        /// Returns a password with a specific id
        /// </summary>
        /// <returns></returns>
        Task<PasswordEntry> QueryById(int id);
    }
}
