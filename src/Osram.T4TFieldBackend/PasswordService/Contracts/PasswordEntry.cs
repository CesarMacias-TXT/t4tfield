﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SQLite;

namespace Osram.TFTBackend.PasswordService.Contracts
{
    [Table("PasswordEntry")]
    public class PasswordEntry
    {
        [PrimaryKey, AutoIncrement]
        public int Id { get; set; }

        /// <summary>
        /// The visual label for the password, will be displayed to the user.
        /// </summary>
        public String Label { get; set; }

        /// <summary>
        /// The 1. password byte in range from 0 to 255. 
        /// </summary>
        public int PasswordByte1 { get; set; }

        /// <summary>
        /// The 2. password byte in range from 0 to 255. 
        /// </summary>
        public int PasswordByte2 { get; set; }

        /// <summary>
        /// The 3. password byte in range from 0 to 255. 
        /// </summary>
        public int PasswordByte3 { get; set; }

        /// <summary>
        /// The 4. password byte in range from 0 to 255. 
        /// </summary>
        public int PasswordByte4 { get; set; }

        /// <summary>
        /// The insertion date, allows sorting by date to show the latest passwords first.
        /// </summary>
        public DateTime Date { get; set; }

        public byte[] PasswordBytes()
        {
            return new byte[] { (byte)PasswordByte4, (byte)PasswordByte3, (byte)PasswordByte2, (byte)PasswordByte1 };
        }

        public uint Password()
        {
            return BitConverter.ToUInt32(PasswordBytes(), 0);
        }
        
        public bool IsValid()
        {
            return  (PasswordByte1 >= 0) && (PasswordByte1 < 256) &&
                    (PasswordByte2 >= 0) && (PasswordByte2 < 256) &&
                    (PasswordByte3 >= 0) && (PasswordByte3 < 256) &&
                    (PasswordByte4 >= 0) && (PasswordByte4 < 256);
        }

        public override string ToString()
        {
            return Label;
        }
    }
}
