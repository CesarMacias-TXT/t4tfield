﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Osram.TFTBackend.DatabaseService;
using Osram.TFTBackend.DatabaseService.Contracts;
using Osram.TFTBackend.PasswordService.Contracts;

namespace Osram.TFTBackend.PasswordService
{
    public class PasswordService : DataStoreBase<PasswordEntry>, IPasswordService
    {
        public PasswordService(IConnectionProvider connectionProvider) : base(connectionProvider)
        {
        }

        public override async Task<bool> ExistsAsync(PasswordEntry entity)
        {
            return (await LoadAsync(entity.Id)) != null;
        }

        public async Task Insert(PasswordEntry passwordEntry)
        {
            await InsertAsync(passwordEntry);
        }

        public async Task Delete(PasswordEntry passwordEntry)
        {
            await RemoveAsync(passwordEntry);
        }

        public async Task Update(PasswordEntry passwordEntry)
        {
            await UpdateAsync(passwordEntry);
        }

        public async Task<List<PasswordEntry>> Query()
        {
            return await Connection.Table<PasswordEntry>().OrderByDescending(entry => entry.Date).ToListAsync();
        }


        public async Task<PasswordEntry> QueryById(int id)
        {
            List<PasswordEntry> list = await Query();
            return  list.Where(x => x.Id == id).First();
        }
    }
}
