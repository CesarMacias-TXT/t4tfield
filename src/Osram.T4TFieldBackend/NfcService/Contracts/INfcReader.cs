﻿using Osram.TFTBackend.NfcService.NfcDataTypes;

namespace Osram.TFTBackend.NfcService.Contracts
{
    /// <summary>
    /// Interface for reading and writing the tags memory.
    /// </summary>
    /// <remarks>
    /// Implementers note:
    /// This interface hides any tag and/or reader peculiarities as block and sector sizes.
    /// As for a tags memory there is no individual byte access instead the memory can only be read or written
    /// block wise. Therefore if a single byte read/write is requested, the interface implementation should read
    /// the block where the byte is contained and return only the requested byte.
    /// Another issues is communication reliability. If the ESG is powered it produces a lot of NFC command malfunctions.
    /// To tackle the NFC communication malfunctions use the following countermeasures:
    /// - Keep The NFC commands short. That means use the Read/Write Single Block commands instead of the Multiple block commands.
    ///   Communication takes longer but is more reliable.
    /// - Implement a retry counter in the read / write functions to repeat the read / write operation in case of an error.
    ///   Throw the NfcReaderException only if all retries failed.
    /// </remarks>
    public interface INfcReader
    {
        /// <summary>
        /// Reads len bytes from the given physical address.
        /// </summary>
        /// <remarks>
        /// Address and length are independent from the used tags block size. If address and/or len are not
        /// block aligned then it will handled internally.
        /// </remarks>
        /// <exception cref="NfcReaderException">Thrown in case of a read error.</exception>
        /// <param name="startAddress">The physical start address.</param>
        /// <param name="len">How many bytes to read.</param>
        /// <returns></returns>
        ByteArray Read(int startAddress, int len);

        /// <summary>
        /// Writes the given data starting with the given physical address.
        /// </summary>
        /// <exception cref="NfcReaderException">Thrown in case of a write error.</exception>
        /// <param name="startAddress">The physical start address.</param>
        /// <param name="data">The data to write</param>
        /// <returns></returns>
        void Write(int startAddress, ByteArray data);

        /// <summary>
        /// Tags serial number if a tag is found otherwise empty.
        /// </summary>
        string NfcTagSerial { get; }
    }

}
