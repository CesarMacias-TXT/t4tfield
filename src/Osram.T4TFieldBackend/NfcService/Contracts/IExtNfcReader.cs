using System;
using System.Threading.Tasks;
using Osram.TFTBackend.NfcService.NfcDataTypes;

namespace Osram.TFTBackend.NfcService.Contracts
{
    /// <summary>
    /// Extended reader interface.
    /// </summary>
    public interface IExtNfcReader : INfcReader
    {
        /// <summary>
        /// Readers connected state.
        /// </summary>
        bool Connected { get; }

        bool IsEnabled { get; }

        bool IsAvailable { get; }

        /// <summary>
        /// Retry counter in case of read / write errors.
        /// </summary>
        int RetryCount { get; set; }

        NfcReaderType ReaderType { get; }

        /// <summary>
        /// Connects to the NFC reader
        /// </summary>
        /// <returns></returns>
        bool Connect();
        /// <summary>
        /// Disconnects the reader.
        /// </summary>
        void Disconnect();

        /// <summary>
        /// Tries to find a NFC tag. If a tag is found NfcTagSerial and NfcTagType are set.
        /// </summary>
        Task  DetectTag();
        
        /// <summary>
        /// This event occurs when nfc adapter is enabled.
        /// </summary>
        event EventHandler<EventArgs> AdapterConnected;

        /// <summary>
        /// This event occurs when nfc adapter is disabled.
        /// </summary>
        event EventHandler<EventArgs> AdapterDisconnected;

        /// <summary>
        /// This event occurs when nfc tag is detected.
        /// </summary>
        event EventHandler<EventArgs> TagDetected;

        /// <summary>
        /// This event occurs when nfc tag is undetected.
        /// </summary>
        event EventHandler<EventArgs> TagUndetected;
    }
}