﻿namespace Osram.TFTBackend.NfcService.Contracts
{
    public interface IMobileNfcReader
    {
        bool IsAvailable { get; }
    }
}
