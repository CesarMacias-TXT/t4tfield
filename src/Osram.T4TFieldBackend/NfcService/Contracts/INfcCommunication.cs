﻿using System;
using System.Threading.Tasks;
using Osram.TFTBackend.NfcService.NfcDataTypes;

namespace Osram.TFTBackend.NfcService.Contracts
{
    public interface INfcCommunication
    {
        NfcReaderType ReaderType { get; }

        bool IsAvailable { get; }
        bool IsEnabled { get; }

        int RetryCount { get; set; }

        ByteArray SendCommand(ByteArray command);
        bool Connect();
        void Disconnect();
        Task DetectTag();

        event EventHandler<EventArgs> AdapterConnected;
        event EventHandler<EventArgs> AdapterDisconnected;
        event EventHandler<string> TagDetected;
        event EventHandler<EventArgs> TagUndetected;
    }
}