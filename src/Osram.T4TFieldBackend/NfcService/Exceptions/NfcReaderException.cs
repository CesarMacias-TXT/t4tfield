﻿using System;

namespace Osram.TFTBackend.NfcService.Exceptions
{
    /// <summary>
    /// NFC reader exceptions.
    /// </summary>
    public class NfcReaderException : Exception
    {
        public NfcReaderException() { }
        public NfcReaderException(string message) : base(message) { }
        public NfcReaderException(string message, Exception inner) : base(message, inner) { }
    }

}

