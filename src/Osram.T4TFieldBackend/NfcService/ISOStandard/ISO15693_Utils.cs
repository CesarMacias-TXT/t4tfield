﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Osram.TFTBackend.NfcService.NfcDataTypes;

namespace Osram.TFTBackend.NfcService.ISOStandard
{
    public static class ISO15693_Utils
    {
        private static ByteArray _command = new ByteArray(4);

        //Request flags 1 to 4
        const byte Subcarrier_Flag = 1;
        const byte Data_Rate_Flag = 2;
        const byte Inventory_Flag = 4;
        const byte Protocol_Extension_Flag = 8;
        //Request flags 5 to 8 when Inventory_flag = 0
        const byte Select_Flag = 16;
        const byte Address_Flag = 32;
        const byte Option_Flag = 64;
        //Request flags 5 to 8 when Inventory_flag = 1
        const byte AFI_Flag = 16;
        const byte Nb_Slots_Flag = 32;


        public enum ReturnCode
        {
            Success = 0x00, //Successful operation
            Mem_Not_Present = 0x01, //Location or memory bank not present
            Locked_Mem = 0x02, // Locked memory bank
            Inventory_Error = 0x03, // Inventory error (*)
            Timeout = 0x0D, // Timeout
            Command_Not_Implemented = 0x0E, //Command not implemented
            Command_Not_Valid = 0x0F, // Command not valid or incorrect formatting
        }

        public enum Command
        {
            Inventory = 0x01,
            Reading = 0x20,
            Writing = 0x21,
            Reading_Multiple = 0x23,
            GetSystemInfo = 0x2B,
            PresentSectorPassword = 0xB3
        }

        public enum RequestFlags
        {
            //flags used in Read and Write
            Read_Write_Option = Data_Rate_Flag | Protocol_Extension_Flag,
            //flags used in Inventory
            Inventory_Option = Data_Rate_Flag | Inventory_Flag | Nb_Slots_Flag
        }

        /// <summary>
        /// If set to false single byte addressing is used, some low memory tags dont support 2 byte addressing.
        /// </summary>
        public static bool UseProtocolExtension { get; set; } = true;

        public static ByteArray CreateReadSingleBlockCommand(int address)
        {
            _command.Clear();
            _command.Add((byte)(UseProtocolExtension ? Data_Rate_Flag | Protocol_Extension_Flag : Data_Rate_Flag));
            _command.Add((byte)Command.Reading);
            _command.Add((byte)(address & 0xff));
            if (UseProtocolExtension)
                _command.Add((byte)((address >> 8) & 0xFF));

            return _command;
        }

        public static ByteArray CreateReadMultipleBlockCommand(int address, int numberOfBlocks)
        {
            _command.Clear();
            _command.Add((byte)(UseProtocolExtension ? Data_Rate_Flag | Protocol_Extension_Flag : Data_Rate_Flag));
            _command.Add((byte)Command.Reading_Multiple);
            _command.Add((byte)(address & 0xff));
            if (UseProtocolExtension)
                _command.Add((byte)((address >> 8) & 0xFF));
            //The blocks are numbered from 00h to 1FFh in the request and the value is minus one (–1) in the field. 
            //For example, if the “Number of blocks” field contains the value 06h, seven blocks are read.
            _command.Add((byte)(numberOfBlocks - 1));

            return _command;
        }

        public static ByteArray PresentSectorPasswordCommand(byte[] nfcTagSerial, byte[] passwordToPresent, byte passwordIndex)
        {            
            _command.Clear();
            // Request Flags
            _command.Add(0x02);
            // PresentSectorPassword
            _command.Add((byte)Command.PresentSectorPassword);
            // IC Mfg Code
            _command.Add(nfcTagSerial[6]);
            // Password index
            _command.Add(passwordIndex);
            // Password
            _command.Add(passwordToPresent[0]);
            _command.Add(passwordToPresent[1]);
            _command.Add(passwordToPresent[2]);
            _command.Add(passwordToPresent[3]);

            return _command;
        }

        public static ByteArray RFTimeoutCommand(byte timeout)
        {
            _command.Clear();

            _command.Add(0x0C);
            _command.Add(0x00);
            _command.Add(0x2E);
            _command.Add(0xFB);
            //timeout in seconds
            _command.Add(timeout);
            _command.Add(0x00);

            return _command;
        }

        public static ByteArray CreateWriteSingleBlockCommand(int dataBlockIndex, byte[] dataToWrite)
        {
            _command.Clear();
            _command.Add((byte)(UseProtocolExtension ? Data_Rate_Flag | Protocol_Extension_Flag : Data_Rate_Flag));
            _command.Add((byte)Command.Writing);
            _command.Add((byte)(dataBlockIndex & 0xFF));
            if (UseProtocolExtension)
                _command.Add((byte)((dataBlockIndex >> 8) & 0xFF));
            _command.Add(dataToWrite[0]);
            _command.Add(dataToWrite[1]);
            _command.Add(dataToWrite[2]);
            _command.Add(dataToWrite[3]);
            return _command;
        }


        public static ByteArray CreateInventoryCommand()
        {
            _command.Clear();
            _command.Add((byte)RequestFlags.Inventory_Option);
            _command.Add((byte)Command.Inventory);
            _command.Add(0);
            return _command;
        }

        public static ByteArray CreateGetSystemInfoCommand(bool useProtocolExtension)
        {
            _command.Clear();
            _command.Add((byte)(useProtocolExtension ? Data_Rate_Flag | Protocol_Extension_Flag : Data_Rate_Flag));
            _command.Add((byte)Command.GetSystemInfo);
            return _command;
        }

    }
}
