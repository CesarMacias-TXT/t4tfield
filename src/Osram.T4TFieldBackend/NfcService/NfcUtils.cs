using System;


namespace Osram.TFTBackend.NfcService
{
    public static class NfcUtils
    {
        public static int BytesInBlock = 4;
        public static int ReadMultipleBlock_MaxNumberOfBlocks = 32;
        public static int ReadMultipleBlock_HalfNumberOfBlocks = ReadMultipleBlock_MaxNumberOfBlocks / 2;

        public static int GetDataBlockAddress(int byteAddress)
        {
            return (int)Math.Floor(byteAddress / (double)BytesInBlock);
        }

        internal static int GetTotalBlocks(int len,int startAddress)
        {
            int startBlockAddress = GetDataBlockAddress(startAddress);
            int endBlockAddress = GetDataBlockAddress(startAddress + len - 1);
            return  endBlockAddress - startBlockAddress + 1;
        }
    }
}