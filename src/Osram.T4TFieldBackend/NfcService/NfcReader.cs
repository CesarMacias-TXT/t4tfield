﻿using MvvmCross;
using MvvmCross.Plugin.Messenger;
using Osram.TFTBackend.Messaging;
using Osram.TFTBackend.NfcService.Contracts;
using Osram.TFTBackend.NfcService.ISOStandard;
using Osram.TFTBackend.NfcService.NfcDataTypes;
using System;
using System.Collections.Generic;
using System.Linq;
using Osram.TFTBackend.CryptographyService.Contracts;
using System.Threading.Tasks;
using Osram.TFTBackend.BaseTypes.Exceptions;
using Osram.TFTBackend.ConfigurationService.Contracts;

namespace Osram.TFTBackend.NfcService
{
    public class NfcReader : IExtNfcReader
    {
        INfcCommunication _nfcCommunication;
        IMvxMessenger _messenger;
        MvxSubscriptionToken subscrToken;
        private string _tagId;
        private bool _connected;
        
        private ICryptographyService _cryptographyService;
        IConfigurationService _configurationService;
        private byte[] Version = new byte[8];

        public NfcReader(IMvxMessenger messenger, ICryptographyService cryptographyService, IConfigurationService configurationService)
        {
            InitNfcCommunication();
            _messenger = messenger;
            subscrToken = _messenger.Subscribe<Events.OnNfcReaderUpdateEvent>(UpdateNfcCommunication);

            _cryptographyService = cryptographyService;

            _configurationService = configurationService;

            Version[0] = 0x75; Version[1] = 0x32; Version[2] = 0xe8; Version[3] = 0x63;
            Version[4] = 0x65; Version[5] = 0xe8; Version[6] = 0x41; Version[7] = 0xe9;
        }

        private void UpdateNfcCommunication(Events.OnNfcReaderUpdateEvent obj)
        {
            InitNfcCommunication();
        }

        private void InitNfcCommunication()
        {
            INfcCommunication newNfcCommunication;
            if (Mvx.IoCProvider.TryResolve(out newNfcCommunication))
            {
                if (_nfcCommunication != null)
                {
                    DetachNfcCommunicationEvents();
                }
                _nfcCommunication = newNfcCommunication;
                _nfcCommunication.RetryCount = RetryCount;
                if (_nfcCommunication != null)
                {
                    AttachNfcCommunicationEvents();
                }
            }
        }

        private void DetachNfcCommunicationEvents()
        {
            _nfcCommunication.AdapterConnected -= _nfcCommunication_AdapterConnected;
            _nfcCommunication.AdapterDisconnected -= _nfcCommunication_AdapterDisconnected;
            _nfcCommunication.TagDetected -= _nfcCommunication_TagDetected;
            _nfcCommunication.TagUndetected -= _nfcCommunication_TagUndetected;
        }

        private void AttachNfcCommunicationEvents()
        {
            _nfcCommunication.AdapterConnected += _nfcCommunication_AdapterConnected;
            _nfcCommunication.AdapterDisconnected += _nfcCommunication_AdapterDisconnected;
            _nfcCommunication.TagDetected += _nfcCommunication_TagDetected;
            _nfcCommunication.TagUndetected += _nfcCommunication_TagUndetected;
        }

        private void _nfcCommunication_TagUndetected(object sender, EventArgs e)
        {
            RaiseTagUndetected(sender, e);
        }

        private void _nfcCommunication_TagDetected(object sender, string tagId)
        {
            _tagId = tagId;
            RaiseTagDetected();
        }

        private void _nfcCommunication_AdapterDisconnected(object sender, EventArgs e)
        {
            RaiseAdapterDisconnected();
        }

        private void _nfcCommunication_AdapterConnected(object sender, EventArgs e)
        {
            RaiseAdapterConnected();
        }

        public bool Connected {
            get {
                return _connected;
            }
        }

        public bool IsAvailable {
            get {
                return _nfcCommunication?.IsAvailable ?? false;
            }
        }

        public bool IsEnabled {
            get {
                return _nfcCommunication?.IsEnabled ?? false;
            }
        }

        /// <summary>
        /// Nfc Tag serial number (ex. 00-A5-12-34-3E-...)
        /// </summary>
        public string NfcTagSerial {
            get {
                return _tagId;
            }
        }

        /// <summary>
        /// retry count for reading and writing
        /// </summary>
        public int RetryCount { get; set; } = 5;

        public NfcReaderType ReaderType {
            get {
                return _nfcCommunication.ReaderType;
            }
        }

        #region [ Events ]
        public event EventHandler<EventArgs> AdapterConnected;
        public event EventHandler<EventArgs> AdapterDisconnected;
        public event EventHandler<EventArgs> TagDetected;
        public event EventHandler<EventArgs> TagUndetected;

        private void RaiseTagDetected()
        {
            TagDetected?.Invoke(this, null);
        }
        private void RaiseTagUndetected(object sender, EventArgs e)
        {
            TagUndetected?.Invoke(this, e);
        }
        public void RaiseAdapterConnected()
        {
            _connected = true;
            AdapterConnected?.Invoke(this, null);
        }
        public void RaiseAdapterDisconnected()
        {
            _connected = false;
            AdapterDisconnected?.Invoke(this, null);
        }
        #endregion

        public bool Connect()
        {
            if (_connected = _nfcCommunication.Connect())
                DetectAddressingMode(SendGetSystemInfoCommand());
            return _connected;
        }

        private void DetectAddressingMode(int icReference)
        {
            switch (icReference)
            {
                case 0x38:      // ST25DV02K-W1 (PWM) 256 bytes uses only single byte addressing
                    ISO15693_Utils.UseProtocolExtension = false;
                    break;
                case 0x4D:      // M24LR16E-R (old) 2k bytes uses 2 byte addressing
                case 0x4E:      // M24LR16E-R
                    ISO15693_Utils.UseProtocolExtension = true;
                    break;
                default:
                    Logger.Error($"Unknown chip type {icReference:X2}");
                    ISO15693_Utils.UseProtocolExtension = true;
                    break;
            }
        }

        public Task DetectTag()
        {
            return _nfcCommunication.DetectTag();
        }

        public void Disconnect()
        {
            _nfcCommunication.Disconnect();
        }

        public ByteArray Read(int startAddress, int len)
        {
            Logger.Trace($"MobileNfcReader.Read Adr={startAddress:X}, Len={len}");
            int startBlockAddress = NfcUtils.GetDataBlockAddress(startAddress);
            int totalBlocksToRead = NfcUtils.GetTotalBlocks(len, startAddress);
            ByteArray array = ReadBlocks(startBlockAddress, totalBlocksToRead);
            Logger.Trace("MobileNfcReader.Read done.");
            return array.Mid(startAddress % NfcUtils.BytesInBlock, len);
        }

        public void Write(int startAddress, ByteArray data)
        {
            Logger.Trace($"MobileNfcReader.Write Adr={startAddress:X}, Len={data.Count}");
            WriteMultipleBlocks(startAddress, data);
            Logger.Trace("MobileNfcReader.Write done.");
        }

        private ByteArray ReadBlocks(int startAddress, int totalBlocks)
        {
            //uncomment this line if you want to use the real read multiple blocks command
            //var res = ReadMultipleBlocks(startAddress, totalBlocks);
            var res = ReadMultipleBlocksWithSingleBlockCommand(startAddress, totalBlocks);

            ByteArray array = new ByteArray();
            foreach (byte[] blockBytes in res.Values)
            {
                array.AddRange(blockBytes);
            }
            return array;
        }

        //commands for writing and reading nfc binary
        private Dictionary<short, byte[]> ReadMultipleBlocks(int startAddress, int totalBlocks)
        {
            Dictionary<short, byte[]> blocks = new Dictionary<short, byte[]>();

            //startAddress = 0;
            //totalBlocks = 192;
            byte[] temp;
            short keysCount = 0;

            int newStartAddress;
            int remainingBlocks;

            int sectorOffset = startAddress % NfcUtils.ReadMultipleBlock_MaxNumberOfBlocks;
            if (sectorOffset > 0)
            {
                int blocksFirstRead = NfcUtils.ReadMultipleBlock_MaxNumberOfBlocks - sectorOffset;
                if (blocksFirstRead > totalBlocks)
                {
                    blocksFirstRead = totalBlocks;
                }
                temp = SendReadMultipleBlockCommand(startAddress,
                    blocksFirstRead);
                blocks.Add(keysCount++, temp);
                newStartAddress = startAddress + blocksFirstRead;
                remainingBlocks = totalBlocks - blocksFirstRead;
            }
            else
            {
                newStartAddress = startAddress;
                remainingBlocks = totalBlocks;
            }

            if (remainingBlocks == 0)
                return blocks;

            int count = (int)Math.Ceiling(remainingBlocks / (decimal)NfcUtils.ReadMultipleBlock_HalfNumberOfBlocks);
            int latestBlocksToRead = remainingBlocks % (NfcUtils.ReadMultipleBlock_HalfNumberOfBlocks);

            for (int i = 0; i < count - 1; i++)
            {
                temp = SendReadMultipleBlockCommand(newStartAddress + i * NfcUtils.ReadMultipleBlock_HalfNumberOfBlocks,
                    NfcUtils.ReadMultipleBlock_HalfNumberOfBlocks);
                blocks.Add(keysCount++, temp);
            }

            temp = SendReadMultipleBlockCommand(newStartAddress + (count - 1) * NfcUtils.ReadMultipleBlock_HalfNumberOfBlocks,
                latestBlocksToRead == 0 ? NfcUtils.ReadMultipleBlock_HalfNumberOfBlocks : latestBlocksToRead);
            blocks.Add(keysCount++, temp);

            return blocks;
        }

        private Dictionary<short, byte[]> ReadMultipleBlocksWithSingleBlockCommand(int startAddress, int totalBlocks)
        {
            Dictionary<short, byte[]> blocks = new Dictionary<short, byte[]>();

            for (int i = 0; i < totalBlocks; i++)
            {
                byte[] temp = SendReadSingleBlockCommand(startAddress + i);
                blocks.Add((short)(startAddress + i), temp);
            }
            return blocks;
        }

        private void WriteMultipleBlocks(int startAddress, ByteArray data)
        {
            int startBlockAddress = NfcUtils.GetDataBlockAddress(startAddress);
            int endBlockAddress = NfcUtils.GetDataBlockAddress(startAddress + data.Count);
            int totalBlocksToWrite = NfcUtils.GetTotalBlocks(data.Count, startAddress);
            int positionInStartBlock = startAddress % NfcUtils.BytesInBlock;
            int positionInEndBlock = (startAddress + data.Count) % NfcUtils.BytesInBlock;

            ByteArray toWrite = new ByteArray(totalBlocksToWrite);

            //check if start address byte is inside block
            if (positionInStartBlock > 0)
            {
                //read the block
                var readbytes = SendReadSingleBlockCommand(startBlockAddress);
                //add first filling bytes
                toWrite.AddRange(readbytes.Take(positionInStartBlock));
            }

            //add ByteArray data to write
            toWrite.AddRange(data);

            //check if end address byte is inside block
            if (positionInEndBlock > 0)
            {
                //read the block
                var readbytes = SendReadSingleBlockCommand(endBlockAddress);
                //add last filling bytes
                toWrite.AddRange(readbytes.Skip(positionInEndBlock));
            }

            //send RF password to unlock ot fit drivers
            if (IsOTFit())
            {
                //if you use BLE on ot fit, you need to set RF timeout for writing
                if (_nfcCommunication.ReaderType == NfcReaderType.Bluetooth)
                {
                    //set RF timeout
                    RFTimeoutCommand(0x02);
                }

                if (!TryPresentSectorPasswordCommand(new byte[] { 0x02, 0x01, 0x00, 0x03 }))
                {
                    FailPresentSectorPasswordCommand();
                }
            }     

            //write the prepared array 
            for (int blockCount = 0; blockCount < totalBlocksToWrite; blockCount++)
            {
                byte[] block = toWrite.Mid(blockCount * NfcUtils.BytesInBlock, NfcUtils.BytesInBlock).ToArray();
                if(!IsOTFit())
                {
                    SendWriteSingleBlockCommand(startBlockAddress + blockCount, block);
                }
                else
                {
                    try
                    {
                        SendWriteSingleBlockCommand(startBlockAddress + blockCount, block);
                    }
                    catch (Exception e)
                    {
                        //retry with a differet password index
                        if (!TryPresentSectorPasswordCommand(new byte[] { 0x00, 0x01, 0x02, 0x03 }))
                        {
                            FailPresentSectorPasswordCommand();
                        }
                    }                  
                }                                
            }

            //if you use BLE on ot fit, you need to set RF timeout only for writing, after you have to be reset it
            if (IsOTFit() && _nfcCommunication.ReaderType == NfcReaderType.Bluetooth)
            {
                //reset RF timeout
                RFTimeoutCommand(0x00);
            }
        }

        public bool IsOTFit()
        {
            //TODO improve way to detect if a device is an OT Fit
            IConfiguration configuration = _configurationService.GetCurrentConfiguration();
            return configuration.LuminaireName.StartsWith("OT FIT");
        }

        public bool TryPresentSectorPasswordCommand(byte[] passwordToTry)
        {
            //present password to unlock OT-Fit drivers
            byte[] nfcTagSerial = RetrieveNFCTagSerial();
            byte[] passwordToWrite = GeneratePassword(nfcTagSerial);
      
            for(int i=0; i < passwordToTry.Length; i++)
            {
                byte response = PresentSectorPasswordCommand(nfcTagSerial, passwordToWrite, passwordToTry[i]);
                if (response == 0x00)
                {
                    return true;
                }
            }

            return false;
        }

        public void FailPresentSectorPasswordCommand()
        {
            if (_nfcCommunication.ReaderType == NfcReaderType.Bluetooth)
            {
                //reset RF timeout
                RFTimeoutCommand(0x00);
            }

            throw new RFPasswordFailedException("PresentSectorPasswordCommand failed", ErrorCodes.RFPasswordFailed);
        }

        private byte[] SendReadSingleBlockCommand(int blockAddress)
        {
            ByteArray command = ISO15693_Utils.CreateReadSingleBlockCommand(blockAddress);

            ByteArray response;

#if DEBUG
            DateTime startReading = DateTime.Now;
#endif
            response = _nfcCommunication.SendCommand(command);
#if DEBUG
            DateTime endReading = DateTime.Now;
            Logger.Trace("Reading time: " + endReading.Subtract(startReading).TotalMilliseconds);
#endif

            byte[] bytes = response.Skip(1).ToArray();
            return bytes;
        }

        /// <summary>
        /// Send the Get System Info command and return the IC reference.
        /// </summary>
        /// <returns></returns>
        private int SendGetSystemInfoCommand()
        {
            ByteArray command = ISO15693_Utils.CreateGetSystemInfoCommand(false);

            ByteArray response = _nfcCommunication.SendCommand(command);
            return response[response.Count - 1];
        }

        private byte[] SendReadMultipleBlockCommand(int blockAddress, int numberOfBlocks)
        {
            ByteArray command = ISO15693_Utils.CreateReadMultipleBlockCommand(blockAddress, numberOfBlocks);

            ByteArray response;

#if DEBUG
            DateTime startReading = DateTime.Now;
#endif
            response = _nfcCommunication.SendCommand(command);
#if DEBUG
            DateTime endReading = DateTime.Now;
            Logger.Trace("Reading time: " + endReading.Subtract(startReading).TotalMilliseconds);
#endif

            byte[] bytes = response.Skip(1).ToArray();
            return bytes;
        }

        private byte[] RetrieveNFCTagSerial()
        {
            //retrieve nfcTagSerial
            string[] nfcTagSerialArray = NfcTagSerial.Split('-');
            byte[] nfcTagSerial = new byte[nfcTagSerialArray.Length];

            for (int i = 0; i < nfcTagSerialArray.Length; i++)
            {
                nfcTagSerial[i] = Convert.ToByte(nfcTagSerialArray[i], 16);
            }

            //if use bluetooth NFC need it to reverse array
            if (_nfcCommunication.ReaderType == NfcReaderType.Bluetooth)
            {
                Array.Reverse(nfcTagSerial);
            }

            return nfcTagSerial;
        }

        public byte[] GeneratePassword(byte[] data)
        {
            byte[] password = new byte[data.Length];            
            Buffer.BlockCopy(data, 0, password, 0, data.Length);
            Array.Reverse(password);
            byte[] hash = _cryptographyService.ComputeMD5Hash(PrepareData(password));
            return hash.Skip(0).Take(4).ToArray();
        }

        private byte[] PrepareData(byte[] data)
        {
            byte[] result = new byte[data.Length + Version.Length];
            Buffer.BlockCopy(data, 0, result, 0, data.Length);
            Buffer.BlockCopy(Version, 0, result, data.Length, Version.Length);
            return result;
        }

        private byte PresentSectorPasswordCommand(byte[] nfcTagSerial, byte[] passwordToWrite, byte passwordIndex)
        {
            try
            {
                ByteArray command = ISO15693_Utils.PresentSectorPasswordCommand(nfcTagSerial, passwordToWrite, passwordIndex);

                ByteArray response = _nfcCommunication.SendCommand(command);

                return response[0];
            }
            catch(Exception e)
            {
                Logger.Trace($"NfcReader.PresentSectorPasswordCommand Error={e.Message:X}");
                return 0x0F;
            }            
        }

        private byte RFTimeoutCommand(byte timeout)
        {
            ByteArray command = ISO15693_Utils.RFTimeoutCommand(timeout);

            ByteArray response = _nfcCommunication.SendCommand(command);

            return response[0];
        }

        private byte SendWriteSingleBlockCommand(int dataBlockIndex, byte[] dataToWrite)
        {
            ByteArray command = ISO15693_Utils.CreateWriteSingleBlockCommand(dataBlockIndex, dataToWrite);

            //#if DEBUG
            //            DateTime startWriting = DateTime.Now;
            //#endif
            ByteArray response = _nfcCommunication.SendCommand(command);

            //#if DEBUG
            //            DateTime endWriting = DateTime.Now;
            //            System.Console.WriteLine("Writing time: " + endWriting.Subtract(startWriting).TotalMilliseconds);
            //#endif
            return response[0];
        }

        //TODO remove test function
        private void TestReadingWriting()
        {
            //TEST------------------------------------------------------------------------------------------------
            ByteArray oneBlock = new ByteArray(1, 4);
            DateTime startReading = DateTime.Now;
            ByteArray ecg = Read(0, 4);
            DateTime endReading = DateTime.Now;
            Write(0, oneBlock);
            DateTime endWriting = DateTime.Now;

            Logger.Trace("Reading time: " + endReading.Subtract(startReading).TotalMilliseconds);
            Logger.Trace("Writing time: " + endWriting.Subtract(endReading).TotalMilliseconds);
            //----------------------------------------------------------------------------------------------------
        }
    }
}
