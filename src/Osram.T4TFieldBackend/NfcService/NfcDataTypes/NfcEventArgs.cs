﻿using System;

namespace Osram.TFTBackend.NfcService.NfcDataTypes
{
    public class NfcEventArgs : EventArgs
    {
        private NfcEventType _eventType;
        public NfcEventType EventType
        {
            get { return _eventType; }
            set { _eventType = value; }
        }

        public NfcEventArgs() : base()
        {

        }

        public NfcEventArgs(NfcEventType eventType) : base()
        {
            _eventType = eventType;
        }
    }

    public enum NfcEventType
    {
        EcgNotFound,
        DeviceNotFound
    }
}
