﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Osram.TFTBackend.NfcService.NfcDataTypes
{
    public class ByteArray : IEnumerable<byte>
    {
        private List<byte> _data;

        public int Count => _data.Count;

        public ByteArray()
        {
            _data = new List<byte>();
        }

        public ByteArray(int capacity)
        {
            _data = new List<byte>(capacity);
        }

        public ByteArray(IEnumerable<byte> data)
        {
            _data = new List<byte>(data);
        }

        /// <summary>
        /// Initianlizes the byte array of the given size with the given value
        /// </summary>
        /// <param name="value">Initial byte value</param>
        /// <param name="count">size of the resulting array</param>
        public ByteArray(byte value, int count)
        {
            _data = new List<byte>(count);
            for (int i = 0; i < count; i++)
            {
                _data.Add(value);
            }
        }

        private ByteArray(List<byte> data)
        {
            _data = data;
        }

        public byte this[int index]
        {
            get { return _data[index]; }
            set { _data[index] = value; }
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        public IEnumerator<byte> GetEnumerator()
        {
            return _data.GetEnumerator();
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            IEnumerable<byte> second = obj as IEnumerable<byte>;
            if (second == null)
                return false;
            return _data.SequenceEqual(second);
        }

        public static bool operator ==(ByteArray arr1, ByteArray arr2)
        {
            if (ReferenceEquals(arr1, null))
            {
                return ReferenceEquals(arr2, null);
            }
            return arr1.Equals(arr2);
        }

        public static bool operator !=(ByteArray arr1, ByteArray arr2)
        {
            return !(arr1 == arr2);
        }

        public override int GetHashCode()
        {
            int hash = 19;
            for (int i = 0; i < _data.Count; i++)
                hash = hash * 31 + _data[i];
            return hash;
        }

        public void Add(byte data)
        {
            _data.Add(data);
        }

        public void AddUshort(ushort data)
        {
            _data.Add((byte)((data >> 8) & 0xFF));
            _data.Add((byte)(data & 0xFF));
        }

        public void AddUInt32(uint data)
        {
            _data.Add((byte)((data >> 24) & 0xFF));
            _data.Add((byte)((data >> 16) & 0xFF));
            _data.Add((byte)((data >> 8) & 0xFF));
            _data.Add((byte)(data & 0xFF));
        }

        public void AddRange(IEnumerable<byte> data)
        {
            _data.AddRange(data);
        }

        public void Clear()
        {
            _data.Clear();
        }

        public ByteArray Left(int count)
        {
            return new ByteArray(_data.GetRange(0, count));
        }

        public ByteArray Right(int count)
        {
            return new ByteArray(_data.GetRange(_data.Count - count, count));
        }

        public ByteArray Mid(int startAddress, int count)
        {
            if (startAddress > Count)
                return new ByteArray();
            if ((startAddress == 0) && (count == Count))
                return this;
            count = Math.Min(count, Count - startAddress);
            return new ByteArray(_data.GetRange(startAddress, count));
        }

        /// <summary>
        /// Returns 2 byte array values as ushort (big enndian).
        /// </summary>
        /// <param name="index"></param>
        /// <returns></returns>
        public ushort GetUshort(int index)
        {
            return (ushort)(_data[index + 1] + (_data[index] << 8));
        }

        public uint GetUInt32(int index)
        {
            return (uint)(_data[index + 3] + (_data[index + 2] << 8) + (_data[index + 1] << 16) + (_data[index] << 24));
        }

        public void SetUshort(int index, ushort data)
        {
            _data[index] = (byte)((data >> 8) & 0xFF);
            _data[index+1] = (byte)(data & 0xFF);
        }

        public void Copy(int sourceStartIndex, ByteArray destArray)
        {
            for (int i = 0; i < destArray.Count; i++)
            {
                _data[sourceStartIndex + i] = destArray._data[i];
            }
        }

        public byte[] ToArray()
        {
            return _data.ToArray();
        }

        public List<byte> ToList()
        {
            return _data;
        }

        public override string ToString()
        {
            return BitConverter.ToString(_data.ToArray()).Replace("-", " ");
        }

        public string ToString(string separator)
        {
            return BitConverter.ToString(_data.ToArray()).Replace("-", separator);
        }

        public string FormatToString(int bytesPerLine, bool includeAddress, int startAddress)
        {
            int lines = Count / bytesPerLine + (((Count % bytesPerLine) > 0) ? 1 : 0);
            StringBuilder sb = new StringBuilder(Count * 3 + lines * 5 + 50);
            int lineAddress = 0;
            for (int i = 0; i < lines; i++)
            {
                ByteArray lineData = Mid(lineAddress, bytesPerLine);
                if (includeAddress)
                {
                    sb.Append($"{lineAddress+startAddress:X4}  ");
                }
                if (i < lines - 1)
                    sb.AppendLine(lineData.ToString());
                else
                    sb.Append(lineData.ToString());
                lineAddress += bytesPerLine;
            }
            return sb.ToString();
        }

        public static ByteArray FromString(string hexString)
        {
            string[] hexBytes = hexString.Split(new char[] {' '}, StringSplitOptions.RemoveEmptyEntries);
            ByteArray data = new ByteArray(hexBytes.Length);
            foreach (var hexByte in hexBytes)
            {
                data.Add(GetByteFromHexString(hexByte));
            }
            return data;
        }

        private static byte GetByteFromHexString(string hexByte)
        {
            try
            {
                return Convert.ToByte(hexByte, 16);
            }
            catch (Exception)
            {
                return 0;
            }
        }
    }
}
