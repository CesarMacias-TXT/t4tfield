﻿namespace Osram.TFTBackend.NfcService.NfcDataTypes
{
    public enum NfcReaderType
    {
        Mobile,
        Bluetooth
    }
}
