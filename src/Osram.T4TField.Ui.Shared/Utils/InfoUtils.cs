﻿#if __IOS__
using Foundation;
#else
using Android.App;
#endif
using Osram.T4TField.Contracts;
using Osram.TFTBackend.NfcService.NfcDataTypes;

namespace T4TField.Ui.Shared.Utils
{
    class InfoUtils : IInfoUtils
    {
        public string GetAppVersion()
        {
#if __IOS__
            return NSBundle.MainBundle.InfoDictionary["CFBundleVersion"].ToString();
#else
            return Android.App.Application.Context.PackageManager.GetPackageInfo(global::Android.App.Application.Context.PackageName, 0).VersionName.ToString();
#endif
        }

        public string GetStoreName()
        {
#if __IOS__
            return "iTunes";
#else
            return "Google Play";
#endif
        }

        public string GetLinkToStore()
        {
            // https://stackoverflow.com/questions/37369196/xamarin-cross-platform-application-package-name-in-c-sharp

#if __IOS__
            return "http://itunes.apple.com/lookup?bundleId=" + NSBundle.MainBundle.InfoDictionary["CFBundleIdentifier"].ToString();
#else
            return "http://play.google.com/store/apps/details?id=" + Application.Context.PackageName;
#endif
        }

        public bool ExistExternalStorage()
        {
#if __IOS__
            return false;
#else
            return !Android.OS.Environment.IsExternalStorageEmulated;
#endif
        }

        public string GetDownloadFolderPath()
        {
#if __IOS__
            return NSFileManager.DefaultManager.GetUrls(NSSearchPathDirectory.DocumentDirectory, NSSearchPathDomain.User)[0].Path;
#else
            return Android.OS.Environment.GetExternalStoragePublicDirectory(Android.OS.Environment.DirectoryDownloads).AbsolutePath;
#endif
        }

        public string GetInAppBillingProductName(string name)
        {
#if __IOS__
            return name;
#else
            return name?.Replace(" (Tuner4TRONIC Field: NFC programming of LED drivers)", "");
#endif
        }

        public NfcReaderType GetDefaultNfcReaderType()
        {
#if __IOS__
            return NfcReaderType.Bluetooth;
#else
            return NfcReaderType.Mobile;
#endif
        }
    }
}
