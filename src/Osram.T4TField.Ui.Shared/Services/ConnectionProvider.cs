﻿using System;
using System.IO;
using SQLite;
using Osram.TFTBackend.DatabaseService.Contracts;

namespace Osram.T4TField.Shared.Services
{
    public class ConnectionProvider : IConnectionProvider
    {
        private const string DatabaseFileName = "t4t.db";

        public string DbPath => Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.Personal), DatabaseFileName);

        public string DbDirectory => Environment.GetFolderPath(Environment.SpecialFolder.Personal);

        public ConnectionProvider()
        {
            InitConnection();
        }

        public SQLiteAsyncConnection Connection { get; private set; }

        public void CloseConnection()
        {
            if (Connection != null)
            {
                Connection?.GetConnection().Dispose();
                Connection = null;
            }

            SQLiteAsyncConnection.ResetPool();
        }

        public void InitConnection()
        {
            Connection = new SQLiteAsyncConnection(DbPath);
        }
    }
}