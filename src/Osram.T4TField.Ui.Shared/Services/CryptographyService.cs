﻿using System;
using System.IO;
using System.Security.Cryptography;
using System.Text;
using Osram.TFTBackend.CryptographyService.Contracts;

namespace Osram.T4TField.Ui.Shared.Services
{
    public class CryptographyService : ICryptographyService
    {
        private readonly MD5 _md5CryptographyService;
        private readonly SHA1 _sha1Service;

        /// <summary>
        /// Global Password for all OSRAM RND C# encryptions
        /// </summary>
        //public const string Password = "Osram.R&D.42";
        public const string Password = "Hello World";

        /// <summary>
        /// crypto key
        /// </summary>
        private byte[] _cryptoKey;

        /// <summary>
        /// crypto initialization vector
        /// </summary>
        private byte[] _cryptoInitVector;

        public Stream DecryptedXmlStream { get; set; }

        public CryptographyService()
        {
            _md5CryptographyService = MD5.Create();
            _sha1Service = SHA1.Create();


            byte[] salt = Encoding.ASCII.GetBytes(Password.Length.ToString());

            PasswordDeriveBytes secretKey = new PasswordDeriveBytes(Password, salt);

            _cryptoKey = secretKey.GetBytes(32);
            _cryptoInitVector = secretKey.GetBytes(16);
        }

        /// <summary>
        /// TODO: is password parameter here really needed? It is not used in T4T-PC !
        /// </summary>
        /// <param name="password"></param>
        public CryptographyService(string password)
        {
            _md5CryptographyService = MD5.Create();
            _sha1Service = SHA1.Create();


            byte[] salt = Encoding.ASCII.GetBytes(password.Length.ToString());

            PasswordDeriveBytes secretKey = new PasswordDeriveBytes(password, salt);

            _cryptoKey = secretKey.GetBytes(32);
            _cryptoInitVector = secretKey.GetBytes(16);
        }

        private string GetContentOfStream(Stream memoryStream)
        {
            memoryStream.Position = 0;
            var sr = new StreamReader(memoryStream);
            return sr.ReadToEnd();
        }

        public Stream Decrypt(Stream streamToDecrypt, string decryptionPassword = "")
        {
            if (streamToDecrypt == null)
            {
                throw new ArgumentNullException($"Stream to decrypt has no content.");
            }

            var stringToDecrypt = GetContentOfStream(streamToDecrypt);
            var anotherArrayToDecrypt = Convert.FromBase64String(stringToDecrypt);

            DecryptedXmlStream = DecryptStringFromBytes(anotherArrayToDecrypt);

            CheckAndCorrectXmlEncoding(DecryptedXmlStream);

            return DecryptedXmlStream;
        }

        public byte[] Decrypt(byte[] encryptedData, string encryptionPassword)
        {
            throw new NotImplementedException();
        }

        public Stream Encrypt(Stream streamToEncrypt)
        {
            throw new NotImplementedException();
        }

        public byte[] Encrypt(byte[] dataToEncrypt, string encryptionPassword)
        {
            throw new NotImplementedException();
        }

        public string ComputeMD5(string input)
        {
            var hash = _md5CryptographyService.ComputeHash(Encoding.Default.GetBytes(input));
            return BitConverter.ToString(hash).Replace("-", string.Empty);
        }

        public byte[] ComputeMD5Hash(byte[] input)
        {
            var hash = _md5CryptographyService.ComputeHash(input);
            return hash;
        }

        public String ComputeMD5(Stream inputStream)
        {
            var hash = _md5CryptographyService.ComputeHash(inputStream);
            return BitConverter.ToString(hash).Replace("-", string.Empty);
        }

        public byte[] ComputeSha1(byte[] input)
        {
            return _sha1Service.ComputeHash(input);
        }

        private void CheckAndCorrectXmlEncoding(Stream xmlStream)
        {
            string xmlAsString = GetContentOfStream(xmlStream);
            xmlAsString = xmlAsString.Replace("<?xml version=\"1.0\" encoding=\"utf-16\"?>", "<?xml version=\"1.0\" encoding=\"utf-8\"?>");
        }
        private Stream DecryptStringFromBytes(byte[] cipherText)
        {
            // Check arguments.
            if (cipherText == null || cipherText.Length <= 0)
            {
                throw new ArgumentNullException("cipherText");
            }

            var decryptedInMemoryStream = new MemoryStream();

            try
            {
                using (Rijndael rijAlg = Rijndael.Create())
                {
                    rijAlg.Key = _cryptoKey;
                    rijAlg.IV = _cryptoInitVector;

                    // Create a decrytor to perform the stream transform.
                    ICryptoTransform decryptor = rijAlg.CreateDecryptor(rijAlg.Key, rijAlg.IV);

                    // Create the streams used for decryption.
                    using (MemoryStream memoryStreamToDecrypt = new MemoryStream(cipherText))
                    {
                        using (CryptoStream cryptoStreamDecrypt =
                            new CryptoStream(memoryStreamToDecrypt, decryptor, CryptoStreamMode.Read))
                        {
                            cryptoStreamDecrypt.CopyTo(decryptedInMemoryStream);
                        }
                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                //throw;
            }

            return decryptedInMemoryStream;
        }

        /// <summary>
        /// Encrypts a given string and converts the result to byte array
        /// </summary>
        /// <param name="plainText">text that should be encrypted </param>        
        /// <returns>byte array containing encrypted data</returns>
        private byte[] EncryptStringToBytes(string plainText)
        {
            // Check arguments.
            if (plainText == null || plainText.Length <= 0)
            {
                throw new ArgumentNullException("plainText");
            }

            byte[] encrypted;

            // Create an Rijndael object
            // with the specified key and initVector.
            using (Rijndael rijAlg = Rijndael.Create())
            {
                rijAlg.Key = _cryptoKey;
                rijAlg.IV = _cryptoInitVector;

                // Create a decrytor to perform the stream transform.
                ICryptoTransform encryptor = rijAlg.CreateEncryptor(rijAlg.Key, rijAlg.IV);

                // Create the streams used for encryption.
                using (MemoryStream memoryStreamEncrypt = new MemoryStream())
                {
                    using (CryptoStream cryptoStreamEncrypt = new CryptoStream(memoryStreamEncrypt, encryptor, CryptoStreamMode.Write))
                    {
                        using (StreamWriter streamWriterEncrypt = new StreamWriter(cryptoStreamEncrypt))
                        {
                            streamWriterEncrypt.Write(plainText);
                        }

                        encrypted = memoryStreamEncrypt.ToArray();
                    }
                }
            }

            // Return the encrypted bytes from the memory stream.
            return encrypted;
        }
    }
}
