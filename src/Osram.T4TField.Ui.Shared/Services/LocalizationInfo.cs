﻿using System.Globalization;
using System.Threading;

#if __IOS__

#else
using Android.App;
#endif

using Osram.T4TField.Contracts;

namespace Osram.T4TField.Shared.Services
{
    public class LocalizationInfo : ILocalizationInfo
    {
        public CultureInfo GetCurrentCultureInfo()
        {
#if __IOS__
            return CultureInfo.CurrentCulture;
#else
            var androidLocale = Application.Context.Resources.Configuration.Locale; // Java.Util.Locale.Default;
            var netLanguage = androidLocale.ToString().Replace("_", "-");
            try
            {
                return new CultureInfo(netLanguage);
            }
            catch (System.Exception)
            {
                return new CultureInfo("en");
            }
#endif
        }

        public void SetCultureInfo(CultureInfo cultureInfo)
        {
            Thread.CurrentThread.CurrentCulture = cultureInfo;
            Thread.CurrentThread.CurrentUICulture = cultureInfo;
            
#if __IOS__
            CultureInfo.CurrentCulture = cultureInfo;
#else
            var locale = new Java.Util.Locale(cultureInfo.TwoLetterISOLanguageName);

            Java.Util.Locale.Default = locale;

            Application.Context.Resources.Configuration.SetLocale(locale);
            var configuration = Application.Context.Resources.Configuration;
            configuration.SetLocale(locale);
            configuration.SetLayoutDirection(locale);
            Application.Context.CreateConfigurationContext(configuration);
#endif
        }
    }
}
