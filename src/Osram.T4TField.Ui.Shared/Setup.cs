using System;
using System.Threading.Tasks;
using Acr.UserDialogs;
using MvvmCross;
using MvvmCross.Plugin;
using Plugin.Settings;
using Osram.T4TField.Contracts;
using Osram.T4TField.Services;
using Osram.T4TField.Ui.Shared.Services;
using Osram.TFTBackend;
using Osram.TFTBackend.CryptographyService.Contracts;
using Osram.TFTBackend.DatabaseService.Contracts;
using T4TField.Ui.Shared.Utils;
using Osram.T4TField.Shared.Services;
using MvvmCross.ViewModels;
using MvvmCross.IoC;

#if __ANDROID__
using Osram.T4TField.Droid.Services;

namespace Osram.T4TField.Droid
#else
using Osram.T4TField.iOS.Services;

namespace Osram.T4TField.iOS
#endif
{
    public partial class Setup : IMvxPlugin
    {
        static Setup()
        {
            AppDomain.CurrentDomain.UnhandledException += OnUnhandledException;
            TaskScheduler.UnobservedTaskException += OnUnobservedTaskException;
        }

        protected override IMvxApplication CreateApp()
        {
            return new OsramMvxApp();
        }
        
        protected override IMvxIoCProvider InitializeIoC()
        {
            MvxIoCProvider.Initialize();

            //Mvx.LazyConstructAndRegisterSingleton<IMvxFormsPageLoader, ViewLoader>();

            // plugins services
            Mvx.IoCProvider.LazyConstructAndRegisterSingleton(() => UserDialogs.Instance);
            Mvx.IoCProvider.LazyConstructAndRegisterSingleton(() => CrossSettings.Current);
            
            // common services
            Mvx.IoCProvider.LazyConstructAndRegisterSingleton<IPlatformSpecificCalls, PlatformSpecificCalls>();
            Mvx.IoCProvider.LazyConstructAndRegisterSingleton<ILocalNotificationService, LocalNotificationService>();

            // shared services
            Mvx.IoCProvider.LazyConstructAndRegisterSingleton<IInfoUtils, InfoUtils>();
            Mvx.IoCProvider.LazyConstructAndRegisterSingleton<ILocalizationInfo, LocalizationInfo>();
            Mvx.IoCProvider.LazyConstructAndRegisterSingleton<IConnectionProvider, ConnectionProvider>();
            Mvx.IoCProvider.LazyConstructAndRegisterSingleton<ICryptographyService, CryptographyService>();

            InitializePlatformSpecificIoC();
#if !DEBUG
            //Identify user for tracking
            Track.TryToIdentify();
#endif
            return base.InitializeIoC();
        }

        private static void OnUnhandledException(object sender, UnhandledExceptionEventArgs e)
        {
            bool isTerminating = e.IsTerminating;
            var exception = e.ExceptionObject as Exception;
            Logger.Error($"UnhandledException: {exception?.ToString() ?? e.ExceptionObject.ToString()}");
        }

        private static void OnUnobservedTaskException(object sender, UnobservedTaskExceptionEventArgs e)
        {
            bool isObserved = e.Observed;
            var exception = e.Exception;
            Logger.Exception("UnobservedTaskException", exception);
        }

        void IMvxPlugin.Load()
        {
            //TODO check if is really need it
        }
    }
}