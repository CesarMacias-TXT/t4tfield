using Osram.T4TField.Droid.Services;
using Osram.T4TField.Droid.ViewPresenter;
using Osram.T4TField.Contracts;
using Osram.T4TField.ViewPresenter;
using ISystemUtils = Osram.T4TField.Droid.Contracts.ISystemUtils;
using System.Collections.Generic;
using System.Reflection;
using Osram.TFTBackend.RestService.Contracts;
using MvvmCross;
using MvvmCross.IoC;
using MvvmCross.Platforms.Android.Core;
using MvvmCross.Platforms.Android.Presenters;
using Osram.T4TField.ViewModels;
using MvvmCross.ViewModels; 

namespace Osram.T4TField.Droid
{
    public partial class Setup : MvxAndroidSetup
    {
        public Setup()
            : base()
        {

        }

        protected override IMvxAndroidViewPresenter CreateViewPresenter()
        {
            var presenter = new OsramAndroidMvxFromsViewPresenter();
            Mvx.IoCProvider.RegisterSingleton<MasterDetailViewPresenter>(presenter);
            return (IMvxAndroidViewPresenter)presenter;
        }

        public override IEnumerable<Assembly> GetViewAssemblies()
        {
            var list = new List<Assembly>();
            list.AddRange(base.GetViewAssemblies());
            list.Add(typeof(MasterDetailViewPresenter).Assembly);
            return list.ToArray();
        }

        public  override IEnumerable<Assembly> GetViewModelAssemblies()
        {
            var list = new List<Assembly>();
            list.AddRange(base.GetViewModelAssemblies());
            list.Add(typeof(MasterDetailViewModel).Assembly);
            return list.ToArray();
        }

        /*protected override IMvxTrace CreateDebugTrace()
        {
            return new DebugOnlyTrace();
        }*/

        private void InitializePlatformSpecificIoC()
        {
            var androidUtils = new AndroidUtils();
            Mvx.IoCProvider.LazyConstructAndRegisterSingleton<IMvxViewModelLoader, T4TField.ViewPresenter.MvxViewModelLoader>();
            Mvx.IoCProvider.RegisterSingleton<IAndroidUtils>(androidUtils);
            Mvx.IoCProvider.RegisterSingleton<ISystemUtils>(androidUtils);
            Mvx.IoCProvider.LazyConstructAndRegisterSingleton<TabGestureService, TabGestureService>();
            Mvx.IoCProvider.LazyConstructAndRegisterSingleton<IAppInfo>(() => new AppInfo(ApplicationContext));
            Mvx.IoCProvider.LazyConstructAndRegisterSingleton<IT4THttpClient, T4THttpClient>();
        }
    }
}