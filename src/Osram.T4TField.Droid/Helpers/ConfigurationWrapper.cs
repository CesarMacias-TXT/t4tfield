﻿using Android.Content;
using Android.Content.Res;
using Java.Util;

namespace Osram.T4TField.Droid.Helpers
{
    public class ConfigurationWrapper
    {
        //Creates a Context with updated Configuration
        public static Context WrapConfiguration(Context context, Configuration config)
        {
            return context.CreateConfigurationContext(config);
        }

        // Creates a Context with updated Locale
        public static Context WrapLocale(Context context, Locale locale)
        {
            var config = context.Resources.Configuration;
            config.Locale = locale;
            return WrapConfiguration(context, config);
        }
    }
}