﻿using System;
using System.Diagnostics;
using MvvmCross.Platform.Platform;

namespace Osram.T4TField.Droid
{
    public class DebugOnlyTrace : IMvxTrace
    {
        public void Trace(MvxTraceLevel level, string tag, Func<string> message)
        {

            Debug.WriteLine(GetTime() + tag + ":" + level + ": " + message());
        }

        public void Trace(MvxTraceLevel level, string tag, string message)
        {
            Debug.WriteLine(GetTime() + tag + ":" + level + ": " + message);
        }

        public void Trace(MvxTraceLevel level, string tag, string message, params object[] args)
        {
            try
            {
                String msg = string.Format(GetTime() + tag + ":" + level + ": " + message, args);
                Debug.WriteLine(msg);
            }
            catch (FormatException)
            {
                this.Trace(MvxTraceLevel.Error, tag, "Exception during trace of {0} {1} {2}", level, message);
            }
        }

        private string GetTime()
        {
            return DateTime.Now.ToString("MM-dd HH:mm:ss.fff ");
        }
    }
}