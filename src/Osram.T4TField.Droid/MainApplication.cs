using System;

using Android.App;
using Android.OS;
using Android.Runtime;
using Plugin.CurrentActivity;

namespace Osram.T4TField.Droid
{
    //You can specify additional application information in this attribute
#if DEBUG
    [Application(Theme = "@style/OsramTheme", Debuggable = true)]
#else
    [Application(Theme = "@style/OsramTheme", Debuggable = false)]
#endif

    public class MainApplication : Application
    {
        public MainApplication(IntPtr handle, JniHandleOwnership transer) : base(handle, transer)
        {
        }

        public override void OnCreate()
        {
            base.OnCreate();
            CrossCurrentActivity.Current.Init(this);
            //A great place to initialize Xamarin.Insights and Dependency Services!
        }

        //public override void OnTerminate()
        //{
        //    base.OnTerminate();
        //}
    }
}