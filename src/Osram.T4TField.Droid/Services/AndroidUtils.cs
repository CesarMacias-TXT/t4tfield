﻿using System;
using Android.OS;
using Android.Views;
using MvvmCross;
using Osram.T4TField.Contracts;
using ISystemUtils = Osram.T4TField.Droid.Contracts.ISystemUtils;

namespace Osram.T4TField.Droid.Services
{
    public class AndroidUtils : IAndroidUtils, ISystemUtils
    {
        public void CloseApp()
        {
            Process.KillProcess(Process.MyPid());
        }

        public void PreventDeviceSleep(bool shouldPrevent)
        {
            try
            {
                if (shouldPrevent)
                {
                    Mvx.IoCProvider.Resolve<MainActivity>().Window.AddFlags(WindowManagerFlags.KeepScreenOn);
                }
                else
                {
                    Mvx.IoCProvider.Resolve<MainActivity>().Window.ClearFlags(WindowManagerFlags.KeepScreenOn);
                }
            }
            catch (Exception ex)
            {
                ex.TrackWarning(GetType().Name, "PreventDeviceSleep");
            }
        }
    }
}