﻿//----------------------------------------------------------------------------------------------
// <copyright file="NFCUnit.cs" company="Osram GmbH">
// COPYRIGHT © OSRAM GmbH. ALL RIGHTS RESERVED.
// THIS SOURCE CODE IS CONFIDENTIAL AND IS NOT INTENDED FOR PUBLIC RELEASE OR
// ANY OTHER FORM OF DISCLOSURE.
// </copyright>
//-------------------------------------------------------------------------------------------------
// <sumamry>
// This class represent a NFC Device. This class open the USB Connection using PID and VID of the NFC.
// On Successfull connection open NFC Unit will send commands to query the power status, version, name etc.,
// This class maintains the state of NFC. Also hold the Luminaire Device Instance which is solely responsible for Luminaire/ECG Communication.
// This class acts and interface to send command to ECG that support NFC Interface.
// </summary>
//-------------------------------------------------------------------------------------------------

using Android.Nfc;
using Android.Nfc.Tech;
using Osram.TFTBackend;
using Osram.TFTBackend.NfcService.Contracts;
using Osram.TFTBackend.NfcService.Exceptions;
using Osram.TFTBackend.NfcService.ISOStandard;
using Osram.TFTBackend.NfcService.NfcDataTypes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Timers;

namespace Osram.T4TField.Droid.Services.Nfc
{
    /// <summary>
    /// This class represent a smartphone NFC Reader Device. 
    /// </summary>
    public class MobileNfcReader : IMobileNfcReader, INfcCommunication
    {
        private const string Nfc_V = "android.nfc.tech.NfcV";
        private NfcAdapter nfcAdapter;
        private Tag droidTag;
        private Timer ReaderTimer = null;

        public MobileNfcReader(NfcAdapter adapter)
        {
            this.nfcAdapter = adapter;
        }

        public event EventHandler<EventArgs> AdapterConnected;

        public event EventHandler<EventArgs> AdapterDisconnected;

        public event EventHandler<string> TagDetected;

        public event EventHandler<EventArgs> TagUndetected;

        /// <summary>
        /// true if Nfc is available on the mobile device
        /// otherwise return false
        /// </summary>
        public bool IsAvailable => nfcAdapter != null;

        /// <summary>
        /// true if Nfc is enabled on the mobile device
        /// otherwise return false
        /// </summary>
        public bool IsEnabled {
            get {
                if (IsAvailable)
                    return nfcAdapter.IsEnabled;
                else
                    return false;
            }
        }

        public int RetryCount { get; set; }

        public NfcReaderType ReaderType { get; } = NfcReaderType.Mobile;

        public void OnNewIntent(object sender, Tag tagFromIntent)
        {
            droidTag = tagFromIntent;

            List<string> techList = droidTag.GetTechList().ToList();
            string tagId = BitConverter.ToString(droidTag.GetId());

            #region NfcV
            if (techList.Contains(Nfc_V))
            {
                NfcV tag;
                if ((tag = GetNfcV(droidTag)) == null)
                {
                    System.Console.WriteLine("Tag type is not NfcV");
                }
                else
                {
                    TagDetected?.Invoke(this, tagId);
                    StartTimerForCheckTagInProximity();
                }
            }
            #endregion

        }


        private void StartTimerForCheckTagInProximity()
        {
            if (ReaderTimer != null)
            {
                ReaderTimer.Stop();
                ReaderTimer.Dispose();
            }

            ReaderTimer = new Timer(3000);
            ReaderTimer.Elapsed += TimerElapsed;
            ReaderTimer.Start();
        }



        /// <summary>
        /// check if the tag is in proximity  
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void TimerElapsed(object sender, ElapsedEventArgs e)
        {
            if (!Connect())
            {
                // could not reconnect
                // implies tag is not in proximity                 
                ReaderTimer.Stop();
                ReaderTimer.Dispose();
                TagUndetected?.Invoke(this, null);
            }
        }

        internal void RaiseReaderDisconnected()
        {
            AdapterDisconnected?.Invoke(this, null);
        }

        internal void RaiseReaderConnected()
        {
            AdapterConnected?.Invoke(this, null);
        }

        private NfcV GetNfcV(Tag tag)
        {
            NfcV nfcv = NfcV.Get(tag);
            if (nfcv == null)
            {
                return null;
            }
            else
            {
                return nfcv;
            }
        }

        /// <summary>
        /// try to connect to tag
        /// </summary>
        /// <returns>true if connect succeds, otherwise false</returns>
        public bool ConnectToTag()
        {
            if (droidTag == null)
            {
                return false;
            }

            NfcV tag = GetNfcV(droidTag);

            if (tag == null)
            {
                return false;
            }

            if (!tag.IsConnected)
            {
                try
                {
                    tag.Close();
                    tag.Connect();
                }
                catch
                {
                    // could not reconnect
                    // implies tag is not in proximity               
                    return false;
                }
            }

            return true;
        }

        public ByteArray SendCommand(ByteArray command)
        {
            NfcV nfcV = GetNfcV(this.droidTag);
            if (nfcV == null)
                throw new NfcReaderException("No tag");

            byte[] response;
            int count = RetryCount;
            Exception lastException = null;
            do
            {
                count--;
                try
                {
                    //ATTENTION! you must call Connect before Transceive
                    //java.lang.IllegalStateException: Call connect() first!
                    //at android.nfc.tech.BasicTagTechnology.checkConnected(BasicTagTechnology.java:52)
                    //at android.nfc.tech.BasicTagTechnology.transceive(BasicTagTechnology.java:143)
                    //at android.nfc.tech.NfcV.transceive(NfcV.java:115)
                    //nfcV.Connect();

                    Logger.Trace($"SendCommand Cmd = {command.ToString()}");
                    response = nfcV.Transceive(command.ToArray());

                    ByteArray resp = new ByteArray(response);
#if DEBUG
                    Logger.Trace($"SendCommand response = {resp.ToString()}");
#endif
                    //nfcV.Close();
                    if ((resp[0] & 0x01) != 0)
                    {
                        Logger.Error($"MobileNfcReader SendCommand nfcV.Transceive failed with an error {response[0]:X}", command);
                        continue;
                    }
                    if ((command[1] == (byte)ISO15693_Utils.Command.Reading) && (response.Length != 5))
                    {
                        Logger.Error("MobileNfcReader SendCommand nfcV.Transceive invalid response len");
                        continue;
                    }
                    if (command[1] == (byte)ISO15693_Utils.Command.Reading)
                        Logger.Trace($"SendCommand Data[0]={response[1]:X} Data[1]={response[2]:X} Data[2]={response[3]:X} Data[3]={response[4]:X} ");
                    return resp;
                }
                catch (Exception e)
                {
                    Logger.Exception("SendCommand", e);
                    lastException = e;
                    //nfcV.Close();
                }
            } while (count > 0);

            throw new NfcReaderException("Transceive fail", lastException);
        }

        public bool Connect()
        {
            return ConnectToTag();
        }

        public void Disconnect()
        {
            //do nothing
        }

        public Task DetectTag()
        {
            //passive detection from OS Android
            //do nothing, wait for detection 
            return Task.FromResult(true);
        }
    }

}
