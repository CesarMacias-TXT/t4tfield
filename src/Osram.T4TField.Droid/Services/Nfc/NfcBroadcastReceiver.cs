using Android.App;
using Android.Content;
using Android.Nfc;
using Osram.TFTBackend;

namespace Osram.T4TField.Droid.Services.Nfc
{
    [IntentFilter(new[] { NfcAdapter.ActionAdapterStateChanged})]
    public class NfcBroadcastReceiver : BroadcastReceiver
    {
        /// <summary>
        /// This is called when Android OS detects a tag near the device 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="tagFromIntent"></param>
        public override void OnReceive(Context context, Intent intent)
        {
            if (intent.Action.Equals(NfcAdapter.ActionAdapterStateChanged))
            {
                int state = intent.GetIntExtra(NfcAdapter.ExtraAdapterState,
                                                     NfcAdapter.StateOff);
                Logger.Trace($"NfcBroadcastReceiver adapter state change: state = {state}");
                switch (state)
                {
                    case NfcAdapter.StateOff:
                        NfcDisconnectEvent();
                        break;
                    case NfcAdapter.StateTurningOff:
                        break;
                    case NfcAdapter.StateOn:
                        NfcConnectEvent();
                        break;
                    case NfcAdapter.StateTurningOn:
                        break;
                }
            }
        }

        public delegate void NfcConnectEventHandler();
        public delegate void NfcDisconnectEventHandler();
        public event NfcConnectEventHandler NfcConnectEvent;
        public event NfcDisconnectEventHandler NfcDisconnectEvent;
    }
}