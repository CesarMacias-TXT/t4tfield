using Android.Content;
using Android.OS;
using Android.Provider;
using System;
using System.IO;

namespace Osram.T4TField.Droid.Services
{
    public class UriExtensions
    {
        /// <summary>
        /// Returns the file extension from an Android Uri.
        /// </summary>
        /// <param name="context"></param>
        /// <param name="uri"></param>
        /// <returns></returns>
        public static string GetExtension(Context context, Android.Net.Uri uri)
        {
            var filePath = GetRealPathFromUri(context, uri);
            return Path.GetExtension(filePath);
        }

        /// <summary>
        /// Returns the file name and extension from an Android Uri.
        /// </summary>
        /// <param name="context"></param>
        /// <param name="uri"></param>
        /// <returns>The file name with extension and without directories.</returns>
        public static string GetFileName(Context context, Android.Net.Uri uri)
        {
            var filePath = GetRealPathFromUri(context, uri);
            return Path.GetFileName(filePath);
        }

        ///<summary>
        /// Get a file path from a Uri. This will get the the path for Storage Access
        /// Framework Documents, as well as the _data field for the MediaStore and
        /// other file-based ContentProviders.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="uri">The Uri to query.</param>
        private static string GetRealPathFromUri(Context context, Android.Net.Uri uri)
        {
            var isKitKat = Build.VERSION.SdkInt >= BuildVersionCodes.Kitkat;

            // DocumentProvider
            if (isKitKat && DocumentsContract.IsDocumentUri(context, uri))
            {
                // ExternalStorageProvider
                if (IsExternalStorageDocument(uri))
                {
                    var docId = DocumentsContract.GetDocumentId(uri);
                    var split = docId.Split(':');
                    var type = split[0];

                    if ("primary".Equals(type, StringComparison.OrdinalIgnoreCase))
                    {
                        return Android.OS.Environment.ExternalStorageDirectory + "/" + split[1];
                    }

                    // TODO handle non-primary volumes
                }
                // DownloadsProvider
                else if (IsDownloadsDocument(uri))
                {

                    var id = DocumentsContract.GetDocumentId(uri);
                    if (id.StartsWith("raw:"))
                    {
                        return id.Replace("raw:", "");
                    }

                    var contentUri = ContentUris.WithAppendedId(Android.Net.Uri.Parse("content://downloads/public_downloads"), Convert.ToInt64(id));

                    return GetDataColumn(context, contentUri, null, null, MediaStore.Files.FileColumns.Data);
                }
                // MediaProvider
                else if (IsMediaDocument(uri))
                {
                    var docId = DocumentsContract.GetDocumentId(uri);
                    var split = docId.Split(':');
                    var type = split[0];

                    Android.Net.Uri contentUri = null;
                    if ("image".Equals(type))
                    {
                        contentUri = MediaStore.Images.Media.ExternalContentUri;
                    }
                    else if ("video".Equals(type))
                    {
                        contentUri = MediaStore.Video.Media.ExternalContentUri;
                    }
                    else if ("audio".Equals(type))
                    {
                        contentUri = MediaStore.Audio.Media.ExternalContentUri;
                    }

                    var selection = "_id=?";
                    var selectionArgs = new string[] {
                        split[1]
                    };

                    return GetDataColumn(context, contentUri, selection, selectionArgs);
                }
            }
            // MediaStore (and general)
            else if ("content".Equals(uri.Scheme, StringComparison.OrdinalIgnoreCase))
            {
                if (IsGoogleDriveDocument(uri))
                {
                    return GetDataColumn(context, uri, null, null, MediaStore.Files.FileColumns.DisplayName);
                }
                if (IsGmailDocument(uri))
                {
                    return GetDataColumn(context, uri, null, null, MediaStore.Files.FileColumns.DisplayName);
                }
                return GetDataColumn(context, uri, null, null);
            }
            // File
            else if ("file".Equals(uri.Scheme, StringComparison.OrdinalIgnoreCase))
            {
                return uri.Path;
            }

            return null;
        }

        /// <summary>
        ///  Get the value of the data column for this Uri. This is useful for
        ///  MediaStore Uris, and other file-based ContentProviders.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="uri">The Uri to query.</param>
        /// <param name="selection">(Optional) Filter used in the query.</param>
        /// <param name="selectionArgs">(Optional) Selection arguments used in the query.</param>
        /// <param name="column"></param>
        /// <returns>The value of the _data column, which is typically a file path.</returns>
        private static string GetDataColumn(Context context, Android.Net.Uri uri, string selection, string[] selectionArgs, string column = MediaStore.Files.FileColumns.Data)
        {
            Android.Database.ICursor cursor = null;
            string[] projection = { column };

            try
            {
                cursor = context.ContentResolver.Query(uri, projection, selection, selectionArgs, null);
                if (cursor != null && cursor.MoveToFirst())
                {
                    var columnIndex = cursor.GetColumnIndexOrThrow(column);
                    return cursor.GetString(columnIndex);
                }
            }
            finally
            {
                cursor?.Close();
            }
            return null;
        }

        ///<summary>Checks whether the Uri authority is ExternalStorageProvider.</summary>
        ///<param name="uri">The Uri to check.</param>
        ///<returns>Whether the Uri authority is ExternalStorageProvider.</returns>
        private static bool IsExternalStorageDocument(Android.Net.Uri uri)
        {
            return "com.android.externalstorage.documents".Equals(uri.Authority);
        }

        ///<summary>Checks whether the Uri authority is DownloadsProvider.</summary>
        ///<param name="uri">The Uri to check.</param>
        ///<returns>Whether the Uri authority is DownloadsProvider.</returns>
        private static bool IsDownloadsDocument(Android.Net.Uri uri)
        {
            return "com.android.providers.downloads.documents".Equals(uri.Authority);
        }

        ///<summary>Checks whether the Uri authority is MediaProvider.</summary>
        ///<param name="uri">The Uri to check.</param>
        ///<returns>Whether the Uri authority is MediaProvider.</returns>
        private static bool IsMediaDocument(Android.Net.Uri uri)
        {
            return "com.android.providers.media.documents".Equals(uri.Authority);
        }

        ///<summary>Checks whether the Uri authority is GoogleDrive Provider.</summary>
        ///<param name="uri">The Uri to check.</param>
        ///<returns>Whether the Uri authority is GoogleDrive Provider.</returns>
        private static bool IsGoogleDriveDocument(Android.Net.Uri uri)
        {
            return "com.google.android.apps.docs.storage.legacy".Equals(uri.Authority);
        }

        ///<summary>Checks whether the Uri authority is Gmail Provider.</summary>
        ///<param name="uri">The Uri to check.</param>
        ///<returns>Whether the Uri authority is Gmail Provider.</returns>
        private static bool IsGmailDocument(Android.Net.Uri uri)
        {
            return "gmail-ls".Equals(uri.Authority);
        }
    }
}