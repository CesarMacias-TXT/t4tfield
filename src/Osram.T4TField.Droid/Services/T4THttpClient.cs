﻿using Android.Net;
using Javax.Net.Ssl;
using Osram.TFTBackend;
using Osram.TFTBackend.RestService.Contracts;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using Xamarin.Android.Net;

namespace Osram.T4TField.Droid.Services
{
    public class T4THttpClient : HttpClient, IT4THttpClient
    {
        public T4THttpClient() : base(GetSSL())
        {
        }

        private static HttpClientHandler GetSSL()
        {
            try
            {
                return new HttpClientHandler
                {
                    ServerCertificateCustomValidationCallback = (message, certificate, chain, sslPolicyErrors) => true
                };
            }
            catch(Exception e)
            {
                //return new AndroidClientHandler();
                return new BypassSslValidationClientHandler();
            }
        }
    }

    internal class BypassHostnameVerifier : Java.Lang.Object, IHostnameVerifier
    {
        public bool Verify(string hostname, ISSLSession session)
        {
            return true;
        }
    }

    internal class BypassSslValidationClientHandler : AndroidClientHandler
    {
        protected override SSLSocketFactory ConfigureCustomSSLSocketFactory(HttpsURLConnection connection)
        {
            return SSLCertificateSocketFactory.GetInsecure(1000, null);
        }

        protected override IHostnameVerifier GetSSLHostnameVerifier(HttpsURLConnection connection)
        {
            return new BypassHostnameVerifier();
        }
    }
}
