using System;
using System.IO;
using Android.App;
using Android.Content;
using Osram.TFTBackend;
using Uri = Android.Net.Uri;

namespace Osram.T4TField.Droid.Services
{
    public enum StartupDataType
    {
        None,
        ImportConfiguration
    }

    public static class StartupState
    {
        public static Uri UriData { get; private set; }
        public static bool HasData => (UriData != null && UriData != Uri.Empty);

        public static StartupDataType DataType { get; private set; }

        public static Stream DataStream { get; private set; }

        public static String FileName { get; private set; }

        public static Uri SetIntent(Intent splashScreenIntent)
        {
            UriData = null;

            if (splashScreenIntent == null) return Uri.Empty;
            if (splashScreenIntent.Action != Intent.ActionView) return Uri.Empty;
            if (splashScreenIntent.Scheme != "content" && splashScreenIntent.Scheme != "file") return Uri.Empty;

            FileName = UriExtensions.GetFileName(Application.Context, splashScreenIntent.Data);
            Logger.Trace($"StartupState.SetIntent Data = {FileName}");

            switch (Path.GetExtension(FileName))
            {
                case FileExtensions.OsrtupExtension:
                case FileExtensions.OsrtulExtension:
                case FileExtensions.OsrtudExtension:
                    DataType = StartupDataType.ImportConfiguration;
                    break;
                default:
                    DataType = StartupDataType.None;
                    break;
            }

            if (DataType == StartupDataType.None)
                return Uri.Empty;

            UriData = splashScreenIntent.Data;
            return UriData;
        }

        public static void OpenStream()
        {
            OpenStreamFromUri(UriData);
        }

        public static void OpenStreamFromUri(Uri uri)
        {
            DataStream = Application.Context.ContentResolver.OpenInputStream(uri);
        }

        public static void Clear()
        {
            if (UriData != null)
            {
                UriData.Dispose();
                UriData = null;
            }

            DataType = StartupDataType.None;

            if (DataStream != null)
            {
                DataStream.Close();
                DataStream.Dispose();
                DataStream = null;
            }
        }
    }

    public static class FileExtensions
    {
        public const String OsrtupExtension = ".osrtup";
        public const String OsrtudExtension = ".osrtud";
        public const String OsrtulExtension = ".osrtul";
    }
}