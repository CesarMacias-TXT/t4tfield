The splat bug is caused by a change in the C# Mono Framwork. The Splat pcl package assumes a fixed order of the returned assemblies.
Which is not working anymore.

As a temporary fix use the following code:
    public class PlatformBitmapLoader : IBitmapLoader
    {
        static readonly Dictionary<string, int> drawableList;

        static PlatformBitmapLoader()
        {
            // NB: This is some hacky shit, but on MonoAndroid at the moment, 
            // this is always the entry assembly.
            // var assm = AppDomain.CurrentDomain.GetAssemblies()[1];
            var assm = AppDomain.CurrentDomain.GetAssemblies()
                .Where(a => a.FullName.Contains("Osram.T4TField.Droid", StringComparison.OrdinalIgnoreCase))
                .SingleOrDefault();
            var resources = assm.GetModules().SelectMany(x => x.GetTypes()).First(x => x.Name == "Resource");

in the file splat\Splat\Android\Bitmaps.cs
Build the monodroid project from the Splat 1.6.2 Sources.