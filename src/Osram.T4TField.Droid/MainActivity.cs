﻿using Acr.UserDialogs;
using Android.App;
using Android.Content;
using Android.Content.PM;
using Android.Nfc;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Google.Android.Material.Snackbar;
using MvvmCross;
using MvvmCross.Binding.BindingContext;
using MvvmCross.Platforms.Android;
using MvvmCross.Platforms.Android.Views;
using MvvmCross.Plugin.Messenger;
using MvvmCross.ViewModels;
using Osram.T4TField.Droid.Helpers;
using Osram.T4TField.Droid.Services;
using Osram.T4TField.Droid.Services.Nfc;
using Osram.T4TField.Droid.ViewPresenter;
using Osram.T4TField.Resources;
using Osram.T4TField.ViewPresenter;
using Osram.TFTBackend;
using Osram.TFTBackend.ImportService.Contracts;
using Osram.TFTBackend.Messaging;
using Osram.TFTBackend.NfcService.Contracts;
using Osram.TFTBackend.NfcService.NfcDataTypes;
using Plugin.CurrentActivity;
using Plugin.InAppBilling;
using Plugin.Permissions;
using Plugin.Permissions.Abstractions;
using Splat;
using System;
using System.Net;
using System.Threading.Tasks;
using Xamarin.Forms;
using Permission = Android.Content.PM.Permission;

namespace Osram.T4TField.Droid
{
    [Activity(Icon = "@drawable/icon",
        ConfigurationChanges = ConfigChanges.Locale | ConfigChanges.ScreenSize | ConfigChanges.Orientation,
        ScreenOrientation = ScreenOrientation.Portrait,
        LaunchMode = LaunchMode.SingleTask
    )]
    public class MainActivity : OsramMvxFormsAppCompatActivity, IMvxAndroidView, IMvxAndroidCurrentTopActivity, NfcAdapter.IReaderCallback
    {
        private bool IsFromIntent;

        protected override void OnCreate(Bundle bundle)
        {
            // Must be called before base.OnCreate (bundle);
            ToolbarResource = Resource.Layout.Toolbar;
            TabLayoutResource = Resource.Layout.TabLayout;
            ServicePointManager
                .ServerCertificateValidationCallback +=
                (sender, cert, chain, sslPolicyErrors) => true;

            CarouselView.FormsPlugin.Android.CarouselViewRenderer.Init();

            // Leverage controls' StyleId attrib. to Xamarin.UITest
            Forms.ViewInitialized += (sender, e) =>
            {
                var automationId = e.View.AutomationId;
                if (!string.IsNullOrWhiteSpace(automationId))
                {
                    e.NativeView.ContentDescription = automationId;
                }
            };

            UserDialogs.Init(() => (Activity)CrossCurrentActivity.Current.Activity);

            var presenter = Mvx.IoCProvider.Resolve<MasterDetailViewPresenter>();
            if (presenter == null)
                return;
            Forms.Init(this, bundle, GetResourceAssembly());

            var mvxFormsApp = new T4TFieldApp();

            presenter.FormsApplication = mvxFormsApp;

            base.OnCreate(bundle);
            Rg.Plugins.Popup.Popup.Init(this, bundle);
            Xamarin.Essentials.Platform.Init(this, bundle);

            Locator.CurrentMutable.InitializeSplat();
            Locator.CurrentMutable.RegisterPlatformBitmapLoader();
            _messenger = Mvx.IoCProvider.Resolve<IMvxMessenger>();
            InitialiseNfc();          

            Mvx.IoCProvider.RegisterSingleton(this);
            Mvx.IoCProvider.RegisterSingleton<IMvxAndroidCurrentTopActivity>(this);

            SupportActionBar.SetDisplayHomeAsUpEnabled(true);
            SupportActionBar.SetHomeButtonEnabled(true);

            //startup intent (set data for import)
            CheckForIntentData(Intent);
        }

        protected override void OnNewIntent(Intent intent)
        {
            CheckForIntentData(intent);
            base.OnNewIntent(intent);
        } 

        public static Android.Support.V7.Widget.Toolbar ToolBar { get; set; }
        public override bool OnCreateOptionsMenu(IMenu menu)
        {
            ToolBar = FindViewById<Android.Support.V7.Widget.Toolbar>(Resource.Id.toolbar);
            return base.OnCreateOptionsMenu(menu);
        }

        private void CheckForIntentData(Intent intent)
        {
            IsFromIntent = true;
            StartupState.SetIntent(intent);
            //if (StartupState.HasData)
            //{
            //    if (!await CheckStoragePermissions())
            //    {
            //        StartupState.Clear();
            //        return;
            //    }
            //    StartupState.OpenStreamFromUri(uri);
            //}
        }

        public object DataContext { get; set; }
        public IMvxViewModel ViewModel { get; set; }
        public void MvxInternalStartActivityForResult(Intent intent, int requestCode)
        {
            StartActivityForResult(intent, requestCode);
        }

        public IMvxBindingContext BindingContext { get; set; }
        public Activity Activity => this;

        protected override void OnPause()
        {
            //this is needed on android because of the ordering in which the app events are called (the forms app pause and the ones in the view).
            // used for view models to suspend vs cleanup
            Osram.AppState = AppState.Suspended;
            base.OnPause();
            DisableNfc();
        }
        protected override async void OnResume()
        {
            Osram.AppState = AppState.Resumed;

            base.OnResume();
            EnableNfc();
            if (IsFromIntent)
            {
                IsFromIntent = false;
                await HandleStartupIntents();
            }
        }
        protected override void OnDestroy()
        {
            base.OnDestroy();
            DisposeNfc();
            _messenger.Unsubscribe<Events.OnNfcReaderChangedEvent>(subscriptionToken);
        }

        protected override void AttachBaseContext(Context context)
        {
            base.AttachBaseContext(ConfigurationWrapper.WrapLocale(context, Java.Util.Locale.Default));
        }

        #region Nfc
        /// <summary>
        /// Receiver for nfc ON or OFF events 
        /// </summary>
        private NfcBroadcastReceiver nfcReceiver;
        private MobileNfcReader nfcReader;
        private NfcAdapter nfcAdapter;
        private IMvxMessenger _messenger;
        private MvxSubscriptionToken subscriptionToken;

        private void InitialiseNfc()
        {
            NfcManager nfcManager = (NfcManager)GetSystemService(NfcService);
            nfcAdapter = nfcManager.DefaultAdapter;

            nfcReceiver = new NfcBroadcastReceiver();
            nfcReceiver.NfcConnectEvent += delegate
            {
                nfcReader?.RaiseReaderConnected();
            };
            nfcReceiver.NfcDisconnectEvent += delegate
            {
                nfcReader?.RaiseReaderDisconnected();
            };

            nfcReader = new MobileNfcReader(nfcAdapter);
            Mvx.IoCProvider.RegisterSingleton<INfcCommunication>(nfcReader);
            Mvx.IoCProvider.RegisterSingleton<IMobileNfcReader>(nfcReader);
            subscriptionToken = _messenger.Subscribe<Events.OnNfcReaderChangedEvent>(RegisterMobileNfcReader);
        }

        private void RegisterMobileNfcReader(Events.OnNfcReaderChangedEvent ev)
        {
            if (ev.ReaderTypeSelected == NfcReaderType.Mobile)
            {
                Mvx.IoCProvider.RegisterSingleton<INfcCommunication>(nfcReader);
                _messenger.Publish(new Events.OnNfcReaderUpdateEvent(this));
            }
        }

        private void DisposeNfc()
        {
            if (nfcAdapter != null)
            {
                nfcAdapter.Dispose();
                nfcAdapter = null;
            }
        }
        private void EnableNfc()
        {
            if (nfcAdapter != null)
            {
                var intent = new Intent(this, GetType()).AddFlags(ActivityFlags.SingleTop);
                nfcAdapter.EnableReaderMode(this, this, NfcReaderFlags.NfcV, null);
            }

            RegisterReceiver(nfcReceiver, new IntentFilter(NfcAdapter.ActionAdapterStateChanged));
        }
        private void DisableNfc()
        {
            if (nfcAdapter != null)
            {
                nfcAdapter.DisableReaderMode(this);
            }
            UnregisterReceiver(nfcReceiver);
        }

        public void OnTagDiscovered(Tag tag)
        {
            Logger.Trace("MainActivity OnTagDiscovered");
            nfcReader.OnNewIntent(this, tag);
        }
        #endregion

        private async Task<bool> CheckStoragePermissions()
        {
            try
            {
                // check storage permission
                var storagePermission = Plugin.Permissions.Abstractions.Permission.Storage;
                var permissionStatus = await CrossPermissions.Current.CheckPermissionStatusAsync(storagePermission);

                if (permissionStatus != PermissionStatus.Granted)
                {
                    var result = await CrossPermissions.Current.RequestPermissionsAsync(storagePermission);

                    PermissionStatus resultStatus;
                    if (!result.TryGetValue(storagePermission, out resultStatus) || resultStatus != PermissionStatus.Granted)
                    {
                        throw new Exception("Permission denied.");
                    }
                }
                return true;
            }
            catch
            {
                var view = FindViewById(Android.Resource.Id.Content);
                var snackbar = Snackbar.Make(view, AppResources.StoragePermissionDenied, Snackbar.LengthShort);
                snackbar.Show();
                return false;
            }
        }

        /// <summary>
        ///     Handle intents for importing configurations or firmware
        /// </summary>
        private async Task HandleStartupIntents()
        {
            if (!StartupState.HasData) return;

            if (!await CheckStoragePermissions())
            {
                StartupState.Clear();
                return;
            }

            try
            {
                StartupState.OpenStream();

                var importService = Mvx.IoCProvider.Resolve<IImportService>();
                switch (StartupState.DataType)
                {
                    case StartupDataType.ImportConfiguration:
                        bool success = await importService.Import(StartupState.FileName, StartupState.DataStream);
                        String msg = success
                            ? string.Format(AppResources.F_ImportSuccessful, StartupState.FileName)
                            : string.Format(AppResources.F_ImportFailed, StartupState.FileName);
                        await Mvx.IoCProvider.Resolve<IUserDialogs>().AlertAsync(msg);
                        break;
                    default:
                        Logger.Error("Unknown intent. Must be defined in StartupDataType");
                        break;
                }
            }
            catch (Exception e)
            {
                Logger.Exception("HandleStartupIntents failed", e);
                throw;
            }
            finally
            {
                StartupState.Clear();
            }
        }

        protected override void OnActivityResult(int requestCode, Result resultCode, Intent data)
        {
            base.OnActivityResult(requestCode, resultCode, data);
            InAppBillingImplementation.HandleActivityResult(requestCode, resultCode, data);
        }

        public override void OnRequestPermissionsResult(int requestCode, string[] permissions, [GeneratedEnum] Permission[] grantResults)
        {
            base.OnRequestPermissionsResult(requestCode, permissions, grantResults);
            PermissionsImplementation.Current.OnRequestPermissionsResult(requestCode, permissions, grantResults);
        }

    }
}

