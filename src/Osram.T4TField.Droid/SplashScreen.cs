using Android.App;
using Android.Content;
using Android.Nfc;
using Android.Views;
using MvvmCross.Platforms.Android.Views;
using Osram.T4TField.Droid.Services;
using System.Threading.Tasks;

namespace Osram.T4TField.Droid
{
    [Activity(MainLauncher = true, Icon = "@drawable/icon", NoHistory = true)]
    [IntentFilter(new[] { Intent.ActionView }, Categories = new[] { Intent.CategoryBrowsable, Intent.CategoryDefault }, DataScheme = "file", DataMimeType = "*/*", DataPathPattern = @".*\\" + FileExtensions.OsrtupExtension)]
    [IntentFilter(new[] { Intent.ActionView }, Categories = new[] { Intent.CategoryBrowsable, Intent.CategoryDefault }, DataScheme = "file", DataMimeType = "*/*", DataPathPattern = @".*\\" + FileExtensions.OsrtulExtension)]
    [IntentFilter(new[] { Intent.ActionView }, Categories = new[] { Intent.CategoryBrowsable, Intent.CategoryDefault }, DataScheme = "file", DataMimeType = "*/*", DataPathPattern = @".*\\" + FileExtensions.OsrtudExtension)]
    [IntentFilter(new[] { Intent.ActionView }, Categories = new[] { Intent.CategoryBrowsable, Intent.CategoryDefault }, DataScheme = "content", DataMimeType = "*/*", DataPathPattern = @".*\\" + FileExtensions.OsrtupExtension)]
    [IntentFilter(new[] { Intent.ActionView }, Categories = new[] { Intent.CategoryBrowsable, Intent.CategoryDefault }, DataScheme = "content", DataMimeType = "*/*", DataPathPattern = @".*\\" + FileExtensions.OsrtulExtension)]
    [IntentFilter(new[] { Intent.ActionView }, Categories = new[] { Intent.CategoryBrowsable, Intent.CategoryDefault }, DataScheme = "content", DataMimeType = "*/*", DataPathPattern = @".*\\" + FileExtensions.OsrtudExtension)]
    /* START NFC INTENT https://developer.android.com/guide/topics/connectivity/nfc/nfc#tech-disc */
    [IntentFilter(new[] { NfcAdapter.ActionNdefDiscovered }, Categories = new[] { Intent.CategoryDefault })]
    [IntentFilter(new[] { NfcAdapter.ActionTechDiscovered }, Categories = new[] { Intent.CategoryDefault }), MetaData(NfcAdapter.ActionTechDiscovered, Resource = "@xml/nfc_tech_filter")]
    [IntentFilter(new[] { NfcAdapter.ActionTagDiscovered }, Categories = new[] { Intent.CategoryDefault })]
    /* END NFC INTENT */
    public class SplashScreen : MvxSplashScreenActivity
    {
        private bool _isInitializationComplete;

        public SplashScreen() : base(Resource.Layout.SplashScreen)
        {
        }

        protected override void RequestWindowFeatures()
        {
            base.RequestWindowFeatures();

            // Hide the status bar.
            Window.DecorView.SystemUiVisibility = StatusBarVisibility.Hidden;
        }

        public override  Task InitializationComplete()
        {
            if (_isInitializationComplete)
                 base.InitializationComplete();

            _isInitializationComplete = true;

            var intent = new Intent(Intent);
            intent.SetClass(ApplicationContext, typeof(MainActivity));
            StartActivity(intent);
            return Task.CompletedTask;
        }
    }
}