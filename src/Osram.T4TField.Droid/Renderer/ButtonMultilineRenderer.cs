﻿using Android.Content;
using Osram.T4TField.Controls;
using Osram.T4TField.Droid.Renderer;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;

[assembly: ExportRenderer(typeof(ButtonMultiline), typeof(ButtonMultilineRenderer))]

namespace Osram.T4TField.Droid.Renderer
{


    class ButtonMultilineRenderer : ButtonRenderer
    {

        public ButtonMultilineRenderer(Context context) : base(context)
        {

        }

        protected override void OnElementChanged(ElementChangedEventArgs<Button> e)
        {
            base.OnElementChanged(e);

            if (this.Control != null)
            {
                Control.SetSingleLine(false);
            }
        }
    }
}