﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Xamarin.Forms.Platform.Android.AppCompat;
using Xamarin.Forms;
using Osram.T4TField.Droid.Renderer;
using Xamarin.Forms.Platform.Android;
using Osram.T4TField.Controls;


[assembly: ExportRenderer(typeof(BindableDataPicker), typeof(BindableDataPikerRenderer))]

namespace Osram.T4TField.Droid.Renderer
{


    class BindableDataPikerRenderer : DatePickerRenderer
    {

        public BindableDataPikerRenderer(Context context) : base(context)
        {

        }

        protected override void OnElementChanged(ElementChangedEventArgs<Xamarin.Forms.DatePicker> e)
        {
            base.OnElementChanged(e);

            int FontSize = (int)(Element as BindableDataPicker)?.FontSize;


            if (this.Control != null)
            {
                TextView label = (TextView)Control;
                label.TextSize = FontSize;
                label.SetIncludeFontPadding(false);
                label.SetLineSpacing(-5, -5);
                label.SetMaxLines(1);

            }
        }



    }
}