﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Xamarin.Forms.Platform.Android.AppCompat;
using Xamarin.Forms;
using Osram.T4TField.Droid.Renderer;
using Xamarin.Forms.Platform.Android;
using Osram.T4TField.Controls;


[assembly: ExportRenderer(typeof(BindablePicker), typeof(BindablePickerRenderer))]

namespace Osram.T4TField.Droid.Renderer
{


    class BindablePickerRenderer : Xamarin.Forms.Platform.Android.AppCompat.PickerRenderer
    {
        public BindablePickerRenderer(Context context) : base(context)
        {

        }

        protected override void OnElementChanged(ElementChangedEventArgs<Picker> e)
        {
            base.OnElementChanged(e);

            if (this.Control != null)
            {
                int FontSize = (int)(Element as BindablePicker)?.FontSize;


                TextView label = (TextView)Control;
                label.TextSize = FontSize;
                label.SetIncludeFontPadding(false);
                label.SetLineSpacing(-5, -5);
                label.SetMaxLines(1);

            }
        }



    }
}