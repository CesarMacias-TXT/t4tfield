using Android.Content;
using Osram.T4TField.Controls;
using Osram.T4TField.Droid;
using System.ComponentModel;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;

[assembly: ExportRenderer(typeof(XFormNumberPicker), typeof(NumberPickerRenderer))]

namespace Osram.T4TField.Droid
{
    public class NumberPickerRenderer : ViewRenderer<XFormNumberPicker, NumberPickerLayout>
    {

        private NumberPickerLayout tp;

        public NumberPickerRenderer(Context context) : base(context)
        {


        }

        protected override void OnElementChanged(ElementChangedEventArgs<XFormNumberPicker> e)
        {
            base.OnElementChanged(e);

            if (e.OldElement != null || Element == null)
                return;

            if (Control == null)
            {
                CreateNewControl();
            }
        }

        private void CreateNewControl()
        {
            //var context = Forms.Context;
            tp = new NumberPickerLayout(Context, Element.Type, Element.Current, Element.Min, Element.Max);

            tp.valueChange += (int value) =>
            {
                if (Element != null)
                {
                    Element.Current = value;
                }
            };
            SetNativeControl(tp);
        }

        protected override void OnElementPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            base.OnElementPropertyChanged(sender, e);
        }
    }
}