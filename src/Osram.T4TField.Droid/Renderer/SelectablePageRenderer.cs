﻿using System;
using System.Linq;
using Android.Views;
using Xamarin.Forms.Platform.Android;
using Osram.T4TField.Droid.Renderer;
using Xamarin.Forms;
using Osram.T4TField.Views;
using Plugin.CurrentActivity;
using System.Collections.ObjectModel;
using Osram.T4TField.ViewModels;
using Android.Widget;
using Osram.T4TField.Droid.Adapter;
using static Osram.T4TField.Utils.FeaturesUtils;
using Osram.T4TField.Utils;
using Osram.TFTBackend.DataModel.Feature.Contracts;
using Android.Content;

[assembly: ExportRenderer(typeof(Osram.T4TField.Views.SelectableView), typeof(AdvancedModePageRenderer))]
namespace Osram.T4TField.Droid.Renderer
{
    class AdvancedModePageRenderer : PageRenderer
    {
        private Spinner SelectFeaturesSpinner;

        private FeatureType SelectedFeaturesType { get; set; }

        private ViewModels.FeatureInfoButton CurrentFeature { get; set; }

        private bool IsAdvancedMode { get; set; }

        public AdvancedModePageRenderer(Context context) : base(context)
        {

        }

        public ObservableCollection<ViewModels.FeatureInfoButton> ListOfFeatures { get; private set; }

        protected override void OnElementChanged(ElementChangedEventArgs<Page> e)
        {
            base.OnElementChanged(e);
            AddCustomElemInToolBar();
        }

        protected override void Dispose(bool disposing)
        {
            base.Dispose(true);
            if (IsAdvancedMode)
            {
                GetToolbar().RemoveAllViews();
            }
        }

        private static Android.Support.V7.Widget.Toolbar GetToolbar() => (CrossCurrentActivity.Current?.Activity as MainActivity)?.FindViewById<Android.Support.V7.Widget.Toolbar>(Resource.Id.toolbar);


        private void AddCustomElemInToolBar()
        {
            if (MainActivity.ToolBar == null || Element == null)
            {
                return;
            }

            IsAdvancedMode = (bool)(Element as SelectableView)?.IsAdvancedMode;
            ListOfFeatures = (ObservableCollection<ViewModels.FeatureInfoButton>)(Element as SelectableView)?.ListOfFeatures;
            SelectedFeaturesType = (FeatureType)(Element as SelectableView)?.SelectedFeature;

            if (IsAdvancedMode)
            {

                Android.Widget.RelativeLayout toolbarLayout = new Android.Widget.RelativeLayout(Context);
                Android.Support.V7.Widget.SwitchCompat SwitchButton = new Android.Support.V7.Widget.SwitchCompat(Context);
                Android.Widget.RelativeLayout.LayoutParams SwitchButtonLayoutParam = new Android.Widget.RelativeLayout.LayoutParams(
                    ViewGroup.LayoutParams.WrapContent,
                    ViewGroup.LayoutParams.MatchParent);
                SwitchButtonLayoutParam.AddRule(Android.Widget.LayoutRules.AlignParentRight);
                SwitchButton.LayoutParameters = SwitchButtonLayoutParam;
                SwitchButton.Checked = getCurrentFeatures().IsSelected;

                SwitchButton.CheckedChange -= selectionChange;
                SwitchButton.CheckedChange += selectionChange;


                SelectFeaturesSpinner = new Android.Widget.Spinner(Context);
                SelectFeaturesSpinner.LayoutParameters = new ViewGroup.LayoutParams(
                   ViewGroup.LayoutParams.WrapContent,
                   ViewGroup.LayoutParams.MatchParent);

                //  var items = createListOfFeatures();
                var adapter = new FeatureAdapter((CrossCurrentActivity.Current?.Activity as MainActivity), ListOfFeatures);
                SelectFeaturesSpinner.Adapter = adapter;
                SelectFeaturesSpinner.SetSelection(getSelectedPositions());
                SelectFeaturesSpinner.ItemSelected -= itemClick;
                SelectFeaturesSpinner.ItemSelected += itemClick;

                toolbarLayout.AddView(SelectFeaturesSpinner);
                toolbarLayout.AddView(SwitchButton);
                toolbarLayout.LayoutParameters = new ViewGroup.LayoutParams(
                ViewGroup.LayoutParams.MatchParent,
                ViewGroup.LayoutParams.MatchParent);
                GetToolbar().Title = String.Empty;
                GetToolbar().AddView(toolbarLayout);

            }
        }




        private void selectionChange(object sender, Android.Support.V7.Widget.SwitchCompat.CheckedChangeEventArgs e)
        {
            updateSelectedFeature(e.IsChecked);
        }

        private void updateSelectedFeature(bool IsChecked)
        {
            getCurrentFeatures().IsSelected = IsChecked;
        }

        private void itemClick(object sender, AdapterView.ItemSelectedEventArgs e)
        {
            ViewModels.FeatureInfoButton features = ((FeatureAdapter)SelectFeaturesSpinner.Adapter)[e.Position];
            if (features.FeatureInfo.FeatureType != SelectedFeaturesType)
            {
                (features.OnClickButton).Execute(null);
            }
        }

        private int getSelectedPositions()
        {
            ViewModels.FeatureInfoButton currentFeatures = getCurrentFeatures();
            var position = ListOfFeatures.IndexOf(currentFeatures);
            return position;
        }

        private ViewModels.FeatureInfoButton getCurrentFeatures()
        {
            if (CurrentFeature == null)
            {
                CurrentFeature = ListOfFeatures.Where(x => x.FeatureInfo.FeatureType == SelectedFeaturesType).First();
            }
            return CurrentFeature;
        }
    }
}