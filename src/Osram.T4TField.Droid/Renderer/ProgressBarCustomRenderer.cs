using System;
using System.ComponentModel;
using Osram.T4TField.Droid.Renderer;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;

using Android.Graphics;
using Osram.T4TField.Controls;
using Android.Content;
using Plugin.CurrentActivity;

[assembly: ExportRenderer(typeof(IndicatorProgressBar), typeof(ProgressBarCustomRenderer))]


namespace Osram.T4TField.Droid.Renderer
{

    class ProgressBarCustomRenderer : ProgressBarRenderer
    {

        public ProgressBarCustomRenderer(Context context) : base(context)
        {

        }

        protected override void OnElementChanged(ElementChangedEventArgs<ProgressBar> e)
        {
            base.OnElementChanged(e);
            Control.SetPadding(0, 0, 0, 0);
            Control.ScaleY = 2; // This changes the height
            Control.Indeterminate = true;
            var margin = Control.Height;
            this.SetWillNotDraw(false);
        }


        protected override void OnDraw(Canvas canvas)
        {
            var margin = Control.PivotY + (Control.PivotY / 2);
            canvas.Translate(0, -margin);
            base.OnDraw(canvas);
        }

        protected override void OnElementPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            base.OnElementPropertyChanged(sender, e);

            if (e.PropertyName == "Indeterminate")
            {
                if (Control.Indeterminate != ((IndicatorProgressBar)sender).Indeterminate)
                {
                    Control.Indeterminate = ((IndicatorProgressBar)sender).Indeterminate;
                }
            }

        }



    }
}