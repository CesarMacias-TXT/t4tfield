using Android.Content;
using Android.Support.V4.Content;
using Android.Views;
using Java.Lang;
using Osram.T4TField.Controls;
using Osram.T4TField.Droid.Controls;
using Osram.T4TField.Droid.Renderer;
using System.ComponentModel;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;

[assembly: ExportRenderer(typeof(TouchedBarLayout), typeof(BoxLayoutRenderer))]

namespace Osram.T4TField.Droid.Renderer
{
    public class BoxLayoutRenderer : ViewRenderer<TouchedBarLayout, BarIndicator>
    {
        private readonly CustomGestureListner _listener;
        private readonly GestureDetector _detector;
        private BarIndicator _bar;

        public BoxLayoutRenderer(Context context) : base(context)
        {
            _listener = new CustomGestureListner();
            _detector = new GestureDetector(context, _listener);

        }

        protected override void OnElementChanged(ElementChangedEventArgs<TouchedBarLayout> e)
        {
            base.OnElementChanged(e);

            if (e.NewElement == null)
            {
                // this.GenericMotion -= HandleGenericMotion;
                // this.Touch -= HandleTouch;
            }

            if (e.OldElement == null)
            {
                // this.GenericMotion += HandleGenericMotion;
                //  this.Touch += HandleTouch;
            }
            //var context = Forms.Context;
            var color = new Android.Graphics.Color(ContextCompat.GetColor(Context, Resource.Color.OsramColor));
            _bar = new BarIndicator(Context, Element.AutomationId, Element.Current, Element.Min, Element.Max, Element.Default, Element.IsEnabled, Element.IsValid, color);
            SetNativeControl(_bar);

            _bar.valueChange += (int value) =>
            {
                if (Element != null)
                {
                    Element.Current = value;
                }
            };

            _bar.OnScrollFinished += () => { Element?.OnGestureFinished?.Execute(null); };

            _bar.Post(new Runnable(() => Run()));
        }


        public void Run()
        {
            int width = _bar.MeasuredWidth;
            int height = _bar.MeasuredHeight;
            _bar.SetHeightAndText(width);
        }


        protected override void OnLayout(bool changed, int l, int t, int r, int b)
        {
            base.OnLayout(changed, l, t, r, b);
            //if(changed)
            //_bar.setHeightAndTextFirstStart(Element.Current);

        }

        void HandleTouch(object sender, TouchEventArgs e)
        {
            _detector.OnTouchEvent(e.Event);
        }

        void HandleGenericMotion(object sender, GenericMotionEventArgs e)
        {
            _detector.OnTouchEvent(e.Event);
        }

        protected override void OnElementPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            base.OnElementPropertyChanged(sender, e);

            if (Control == null || Element == null) return;
            if (e.PropertyName == TouchedBarLayout.CurrentProperty.PropertyName)
            {
                if (Element.Current != Control.Current)
                {
                    Control.Current = Element.Current;
                }
            }
            if (e.PropertyName == TouchedBarLayout.MinProperty.PropertyName)
            {
                if (Element.Min != Control.LimitedMinValue)
                {
                    Control.LimitedMinValue = Element.Min;
                }
            }
            if (e.PropertyName == TouchedBarLayout.MaxProperty.PropertyName)
            {
                if (Element.Max != Control.LimitedMaxValue)
                {
                    Control.LimitedMaxValue = Element.Max;
                }
            }
            if (e.PropertyName == TouchedBarLayout.IsValidProperty.PropertyName)
            {
                if (Element.IsValid != Control.IsValid)
                {
                    Control.IsValid = Element.IsValid;
                }
            }
            if (e.PropertyName == TouchedBarLayout.IsEnabledProperty.PropertyName)
            {
                if (Element.IsEnabled != Control.IsEnabled)
                {
                    Control.IsEnabled = Element.IsEnabled;
                }
            }

        }
    }
}