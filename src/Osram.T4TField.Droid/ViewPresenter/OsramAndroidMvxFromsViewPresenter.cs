﻿using MvvmCross;
using MvvmCross.Platforms.Android.Presenters;
using Osram.T4TField.Droid.Services;
using Osram.T4TField.ViewPresenter;
using Xamarin.Forms;

namespace Osram.T4TField.Droid.ViewPresenter
{
    public class OsramAndroidMvxFromsViewPresenter : MasterDetailViewPresenter, IMvxAndroidViewPresenter
    {
        protected override void OnPagePresented(Page page)
        {
            Mvx.IoCProvider.Resolve<TabGestureService>().UpdateCurrentTabView(page);

            //if (page is AvalableDevicesListView || page is SearchingDeviceView)
            //{
            //    Mvx.Resolve<MainActivity>().PromptBleStart();
            //}
        }
    }
}