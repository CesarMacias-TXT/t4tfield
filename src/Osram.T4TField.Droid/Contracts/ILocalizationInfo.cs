﻿using System.Globalization;

namespace Osram.T4TField.Droid.Contracts
{
    public interface ILocalizationInfo
    {
        CultureInfo GetCurrentCultureInfo();
    }
}