﻿namespace Osram.T4TField.Droid.Contracts
{
    public interface ISystemUtils
    {
        void PreventDeviceSleep(bool shouldPrevent);
    }
}