using System;

namespace Osram.T4TField.Droid.Contracts
{
    public interface IPlatformSpecificCalls
    {
        void BeginInvokeOnMainThread(Action action);
    }
}