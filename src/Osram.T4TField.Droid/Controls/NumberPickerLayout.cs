﻿using Android.Content;
using Android.Views;
using Android.Widget;
using System;
using System.Collections.Generic;
using System.Linq;
using static Osram.T4TField.Controls.XFormNumberPicker;

namespace Osram.T4TField.Droid
{
    public class NumberPickerLayout : LinearLayout
    {


        private NumberPicker np;

        public Action<int> valueChange;


        public NumberPickerLayout(Context context, PickerType type, int currentValue, int? min, int? max) : base(context)
        {
            np = new NumberPicker(context);
            this.SetGravity(GravityFlags.Center);

            np.SetDisplayedValues(null);
            if (type.Equals(PickerType.Hour))
            {
                np.MinValue = min != null ? (int)min : 0;
                np.MaxValue = max != null ? (int)max : 23;
                String[] lm = { "00", "01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23" };
                np.SetDisplayedValues(lm.Where(n => int.Parse(n) >= np.MinValue && int.Parse(n) <= np.MaxValue).ToArray());

            }
            else if (type.Equals(PickerType.Minutes))
            {
                np.MinValue = min != null ? (int)min : 0;
                np.MaxValue = max != null ? (int)max : 59;
                String[] lm = { "00", "01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "24", "25", "26", "27", "28", "29", "30", "31", "32", "33", "34", "35", "36", "37", "38", "39", "40", "41", "42", "43", "44", "45", "46", "47", "48", "49", "50", "51", "52", "53", "54", "55", "56", "57", "58", "59" };
                np.SetDisplayedValues(lm.Where(n => int.Parse(n) >= np.MinValue && int.Parse(n) <= np.MaxValue).ToArray());

            }
            else if (type.Equals(PickerType.Number))
            {
                if (min != null)
                {
                    np.MinValue = (int)min;
                }
                if (max != null)
                {
                    np.MaxValue = (int)max;
                }


                if (min != null && max != null)
                {
                    List<String> lm = new List<String>();
                    for (var i = min; i <= max; i++)
                    {
                        lm.Add(i.ToString());
                    }

                    np.SetDisplayedValues(lm.ToArray());
                }
            }

            np.Value = currentValue;

            np.ValueChanged += (sender, e) =>
            {
                valueChange(e.NewVal);
            };
            AddView(np);
        }

    }

}