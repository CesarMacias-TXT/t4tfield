﻿using Android.Content;
using Android.Graphics;
using Android.Support.V4.Content;
using Android.Support.V4.View;
using Android.Util;
using Android.Views;
using Android.Widget;
using Osram.T4TField.Controls.SliderWheel;
using Osram.T4TField.Utils;
using Plugin.CurrentActivity;


using Splat;
using System;

namespace Osram.T4TField.Droid.Controls
{
    public class SliderWheel : SquareLayout
    {
        private int _ballWidth;

        private SliderWheelType _controlType;
        private int _controlWidth;
        private int _currentAngle;
        private PointF _currentPosition;
        private bool _dragging;

        private bool _isButtonVisible;

        private bool _isChecked;

        private int _onoffButtonWidth;
        private Area _previousArea = Area.NoKnownArea;
        private SliderWheelSkin _skin;
        private int _value = -1;

        public SliderWheel(Context context, SliderWheelType controlType = SliderWheelType.OnOffSlider)
            : base(context)
        {
            Initialize();
            ControlType = controlType;

        }

        private ImageView SliderImageView { get; set; }
        public RelativeLayout CenterButtonLayout { get; set; }
        private RelativeLayout StatusBallLayout { get; set; }
        private ImageView OnOffButtonImageView { get; set; }
        private TextView BallStatusText { get; set; }
        private TextView CenterButtonText { get; set; }
        private TextView CenterButtonUnitText { get; set; }
        private TextView CenterStatusText { get; set; }

        private TextView BottomStatusText { get; set; }

        private TextView StatusText { get; set; }
        public ImageView StatusBallImage { get; set; }

        public bool NeedSave { get; set; }

        public int Value {
            get { return _value; }
            set {
                System.Diagnostics.Debug.WriteLine("_value = {0}  value = {1} _dragging = {2}", _value, value, _dragging);
                if (!_dragging)
                {
                    InternalSetValue(value);
                }
            }
        }

        public int MinValue { get; set; }
        public int MaxValue { get; set; }

        private int outputValue;
        public int OutputValue {

            get { return outputValue; }
            set {
                outputValue = value;
                // Change Position of ball if not correct
                if ((Value - 1) * StepValue >= value || (Value + 1) * StepValue <= value)
                {
                    var s = (int)Math.Round((double)value / StepValue);
                    InternalSetValue(s);
                }
                UpdateCenterText();
            }
        }

        public int OutputValueMin { get; set; }
        public int OutputValueMax { get; set; }

        public double StepValue { get; set; }

        private string secondOutputValue;
        public string SecondOutputValue {

            get { return secondOutputValue; }
            set {
                secondOutputValue = value;
                UpdateCenterText();
            }
        }

        private string outputUnit;
        private bool _isEnabled;

        public string OutputUnit {

            get { return outputUnit; }
            set {
                outputUnit = value;
                UpdateCenterText();
            }
        }

        public bool IsChecked {
            get { return _isChecked; }
            set {
                _isChecked = value;

                OnOffButtonImageView.SetImageDrawable(value
                    ? _skin.MiddleButtonCheckedImage.ToNative()
                    : _skin.MiddleButtonUncheckedImage.ToNative());
            }
        }

        public bool IsEnabled {
            get { return _isEnabled; }
            set {
                _isEnabled = value;
                UpdateTextColorByEnabledStaus();
            }
        }


        public string Text {
            get { return CenterButtonText.Text; }
            set {
                CenterButtonText.Text = value;
                //CenterButtonText.TextSize = 40;//Update on text size
            }
        }

        public bool IsButtonVisible {
            get { return _isButtonVisible; }
            set {
                _isButtonVisible = value;
                UpdateControlType();
            }
        }

        public SliderWheelType ControlType {
            get { return _controlType; }
            set {
                _controlType = value;

                UpdateControlType();

                UpdateSkin();
            }
        }

        public event EventHandler OnValueChanged;
        public event EventHandler OnButtonClicked;

        public event EventHandler OnValueClicked;

        public void InternalSetValue(int value)
        {
            if (_value != value)
            {
                _value = Math.Max(Math.Min(value, MaxValue), MinValue);

                if (ControlType == SliderWheelType.OnOffSlider || ControlType == SliderWheelType.Slider ||
                    ControlType == SliderWheelType.SaveSlider)
                {
                    UpdateSlider();
                    UpdateBallText();
                }
            }
        }

        private void UpdateControlType()
        {
            switch (ControlType)
            {
                case SliderWheelType.SaveSlider:
                    CenterButtonLayout.Visibility = (IsButtonVisible && !_dragging)
                        ? ViewStates.Visible
                        : ViewStates.Invisible;
                    OnOffButtonImageView.Visibility = ViewStates.Visible;
                    CenterButtonText.Visibility = ViewStates.Visible;

                    StatusBallLayout.Visibility = ViewStates.Visible;
                    BallStatusText.Visibility = ViewStates.Visible;

                    SliderImageView.Visibility = ViewStates.Visible;

                    CenterStatusText.Visibility = CenterButtonLayout.Visibility == ViewStates.Visible
                        ? ViewStates.Invisible
                        : ViewStates.Visible;
                    break;
                case SliderWheelType.OnOffSlider:
                    CenterButtonLayout.Visibility = (IsButtonVisible && !_dragging)
                        ? ViewStates.Visible
                        : ViewStates.Invisible;
                    OnOffButtonImageView.Visibility = ViewStates.Visible;
                    CenterButtonText.Visibility = ViewStates.Visible;

                    SliderImageView.Visibility = ViewStates.Visible;

                    StatusBallLayout.Visibility = ViewStates.Visible;
                    BallStatusText.Visibility = ViewStates.Visible;

                    CenterStatusText.Visibility = CenterButtonLayout.Visibility == ViewStates.Visible
                        ? ViewStates.Invisible
                        : ViewStates.Visible;
                    break;
                case SliderWheelType.Slider:
                    CenterButtonLayout.Visibility = ViewStates.Invisible;

                    SliderImageView.Visibility = ViewStates.Visible;

                    StatusBallLayout.Visibility = ViewStates.Visible;
                    BallStatusText.Visibility = ViewStates.Visible;

                    CenterStatusText.Visibility = ViewStates.Visible;
                    break;
                case SliderWheelType.OnOff:
                    CenterButtonLayout.Visibility = ViewStates.Visible;
                    OnOffButtonImageView.Visibility = ViewStates.Visible;
                    CenterButtonText.Visibility = ViewStates.Visible;

                    SliderImageView.Visibility = ViewStates.Invisible;
                    StatusBallLayout.Visibility = ViewStates.Invisible;
                    BallStatusText.Visibility = ViewStates.Invisible;

                    CenterStatusText.Visibility = CenterButtonLayout.Visibility == ViewStates.Visible
                        ? ViewStates.Invisible
                        : ViewStates.Visible;
                    break;
            }
        }

        private void Initialize()
        {
            //Background
            SliderImageView = new ImageView(Context);
            SliderImageView.LayoutParameters = new LayoutParams(ViewGroup.LayoutParams.MatchParent, ViewGroup.LayoutParams.MatchParent);
            SliderImageView.SetScaleType(ImageView.ScaleType.FitXy);
            AddView(SliderImageView);

            var centerText = new LinearLayout(Context);
            centerText.Orientation = Orientation.Vertical;

            CenterStatusText = new TextView(Context);
            CenterStatusText.SetTextSize(ComplexUnitType.Pt, 26);
            CenterStatusText.SetTextColor(new Color(ContextCompat.GetColor(Context, Resource.Color.OsramColor)));
            CenterStatusText.TextAlignment = TextAlignment.Center;
            CenterStatusText.Gravity = GravityFlags.Center;

            BottomStatusText = new TextView(Context);
            BottomStatusText.SetTextSize(ComplexUnitType.Pt, 7);
            //   BottomStatusText.SetTextColor(new Color(ContextCompat.GetColor(Context, Resource.Color.OsramColor)));
            BottomStatusText.TextAlignment = TextAlignment.Center;
            BottomStatusText.Gravity = GravityFlags.CenterHorizontal;


            CenterButtonUnitText = new TextView(Context);
            CenterButtonUnitText.SetTextSize(ComplexUnitType.Pt, 16);
            CenterButtonUnitText.SetTextColor(new Color(ContextCompat.GetColor(Context, Resource.Color.OsramColor)));
            CenterButtonUnitText.TextAlignment = TextAlignment.Center;
            CenterButtonUnitText.Gravity = GravityFlags.Center;


            var param = new LayoutParams(ViewGroup.LayoutParams.WrapContent, ViewGroup.LayoutParams.WrapContent);
            param.AddRule(LayoutRules.CenterInParent);
            centerText.LayoutParameters = param;
            centerText.AddView(CenterStatusText);
            centerText.AddView(CenterButtonUnitText);
            centerText.AddView(BottomStatusText);

            centerText.Clickable = true;
            centerText.Click += OnCenterStatusTextClicked;

            AddView(centerText);

            //OnOffButton
            CenterButtonLayout = new RelativeLayout(Context);
            OnOffButtonImageView = new ImageView(Context);
            OnOffButtonImageView.ContentDescription = "OnOffButton";
            OnOffButtonImageView.Clickable = true;
            OnOffButtonImageView.SetScaleType(ImageView.ScaleType.FitCenter);
            var layoutParams = new LayoutParams(ViewGroup.LayoutParams.WrapContent, ViewGroup.LayoutParams.WrapContent);
            layoutParams.AddRule(LayoutRules.CenterInParent);
            OnOffButtonImageView.LayoutParameters = layoutParams;
            OnOffButtonImageView.Touch += OnOffButtonImageViewOnTouch;
            CenterButtonLayout.AddView(OnOffButtonImageView);

            CenterButtonText = new TextView(Context);
            CenterButtonText.SetTextSize(ComplexUnitType.Pt, 25);
            CenterButtonText.SetTextColor(new Color(ContextCompat.GetColor(Context, Resource.Color.OsramColor)));
            param = new LayoutParams(ViewGroup.LayoutParams.WrapContent, ViewGroup.LayoutParams.WrapContent);
            param.AddRule(LayoutRules.CenterInParent);
            CenterButtonText.LayoutParameters = param;
            CenterButtonLayout.AddView(CenterButtonText);

            layoutParams = new LayoutParams(ViewGroup.LayoutParams.WrapContent, ViewGroup.LayoutParams.WrapContent);
            layoutParams.AddRule(LayoutRules.CenterVertical);
            CenterButtonLayout.LayoutParameters = layoutParams;

            AddView(CenterButtonLayout);

            //StatusBall
            StatusBallLayout = new RelativeLayout(Context);
            StatusBallImage = new ImageView(Context);
            StatusBallImage.LayoutParameters = new LayoutParams(ViewGroup.LayoutParams.WrapContent,
            ViewGroup.LayoutParams.WrapContent);
            StatusBallImage.SetScaleType(ImageView.ScaleType.FitXy);
            StatusBallLayout.AddView(StatusBallImage);

            BallStatusText = new TextView(Context);
            BallStatusText.Id = 1111;
            BallStatusText.ContentDescription = "Slider value id: " + BallStatusText.Id;
            BallStatusText.SetTextSize(ComplexUnitType.Pt, 11);
            BallStatusText.SetTextColor(new Color(ContextCompat.GetColor(Context, Resource.Color.OsramColor)));
            param = new LayoutParams(ViewGroup.LayoutParams.WrapContent, ViewGroup.LayoutParams.WrapContent);
            param.AddRule(LayoutRules.CenterInParent);
            BallStatusText.LayoutParameters = param;
            StatusBallLayout.AddView(BallStatusText);

            AddView(StatusBallLayout);


            InternalSetValue(MinValue);

            //UpdateSkin();
        }

        private void OnCenterStatusTextClicked(object sender, EventArgs e)
        {
            OnValueClicked?.Invoke(this, EventArgs.Empty);
        }

        private void UpdateTextColorByEnabledStaus()
        {
            var Color = Resource.Color.OsramColor; ;
            if (!IsEnabled)
            {
                Color = Resource.Color.material_grey_600;
            }
            CenterButtonUnitText.SetTextColor(new Color(ContextCompat.GetColor(Context, Color)));
            CenterButtonText.SetTextColor(new Color(ContextCompat.GetColor(Context, Color)));
            BallStatusText.SetTextColor(new Color(ContextCompat.GetColor(Context, Color)));
            CenterStatusText.SetTextColor(new Color(ContextCompat.GetColor(Context, Color)));

        }



        private void UpdateSkin()
        {
            _skin = SliderWheelUtils.GetSliderWheelSkin(ControlType);

            SliderImageView.SetImageDrawable(_skin.SliderBkImage.ToNative());

            OnOffButtonImageView.SetImageDrawable(_skin.MiddleButtonUncheckedImage.ToNative());

            StatusBallImage.SetImageDrawable(_skin.StatusBallImage.ToNative());
        }

        private void OnOffButtonImageViewOnTouch(object sender, TouchEventArgs touchEventArgs)
        {
            if (touchEventArgs.Event.Action == MotionEventActions.Down)
            {
                OnMiddleButtonDown();
            }
            else if (touchEventArgs.Event.Action == MotionEventActions.Up)
            {
                OnMiddleButtonUp();
            }
        }

        private void OnMiddleButtonUp()
        {
            OnOffButtonImageView.SetImageDrawable(IsChecked
                ? _skin.MiddleButtonCheckedImage.ToNative()
                : _skin.MiddleButtonUncheckedImage.ToNative());

            OnButtonClicked?.Invoke(this, new EventArgs());
        }

        private void OnMiddleButtonDown()
        {
            OnOffButtonImageView.SetImageDrawable(_skin.MiddleButtonPressedImage.ToNative());
        }


        private static ViewGroup FindChildOfType<T>(ViewGroup parent)
        {
            if (parent.ChildCount == 0)
                return null;

            for (var i = 0; i < parent.ChildCount; i++)
            {
                var child = parent.GetChildAt(i) as ViewGroup;

                if (child == null)
                    continue;

                var type = child.GetType();

                if (child is T)
                {
                    return child;
                }

                var result = FindChildOfType<T>(child);
                if (result != null)
                    return result;
            }

            return null;
        }

        public override bool OnTouchEvent(MotionEvent motionEvent)
        {
            if (IsEnabled)
            {

                var index = motionEvent.ActionIndex;
                var x = motionEvent.GetX(index);
                var y = motionEvent.GetY(index);

                _currentPosition = new PointF(x, y);

                _currentAngle =
                    (int)SliderWheelUtils.GetTouchDegrees(x, y, Center.X, Center.Y, SliderWheelConsts.Clockwise);

                //Logger.Trace("x[" + x + "] y[" + y + "] cx[" + Center.X + "] cy[" + Center.Y + "] angle[" + _currentAngle + "]");

                Area currentArea;
                // Mvx.Resolve<TabGestureService>().SetGestureEnabled(false);
                MainActivity main = CrossCurrentActivity.Current.Activity as MainActivity;
                // { Xamarin.Forms.Platform.Android.ViewRenderer<Xamarin.Forms.CarouselView, Android.Support.V7.Widget.RecyclerView>}
                var viewGroup = (ViewGroup)((ViewGroup)main.FindViewById(Android.Resource.Id.Content)).GetChildAt(0);

                //Xamarin.Forms.Platform.Android.ViewRenderer<Xamarin.Forms.CarouselView, Android.Support.V7.Widget.RecyclerView> _currentViewPager = FindChildOfType<Xamarin.Forms.Platform.Android.ViewRenderer<Xamarin.Forms.CarouselView, Android.Support.V7.Widget.RecyclerView>>(viewGroup);
                ViewGroup _currentViewPager = FindChildOfType<ViewPager>(viewGroup);
                switch (motionEvent.Action)
                {
                    case MotionEventActions.Down:
                        //var type = _currentViewPager.GetType();
                        //type.InvokeMember("EnableGesture",
                        // BindingFlags.Instance | BindingFlags.Public | BindingFlags.SetProperty,
                        //Type.DefaultBinder, _currentViewPager, new object[] { false });
                        if (_currentViewPager is CarouselView.FormsPlugin.Android.HorizontalViewPager)
                        {
                            ((CarouselView.FormsPlugin.Android.HorizontalViewPager)_currentViewPager).SetPagingEnabled(false);
                        }

                        _dragging = true;
                        currentArea = GetCurrentArea();

                        if (ControlType == SliderWheelType.OnOffSlider || ControlType == SliderWheelType.SaveSlider)
                        {
                            CenterButtonLayout.Visibility = ViewStates.Invisible;
                            CenterStatusText.Visibility = (CenterButtonLayout.Visibility == ViewStates.Invisible)
                                ? ViewStates.Visible
                                : ViewStates.Invisible;
                        }

                        if (currentArea == Area.SliderArea)
                        {
                            UpdateValueFromSlider(_currentAngle);
                        }
                        _previousArea = currentArea;
                        break;
                    case MotionEventActions.Move:
                        _currentViewPager.Enabled = false;
                        currentArea = GetCurrentArea();

                        if (_previousArea == Area.NoKnownArea && currentArea == Area.SliderArea)
                        {
                            if (_previousArea == Area.SliderArea)
                            {
                                UpdateValueFromSlider(_currentAngle);
                            }
                            _previousArea = currentArea;
                        }
                        else if (_previousArea == Area.SliderArea)
                        {
                            UpdateValueFromSlider(_currentAngle, true);
                        }
                        break;
                    case MotionEventActions.Up:
                        _previousArea = Area.NoKnownArea;
                        _currentAngle = -1;
                        _dragging = false;

                        if (ControlType == SliderWheelType.OnOffSlider || ControlType == SliderWheelType.SaveSlider)
                        {
                            CenterButtonLayout.Visibility = ViewStates.Visible;
                            CenterStatusText.Visibility = (CenterButtonLayout.Visibility == ViewStates.Invisible)
                                ? ViewStates.Visible
                                : ViewStates.Invisible;
                        }


                        //_currentViewPager.GetType().InvokeMember("EnableGesture",
                        //BindingFlags.Instance | BindingFlags.Public | BindingFlags.SetProperty,
                        //Type.DefaultBinder, _currentViewPager, new object[] { true });
                        if (_currentViewPager is CarouselView.FormsPlugin.Android.HorizontalViewPager)
                        {
                            ((CarouselView.FormsPlugin.Android.HorizontalViewPager)_currentViewPager).SetPagingEnabled(true);
                        }
                        break;
                }
            }

            return true;
        }

        protected override void OnSizeChanged(int w, int h, int oldw, int oldh)
        {
            ChangeSize(w, h, oldw, oldh);
            base.OnSizeChanged(w, h, oldw, oldh);
        }

        public void ChangeSize(int w, int h, int oldw, int oldh)
        {
            var scale = w / (_skin.SliderBkImage.ToNative().IntrinsicWidth * 1.0);

            _controlWidth = w;
            _ballWidth = (int)(_skin.StatusBallImage.ToNative().IntrinsicWidth * scale);
            _onoffButtonWidth = (int)(_skin.MiddleButtonCheckedImage.ToNative().IntrinsicWidth * scale);

            CenterButtonLayout.ScaleX = (float)scale;
            CenterButtonLayout.ScaleY = (float)scale;

            StatusBallLayout.LayoutParameters.Width = _ballWidth;
            StatusBallLayout.LayoutParameters.Height = _ballWidth;
            if (w < 700)
            {
                CenterStatusText.SetTextSize(ComplexUnitType.Pt, 17);
            }
            else
            {
                CenterStatusText.SetTextSize(ComplexUnitType.Pt, 26);
            }
            UpdateSlider();
        }

        private Area GetCurrentArea()
        {
            var distanceFromCenter = SliderWheelUtils.GetDistanceBetweenPoints(_currentPosition.X, _currentPosition.Y,
                Center.X, Center.Y);

            if (ControlType == SliderWheelType.OnOff || ControlType == SliderWheelType.OnOffSlider ||
                ControlType == SliderWheelType.SaveSlider)
            {
                if (_onoffButtonWidth / 2 >= distanceFromCenter)
                {
                    return Area.ButtonArea;
                }
            }
            if (ControlType == SliderWheelType.OnOffSlider || ControlType == SliderWheelType.Slider ||
                ControlType == SliderWheelType.SaveSlider)
            {
                if (_controlWidth / 2 >= distanceFromCenter && _onoffButtonWidth / 2 <= distanceFromCenter)
                {
                    if (SliderWheelConsts.Clockwise)
                    {
                        if (((_currentAngle - SliderWheelConsts.StartAngle + 360) % 360) <= SliderWheelConsts.SliderAngle)
                        {
                            return Area.SliderArea;
                        }
                        return Area.NoKnownArea;
                    }
                }
            }

            return Area.NoKnownArea;
        }

        private void UpdateValueFromSlider(int angle, bool limit = false)
        {
            var newValue = SliderWheelUtils.SliderAngleToValue(angle, SliderWheelConsts.StartAngle,
                SliderWheelConsts.SliderAngle, SliderWheelConsts.MinValue, SliderWheelConsts.MaxValue,
                SliderWheelConsts.Clockwise);

            if (limit && Math.Abs(Value - newValue) > SliderWheelConsts.MaxValueChangeAllowed)
            {
                if (newValue - Value < 0)
                {
                    newValue = 100;
                }
                else
                {
                    return;
                }
            }

            InternalSetValue(newValue);

            OnValueChanged?.Invoke(this, EventArgs.Empty);
        }

        private void UpdateSlider()
        {
            var angle = SliderWheelUtils.ValueToSliderAngle(Value, SliderWheelConsts.StartAngle,
                SliderWheelConsts.SliderAngle, SliderWheelConsts.MinValue, SliderWheelConsts.MaxValue,
                SliderWheelConsts.Clockwise);

            var radix = Width / 2;

            var rads = angle * Math.PI / 180 + Math.PI / 2;
            var halfSliderTrack = (Width - _onoffButtonWidth) / 4;
            var x = Math.Cos(rads) * (Width / 2 - halfSliderTrack);
            var y = Math.Sin(rads) * (Width / 2 - halfSliderTrack);

            StatusBallLayout.TranslationX = (radix - (float)x - _ballWidth / 2);
            StatusBallLayout.TranslationY = (radix - (float)y - _ballWidth / 2);
        }

        private void UpdateBallText()
        {
            if (BallStatusText != null)
                BallStatusText.Text = Value + "%";


        }

        private void UpdateCenterText()
        {
            if (CenterStatusText != null)
            {
                //if (Value == MinValue)
                //{
                //    CenterStatusText.Text = "MIN";
                //}
                //else if (Value == MaxValue)
                //{
                //    CenterStatusText.Text = "MAX";
                //}
                //else
                {
                    CenterStatusText.Text = OutputValue.ToString();
                    CenterButtonUnitText.Text = OutputUnit;
                    BottomStatusText.Text = SecondOutputValue;
                }
            }
        }
    }
}
