﻿
using Android.Views;
using Android.Widget;
using Android.Graphics;
using Android.Content;
using System;
using Android.Graphics.Drawables;
using Android.Support.V4.Content;

namespace Osram.T4TField.Droid.Controls
{
    public class BarIndicator : LinearLayout, GestureDetector.IOnGestureListener
    {

        private GestureDetector gestureDetector;
        private TextView textBar;
        private string id;
        private Color Color;

        private int _MinValue = 0;
        private int _MaxValue = 100;


        private int[] loc;

        private int min_y;
        private int max_y;

        private int current = 100;
        public int Current
        {
            get
            {
                return current;
            }

            set
            {
                current = value;
                if (IsValid)
                    SetHeightAndText(MeasuredWidth);
            }

        }



        public int LimitedMinValue { get; set; }
        public int LimitedMaxValue { get; set; }
        public int DefaultValue { get; set; }

        private bool isValid;
        public bool IsValid
        {
            get { return isValid; }
            set
            {
                isValid = value;
                if (!isValid)
                    NotValidBarValue();
                else
                    //restore current value
                    SetHeightAndText(MeasuredWidth);
            }
        }

        private bool isEnabled;
        public bool IsEnabled
        {
            get { return isEnabled; }
            set
            {
                isEnabled = value;
                if (isEnabled)
                {
                    EnableBar();
                }
                else
                {
                    DisableBar();
                }
            }
        }


        public Action<int> valueChange;
        public Action OnScrollFinished;

        public BarIndicator(Context context, string automationId, int currentValue, int min, int max, int defaultValue, bool isBarEnabled, bool isValid, Color color) : base(context)
        {
            this.SetGravity(GravityFlags.Bottom);
            this.Current = currentValue;
            this.LimitedMinValue = min;
            this.LimitedMaxValue = max;
            this.DefaultValue = defaultValue;
            this.IsEnabled = isBarEnabled;
            this.IsValid = isValid;
            this.id = automationId + "_Value";
            textBar = InitTextBar();
            AddView(textBar);
            gestureDetector = new GestureDetector(Context, this);
            Color = color;
        }


        public TextView InitTextBar()
        {
            var textBar = new TextView(this.Context);
            textBar.TextSize = 20;
            textBar.SetTextColor(Color.White);
            textBar.Id = 10;
            textBar.ContentDescription = "Sliding bar value";
            textBar.TextAlignment = TextAlignment.Center;
            textBar.Gravity = GravityFlags.Center;
            textBar.SetBackgroundColor(GetColor());
            return textBar;
        }

        public void SetHeightAndText(int width)
        {
            if (textBar != null)
            {
                textBar.SetWidth(width);

                var lh = textBar.LineHeight;

                if (loc == null)
                {
                    loc = new int[2];
                    textBar.GetLocationOnScreen(loc);
                    max_y = loc[1];
                    min_y = (max_y - textBar.Top) - lh;
                }

                float tot_h = max_y - min_y;
                var parent = textBar.Top;
                var stepDim = tot_h / (_MaxValue);

                var height = current * stepDim;
                var alpha = (height * 200 / tot_h) + 55;

                if (_MinValue < 11 && current < 11)
                {
                    alpha = alpha / 100 * 50;
                    if (current < _MinValue)
                    {
                        current = _MinValue;
                    }
                    height = ((int)stepDim * (10));
                }

                textBar.Text = Java.Lang.Math.Round(current) + "";
                textBar.SetHeight((int)height);
                if (IsValuesInLimits() || current == DefaultValue)
                    textBar.SetBackgroundColor(GetColor((int)alpha));
            }
        }


        public override bool OnTouchEvent(MotionEvent e)
        {
            if (!IsEnabled)
                return false;

            Console.WriteLine("BarIndicator gesture action: {0}", e.Action.ToString());
            if (gestureDetector.OnTouchEvent(e))
            {
                if (e.Action == MotionEventActions.Up || e.Action == MotionEventActions.Cancel)
                {
                    handleScrollFinished();

                }
                return true;
            }
            if (e.Action == MotionEventActions.Up || e.Action == MotionEventActions.Cancel)
            {
                handleScrollFinished();
            }
            return false;
        }

        private void handleScrollFinished()
        {
            OnScrollFinished?.Invoke();
        }

        public bool OnDown(MotionEvent e)
        {

            return true;
        }

        public bool OnFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY)
        {
            return true;
        }

        public void OnLongPress(MotionEvent e)
        {





        }

        public bool OnScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY)
        {

            var lh = textBar.LineHeight;
            if (loc == null)
            {
                loc = new int[2];
                textBar.GetLocationOnScreen(loc);
                max_y = loc[1];
                min_y = (max_y - textBar.Top) - lh;
            }

            float t_pos = e2.RawY;
            float tot_h = max_y - min_y;

            var parent = textBar.Top;
            var stepDim = tot_h / (_MaxValue);

            if (t_pos > min_y && t_pos < max_y)
            {
                var new_h = max_y - t_pos;
                //var height = new_h;
                var value = (new_h * 100 / tot_h);
                var alpha = (new_h * 200 / tot_h) + 55;
                var height = (value) * stepDim;
                var roundedValue = Java.Lang.Math.Round(value);
                Color backgroundColor = GetColor((int)alpha);

                if (_MinValue < 11 && value < 11)
                {
                    alpha = alpha / 100 * 50;
                    if (value > _MinValue)
                    {
                        textBar.Text = Convert.ToString(roundedValue);
                    }
                    else
                    {
                        textBar.Text = Convert.ToString(_MinValue);
                    }
                    textBar.SetHeight((int)stepDim * (10));

                }
                else if (value > _MinValue)
                {
                    textBar.SetHeight((int)height);
                    textBar.Text = Convert.ToString(roundedValue);
                    if (value > _MaxValue - 1)
                    {
                        textBar.Text = Convert.ToString(_MaxValue);
                        textBar.SetHeight((int)tot_h);
                    }
                }
                //else
                //{
                //    textBar.Text = Convert.ToString(_MinValue);
                //    textBar.SetHeight((int)stepDim * (_MinValue));
                //}

                if (!IsValuesInLimits() && Convert.ToInt16(textBar.Text) != DefaultValue)
                {
                    backgroundColor = Color.DarkRed;
                }
                textBar.SetBackgroundColor(backgroundColor);
                valueChange(roundedValue);
            }
            return true;
        }

        private bool IsValuesInLimits()
        {
            var value = Convert.ToInt16(textBar.Text);
            if (value > LimitedMaxValue || value < LimitedMinValue)
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        public void OnShowPress(MotionEvent e)
        {

        }

        public bool OnSingleTapUp(MotionEvent e)
        {

            var height = max_y - min_y;
            var touchedSize = height * 0.30;

            var touchedPos = e.RawY;
            Color background = GetColor();
            bool upperTap = touchedPos < min_y + touchedSize;
            bool bottomTap = touchedPos > max_y - touchedSize;

            if (bottomTap || upperTap)
            {
                // long press on bottom part of the bar
                if (bottomTap)
                {
                    int alpha = 25;
                    textBar.Text = Convert.ToString(DefaultValue);
                    textBar.SetHeight((height / 100) * (10));
                    valueChange(DefaultValue);
                    background = GetColor(alpha);
                }

                // long press on Uppper part of the bar
                if (upperTap)
                {
                    if (current == LimitedMaxValue)
                        return true;

                    float tot_h = max_y - min_y;
                    var parent = textBar.Top;
                    var stepDim = tot_h / (_MaxValue);

                    height = LimitedMaxValue * (int)stepDim;
                    int alpha = (int)(height * 200 / tot_h) + 55;

                    textBar.Text = Convert.ToString(LimitedMaxValue);
                    textBar.SetHeight(height);
                    valueChange(LimitedMaxValue);
                    background = GetColor(alpha);
                }

                if (!IsValuesInLimits() && Convert.ToInt16(textBar.Text) != DefaultValue)
                {
                    background = Color.DarkRed;
                }
                textBar.SetBackgroundColor(background);
            }
            return true;
        }

        private void EnableBar()
        {
            textBar?.SetBackgroundColor(Color);
        }

        private void DisableBar()
        {
            textBar?.SetBackgroundColor(Color.Rgb(221, 221, 221));
        }

        private void NotValidBarValue()
        {
            if (textBar != null)
            {
                textBar.SetBackgroundColor(Color.Rgb(238, 238, 238));
                textBar.Text = string.Empty;
            }
        }

        private Color GetColor(int? alpha = null)
        {
            if (IsEnabled)
            {
                // return Color.Argb((int)alpha, 242, 101, 36);
                return new Color(Color.R, Color.G, Color.B, (byte)(alpha ?? 255));

            }
            else
            {
                return Color.Argb(255, 221, 221, 221);
            }
        }
    }
}