using System;
using Android.Content;
using Android.Graphics;
using Android.Runtime;
using Android.Util;
using Android.Views;
using Android.Widget;

namespace Osram.T4TField.Droid.Controls
{
    public class SquareLayout : RelativeLayout
    {
        protected SquareLayout(IntPtr javaReference, JniHandleOwnership transfer)
            : base(javaReference, transfer)
        {
        }

        public SquareLayout(Context context)
            : base(context)
        {
        }

        public SquareLayout(Context context, IAttributeSet attrs)
            : base(context, attrs)
        {
        }

        public SquareLayout(Context context, IAttributeSet attrs, int defStyle)
            : base(context, attrs, defStyle)
        {
        }

        protected PointF Center
        {
            get { return new PointF(Width/2, Height/2); }
        }

        protected override void OnMeasure(int widthMeasureSpec, int heightMeasureSpec)
        {
            var width = MeasureSpec.GetSize(widthMeasureSpec);
            var height = MeasureSpec.GetSize(heightMeasureSpec);

            if (height == 0 && width == 0)
            {
                base.OnMeasure(widthMeasureSpec, heightMeasureSpec);
                return;
            }

            if (height == 0)
            {
                height = width;
            }
            else if (width == 0)
            {
                width = height;
            }

            if (width > height)
                width = height;
            else
                height = width;

            base.OnMeasure(
                MeasureSpec.MakeMeasureSpec(width, MeasureSpecMode.Exactly),
                MeasureSpec.MakeMeasureSpec(height, MeasureSpecMode.Exactly)
                );
        }
    }
}