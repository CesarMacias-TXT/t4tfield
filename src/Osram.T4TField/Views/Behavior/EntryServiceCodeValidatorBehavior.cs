﻿using Osram.TFTBackend;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace Osram.T4TField.Views.Behavior
{
    
    public class EntryServiceCodeValidatorBehavior : Behavior<Entry>
    {
        public int MaxValue { get; set; }
        public int MaxLength { get; set; }


        protected override void OnAttachedTo(Entry bindable)
        {
            base.OnAttachedTo(bindable);
            bindable.TextChanged += OnEntryTextChanged;
           
        }

        protected override void OnDetachingFrom(Entry bindable)
        {
            base.OnDetachingFrom(bindable);
            bindable.TextChanged -= OnEntryTextChanged;
        }

        void OnEntryTextChanged(object sender, TextChangedEventArgs e)
        {
            var entry = (Entry)sender;
            var value = entry.Text;
            entry.TextChanged -= OnEntryTextChanged;
            try {
                if (entry.Text.Length > this.MaxLength)
                {
                    string entryText = entry.Text;
                    entry.Text = e.OldTextValue;
                }
                else if (e.NewTextValue.Equals(""))
                {
                    entry.Text = "0";
                }
                else if (int.Parse(entry.Text) > this.MaxValue)
                {
                    entry.Text = this.MaxValue.ToString();
                }
            }
            catch (Exception ex)
            {
                entry.Text = e.OldTextValue;
                Logger.Exception("EntryServiceCodeValidatorBehavior.OnEntryTextChanged", ex);
            }
            entry.TextChanged += OnEntryTextChanged;
        }
    }
}
