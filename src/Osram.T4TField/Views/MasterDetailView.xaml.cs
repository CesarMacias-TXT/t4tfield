﻿using MvvmCross.ViewModels;
using MvvmCross.Views;
using Osram.T4TField.ViewModels;
using Xamarin.Forms;

namespace Osram.T4TField.Views
{
    public partial class MasterDetailView : IMvxView
    {
        public IMvxViewModel ViewModel { get; set; }
        public object DataContext { get; set; }

        public MasterDetailView()
        {
            InitializeComponent();
        }

        protected override void OnBindingContextChanged()
        {
            base.OnBindingContextChanged();

            SetBinding(TitleProperty, new Binding("Title"));

            var vm = BindingContext as BaseViewModel;
            if (vm != null)
                vm.Loaded();
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();

            var vm = BindingContext as BaseViewModel;
            if (vm == null) return;

            vm.Resumed();
        }
    }
}