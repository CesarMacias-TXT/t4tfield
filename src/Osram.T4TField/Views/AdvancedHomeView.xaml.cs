﻿using MvvmCross;
using Osram.T4TField.Contracts;
using Xamarin.Forms;
using Osram.T4TField.Controls;

namespace Osram.T4TField.Views
{
    public partial class AdvancedHomeView
    {
        bool IsGridToBeCreate = true;
        public AdvancedHomeView()
        {
            InitializeComponent();
            //PageNumber.TextColor = Styles.OsramColor;
            SlideMenu = new RightSideMasterPage();
            GrdView.ItemTemplate = typeof(FeatureButton);
        }
         
        protected override bool OnBackButtonPressed()
        {
            if (Device.RuntimePlatform == Device.Android)
            {
                Mvx.IoCProvider.Resolve<IAndroidUtils>().CloseApp();
            }
            return base.OnBackButtonPressed();
        }

        protected override void OnAppearing()
        {
            if (IsGridToBeCreate)
            {
               GrdView.BuildTiles();
                IsGridToBeCreate = false;
            }
         
            base.OnAppearing();
        }

        private void OnButtonCommand()
        {
           // throw new NotImplementedException();
        }
    }

}