﻿using CarouselView.FormsPlugin.Abstractions;
using MvvmCross;
using MvvmCross.Plugin.Messenger;
using Osram.T4TField.Messaging;
using Osram.T4TField.ViewModels.FeaturesViewModel;
using Osram.TFTBackend.DataModel.Feature.Contracts;

namespace Osram.T4TField.Views
{
    public partial class FeaturesCarouselView : SelectableView
    {
        private MvxSubscriptionToken _astroModeToken;
        private IMvxMessenger _messenger;

        public FeaturesCarouselView()
        {
            InitializeComponent();
            _messenger = Mvx.IoCProvider.Resolve<IMvxMessenger>();
        }

        private void OnChangeAstroDimMode(Events.SelectedAstroModeEvent ev)
        {
            if (ev.Mode == AstroDimMode.AstroBased)
            {
                FeaturesCarousel.Position = 0;
                if (IsAdvancedMode)
                {
                    //FeaturesCarousel.InsertPage(Mvx.IocConstruct<AstroDimAstroBasedViewModel>(), 1);
                    FeaturesCarousel.ItemsSource.Add(Mvx.IoCProvider.IoCConstruct<AstroDimAstroBasedViewModel>());
                }
            }
            else if (ev.Mode == AstroDimMode.TimeBased)
            {
                FeaturesCarousel.Position = 0;
                if (IsAdvancedMode && FeaturesCarousel.ItemsSource.GetCount() > 2)
                {
                    //FeaturesCarousel.RemovePage(1);
                    FeaturesCarousel.ItemsSource.RemoveAt(1);
                }
            }
        }

        protected override void OnDisappearing()
        {
            _astroModeToken.Dispose();
            base.OnDisappearing();
        }

        protected override void OnBindingContextChanged()
        {
            HeaderCarousel.BindingContext = BindingContext;
            base.OnBindingContextChanged();
        }

        private void OnChidAddedd()
        {

        }

        protected override void OnAppearing()
        {
            _astroModeToken = _messenger.Subscribe<Events.SelectedAstroModeEvent>(OnChangeAstroDimMode);
            base.OnAppearing();
        }
    }
}



