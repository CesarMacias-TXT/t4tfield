﻿using Xamarin.Forms;

namespace Osram.T4TField.Views
{
    public partial class LoginView
    {
        public LoginView()
        {
            InitializeComponent();
            NavigationPage.SetHasNavigationBar(this, false);
        }


        protected override bool OnBackButtonPressed()
        {
            return true;
        }
    }
}
