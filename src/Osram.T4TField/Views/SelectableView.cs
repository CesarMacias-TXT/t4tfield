﻿using Osram.T4TField.ViewModels;
using Osram.TFTBackend.DataModel.Feature.Contracts;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;


namespace Osram.T4TField.Views
{   


    public partial class SelectableView : BaseView
    {

        public static readonly BindableProperty SwitchCommandCommandProperty = BindableProperty.Create(nameof(SwitchCommand), typeof(ICommand), typeof(SelectableView));
        public string SwitchCommand
        {
            get
            {
                return (string)GetValue(SwitchCommandCommandProperty);
            }
            set
            {
                SetValue(SwitchCommandCommandProperty, value);
            }
        }

        public static readonly BindableProperty IsAdvancedModeProperty = BindableProperty.Create(nameof(IsAdvancedMode), typeof(bool), typeof(SelectableView), false);
        public bool IsAdvancedMode
        {
            get
            {
                return (bool) GetValue(IsAdvancedModeProperty);
            }
            set
            {
                SetValue(IsAdvancedModeProperty, value);
            }
        }


        //TODO refactor ImageButton with specific type
        public static readonly BindableProperty ListOfFeaturesProperty = BindableProperty.Create(nameof(ListOfFeatures), typeof(ObservableCollection<FeatureInfoButton>), typeof(SelectableView));
        public ObservableCollection<FeatureInfoButton> ListOfFeatures
        {
            get
            {
                return (ObservableCollection<FeatureInfoButton>)GetValue(ListOfFeaturesProperty);
            }
            set
            {
                SetValue(ListOfFeaturesProperty, value);
            }
        }


        public static readonly BindableProperty SelectedFeatureProperty = BindableProperty.Create(nameof(SelectedFeature), typeof(FeatureType), typeof(SelectableView), FeatureType.Default
            );
        public FeatureType SelectedFeature
        {
            get
            {
                return (FeatureType)GetValue(SelectedFeatureProperty);
            }
            set
            {
                SetValue(SelectedFeatureProperty, value);
            }
        }


    }
}
