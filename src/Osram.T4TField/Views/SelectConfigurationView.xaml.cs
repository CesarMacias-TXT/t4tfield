﻿using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;
using Osram.T4TField.ViewModels;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Osram.T4TField.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class SelectConfigurationView : PopupView
    {
        public SelectConfigurationView()
        {
            InitializeComponent();
        }

        private void ListView_OnItemTapped(object sender, ItemTappedEventArgs e)
        {
            SelectConfigurationViewModel vm = BindingContext as SelectConfigurationViewModel;
            vm.SelectItemCommand.Execute(e.Item);
        }
    }
}