﻿namespace Osram.T4TField.Views
{
    public partial class ImprintView
    {
        public ImprintView()
        {
            InitializeComponent();
        }

        protected override void OnBindingContextChanged()
        {
            base.OnBindingContextChanged();
            //has to be bind manually
            ImprintWebView.BindingContext = BindingContext;
        }
    }
}