﻿using Osram.T4TField.ViewModels;
using Osram.T4TField.ViewModels.FeaturesViewModel;
using Osram.T4TField.Views.FeaturesView;
using Xamarin.Forms;

namespace Osram.T4TField.Views.TemplateSelectors
{
    public class FeaturesTemplateSelector : DataTemplateSelector
    {
        //private readonly DataTemplate ReferenceScheduleView;
        //private readonly DataTemplate FadeTimingView;
        private readonly DataTemplate TuningFactorView;
        private readonly DataTemplate AstroDIMView;
        private readonly DataTemplate AstroDimAstroBasedView;
        private readonly DataTemplate AstroDimInformationView;
        private readonly DataTemplate ConstantLumenView;

        public FeaturesTemplateSelector()
        {
            AstroDimAstroBasedView = new DataTemplate(typeof(AstroDimAstroBasedView));
            AstroDIMView = new DataTemplate(typeof(AstroDimView));

            AstroDimInformationView = new DataTemplate(typeof(AstroDimInformationView));           
            //this.ReferenceScheduleView = new DataTemplate(typeof(ReferenceScheduleView));
            TuningFactorView = new DataTemplate(typeof(TuningFactorView));
            //this.FadeTimingView = new DataTemplate(typeof(ReferenceScheduleView));

            ConstantLumenView = new DataTemplate(typeof(ConstantLumenView));
        }

        protected override DataTemplate OnSelectTemplate(object item, BindableObject container)
        {
            switch (item)
            {
                case AstroDimViewModel _:
                    return AstroDIMView;
                case AstroDimInformationViewModel _:
                    return AstroDimInformationView;
                case TuningFactorViewModel _:
                    return TuningFactorView;
                case AstroDimAstroBasedViewModel _:
                    return AstroDimAstroBasedView;
                case ConstantLumenViewModel _:
                    return ConstantLumenView;
                default:
                    return null;
            }
        }
    }
}
