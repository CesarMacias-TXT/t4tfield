﻿
using Osram.T4TField.Controls.ListCellView;
using Xamarin.Forms;


namespace Osram.T4TField.Views.TemplateSelectors
{
    public class HeaderCarouselTemplateSelector : DataTemplateSelector
    {
        public DataTemplate AstroModeTemplate { get; set; }
       
        public HeaderCarouselTemplateSelector()
        {

           // AstroModeTemplate
        }


        protected override DataTemplate OnSelectTemplate(object item, BindableObject container)
        {
            var pippo = container.BindingContext;
            try
            {
                var header = (HeaderCarouselObject)item;
                switch (header.type)
                {
                    case 
                         HeaderCarousel.AstroMode:
                         return AstroModeTemplate;
                    default: throw new System.Exception("HeaderCarousel not found!");
                }
            }catch
            {
                throw new System.Exception("HeaderCarousel not exist!");
            }
        }
    }


    public class HeaderCarouselObject
    {

        public HeaderCarousel type { get; set; }
        public bool IsEditable { get; set;  }
    }


    public enum HeaderCarousel
    {

     AstroMode
    }
}
