﻿using Osram.T4TField.Controls;
using Osram.T4TField.ViewModels;
using System;
using System.Collections.Generic;
using static Osram.T4TField.Controls.XFormNumberPicker;

namespace Osram.T4TField.Views.FeaturesView
{
    public partial class TuningFactorPopupView : PopupView
    {
        private int Value;
        private int? MinValue;
        private int? MaxValue;

        List<XFormNumberPicker> Values;

        public TuningFactorPopupView()
        {
            InitializeComponent();

            Values = new List<XFormNumberPicker>();
        }

        protected override void OnAppearing()
        {
            TuningFactorPopupViewModel vm = BindingContext as TuningFactorPopupViewModel;
            Value = vm.Value;
            MinValue = vm.MinValue;
            MaxValue = vm.MaxValue;
            CreatePickers();

            base.OnAppearing();
        }

        void CreatePickers()
        {
            var maxValueTemp = MaxValue;
            var valueTemp = Value;

            while (maxValueTemp > 0)
            {
                var lastNumber = valueTemp % 10;
                valueTemp /= 10;

                int max = 0;
                var pow = Math.Pow(10, Values.Count);
                for (var i = 1; i <= 9; i++)
                {
                    if (i * pow > MaxValue)
                    {
                        break;
                    }
                    max = i;
                }

                XFormNumberPicker picker = new XFormNumberPicker
                {
                    Min = 0,
                    Max = max,
                    Current = lastNumber,
                    Type = PickerType.Number
                };
                picker.PropertyChanged += Picker_PropertyChanged;

                Values.Add(picker);

                maxValueTemp /= 10;
            }

            for (var i = Values.Count - 1; i >= 0; i--)
            {
                ValueContainer.Children.Add(Values[i]);
            }

            // remove first child beacuse it is used only for allocating space
            ValueContainer.Children.RemoveAt(0);
        }

        private void Picker_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            var temp = 0d;
            for (var i = 0; i < Values.Count; i++)
            {
                temp += Values[i].Current * Math.Pow(10, i);
            }

            (BindingContext as TuningFactorPopupViewModel).Value = (int)temp;
        }
    }
}
