﻿using MvvmCross;
using Osram.T4TField.Contracts;
using Xamarin.Forms;

namespace Osram.T4TField.Views
{
    public partial class BasicHomeView
    {
        public BasicHomeView()
        {
            InitializeComponent();
            //PageNumber.TextColor = Styles.OsramColor;
            SlideMenu = new RightSideMasterPage();
        }

        protected override bool OnBackButtonPressed()
        {
            if (Device.RuntimePlatform == Device.Android)
            {
                Mvx.IoCProvider.Resolve<IAndroidUtils>().CloseApp();
            }
            return base.OnBackButtonPressed();
        }
    }
}