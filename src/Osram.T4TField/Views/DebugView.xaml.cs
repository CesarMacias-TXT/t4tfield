﻿using Xamarin.Forms.Xaml;

namespace Osram.T4TField.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class DebugView : BaseView
    {
        public DebugView()
        {
            InitializeComponent();
        }
    }
}
