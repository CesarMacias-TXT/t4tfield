﻿using MvvmCross.ViewModels;
using MvvmCross.Views;
using Rg.Plugins.Popup.Pages;

namespace Osram.T4TField.Views
{
    public class PopupView : PopupPage, IMvxView
    {
        public IMvxViewModel ViewModel { get; set; }
        public object DataContext { get; set; }
    }
}
