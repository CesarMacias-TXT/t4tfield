﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Osram.T4TField.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class SearchingBTDeviceView 
    {
        public SearchingBTDeviceView()
        {
            InitializeComponent();
        }


       

    }

    public class InvertBoolConverter : IValueConverter
        {
            

            public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
            {
                return !((bool)value);
            }

         

            public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
            {
                return null;
            }
        }
}
