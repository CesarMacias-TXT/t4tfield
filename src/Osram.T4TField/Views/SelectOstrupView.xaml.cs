﻿using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Osram.T4TField.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class SelectOstrupView
    {
        public ObservableCollection<string> Items { get; set; }

        public SelectOstrupView()
        {
            InitializeComponent();
        }
    }
}