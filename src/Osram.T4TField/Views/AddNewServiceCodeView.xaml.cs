﻿using Rg.Plugins.Popup.Pages;
using Rg.Plugins.Popup.Services;
using System;
using Osram.T4TField.ViewModels;
using Xamarin.Forms;

namespace Osram.T4TField.Views
{
    public partial class AddNewServiceCodeView : PopupView
    {
        public AddNewServiceCodeView()
        {
            InitializeComponent();
        }

        protected override bool OnBackgroundClicked()
        {
            CloseProgrammingViewModel();
            return base.OnBackgroundClicked();
        }

        private void CloseProgrammingViewModel()
        {
            var vm = BindingContext as ProgrammingViewModel;
            vm?.Close();
            BindingContext = null;
            Content = null;
            GC.Collect();
        }

        protected override bool OnBackButtonPressed()
        {
            CloseProgrammingViewModel();
            return base.OnBackButtonPressed();
        }
    }
}
