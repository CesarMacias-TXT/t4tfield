using System;
using Osram.T4TField.ViewModels;
using Osram.TFTBackend.NfcService.NfcDataTypes;

namespace Osram.T4TField.Models
{
    public class NFCInterface : BaseViewModel
    {
        public string Name { get; set; }

        private string configurationDescriptor;
        public string ConfigurationDescriptor
        { 
             get { return configurationDescriptor; }
             set { configurationDescriptor = value;
                RaisePropertyChanged();
             }
        }

        public string ImageSource { get; set; }

        public  bool IsConfigurable { get; set; }

        public NfcReaderType NFCInterfaceType { get; set; }
    }
}