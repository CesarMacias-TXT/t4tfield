using System.Globalization;

namespace Osram.T4TField.Models
{
    public class Language
    {
        public string Name
        {
            get { return CultureInfo.NativeName;}
        }

        public CultureInfo CultureInfo { get; set; }
    }
}