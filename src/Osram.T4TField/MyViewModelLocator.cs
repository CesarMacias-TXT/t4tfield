﻿using MvvmCross;
using MvvmCross.Exceptions;
using MvvmCross.Navigation.EventArguments;
using MvvmCross.ViewModels;
using MvvmCross.Views;
using Osram.T4TField.ViewModels;
using Osram.T4TField.ViewPresenter;
using System;
using System.Collections.Generic;
using System.Text;

namespace Osram.T4TField
{
    public class MyViewModelLocator
  : MvxDefaultViewModelLocator
    {
       

        public override IMvxViewModel Load(Type viewModelType, IMvxBundle parameterValues, IMvxBundle savedState, IMvxNavigateEventArgs navigationArgs) {
            if (viewModelType == null)
                throw new ArgumentNullException(nameof(viewModelType));

            IMvxViewModel viewModel;
            try
            {
                if (Mvx.IoCProvider.CanResolve(viewModelType))
                {
                    viewModel = (IMvxViewModel)Mvx.IoCProvider.Resolve(viewModelType);
                }
                else
                {
                    viewModel = (IMvxViewModel)Mvx.IoCProvider.IoCConstruct(viewModelType);

                    Mvx.IoCProvider.RegisterSingleton(viewModelType, viewModel);
                }
               
            }
            catch (Exception exception)
            {
                throw exception.MvxWrap("Problem creating viewModel of type {0}", viewModelType.Name);
            }

            RunViewModelLifecycle(viewModel,parameterValues, savedState, navigationArgs);

            return viewModel;
        }


    }

}
