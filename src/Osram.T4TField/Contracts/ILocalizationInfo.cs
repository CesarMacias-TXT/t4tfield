﻿using System.Globalization;

namespace Osram.T4TField.Contracts
{
    public interface ILocalizationInfo
    {
        CultureInfo GetCurrentCultureInfo();

        void SetCultureInfo(CultureInfo cultureInfo);
    }
}