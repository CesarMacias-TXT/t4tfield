﻿namespace Osram.T4TField.Contracts
{
    public interface IAppInfo
    {
        string Version { get; }
        string ApplicationName { get; }
    }
}