﻿namespace Osram.T4TField.Contracts
{
    public interface ISystemUtils
    {
        void PreventDeviceSleep(bool shouldPrevent);
    }
}