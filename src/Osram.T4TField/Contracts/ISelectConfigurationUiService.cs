using System.Threading.Tasks;
using Osram.T4TField.ViewModels;
using Osram.TFTBackend.BaseTypes;
using Osram.TFTBackend.ConfigurationService.Contracts;

namespace Osram.T4TField.Contracts
{
    /// <summary>
    /// Selects a configuration from a Gtin. If more than one configuration matches the Gtin a UI is displayed
    /// for user selection.
    /// </summary>
    public interface ISelectConfigurationUiService
    {
        Task<IConfiguration> SelectConfiguration(BaseViewModel vm, EcgSelector selector);
    }
}