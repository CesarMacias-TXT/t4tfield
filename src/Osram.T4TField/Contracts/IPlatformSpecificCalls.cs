using System;

namespace Osram.T4TField.Contracts
{
    public interface IPlatformSpecificCalls
    {
        void BeginInvokeOnMainThread(Action action);
    }
}