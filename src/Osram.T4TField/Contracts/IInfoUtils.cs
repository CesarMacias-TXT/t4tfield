﻿using Osram.TFTBackend.NfcService.NfcDataTypes;

namespace Osram.T4TField.Contracts
{
    public interface IInfoUtils
    {
        string GetAppVersion();

        string GetStoreName();

        string GetLinkToStore();

        bool ExistExternalStorage();

        string GetDownloadFolderPath();

        string GetInAppBillingProductName(string name);

        NfcReaderType GetDefaultNfcReaderType();
    }
}
