﻿using System;
using MvvmCross.ViewModels;

namespace Osram.T4TField.ViewPresenter
{
    public class CloseModalPresentationHint : MvxPresentationHint
    {
        public CloseModalPresentationHint(Type viewModelToCloseType)
        {
            ViewModelToCloseType = viewModelToCloseType;
        }

        public Type ViewModelToCloseType { get; private set; }
    }

    public class GoBackToHomePresentationHint : MvxPresentationHint
    {
    }
}