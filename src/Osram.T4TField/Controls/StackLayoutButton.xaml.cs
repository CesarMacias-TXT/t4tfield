﻿
using System.Windows.Input;
using Xamarin.Forms;
using ImageButton = Osram.T4TField.ViewModels.ImageButton;

namespace Osram.T4TField.Controls
{
    public partial class StackLayoutButton : StackLayout
    {

        public static readonly BindableProperty IsBtnEnabledProperty =
        BindableProperty.Create(nameof(IsBtnEnabled), typeof(bool), typeof(StackLayoutButton), true, BindingMode.TwoWay, propertyChanged: ((bindable, value, newValue) =>
        {
            
            var button = (StackLayoutButton)bindable;
            ImageButton currentImgBtn = (ImageButton) button.BindingContext;
            if ((bool)newValue == true){
                //SetEnableBtn(button, currentImgBtn.ImageButtonType);
                button.DisabledBox.IsVisible = false;
                button.ForceLayout();
            }
            else
            {
                button.DisabledBox.IsVisible = true;
            }
         }));

        public static readonly BindableProperty IsFeatureSelectedProperty =
        BindableProperty.Create(nameof(IsFeatureSelected), typeof(bool), typeof(StackLayoutButton), false, BindingMode.TwoWay, propertyChanged: ((bindable, value, newValue) =>
        {

            var button = (StackLayoutButton)bindable;
            ImageButton currentImgBtn = (ImageButton)button.BindingContext;
            if ((bool)newValue == true)
            {
                //SetEnableBtn(button, currentImgBtn.ImageButtonType);
                button.SelectedBox.IsVisible = true;
                button.ForceLayout();
            }
            else
            {
                button.SelectedBox.IsVisible = false;
            }
        }));

        
        public bool IsFeatureSelected
        {
            get { return (bool)GetValue(IsFeatureSelectedProperty); }
            set { SetValue(IsFeatureSelectedProperty, value); }
        }





        public Color TextColor
        {
            get { return this.Text.TextColor; }
            set { this.Text.TextColor = value; }
        }



        public bool IsBtnEnabled
        {
            get { return (bool)GetValue(IsBtnEnabledProperty); }
            set { SetValue(IsBtnEnabledProperty, value); }
        }

        public Label TextControl
        {
            get { return this.Text; }
            set { this.Text = value;}
        }

        public const string ClickCommandPropertyName = "ClickCommand";
        public static readonly BindableProperty ClickCommandProperty =
        BindableProperty.CreateAttached(
                "ClickCommand", /* string PropertyName */
                typeof(ICommand), /* Type returnType */
                typeof(StackLayoutButton), /* Type declaringType */
                null, /* default value */
                BindingMode.Default, /* defaultBindingMode */
                propertyChanged: (bindable, oldValue, newValue) => {
                    OnCommandPropertyChanged(bindable, (ICommand)oldValue, (ICommand)newValue);
                });


        private static void OnCommandPropertyChanged(BindableObject bindable, ICommand oldValue, ICommand newValue)
        {
            var source = bindable as StackLayoutButton;
            if (source == null)
            {
                return;
            }
            source.OnCommandChanged();
        }

        private void OnCommandChanged()
        {
            OnPropertyChanged("ClickCommand");
        }


      
        

        public ICommand ClickCommand
        {
            get
            {
                var command = (ICommand)GetValue(ClickCommandProperty);
                return command;
            }
            set
            {
                SetValue(ClickCommandProperty, value);
            }
        }

        public ICommand ExecuteCommand
        {
            get
            {
                return new Command(ExecuteAction());
            }
        }



        private System.Action ExecuteAction()
        {
           return new System.Action(Exec);

        }

        private void Exec()
        {      
            if (ClickCommand != null && IsBtnEnabled)
            {
                ClickCommand.Execute(null);
            }
        }

        public StackLayoutButton()
        {
            InitializeComponent();
            //InitButton(this.IsEnabled, this.ButtonType);
            TapGestureRecognizer tap = new TapGestureRecognizer();          
            tap.Command = ExecuteCommand;
            Button.GestureRecognizers.Add(tap);
           
        }




      }


   
}



