﻿using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;

namespace Osram.T4TField.Controls
{
    public class IndicatorProgressBar: ProgressBar
    {

        public static readonly BindableProperty IndeterminateProperty = BindableProperty.Create(nameof(Indeterminate), typeof(Boolean), typeof(IndicatorProgressBar), true, BindingMode.TwoWay);

        public IndicatorProgressBar()
        {
        }

        public Boolean Indeterminate
        {
             get { return (Boolean)GetValue(IndeterminateProperty); }
             set {
                SetValue(IndeterminateProperty, value);
             }

        }

    }





}
