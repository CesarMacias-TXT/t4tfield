﻿using Xamarin.Forms;
using System.Windows.Input;

namespace Osram.T4TField.Controls
{
    public class TouchedBarLayout : View
    {

        public static readonly BindableProperty CurrentProperty = BindableProperty.Create(nameof(Current), typeof(int), typeof(TouchedBarLayout), 0);
        public static readonly BindableProperty MinProperty = BindableProperty.Create(nameof(Min), typeof(int), typeof(TouchedBarLayout), 0);
        public static readonly BindableProperty MaxProperty = BindableProperty.Create(nameof(Max), typeof(int), typeof(TouchedBarLayout), 100);
        public static readonly BindableProperty DefaultProperty = BindableProperty.Create(nameof(Default), typeof(int), typeof(TouchedBarLayout), 0);
        public static readonly BindableProperty IsValidProperty = BindableProperty.Create(nameof(IsValid), typeof(bool), typeof(TouchedBarLayout), true);
        public static readonly BindableProperty OnGestureFinishedProperty =
            BindableProperty.Create(nameof(OnGestureFinished), typeof(ICommand), typeof(TouchedBarLayout), null);

        public TouchedBarLayout()
        {
           BackgroundColor = Color.FromHex("#eeeeee");
        }

        public int Min
        {
            get { return (int)GetValue(MinProperty); }
            set { SetValue(MinProperty, value); OnPropertyChanged(); }
        }

        public int Max
        {
            get { return (int)GetValue(MaxProperty); }
            set { SetValue(MaxProperty, value); OnPropertyChanged(); }
        }

        public int Default
        {
            get { return (int)GetValue(DefaultProperty); }
            set { SetValue(DefaultProperty, value); OnPropertyChanged(); }
        }

        public bool IsValid
        {
            get { return (bool)GetValue(IsValidProperty); }
            set { SetValue(IsValidProperty, value); OnPropertyChanged(); }
        }

        public int Current
        {
            get { return (int)GetValue(CurrentProperty); }
            set
            {
                SetValue(CurrentProperty, value);
                OnPropertyChanged();
            }
        }

        public ICommand OnGestureFinished
        {
            get { return (ICommand)GetValue(OnGestureFinishedProperty); }
            set { SetValue(OnGestureFinishedProperty, value); }
        }

    }
}