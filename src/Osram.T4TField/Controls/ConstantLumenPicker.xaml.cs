﻿using MvvmCross.Binding.BindingContext;
using Osram.TFTBackend;
using System;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Osram.T4TField.Controls
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ConstantLumenPicker : Grid
    {
        public static readonly BindableProperty CurrentProperty =
               BindableProperty.Create(nameof(Current), typeof(int), typeof(ConstantLumenPicker), 0,
                   BindingMode.TwoWay, propertyChanged: OnCurrentPropertyChanged);

        public static readonly BindableProperty TimeProperty =
               BindableProperty.Create(nameof(Time), typeof(int), typeof(ConstantLumenPicker), 0,
                   BindingMode.TwoWay, propertyChanged: OnTimePropertyChanged);

        public static readonly BindableProperty MinProperty =
               BindableProperty.Create(nameof(Min), typeof(int), typeof(ConstantLumenPicker), 0,
                   BindingMode.TwoWay, propertyChanged: OnMinPropertyChanged);

        public static readonly BindableProperty MaxProperty =
               BindableProperty.Create(nameof(Max), typeof(int), typeof(ConstantLumenPicker), 0,
                   BindingMode.TwoWay, propertyChanged: OnMaxPropertyChanged);

        public ConstantLumenPicker()
        {
            InitializeComponent();

            Entry.Completed += (object sender, EventArgs e) => OnEntryTextCompleted(sender);
            Entry.Unfocused += (object sender, FocusEventArgs e) => OnEntryTextCompleted(sender);
            Entry.TextChanged += (object sender, TextChangedEventArgs e) => OnEntryTextChanged(sender, e);

            Decrease.Command = new Command(OnDecreaseCommand);
            Increase.Command = new Command(OnIncreaseCommand);
            //BindingContext = this;
        }

        public int Current
        {
            get {
                return (int)GetValue(CurrentProperty); }
            set
            {
                if (value == 255)
                {
                    if (Min != 255)
                    {
                        Decrease.IsEnabled = true;
                    }
                    else
                    {
                        Decrease.IsEnabled = false;
                    }                   
                    Increase.IsEnabled = false;
                    Entry.InputTransparent = true;
                    Entry.TextColor = Color.DarkGray;
                    Entry.BackgroundColor = Color.DarkGray;                    
                }
                else if (Time != 255)
                {
                    Decrease.IsEnabled = true;
                    Increase.IsEnabled = true;
                    Entry.InputTransparent = false;
                    //Entry.TextColor = Color.FromRgb(0xE9, 0x5B, 0x0D);
                    Entry.TextColor = Color.Black;
                    Entry.BackgroundColor = Color.White;
                }

                Entry.Text = value.ToString();

                SetValue(CurrentProperty, value);
                OnPropertyChanged();
            }
        }

        public int Time
        {
            get
            {
                return (int)GetValue(TimeProperty);
            }
            set
            {
                if (value == 255)
                {
                    Decrease.IsEnabled = false;
                    Increase.IsEnabled = false;
                    Entry.InputTransparent = true;
                    Entry.TextColor = Color.DarkGray;
                    Entry.BackgroundColor = Color.DarkGray;
                }
                else
                {
                    Decrease.IsEnabled = true;
                    Increase.IsEnabled = true;
                    Entry.InputTransparent = false;
                    //Entry.TextColor = Color.FromRgb(0xE9, 0x5B, 0x0D);
                    Entry.TextColor = Color.Black;
                    Entry.BackgroundColor = Color.White;
                }

                SetValue(TimeProperty, value);
                OnPropertyChanged();
            }
        }

        public int Min
        {
            get
            {
                return (int)GetValue(MinProperty);
            }
            set
            {
                if (value != 255 && Current == 255)
                {
                    Decrease.IsEnabled = true;
                }
                else if (value == 255)
                {
                    Decrease.IsEnabled = false;
                }

                SetValue(MinProperty, value);
                OnPropertyChanged();
            }
        }

        public int Max
        {
            get
            {
                return (int)GetValue(MaxProperty);
            }
            set
            {
                SetValue(MaxProperty, value);
                OnPropertyChanged();
            }
        }

        private static void OnCurrentPropertyChanged (BindableObject bindable, object oldValue, object newValue)
        {
            var clmp = (ConstantLumenPicker)bindable;

            if (clmp == null)
            {
                return;
            }

            int value = (int)newValue;
            clmp.Current = value;
        }

        private static void OnTimePropertyChanged(BindableObject bindable, object oldValue, object newValue)
        {
            var clmp = (ConstantLumenPicker)bindable;

            if (clmp == null)
            {
                return;
            }

            int value = (int)newValue;
            clmp.Time = value;
        }

        private static void OnMinPropertyChanged(BindableObject bindable, object oldValue, object newValue)
        {
            var clmp = (ConstantLumenPicker)bindable;

            if (clmp == null)
            {
                return;
            }

            int value = (int)newValue;
            clmp.Min = value;
        }

        private static void OnMaxPropertyChanged(BindableObject bindable, object oldValue, object newValue)
        {
            var clmp = (ConstantLumenPicker)bindable;

            if (clmp == null)
            {
                return;
            }

            int value = (int)newValue;
            clmp.Max = value;
        }

        public void OnEntryTextChanged(object sender, TextChangedEventArgs e)
        {
            //TODO remove, is it really need it?

            //var entry = (Entry)sender;
            //try
            //{
            //    //if (e.NewTextValue != null && e.NewTextValue.Equals(""))
            //    //{
            //    //    Current = 0;
            //    //}
            //}
            //catch (Exception ex)
            //{
            //    if (e.NewTextValue != null && e.NewTextValue.Equals(""))
            //    {
            //        Current = 0;
            //    }
            //    else
            //    {
            //        Current = int.Parse(e.OldTextValue);
            //        Logger.Exception("ConstantLumenPicker.OnEntryTextChanged", ex);
            //    }
            //}            
        }

        public void OnEntryTextCompleted(object sender)
        {
            var entry = (Entry)sender;            

            try
            {
                if (entry.Text.Length > 3)
                {
                    Current = Max;
                }
                else if (entry.Text != "")
                {
                    if (int.Parse(entry.Text) >= Max)
                    {
                        Current = Max;
                    }
                    else if (int.Parse(entry.Text) <= Min)
                    {
                        Current = Min;
                    }
                    else
                    {
                        Current = int.Parse(entry.Text);
                    }
                }
                else
                {
                    Current = Min;
                }
            }
            catch (Exception ex)
            {
                Current = Max;
                Logger.Exception("ConstantLumenPicker.OnEntryTextCompleted", ex);
            }
        }

        private void OnDecreaseCommand()
        {
            Increase.IsEnabled = true;

            if (Current == (Min + 1))
            {
                Decrease.IsEnabled = false;
            }
            else
            {
                Decrease.IsEnabled = true;
            }            

            if(Current > Min)
            {
                Current--;
            }            
        }

        private void OnIncreaseCommand()
        {
            Decrease.IsEnabled = true;

            if (Current == (Max - 1))
            {
                Increase.IsEnabled = false;
            }
            else
            {
                Increase.IsEnabled = true;
            }

            if (Current < Max)
            {
                Current++;
            }
        }
    }
}