using System.Windows.Input;
using Xamarin.Forms;

namespace Osram.T4TField.Controls.SliderWheel
{
    public class SliderWheelView : View
    {
        public static readonly BindableProperty CommandProperty =
            BindableProperty.Create(nameof(Command), typeof(ICommand), typeof(SliderWheelView), null);

        public static readonly BindableProperty SetValueCommandProperty =
            BindableProperty.Create(nameof(SetValueCommand), typeof(ICommand), typeof(SliderWheelView), null);

        public static readonly BindableProperty MinValueProperty =
            BindableProperty.Create(nameof(MinValue), typeof(int), typeof(SliderWheelView), 0, BindingMode.TwoWay);

        public static readonly BindableProperty MaxValueProperty =
            BindableProperty.Create(nameof(MaxValue), typeof(int), typeof(SliderWheelView), 100, BindingMode.TwoWay);

        public static readonly BindableProperty LightIntensityProperty =
            BindableProperty.Create(nameof(LightIntensity), typeof(int), typeof(SliderWheelView), 0, BindingMode.TwoWay);

        public static readonly BindableProperty OutputProperty =
         BindableProperty.Create(nameof(Output), typeof(int), typeof(SliderWheelView), 0, BindingMode.TwoWay);

        public static readonly BindableProperty OutputMinProperty =
          BindableProperty.Create(nameof(OutputMin), typeof(int), typeof(SliderWheelView), 0, BindingMode.TwoWay);

        public static readonly BindableProperty OutputMaxProperty =
         BindableProperty.Create(nameof(OutputMax), typeof(int), typeof(SliderWheelView), 0, BindingMode.TwoWay);

        public static readonly BindableProperty SecondOutputProperty =
         BindableProperty.Create(nameof(SecondOutput), typeof(string), typeof(SliderWheelView), "", BindingMode.TwoWay);

        public static readonly BindableProperty OutputUnitProperty =
         BindableProperty.Create(nameof(OutputUnit), typeof(string), typeof(SliderWheelView), "", BindingMode.TwoWay);

        public static readonly BindableProperty TypeProperty =
            BindableProperty.Create(nameof(Type), typeof(SliderWheelType), typeof(SliderWheelView), SliderWheelType.OnOffSlider,
                BindingMode.OneWay);

        public static readonly BindableProperty IsCheckedProperty =
            BindableProperty.Create(nameof(IsChecked), typeof(bool), typeof(SliderWheelView), false, BindingMode.OneWay);

        public static readonly BindableProperty TextProperty =
            BindableProperty.Create(nameof(Text), typeof(string), typeof(SliderWheelView), "", BindingMode.OneWay);

        public static readonly BindableProperty IsButtonVisibleProperty =
            BindableProperty.Create(nameof(IsButtonVisible), typeof(bool), typeof(SliderWheelView), true, BindingMode.OneWay);

        public static readonly BindableProperty IsSliderEnabledProperty =
           BindableProperty.Create(nameof(IsSliderEnabled), typeof(bool), typeof(SliderWheelView), true, BindingMode.TwoWay);

        public ICommand Command {
            get { return (ICommand)GetValue(CommandProperty); }
            set { SetValue(CommandProperty, value); }
        }

        public ICommand SetValueCommand {
            get { return (ICommand)GetValue(SetValueCommandProperty); }
            set { SetValue(SetValueCommandProperty, value); }
        }

        public int MinValue {
            get { return (int)GetValue(MinValueProperty); }
            set { SetValue(MinValueProperty, value); }
        }

        public int MaxValue {
            get { return (int)GetValue(MaxValueProperty); }
            set { SetValue(MaxValueProperty, value); }
        }

        public int LightIntensity {
            get { return (int)GetValue(LightIntensityProperty); }
            set { SetValue(LightIntensityProperty, value); }
        }

        public int Output {
            get { return (int)GetValue(OutputProperty); }
            set { SetValue(OutputProperty, value); }
        }

        public int OutputMin {
            get { return (int)GetValue(OutputMinProperty); }
            set { SetValue(OutputMinProperty, value); }
        }

        public int OutputMax {
            get { return (int)GetValue(OutputMaxProperty); }
            set { SetValue(OutputMaxProperty, value); }
        }

        public string SecondOutput {
            get { return (string)GetValue(SecondOutputProperty); }
            set { SetValue(SecondOutputProperty, value); }
        }

        public string OutputUnit {
            get { return (string)GetValue(OutputUnitProperty); }
            set { SetValue(OutputUnitProperty, value); }
        }

        public SliderWheelType Type {
            get { return (SliderWheelType)GetValue(TypeProperty); }
            set { SetValue(TypeProperty, value); }
        }

        public bool IsChecked {
            get { return (bool)GetValue(IsCheckedProperty); }
            set { SetValue(TypeProperty, value); }
        }

        public bool IsSliderEnabled {
            get { return (bool)GetValue(IsSliderEnabledProperty); }
            set { SetValue(IsSliderEnabledProperty, value); }
        }

        public string Text {
            get { return (string)GetValue(TextProperty); }
            set { SetValue(TextProperty, value); }
        }

        public bool IsButtonVisible {
            get { return (bool)GetValue(IsButtonVisibleProperty); }
            set { SetValue(IsButtonVisibleProperty, value); }
        }
    }
}