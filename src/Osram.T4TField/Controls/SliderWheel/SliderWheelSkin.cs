using Splat;

namespace Osram.T4TField.Controls.SliderWheel
{
    public class SliderWheelSkin
    {
        public IBitmap WheelBkImage { get; set; }

        public IBitmap MiddleButtonUncheckedImage { get; set; }

        public IBitmap MiddleButtonCheckedImage { get; set; }

        public IBitmap MiddleButtonPressedImage { get; set; }

        public IBitmap StatusBallImage { get; set; }

        public IBitmap SliderBkImage { get; set; }
    }
}