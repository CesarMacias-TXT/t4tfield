namespace Osram.T4TField.Controls.SliderWheel
{
    public enum SliderWheelType
    {
        OnOffSlider,
        Slider,
        OnOff,
        SaveSlider
    }
}