﻿namespace Osram.T4TField.Controls.SliderWheel
{
    public static class SliderWheelConsts
    {
        public const int MaxValue = 100;
        public const int MinValue = 0;
        public const int StartAngle = 196;
        public const int SliderAngle = 329;
        public const bool Clockwise = true;
        public const int MaxValueChangeAllowed = 60;
    }
}