﻿using System;
using Xamarin.Forms;

namespace Osram.T4TField.Controls
{
    public class XFormNumberPicker : View
    {

        public static readonly BindableProperty TypeProperty =
                   BindableProperty.Create(nameof(Type), typeof(PickerType), typeof(XFormNumberPicker), PickerType.Hour,
                       BindingMode.TwoWay);

        public static readonly BindableProperty CurrentProperty =
                BindableProperty.Create(nameof(Current), typeof(int), typeof(XFormNumberPicker), 12,
                    BindingMode.TwoWay);

        public static readonly BindableProperty MinProperty =
                BindableProperty.Create(nameof(Min), typeof(int), typeof(XFormNumberPicker), 12,
                    BindingMode.TwoWay);

        public static readonly BindableProperty MaxProperty =
                BindableProperty.Create(nameof(Max), typeof(int), typeof(XFormNumberPicker), 12,
                    BindingMode.TwoWay);

        public XFormNumberPicker()
        {

        }

        public Action<int> valueChange;

        public PickerType Type {
            get { return (PickerType)GetValue(TypeProperty); }
            set { SetValue(TypeProperty, value); OnPropertyChanged(); }
        }

        public int Current {
            get { return (int)GetValue(CurrentProperty); }
            set {
                SetValue(CurrentProperty, value);
                OnPropertyChanged();
            }
        }

        public int? Min {
            get { return (int?)GetValue(MinProperty); }
            set {
                SetValue(MinProperty, value);
                OnPropertyChanged();
            }
        }

        public int? Max {
            get { return (int?)GetValue(MaxProperty); }
            set {
                SetValue(MaxProperty, value);
                OnPropertyChanged();
            }
        }

        public enum PickerType
        {
            Hour,
            Minutes,
            Number
        }

    }
}