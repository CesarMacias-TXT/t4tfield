﻿using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows.Input;
using Osram.T4TField.ViewModels;
using Xamarin.Forms;

namespace Osram.T4TField.Controls
{
    public  partial class AFeaturesGridView : Grid
    {
        public static readonly BindableProperty CommandParameterProperty = BindableProperty.Create(nameof(CommandParameter), typeof(object), typeof(AFeaturesGridView));
        public static readonly BindableProperty CommandProperty = BindableProperty.Create(nameof(Command), typeof(ICommand), typeof(AFeaturesGridView));
        public static readonly BindableProperty FeaturesListProperty = BindableProperty.Create(nameof(FeaturesList), typeof(ObservableCollection<FeatureInfoButton>), typeof(AFeaturesGridView), null, BindingMode.TwoWay);



        public ObservableCollection<FeatureInfoButton> FeaturesList
        {
            get
            {
                return (ObservableCollection<FeatureInfoButton>)GetValue(FeaturesListProperty);
            }
            set
            {

                SetValue(FeaturesListProperty, value);
      
            }
        }





        private int _maxColumns = 2;
        private float _tileHeight = 100;


        public AFeaturesGridView()
        {
            for (var i = 0; i < MaxColumns; i++)
            {
                ColumnDefinitions.Add(new ColumnDefinition());
            }
        }

        public Type ItemTemplate { get; set; }

        public int MaxColumns
        {
            get { return _maxColumns; }
            set { _maxColumns = value; }
        }

        public float TileHeight
        {
            get { return _tileHeight; }
            set { _tileHeight = value; }
        }

        public object CommandParameter
        {
            get { return GetValue(CommandParameterProperty); }
            set { SetValue(CommandParameterProperty, value); }
        }

        public ICommand Command
        {
            get { return (ICommand)GetValue(CommandProperty); }
            set { SetValue(CommandProperty, value); }
        }

        public void BuildTiles()
        {
            // Wipe out the previous row definitions if they're there.
            if (RowDefinitions.Any())
            {
                RowDefinitions.Clear();
            }
            var enumerable = FeaturesList;
            var numberOfRows = Math.Ceiling(enumerable.Count / (float)MaxColumns);
            for (var i = 0; i < numberOfRows; i++)
            {
               
                RowDefinitions.Add(new RowDefinition { Height = TileHeight });
            }

            for (var index = 0; index < enumerable.Count; index++)
            {
                var column = index % MaxColumns;
                var row = (int)Math.Floor(index / (float)MaxColumns);
                var tile = BuildTile(enumerable[index]);
                Children.Add(tile, column, row);
            }

        }



        private FeatureButton BuildTile(object item1)
        {
            FeatureButton buildTile =    (FeatureButton)Activator.CreateInstance(ItemTemplate);
            FeatureInfoButton currentItem  =  (FeatureInfoButton)item1;

            
            buildTile.BackgroundColor = Color.White;
            buildTile.BindingContext = currentItem;
           
          //  

            var isEnabledBinding = new Binding()
            {
                Path = "FeatureInfo.IsAvailable",
                Source = currentItem
            };
            isEnabledBinding.Mode = BindingMode.TwoWay;
            buildTile.SetBinding(FeatureButton.IsBtnEnabledProperty, isEnabledBinding);


            var isSelectedBinding = new Binding()
            {
                Path = "IsSelected",
                Source = currentItem
            };
            isSelectedBinding.Mode = BindingMode.TwoWay;
            buildTile.SetBinding(FeatureButton.IsFeatureSelectedProperty, isSelectedBinding);


            var featuresStatusBinding = new Binding()
            {
                Path = "FeatureInfo.Status",
                Source = currentItem
            };
            featuresStatusBinding.Mode = BindingMode.TwoWay;
            buildTile.SetBinding(FeatureButton.FeatureStatusProperty, featuresStatusBinding);

            var LockTypeBinding = new Binding()
            {
                Path = "FeatureInfo.Lock",
                Source = currentItem
            };
            LockTypeBinding.Mode = BindingMode.TwoWay;
            buildTile.SetBinding(FeatureButton.LockTypeProperty, LockTypeBinding);


            // BindingOperations.SetBinding(buildTile, StackLayoutButton.IsBtnEnabledProperty, b);

            buildTile.IsFeatureSelected = currentItem.IsSelected;
            buildTile.IsBtnEnabled = currentItem.IsEnabled;
            buildTile.ClickCommand = currentItem.OnClickButton;
            return buildTile;
          
        }
    }
}