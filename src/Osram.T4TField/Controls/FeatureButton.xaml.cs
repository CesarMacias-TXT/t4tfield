﻿
using Osram.T4TField.ViewModels;
using System.Windows.Input;
using Xamarin.Forms;
using System;
using Osram.TFTBackend.DataModel.Feature.Contracts;

namespace Osram.T4TField.Controls
{
    public partial class FeatureButton : StackLayout
    {

        public static readonly BindableProperty IsBtnEnabledProperty =
        BindableProperty.Create(nameof(IsBtnEnabled), typeof(bool), typeof(FeatureButton), true, BindingMode.TwoWay, propertyChanged: ((bindable, value, newValue) =>
        {
            
            var button  = (FeatureButton)bindable;
            FeatureInfoButton featureBtn = (FeatureInfoButton)button.BindingContext;
            if ((bool)newValue == true){
                //SetEnableBtn(button, currentImgBtn.ImageButtonType);
                button.DisabledBox.IsVisible = false;
                button.ForceLayout();
            }
            else
            {
                button.DisabledBox.IsVisible = true;
            }
         }));


        public static readonly BindableProperty LockTypeProperty =
        BindableProperty.Create(nameof(LockType), typeof(ProtectionLock), typeof(FeatureButton), ProtectionLock.None, BindingMode.TwoWay, propertyChanged: ((bindable, value, newValue) =>
       {

           var button = (FeatureButton)bindable;
           FeatureInfoButton featureBtn = (FeatureInfoButton)button.BindingContext;
           var lockIcon = button.LockIconBox;
           switch ((ProtectionLock)newValue)
           {
               case ProtectionLock.None:
                   lockIcon.IsVisible = false;
               break;

               case ProtectionLock.Master:
                   lockIcon.Source = featureBtn.LockMasterIconLink;
                   lockIcon.IsVisible = true;
               break;

               case ProtectionLock.Service:
                   lockIcon.Source = featureBtn.LockServiceIconLink;
                   lockIcon.IsVisible = true;
               break;
              
           }
         
       }));

        public static readonly BindableProperty IsFeatureSelectedProperty =
        BindableProperty.Create(nameof(IsFeatureSelected), typeof(bool), typeof(FeatureButton), false, BindingMode.TwoWay, propertyChanged: ((bindable, value, newValue) =>
        {

            var button = (FeatureButton)bindable;
            FeatureInfoButton currentImgBtn = (FeatureInfoButton)button.BindingContext;
            if ((bool)newValue == true)
            {
                //SetEnableBtn(button, currentImgBtn.ImageButtonType);
                button.SelectedBox.IsVisible = true;
                button.ForceLayout();
            }
            else
            {
                button.SelectedBox.IsVisible = false;
            }
        }));


        public static readonly BindableProperty FeatureStatusProperty =
        BindableProperty.Create(nameof(FeatureStatus), typeof(FeatureStatus), typeof(FeatureButton), TFTBackend.DataModel.Feature.Contracts.FeatureStatus.Ok, BindingMode.TwoWay, propertyChanged: ((bindable, value, newValue) =>
        {

            var button = (FeatureButton)bindable;
            FeatureInfoButton currentImgBtn = (FeatureInfoButton)button.BindingContext;
            switch (currentImgBtn.FeatureInfo.Status)
            {
                case TFTBackend.DataModel.Feature.Contracts.FeatureStatus.Error:
                    button.FeatureStatus.Source = ImageSource.FromFile(currentImgBtn.ErrorIconLink); 
                    button.FeatureStatus.IsVisible = true;
                    break;
                case TFTBackend.DataModel.Feature.Contracts.FeatureStatus.Ok:
                    button.FeatureStatus.IsVisible = false;
                    break;
                case TFTBackend.DataModel.Feature.Contracts.FeatureStatus.Warning:
                    button.FeatureStatus.Source = ImageSource.FromFile(currentImgBtn.WarningIconLink);
                    button.FeatureStatus.IsVisible = true;
                    break;
                default:
                    break;
            }

        }));

      

        public bool IsFeatureSelected
        {
            get { return (bool)GetValue(IsFeatureSelectedProperty); }
            set { SetValue(IsFeatureSelectedProperty, value); }
        }

        public bool LockType
        {
            get { return (bool)GetValue(LockTypeProperty); }
            set { SetValue(LockTypeProperty, value); }
        }


        public Color TextColor
        {
            get { return this.Text.TextColor; }
            set { this.Text.TextColor = value; }
        }



        public bool IsBtnEnabled
        {
            get { return (bool)GetValue(IsBtnEnabledProperty); }
            set { SetValue(IsBtnEnabledProperty, value); }
        }

        public Label TextControl
        {
            get { return this.Text; }
            set { this.Text = value;}
        }

        public const string ClickCommandPropertyName = "ClickCommand";
        public static readonly BindableProperty ClickCommandProperty =
        BindableProperty.CreateAttached(
                "ClickCommand", /* string PropertyName */
                typeof(ICommand), /* Type returnType */
                typeof(StackLayoutButton), /* Type declaringType */
                null, /* default value */
                BindingMode.Default, /* defaultBindingMode */
                propertyChanged: (bindable, oldValue, newValue) => {
                    OnCommandPropertyChanged(bindable, (ICommand)oldValue, (ICommand)newValue);
                });


        private static void OnCommandPropertyChanged(BindableObject bindable, ICommand oldValue, ICommand newValue)
        {
            var button = (FeatureButton)bindable;
            if (button == null)
            {
                return;
            }
            button.OnCommandChanged();
        }

        private void OnCommandChanged()
        {
            OnPropertyChanged("ClickCommand");
        }

        public ICommand ClickCommand
        {
            get
            {
                var command = (ICommand)GetValue(ClickCommandProperty);
                return command;
            }
            set
            {
                SetValue(ClickCommandProperty, value);
            }
        }

        public ICommand ExecuteCommand
        {
            get
            {
                return new Command(ExecuteAction());
            }
        }

        private System.Action ExecuteAction()
        {
           return new System.Action(Exec);
        }

        private void Exec()
        {      
            if (ClickCommand != null && IsBtnEnabled)
            {
                ClickCommand.Execute(null);
            }
        }

        public FeatureButton()
        {
            InitializeComponent();
            TapGestureRecognizer tap = new TapGestureRecognizer();          
            tap.Command = ExecuteCommand;
            Button.GestureRecognizers.Add(tap);
         }


    }


   
}



