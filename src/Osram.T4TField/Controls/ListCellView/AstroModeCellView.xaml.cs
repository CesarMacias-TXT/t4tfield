using MvvmCross;
using MvvmCross.Plugin.Messenger;
using Osram.T4TField.Messaging;
using Osram.T4TField.Resources;
using Osram.TFTBackend.DataModel.Data.Contracts;
using Osram.TFTBackend.DataModel.Feature.Contracts;
using System;
using System.Linq;
using Xamarin.Forms;

namespace Osram.T4TField.Controls.ListCellView
{
    public partial class AstroModeCellView : ViewCell
    {
        private readonly IMvxMessenger _messenger;

        private readonly IAstroDimFeature _astroDimFeature;
        private readonly IOperatingModeData _operatingModeData;

        private int indexNone = 2;

        public AstroModeCellView()
        {
            _messenger = Mvx.IoCProvider.Resolve<IMvxMessenger>();
            _astroDimFeature = Mvx.IoCProvider.Resolve<IAstroDimFeature>();
            _operatingModeData = Mvx.IoCProvider.Resolve<IOperatingModeData>();

            InitializeComponent();

            ModeAstroDim.Items.Add(AppResources.F_AstroModeTimeBased);
            ModeAstroDim.Items.Add(AppResources.F_AstroModeAstroBased);
            ModeAstroDim.SelectedIndex = GetAstroDimModeIndex();
            ModeAstroDim_SelectedIndexChanged(ModeAstroDim, null);
            ModeAstroDim.SelectedIndexChanged -= ModeAstroDim_SelectedIndexChanged;
            ModeAstroDim.SelectedIndexChanged += ModeAstroDim_SelectedIndexChanged;

            ModeOperating.Items.Add(AppResources.F_OperatingModeOnOff);
            ModeOperating.Items.Add(AppResources.F_OperatingModeAstroDim);
            ModeOperating.Items.Add(AppResources.F_OperatingModeOther);
            ModeOperating.SelectedIndex = GetDimmingModeIndex();

            // Remove "Other" if is not already selected
            if (ModeOperating.SelectedIndex != indexNone)
            {
                ModeOperating.Items.RemoveAt(indexNone);
            }

            ModeDimming_SelectedIndexChanged(ModeOperating, null);
            ModeOperating.SelectedIndexChanged -= ModeDimming_SelectedIndexChanged;
            ModeOperating.SelectedIndexChanged += ModeDimming_SelectedIndexChanged;
        }


        public static readonly BindableProperty IsEditableProperty = BindableProperty.Create(nameof(IsEditable), typeof(bool), typeof(AstroModeCellView), true, BindingMode.TwoWay, propertyChanged: ((bindable, value, newValue) =>
        {
            AstroModeCellView elem = (AstroModeCellView)bindable;
            elem.ModeAstroDim.IsEnabled = (bool)newValue;

        }));



        public bool IsEditable {
            get {
                return (bool)GetValue(IsEditableProperty);
            }
            set {
                SetValue(IsEditableProperty, value);
            }
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();
        }


        //public bool IsOperatingModeEditable
        //{
        //    get
        //    {
        //        return isOperatingModeEditable;
        //    }
        //    set
        //    {
        //        isOperatingModeEditable = value;
        //    }
        //}

        //private bool isModePickerEditable;
        //public bool IsModePickerEditable
        //{
        //    get
        //    {
        //        return isModePickerEditable;
        //    }
        //    set
        //    {
        //         isModePickerEditable = value;
        //    }
        //}
        private int GetAstroDimModeIndex()
        {
            var mode = _astroDimFeature.AstroDimMode;
            int index = 0;
            if (mode == AstroDimMode.AstroBased)
            {
                var elem = ModeAstroDim.Items.Where(item => item == AppResources.F_AstroModeAstroBased).First();
                index = ModeAstroDim.Items.IndexOf(elem);
            }
            else if (mode == AstroDimMode.TimeBased)
            {
                var elem = ModeAstroDim.Items.Where(item => item == AppResources.F_AstroModeTimeBased).First();
                index = ModeAstroDim.Items.IndexOf(elem);
            }
            return index;
        }

        private int GetDimmingModeIndex()
        {
            var operatingModeEnum = _operatingModeData.GetCurrentOperatingModeEnum();

            int index = 0;
            if (operatingModeEnum == OperatingModeEnum.OnOff)
            {
                var elem = ModeOperating.Items.Where(item => item == AppResources.F_OperatingModeOnOff).First();
                index = ModeOperating.Items.IndexOf(elem);
            }
            else if (operatingModeEnum == OperatingModeEnum.AstroDim)
            {
                var elem = ModeOperating.Items.Where(item => item == AppResources.F_OperatingModeAstroDim).First();
                index = ModeOperating.Items.IndexOf(elem);
            }
            else
            {
                var elem = ModeOperating.Items.Where(item => item == AppResources.F_OperatingModeOther).First();
                index = ModeOperating.Items.IndexOf(elem);
            }
            return index;
        }


        private void ModeAstroDim_SelectedIndexChanged(object sender, EventArgs e)
        {
            var picker = sender as Picker;
            if (picker != null)
            {
                var mode = GetAstroDimModeEnum((string)picker.SelectedItem);
                _messenger.Publish(new Events.SelectedAstroModeEvent(this, mode));
            }
        }

        private void ModeDimming_SelectedIndexChanged(object sender, EventArgs e)
        {
            var picker = sender as Picker;
            if (picker != null)
            {
                var mode = GetOperatingModeEnum((string)picker.SelectedItem);

                // if the OperatingMode selected is not NONE we have to remove it
                if (mode != OperatingModeEnum.None && picker.Items.Contains(AppResources.F_OperatingModeOther))
                {
                    picker.Items.RemoveAt(indexNone);
                }

                if (e != null)
                {
                    _operatingModeData.SetOperatingModeEnum(mode);
                }
                _messenger.Publish(new Events.SelectedOperatingModeEvent(this, mode));
                EnableDisableOperatingModePicker(mode);
            }
        }

        public void EnableDisableOperatingModePicker(OperatingModeEnum mode)
        {
            if (mode == OperatingModeEnum.AstroDim && IsEditable)
            {
                ModeAstroDim.IsEnabled = true;
            }
            else
            {
                ModeAstroDim.IsEnabled = false;
            }
        }


        private AstroDimMode GetAstroDimModeEnum(string text)
        {
            if (text == AppResources.F_AstroModeTimeBased)
            {
                return AstroDimMode.TimeBased;
            }
            if (text == AppResources.F_AstroModeAstroBased)
            {
                return AstroDimMode.AstroBased;
            }

            throw new Exception("AstroMode enum not found!");
        }

        private OperatingModeEnum GetOperatingModeEnum(string text)
        {
            if (text == AppResources.F_OperatingModeOnOff)
            {
                return OperatingModeEnum.OnOff;
            }
            if (text == AppResources.F_OperatingModeAstroDim)
            {
                return OperatingModeEnum.AstroDim;
            }
            if (text == AppResources.F_OperatingModeOther)
            {
                return OperatingModeEnum.None;
            }

            throw new Exception("AstroMode enum not found!");
        }


    }
}