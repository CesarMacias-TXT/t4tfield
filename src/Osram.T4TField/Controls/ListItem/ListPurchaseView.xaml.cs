using Osram.TFTBackend.InAppPurchaseService.Data;
using Xamarin.Forms;
using MvvmCross.Commands;

namespace Osram.T4TField.Controls.ListItem
{
    public partial class ListPurchaseView : Grid
    {

        private BoxView _separatorLine;

        public ListPurchaseView()
        {
            InitializeComponent();

            _separatorLine = new BoxView()
            {
                BackgroundColor = Color.FromRgb(240, 240, 240),
                HeightRequest = 1,
            };

            Children.Add(_separatorLine, 0, 10);
            SetColumnSpan(_separatorLine, 2);

            HasSeparator = true;

            if (Device.RuntimePlatform == Device.iOS)
            {
                BackgroundColor = Color.White;
            }
        }

        public bool HasSeparator {
            get { return _separatorLine.IsVisible; }
            set { _separatorLine.IsVisible = value; }
        }

        public static readonly BindableProperty PurchaseCommandProperty =
           BindableProperty.Create(nameof(PurchaseCommand), typeof(IMvxCommand<BillingProduct>), typeof(ListPurchaseView), propertyChanged:
               (bindable, value, newValue) =>
               {
                   var control = (ListPurchaseView)bindable;
               });

        public IMvxCommand<BillingProduct> PurchaseCommand {
            get { return (IMvxCommand<BillingProduct>)GetValue(PurchaseCommandProperty); }
            set { SetValue(PurchaseCommandProperty, value); }
        }

        public static readonly BindableProperty NameProperty =
          BindableProperty.Create(nameof(Name), typeof(string), typeof(ListPurchaseView), "", propertyChanged:
               (bindable, value, newValue) =>
               {
                   var control = (ListPurchaseView)bindable;
                   control.NameLabel.Text = (string)newValue;
               });

        public string Name {
            get { return (string)GetValue(NameProperty); }
            set { SetValue(NameProperty, value); }
        }

        public static readonly BindableProperty DescriptionProperty =
            BindableProperty.Create(nameof(Description), typeof(string), typeof(ListPurchaseView), "", propertyChanged:
               (bindable, value, newValue) =>
               {
                   var control = (ListPurchaseView)bindable;
                   var text = (string)newValue;

                   control.DescriptionLabel.Text = text;
                   control.DescriptionLabel.IsVisible = !string.IsNullOrEmpty(text);

                   if (!string.IsNullOrEmpty(text))
                   {
                       Grid.SetRowSpan(control.NameLabel, 1);

                   }

               });

        public string Description {
            get { return (string)GetValue(DescriptionProperty); }
            set { SetValue(DescriptionProperty, value); }
        }

        public static readonly BindableProperty PurchaseTextProperty =
          BindableProperty.Create(nameof(PurchaseText), typeof(string), typeof(ListPurchaseView), "", propertyChanged:
               (bindable, value, newValue) =>
               {
                   var control = (ListPurchaseView)bindable;
                   control.PurchaseButton.Text = (string)newValue;
               });

        public string PurchaseText {
            get { return (string)GetValue(PurchaseTextProperty); }
            set { SetValue(PurchaseTextProperty, value); }
        }

        //public static readonly BindableProperty PriceProperty =
        //  BindableProperty.Create(nameof(Price), typeof(string), typeof(ListPurchaseView), "", propertyChanged:
        //       (bindable, value, newValue) =>
        //       {
        //           var control = (ListPurchaseView)bindable;
        //           control.PriceLabel.Text = (string)newValue;
        //       });

        //public string Price {
        //    get { return (string)GetValue(PriceProperty); }
        //    set { SetValue(PriceProperty, value); }
        //}

        //public static readonly BindableProperty TypeProperty =
        //  BindableProperty.Create(nameof(Type), typeof(string), typeof(ListPurchaseView), "", propertyChanged:
        //       (bindable, value, newValue) =>
        //       {
        //           var control = (ListPurchaseView)bindable;
        //           control.TypeLabel.Text = (string)newValue;
        //       });

        //public string Type {
        //    get { return (string)GetValue(TypeProperty); }
        //    set { SetValue(TypeProperty, value); }
        //}
    }
}