﻿using Osram.T4TField.Utils;
using Xamarin.Forms;

namespace Osram.T4TField.Controls.ListItem
{
    public partial class ListReportView : Grid
    {
        private BoxView _separatorLine;

        public ListReportView()
        {
            InitializeComponent();

            _separatorLine = new BoxView()
            {
                BackgroundColor = Color.FromRgb(240, 240, 240),
                HeightRequest = 1,
            };

            Children.Add(_separatorLine, 0, 1);
            SetColumnSpan(_separatorLine, 2);

            HasSeparator = true;

            if (Device.RuntimePlatform == Device.iOS)
            {
                BackgroundColor = Color.White;
            }
        }

        public bool HasSeparator {
            get { return _separatorLine.IsVisible; }
            set { _separatorLine.IsVisible = value; }
        }

        public static readonly BindableProperty Element1TextProperty =
        BindableProperty.Create(nameof(Element1Text), typeof(string), typeof(ListReportView), "", propertyChanged:
            (bindable, value, newValue) =>
            {
                var control = (ListReportView)bindable;


                control.Elem1.Text = (string)newValue;


            }, defaultBindingMode: BindingMode.TwoWay);


        public static readonly BindableProperty Element2TextProperty = BindableProperty.Create(nameof(Element2Text), typeof(string), typeof(ListReportView), "", propertyChanged:
        (bindable, value, newValue) =>
        {
            var control = (ListReportView)bindable;


            control.Elem2.Text = (string)newValue;


        }, defaultBindingMode: BindingMode.TwoWay);

        public static readonly BindableProperty TypeProperty = BindableProperty.Create(nameof(Type), typeof(FlatPropertyType), typeof(ListReportView), FlatPropertyType.KeyValue, propertyChanged:
       (bindable, value, newValue) =>
       {
           var control = (ListReportView)bindable;

           if (newValue.Equals(FlatPropertyType.Title))
           {
               control.BackgroundColor = Color.FromHex("#cccccc");
           }



       }, defaultBindingMode: BindingMode.TwoWay);

        public string Element1Text {

            get { return (string)GetValue(Element1TextProperty); }
            set {

                SetValue(Element1TextProperty, value);
            }
        }

        public string Element2Text {

            get { return (string)GetValue(Element2TextProperty); }
            set {

                SetValue(Element2TextProperty, value);
            }
        }

        public FlatPropertyType Type {

            get { return (FlatPropertyType)GetValue(TypeProperty); }
            set {

                SetValue(TypeProperty, value);
            }
        }


    }

}