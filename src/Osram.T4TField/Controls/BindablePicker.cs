﻿using Xamarin.Forms;

namespace Osram.T4TField.Controls
{
    public class BindablePicker : Picker
    {

        public static readonly BindableProperty FontSizeProperty = BindableProperty.Create(nameof(FontSize), typeof(int), typeof(BindablePicker), 14);
        public int FontSize {
            get {
                return (int)GetValue(FontSizeProperty);
            }
            set {
                SetValue(FontSizeProperty, value);
            }
        }
        public BindablePicker()
        {

        }
    }
}
