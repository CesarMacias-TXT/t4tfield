﻿#region Pace Source File Header

/////////////////////////////////////////////////////////
//
//                                             __---__
// (c)                                      P A C E   /\
//
// Aerospace Engineering and Information Technology GmbH
//
// Rotherstraße 20						phone:+49(30)293 62 0
// 10245 Berlin							fax:+49(30)293 62 111
// Germany								mailto:info@pace.de
//										http://www.pace.de/
//
// File:			$Workfile: PiePieceSkia.cs $
// Author:			$Author: robertw $
// Date:			$Date: 2014-10-17 15:23:32+02:00 $
// Last change:		$Modtime: 2014-10-17 13:06:22+02:00 $
// Version:			$Revision: 2 $
//
/////////////////////////////////////////////////////////////////////////////

#endregion Pace Source File Header

using Plugin.DeviceInfo;
using SkiaSharp;
using SkiaSharp.Views.Forms;
using Xamarin.Forms;

namespace Osram.T4TField.Controls
{
    /// <summary>
    /// A pie piece shape
    /// </summary>
    //[Xamarin.Forms.Xaml.XamlCompilation(Xamarin.Forms.Xaml.XamlCompilationOptions.Compile)]
    public class RoundedBox : SKCanvasView
    {
        public RoundedBox()
        {
        }

        #region Dependency Properties 

        public static readonly BindableProperty BorderColorProperty =
            BindableProperty.Create(nameof(BorderColor), typeof(Color), typeof(RoundedBox), Color.Transparent, propertyChanged: (e, o, n) => ((RoundedBox)e).InvalidateSurface());

        public static readonly BindableProperty BorderThicknessProperty =
            BindableProperty.Create(nameof(BorderThickness), typeof(float), typeof(RoundedBox), 0F, propertyChanged: (e, o, n) => ((RoundedBox)e).InvalidateSurface());

        public static readonly BindableProperty ColorProperty =
            BindableProperty.Create(nameof(Color), typeof(Color), typeof(RoundedBox), Color.Transparent, propertyChanged: (e, o, n) => ((RoundedBox)e).InvalidateSurface());

        public static readonly BindableProperty TextColorProperty =
                   BindableProperty.Create(nameof(TextColor), typeof(Color), typeof(RoundedBox), Color.DarkGray, propertyChanged: (e, o, n) => ((RoundedBox)e).InvalidateSurface());

        public static readonly BindableProperty TextProperty =
                 BindableProperty.Create(nameof(Text), typeof(string), typeof(RoundedBox), string.Empty, propertyChanged: (e, o, n) => ((RoundedBox)e).InvalidateSurface());

        /*
         *  Workaround to T4T-199 - https://github.com/xamarin/Xamarin.Forms/pull/968
         *  
         *  When the RoundedBox is created with IsEnabled=false the GestureRecognizers will not work even if the RoundedBox becomes enabled;
         *  
         */
        public static readonly BindableProperty IsEnabledFakeProperty =
                 BindableProperty.Create(nameof(IsEnabledFake), typeof(bool?), typeof(RoundedBox), null, propertyChanged: (e, o, n) => ((RoundedBox)e).InvalidateSurface());

        /// <summary>
        /// The border color
        /// </summary>
        public Color BorderColor
        {
            get { return (Color)GetValue(BorderColorProperty); }
            set { SetValue(BorderColorProperty, value); }
        }

        /// <summary>
        /// The border thickness
        /// </summary>
        public float BorderThickness
        {
            get { return (float)GetValue(BorderThicknessProperty); }
            set { SetValue(BorderThicknessProperty, value); }
        }

        /// <summary>
        /// The fill color
        /// </summary>
        public Color Color
        {
            get { return (Color)GetValue(ColorProperty); }
            set { SetValue(ColorProperty, value); }
        }


        public Color TextColor
        {
            get { return (Color)GetValue(TextColorProperty); }
            set { SetValue(TextColorProperty, value); }
        }


        public string Text
        {
            get { return (string)GetValue(TextProperty); }
            set { SetValue(TextColorProperty, value); }
        }

        /// <summary>
        /// The border color
        /// </summary>
        public bool? IsEnabledFake
        {
            get { return (bool)GetValue(IsEnabledFakeProperty); }
            set {
                if (value != null)
                {
                    SetValue(IsEnabledProperty, value);
                }
                SetValue(IsEnabledFakeProperty, value);
            }
        }

        #endregion Dependency Properties

        #region SKCanvasView

        protected override void OnPaintSurface(SKPaintSurfaceEventArgs e)
        {
            SKImageInfo info = e.Info;
            SKSurface surface = e.Surface;
            SKCanvas canvas = surface.Canvas;
            int _Height = e.Info.Height;
            int _Width = e.Info.Width;
            int _Radius = 0;
            canvas.Clear();

            // calculate max _Radius

            if (_Width > _Height) {
                _Radius = _Height;
            } else {
                _Radius = _Width;
            }
          //  _Radius = (_Radius / 100);

            SKPaint paint = new SKPaint
            {
                Style = SKPaintStyle.Stroke,
                Color = BorderColor.ToSKColor(),
                StrokeWidth = BorderThickness,
               
            };

            canvas.DrawCircle(_Width / 2, _Height / 2, (( _Radius - BorderThickness)/2)*0.8f , paint);

            // add text inside 

            // Create an SKPaint object to display the text
            SKPaint textPaint = new SKPaint
            {
                Color = TextColor.ToSKColor()
            };

            // Adjust TextSize property so text is 90% of screen width
            float textWidth = textPaint.MeasureText(Text);
            textPaint.TextSize =  (_Radius/2) * textPaint.TextSize / textWidth;

            if (Device.RuntimePlatform == Device.iOS)
                textPaint.Typeface = SKTypeface.FromFamilyName("Arial");

            // Find the text bounds
            //SKRect textBounds;
            //SKRect ref need to be initialized
            var textBounds = new SKRect();
            textPaint.MeasureText(Text, ref textBounds);


            float xText = _Width / 2 - textBounds.MidX;
            float yText = _Height / 2 - textBounds.MidY;

            canvas.DrawText(Text, xText, yText, textPaint);
           
        }

        #endregion SKCanvasView
    }
}
