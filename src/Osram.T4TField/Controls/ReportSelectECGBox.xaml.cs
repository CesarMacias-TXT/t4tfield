﻿
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Osram.T4TField.Controls
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ReportSelectECGBox : Grid
    {
        public ReportSelectECGBox()
        {
            InitializeComponent();
        }
    }
}
