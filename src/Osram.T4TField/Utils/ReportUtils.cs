﻿using CsvHelper;
using MvvmCross;
using Osram.T4TField.Resources;
using Osram.TFTBackend.DataModel.Data.Contracts;
using Osram.TFTBackend.DataModel.Feature.Contracts;
using Osram.TFTBackend.FileService.Contracts;
using System;
using System.Collections.Generic;
using System.IO;
using System.Collections.ObjectModel;
using System.Text;
using System.Threading.Tasks;
using Osram.TFTBackend.DataModel.Helper;
using System.Globalization;
using Osram.T4TField.Contracts;
using Osram.TFTBackend.KeyService.Contracts;
using Osram.TFTBackend.ConfigurationService.Contracts;
using Osram.TFTBackend.ConfigurationService.DataModel;

namespace Osram.T4TField.Utils
{
    class ReportUtils
    {
        private static ReportUtils _instance;

        private IAstroDimFeature _astroDimFeature;
        private IConstantLumenFeature _constantLumen;
        private IDaliPropertyData _daliPropertyData;
        private IDexalPSUFeature _dexalPSU;
        private IDriverGuardFeature _driverGuard;
        private IEmergencyModeFeature _emergencyMode;
        private IEndOfLifeFeature _endOfLife;
        private ILampOperatingTimeFeature _lampOperatingTime;
        private IMainsDimFeature _mainsDimFeature;
        private IOperatingCurrentFeature _operatingCurrentFeature;
        private IOperatingModeData _operatingModeData;
        private IPresenceDetectionFeature _presenceDetectionFeature;
        private IStepDimFeature _stepDimFeature;
        private IThermalProtectionFeature _thermalProtection;
        private ITuningFactorFeature _tuningFactorFeature;
        private IDimToDarkFeature _dimToDark;
        private ISoftSwitchOffFeature _softSwitchOff;
        private ITouchDimFeature _touchDim;
        private ICorridorFeature _corridor;


        private IFileService _fileService;
        private IInfoUtils _infoUtils;
        private IKeyService _keyService;

        private ReportUtils()
        {
            _astroDimFeature = Mvx.IoCProvider.Resolve<IAstroDimFeature>();
            _constantLumen = Mvx.IoCProvider.Resolve<IConstantLumenFeature>();
            _daliPropertyData = Mvx.IoCProvider.Resolve<IDaliPropertyData>();
            _dexalPSU = Mvx.IoCProvider.Resolve<IDexalPSUFeature>();
            _dimToDark = Mvx.IoCProvider.Resolve<IDimToDarkFeature>();
            _driverGuard = Mvx.IoCProvider.Resolve<IDriverGuardFeature>();
            _emergencyMode = Mvx.IoCProvider.Resolve<IEmergencyModeFeature>();
            _endOfLife = Mvx.IoCProvider.Resolve<IEndOfLifeFeature>();
            _lampOperatingTime = Mvx.IoCProvider.Resolve<ILampOperatingTimeFeature>();
            _mainsDimFeature = Mvx.IoCProvider.Resolve<IMainsDimFeature>();
            _operatingCurrentFeature = Mvx.IoCProvider.Resolve<IOperatingCurrentFeature>();
            _operatingModeData = Mvx.IoCProvider.Resolve<IOperatingModeData>();
            _presenceDetectionFeature = Mvx.IoCProvider.Resolve<IPresenceDetectionFeature>();
            _softSwitchOff = Mvx.IoCProvider.Resolve<ISoftSwitchOffFeature>();
            _stepDimFeature = Mvx.IoCProvider.Resolve<IStepDimFeature>();
            _thermalProtection = Mvx.IoCProvider.Resolve<IThermalProtectionFeature>();
            _tuningFactorFeature = Mvx.IoCProvider.Resolve<ITuningFactorFeature>();
            _touchDim = Mvx.IoCProvider.Resolve<ITouchDimFeature>();
            _corridor = Mvx.IoCProvider.Resolve<ICorridorFeature>();

            _fileService = Mvx.IoCProvider.Resolve<IFileService>();
            _infoUtils = Mvx.IoCProvider.Resolve<IInfoUtils>();
            _keyService = Mvx.IoCProvider.Resolve<IKeyService>();
        }

        public static ReportUtils Instance {
            get {
                if (_instance == null)
                {
                    _instance = new ReportUtils();
                }
                return _instance;
            }
        }

        // ---------------------

        public async Task<string> CreateCsv(List<ReportFeature> report, IConfiguration configuration)
        {
            using (var stream = new MemoryStream())
            using (var textWriter = new StreamWriter(stream))
            using (var csv = new CsvWriter(textWriter, CultureInfo.InvariantCulture))
            {
                byte[] bom = new byte[] { 0xEF, 0xBB, 0xBF };
                stream.Write(bom, 0, bom.Length);
                csv.Configuration.Delimiter = Mvx.IoCProvider.Resolve<ILocalizationInfo>().GetCurrentCultureInfo().TextInfo.ListSeparator;
                csv.WriteField(AppResources.F_LuminaireInfo + ":");
                csv.NextRecord();
                csv.WriteField(AppResources.F_Name);
                csv.WriteField(configuration?.LuminaireName);
                csv.NextRecord();
                csv.WriteField(AppResources.F_OrderCode);
                csv.WriteField(configuration?.BasicCode);
                csv.NextRecord();
                csv.WriteField(AppResources.F_CustomerProject);
                csv.WriteField("");
                csv.NextRecord();
                csv.WriteField(AppResources.F_Description);
                csv.WriteField(configuration?.DeviceDescription);
                csv.NextRecord();
                csv.WriteField(AppResources.F_ConfigID);
                csv.WriteField("");
                csv.NextRecord();
                csv.WriteField(AppResources.F_ECG1Name);
                csv.WriteField(configuration?.LuminaireName);
                csv.NextRecord();
                csv.WriteField(AppResources.F_ECG1Multiplicity);
                csv.WriteField("");
                csv.NextRecord();
                csv.WriteField(AppResources.F_ECG2Name);
                csv.WriteField("");
                csv.NextRecord();
                csv.WriteField(AppResources.F_ECG2Multiplicity);
                csv.WriteField("");
                csv.NextRecord();
                csv.WriteField(AppResources.F_ImageFileName);
                csv.WriteField(configuration?.ImageFileName);
                csv.NextRecord();

                // Write of header features
                csv.NextRecord();
                csv.WriteField(AppResources.F_ParametersValue + ":");
                csv.NextRecord();

                // Write of features
                foreach (var feature in report)
                {
                    foreach (var property in feature.ReportProperties)
                    {
                        csv.WriteField(feature.Name);
                        csv.WriteField(property.Name);
                        csv.WriteField(property.Value);
                        csv.NextRecord();
                    }
                }

                textWriter.Flush();
                stream.Flush();
                stream.Seek(0, SeekOrigin.Begin);

                bool isPermissionGranted = await PermissionUtils.Instance.CheckPermissionStatus(Plugin.Permissions.Abstractions.Permission.Storage);
                string path = null;
                if (isPermissionGranted)
                {
                    string nameCsv = DateTime.Now.ToString("yyyyMMdd_HHmmss") + ".csv";

                    path = _fileService.CombinePath(_infoUtils.GetDownloadFolderPath(), AppResources.F_T4TFieldReports);
                    _fileService.EnsureFolderExists(path);
                    path = _fileService.CombinePath(path, nameCsv);
                    _fileService.CopyStreamToFile(stream, path);
                }

                return path;
            }
        }

        public ObservableCollection<FlatReportProperty> GetFlatReport(List<ReportFeature> report)
        {
            ObservableCollection<FlatReportProperty> flatReport = new ObservableCollection<FlatReportProperty>();

            foreach (var reportFeature in report)
            {
                flatReport.Add(new FlatReportProperty { Type = FlatPropertyType.Title, Element1 = reportFeature.Name });
                foreach (var reportProperty in reportFeature.ReportProperties)
                {
                    flatReport.Add(new FlatReportProperty { Type = FlatPropertyType.KeyValue, Element1 = reportProperty.Name, Element2 = reportProperty.Value });
                }
            }

            return flatReport;
        }

        private string GetConfigurationLock(Dictionary<String, Permission> permissionsDictonary, String name)
        {
            ProtectionLock protectionLock = GetLockByPermissionName(permissionsDictonary, name);
            if (protectionLock == ProtectionLock.Master)
            {
                return AppResources.F_Report_Master;
            }
            else if (protectionLock == ProtectionLock.Service)
            {
                return AppResources.F_Report_Service;
            }
            else
            {
                return AppResources.F_Report_None;
            }
        }

        private ProtectionLock GetLockByPermissionName(Dictionary<String, Permission> permissionsDictonary, String name)
        {
            Permission permission;
            if (permissionsDictonary.TryGetValue(name, out permission))
            {
                return _keyService.GetProtectionLock(permission.Value);
            }
            return ProtectionLock.None;
        }

        private bool IsAvailableAdvanced(IConfiguration configuration, String name)
        {
            EcgProperty property;
            if (configuration.PropertyDictonary.TryGetValue(name, out property))
            {
                return true;
            }
            return false;
        }

        public List<ReportFeature> GetReport(IConfiguration configuration)
        {
            List<ReportFeature> report = new List<ReportFeature>();

            var permissionDictionary = configuration.PermissionsDictonary;

            // ----------
            // IConfigurationLock
            // ----------
            ReportFeature configurationLock = new ReportFeature { Name = AppResources.F_Report_ConfigurationLock };
            if (_keyService.HasMasterPassword())
            {
                configurationLock.ReportProperties.Add(new ReportProperty { Name = AppResources.F_Report_MasterKeyActive, Value = AppResources.F_Report_Yes });
            }
            else
            {
                configurationLock.ReportProperties.Add(new ReportProperty { Name = AppResources.F_Report_MasterKeyActive, Value = AppResources.F_Report_No });
            }
            if (_keyService.HasServicePassword())
            {
                configurationLock.ReportProperties.Add(new ReportProperty { Name = AppResources.F_Report_ServiceKeyActive, Value = AppResources.F_Report_Yes });
            }
            else
            {
                configurationLock.ReportProperties.Add(new ReportProperty { Name = AppResources.F_Report_ServiceKeyActive, Value = AppResources.F_Report_No });
            }

            // ----------
            // IOperatingModes
            // ----------
            {
                ReportFeature operatingModes = new ReportFeature { Name = AppResources.F_Report_OperatingModes };
                var operatingMode = _operatingModeData.GetCurrentReportOperatingModeName();
                operatingModes.ReportProperties.Add(new ReportProperty { Name = AppResources.F_Report_Mode, Value = operatingMode });

                //configurationLock.ReportProperties.Add(new ReportProperty { Name = AppResources.F_Report_OperatingModes, Value = GetConfigurationLock(permissionDictionary, "Operating Modes") });
                report.Add(operatingModes);
            }

            // ----------
            // IOperatingCurrent
            // ----------
            if (_operatingCurrentFeature.Info.IsAvailable)
            {
                ReportFeature operatingCurrent = new ReportFeature(_operatingCurrentFeature, AppResources.F_Report_OperatingCurrent);
                if (_operatingCurrentFeature.OperatingCurrentMode == OperatingCurrentMode.FixedCurrent)
                {
                    operatingCurrent.ReportProperties.Add(new ReportProperty { Name = AppResources.F_Report_Mode, Value = AppResources.F_Report_FixedCurrent });
                    operatingCurrent.ReportProperties.Add(new ReportProperty { Name = AppResources.F_Report_OperatingCurrent, Value = _operatingCurrentFeature.OperatingCurrent + " " + AppResources.F_Report_MilliAmpere });
                }
                else
                {
                    operatingCurrent.ReportProperties.Add(new ReportProperty { Name = AppResources.F_Report_Mode, Value = AppResources.F_Report_LEDSet2 });
                }

                configurationLock.ReportProperties.Add(new ReportProperty { Name = AppResources.F_Report_OperatingCurrent, Value = GetConfigurationLock(permissionDictionary, "Operating Current") });
                report.Add(operatingCurrent);
            }

            // ----------
            // ITuningFactorFeature
            // ----------
            if (_tuningFactorFeature.Info.IsAvailable ? true : IsAvailableAdvanced(configuration, "TF-0:Enable"))
            {
                ReportFeature tuningFactor = new ReportFeature(_tuningFactorFeature, AppResources.F_Report_TuningFactor);
                if (_tuningFactorFeature.Info.IsEnabled)
                {
                    tuningFactor.ReportProperties.Add(new ReportProperty { Name = AppResources.F_Report_Active, Value = AppResources.F_Report_Yes });
                    tuningFactor.ReportProperties.Add(new ReportProperty { Name = AppResources.F_Report_TuningLevel, Value = _tuningFactorFeature.TuningFactor + AppResources.F_Report_Percentage });
                    tuningFactor.ReportProperties.Add(new ReportProperty { Name = AppResources.F_Report_MaximumLimit, Value = _tuningFactorFeature.MaximumTuningFactor + AppResources.F_Report_Percentage });
                    tuningFactor.ReportProperties.Add(new ReportProperty { Name = AppResources.F_Report_MinimumLimit, Value = _tuningFactorFeature.MinimumTuningFactor + AppResources.F_Report_Percentage });
                    tuningFactor.ReportProperties.Add(new ReportProperty { Name = AppResources.F_Report_ReferenceLumen, Value = _tuningFactorFeature.ReferenceLumenOutput + " " + AppResources.F_Report_Lumen });
                }
                else
                {
                    tuningFactor.ReportProperties.Add(new ReportProperty { Name = AppResources.F_Report_Active, Value = AppResources.F_Report_No });
                }

                configurationLock.ReportProperties.Add(new ReportProperty { Name = AppResources.F_Report_TuningFactorAndLimits, Value = GetConfigurationLock(permissionDictionary, "Tuning Factor: Limits / Lumen") });
                configurationLock.ReportProperties.Add(new ReportProperty { Name = AppResources.F_Report_TuningLevel, Value = GetConfigurationLock(permissionDictionary, "Tuning Factor: Level") });
                report.Add(tuningFactor);
            }

            // ----------
            // ICostantLumen
            // ----------
            if (_constantLumen.Info.IsAvailable)
            {
                ReportFeature costantLumen = new ReportFeature(_constantLumen, AppResources.F_Report_CostantLumen);
                if (_constantLumen.Info.IsEnabled)
                {
                    costantLumen.ReportProperties.Add(new ReportProperty { Name = AppResources.F_Report_Active, Value = AppResources.F_Report_Yes });
                    costantLumen.ReportProperties.Add(new ReportProperty { Name = AppResources.F_Report_Step + " 1", Value = CreateOutputValue(_constantLumen.Times.Element1, AppResources.F_Report_KiloWattHour, _constantLumen.AdjustmentLevels.Element1, AppResources.F_Report_Percentage) });
                    if (_constantLumen.Times.Element2 != _constantLumen.TimesOff.Element2)
                    {
                        costantLumen.ReportProperties.Add(new ReportProperty { Name = AppResources.F_Report_Step + " 2", Value = CreateOutputValue(_constantLumen.Times.Element2, AppResources.F_Report_KiloWattHour, _constantLumen.AdjustmentLevels.Element2, AppResources.F_Report_Percentage) });
                    }
                    if (_constantLumen.Times.Element3 != _constantLumen.TimesOff.Element3)
                    {
                        costantLumen.ReportProperties.Add(new ReportProperty { Name = AppResources.F_Report_Step + " 3", Value = CreateOutputValue(_constantLumen.Times.Element3, AppResources.F_Report_KiloWattHour, _constantLumen.AdjustmentLevels.Element3, AppResources.F_Report_Percentage) });
                    }
                    if (_constantLumen.Times.Element4 != _constantLumen.TimesOff.Element4)
                    {
                        costantLumen.ReportProperties.Add(new ReportProperty { Name = AppResources.F_Report_Step + " 4", Value = CreateOutputValue(_constantLumen.Times.Element4, AppResources.F_Report_KiloWattHour, _constantLumen.AdjustmentLevels.Element4, AppResources.F_Report_Percentage) });
                    }
                    if (_constantLumen.Times.Element5 != _constantLumen.TimesOff.Element5)
                    {
                        costantLumen.ReportProperties.Add(new ReportProperty { Name = AppResources.F_Report_Step + " 5", Value = CreateOutputValue(_constantLumen.Times.Element5, AppResources.F_Report_KiloWattHour, _constantLumen.AdjustmentLevels.Element5, AppResources.F_Report_Percentage) });
                    }
                    if (_constantLumen.Times.Element6 != _constantLumen.TimesOff.Element6)
                    {
                        costantLumen.ReportProperties.Add(new ReportProperty { Name = AppResources.F_Report_Step + " 6", Value = CreateOutputValue(_constantLumen.Times.Element6, AppResources.F_Report_KiloWattHour, _constantLumen.AdjustmentLevels.Element6, AppResources.F_Report_Percentage) });
                    }
                    if (_constantLumen.Times.Element7 != _constantLumen.TimesOff.Element7)
                    {
                        costantLumen.ReportProperties.Add(new ReportProperty { Name = AppResources.F_Report_Step + " 7", Value = CreateOutputValue(_constantLumen.Times.Element7, AppResources.F_Report_KiloWattHour, _constantLumen.AdjustmentLevels.Element7, AppResources.F_Report_Percentage) });
                    }
                    if (_constantLumen.Times.Element8 != _constantLumen.TimesOff.Element8)
                    {
                        costantLumen.ReportProperties.Add(new ReportProperty { Name = AppResources.F_Report_Step + " 8", Value = CreateOutputValue(_constantLumen.Times.Element8, AppResources.F_Report_KiloWattHour, _constantLumen.AdjustmentLevels.Element8, AppResources.F_Report_Percentage) });
                    }
                }
                else
                {
                    costantLumen.ReportProperties.Add(new ReportProperty { Name = AppResources.F_Report_Active, Value = AppResources.F_Report_No });
                }

                configurationLock.ReportProperties.Add(new ReportProperty { Name = AppResources.F_Report_CostantLumen, Value = GetConfigurationLock(permissionDictionary, "Constant Lumen") });
                report.Add(costantLumen);
            }

            // ----------
            // ILampOperatingTime
            // ----------
            if (_lampOperatingTime.Info.IsAvailable)
            {
                ReportFeature lampOperatingTime = new ReportFeature(_lampOperatingTime, AppResources.F_Report_LampOperatingTime);
                lampOperatingTime.ReportProperties.Add(new ReportProperty { Name = AppResources.F_Report_Time, Value = _lampOperatingTime.LampOperationCounter / 60 + " " + AppResources.F_Report_Hour });

                configurationLock.ReportProperties.Add(new ReportProperty { Name = AppResources.F_Report_LampOperatingTime, Value = GetConfigurationLock(permissionDictionary, "Lamp Operating Time") });
                report.Add(lampOperatingTime);
            }

            // ----------
            // IEndOfLife
            // ----------
            if (_endOfLife.Info.IsAvailable)
            {
                ReportFeature endOfLife = new ReportFeature(_endOfLife, AppResources.F_Report_EndOfLife);
                if (_endOfLife.Info.IsEnabled)
                {
                    endOfLife.ReportProperties.Add(new ReportProperty { Name = AppResources.F_Report_Active, Value = AppResources.F_Report_Yes });
                    endOfLife.ReportProperties.Add(new ReportProperty { Name = AppResources.F_Report_LifeTime, Value = _endOfLife.EndOfLifeTime + " " + AppResources.F_Report_KiloWattHour });
                }
                else
                {
                    endOfLife.ReportProperties.Add(new ReportProperty { Name = AppResources.F_Report_Active, Value = AppResources.F_Report_No });
                }

                configurationLock.ReportProperties.Add(new ReportProperty { Name = AppResources.F_Report_EndOfLife, Value = GetConfigurationLock(permissionDictionary, "End Of Life") });
                report.Add(endOfLife);
            }

            // ----------
            // IThermalProtection
            // ----------
            if (_thermalProtection.Info.IsAvailable)
            {
                ReportFeature thermalProtection = new ReportFeature(_thermalProtection, AppResources.F_Report_ThermalProtection);
                if (_thermalProtection.Info.IsEnabled)
                {
                    thermalProtection.ReportProperties.Add(new ReportProperty { Name = AppResources.F_Report_Active, Value = AppResources.F_Report_Yes });
                    thermalProtection.ReportProperties.Add(new ReportProperty { Name = AppResources.F_Report_Mode, Value = _thermalProtection.ThermalProtectionMode == ThermalProtectionMode.ResistorBased ? AppResources.F_Report_ResistorBased : AppResources.F_Report_TemperatureBased });
                    if (_thermalProtection.ThermalProtectionMode == ThermalProtectionMode.ResistorBased)
                    {
                        thermalProtection.ReportProperties.Add(new ReportProperty { Name = AppResources.F_Report_StartDerating, Value = _thermalProtection.StartDeratingResistance + " " + AppResources.F_Report_Ohm });
                        thermalProtection.ReportProperties.Add(new ReportProperty { Name = AppResources.F_Report_EndDerating, Value = _thermalProtection.EndDeratingResistance + " " + AppResources.F_Report_Ohm });
                        if (_thermalProtection.ShutOff)
                        {
                            thermalProtection.ReportProperties.Add(new ReportProperty { Name = AppResources.F_Report_ShutOff, Value = AppResources.F_Report_Yes });
                            thermalProtection.ReportProperties.Add(new ReportProperty { Name = AppResources.F_Report_ShutOffValue, Value = _thermalProtection.ShutOffValue + " " + AppResources.F_Report_Ohm });
                        }
                        else
                        {
                            thermalProtection.ReportProperties.Add(new ReportProperty { Name = AppResources.F_Report_ShutOff, Value = AppResources.F_Report_No });
                        }
                        thermalProtection.ReportProperties.Add(new ReportProperty { Name = AppResources.F_Report_DeratingLevel, Value = _thermalProtection.FinalPowerReductionLevel + AppResources.F_Report_Percentage });
                    }
                }
                else
                {
                    thermalProtection.ReportProperties.Add(new ReportProperty { Name = AppResources.F_Report_Active, Value = AppResources.F_Report_No });
                }

                configurationLock.ReportProperties.Add(new ReportProperty { Name = AppResources.F_Report_ThermalProtection, Value = GetConfigurationLock(permissionDictionary, "Thermal Protection") });
                report.Add(thermalProtection);
            }

            // ----------
            // IDexalPSU
            // ----------
            if (_dexalPSU.Info.IsAvailable)
            {
                ReportFeature dexalPSU = new ReportFeature(_dexalPSU, AppResources.F_Report_DexalPSU);
                if (_dexalPSU.Info.IsEnabled)
                {
                    dexalPSU.ReportProperties.Add(new ReportProperty { Name = AppResources.F_Report_Enabled, Value = AppResources.F_Report_Yes });
                }
                else
                {
                    dexalPSU.ReportProperties.Add(new ReportProperty { Name = AppResources.F_Report_Enabled, Value = AppResources.F_Report_No });
                }

                configurationLock.ReportProperties.Add(new ReportProperty { Name = AppResources.F_Report_DexalPSU, Value = GetConfigurationLock(permissionDictionary, "Dexal PSU") });
                report.Add(dexalPSU);
            }

            // ----------
            // IDimToDark
            // ----------
            if (_dimToDark.Info.IsAvailable)
            {
                ReportFeature dimToDark = new ReportFeature(_dimToDark, AppResources.F_Report_DimToDark);
                
                dimToDark.ReportProperties.Add(new ReportProperty { Name = AppResources.F_Report_Active, Value = _dimToDark.Active ? AppResources.F_Report_Yes : AppResources.F_Report_No });

                configurationLock.ReportProperties.Add(new ReportProperty { Name = AppResources.F_Report_DimToDark, Value = GetConfigurationLock(permissionDictionary, "Dim To Dark") });
                report.Add(dimToDark);
            }

            // ----------
            // IDriverGuard
            // ----------
            if (_driverGuard.Info.IsAvailable)
            {
                ReportFeature driverGuard = new ReportFeature(_driverGuard, AppResources.F_Report_DriverGuard);
                if (_driverGuard.Info.IsEnabled)
                {
                    driverGuard.ReportProperties.Add(new ReportProperty { Name = AppResources.F_Report_Enabled, Value = AppResources.F_Report_Yes });
                    driverGuard.ReportProperties.Add(new ReportProperty { Name = AppResources.F_Report_PrestartDerating, Value = _driverGuard.PrestartDerating + AppResources.F_Report_DegreeCelsius });
                    driverGuard.ReportProperties.Add(new ReportProperty { Name = AppResources.F_Report_DeratingLevel, Value = _driverGuard.DeratingLevel + AppResources.F_Report_Percentage });
                    driverGuard.ReportProperties.Add(new ReportProperty { Name = AppResources.F_Report_MaximumPower, Value = _driverGuard.PowerDeratingLevel + " " + AppResources.F_Report_Watt });
                }
                else
                {
                    driverGuard.ReportProperties.Add(new ReportProperty { Name = AppResources.F_Report_Enabled, Value = AppResources.F_Report_No });
                }

                configurationLock.ReportProperties.Add(new ReportProperty { Name = AppResources.F_Report_DriverGuard, Value = GetConfigurationLock(permissionDictionary, "Driver Guard") });
                report.Add(driverGuard);
            }            

            // ----------
            // IAstroDimFeature
            // ----------
            if (_astroDimFeature.Info.IsAvailable
                && (_operatingModeData.GetCurrentReportOperatingModeEnum() == OperatingModeReportEnum.AstroDim
                        || _operatingModeData.GetCurrentReportOperatingModeEnum() == OperatingModeReportEnum.StepDim_AstroDim_Dali_WiringSelection
                        || _operatingModeData.GetCurrentReportOperatingModeEnum() == OperatingModeReportEnum.AstroDim_Dali
                        || _operatingModeData.GetCurrentReportOperatingModeEnum() == OperatingModeReportEnum.AstroDim_PresenceDetection_Dali
                        || _operatingModeData.GetCurrentReportOperatingModeEnum() == OperatingModeReportEnum.Autodetect_AstroDim_StepDimInverse_Dali))
            {
                ReportFeature astroDim = new ReportFeature(_astroDimFeature, AppResources.F_Report_AstroDim);

                astroDim.ReportProperties.Add(new ReportProperty { Name = AppResources.F_Report_Submode, Value = _astroDimFeature.AstroDimMode == AstroDimMode.AstroBased ? AppResources.F_Report_AstroBased : AppResources.F_Report_TimeBased });
                astroDim.ReportProperties.Add(new ReportProperty { Name = AppResources.F_Report_Location, Value = _astroDimFeature.Location.Name });
                astroDim.ReportProperties.Add(new ReportProperty { Name = AppResources.F_Report_Latitude, Value = _astroDimFeature.Location.LatitudeValue.ToString() });
                astroDim.ReportProperties.Add(new ReportProperty { Name = AppResources.F_Report_Longitude, Value = _astroDimFeature.Location.LongitudeValue.ToString() });

                TimeValue tv;
                if (_astroDimFeature.AstroDimMode == AstroDimMode.AstroBased)
                {
                    tv = _astroDimFeature.GetAstroBasedReferenceScheduleTime();
                    astroDim.ReportProperties.Add(new ReportProperty { Name = AppResources.F_Report_TimeZone, Value = _astroDimFeature.Location.GetUTCFromOffset(_astroDimFeature.Location.Offset) });
                }
                else
                {
                    tv = _astroDimFeature.GetTimeBasedReferenceScheduleTime();
                }
                astroDim.ReportProperties.Add(new ReportProperty { Name = AppResources.F_Report_SwitchOnFadeTime, Value = ConstantListValues.Instance.AstroDimSwitchOnRangeValues().Find(x => x.Key == _astroDimFeature.SwitchOnFadeTime).Value + " " + AppResources.F_Report_mmss });
                astroDim.ReportProperties.Add(new ReportProperty { Name = AppResources.F_Report_AstroDimFadeTime, Value = ConstantListValues.Instance.AstroDimRangeValues().Find(x => x.Key == _astroDimFeature.AstroDimFadeTime).Value + " " + AppResources.F_Report_mmss });
                var switchOff = ConstantListValues.Instance.AstroDimSwitchOffRangeValues().Find(x => x.Key == _astroDimFeature.SwitchOffFadeTime).Value;
                astroDim.ReportProperties.Add(new ReportProperty { Name = AppResources.F_Report_SwitchOffFadeTime, Value = switchOff + (switchOff != "OFF" ? (" " + AppResources.F_Report_mmss) : "") });
                astroDim.ReportProperties.Add(new ReportProperty { Name = AppResources.F_Report_Step + " 1", Value = CreateOutputValue(AppResources.F_Report_ON, "", _astroDimFeature.DimmingLevels.Element1, AppResources.F_Report_Percentage) });
                astroDim.ReportProperties.Add(new ReportProperty { Name = AppResources.F_Report_Step + " 2", Value = CreateOutputValue(tv.Element2.ToString("HH:mm", CultureInfo.InvariantCulture), AppResources.F_Report_hhmm, _astroDimFeature.DimmingLevels.Element2, AppResources.F_Report_Percentage) });
                astroDim.ReportProperties.Add(new ReportProperty { Name = AppResources.F_Report_Step + " 3", Value = CreateOutputValue(tv.Element3.ToString("HH:mm", CultureInfo.InvariantCulture), AppResources.F_Report_hhmm, _astroDimFeature.DimmingLevels.Element3, AppResources.F_Report_Percentage) });
                astroDim.ReportProperties.Add(new ReportProperty { Name = AppResources.F_Report_Step + " 4", Value = CreateOutputValue(tv.Element4.ToString("HH:mm", CultureInfo.InvariantCulture), AppResources.F_Report_hhmm, _astroDimFeature.DimmingLevels.Element4, AppResources.F_Report_Percentage) });
                astroDim.ReportProperties.Add(new ReportProperty { Name = AppResources.F_Report_Step + " 5", Value = CreateOutputValue(tv.Element5.ToString("HH:mm", CultureInfo.InvariantCulture), AppResources.F_Report_hhmm, _astroDimFeature.DimmingLevels.Element5, AppResources.F_Report_Percentage) });
                astroDim.ReportProperties.Add(new ReportProperty { Name = AppResources.F_Report_Step + " 6", Value = CreateOutputValue(AppResources.F_Report_OFF, "", _astroDimFeature.DimmingLevels.Element6, AppResources.F_Report_Percentage) });

                configurationLock.ReportProperties.Add(new ReportProperty { Name = AppResources.F_Report_AstroDim, Value = GetConfigurationLock(permissionDictionary, "Diming Levels") });
                report.Add(astroDim);
            }

            // ----------
            // ISoftSwitchOff
            // ----------
            if (_softSwitchOff.Info.IsAvailable)
            {
                ReportFeature softSwitchOff = new ReportFeature(_softSwitchOff, AppResources.F_Report_SoftSwitchOff);
                if (_softSwitchOff.Info.IsEnabled)
                {
                    softSwitchOff.ReportProperties.Add(new ReportProperty { Name = AppResources.F_Report_Enabled, Value = AppResources.F_Report_Yes });
                    var softSwitchOffTime = ConstantListValues.Instance.DaliFadeTimes.Find(x => x.Key == _softSwitchOff.FadeTime).Value;
                    softSwitchOff.ReportProperties.Add(new ReportProperty { Name = AppResources.F_Report_FadeTime, Value = softSwitchOffTime + " " + AppResources.F_Report_seconds });
                }
                else
                {
                    softSwitchOff.ReportProperties.Add(new ReportProperty { Name = AppResources.F_Report_Enabled, Value = AppResources.F_Report_No });
                }

                configurationLock.ReportProperties.Add(new ReportProperty { Name = AppResources.F_Report_SoftSwitchOff, Value = GetConfigurationLock(permissionDictionary, "Soft Switch Off") });
                report.Add(softSwitchOff);
            }

            // ----------
            // IStepDimFeature
            // ----------
            if (_stepDimFeature.Info.IsAvailable
                    && (_operatingModeData.GetCurrentReportOperatingModeEnum() == OperatingModeReportEnum.StepDim_AstroDim_Dali_WiringSelection
                        || _operatingModeData.GetCurrentReportOperatingModeEnum() == OperatingModeReportEnum.StepDim_Dali
                        || _operatingModeData.GetCurrentReportOperatingModeEnum() == OperatingModeReportEnum.StepDimInverse_Dali
                        || _operatingModeData.GetCurrentReportOperatingModeEnum() == OperatingModeReportEnum.Autodetect_OnOff_StepDimInverse_Dali
                        || _operatingModeData.GetCurrentReportOperatingModeEnum() == OperatingModeReportEnum.Autodetect_AstroDim_StepDimInverse_Dali))
            {
                ReportFeature stepDim = new ReportFeature(_stepDimFeature, AppResources.F_Report_StepDim);

                stepDim.ReportProperties.Add(new ReportProperty { Name = AppResources.F_Report_StepDimNominalLevel, Value = _stepDimFeature.NominalLevel + " " + AppResources.F_Report_Percentage });
                stepDim.ReportProperties.Add(new ReportProperty { Name = AppResources.F_Report_StepDimSDLevel, Value = _stepDimFeature.SdLevel + " " + AppResources.F_Report_Percentage });
                var startFadeTime = ConstantListValues.Instance.FadeTimeRangeValues.Find(x => x.Key == _stepDimFeature.StartFadeTime).Value;
                stepDim.ReportProperties.Add(new ReportProperty { Name = AppResources.F_Report_StepDimStartFadeTime, Value = startFadeTime + " " + AppResources.F_Report_mmss });
                var holdTime = ConstantListValues.Instance.SwitchOnFadeTimes.Find(x => x.Key == _stepDimFeature.HoldTime).Value;
                stepDim.ReportProperties.Add(new ReportProperty { Name = AppResources.F_Report_StepDimHoldTime, Value = holdTime + " " + AppResources.F_Report_mmss });
                var endFadeTime = ConstantListValues.Instance.FadeTimeRangeValues.Find(x => x.Key == _stepDimFeature.EndFadeTime).Value;
                stepDim.ReportProperties.Add(new ReportProperty { Name = AppResources.F_Report_StepDimEndFadeTime, Value = endFadeTime + " " + AppResources.F_Report_mmss });
                var switchOn = ConstantListValues.Instance.SwitchOnFadeTimes.Find(x => x.Key == _stepDimFeature.StartupFadeTime).Value;
                stepDim.ReportProperties.Add(new ReportProperty { Name = AppResources.F_Report_StepDimSwitchOnFadeTime, Value = switchOn + " " + AppResources.F_Report_mmss });

                // No configurationLock
                report.Add(stepDim);
            }

            // ----------
            // IMainsDimFeature
            // ----------
            if (_mainsDimFeature.Info.IsAvailable
                    && _operatingModeData.GetCurrentReportOperatingModeEnum() == OperatingModeReportEnum.MainsDim_Dali)
            {
                ReportFeature mainsDim = new ReportFeature(_mainsDimFeature, AppResources.F_Report_MainsDim);

                mainsDim.ReportProperties.Add(new ReportProperty { Name = AppResources.F_Report_MainsDimStartVoltage, Value = _mainsDimFeature.StartVoltage + " " + AppResources.F_Report_Volt });
                mainsDim.ReportProperties.Add(new ReportProperty { Name = AppResources.F_Report_MainsDimStartLevel, Value = _mainsDimFeature.StartLevel + " " + AppResources.F_Report_Percentage });
                mainsDim.ReportProperties.Add(new ReportProperty { Name = AppResources.F_Report_MainsDimStopVoltage, Value = _mainsDimFeature.StopVoltage + " " + AppResources.F_Report_mmss });
                mainsDim.ReportProperties.Add(new ReportProperty { Name = AppResources.F_Report_MainsDimStopLevel, Value = _mainsDimFeature.StopLevel + " " + AppResources.F_Report_Volt });

                // No configurationLock
                report.Add(mainsDim);
            }

            // ----------
            // IPresenceDetection
            // ----------
            if (_presenceDetectionFeature.Info.IsAvailable
                && (_operatingModeData.GetCurrentReportOperatingModeEnum() == OperatingModeReportEnum.AstroDim_PresenceDetection_Dali
                        || _operatingModeData.GetCurrentReportOperatingModeEnum() == OperatingModeReportEnum.Autodetect_AstroDim_StepDimInverse_Dali))
            {
                ReportFeature presenceDetection = new ReportFeature(_presenceDetectionFeature, AppResources.F_Report_PresenceDetection);

                presenceDetection.ReportProperties.Add(new ReportProperty { Name = AppResources.F_Report_PresenceDetectionPDLevel, Value = _presenceDetectionFeature.PdLevel + " " + AppResources.F_Report_Percentage });
                var startFadeTime = ConstantListValues.Instance.FadeTimeRangeValues.Find(x => x.Key == _presenceDetectionFeature.StartFadeTime).Value;
                presenceDetection.ReportProperties.Add(new ReportProperty { Name = AppResources.F_Report_PresenceDetectionStartFadeTime, Value = startFadeTime + " " + AppResources.F_Report_mmss });
                var endFadeTime = ConstantListValues.Instance.FadeTimeRangeValues.Find(x => x.Key == _presenceDetectionFeature.EndFadeTime).Value;
                presenceDetection.ReportProperties.Add(new ReportProperty { Name = AppResources.F_Report_PresenceDetectionEndFadeTime, Value = endFadeTime + " " + AppResources.F_Report_mmss });
                var holdTime = ConstantListValues.Instance.SwitchOnFadeTimes.Find(x => x.Key == _presenceDetectionFeature.HoldTime).Value;
                presenceDetection.ReportProperties.Add(new ReportProperty { Name = AppResources.F_Report_PresenceDetectionHoldTime, Value = holdTime + " " + AppResources.F_Report_mmss });

                // No configurationLock
                report.Add(presenceDetection);
            }

            // ----------
            // IDaliPropertyData
            // ----------
            if (_daliPropertyData.IsAvailable)
            {
                ReportFeature daliPropertyData = new ReportFeature { Name = AppResources.F_Report_DaliPropertyData };
                daliPropertyData.ReportProperties.Add(new ReportProperty { Name = AppResources.F_Report_Active, Value = AppResources.F_Report_Yes });
                var fadeTime = ConstantListValues.Instance.DaliFadeTimes.Find(x => x.Key == _daliPropertyData.FadeTime).Value;
                daliPropertyData.ReportProperties.Add(new ReportProperty { Name = AppResources.F_Report_DaliPropertyFadeTime, Value = TimeSpan.FromSeconds(Double.Parse(fadeTime)).ToString(@"mm\:ss\:fff", CultureInfo.InvariantCulture) + " " + AppResources.F_Report_mmss });
                var fadeRate = ConstantListValues.Instance.DaliFadeRates.Find(x => x.Key == _daliPropertyData.FadeRate).Value;
                daliPropertyData.ReportProperties.Add(new ReportProperty { Name = AppResources.F_Report_DaliPropertyFadeRate, Value = fadeRate + " " + AppResources.F_Report_StepsPerSecond });
                daliPropertyData.ReportProperties.Add(new ReportProperty { Name = AppResources.F_Report_DaliPropertyMinLevel, Value = _daliPropertyData.MinLevel + "" });
                daliPropertyData.ReportProperties.Add(new ReportProperty { Name = AppResources.F_Report_DaliPropertyMaxLevel, Value = _daliPropertyData.MaxLevel + "" });
                daliPropertyData.ReportProperties.Add(new ReportProperty { Name = AppResources.F_Report_DaliPropertyPowerOnLevel, Value = _daliPropertyData.PowerOnLevel + "" });
                daliPropertyData.ReportProperties.Add(new ReportProperty { Name = AppResources.F_Report_DaliPropertySystemFailureLevel, Value = _daliPropertyData.SystemFailureLevel + "" });
                daliPropertyData.ReportProperties.Add(new ReportProperty { Name = AppResources.F_Report_DaliPropertyDimmingCurve, Value = _daliPropertyData.DimmingCurve == 0 ? AppResources.F_Logarithmic : AppResources.F_Linear });
                //daliPropertyData.ReportProperties.Add(new ReportProperty { Name = AppResources.F_Report_DaliPropertyAddress, Value = _daliPropertyData.Address == 0 ? AppResources.F_NotAssigned : AppResources.F_KeepExisting });

                // No configurationLock
                report.Add(daliPropertyData);
            }

            // ----------
            // IEmergencyMode
            // ----------
            if (_emergencyMode.Info.IsAvailable)
            {
                ReportFeature emergencyMode = new ReportFeature(_emergencyMode, AppResources.F_Report_EmergencyMode);
                if (_emergencyMode.DCLightLevel < 255)
                {
                    emergencyMode.ReportProperties.Add(new ReportProperty { Name = AppResources.F_Report_Active, Value = AppResources.F_Report_Yes });
                    emergencyMode.ReportProperties.Add(new ReportProperty { Name = AppResources.F_Report_DCLightLevel, Value = _emergencyMode.DCLightLevel + "" });
                    emergencyMode.ReportProperties.Add(new ReportProperty { Name = AppResources.F_Report_Dali_Lock, Value = _emergencyMode.LockDali ? AppResources.F_Report_Yes : AppResources.F_Report_No });
                }
                else
                {
                    emergencyMode.ReportProperties.Add(new ReportProperty { Name = AppResources.F_Report_Active, Value = AppResources.F_Report_No });
                    emergencyMode.ReportProperties.Add(new ReportProperty { Name = AppResources.F_Report_Dali_Lock, Value = _emergencyMode.LockDali ? AppResources.F_Report_Yes : AppResources.F_Report_No });
                }

                configurationLock.ReportProperties.Add(new ReportProperty { Name = AppResources.F_Report_EmergencyMode, Value = GetConfigurationLock(permissionDictionary, "Emergency Mode") });
                report.Add(emergencyMode);
            }

            // ----------
            // ITouchDim
            // ----------
            if (_touchDim.Info.IsAvailable)
            {
                ReportFeature touchDim = new ReportFeature(_touchDim, AppResources.F_Report_TouchDim);
                if (_touchDim.Info.IsEnabled)
                {
                    touchDim.ReportProperties.Add(new ReportProperty { Name = AppResources.F_Report_Active, Value = AppResources.F_Report_Yes });
                    touchDim.ReportProperties.Add(new ReportProperty { Name = AppResources.F_Report_TouchDimSwitch, Value = _touchDim.SwitchOnOff ? AppResources.F_Report_Yes : AppResources.F_Report_No });
                    touchDim.ReportProperties.Add(new ReportProperty { Name = AppResources.F_Report_TouchDimSetResetLevel, Value = _touchDim.SetResetLevel ? AppResources.F_Report_Yes : AppResources.F_Report_No });
                    touchDim.ReportProperties.Add(new ReportProperty { Name = AppResources.F_Report_TouchDimDim, Value = _touchDim.DimUpDown ? AppResources.F_Report_Yes : AppResources.F_Report_No });
                    touchDim.ReportProperties.Add(new ReportProperty { Name = AppResources.F_Report_TouchDimMainsPowerOn, Value = _touchDim.MainsPowerLevel + " " });
                    var touchDimFadeTime1 = ConstantListValues.Instance.TouchDimFadeTimes.Find(x => x.Key == _touchDim.FadeTime1).Value;                    
                    touchDim.ReportProperties.Add(new ReportProperty { Name = AppResources.F_Report_TouchDimOSFadeTime1, Value = TimeSpan.FromSeconds(Double.Parse(touchDimFadeTime1)).ToString(@"mm\:ss\.fff", CultureInfo.InvariantCulture) + " " + AppResources.F_Report_mmss });
                    touchDim.ReportProperties.Add(new ReportProperty { Name = AppResources.F_Report_TouchDimOSDaliLevel, Value = _touchDim.OperativeDaliLevel + " " });
                    var touchDimHoldTime2 = ConstantListValues.Instance.TouchDimHoldTimes1RangeValues.Find(x => x.Key == _touchDim.HoldTime2).Value;
                    touchDim.ReportProperties.Add(new ReportProperty { Name = AppResources.F_Report_TouchDimOSHoldTime2, Value = touchDimHoldTime2 == "" ? AppResources.Infinite : TimeSpan.FromSeconds(Double.Parse(touchDimHoldTime2)).ToString(@"hh\:mm\:ss", CultureInfo.InvariantCulture) + " " + AppResources.F_Report_hhmmss });
                    var touchDimFadeTime3 = ConstantListValues.Instance.TouchDimFadeTimes.Find(x => x.Key == _touchDim.FadeTime3).Value;
                    touchDim.ReportProperties.Add(new ReportProperty { Name = AppResources.F_Report_TouchDimOSFadeTime3, Value = TimeSpan.FromSeconds(Double.Parse(touchDimFadeTime3)).ToString(@"mm\:ss\.fff", CultureInfo.InvariantCulture) + " " + AppResources.F_Report_mmss });
                    var touchDimFadeTime4 = ConstantListValues.Instance.TouchDimFadeTimes.Find(x => x.Key == _touchDim.FadeTime4).Value;
                    touchDim.ReportProperties.Add(new ReportProperty { Name = AppResources.F_Report_TouchDimSFadeTime4, Value = TimeSpan.FromSeconds(Double.Parse(touchDimFadeTime4)).ToString(@"mm\:ss\.fff", CultureInfo.InvariantCulture) + " " + AppResources.F_Report_mmss });
                    touchDim.ReportProperties.Add(new ReportProperty { Name = AppResources.F_Report_TouchDimSDaliLevel, Value = _touchDim.StandbyDaliLevel + " " });
                    var touchDimHoldTime5 = ConstantListValues.Instance.TouchDimHoldTimes1RangeValues.Find(x => x.Key == _touchDim.HoldTime5).Value;
                    touchDim.ReportProperties.Add(new ReportProperty { Name = AppResources.F_Report_TouchDimOSHoldTime5, Value = touchDimHoldTime5 == "" ? AppResources.Infinite : TimeSpan.FromSeconds(Double.Parse(touchDimHoldTime5)).ToString(@"hh\:mm\:ss", CultureInfo.InvariantCulture) + " " + AppResources.F_Report_hhmmss });
                    touchDim.ReportProperties.Add(new ReportProperty { Name = AppResources.F_Report_TouchDimLS, Value = _touchDim.LsActive ? AppResources.F_Report_Yes : AppResources.F_Report_No });
                    touchDim.ReportProperties.Add(new ReportProperty { Name = AppResources.F_Report_TouchDimPD, Value = _touchDim.PdActive ? AppResources.F_Report_Yes : AppResources.F_Report_No });
                    touchDim.ReportProperties.Add(new ReportProperty { Name = AppResources.F_Report_TouchDimPDHoliday, Value = _touchDim.PdHolidayMode ? AppResources.F_Report_Yes : AppResources.F_Report_No });
                    touchDim.ReportProperties.Add(new ReportProperty { Name = AppResources.F_Report_TouchDimAutoDisablePD, Value = _touchDim.AutoDisablePdActive ? AppResources.F_Report_Yes : AppResources.F_Report_No });
                    //Only for TD5
                    var mainsPowerColour = ConstantListValues.Instance.TouchDimColorRangeValues.Find(x => x.Key == _touchDim.MainsPowerColour).Value;
                    touchDim.ReportProperties.Add(new ReportProperty { Name = AppResources.F_Report_TouchDimMainsPC, Value = mainsPowerColour == "" ? AppResources.Save : mainsPowerColour + "" });
                    var switchOnColour = ConstantListValues.Instance.TouchDimColorRangeValues.Find(x => x.Key == _touchDim.SwitchOnColour).Value;
                    touchDim.ReportProperties.Add(new ReportProperty { Name = AppResources.F_Report_TouchDimSwitchColour, Value = switchOnColour == "" ? AppResources.Save : switchOnColour + "" });
                    touchDim.ReportProperties.Add(new ReportProperty { Name = AppResources.F_Report_TouchDimPowerLevel, Value = _touchDim.MainsPowerLevel2 == 255 ? AppResources.Save : _touchDim.MainsPowerLevel2 + "" });
                    touchDim.ReportProperties.Add(new ReportProperty { Name = AppResources.F_Report_TouchDimSwitchOnLevel, Value = _touchDim.OperativeLevel == 255 ? AppResources.Save : _touchDim.OperativeLevel + "" });
                    touchDim.ReportProperties.Add(new ReportProperty { Name = AppResources.F_Report_TouchDim2ShortPushEnable, Value = _touchDim.SwitchOnOff2 ? AppResources.F_Report_Yes : AppResources.F_Report_No });
                    touchDim.ReportProperties.Add(new ReportProperty { Name = AppResources.F_Report_TouchDim2DoublePushEnable, Value = _touchDim.SetResetLevel2 ? AppResources.F_Report_Yes : AppResources.F_Report_No });
                    touchDim.ReportProperties.Add(new ReportProperty { Name = AppResources.F_Report_TouchDim2LongPushEnable, Value = _touchDim.DimUpDown2 ? AppResources.F_Report_Yes : AppResources.F_Report_No });
                }
                else
                {
                    touchDim.ReportProperties.Add(new ReportProperty { Name = AppResources.F_Report_Active, Value = AppResources.F_Report_No });
                }

                configurationLock.ReportProperties.Add(new ReportProperty { Name = AppResources.F_Report_TouchDim, Value = GetConfigurationLock(permissionDictionary, "TouchDIM") });
                report.Add(touchDim);
            }

            // ----------
            // ICorridor
            // ----------
            if (_corridor.Info.IsAvailable)
            {
                ReportFeature corridor = new ReportFeature(_corridor, AppResources.F_Report_Corridor);
                if (_corridor.Info.IsEnabled)
                {
                    corridor.ReportProperties.Add(new ReportProperty { Name = AppResources.F_Report_Active, Value = AppResources.F_Report_Yes });
                    corridor.ReportProperties.Add(new ReportProperty { Name = AppResources.F_Report_TouchDimSwitch, Value = _corridor.SwitchOnOff ? AppResources.F_Report_Yes : AppResources.F_Report_No });
                    corridor.ReportProperties.Add(new ReportProperty { Name = AppResources.F_Report_TouchDimSetResetLevel, Value = _corridor.SetResetLevel ? AppResources.F_Report_Yes : AppResources.F_Report_No });
                    corridor.ReportProperties.Add(new ReportProperty { Name = AppResources.F_Report_TouchDimDim, Value = _corridor.DimUpDown ? AppResources.F_Report_Yes : AppResources.F_Report_No });
                    var corridorFadeTime1 = ConstantListValues.Instance.TouchDimFadeTimes.Find(x => x.Key == _touchDim.FadeTime1).Value;
                    corridor.ReportProperties.Add(new ReportProperty { Name = AppResources.F_Report_CorridorOFadeTime, Value = TimeSpan.FromSeconds(Double.Parse(corridorFadeTime1)).ToString(@"mm\:ss\.fff", CultureInfo.InvariantCulture) + " " + AppResources.F_Report_mmss });
                    var corridorHoldTime2 = ConstantListValues.Instance.TouchDimHoldTimes2RangeValues.Find(x => x.Key == _corridor.HoldTime2).Value;
                    var test = TimeSpan.FromSeconds(Double.Parse(corridorHoldTime2)).ToString(@"hh\:mm\:ss", CultureInfo.InvariantCulture);
                    corridor.ReportProperties.Add(new ReportProperty { Name = AppResources.F_Report_CorridorOHoldTime, Value = corridorHoldTime2 == "" ? AppResources.Infinite : TimeSpan.FromSeconds(Double.Parse(corridorHoldTime2)).ToString(@"hh\:mm\:ss", CultureInfo.InvariantCulture) + " " + AppResources.F_Report_hhmmss });                                  
                    corridor.ReportProperties.Add(new ReportProperty { Name = AppResources.F_Report_CorridorODali, Value = _corridor.StandbyDaliLevel + " " });
                    var corridorFadeTime3 = ConstantListValues.Instance.TouchDimFadeTimes.Find(x => x.Key == _corridor.FadeTime3).Value;
                    corridor.ReportProperties.Add(new ReportProperty { Name = AppResources.F_Report_CorridorS1FadeTime, Value = TimeSpan.FromSeconds(Double.Parse(corridorFadeTime3)).ToString(@"mm\:ss\.fff", CultureInfo.InvariantCulture) + " " + AppResources.F_Report_mmss });
                    var corridorHoldTime4 = ConstantListValues.Instance.TouchDimHoldTimes2RangeValues.Find(x => x.Key == _corridor.HoldTime4).Value;
                    corridor.ReportProperties.Add(new ReportProperty { Name = AppResources.F_Report_CorridorS1HoldTime, Value = corridorHoldTime4 == "" ? AppResources.Infinite : TimeSpan.FromSeconds(Double.Parse(corridorHoldTime4)).ToString(@"hh\:mm\:ss", CultureInfo.InvariantCulture) + " " + AppResources.F_Report_hhmmss });
                    corridor.ReportProperties.Add(new ReportProperty { Name = AppResources.F_Report_CorridorS1Dali, Value = _corridor.OperativeDaliLevel + " " });
                    var corridorFadeTime5 = ConstantListValues.Instance.TouchDimFadeTimes.Find(x => x.Key == _corridor.FadeTime5).Value;
                    corridor.ReportProperties.Add(new ReportProperty { Name = AppResources.F_Report_CorridorS2FadeTime, Value = TimeSpan.FromSeconds(Double.Parse(corridorFadeTime5)).ToString(@"mm\:ss\.fff", CultureInfo.InvariantCulture) + " " + AppResources.F_Report_mmss });
                    var corridorHoldTime6 = ConstantListValues.Instance.TouchDimHoldTimes2RangeValues.Find(x => x.Key == _corridor.HoldTime6).Value;
                    corridor.ReportProperties.Add(new ReportProperty { Name = AppResources.F_Report_CorridorS2HoldTime, Value = corridorHoldTime6 == "" ? AppResources.Infinite : TimeSpan.FromSeconds(Double.Parse(corridorHoldTime6)).ToString(@"hh\:mm\:ss", CultureInfo.InvariantCulture) + " " + AppResources.F_Report_hhmmss });
                    corridor.ReportProperties.Add(new ReportProperty { Name = AppResources.F_Report_CorridorS2Dali, Value = _corridor.Standby2DaliLevel + " " });
                    var startupBehaviour = ConstantListValues.Instance.TouchDimStartupBehaviourValues.Find(x => x.Key == _corridor.StartupBehaviour).Value;
                    corridor.ReportProperties.Add(new ReportProperty { Name = AppResources.F_Report_CorridorStartupBehaviour, Value = startupBehaviour + " " });                 
                }
                else
                {
                    corridor.ReportProperties.Add(new ReportProperty { Name = AppResources.F_Report_Active, Value = AppResources.F_Report_No });
                }

                configurationLock.ReportProperties.Add(new ReportProperty { Name = AppResources.F_Report_Corridor, Value = GetConfigurationLock(permissionDictionary, "Corridor") });
                report.Add(corridor);
            }

            report.Add(configurationLock);

            return report;
        }

        private string CreateOutputValue(int firstValue, string firstUnitMeasure, int secondValue, string secondUnitMeasure)
        {
            return CreateOutputValue(firstValue.ToString(), firstUnitMeasure, secondValue, secondUnitMeasure);
        }
        private string CreateOutputValue(string firstValue, string firstUnitMeasure, int secondValue, string secondUnitMeasure)
        {
            StringBuilder sb = new StringBuilder("");
            sb.Append(firstValue);
            sb.Append(firstUnitMeasure);
            sb.Append(" ");
            sb.Append(secondValue);
            sb.Append(secondUnitMeasure);
            return sb.ToString();
        }
    }

    public class ReportFeature
    {
        public ReportFeature()
        {
            Enabled = true;
            Available = true;
            ReportProperties = new List<ReportProperty>();
        }
        public ReportFeature(IFeature feature, string name)
        {
            Name = name;
            Enabled = feature.Info.IsEnabled;
            Available = feature.Info.IsAvailable;
            ReportProperties = new List<ReportProperty>();
        }

        public bool Available { get; set; }
        public bool Enabled { get; set; }
        public string Name { get; set; }

        public IList<ReportProperty> ReportProperties { get; set; }
    }

    public class ReportProperty
    {
        public string Name { get; set; }
        public string Value { get; set; }
    }


    public class FlatReportProperty
    {
        public FlatPropertyType Type { get; set; }
        public string Element1 { get; set; }
        public string Element2 { get; set; }
    }


    public enum FlatPropertyType
    {

        Title,
        KeyValue

    }
}
