namespace Osram.T4TField.Utils
{
    public static class SettingsKeys
    {
        //Language
        public const string Language = "language";

        public const string PrivacyAgreementAccepted = "PrivacyAgreementAccepted";
        public const string ParameterCacheKey = "ParameterCacheKey";


        public const string AgreementsAccepted = "AgreementsAccepted";

    }
}