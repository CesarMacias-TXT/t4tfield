﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Osram.T4TField.Utils
{
    /// <summary>
    /// Represents extension class which contains methods used to convert the data type of values.
    /// </summary>
    public static class DateTimeExtension
    {
        /// <summary>
        /// Function to extract minutes from date time value in Astro dim.
        /// </summary>
        /// <param name="dateValue">The date value.</param>
        /// <returns>Returns minutes.</returns>
        public static double ADTimeToDouble(this DateTime dateValue)
        {
            double returnValue = 0;

            TimeSpan ts = dateValue.TimeOfDay;
            double hourseValue = Convert.ToDouble(ts.Hours, CultureInfo.InvariantCulture);
            int minutesValue = Convert.ToInt32(ts.Minutes, CultureInfo.InvariantCulture);
            double dateMinutesValue = Math.Round(double.Parse("0." + minutesValue.ToString("00")) * 1.67, 2);
            returnValue = hourseValue + dateMinutesValue;
            if (dateValue.ToString("tt", CultureInfo.InvariantCulture).Contains("PM"))
            {
                returnValue = returnValue - 12;
            }
            else
            {
                returnValue = returnValue + 12;
            }

            return returnValue;
        }

        /// <summary>
        /// Function to find out day from the date value  .
        /// </summary>
        /// <param name="dateValue">The date value.</param>
        /// <returns>Returns day.</returns>
        public static double DayToDouble(this DateTime dateValue)
        {
            double returnValue = 0;

            int dayOfYear = dateValue.DayOfYear;
            returnValue = dayOfYear * 0.0329;

            return returnValue;
        }

        /// <summary>
        /// Function to find out time from date.
        /// </summary>
        /// <param name="dateValue">The date value.</param>
        /// <returns>Return time.</returns>
        public static double TimeToDouble(this DateTime dateValue)
        {
            double returnValue = 0;

            TimeSpan ts = dateValue.TimeOfDay;
            double hourseValue = Convert.ToDouble(ts.Hours, CultureInfo.InvariantCulture);
            int minutesValue = Convert.ToInt32(ts.Minutes, CultureInfo.InvariantCulture);
            double dateMinutesValue = Math.Round(double.Parse("0." + minutesValue.ToString("00")) * 1.67, 2);
            returnValue = hourseValue + dateMinutesValue;

            return returnValue;
        }
    }
}
