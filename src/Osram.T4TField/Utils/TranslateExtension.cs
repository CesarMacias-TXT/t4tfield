﻿using System;
using MvvmCross;
using Osram.T4TField.Contracts;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Osram.T4TField.Utils
{
    [ContentProperty("Text")]
    public class TranslateExtension : IMarkupExtension
    {
        private readonly ILocalizationService _localizationService;

        public TranslateExtension()
        {
            _localizationService = Mvx.IoCProvider.Resolve<ILocalizationService>();
        }

        public string Text { get; set; }

        public object ProvideValue(IServiceProvider serviceProvider)
        {
            if (Text == null)
                return "";

            return _localizationService.GetString(Text);
        }
    }
}