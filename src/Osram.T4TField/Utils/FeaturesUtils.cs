﻿using MvvmCross;
using Osram.T4TField.ViewModels;
using Osram.T4TField.ViewModels.FeaturesViewModel;
using Osram.TFTBackend.DataModel;
using Osram.TFTBackend.DataModel.Feature.Contracts;
using System.Collections.Generic;

namespace Osram.T4TField.Utils
{
    public static class FeaturesUtils
    {   

 

        /// <summary>
        /// return the list of all the features can be handling by T4T F app.
        /// </summary>
        public static List<IFeatureInfo> getListOfAvailableFeatures() {

            IFeatureManager featureManager = Mvx.IoCProvider.Resolve<IFeatureManager>();
            var featuresList = featureManager.GetAllFeaturesInfoList();
            return featuresList;

        }


        /// <summary>
        /// return the list of all viewModel needed for each features (To be used in carousel view)
        /// </summary>
        public static List<BaseFeatureViewModel> getListOfViewModelByFeaturesType(FeatureType FeatureType)
        {
            List<BaseFeatureViewModel> viewModelList = new List<BaseFeatureViewModel>();
            switch (FeatureType)
            {
                case FeatureType.AstroDIM:
                    viewModelList.Add(Mvx.IoCProvider.IoCConstruct(typeof(AstroDimViewModel)) as AstroDimViewModel);
                    viewModelList.Add(Mvx.IoCProvider.IoCConstruct(typeof(AstroDimInformationViewModel)) as AstroDimInformationViewModel);
                   break;
                case FeatureType.StepDIM:
                    break;
                case FeatureType.TuningFactor:
                    viewModelList.Add(Mvx.IoCProvider.IoCConstruct(typeof(TuningFactorViewModel)) as TuningFactorViewModel);
                    break;
                default:
                    break;
            }
            return viewModelList;
        }
    }
}
