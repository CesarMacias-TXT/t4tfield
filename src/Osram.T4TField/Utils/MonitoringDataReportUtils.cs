﻿using CsvHelper;
using MvvmCross;
using Osram.T4TField.Resources;
using Osram.TFTBackend.DataModel.Feature.Contracts;
using Osram.TFTBackend.FileService.Contracts;
using System;
using System.Collections.Generic;
using System.IO;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using Osram.T4TField.Contracts;
using Osram.TFTBackend.ConfigurationService.Contracts;
using Osram.TFTBackend.ConfigurationService.DataModel;
using System.Globalization;

namespace Osram.T4TField.Utils
{
    class MonitoringDataReportUtils
    {
        private static MonitoringDataReportUtils _instance;

        private IMonitoringDataFeature _monitoringDataFeature;

        private IFileService _fileService;
        private IInfoUtils _infoUtils;

        private MonitoringDataReportUtils()
        {            
            _monitoringDataFeature = Mvx.IoCProvider.Resolve<IMonitoringDataFeature>();

            _fileService = Mvx.IoCProvider.Resolve<IFileService>();
            _infoUtils = Mvx.IoCProvider.Resolve<IInfoUtils>();
        }

        public static MonitoringDataReportUtils Instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = new MonitoringDataReportUtils();
                }
                return _instance;
            }
        }

        // ---------------------

        public async Task<string> CreateCsv(List<MonitoringDataReportFeature> report, IConfiguration configuration)
        {
            using (var stream = new MemoryStream())
            using (var textWriter = new StreamWriter(stream))
            using (var csv = new CsvWriter(textWriter, CultureInfo.InvariantCulture))
            {
                byte[] bom = new byte[] { 0xEF, 0xBB, 0xBF };
                stream.Write(bom, 0, bom.Length);
                csv.Configuration.Delimiter = Mvx.IoCProvider.Resolve<ILocalizationInfo>().GetCurrentCultureInfo().TextInfo.ListSeparator;
                csv.WriteField(AppResources.F_LuminaireInfo + ":");
                csv.NextRecord();
                csv.WriteField(AppResources.F_Name);
                csv.WriteField(configuration?.LuminaireName);
                csv.NextRecord();
                csv.WriteField(AppResources.F_OrderCode);
                csv.WriteField(configuration?.BasicCode);
                csv.NextRecord();
                csv.WriteField(AppResources.F_CustomerProject);
                csv.WriteField("");
                csv.NextRecord();
                csv.WriteField(AppResources.F_Description);
                csv.WriteField(configuration?.DeviceDescription);
                csv.NextRecord();
                csv.WriteField(AppResources.F_ConfigID);
                csv.WriteField("");
                csv.NextRecord();
                csv.WriteField(AppResources.F_ECG1Name);
                csv.WriteField(configuration?.LuminaireName);
                csv.NextRecord();
                csv.WriteField(AppResources.F_ECG1Multiplicity);
                csv.WriteField("");
                csv.NextRecord();
                csv.WriteField(AppResources.F_ECG2Name);
                csv.WriteField("");
                csv.NextRecord();
                csv.WriteField(AppResources.F_ECG2Multiplicity);
                csv.WriteField("");
                csv.NextRecord();
                csv.WriteField(AppResources.F_ImageFileName);
                csv.WriteField(configuration?.ImageFileName);
                csv.NextRecord();

                // Write of header features
                csv.NextRecord();
                csv.WriteField(AppResources.F_ParametersValue + ":");
                csv.NextRecord();

                // Write of features
                foreach (var feature in report)
                {
                    foreach (var property in feature.ReportProperties)
                    {
                        csv.WriteField(feature.Name);
                        csv.WriteField(property.Name);
                        csv.WriteField(property.Value);
                        csv.NextRecord();
                    }
                }

                textWriter.Flush();
                stream.Flush();
                stream.Seek(0, SeekOrigin.Begin);

                bool isPermissionGranted = await PermissionUtils.Instance.CheckPermissionStatus(Plugin.Permissions.Abstractions.Permission.Storage);
                string path = null;
                if (isPermissionGranted)
                {
                    string nameCsv = DateTime.Now.ToString("yyyyMMdd_HHmmss") + ".csv";

                    path = _fileService.CombinePath(_infoUtils.GetDownloadFolderPath(), AppResources.F_T4TFieldReports);
                    _fileService.EnsureFolderExists(path);
                    path = _fileService.CombinePath(path, nameCsv);
                    _fileService.CopyStreamToFile(stream, path);
                }

                return path;
            }
        }

        public ObservableCollection<FlatReportProperty> GetFlatReport(List<MonitoringDataReportFeature> report)
        {
            ObservableCollection<FlatReportProperty> flatReport = new ObservableCollection<FlatReportProperty>();

            foreach (var reportFeature in report)
            {
                flatReport.Add(new FlatReportProperty { Type = FlatPropertyType.Title, Element1 = reportFeature.Name });
                foreach (var reportProperty in reportFeature.ReportProperties)
                {
                    flatReport.Add(new FlatReportProperty { Type = FlatPropertyType.KeyValue, Element1 = reportProperty.Name, Element2 = reportProperty.Value });
                }
            }

            return flatReport;
        }

        public List<MonitoringDataReportFeature> GetReport(IConfiguration configuration)
        {
            List<MonitoringDataReportFeature> report = new List<MonitoringDataReportFeature>();

            // ----------
            // Active Energy and Power
            // ----------
            if (_monitoringDataFeature.ActiveEnergyPower.IsArray)
            {
                MonitoringDataReportFeature activeEnergyPower = new MonitoringDataReportFeature(_monitoringDataFeature, AppResources.F_MonitoringData_ActiveEP);

                activeEnergyPower.ReportProperties.Add(new MonitoringDataReportProperty { Name = AppResources.F_MonitoringData_ActiveEnergy, Value = FormatOutputValue(CreateOutputValue(1, 6, _monitoringDataFeature.ActiveEnergyPower), 6, AppResources.F_Report_WattHour) });
                activeEnergyPower.ReportProperties.Add(new MonitoringDataReportProperty { Name = AppResources.F_MonitoringData_ActivePower, Value = FormatOutputValue(CreateOutputValue(8, 4, _monitoringDataFeature.ActiveEnergyPower), 4, AppResources.F_Report_Watt) });

                report.Add(activeEnergyPower);
            }

            // ----------
            // Apparent Energy and Power
            // ----------
            if (_monitoringDataFeature.ApparentEnergyPower.IsArray)
            {
                MonitoringDataReportFeature apparentEnergyPower = new MonitoringDataReportFeature(_monitoringDataFeature, AppResources.F_MonitoringData_ApparentEP);

                apparentEnergyPower.ReportProperties.Add(new MonitoringDataReportProperty { Name = AppResources.F_MonitoringData_ApparentEnergy, Value = FormatOutputValue(CreateOutputValue(1, 6, _monitoringDataFeature.ApparentEnergyPower), 6, AppResources.F_Report_VoltAmpereHour) });
                apparentEnergyPower.ReportProperties.Add(new MonitoringDataReportProperty { Name = AppResources.F_MonitoringData_ApparentPower, Value = FormatOutputValue(CreateOutputValue(8, 4, _monitoringDataFeature.ApparentEnergyPower), 4, AppResources.F_Report_VoltAmpere) });

                report.Add(apparentEnergyPower);
            }

            // ----------
            // Load Side Energy and Power
            // ----------
            if (_monitoringDataFeature.LoadSideEnergyPower.IsArray)
            {
                MonitoringDataReportFeature loadSideEnergyPower = new MonitoringDataReportFeature(_monitoringDataFeature, AppResources.F_MonitoringData_LoadSideEP);

                loadSideEnergyPower.ReportProperties.Add(new MonitoringDataReportProperty { Name = AppResources.F_MonitoringData_ActiveLoadEnergy, Value = FormatOutputValue(CreateOutputValue(1, 6, _monitoringDataFeature.LoadSideEnergyPower), 6, AppResources.F_Report_WattHour) });
                loadSideEnergyPower.ReportProperties.Add(new MonitoringDataReportProperty { Name = AppResources.F_MonitoringData_ActiveLoadPower, Value = FormatOutputValue(CreateOutputValue(8, 4, _monitoringDataFeature.LoadSideEnergyPower), 4, AppResources.F_Report_Watt) });

                report.Add(loadSideEnergyPower);
            }

            // ----------
            // Control Gear Diagnostics
            // ----------
            if (_monitoringDataFeature.ControlGearDiagnostics.IsArray)
            {
                MonitoringDataReportFeature controlGearDiagnostics = new MonitoringDataReportFeature(_monitoringDataFeature, AppResources.F_MonitoringData_ControlGearD);

                controlGearDiagnostics.ReportProperties.Add(new MonitoringDataReportProperty { Name = AppResources.F_MonitoringData_OperatingTime, Value = FormatOutputValue(
                        TimeSpan.FromSeconds(Double.Parse(CreateOutputValue(0, 4, _monitoringDataFeature.ControlGearDiagnostics))).ToString(@"hh\:mm", CultureInfo.InvariantCulture), 4, AppResources.F_Report_hhmm) });
                controlGearDiagnostics.ReportProperties.Add(new MonitoringDataReportProperty { Name = AppResources.F_MonitoringData_StartCounter, Value = FormatOutputValue(CreateOutputValue(4, 3, _monitoringDataFeature.ControlGearDiagnostics), 3, "") });
                controlGearDiagnostics.ReportProperties.Add(new MonitoringDataReportProperty { Name = AppResources.F_MonitoringData_ExternalSV, Value = FormatOutputValue(CreateOutputValue(7, 2, _monitoringDataFeature.ControlGearDiagnostics), 2, AppResources.F_Report_VRMS) });
                controlGearDiagnostics.ReportProperties.Add(new MonitoringDataReportProperty { Name = AppResources.F_MonitoringData_ExternalSVF, Value = FormatOutputValue(_monitoringDataFeature.ControlGearDiagnostics[9].ToString(), 1, AppResources.F_Report_Heartz) });
                controlGearDiagnostics.ReportProperties.Add(new MonitoringDataReportProperty { Name = AppResources.F_MonitoringData_PowerFactor, Value = FormatOutputValue(_monitoringDataFeature.ControlGearDiagnostics[10].ToString(), 1, "") });
                controlGearDiagnostics.ReportProperties.Add(new MonitoringDataReportProperty { Name = AppResources.F_MonitoringData_OverallFC, Value = FormatOutputValue(_monitoringDataFeature.ControlGearDiagnostics[11].ToString(), 1, "") });
                controlGearDiagnostics.ReportProperties.Add(new MonitoringDataReportProperty { Name = AppResources.F_MonitoringData_OverallFCC, Value = FormatOutputValue(_monitoringDataFeature.ControlGearDiagnostics[12].ToString().ToString(), 1, "") });
                controlGearDiagnostics.ReportProperties.Add(new MonitoringDataReportProperty { Name = AppResources.F_MonitoringData_ExternalSU, Value = FormatOutputValue(_monitoringDataFeature.ControlGearDiagnostics[13].ToString(), 1, "") });
                controlGearDiagnostics.ReportProperties.Add(new MonitoringDataReportProperty { Name = AppResources.F_MonitoringData_ExternalSUC, Value = FormatOutputValue(_monitoringDataFeature.ControlGearDiagnostics[14].ToString().ToString(), 1, "") });
                controlGearDiagnostics.ReportProperties.Add(new MonitoringDataReportProperty { Name = AppResources.F_MonitoringData_ExternalSO, Value = FormatOutputValue(_monitoringDataFeature.ControlGearDiagnostics[15].ToString(), 1, "") });
                controlGearDiagnostics.ReportProperties.Add(new MonitoringDataReportProperty { Name = AppResources.F_MonitoringData_ExternalSOC, Value = FormatOutputValue(_monitoringDataFeature.ControlGearDiagnostics[16].ToString().ToString(), 1, "") });
                controlGearDiagnostics.ReportProperties.Add(new MonitoringDataReportProperty { Name = AppResources.F_MonitoringData_OutputPL, Value = FormatOutputValue(_monitoringDataFeature.ControlGearDiagnostics[17].ToString(), 1, "") });
                controlGearDiagnostics.ReportProperties.Add(new MonitoringDataReportProperty { Name = AppResources.F_MonitoringData_OutputPLC, Value = FormatOutputValue(_monitoringDataFeature.ControlGearDiagnostics[18].ToString().ToString(), 1, "") });
                controlGearDiagnostics.ReportProperties.Add(new MonitoringDataReportProperty { Name = AppResources.F_MonitoringData_ThermalDerating, Value = FormatOutputValue(_monitoringDataFeature.ControlGearDiagnostics[19].ToString(), 1, "") });
                controlGearDiagnostics.ReportProperties.Add(new MonitoringDataReportProperty { Name = AppResources.F_MonitoringData_ThermalDeratingC, Value = FormatOutputValue(_monitoringDataFeature.ControlGearDiagnostics[20].ToString().ToString(), 1, "") });
                controlGearDiagnostics.ReportProperties.Add(new MonitoringDataReportProperty { Name = AppResources.F_MonitoringData_ThermalShutdown, Value = FormatOutputValue(_monitoringDataFeature.ControlGearDiagnostics[21].ToString(), 1, "") });
                controlGearDiagnostics.ReportProperties.Add(new MonitoringDataReportProperty { Name = AppResources.F_MonitoringData_ThermalShutdownC, Value = FormatOutputValue(_monitoringDataFeature.ControlGearDiagnostics[22].ToString().ToString(), 1, "") });
                controlGearDiagnostics.ReportProperties.Add(new MonitoringDataReportProperty { Name = AppResources.F_MonitoringData_Temperature, Value = FormatOutputValue(_monitoringDataFeature.ControlGearDiagnostics[23].ToString(), 1, AppResources.F_Report_DegreeCelsius) });
                controlGearDiagnostics.ReportProperties.Add(new MonitoringDataReportProperty { Name = AppResources.F_MonitoringData_OutputCurrentP, Value = FormatOutputValue(_monitoringDataFeature.ControlGearDiagnostics[24].ToString().ToString(), 1, "") });

                report.Add(controlGearDiagnostics);
            }

            // ----------
            // Light Source Diagnostics
            // ----------
            if (_monitoringDataFeature.LightSourceDiagnostics.IsArray)
            {
                MonitoringDataReportFeature lightSourceDiagnostics = new MonitoringDataReportFeature(_monitoringDataFeature, AppResources.F_MonitoringData_LightSourceD);

                lightSourceDiagnostics.ReportProperties.Add(new MonitoringDataReportProperty { Name = AppResources.F_MonitoringData_StartCounterR, Value = FormatOutputValue(CreateOutputValue(0, 3, _monitoringDataFeature.LightSourceDiagnostics), 3, "") });
                lightSourceDiagnostics.ReportProperties.Add(new MonitoringDataReportProperty { Name = AppResources.F_MonitoringData_StartCounter1, Value = FormatOutputValue(CreateOutputValue(3, 3, _monitoringDataFeature.LightSourceDiagnostics), 3, "") });
                lightSourceDiagnostics.ReportProperties.Add(new MonitoringDataReportProperty { Name = AppResources.F_MonitoringData_OnTimeResettable, Value = FormatOutputValue(
                        TimeSpan.FromSeconds(Double.Parse(CreateOutputValue(6, 4, _monitoringDataFeature.LightSourceDiagnostics))).ToString(@"hh\:mm", CultureInfo.InvariantCulture), 4, AppResources.F_Report_hhmm) });
                lightSourceDiagnostics.ReportProperties.Add(new MonitoringDataReportProperty { Name = AppResources.F_MonitoringData_OnTime, Value = FormatOutputValue(
                        TimeSpan.FromSeconds(Double.Parse(CreateOutputValue(10, 4, _monitoringDataFeature.LightSourceDiagnostics))).ToString(@"hh\:mm", CultureInfo.InvariantCulture), 4, AppResources.F_Report_hhmm) });
                lightSourceDiagnostics.ReportProperties.Add(new MonitoringDataReportProperty { Name = AppResources.F_MonitoringData_LightSourceVoltage, Value = FormatOutputValue(CreateOutputValue(14, 2, _monitoringDataFeature.LightSourceDiagnostics), 2, AppResources.F_Report_Volt) });
                lightSourceDiagnostics.ReportProperties.Add(new MonitoringDataReportProperty { Name = AppResources.F_MonitoringData_LightSourceCurrent, Value = FormatOutputValue(CreateOutputValue(16, 2, _monitoringDataFeature.LightSourceDiagnostics), 2, AppResources.F_Report_MilliAmpere) });
                lightSourceDiagnostics.ReportProperties.Add(new MonitoringDataReportProperty { Name = AppResources.F_MonitoringData_OverallFailureC1, Value = FormatOutputValue(_monitoringDataFeature.LightSourceDiagnostics[18].ToString(), 1, "") });
                lightSourceDiagnostics.ReportProperties.Add(new MonitoringDataReportProperty { Name = AppResources.F_MonitoringData_OverallFailureCC1, Value = FormatOutputValue(_monitoringDataFeature.LightSourceDiagnostics[19].ToString(), 1, "") });
                lightSourceDiagnostics.ReportProperties.Add(new MonitoringDataReportProperty { Name = AppResources.F_MonitoringData_ShortCircuit, Value = FormatOutputValue(_monitoringDataFeature.LightSourceDiagnostics[20].ToString(), 1, "") });
                lightSourceDiagnostics.ReportProperties.Add(new MonitoringDataReportProperty { Name = AppResources.F_MonitoringData_ShortCircuitCounter, Value = FormatOutputValue(_monitoringDataFeature.LightSourceDiagnostics[21].ToString(), 1, "") });
                lightSourceDiagnostics.ReportProperties.Add(new MonitoringDataReportProperty { Name = AppResources.F_MonitoringData_OpenCircuit, Value = FormatOutputValue(_monitoringDataFeature.LightSourceDiagnostics[22].ToString(), 1, "") });
                lightSourceDiagnostics.ReportProperties.Add(new MonitoringDataReportProperty { Name = AppResources.F_MonitoringData_OpenCircuitCounter, Value = FormatOutputValue(_monitoringDataFeature.LightSourceDiagnostics[23].ToString(), 1, "") });
                lightSourceDiagnostics.ReportProperties.Add(new MonitoringDataReportProperty { Name = AppResources.F_MonitoringData_ThermalDerating1, Value = FormatOutputValue(_monitoringDataFeature.LightSourceDiagnostics[24].ToString(), 1, "") });
                lightSourceDiagnostics.ReportProperties.Add(new MonitoringDataReportProperty { Name = AppResources.F_MonitoringData_ThermalDeratingC1, Value = FormatOutputValue(_monitoringDataFeature.LightSourceDiagnostics[25].ToString(), 1, "") });
                lightSourceDiagnostics.ReportProperties.Add(new MonitoringDataReportProperty { Name = AppResources.F_MonitoringData_ThermalShutdown1, Value = FormatOutputValue(_monitoringDataFeature.LightSourceDiagnostics[26].ToString(), 1, "") });
                lightSourceDiagnostics.ReportProperties.Add(new MonitoringDataReportProperty { Name = AppResources.F_MonitoringData_ThermalShutdownC1, Value = FormatOutputValue(_monitoringDataFeature.LightSourceDiagnostics[27].ToString(), 1, "") });
                lightSourceDiagnostics.ReportProperties.Add(new MonitoringDataReportProperty { Name = AppResources.F_MonitoringData_Temperature1, Value = FormatOutputValue(_monitoringDataFeature.LightSourceDiagnostics[28].ToString(), 1, AppResources.F_Report_DegreeCelsius) });

                report.Add(lightSourceDiagnostics);
            }

            return report;
        }

        private string FormatOutputValue(string outputValue, int length, string unitMeasure)
        {
            switch (length)
            {
                case 1:
                    if (outputValue == "254")
                    {
                        return AppResources.F_NotAvailable;
                    }
                    if (outputValue == "255")
                    {
                        return AppResources.F_ParametersNotImplemented;
                    }
                    return outputValue + " " + unitMeasure;
                case 2:
                    if (outputValue == "65534")
                    {
                        return AppResources.F_NotAvailable;
                    }
                    if (outputValue == "65535")
                    {
                        return AppResources.F_ParametersNotImplemented;
                    }
                    return outputValue + " " + unitMeasure;
                case 3:
                    if (outputValue == "16777214")
                    {
                        return AppResources.F_NotAvailable;
                    }
                    if (outputValue == "16777215")
                    {
                        return AppResources.F_ParametersNotImplemented;
                    }
                    return outputValue + " " + unitMeasure;
                case 4:
                    if (outputValue == "0")
                    {
                        return "0.00 " + unitMeasure;
                    }

                    if (outputValue == "4294967294")
                    {
                        return AppResources.F_NotAvailable;
                    }
                    if (outputValue == "4294967295")
                    {
                        return AppResources.F_ParametersNotImplemented;
                    }
                    return outputValue + " " + unitMeasure;
                case 6:
                    if (outputValue == "0")
                    {
                        return "0.0 " + unitMeasure;
                    }

                    if (outputValue == "281474976710654")
                    {
                        return AppResources.F_NotAvailable;
                    }
                    if (outputValue == "281474976710655")
                    {
                        return AppResources.F_ParametersNotImplemented;
                    }
                    return (Convert.ToUInt64(outputValue) * 0.1).ToString() + " " + unitMeasure;
                default:
                    return outputValue + " " + unitMeasure;
            }
        }
        private string CreateOutputValue(int firstIndex, int length, ProgrammingValue property)
        {
            byte[] propertyReaded = new byte[length];

            for (int i = 0; i < length; i++)
            {
                propertyReaded[i] = property[firstIndex + i];
            }

            // In some architecture the bytes are read in LittleEndian
            if (BitConverter.IsLittleEndian)
            {
                Array.Reverse(propertyReaded, 0, length);
            }

            switch (length)
            {
                case 2:
                    return BitConverter.ToUInt16(propertyReaded, 0).ToString();
                case 3:
                    byte[] propertyReaded3 = new byte[4];
                    // In some architecture the bytes are read in LittleEndian
                    if (BitConverter.IsLittleEndian)
                    {
                        propertyReaded.CopyTo(propertyReaded3, 0);
                    }
                    else
                    {
                        propertyReaded.CopyTo(propertyReaded3, 1);
                    }                                    
                    return BitConverter.ToUInt32(propertyReaded3, 0).ToString();
                case 4:
                    return BitConverter.ToUInt32(propertyReaded, 0).ToString();
                case 6:
                    byte[] propertyReaded6 = new byte[8];
                    // In some architecture the bytes are read in LittleEndian
                    if (BitConverter.IsLittleEndian)
                    {
                        propertyReaded.CopyTo(propertyReaded6, 0);
                    }
                    else
                    {
                        propertyReaded.CopyTo(propertyReaded6, 2);
                    }                    
                    return BitConverter.ToUInt64(propertyReaded6, 0).ToString();
                default:
                    return "0";
            }         
        }
    }

    public class MonitoringDataReportFeature
    {
        public MonitoringDataReportFeature()
        {
            Enabled = true;
            Available = true;
            ReportProperties = new List<MonitoringDataReportProperty>();
        }
        public MonitoringDataReportFeature(IFeature feature, string name)
        {
            Name = name;
            Enabled = feature.Info.IsEnabled;
            Available = feature.Info.IsAvailable;
            ReportProperties = new List<MonitoringDataReportProperty>();
        }

        public bool Available { get; set; }
        public bool Enabled { get; set; }
        public string Name { get; set; }

        public IList<MonitoringDataReportProperty> ReportProperties { get; set; }
    }

    public class MonitoringDataReportProperty
    {
        public string Name { get; set; }
        public string Value { get; set; }
    }
}
