﻿using Osram.TFTBackend;
using Plugin.Permissions;
using Plugin.Permissions.Abstractions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Osram.T4TField.Utils
{
    public class PermissionUtils
    {
        private static PermissionUtils instance;

        private PermissionUtils() { }

        public static PermissionUtils Instance {
            get {
                if (instance == null)
                {
                    instance = new PermissionUtils();
                }
                return instance;
            }
        }


        public async Task<bool> CheckPermissionStatus(Permission type)
        {
            try
            {
                var status = await CrossPermissions.Current.CheckPermissionStatusAsync(type);
                if (status != PermissionStatus.Granted)
                {
                    var results = await CrossPermissions.Current.RequestPermissionsAsync(new[] { type });
                    results.TryGetValue(type, out status);
                }
                if (status == PermissionStatus.Granted)
                {
                    return true;
                }
            }
            catch (Exception ex)
            {
                Logger.Exception(nameof(PermissionUtils), ex);
            }
            return false;
        }
    }
}
