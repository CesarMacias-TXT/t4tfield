﻿using Acr.UserDialogs;
using MvvmCross.Commands;
using MvvmCross.Plugin.Email;
using Osram.T4TField.Contracts;
using Osram.T4TField.Resources;
using Osram.T4TField.Utils;
using Osram.TFTBackend.ConfigurationService.Contracts;
using Osram.TFTBackend.FileService.Contracts;
using Osram.TFTBackend.InAppPurchaseService.Contracts;
using Osram.TFTBackend.KeyService.Contracts;
using Osram.TFTBackend.Messaging;
using Plugin.Settings.Abstractions;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Threading.Tasks;
using System.Windows.Input;

namespace Osram.T4TField.ViewModels
{
    public class ReportViewModel : HomeViewModel
    {
        public string Report { get; set; }

        public ICommand ExportButtonCommand { get; set; }

        private List<ReportFeature> _reportList;
        private List<MonitoringDataReportFeature> _reportMonitorigDataList;
        private string _pathCsvFile;

        public ObservableCollection<FlatReportProperty> reportFlatList;

        private IMvxComposeEmailTaskEx _mailService;
        private IFileService _fileService;

        private IConfiguration configuration;

        private async Task OnExportButtonCommand()
        {

            EmailAttachment emailAttach = new EmailAttachment();
            IEnumerable<EmailAttachment> attachList;
            IEnumerable<string> emailList = new string[] { };

            try
            {
                using (Stream fileStream = _fileService.GetStreamFromFile(_pathCsvFile))
                {
                    if (fileStream != null)
                    {
                        emailAttach.Content = fileStream;
                        emailAttach.ContentType = "octet/stream";
                        emailAttach.FileName = _pathCsvFile.Substring(_pathCsvFile.LastIndexOf("/") + 1);
                        attachList = new EmailAttachment[] { emailAttach };
                        _mailService.ComposeEmail(emailList, null, null, null, false, attachList, null);
                    }
                    else
                    {
                        IsExportEnable = false;
                        await _userDialogs.AlertAsync(AppResources.F_ReportError);
                        CreateCsvAsync();
                    }
                }
            }
            catch (Exception)
            {
                IsExportEnable = false;
                await _userDialogs.AlertAsync(AppResources.F_EmailConfigurationNeeded);
            }
        }

        public override void OnRemoveSelectedECGCommand()
        {
            base.OnRemoveSelectedECGCommand();
            ReportFlatList.Clear();
            IsExportEnable = false;
        }

        public ObservableCollection<FlatReportProperty> ReportFlatList {
            get { return reportFlatList; }
            set {
                reportFlatList = value;
                RaisePropertyChanged();
            }
        }

        private bool isExportEnable;
        public bool IsExportEnable {
            get { return isExportEnable; }
            set {
                isExportEnable = value;
                RaisePropertyChanged();
            }
        }

        private ImageButton readDriverBtn;
        public ImageButton ReadDriverBtn
        {
            get { return readDriverBtn; }
        }        

        private ImageButton readMonitoringBtn;
        public ImageButton ReadMonitoringBtn
        {
            get { return readMonitoringBtn; }
        }

        public ReportViewModel(IFileService fileService, IMvxComposeEmailTaskEx mailService, IConfigurationService configurationService, ISelectConfigurationUiService selectConfigurationUiService, IUserDialogs dialog, ISettings settings, IKeyService keyService, IInAppPurchaseService inAppPurchaseService) : base(configurationService, selectConfigurationUiService, dialog, settings, keyService, inAppPurchaseService)
        {
            _fileService = fileService;
            _mailService = mailService;

            InitGUI();
        }

        private void InitGUI()
        {
            Title = AppResources.F_Reports_title;
            IsExportEnable = false;
            IsOnlyReadEcgBtn = false;
            ExportButtonCommand = new MvxAsyncCommand(OnExportButtonCommand);

            readDriverBtn = new ImageButton();
            readDriverBtn.IconLink = "ReadECG.png";
            readDriverBtn.Label = AppResources.F_Report_ReadDriver;
            readDriverBtn.OnClickButton = NewConfigurationFromLuminaireCommand;

            readMonitoringBtn = new ImageButton();
            readMonitoringBtn.IconLink = "ReadECG.png";
            readMonitoringBtn.Label = AppResources.F_Report_ReadMonitoring;
            readMonitoringBtn.OnClickButton = NewMonitoringDataConfigurationFromLuminaireCommand;
        }

        public override void Start()
        {
            base.Start();
        }

        protected override void FeatureListUpdated()
        {
            _reportList = ReportUtils.Instance.GetReport(configuration);
            ReportFlatList = ReportUtils.Instance.GetFlatReport(_reportList);
            CreateCsvAsync();
            _configurationService.ResetConfiguration();
        }

        protected override void MonitoringDataFeatureListUpdated()
        {
            _reportMonitorigDataList = MonitoringDataReportUtils.Instance.GetReport(configuration);
            ReportFlatList = MonitoringDataReportUtils.Instance.GetFlatReport(_reportMonitorigDataList);

            if (_reportMonitorigDataList.Count == 0)
            {
                _userDialogs.Alert(AppResources.ErrorNoData, AppResources.F_DialogProgrammingErrorTitle);
            }
            else
            {
                CreateMonitoringDataCsvAsync();
            }
            
            _configurationService.ResetConfiguration();
        }

        public override void ShowConfigurationData(Events.ConfigurationChangedEvent configurationEvent)
        {
            configuration = configurationEvent.Configuration;
            base.ShowConfigurationData(configurationEvent);
        }

        public override void ResetConfigurationData(Events.ConfigurationResetEvent configurationEvent)
        {
        }

        private async void CreateCsvAsync()
        {
            _pathCsvFile = await ReportUtils.Instance.CreateCsv(_reportList, configuration);
            IsExportEnable = true;
        }

        private async void CreateMonitoringDataCsvAsync()
        {
            _pathCsvFile = await MonitoringDataReportUtils.Instance.CreateCsv(_reportMonitorigDataList, configuration);
            IsExportEnable = true;
        }
    }
}
