﻿using Acr.UserDialogs;
using MvvmCross.Commands;
using Osram.T4TField.Contracts;
using Osram.T4TField.Resources;
using Osram.T4TField.Utils;
using Osram.T4TField.ViewModels.FeaturesViewModel;
using Osram.TFTBackend;
using Osram.TFTBackend.BaseTypes.Exceptions;
using Osram.TFTBackend.ConfigurationService.Contracts;
using Osram.TFTBackend.ConfigurationService.DataModel;
using Osram.TFTBackend.DataModel.Feature.Contracts;
using Osram.TFTBackend.InAppPurchaseService.Contracts;
using Osram.TFTBackend.KeyService.Contracts;
using Osram.TFTBackend.Messaging;
using Osram.TFTBackend.ProgrammingService.Contracts;
using Plugin.Settings.Abstractions;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Threading;
using System.Threading.Tasks;
using static Osram.T4TField.ViewModels.FeaturesCarouselViewModel;

namespace Osram.T4TField.ViewModels
{
    class AdvancedHomeViewModel : HomeViewModel
    {

        public AdvancedHomeViewModel(IConfigurationService configurationService, ISelectConfigurationUiService selectConfigurationUiService, IUserDialogs userDialogs, ISettings settings, IKeyService keyService, IInAppPurchaseService inAppPurchaseService) : base(configurationService, selectConfigurationUiService, userDialogs, settings, keyService, inAppPurchaseService)
        {
            _configurationService = configurationService;
            _userDialogs = userDialogs;

            initButtomActionBar();
            initAdvancedModeInterface();
        }

        private ObservableCollection<FeatureInfoButton> featuresList;
        public ObservableCollection<FeatureInfoButton> FeaturesList {
            get { return featuresList; }
            set {
                featuresList = value;
                RaisePropertyChanged();

            }
        }

        public override void ShowConfigurationData(Events.ConfigurationChangedEvent configurationEvent)
        {
        }

        public override void ResetConfigurationData(Events.ConfigurationResetEvent configurationEvent)
        {
        }


        private void UpdateFeaturesStatus()
        {
            foreach (FeatureInfoButton item in FeaturesList)
            {
                item.RaiseAllPropertiesChanged();
            }
        }

        protected override void FeatureListUpdated()
        {
            UpdateFeaturesStatus();
        }

        public override void OnRemoveSelectedECGCommand()
        {
            base.OnRemoveSelectedECGCommand();
            EnableDisableFeaturesByConfigurations(false);
            ProgramBtn.IsEnabled = false;
            CompareBtn.IsEnabled = false;
            SummaryBtn.IsEnabled = false;
        }

        private void EnableDisableFeaturesByConfigurations(bool isEnabled)
        {
            foreach (FeatureInfoButton feature in FeaturesList)
            {
                feature.FeatureInfo.IsAvailable = isEnabled;
                feature.RaiseAllPropertiesChanged();
            }
            //FeaturesList = FeaturesList;
        }

        private ImageButton programBtn;
        public ImageButton ProgramBtn {
            get { return programBtn; }
            set { programBtn = value; }
        }

        private ImageButton compareBtn;
        public ImageButton CompareBtn {
            get { return compareBtn; }
            set { compareBtn = value; }
        }

        private ImageButton summaryBtn;
        public ImageButton SummaryBtn {
            get { return summaryBtn; }
            set { summaryBtn = value; }
        }

        private ImageButton moreBtn;
        public ImageButton MoreBtn {
            get { return moreBtn; }
            set { moreBtn = value; }
        }


        public override void Resumed()
        {
            UpdateFeaturesStatus();
            enableDisableProgramButton();
            base.Resumed();

            //check here because state could be changed when app is in background
        }

        private void enableDisableProgramButton()
        {
            programBtn.IsEnabled = CheckIfThereIsFeaturesSelected();
        }

        private bool CheckIfThereIsFeaturesSelected()
        {
            if (featuresList != null)
            {
                foreach (ImageButton feature in featuresList)
                {
                    if (feature.IsSelected)
                    {
                        return true;
                    }
                }
            }
            return false;
        }

        private void initAdvancedModeInterface()
        {
            ObservableCollection<FeatureInfoButton> FeatureInfoButtonList = new ObservableCollection<FeatureInfoButton>();
            var Featureslist = FeaturesUtils.getListOfAvailableFeatures();

            foreach (IFeatureInfo feature in Featureslist)
            {
                feature.Lock = ProtectionLock.Service;
                FeatureInfoButton featureInfoButton = createNewFeatureInfoButton(feature);
                FeatureInfoButtonList.Add(featureInfoButton);
            }

            FeaturesList = FeatureInfoButtonList;
        }


        private FeatureInfoButton createNewFeatureInfoButton(IFeatureInfo CurrentFeature)
        {
            var featureButton = new FeatureInfoButton(CurrentFeature);

            switch (CurrentFeature.FeatureType)
            {
                case FeatureType.AstroDIM:
                    SetGraphicalInfo(featureButton, "AstroDIM.png", AppResources.F_BMMenuDimming, new MvxCommand(OnDimmingButtonCommand));
                    break;
                case FeatureType.StepDIM:
                    break;
                case FeatureType.TuningFactor:
                    SetGraphicalInfo(featureButton, "Operating_Current.png", AppResources.F_BMmenuTuning, new MvxCommand(OnTuningButtonCommand));
                    break;
                case FeatureType.Default:
                    break;
                default:
                    break;
            }
            return featureButton;
        }


        private void SetGraphicalInfo(FeatureInfoButton featureButton, String ImageLink, String FeatureName, MvxCommand Command)
        {
            featureButton.IconLink = ImageLink;
            featureButton.Label = FeatureName;
            featureButton.WarningIconLink = "FWarning.png";
            featureButton.ErrorIconLink = "FError.png";
            featureButton.SelectedIconLink = "FSelected.png";
            featureButton.LockMasterIconLink = "LockMaster.png";
            featureButton.LockServiceIconLink = "LockService.png";
            featureButton.OnClickButton = Command;
        }


        private void initButtomActionBar()
        {

            programBtn = new ImageButton();
            programBtn.IconLink = "program.png";
            programBtn.Label = AppResources.F_ProgramButton; ;
            programBtn.IsEnabled = false;
            programBtn.OnClickButton = new MvxAsyncCommand(OnProgramingCommandAsync);

            compareBtn = new ImageButton();
            compareBtn.IconLink = "compare.png";
            compareBtn.Label = AppResources.F_CompareButton;
            compareBtn.IsEnabled = false;

            summaryBtn = new ImageButton();
            summaryBtn.IconLink = "summary.png";
            summaryBtn.Label = AppResources.F_SummaryButton;
            summaryBtn.IsEnabled = true;

            moreBtn = new ImageButton();
            moreBtn.IconLink = "more.png";
            moreBtn.Label = AppResources.F_MoreButton; ;
            moreBtn.IsEnabled = true;

        }


        private void OnTuningButtonCommand()
        {
            ShowAsPageBeforeViewModel<FeaturesCarouselViewModel>(new FeaturesCarouselViewModel.CarouselParameter<TuningFactorViewModel>(T4TFAppMode.ADVANCED_MODE, FeaturesList, FeatureType.TuningFactor));
        }


        private void OnDimmingButtonCommand()
        {
            ShowAsPageBeforeViewModel<FeaturesCarouselViewModel>(new FeaturesCarouselViewModel.CarouselParameter<AstroDimViewModel, AstroDimInformationViewModel>(T4TFAppMode.ADVANCED_MODE, FeaturesList, FeatureType.AstroDIM));
        }

        private async Task OnProgramingCommandAsync()
        {
            if (CheckIfAdapterIsReady())
            {
                bool isPositionGuidePopupShown = settings.GetValueOrDefault(PositioningGuideViewModel.IsPositioningGuidePopupShown, false);
                if (isPositionGuidePopupShown)
                {
                    await ProgramDevicePopup();
                }
                else
                {
                    ShowViewModel<PositioningGuideViewModel>(new PositioningGuideViewModel.BTGuidePopupParameter(true)
                    {
                        Action = ProgramDevicePopup,
                    });
                }
            }
            else
            {
                await _userDialogs.AlertAsync(new AlertConfig().Message = GetAdapterDisabledMessage());
            }
        }

        private async Task ProgramDevicePopup()
        {
            MyShowViewModel<ProgrammingViewModel>(RequestType.Popup, new ProgrammingViewModel.ProgrammingParameter(true)
            {
                Title = AppResources.F_ProgrammingViewWritingPopupTitle,
                ProgrammingAction = ProgramDevice,
                ProgrammingType = ProgrammingType.Write,
                TextDialogAfterAction = AppResources.F_DialogProgrammingSuccessfulText
            });
        }

        private async Task<bool> ProgramDevice(BaseViewModel vm, IProgrammingService programmingService, CancellationToken cancellationToken)
        {
            try
            {
                if (programmingService.IsSameDevice())
                {
                    IEnumerable<EcgProperty> cells = _configurationService.GetConfigurationProperties();
                    programmingService.ProgramConfiguration(cells);
                    return await Task.FromResult(true);
                }
                else
                {
                    throw new OperationFailedException("CopyAndPasteViewModel.PasteDevice: incompatible devices", ErrorCodes.DifferentDevice);
                }
            }
            catch (OperationCanceledException e)
            {
                throw e;
            }
            catch (OperationFailedException operationFailedException)
            {
                Logger.Exception("CopyAndPasteViewModel.PasteDevice", operationFailedException);
                throw operationFailedException;
            }
            catch (Exception e)
            {
                Logger.Exception("ProgrammDevice", e);
            }
            return await Task.FromResult(false);
        }


    }
}
