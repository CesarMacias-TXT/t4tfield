﻿using Acr.UserDialogs;
using MvvmCross.Commands;
using Osram.T4TField.Contracts;
using Osram.T4TField.Resources;
using Osram.TFTBackend;
using Osram.TFTBackend.BaseTypes;
using Osram.TFTBackend.BaseTypes.Exceptions;
using Osram.TFTBackend.ConfigurationService.Contracts;
using Osram.TFTBackend.InAppPurchaseService.Contracts;
using Osram.TFTBackend.Messaging;
using Osram.TFTBackend.Nfc2.Tag;
using Osram.TFTBackend.ProgrammingService.Contracts;
using Plugin.Settings.Abstractions;
using System;
using System.IO;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;

namespace Osram.T4TField.ViewModels
{
    public class CopyAndPasteViewModel : ConnectionBaseViewModel
    {
        private Nfc2Tag _sourceTag;
        private readonly ISelectConfigurationUiService _selectConfigurationUiService;
        private readonly IInAppPurchaseService _inAppPurchaseService;

        public CopyAndPasteViewModel(ISelectConfigurationUiService selectConfigurationUiService, IUserDialogs dialog, ISettings settings, IInAppPurchaseService inAppPurchaseService)
        {
            _userDialogs = dialog;
            _settings = settings;
            InitCopyAndPastePage();
            InitCommands();
            _selectConfigurationUiService = selectConfigurationUiService;
            _inAppPurchaseService = inAppPurchaseService;
        }

        private void ShowConfigurationData(IConfiguration configuration)
        {
            if (configuration != null)
            {
                if (configuration.ThePicture != null && configuration.ThePicture.Length > 0)
                {
                    ECGImageSource = ImageSource.FromStream(() => new MemoryStream(configuration.ThePicture));
                }
                else
                {
                    ECGImageSource = "ecgDefault.png";
                }
                Luminairename = configuration.LuminaireName;
                GtinNaed = AppResources.F_GTIN + ": " + configuration.Gtin.ToString();
                BasicCode = AppResources.F_BasicCode + ": " + configuration.BasicCode;
            }
            IsReadyToBeWritten = true;

        }

        private void InitCommands()
        {
            ReadCommand = new MvxAsyncCommand(OnReadButtonCommandAsync);
            WriteCommand = new MvxAsyncCommand(OnWriteButtonCommandAsync);
        }


        private void InitCopyAndPastePage()
        {
            Title = AppResources.F_NavigationCopyPaste;
            ECGImageSource = "copyPasteSelectECG";
            Luminairename = AppResources.F_CopyECGDevice;
            GtinNaed = string.Empty;
            BasicCode = string.Empty;
            IsReadyToBeWritten = false;
        }


        private string luminairename;
        public string Luminairename {
            get { return luminairename; }
            set {
                luminairename = value;
                RaisePropertyChanged();
            }
        }


        private ImageSource ecgImageSource;
        public ImageSource ECGImageSource {
            get {
                return ecgImageSource;
            }
            set {
                ecgImageSource = value;
                RaisePropertyChanged();
            }
        }


        private String gtinNaed;
        public string GtinNaed {
            get { return gtinNaed; }
            set {
                gtinNaed = value;
                RaisePropertyChanged();
            }
        }

        private String basicCode;
        public string BasicCode {
            get { return basicCode; }
            set {
                basicCode = value;
                RaisePropertyChanged();
            }
        }

        private Boolean _isReadyToBeWritten;
        private IUserDialogs _userDialogs;
        private ISettings _settings;

        public Boolean IsReadyToBeWritten {
            get { return _isReadyToBeWritten; }
            set {
                _isReadyToBeWritten = value;
                RaisePropertyChanged();
            }
        }

        #region Commands
        public ICommand ReadCommand { get; set; }
        public ICommand WriteCommand { get; set; }

        private async Task OnReadButtonCommandAsync()
        {
            if (CheckIfAdapterIsReady())
            {
                bool isPositionGuidePopupShown = settings.GetValueOrDefault(PositioningGuideViewModel.IsPositioningGuidePopupShown, false);
                if (isPositionGuidePopupShown)
                {
                    await CopyDevicePopup();
                }
                else
                {
                    ShowViewModel<PositioningGuideViewModel>(new PositioningGuideViewModel.BTGuidePopupParameter(true)
                    {
                        Action = CopyDevicePopup,
                    });
                }
            }
            else
            {
                await _userDialogs.AlertAsync(new AlertConfig().Message = GetAdapterDisabledMessage());
            }
        }


        private async Task CopyDevicePopup()
        {

            MyShowViewModel<ProgrammingViewModel>(RequestType.Popup, new ProgrammingViewModel.ProgrammingParameter(true)
            {
                Title = AppResources.F_ProgrammingViewReadingPopupTitle,
                ProgrammingAction = CopyDevice,
                ProgrammingType = ProgrammingType.Read
            });
        }

        private async Task OnWriteButtonCommandAsync()
        {
            if (CheckIfAdapterIsReady())
            {
                if (await _inAppPurchaseService.CanConsumePurchaseAsync())
                {
                    bool isPaidFeaturesPopupShown = settings.GetValueOrDefault(PaidFeaturesPopupViewModel.IsPaidFeaturesPopupViewModelPopupShown, false);

                    if (isPaidFeaturesPopupShown)
                    {
                        await ShowPositionGuidePopup();
                    }
                    else
                    {
                        MyShowViewModel<PaidFeaturesPopupViewModel>(RequestType.Popup, new PaidFeaturesPopupViewModel.BlankDriversPopupParameter(true)
                        {
                            Title = AppResources.F_PaidFeature,
                            Action = ShowPositionGuidePopup,
                        });
                    }
                }
                else
                {

                    _userDialogs.Alert(new AlertConfig()
                    {
                        Message = AppResources.F_ManageSubscriptionNoPremium,
                        OnAction = () => ShowViewModel<ManageSubscriptionViewModel>(),
                    });
                }
            }
            else
            {
                await _userDialogs.AlertAsync(new AlertConfig().Message = GetAdapterDisabledMessage());
            }
        }
        #endregion

        private async Task ShowPositionGuidePopup()
        {
            bool isPositionGuidePopupShown = settings.GetValueOrDefault(PositioningGuideViewModel.IsPositioningGuidePopupShown, false);

            if (isPositionGuidePopupShown)
            {
                await PasteDevicePopup();
            }
            else
            {
                ShowViewModel<PositioningGuideViewModel>(new PositioningGuideViewModel.BTGuidePopupParameter(true)
                {
                    Action = PasteDevicePopup,
                });
            }
        }

        private async Task PasteDevicePopup()
        {
            MyShowViewModel<ProgrammingViewModel>(RequestType.Popup, new ProgrammingViewModel.ProgrammingParameter(true)
            {
                Title = AppResources.F_ProgrammingViewWritingPopupTitle,
                ProgrammingAction = PasteDevice,
                ProgrammingType = ProgrammingType.Write,
                TextDialogAfterAction = AppResources.F_DialogProgrammingSuccessfulText
            });
        }

        private async Task<bool> CopyDevice(BaseViewModel vm, IProgrammingService programmingService, CancellationToken cancellationToken)
        {
            Logger.Trace("CopyDevice started.");
            try
            {
                cancellationToken.ThrowIfCancellationRequested();
                EcgSelector selector = programmingService.ReadDevice(cancellationToken);
                _sourceTag = programmingService.CopyDevice();
                if (_sourceTag != null)
                {
                    IsReadyToBeWritten = true;
                    cancellationToken.ThrowIfCancellationRequested();
                    IConfiguration configuration = await _selectConfigurationUiService.SelectConfiguration(this, selector);
                    cancellationToken.ThrowIfCancellationRequested();
                    ShowConfigurationData(configuration);
                    _messenger.Publish(new Events.ConfigurationResetEvent(this, true));
                    Logger.Trace("CopyDevice success.");
                    return true;
                }
                Logger.Error("CopyDevice failed");
            }
            catch (OperationCanceledException e)
            {
                throw e;
            }
            catch (Exception e)
            {
                Logger.Exception("CopyDevice", e);
            }
            return false;
        }

        private async Task<bool> PasteDevice(BaseViewModel vm, IProgrammingService programmingService, CancellationToken cancellationTokens)
        {
            Logger.Trace("PasteDevice started.");
            try
            {
                if (_sourceTag == null)
                {
                    Logger.Error("PasteDevice no source tag");
                    throw new OperationFailedException("CopyAndPasteViewModel.PasteDevice: no source tag", ErrorCodes.NoSourceTag);
                }
                if (!programmingService.PasteDevice(_sourceTag))
                {
                    Logger.Error("PasteDevice incompatible devices");
                    throw new OperationFailedException("CopyAndPasteViewModel.PasteDevice: incompatible devices", ErrorCodes.IncompatibleDevices);
                }
                Logger.Trace("PasteDevice success.");

                await _inAppPurchaseService.ConsumePurchaseAsync();

                return await Task.FromResult(true);
            }
            catch (OperationCanceledException e)
            {
                throw e;
            }
            catch (OperationFailedException operationFailedException)
            {
                Logger.Exception("CopyAndPasteViewModel.PasteDevice", operationFailedException);
                throw operationFailedException;
            }
            catch (Exception e)
            {
                Logger.Exception("PasteDevice", e);
            }
            Logger.Trace("PasteDevice failed.");
            return await Task.FromResult(false);
        }



    }
}
