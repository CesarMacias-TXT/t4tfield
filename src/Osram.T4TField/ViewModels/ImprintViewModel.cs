﻿using System;
using System.IO;
using System.Text.RegularExpressions;
using Osram.T4TField.Resources;
using Osram.T4TField.Utils;
using Osram.TFTBackend;
using Xamarin.Forms;
using Osram.T4TField.Contracts;

namespace Osram.T4TField.ViewModels
{
    public class ImprintViewModel : ConnectionBaseViewModel
    {
        //private string _imprintText;

        private readonly IAppInfo _appInfo;

        public ImprintViewModel(IAppInfo appInfo)
        {
            _appInfo = appInfo;

           
            Title = AppResources.F_ImprintTxt;
        }




        public override void Start()
        {
            base.Start();

            try
            {
                var aboutHtml = string.Empty;
                var resourceStream = ResourceUtils.GetLocalizedResourceStream(LocalizationService, "Osram.T4TField.Resources.AboutInfo.html");
                if (resourceStream != null)
                {
                    using (var reader = new StreamReader(resourceStream))
                    {
                        aboutHtml = reader.ReadToEnd();
                    }
                }

                if (Device.RuntimePlatform == Device.Android)
                {
                    var iconFileName = "HtmlImages/osram.png";
                    ImprintPage = aboutHtml.Replace("#versionnr#", Version).Replace("#icon#", iconFileName);
                }
                else
                {
                    var iconFileName = "osram.png";
                    ImprintPage = aboutHtml.Replace("#versionnr#", Version).Replace("#icon#", iconFileName);
                }
            }
            catch (Exception ex)
            {
                Logger.Exception(nameof(Start), ex);
            }
        }

        public string ApplicationName
        {
            get { return _appInfo.ApplicationName; }
        }

        public string Version
        {
            get { return $"v{_appInfo.Version}"; }
        }

        public string ImprintPage { get; set; }
    }
}