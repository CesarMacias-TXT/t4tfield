﻿using System;
using Osram.T4TField.Models;

namespace Osram.T4TField.ViewModels
{
    public class NFCInterfaceViewModel : ListItemViewModel
    {


        public NFCInterfaceViewModel( NFCInterface nfcInterface, Action<ListItemViewModel> selectItemAction, Action<ListItemViewModel> clickItemAction) : base(selectItemAction, clickItemAction)
        {
            Name = nfcInterface.Name;
            ItemIconSource = nfcInterface.ImageSource;
            HasArrow = nfcInterface.IsConfigurable;
            this.NfcInterface = nfcInterface;
        }


        public NFCInterface NfcInterface { get; set; }



    }
}