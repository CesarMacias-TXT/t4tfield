﻿using MvvmCross.Commands;
using Osram.TFTBackend.PersistenceService;
using System;
using System.Windows.Input;

namespace Osram.T4TField.ViewModels
{
    public class EcgDeviceInfoViewModel : ListItemViewModel
    {
        public ICommand DeletePasswordCommand { get; set; }
    
        public EcgDeviceInfoViewModel(EcgDeviceInfo ecgDeviceInfo, Action<ListItemViewModel> selectItemAction, Action<ListItemViewModel> clickItemAction, Action<EcgDeviceInfo> OnDeletePasswordCommand) : base(selectItemAction, clickItemAction)
        {
            this.DeletePasswordCommand = new MvxCommand<EcgDeviceInfo>(OnDeletePasswordCommand);  
            this.ecgDeviceInfo = ecgDeviceInfo;
            Description = ecgDeviceInfo.Description;
            Name = ecgDeviceInfo.FileName;
            HasArrow = true;
        }

        public EcgDeviceInfo ecgDeviceInfo { get; set; }
    }
}