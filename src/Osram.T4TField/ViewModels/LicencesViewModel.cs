﻿using System;
using System.IO;
using System.Text.RegularExpressions;
using Osram.T4TField.Resources;
using Osram.T4TField.Utils;
using Osram.TFTBackend;

namespace Osram.T4TField.ViewModels
{
    public class LicencesViewModel : ConnectionBaseViewModel
    {
        private string _thirdPartyLicences;

        public LicencesViewModel()
        {           
            Title = AppResources.Licenses;
        }

        public override void Start()
        {
            base.Start();

                try
                {
                    var licencesTxt = "";
                    var stream = ResourceUtils.GetLocalizedResourceStream(LocalizationService, "Osram.T4TField.Resources.LicenseInfo.html");
                    if (stream != null)  
                    {
                        using (var reader = new StreamReader(stream))
                        {
                            licencesTxt = reader.ReadToEnd();
                        }
                    }
                    _thirdPartyLicences = licencesTxt; // "<div>" + Regex.Replace(licencesTxt, @"\r\n?|\n", "<br />") + "</div>";
                }
                catch (Exception ex)
                {
                    Logger.Exception(nameof(Start), ex);
                }
        }

        public string ThirdPartyLicences
        {
            get { return _thirdPartyLicences; }
        }
    }
}