﻿using System;
using System.Collections.Generic;
using System.Linq;
using Osram.T4TField.Messaging;
using Osram.T4TField.Resources;

namespace Osram.T4TField.ViewModels
{
    public class LanguageViewModel : ConnectionBaseViewModel
    {

        public LanguageViewModel()
        {
            Title = AppResources.F_ChangeLanguage;
        }

        private void SelectLanguage(ListItemViewModel listItemViewModel)
        {
            LanguageDetailViewModel languageDetailViewModel = (LanguageDetailViewModel) listItemViewModel;

            if (languageDetailViewModel == null) return;

            if (!languageDetailViewModel.IsSelected)
            {
                languageDetailViewModel.SetSelected( true);
                return;
            }

            LocalizationService.CurrentLanguage = languageDetailViewModel.Language;
            _messenger.Publish(new Events.OnLanguageChangedEvent(this));
            this.Close();
        }

        public List<LanguageDetailViewModel> Languages { get; private set; }

        public new Parameter ClassParameter => (Parameter)base.ClassParameter;

        public override void Start()
        {
            base.Start();

            Languages = LocalizationService.SupportedLanguages.Select(l => new LanguageDetailViewModel(l, SelectLanguage)).ToList();
            var currentLanguage = LocalizationService.CurrentLanguage;
            var language = Languages.Single(l => l.Language == currentLanguage);
            language.SetSelected(true);
        }

        public class Parameter : BaseViewModelParameter
        {
            public Parameter(bool isModal)
                : base(isModal)
            {
            }

            public Action OnClose { get; set; }
        }
    }
}