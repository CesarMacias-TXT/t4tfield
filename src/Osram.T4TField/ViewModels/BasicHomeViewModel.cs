﻿using Acr.UserDialogs;
using MvvmCross.ViewModels;
using MvvmCross.Commands;
using Osram.T4TField.Contracts;
using Osram.T4TField.Resources;
using Osram.T4TField.Utils;
using Osram.T4TField.ViewModels.FeaturesViewModel;
using Osram.TFTBackend.ConfigurationService.Contracts;
using Osram.TFTBackend.DataModel.Feature.Contracts;
using Osram.TFTBackend.InAppPurchaseService.Contracts;
using Osram.TFTBackend.KeyService.Contracts;
using Osram.TFTBackend.Messaging;
using Plugin.Settings.Abstractions;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows.Input;
using static Osram.T4TField.ViewModels.FeaturesCarouselViewModel;

namespace Osram.T4TField.ViewModels
{
    class BasicHomeViewModel : HomeViewModel
    {
        public BasicHomeViewModel(IConfigurationService configurationService, ISelectConfigurationUiService selectConfigurationUiService, IUserDialogs dialog, ISettings settings, IKeyService keyService, IInAppPurchaseService inAppPurchaseService) : base(configurationService, selectConfigurationUiService, dialog, settings, keyService, inAppPurchaseService)
        {
            InitialiseBasicModeGui();
        }


        public ICommand TuningButtonCommand { get; set; }
        public ICommand ConstantLumenButtonCommand { get; set; }
        public ICommand DimmingButtonCommand { get; set; }
        public ICommand CopyAndPasteButtonCommand { get; set; }
        public ICommand ReportButtonCommand { get; set; }
        public ICommand ProtectionButtonCommand { get; set; }


        private void OnTuningButtonCommand()
        {
            ShowViewModel<FeaturesCarouselViewModel>(new FeaturesCarouselViewModel.CarouselParameter<TuningFactorViewModel>(T4TFAppMode.BASIC_MODE, FeatureButtonList, FeatureType.TuningFactor));
        }

        private void OnCopyAndPasteButtonCommand()
        {
            ShowViewModel<CopyAndPasteViewModel>();
        }

        private void OnReportButtonCommandd()
        {
            ShowViewModel<ReportViewModel>();
        }

        private void OnDimmingButtonCommand()
        {
            ShowViewModel<FeaturesCarouselViewModel>(new FeaturesCarouselViewModel.CarouselParameter<AstroDimViewModel>(T4TFAppMode.BASIC_MODE, FeatureButtonList, FeatureType.AstroDIM));
        }

        private void OnConstantLumenButtonCommand()
        {
            ShowViewModel<FeaturesCarouselViewModel>(new CarouselParameter<ConstantLumenViewModel>(T4TFAppMode.BASIC_MODE, FeatureButtonList, FeatureType.ConstantLumen));
        }

        private void OnProtectionButtonCommand()
        {
            ShowViewModel<ProtectionViewModel>();
        }

        private FeatureInfoButton dimmingBtn;
        public FeatureInfoButton DimmingBtn {
            get { return dimmingBtn; }
            set {
                dimmingBtn = value;
                RaisePropertyChanged();
            }
        }

        private string _bmMenuChooseAction;
        public string BMMenuChooseAction {
            get { return _bmMenuChooseAction; }
            set {
                _bmMenuChooseAction = value;
                RaisePropertyChanged();
            }
        }

        private ImageButton oEMkeyBtn;
        public ImageButton OEMkeyBtn {
            get { return oEMkeyBtn; }
        }

        private ImageButton copyConfigurationBtn;
        public ImageButton CopyConfigurationBtn {
            get { return copyConfigurationBtn; }
            set { copyConfigurationBtn = value; }
        }

        private ImageButton reportBtn;
        public ImageButton ReportBtn
        {
            get { return reportBtn; }
            set { reportBtn = value; }
        }

        private FeatureInfoButton tuningBtn;

        public FeatureInfoButton TuningBtn {
            get { return tuningBtn; }
            set {
                tuningBtn = value;
                RaisePropertyChanged();
            }
        }

        private FeatureInfoButton constantLumenBtn;

        public FeatureInfoButton ConstantLumenBtn
        {
            get { return constantLumenBtn; }
            set
            {
                constantLumenBtn = value;
                RaisePropertyChanged();
            }
        }

        public ObservableCollection<FeatureInfoButton> FeatureButtonList { get; private set; }

        public override void ShowConfigurationData(Events.ConfigurationChangedEvent configurationEvent)
        {
            base.ShowConfigurationData(configurationEvent);
            UpdateFeaturesButtonStatus();
        }

        public override void ResetConfigurationData(Events.ConfigurationResetEvent configurationEvent)
        {
            base.ResetConfigurationData(configurationEvent);
        }

        public override void OnRemoveSelectedECGCommand()
        {
            base.OnRemoveSelectedECGCommand();
            IsSelectECGVisible = true;
            IsECGInfoBoxVisible = false;

            TuningBtn.FeatureInfo.IsAvailable = false;
            ConstantLumenBtn.FeatureInfo.IsAvailable = false;
            DimmingBtn.FeatureInfo.IsAvailable = false;
            TuningBtn.FeatureInfo.Lock = ProtectionLock.None;
            ConstantLumenBtn.FeatureInfo.Lock = ProtectionLock.None;
            DimmingBtn.FeatureInfo.Lock = ProtectionLock.None;
            UpdateFeaturesButtonStatus();
        }

        private void UpdateFeaturesButtonStatus()
        {
            TuningBtn.RaiseAllPropertiesChanged();
            ConstantLumenBtn.RaiseAllPropertiesChanged();
            DimmingBtn.RaiseAllPropertiesChanged();
        }

        protected override void FeatureListUpdated()
        {
            //if (!_keyService.IsDevicePasswordProtected)
            //{
            //    _userDialogs.Alert(new AlertConfig().Message = AppResources.F_NoMasterPasswordProtectedMessage);
            //}
            UpdateFeaturesButtonStatus();
        }

        private void InitialiseBasicModeGui()
        {

            BMMenuChooseAction = AppResources.F_BMMenuChooseAction;

            IsOnlyReadEcgBtn = false;
            TuningButtonCommand = new MvxCommand(OnTuningButtonCommand);
            CopyAndPasteButtonCommand = new MvxCommand(OnCopyAndPasteButtonCommand);
            ReportButtonCommand = new MvxCommand(OnReportButtonCommandd);
            DimmingButtonCommand = new MvxCommand(OnDimmingButtonCommand);
            ConstantLumenButtonCommand = new MvxCommand(OnConstantLumenButtonCommand);
            ProtectionButtonCommand = new MvxCommand(OnProtectionButtonCommand);
            FeatureButtonList = new ObservableCollection<FeatureInfoButton>();
            var Featureslist = FeaturesUtils.getListOfAvailableFeatures();

            dimmingBtn = new FeatureInfoButton(GetFeaturesByType(FeatureType.AstroDIM, Featureslist));
            SetGraphicalInfo(dimmingBtn, "dimming.png", AppResources.F_BMMenuDimming, DimmingButtonCommand);


            tuningBtn = new FeatureInfoButton(GetFeaturesByType(FeatureType.TuningFactor, Featureslist));
            SetGraphicalInfo(tuningBtn, "tuning.png", AppResources.F_BMmenuTuning, TuningButtonCommand);

            constantLumenBtn = new FeatureInfoButton(GetFeaturesByType(FeatureType.ConstantLumen, Featureslist));
            SetGraphicalInfo(constantLumenBtn, "constant_lumen.png", AppResources.F_Report_CostantLumen, ConstantLumenButtonCommand);

            copyConfigurationBtn = new ImageButton();
            copyConfigurationBtn.IconLink = "copyPaste.png";
            copyConfigurationBtn.Label = AppResources.F_BMMenuCopyPaste;
            copyConfigurationBtn.OnClickButton = CopyAndPasteButtonCommand;

            reportBtn = new ImageButton();
            reportBtn.IconLink = "report_home.png";
            reportBtn.Label = AppResources.F_Report;
            reportBtn.OnClickButton = ReportButtonCommand;

            oEMkeyBtn = new ImageButton();
            oEMkeyBtn.IconLink = "OEMKeyList.png";
            oEMkeyBtn.Label = AppResources.F_BMMenuProtection;
            oEMkeyBtn.OnClickButton = ProtectionButtonCommand;

            FeatureButtonList.Add(dimmingBtn);
            FeatureButtonList.Add(tuningBtn);
            FeatureButtonList.Add(constantLumenBtn);

            OnRemoveSelectedECGCommand();
        }

        protected override void OnLanguageChanged()
        {
            copyConfigurationBtn.Label = AppResources.F_BMMenuCopyPaste;
            oEMkeyBtn.Label = AppResources.F_BMMenuProtection;
            dimmingBtn.Label = AppResources.F_BMMenuDimming;
            tuningBtn.Label = AppResources.F_BMmenuTuning;
            constantLumenBtn.Label = AppResources.F_Report_CostantLumen;
            BMMenuChooseAction = AppResources.F_BMMenuChooseAction;

            base.OnLanguageChanged();
        }

        private void SetGraphicalInfo(FeatureInfoButton fetaureButton, String ImageLink, String FeatureName, ICommand Command)
        {
            fetaureButton.IconLink = ImageLink;
            fetaureButton.Label = FeatureName;
            fetaureButton.WarningIconLink = "FWarningBM.png";
            fetaureButton.ErrorIconLink = "FErrorBM.png";
            fetaureButton.LockMasterIconLink = "LockMasterBM.png";
            fetaureButton.LockServiceIconLink = "LockServiceBM.png";
            fetaureButton.OnClickButton = Command;

        }


        private IFeatureInfo GetFeaturesByType(FeatureType featureType, List<IFeatureInfo> ListOfFeatures)
        {
            var currentFeature = ListOfFeatures.Where(x => x.FeatureType == featureType).First();
            //currentFeature.Lock = ProtectionLock.Service;
            //currentFeature.Status = FeatureStatus.Warning;
            //currentFeature.Lock = ProtectionLock.Service;
            //currentFeature.IsEnabled = true;

            return currentFeature;
        }

        #region Lifecycle

        public override void Resumed()
        {
            base.Resumed();

            UpdateFeaturesButtonStatus();
        }

        #endregion
    }
}
