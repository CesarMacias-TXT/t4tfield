﻿using System;
using System.IO;
using MvvmCross.Commands;
using Plugin.Settings.Abstractions;
using Osram.T4TField.Contracts;
using Osram.T4TField.Resources;
using Osram.T4TField.Utils;
using Osram.TFTBackend;

namespace Osram.T4TField.ViewModels
{
    public class TermsOfUseViewModel : ConnectionBaseViewModel
    {
        private readonly IAppInfo _appInfo;
        private readonly ISettings _settings;
        private string _termsOfUse;

        public TermsOfUseViewModel( IAppInfo appInfo, ISettings settings)
        {
            _appInfo = appInfo;
            _settings = settings;
            Title = AppResources.F_TermsConditionsTxt;
        }

        public override void Start()
        {
            base.Start();

            try
            {
                var resourceName = "Osram.T4TField.Resources.TermsOfUse.html";

                var termsOfUseText = string.Empty;
                var resourceStream = ResourceUtils.GetLocalizedResourceStream(LocalizationService, resourceName);
                if (resourceStream != null)
                {
                    using (var reader = new StreamReader(resourceStream))
                    {
                        termsOfUseText = reader.ReadToEnd();
                    }
                }

                _termsOfUse = termsOfUseText.Replace("#versionnr#", $"v{_appInfo.Version}");
            }
            catch (Exception ex)
            {
                Logger.Exception(nameof(Start), ex);
            }
        }

        public string TermsOfUse
        {
            get { return _termsOfUse; }
        }

        public bool IsModal
        {
            get { return ClassParameter != null && ClassParameter.IsModal; }
        }

        private MvxCommand _iReadCommand;
        public MvxCommand IReadCommand
        {
            get { return _iReadCommand ?? (_iReadCommand = new MvxCommand(IRead)); }
        }

        private void IRead()
        {
            Cancel();
        }

        public class Parameter : BaseViewModelParameter
        {
            public Parameter(bool isModal) : base(isModal)
            {
            }
        }
    }
}