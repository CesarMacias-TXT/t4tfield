﻿using MvvmCross.Commands;
using Osram.T4TField.Resources;
using Osram.TFTBackend.InitalizeDatabaseService;
using Osram.TFTBackend.RestService.Contracts;
using Plugin.Settings.Abstractions;
using System.Threading.Tasks;
using System.Windows.Input;

namespace Osram.T4TField.ViewModels
{
    public class OnlineServicesManagerViewModel : ConnectionBaseViewModel
    {
        public const string IsOnlineServicesViewModelPopupShown = "IsOnlineServicesViewModelPopupShownShown";

        public ICommand ActiveCommand { get; set; }
        public ICommand DisableCommand { get; set; }

        private IRestService _restService;
        private readonly ISettings _settings;

        public OnlineServicesManagerViewModel(ISettings settings, IRestService restService)
        {
            _settings = settings;
            _restService = restService;

            Title = AppResources.F_OnlineServices;
            ActiveCommand = new MvxAsyncCommand(OnActiveCommand);
            isSwitchSelected = settings.GetValueOrDefault(InitalizeDatabaseService.IsOnlineServicesActive, false);
            lastUpdateDate = settings.GetValueOrDefault(InitalizeDatabaseService.LastUpdateOnlineServices, "");
            isImportingNewConfigurations = false;
        }        

        private bool isSwitchSelected = false;

        public bool IsSwitchSelected
        {
            get { return isSwitchSelected; }
            set
            {
                isSwitchSelected = value;
                settings.AddOrUpdateValue(InitalizeDatabaseService.IsOnlineServicesActive, value);
                RaisePropertyChanged();
            }
        }

        private string lastUpdateDate = "";
        public string LastUpdateDate
        {
            get { return lastUpdateDate; }
            set
            {
                lastUpdateDate = value;
                settings.AddOrUpdateValue(InitalizeDatabaseService.LastUpdateOnlineServices, value);
                RaisePropertyChanged();
            }
        }

        private bool isImportingNewConfigurations = false;
        public bool IsImportingNewConfigurations
        {
            get { return isImportingNewConfigurations; }
            set
            {
                isImportingNewConfigurations = value;
                RaisePropertyChanged();
            }
        }

        private async Task OnActiveCommand()
        {
            IsImportingNewConfigurations = true;
            await _restService.ImportNewConfigurations();
            IsImportingNewConfigurations = false;
            this.Close();
        }

        public override void Start()
        {
            base.Start();

            if(!settings.GetValueOrDefault(LiabilityViewModel.IsLiabilityPopupShown, false))
            {

            }
        }
    }

}
