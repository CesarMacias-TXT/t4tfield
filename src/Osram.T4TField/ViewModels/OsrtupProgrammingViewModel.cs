﻿using Acr.UserDialogs;
using MvvmCross.Commands;
using Osram.T4TField.Resources;
using Osram.TFTBackend;
using Osram.TFTBackend.BaseTypes;
using Osram.TFTBackend.BaseTypes.Exceptions;
using Osram.TFTBackend.ConfigurationService.Contracts;
using Osram.TFTBackend.ConfigurationService.DataModel;
using Osram.TFTBackend.InAppPurchaseService.Contracts;
using Osram.TFTBackend.KeyService.Contracts;
using Osram.TFTBackend.PersistenceService;
using Osram.TFTBackend.PersistenceService.Contracts;
using Osram.TFTBackend.ProgrammingService.Contracts;
using Osram.TFTBackend.RestService.Contracts;
using Plugin.Settings.Abstractions;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;

namespace Osram.T4TField.ViewModels
{
    public class OsrtupProgrammingViewModel : ConnectionBaseViewModel
    {
        private readonly IPersistenceService _persistenceService;
        private readonly IConfigurationService _configurationService;
        private readonly IKeyService _keyService;
        private readonly IUserDialogs _userDialogs;
        private readonly ISettings _settings;
        private readonly IInAppPurchaseService _inAppPurchaseService;
        private IRestService _restService;

        private string _productionFileName;
        public string ProductionFileName {
            get { return _productionFileName; }
            set {
                _productionFileName = value;
                RaisePropertyChanged();
            }
        }

        private ImageSource ecgImageSource;
        public ImageSource ECGImageSource {
            get { return ecgImageSource; }
            set {
                ecgImageSource = value;
                RaisePropertyChanged();
            }
        }

        public ICommand ProgrammCommand { get; set; }

        public OsrtupProgrammingViewModel(ISettings settings, IPersistenceService persistenceService, IConfigurationService configurationService, IKeyService keyService, IUserDialogs dialogs, IInAppPurchaseService inAppPurchaseService, IRestService restService)
        {
            Title = AppResources.F_Navigation_OstrupProgramming;
            _persistenceService = persistenceService;
            _configurationService = configurationService;
            _keyService = keyService;
            _userDialogs = dialogs;
            _settings = settings;
            _inAppPurchaseService = inAppPurchaseService;
            _restService = restService;

            ProgrammCommand = new MvxCommand(async() => await OnProgrammCommandAsync());
        }

        private async Task OnProgrammCommandAsync()
        {
            if (CheckIfAdapterIsReady())
            {
                if (await _inAppPurchaseService.CanConsumePurchaseAsync())
                {
                    bool isPaidFeaturesPopupShown = settings.GetValueOrDefault(PaidFeaturesPopupViewModel.IsPaidFeaturesPopupViewModelPopupShown, false);
                    //bool isSubscriptionActive = _inAppPurchaseService.GetActivePurchasedSubscriptions().Count() > 0;

                    if (isPaidFeaturesPopupShown)
                    {
                        await ShowPositionGuidePopup();
                    }
                    else
                    {
                        MyShowViewModel<PaidFeaturesPopupViewModel>(RequestType.Popup, new PaidFeaturesPopupViewModel.BlankDriversPopupParameter(true)
                        {
                            Title = AppResources.F_PaidFeature,
                            Action = ShowPositionGuidePopup,
                        });
                    }
                }
                else
                {
                    _userDialogs.Alert(new AlertConfig()
                    {
                        Message = AppResources.F_ManageSubscriptionNoPremium,
                        OnAction = () => ShowViewModel<ManageSubscriptionViewModel>(),
                    });
                }
            }
            else
            {
                await _userDialogs.AlertAsync(new AlertConfig().Message = GetAdapterDisabledMessage());
            }
        }

        private async Task ShowPositionGuidePopup()
        {
            bool isPositionGuidePopupShown = settings.GetValueOrDefault(PositioningGuideViewModel.IsPositioningGuidePopupShown, false);

            if (isPositionGuidePopupShown)
            {
                await ProgramEcgPopup();
            }
            else
            {
                ShowViewModel<PositioningGuideViewModel>(new PositioningGuideViewModel.BTGuidePopupParameter(true)
                {
                    Action = ProgramEcgPopup,
                });
            }
        }

        private async Task ProgramEcgPopup()
        {
            MyShowViewModel<ProgrammingViewModel>(RequestType.Popup, new ProgrammingViewModel.ProgrammingParameter(true)
            {
                Title = AppResources.F_ProgrammingViewWritingPopupTitle,
                ProgrammingAction = ProgramEcg,
                ProgrammingType = ProgrammingType.Write,
                TextDialogAfterAction = AppResources.F_DialogProgrammingSuccessfulText
            });
        }

        private async Task<bool> ProgramEcg(BaseViewModel owner, IProgrammingService programmingService, CancellationToken cancellationToken)
        {
            Logger.Trace("OsrtupProgrammingViewModel.ProgrammEcg reading ECG ...");
            EcgSelector selector = programmingService.ReadDevice(cancellationToken);
            if (selector.IsValid)
            {
                Logger.Trace($"OsrtupProgrammingViewModel.ProgrammEcg reading ECG done Selector = {selector}");
                IConfiguration configuration = _configurationService.GetCurrentConfiguration();

                if (configuration != null && selector.EqualsWithoutHwVersion(configuration.Selector))
                {
                    List<EcgProperty> properties = _configurationService.GetConfigurationProperties().ToList();
                    //read only the master password
                    IEnumerable<EcgProperty> masterPassword = properties.Where(p => (p.PropertyName == "BIO-0:MasterPwd") || (p.PropertyName == "MSK-0:MasterPwd") || (p.PropertyName == "PwmConfig-0:MasterPwd"));
                    programmingService.GetPropertyValues(masterPassword);

                    // only allow programming when the passwords are the same or the destination is not protected
                    //if (configuration.NewMasterPassword == 0)
                    //{
                    //    throw new OperationFailedException("ProgrammEcg: Master passwords is not setted!", ErrorCodes.MasterKeysNotSetted);
                    //}

                    if (!_keyService.HasMasterPassword())
                    {
                        // Manage of Master Password
                        _keyService.ManageMasterPasswordForDeviceProgramming(0, configuration.NewMasterPassword);

                        // Manage of Service Password, the service pass is always clear, not encrypted
                        _keyService.ManageServicePasswordForDeviceProgramming(GetServicePasswordDecryptedBytes(properties));

                        programmingService.ProgramConfiguration(properties, "osrtupImport");

                        await _inAppPurchaseService.ConsumePurchaseAsync();

                        return await Task.FromResult(true);
                    }
                    else if (_keyService.ValidateMasterPassword(configuration.UnlockDriver ? configuration.ReworkKey : configuration.NewMasterPassword))
                    {
                        // Manage of Master Password
                        _keyService.ManageMasterPasswordForDeviceProgramming(configuration.OldMasterPassword, configuration.NewMasterPassword);

                        // Manage of Service Password, the service pass is always clear, not encrypted
                        _keyService.ManageServicePasswordForDeviceProgramming(GetServicePasswordDecryptedBytes(properties));

                        programmingService.ProgramConfiguration(properties, "osrtupImport");

                        await _inAppPurchaseService.ConsumePurchaseAsync();

                        return await Task.FromResult(true);
                    }

                    throw new OperationFailedException("ProgrammEcg: Master password are not the same as in the device!", ErrorCodes.MasterKeysNotMatching);
                }
                throw new OperationFailedException("ProgrammEcg: Device is not supported!", ErrorCodes.DeviceNotSupported);
            }
            Logger.Trace("OsrtupProgrammingViewModel.ProgrammEcg reading ECG failed");
            return await Task.FromResult(false);
        }

        private uint GetServicePasswordDecryptedBytes(List<EcgProperty> properties)
        {
            EcgProperty servicePassword = properties.Where(p => (p.PropertyName == "MSK-0:ServicePwd") || (p.PropertyName == "PwmConfig-0:ServicePwd")).FirstOrDefault();
            if (servicePassword == null)
            {
                return 0;
            }
            return Convert.ToUInt32(servicePassword.IntValue);
        }

        public override void Resumed()
        {
            base.Resumed();
            EcgDeviceInfo info = (ClassParameter as OsrtupProgrammingParameter)?.Info;
            if (info != null)
            {
                ProductionFileName = info.FileName;
                EcgTuple ecg = _persistenceService.GetDeviceConfiguration(info.Id);
                _configurationService.SelectConfiguration(ecg);
                if (ecg.ThePicture != null && ecg.ThePicture.Length > 0)
                {
                    ECGImageSource = ImageSource.FromStream(() => new MemoryStream(ecg.ThePicture));
                }
                else
                {
                    ECGImageSource = "ecgDefault.png";
                }
            }
        }

        public override void Suspended()
        {
            base.Suspended();
            Logger.Trace("OsrtupProgrammingViewModel.Suspended");
            _configurationService.ResetConfiguration();
        }

        #region Parameter for this ViewModel
        public class OsrtupProgrammingParameter : BaseViewModelParameter
        {
            public OsrtupProgrammingParameter(bool isModal, EcgDeviceInfo info)
                : base(isModal)
            {
                Info = info;
            }

            public EcgDeviceInfo Info { get; }
        }
        #endregion
    }
}
