﻿using Osram.TFTBackend.DataModel.Feature.Contracts;
using Osram.TFTBackend.KeyService.Contracts;

namespace Osram.T4TField.ViewModels
{
    public class BaseFeatureViewModel : BaseViewModel
    {
        public BaseFeatureViewModel(IKeyService keyService)
        {
            IsMasterPasswordProtected = keyService.IsDevicePasswordProtected;
        }

        public bool _isEnabled { get; set; }

        public bool IsEnabled {
            get { return _isEnabled; }
            set {
                _isEnabled = value;
                RaisePropertyChanged(() => IsEnabled);
                RaisePropertyChanged(() => IsEditable);
            }
        }

        public bool _isMasterPasswordProtected { get; set; }

        public bool IsMasterPasswordProtected {
            get { return _isMasterPasswordProtected; }
            set {
                _isMasterPasswordProtected = value;
                RaisePropertyChanged(() => IsMasterPasswordProtected);
                RaisePropertyChanged(() => IsEditable);
            }
        }

        public ProtectionLock _lock { get; set; }

        public ProtectionLock Lock {
            get { return _lock; }
            set {
                _lock = value;
                RaisePropertyChanged(() => Lock);
            }
        }

        public bool IsEditable {
            get {
                //if (IsEnabled == false || Lock == ProtectionLock.Master || !IsMasterPasswordProtected)
                if (IsEnabled == false || Lock == ProtectionLock.Master)
                {
                    return false;
                }
                return true;
            }
        }

    }
}
