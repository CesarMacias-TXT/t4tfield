﻿using System;
using System.Windows.Input;
using MvvmCross.ViewModels;
using MvvmCross.Commands;

namespace Osram.T4TField.ViewModels
{
    public class ListItemViewModel : MvxViewModel
    {
        public Action<ListItemViewModel> SelectItemAction { get; set;}
        public Action<ListItemViewModel> ClickItemAction { get; set;}

        private bool _isSelected;
        public ICommand ClickItemCommand { get; }

        public ListItemViewModel(Action<ListItemViewModel> selectItemAction = null, Action<ListItemViewModel> clickItemAction = null)
        {
            SelectItemAction = selectItemAction;
            ClickItemAction = clickItemAction;

            ClickItemCommand = new MvxCommand(ClickItem);
        }

        public virtual string Name { get; set; }

        public virtual string Description { get; set; }

        public string ItemIconSource { get; set; }

        public bool IsSelected
        {
            get
            {
                return _isSelected;
            }
            set
            {
                if (_isSelected == value)
                    return;

                _isSelected = value;
                RaiseAllPropertiesChanged();
                SelectItemAction?.Invoke(this);
            }
        }

        public virtual bool HasArrow { get; set; }

        public bool IsSelectingAllowed { get; set; }

        public void SetSelected(bool isSelected)
        {
            _isSelected = isSelected;
            RaisePropertyChanged(nameof(IsSelected));
        }


        public void SetHasArrow(bool hasArrow)
        {
              HasArrow = hasArrow;
              RaisePropertyChanged(nameof(HasArrow));
        }




        private void ClickItem()
        {
            ClickItemAction?.Invoke(this);
        }

        public string Icon => ItemIconSource;
    }
}
