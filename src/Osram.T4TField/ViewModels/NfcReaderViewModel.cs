﻿using MvvmCross;
using Plugin.Settings.Abstractions;
using System.Collections.Generic;
using Osram.T4TField.Models;
using Osram.T4TField.Resources;
using Osram.TFTBackend.NfcService.Contracts;
using Osram.TFTBackend.NfcService.NfcDataTypes;
using System;
using System.Linq;

namespace Osram.T4TField.ViewModels
{
    public class NfcReaderViewModel : ConnectionBaseViewModel
    {       
        public string BTInterface => AppResources.About;
        public string NfcInterface => AppResources.F_ChooseNfcReader;

        private ISettings _settings;

        public List<NFCInterfaceViewModel> NFCInterfaceList { get; private set; }

        public NfcReaderViewModel(ISettings settings)
        {
            _settings = settings;
            Title = AppResources.F_ChooseNfcReader;
            NFCInterfaceList = createNFCInterfaceList();
          
        }

        private List<NFCInterfaceViewModel> createNFCInterfaceList()
        {
            var L = new List<NFCInterfaceViewModel>();
            var NFCInterface = new NFCInterface() { Name = AppResources.F_ReaderMobile, ImageSource = "nfcIcon.png", IsConfigurable=false , NFCInterfaceType = NfcReaderType.Mobile};
            var BTInterface = new NFCInterface()  { Name = AppResources.F_ReaderBTLE, ImageSource = "btIcon.png", IsConfigurable = false, ConfigurationDescriptor = AppResources.F_SearchDevice, NFCInterfaceType = NfcReaderType.Bluetooth };

            L.Add(new NFCInterfaceViewModel(NFCInterface, SelectInterface, null));
            L.Add(new NFCInterfaceViewModel(BTInterface, SelectInterface, null));
            return L;
        }

        private void deselectAllItem(string name) {

            foreach (NFCInterfaceViewModel NFCInterface in NFCInterfaceList)
            {
                if (!NFCInterface.Name.Equals(name))
                {
                     NFCInterface.SetSelected(false) ;
                     NFCInterface.SetHasArrow(false);
                     NFCInterface.NfcInterface.ConfigurationDescriptor = String.Empty;
                }
                
            }

        }

        private void configureBTInterface(NFCInterfaceViewModel _InterfaceViewModel)
        {
            _InterfaceViewModel.SetHasArrow(true);
            if (Mvx.IoCProvider.Resolve<INfcCommunication>().ReaderType != NfcReaderType.Bluetooth){
                SelectReader(NfcReaderType.Bluetooth);
                _settings.AddOrUpdateValue("NFCSelectedInterface", (decimal)NfcReaderType.Bluetooth);
            }

            if (BluetoothReader.IsDeviceConnected)  {
                _InterfaceViewModel.NfcInterface.ConfigurationDescriptor = BluetoothReader.ConnectedDevice.Name;
            }
            else {
                _InterfaceViewModel.NfcInterface.ConfigurationDescriptor = "Search device";
            }           
        }

        private void configureMobileInterface()
        {
            if (Mvx.IoCProvider.Resolve<INfcCommunication>().ReaderType != NfcReaderType.Mobile)
            {
                SelectReader(NfcReaderType.Mobile);
            }
            _settings.AddOrUpdateValue("NFCSelectedInterface", (decimal)NfcReaderType.Mobile);
        }

        private void SelectInterface(ListItemViewModel listItemViewModel)
        {
            NFCInterfaceViewModel _InterfaceViewModel = (NFCInterfaceViewModel)listItemViewModel;

            if (_InterfaceViewModel == null) return;
            deselectAllItem(_InterfaceViewModel.Name);

            if (!_InterfaceViewModel.IsSelected)
            {
                _InterfaceViewModel.SetSelected(true);
                if (_InterfaceViewModel.NfcInterface.NFCInterfaceType.Equals(NfcReaderType.Bluetooth))
                {
                    SearchBTDevice();
                }
                return;
            }

            ConfigureInterfaceByType(_InterfaceViewModel);
        }

        private void  ConfigureInterfaceByType( NFCInterfaceViewModel interfaceViewModel) {
            switch (interfaceViewModel.NfcInterface.NFCInterfaceType)
            {
                case NfcReaderType.Mobile:
                    configureMobileInterface();
                    break;
                case NfcReaderType.Bluetooth:
                    configureBTInterface(interfaceViewModel);
                    break;
                default:
                    configureMobileInterface();
                    break;
            }
        }

        private void SearchBTDevice()
        {
            var _selectedInterface = (NfcReaderType)(_settings.GetValueOrDefault("NFCSelectedInterface", (decimal)NfcReaderType.Mobile));
            if(_selectedInterface.Equals(NfcReaderType.Bluetooth))
              ShowViewModel<SearchingBTDeviceViewModel>();
        }

        private void SelectReader(NfcReaderType type)
        {
            _messenger.Publish(new TFTBackend.Messaging.Events.OnNfcReaderChangedEvent(this, type));
        }

        public override void Start()
        {
            base.Start();
            //restoreState();
            //nfcInterface.SetSelected(true);
        }

        public override void Resumed()
        {
            base.Resumed();
            restoreState();

            // setBTInformation();
        }

        public void restoreState()
        {
            var _selectedInterface = (NfcReaderType)(_settings.GetValueOrDefault("NFCSelectedInterface", (decimal)NfcReaderType.Mobile));
            var nfcInterface = NFCInterfaceList.Single(l => l.NfcInterface.NFCInterfaceType == _selectedInterface);
            NFCInterfaceViewModel _InterfaceViewModel = nfcInterface;
            // SelectInterface(nfcInterface);
            _InterfaceViewModel.SetSelected(true);
            ConfigureInterfaceByType(_InterfaceViewModel);
        }
    }
}
