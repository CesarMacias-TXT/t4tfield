﻿using MvvmCross.Navigation;
using MvvmCross.Commands;
using Osram.T4TField.Resources;
using Plugin.Settings.Abstractions;
using System;
using System.Threading.Tasks;
using System.Windows.Input;

namespace Osram.T4TField.ViewModels
{
    public class PositioningGuideViewModel : ConnectionBaseViewModel
    {
        public const string IsPositioningGuidePopupShown = "IsPositioningGuidePopupShown";

        public ICommand ConfirmCommand { get; set; }

        private readonly IMvxNavigationService _navigation;
        private readonly ISettings _settings;

        public PositioningGuideViewModel(ISettings settings, IMvxNavigationService navigation)
        {
            _navigation = navigation;
            _settings = settings;
            Title = AppResources.F_ProgrammingInformation;
            ConfirmCommand = new MvxCommand(OnConfirmCommand);
        }

        private bool isSwitchSelected = false;
        private Func<Task> _action;

        public bool IsSwitchSelected {
            get { return isSwitchSelected; }
            set {
                isSwitchSelected = value;
                RaisePropertyChanged();
            }
        }

        private void OnConfirmCommand()
        {
            if (IsSwitchSelected)
            {
                settings.AddOrUpdateValue(IsPositioningGuidePopupShown, true);
            }

            _navigation.Close(this);
            _action();
        }

        public override void Start()
        {
            base.Start();

            _action = (ClassParameter as BTGuidePopupParameter)?.Action;
        }

        public class BTGuidePopupParameter : BaseViewModelParameter
        {
            public BTGuidePopupParameter(bool isModal) : base(isModal)
            {
            }

            public Func<Task> Action { get; set; }
        }
    }

}
