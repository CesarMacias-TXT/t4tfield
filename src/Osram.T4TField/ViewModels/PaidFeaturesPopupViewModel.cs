﻿using MvvmCross.Commands;
using Osram.T4TField.Resources;
using Plugin.Settings.Abstractions;
using System;
using System.Threading.Tasks;
using System.Windows.Input;
using Rg.Plugins.Popup.Services;

namespace Osram.T4TField.ViewModels
{
    public class PaidFeaturesPopupViewModel : ConnectionBaseViewModel
    {
        public const string IsPaidFeaturesPopupViewModelPopupShown = "IsPaidFeaturesPopupViewModelPopupShownShown";

        public ICommand ConfirmCommand { get; set; }
        public ICommand DisableCommand { get; set; }

        private readonly ISettings _settings;

        public PaidFeaturesPopupViewModel(ISettings settings)
        {
            _settings = settings;
            Title = AppResources.F_PaidFeature;
            ConfirmCommand = new MvxCommand(OnConfirmCommand);
            DisableCommand = new MvxCommand(OnDisableCommand);
        }

        private bool isSwitchSelected = false;
        private Func<Task> _action;

        public bool IsSwitchSelected {
            get { return isSwitchSelected; }
            set {
                isSwitchSelected = value;
                RaisePropertyChanged();
            }
        }

        private void OnConfirmCommand()
        {
            if (IsSwitchSelected)
            {
                settings.AddOrUpdateValue(IsPaidFeaturesPopupViewModelPopupShown, true);
            }
            ClosePopup();
            _action();            
        }

        private void OnDisableCommand()
        {
            if (IsSwitchSelected)
            {
                settings.AddOrUpdateValue(IsPaidFeaturesPopupViewModelPopupShown, true);
            }
            ClosePopup();         
        }

        private void ClosePopup()
        {
            try
            {
                PopupNavigation.PopAsync();
            }
            catch (Exception) { }
        }

        public override void Start()
        {
            base.Start();
            Title = (ClassParameter as BlankDriversPopupParameter)?.Title;
            _action = (ClassParameter as BlankDriversPopupParameter)?.Action;
        }        

        public class BlankDriversPopupParameter : BaseViewModelParameter
        {
            public BlankDriversPopupParameter(bool isModal) : base(isModal)
            {
            }

            public String Title { get; set; }
            public Func<Task> Action { get; set; }
        }
    }
}
