using MvvmCross.Commands;
using Osram.T4TField.Resources;
using Osram.TFTBackend.InitalizeDatabaseService;
using Osram.TFTBackend.RestService.Contracts;
using Plugin.Settings.Abstractions;
using System.Windows.Input;

namespace Osram.T4TField.ViewModels
{
    public class OnlineServicesViewModel : ConnectionBaseViewModel
    {
        public const string IsOnlineServicesViewModelPopupShown = "IsOnlineServicesViewModelPopupShownShown";

        public ICommand ActiveCommand { get; set; }
        public ICommand DisableCommand { get; set; }

        private IRestService _restService;
        private readonly ISettings _settings;

        public OnlineServicesViewModel(ISettings settings, IRestService restService)
        {
            _settings = settings;
            _restService = restService;

            Title = AppResources.F_OnlineServices;
            ActiveCommand = new MvxCommand(OnActiveCommand);
            DisableCommand = new MvxCommand(OnDisableCommand);
        }

        private void OnActiveCommand()
        {
            settings.AddOrUpdateValue(InitalizeDatabaseService.IsOnlineServicesActive, true);
            _restService.ImportNewConfigurations();            
            this.Close();
        }

        private void OnDisableCommand()
        {
            settings.AddOrUpdateValue(InitalizeDatabaseService.IsOnlineServicesActive, false);
            this.Close();
        }

        public override void Start()
        {
            base.Start();

            if(!settings.GetValueOrDefault(LiabilityViewModel.IsLiabilityPopupShown, false))
            {

            }
        }
    }
}
