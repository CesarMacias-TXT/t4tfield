﻿using MvvmCross.Commands;
using Osram.T4TField.Contracts;
using Osram.T4TField.Resources;
using Osram.TFTBackend;
using Osram.TFTBackend.InAppPurchaseService.Contracts;
using Osram.TFTBackend.InAppPurchaseService.Data;
using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;

namespace Osram.T4TField.ViewModels
{
    public class ManageSubscriptionViewModel : ConnectionBaseViewModel
    {

        private readonly IInAppPurchaseService _purchaseService;
        private readonly ILocalizationInfo _localizationInfo;
        private readonly IInfoUtils _infoUtils;

        public ManageSubscriptionViewModel(IInAppPurchaseService purchaseService, ILocalizationInfo localizationInfo, IInfoUtils infoUtils)
        {
            Title = AppResources.F_ManageCredits;

            _purchaseService = purchaseService;
            _localizationInfo = localizationInfo;
            _infoUtils = infoUtils;

            RefreshCommand = new MvxAsyncCommand(OnRefreshCommand);
            OpenTermsAndConditions = new Command(OnOpenTermsAndConditions);

            Packages = new ObservableCollection<PackageDetailViewModel>();
        }

        private void OnOpenTermsAndConditions()
        {
            ShowViewModel<PrivacyAgreementViewModel>();
        }

        private async Task OnRefreshCommand()
        {
            await RefreshProducts();
        }

        private async Task RefreshProducts()
        {
            try
            {
                await _purchaseService.RefreshProductsAsync();
                Packages.Clear();
                var availableInApps = _purchaseService.GetAvailableInApps().Select(p => new PackageDetailViewModel(p, OnPurchaseCommandAsync));
                foreach (var sub in availableInApps)
                {
                    Packages.Add(sub);
                }
                var availableSubscriptions = _purchaseService.GetAvailableSubscriptions().Select(p => new PackageDetailViewModel(p, OnPurchaseCommandAsync));
                foreach (var sub in availableSubscriptions)
                {
                    Packages.Add(sub);
                }
            }
            catch (Exception ex)
            {
                Logger.Exception("ManageSubscriptionViewModel.RefreshProducts", ex);
                Packages.Clear();
            }
        }

        private async Task RestorePurchases()
        {
            try
            {
                await _purchaseService.RestorePurchasesAsync();
            }
            catch (Exception ex)
            {
                Logger.Exception("ManageSubscriptionViewModel.RestorePurchases", ex);
            }
        }

        private void UpdatePuchaseView()
        {
            if (_purchaseService.GetActivePurchasedSubscriptions().Count() > 0)
            {
                SubscriptionStatus = AppResources.F_ManageSubscriptionActive;
                SubscriptionStatusColor = Styles.OsramColor;

                var firstSubscription = _purchaseService.GetActivePurchasedSubscriptions().First();

                var numberType = AppResources.F_Days;
                switch (firstSubscription.SubscriptionUnit)
                {
                    case InAppSubscriptionUnitEnum.DAYS:
                        numberType = AppResources.F_Days;
                        break;
                    case InAppSubscriptionUnitEnum.WEEKS:
                        numberType = AppResources.F_Weeks;
                        break;
                    case InAppSubscriptionUnitEnum.MONTHS:
                        numberType = AppResources.F_Months;
                        break;
                    case InAppSubscriptionUnitEnum.YEARS:
                        numberType = AppResources.F_Years;
                        break;
                }

                var expirationDate = firstSubscription.GetEstimatedExpirationDate().ToString("D", _localizationInfo.GetCurrentCultureInfo());

                var description = string.Format(AppResources.F_ManageSubscriptionSubscriptionInfo1, firstSubscription.SubscriptionQuantity, numberType.ToLower());
                description += Environment.NewLine;
                description += string.Format(AppResources.F_ManageSubscriptionSubscriptionInfo2, expirationDate);

                SubscriptionDescription = description;
            }
            else if (_purchaseService.GetActivePurchasedInApps().Count() > 0)
            {
                SubscriptionStatus = AppResources.F_ManageSubscriptionActive;
                SubscriptionStatusColor = Styles.OsramColor;

                int availableCredits = 0;
                foreach (var inApp in _purchaseService.GetActivePurchasedInApps())
                {
                    if (inApp.AvailableCredits > 0)
                    {
                        availableCredits += inApp.AvailableCredits;
                    }
                }

                SubscriptionDescription = string.Format(AppResources.F_ManageSubscriptionInAppInfo, availableCredits);
            }
            else
            {
                SubscriptionStatus = AppResources.F_ManageSubscriptionInactive;
                SubscriptionStatusColor = Styles.DarkGray;
                SubscriptionDescription = AppResources.F_ManageSubscriptionDescription;
            }
        }

        private async Task OnPurchaseCommandAsync(ListPurchaseViewModel listPurchaseViewModel)
        {
            PackageDetailViewModel packageDetailViewModel = (PackageDetailViewModel)listPurchaseViewModel;

            var billingProduct = packageDetailViewModel.Product;

            await _purchaseService.MakePurchaseAsync(billingProduct.InAppBillingProduct.ProductId, billingProduct.ItemType);

            UpdatePuchaseView();
        }

        public ObservableCollection<PackageDetailViewModel> Packages { get; private set; }

        public IMvxCommand RefreshCommand { get; set; }
        public ICommand OpenTermsAndConditions { get; set; }

        public new Parameter ClassParameter => (Parameter)base.ClassParameter;

        public string SubscriptionManageText => Device.RuntimePlatform == Device.Android
            ? string.Format(AppResources.F_ManageSubscriptionRenew_Google, _infoUtils.GetStoreName())
            : string.Format(AppResources.F_ManageSubscriptionRenew_Apple, _infoUtils.GetStoreName());

        public string SubscriptionRenewText => string.Format(AppResources.F_ManageSubscriptionManage, _infoUtils.GetStoreName());

        private string subscriptionDescription;
        public string SubscriptionDescription {
            get { return subscriptionDescription; }
            set {
                subscriptionDescription = value;
                RaisePropertyChanged();
            }
        }

        private string subscriptionStatus;
        public string SubscriptionStatus {
            get { return subscriptionStatus; }
            set {
                subscriptionStatus = value;
                RaisePropertyChanged();
            }
        }

        private Color subscriptionStatusColor;
        public Color SubscriptionStatusColor {
            get { return subscriptionStatusColor; }
            set {
                subscriptionStatusColor = value;
                RaisePropertyChanged();
            }
        }

        public override async void Start()
        {
            base.Start();

            await RestorePurchases();

            UpdatePuchaseView();

            await RefreshProducts();
        }
    }

    public class Parameter : BaseViewModelParameter
    {
        public Parameter(bool isModal) : base(isModal)
        {
        }

        public Action OnClose { get; set; }
    }
}