﻿using MvvmCross;
using Osram.T4TField.Contracts;
using Osram.T4TField.Resources;
using Osram.TFTBackend.InAppPurchaseService.Data;
using Plugin.InAppBilling.Abstractions;
using System;
using System.Text;
using System.Threading.Tasks;

namespace Osram.T4TField.ViewModels
{
    public class PackageDetailViewModel : ListPurchaseViewModel
    {
        public PackageDetailViewModel(BillingProduct product, Func<ListPurchaseViewModel, Task> clickPurchaseAction) : base(clickPurchaseAction)
        {
            Product = product;

            Name = Mvx.IoCProvider.Resolve<IInfoUtils>().GetInAppBillingProductName(product.InAppBillingProduct.Name);
            Description = product.InAppBillingProduct.Description;
            Price = product.InAppBillingProduct.LocalizedPrice;
            Currency = product.InAppBillingProduct.CurrencyCode;

            PurchaseText = GetPurchaseText(product);
        }

        public BillingProduct Product { get; private set; }

        private string GetPurchaseText(BillingProduct product)
        {
            StringBuilder text = new StringBuilder();
            text.Append(product.InAppBillingProduct.LocalizedPrice);

            text.Append(Environment.NewLine);

            if (product.ItemType == ItemType.InAppPurchase)
            {
                text.Append(AppResources.F_ManageSubscriptionBuyNow.ToLower());
            }
            else if (product.ItemType == ItemType.Subscription)
            {
                text.Append(AppResources.F_ManageSubscriptionSubscribeNow.ToLower());
            }

            return text.ToString();
        }
    }
}