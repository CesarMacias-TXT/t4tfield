﻿using MvvmCross.Commands;
using Plugin.Settings.Abstractions;
using System.Windows.Input;
using Osram.T4TField.Resources;

namespace Osram.T4TField.ViewModels
{
    public class LoginViewModel : BaseViewModel
    {
        public ICommand LoginClick { get; set; }

        public string UsernameLabel { get; set; }
        public string PasswordLabel { get; set; }
        public string LoginLabel { get; set; }

        private string username;
        public string Username
        {
            set { SetProperty(ref username, value); }
        }

        private string password;
        private ISettings _settings;

        public string Password
        {
            set
            {
                SetProperty(ref password, value);
            }
        }

        public LoginViewModel(ISettings settings)
        {
            InitialiseGui();
            LoginClick = new MvxCommand(OnLoginClick);
            _settings = settings;
        }

        //TODO string resources 
        private void InitialiseGui()
        {
            UsernameLabel = AppResources.F_Username;
            PasswordLabel = AppResources.F_Password;
            LoginLabel = AppResources.F_Login;
        }

        private void OnLoginClick()
        {
            bool succed = CheckCredentials();
            if (succed)
            {
                _settings.AddOrUpdateValue("logged", true);
                Cancel();
            }
        }

        //TODO check credential with server
        private bool CheckCredentials()
        {
            return true;
        }
    }
}
