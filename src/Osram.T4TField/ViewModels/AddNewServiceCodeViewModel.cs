﻿using Acr.UserDialogs;
using MvvmCross.Commands;
using Rg.Plugins.Popup.Services;
using System;
using System.Threading.Tasks;
using Osram.TFTBackend.PasswordService.Contracts;

namespace Osram.T4TField.ViewModels
{
    public class AddNewServiceCodeViewModel : BaseViewModel
    {

        private IUserDialogs _dialog;
        IPasswordService _passwordService;
        private int passwordByte1;
        private int passwordByte2;
        private int passwordByte3;
        private int passwordByte4;
        private bool isSaveButtonEnable = false;
        private Action<PasswordEntry> _programmingAction;
        private string label;
        private PasswordEntry _selectedItem;

        public AddNewServiceCodeViewModel(IUserDialogs dialog, IPasswordService passwordService)
        {
           _dialog = dialog;
         
           _passwordService = passwordService;
         
           CloseCommand = new MvxCommand(OnCloseButtonCommand);
           AddNewServiceCommand = new MvxCommand(OnAddNewServiceCommand);
           IsSaveButtonEnable = false;
        }

        private void OnAddNewServiceCommand()
        {
            var passwordEntry = _selectedItem;
            if (passwordEntry == null)
            {
                passwordEntry = new PasswordEntry()
                {
                    Date = DateTime.Now
                };
            }
            passwordEntry.Label = Label;
            passwordEntry.PasswordByte1 = PasswordByte1;
            passwordEntry.PasswordByte2 = PasswordByte2;
            passwordEntry.PasswordByte3 = PasswordByte3;
            passwordEntry.PasswordByte4 = PasswordByte4;

            _programmingAction?.Invoke(passwordEntry);
            OnCloseButtonCommand();
        }




        /// <summary>
        /// The 2. password byte in range from 0 to 255. 
        /// </summary>

        public string Label
        {
            get { return label; }
            set
            {
                label = value;
                EnableDisableSaveBtn();
                RaisePropertyChanged();
            }
        }

        /// <summary>
        /// The 2. password byte in range from 0 to 255. 
        /// </summary>

        public int PasswordByte1
        {
            get { return passwordByte1; }
            set
            {
                passwordByte1 = value;
                RaisePropertyChanged();
            }
        }

        /// <summary>
        /// The 2. password byte in range from 0 to 255. 
        /// </summary>
        public int PasswordByte2
        {
            get { return passwordByte2; }
            set
            {
                passwordByte2 = value;
                RaisePropertyChanged();
            }
        }

        /// <summary>
        /// The 3. password byte in range from 0 to 255. 
        /// </summary>
        public int PasswordByte3
        {
            get { return passwordByte3; }
            set
            {
                passwordByte3 = value;
                RaisePropertyChanged();
            }
        }

        /// <summary>
        /// The 4. password byte in range from 0 to 255. 
        /// </summary>
        public int PasswordByte4
        {
            get { return passwordByte4; }
            set
            {
                passwordByte4 = value;
                RaisePropertyChanged();
            }
        }


        public bool IsSaveButtonEnable
        {
            get { return isSaveButtonEnable; }
            set
            {
                isSaveButtonEnable = value;
                RaisePropertyChanged();
            }
        }



        public override Task Cleanup()
        {
           return base.Cleanup();
        }


        private void OnCloseButtonCommand()
        {
            Close();
            PopupNavigation.PopAsync();
        }

         public override void Close()
         {
            Cleanup();
         }

        public override void Start()
        {
          base.Start();
          Title = (ClassParameter as AddNewServiceCodeParameter)?.Title;
          _programmingAction = (ClassParameter as AddNewServiceCodeParameter)?.ProgrammingAction;
          _selectedItem = (ClassParameter as AddNewServiceCodeParameter)?.SelectedItem;
          RestoreServiceCodeIfIsUpdate();
        }


        private void RestoreServiceCodeIfIsUpdate()
        {
            if (_selectedItem != null) {
                this.Label= _selectedItem.Label;
                this.PasswordByte1 = _selectedItem.PasswordByte1;
                this.PasswordByte2 = _selectedItem.PasswordByte2;
                this.PasswordByte3 = _selectedItem.PasswordByte3;
                this.PasswordByte4 = _selectedItem.PasswordByte4;
                IsSaveButtonEnable = true;
            }
         
        }

        private void EnableDisableSaveBtn()
        {

            if (label == null){
                IsSaveButtonEnable = false;
                return;
            }
                
            if  (!label.Trim().Equals(string.Empty)) {
                IsSaveButtonEnable = true;
                return;
            }

            IsSaveButtonEnable = false;
        }


        public MvxCommand CloseCommand { get; private set; }

        public MvxCommand AddNewServiceCommand { get; private set; }
       
        #region Parameter for this ViewModel
        public class AddNewServiceCodeParameter : BaseViewModelParameter
        {
            public AddNewServiceCodeParameter(bool isModal)
                : base(isModal)
            {
            }

            public String Title { get; set; }
            public Action<PasswordEntry> ProgrammingAction { get; set; }
            public PasswordEntry SelectedItem { get; set; }

        }
        #endregion

    }
}
