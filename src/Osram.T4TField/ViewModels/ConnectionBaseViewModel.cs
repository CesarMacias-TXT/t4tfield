﻿using MvvmCross;
using Osram.T4TField.Contracts;
using Osram.T4TField.Resources;
using Osram.TFTBackend.BluetoothService;
using Osram.TFTBackend.BluetoothService.Contracts;
using Osram.TFTBackend.NfcService;
using Osram.TFTBackend.NfcService.Contracts;
using Osram.TFTBackend.NfcService.NfcDataTypes;
using Plugin.BLE.Abstractions.Contracts;
using Plugin.Settings.Abstractions;
using System;
using System.Threading.Tasks;

namespace Osram.T4TField.ViewModels
{
    /// <summary>
    ///     Should be used by all view models/views that are using a connected device or need tho show the connection
    ///     indicator.
    /// </summary>
    public abstract class ConnectionBaseViewModel : BaseViewModel
    {
        protected IExtNfcReader NfcReader;
        protected IBluetoothReader BluetoothReader;
        private CurrentDeviceState currentDeviceState;

        private CurrentAdapterState currentNfcState;
        private CurrentAdapterState currentBluetoothState;
        private bool workingProgress;
        private BTDeviceState currentBTDeviceState;
        public ISettings settings;

        private readonly IInfoUtils _infoUtils;

        #region C'tor

        protected ConnectionBaseViewModel()
        {
            CurrentDeviceState = CurrentDeviceState.Offline;
            CurrentNfcState = CurrentAdapterState.Disabled;
            currentBluetoothState = CurrentAdapterState.Disabled;

            this.settings = Mvx.IoCProvider.Resolve<ISettings>();
            this._infoUtils = Mvx.IoCProvider.Resolve<IInfoUtils>();

            CheckNfcReader();
            CheckBluetooth();
            CheckBTDevice();
            UpdateStatus();
            PrepareNFCNotificationArea();
        }




        #endregion

        #region Properties

        public CurrentDeviceState CurrentDeviceState {
            get {
                return currentDeviceState;
            }
            set {
                currentDeviceState = value;
                UpdateStatus();
            }
        }

        public bool WorkingProgress {
            get {
                return workingProgress;
            }
            set {
                workingProgress = value;
                UpdateStatus();
            }
        }

        public CurrentAdapterState CurrentNfcState {
            get {
                return currentNfcState;
            }
            set {
                currentNfcState = value;
                NfcImageSource = currentNfcState == CurrentAdapterState.Enabled ? "nfcIcon" : "nfcIconUn";
                NfcStatus = currentNfcState == CurrentAdapterState.Enabled ? AppResources.F_NfcEnable : AppResources.F_NfcDisabled;
                UpdateStatus();
            }
        }

        public BTDeviceState CurrentBTDeviceState {
            get {
                return currentBTDeviceState;
            }
            set {
                currentBTDeviceState = value;

                BTDeviceImageSource = currentBTDeviceState == BTDeviceState.Connected ? "tertiumConn" : "tertiumUnconnected.png";
                BTDeviceDeviceStatus = currentBTDeviceState == BTDeviceState.Connected ? BluetoothReader.ConnectedDevice.Name : AppResources.F_Disconnected;
                UpdateStatus();
            }
        }

        public CurrentAdapterState CurrentBluetoothState {
            get {
                return currentBluetoothState;
            }
            set {
                currentBluetoothState = value;
                BluetoothImageSource = currentBluetoothState == CurrentAdapterState.Enabled ? "btIcon" : "btIconUn";
                BluetoothStatus = currentBluetoothState == CurrentAdapterState.Enabled ? AppResources.F_BluetoothEnabled : AppResources.F_BluetoothDisabled;
                UpdateStatus();
            }
        }

        //Nfc
        public string NfcImageSource { get; set; }
        public string NfcStatus { get; set; }

        //bluetooth
        public string BluetoothImageSource { get; set; }
        public string BluetoothStatus { get; set; }

        // Tertium device 
        public string BTDeviceImageSource { get; set; }
        public string BTDeviceDeviceStatus { get; set; }

        public bool IsNFcBtNotificationVisible { get; set; }
        public bool IsNFcMobileNotificationVisible { get; set; }

        #endregion

        #region LifecycleEvents

        public override void Start()
        {
            base.Start();
        }


        public override void Loaded()
        {
            base.Loaded();
        }

        public override void Resumed()
        {
            base.Resumed();
            //check here because state could be changed when app is in background
            RefreshNotificationArea();

        }


        public void RefreshNotificationArea()
        {
            PrepareNFCNotificationArea();
            CheckNfcReader();
            CheckBluetooth();
            CheckBTDevice();
            UpdateStatus();
        }


        public override void Close()
        {
            base.Close();
            Cleanup();
        }

        public void UpdateStatus()
        {
            RaiseAllPropertiesChanged();
        }


        public override Task Cleanup()
        {
            //UnRegister here for the events from the services
            UnsetAdapterEvents();
            return base.Cleanup();
        }

        private void UnsetAdapterEvents()
        {
            if (NfcReader != null)
            {
                NfcReader.AdapterConnected -= OnAdapterConnected;
                NfcReader.AdapterDisconnected -= OnAdapterDisconnected;
            }
        }


        #endregion

        private void OnAdapterDisconnected(object sender, EventArgs e)
        {
            NfcReader reader = sender as NfcReader;
            if (reader != null)
            {
                switch (reader.ReaderType)
                {
                    case NfcReaderType.Bluetooth:
                        CurrentBluetoothState = CurrentAdapterState.Disabled;
                        break;
                    case NfcReaderType.Mobile:
                        CurrentNfcState = CurrentAdapterState.Disabled;
                        break;
                }

            }
        }

        private void OnAdapterConnected(object sender, EventArgs e)
        {
            NfcReader reader = sender as NfcReader;
            if (reader != null)
            {
                switch (reader.ReaderType)
                {
                    case NfcReaderType.Bluetooth:
                        CurrentBluetoothState = CurrentAdapterState.Enabled;
                        break;
                    case NfcReaderType.Mobile:
                        CurrentNfcState = CurrentAdapterState.Enabled;
                        break;
                }

            }
        }




        protected void SetAdapterEvents()
        {
            if (NfcReader != null)
            {
                NfcReader.AdapterConnected -= OnAdapterConnected;
                NfcReader.AdapterConnected += OnAdapterConnected;
                NfcReader.AdapterDisconnected -= OnAdapterDisconnected;
                NfcReader.AdapterDisconnected += OnAdapterDisconnected;
                if (GetCurrentNFCInterfaceType().Equals(NfcReaderType.Bluetooth) && BluetoothReader != null)
                {


                    BluetoothReader.DeviceDisconnected -= DeviceDisconnected;
                    BluetoothReader.DeviceDisconnected += DeviceDisconnected;

                    BluetoothReader.DeviceLost -= DeviceDisconnected;
                    BluetoothReader.DeviceLost += DeviceDisconnected;

                    BluetoothReader.DeviceConnected -= DeviceConnected;
                    BluetoothReader.DeviceConnected += DeviceConnected;
                }



            }
        }

        private void DeviceConnected(object sender, IDevice e)
        {
            BluetoothNfcReader reader = sender as BluetoothNfcReader;
            if (reader != null)
            {
                CurrentBTDeviceState = BTDeviceState.Connected;
            }
        }

        private void DeviceDisconnected(object sender, IDevice e)
        {
            BluetoothNfcReader reader = sender as BluetoothNfcReader;
            if (reader != null)
            {
                CurrentBTDeviceState = BTDeviceState.Disconnected;
            }
        }

        private void CheckNfcReader()
        {
            if (NfcReader == null)
            {
                if (Mvx.IoCProvider.TryResolve<IExtNfcReader>(out NfcReader))
                {
                    //Check reader state when app is in foreground 
                    SetAdapterEvents();
                }
            }
            else
            {
                //Check reader state changes when app is in background
                IsNfcEnabled();
            }
        }

        public NfcReaderType GetCurrentNFCInterfaceType()
        {
            return (NfcReaderType)(settings.GetValueOrDefault("NFCSelectedInterface", (decimal)_infoUtils.GetDefaultNfcReaderType()));
        }

        public void PrepareNFCNotificationArea()
        {
            if (NfcReader.IsAvailable && GetCurrentNFCInterfaceType().Equals(NfcReaderType.Mobile))
            {
                IsNFcBtNotificationVisible = false;
                IsNFcMobileNotificationVisible = true;
            }
            else
            {
                IsNFcBtNotificationVisible = true;
                IsNFcMobileNotificationVisible = false;
            }
        }



        private void CheckBluetooth()
        {
            if (BluetoothReader == null)
            {
                if (Mvx.IoCProvider.TryResolve<IBluetoothReader>(out BluetoothReader))
                {
                    //Check reader state when app is in foreground 
                    SetAdapterEvents();
                }
            }
            else
            {
                //Check reader state changes when app is in background
                IsBluetoothEnabled();
            }
        }


        private void CheckBTDevice()
        {
            if (BluetoothReader != null)
            {
                if (BluetoothReader.IsDeviceConnected)
                    CurrentBTDeviceState = BTDeviceState.Connected;
                else
                    CurrentBTDeviceState = BTDeviceState.Disconnected;
            }
        }


        private void IsNfcEnabled()
        {
            if (NfcReader != null && NfcReader.IsEnabled)
            {
                CurrentNfcState = CurrentAdapterState.Enabled;
            }
            else
            {
                CurrentNfcState = CurrentAdapterState.Disabled; ;
            }
        }

        private void IsBluetoothEnabled()
        {
            if (BluetoothReader != null && BluetoothReader.IsEnabled)
            {
                CurrentBluetoothState = CurrentAdapterState.Enabled;
            }
            else
            {
                CurrentBluetoothState = CurrentAdapterState.Disabled;
            }
        }

        protected bool CheckIfAdapterIsReady()
        {
            var reader = Mvx.IoCProvider.Resolve<INfcCommunication>();
            if (reader.ReaderType == TFTBackend.NfcService.NfcDataTypes.NfcReaderType.Bluetooth)
                return ((IBluetoothReader)reader).IsEnabled && ((IBluetoothReader)reader).IsDeviceConnected;
            else
                return reader.IsEnabled;
        }

        protected string GetAdapterDisabledMessage()
        {
            var reader = Mvx.IoCProvider.Resolve<INfcCommunication>();
            if (reader.ReaderType == TFTBackend.NfcService.NfcDataTypes.NfcReaderType.Bluetooth)
            {
                if (!((IBluetoothReader)reader).IsEnabled)
                    return AppResources.AndroidBluetoothDisabled;
                return AppResources.F_BluetoothDeviceNotConnected;
            }

            else
                return AppResources.F_NfcDisabledUserMessage;
        }
    }

    //TODO check if current device is useful yet..
    public enum CurrentDeviceState
    {
        Offline,
        Online
    }

    public enum BTDeviceState
    {
        Connected,
        Disconnected,
    }


    public enum CurrentAdapterState
    {
        Enabled,
        Disabled
    }

}
