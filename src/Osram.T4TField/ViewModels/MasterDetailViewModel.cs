﻿using Acr.UserDialogs;
using MvvmCross;
using MvvmCross.Commands;
using MvvmCross.Plugin.Email;
using MvvmCross.Plugin.Messenger;
using Osram.T4TField.Contracts;
using Osram.T4TField.Resources;
using Osram.TFTBackend.BluetoothService;
using Osram.TFTBackend.BluetoothService.BluetoothDataTypes;
using Osram.TFTBackend.BluetoothService.Contracts;
using Osram.TFTBackend.Messaging;
using Osram.TFTBackend.NfcService.Contracts;
using Osram.TFTBackend.NfcService.NfcDataTypes;
using Plugin.BLE.Abstractions.Contracts;
using Plugin.DeviceInfo;
using System;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;

namespace Osram.T4TField.ViewModels
{
    public class MasterDetailViewModel : ConnectionBaseViewModel
    {
        private bool _isLiabilityPopupShown = false;
        private bool _isOnlineServicesPopupShown = false;

        private BluetoothNfcReader blReader;
        private MvxSubscriptionToken subscrToken;

        private IMvxComposeEmailTask _mailService;
        private IInfoUtils _infoUtils;
        private IUserDialogs _userDialogs;

        //keep all these references here even if not apparently used this ensures they are created
        public MasterDetailViewModel(IMvxComposeEmailTask mailService, IUserDialogs userDialogs, IInfoUtils infoUtils)
        {
            //_wifiInfo = Mvx.Resolve<IConnectivity>();
            _mailService = mailService;
            _userDialogs = userDialogs;
            _infoUtils = infoUtils; // Mvx.Resolve<IInfoUtils>();

            OpenSettingsCommand = new Command(() => ShowViewModel<SettingsViewModel>());
            OpenDebugCommand = new Command(() => ShowViewModel<DebugViewModel>());

            blReader = new BluetoothNfcReader(Mvx.IoCProvider.Resolve<IBluetoothLE>(), new BluetoothResponseQueue());
            Mvx.IoCProvider.RegisterSingleton<IBluetoothReader>(blReader);
            subscrToken = _messenger.Subscribe<Events.OnNfcReaderChangedEvent>(RegisterBluetoothReader);
            OpenAboutCommand = new Command(() => ShowViewModel<AboutViewModel>());
            OpenNfcReaderCommand = new Command(() => ShowViewModel<NfcReaderViewModel>());
            //OpenReportCommand = new Command(() => ShowViewModel<ReportViewModel>());
            ManageCreditsCommand = new Command(() => ShowViewModel<ManageSubscriptionViewModel>());
            OpenSendFeedbackCommand = new MvxAsyncCommand(SendFeedback);
            OpenShareAppCommand = new MvxAsyncCommand(ShareApp);
            OpenChangeLanguageCommand = new Command(() => ShowViewModel<LanguageViewModel>());
            OpenOnlineServicesCommand = new Command(() => ShowViewModel<OnlineServicesManagerViewModel>());
        }

        private async Task ShareApp()
        {
            try
            {
                StringBuilder body = new StringBuilder();
                body.Append("Android:");
                body.Append("\n");
                body.Append("https://play.google.com/store/apps/details?id=com.osram.t4tfield ");
                body.Append("\n");
                body.Append("\n");
                body.Append("iPhone:");
                body.Append("\n");
                body.Append("http://itunes.apple.com/app/id1452998854");
                body.Append("\n");
                body.Append("\n");

                _mailService.ComposeEmail(null, subject: AppResources.F_ShareApp_Subject, body: body.ToString(), isHtml: false, dialogTitle: AppResources.F_ShareApp_DialogTitle);
            }
            catch (Exception)
            {
                await _userDialogs.AlertAsync(AppResources.F_EmailConfigurationNeeded);
            }
        }

        private async Task SendFeedback()
        {
            try
            {
                StringBuilder body = new StringBuilder();
                body.Append(AppResources.F_SendFeedback_Describe);
                body.Append("\n");
                body.Append("\n");
                body.Append("\n");
                body.Append("---------------");
                body.Append("\n");
                body.Append(AppResources.F_SendFeedback_Reminder);
                body.Append("\n");
                body.Append("\n");
                body.Append("\n");
                body.Append(AppResources.F_SendFeedback_AppVersion);
                body.Append(": ");
                body.Append(_infoUtils.GetAppVersion());
                body.Append("\n");
                body.Append(AppResources.F_SendFeedback_Idiom);
                body.Append(": ");
                body.Append(CrossDeviceInfo.Current.Idiom);
                body.Append("\n");
                body.Append(AppResources.F_SendFeedback_Platform);
                body.Append(": ");
                body.Append(CrossDeviceInfo.Current.Platform);
                body.Append("\n");
                body.Append(AppResources.F_SendFeedback_Model);
                body.Append(": ");
                body.Append(CrossDeviceInfo.Current.Model);
                body.Append("\n");
                body.Append(AppResources.F_SendFeedback_OsVersion);
                body.Append(": ");
                body.Append(CrossDeviceInfo.Current.Version);
                body.Append("\n");
                body.Append("\n");

                _mailService.ComposeEmail(AppResources.F_SendFeedback_To, subject: AppResources.F_SendFeedback_Subject, body: body.ToString(), isHtml: false, dialogTitle: AppResources.F_SendFeedback_DialogTitle);
            }
            catch (Exception)
            {
                await _userDialogs.AlertAsync(AppResources.F_EmailConfigurationNeeded);
            }
        }

        private void SetCurrentNFCReader()
        {
            var CurrentNfcReaderType = GetCurrentNFCInterfaceType();
            _messenger.Publish(new TFTBackend.Messaging.Events.OnNfcReaderChangedEvent(this, CurrentNfcReaderType));
            RestoreLastConnectedDevice(CurrentNfcReaderType);
        }

        private async void RestoreLastConnectedDevice(NfcReaderType CurrentNfcReaderType)
        {
            Guid guid = (Guid)settings.GetValueOrDefault("LastConnectedBTDevice", new Guid("00000000-0000-0000-0000-000000000000"));

            if (CurrentNfcReaderType.Equals(NfcReaderType.Bluetooth) && (guid.CompareTo(new Guid("00000000-0000-0000-0000-000000000000")) == 1))
            {
                await blReader.ConnectToKnownDeviceAsync(guid);
            }
        }

        private void RegisterBluetoothReader(Events.OnNfcReaderChangedEvent ev)
        {
            var InterfaceType = ev.ReaderTypeSelected;

            if (InterfaceType == NfcReaderType.Bluetooth)
            {
                Mvx.IoCProvider.RegisterSingleton<INfcCommunication>(blReader);
                _messenger.Publish(new Events.OnNfcReaderUpdateEvent(this));
            }
            SetCurrentNFCInterface(InterfaceType);
        }

        public string UserName { get; private set; }

        public ICommand OpenSettingsCommand { get; }
        public ICommand OpenDebugCommand { get; }

        public string ManageCreditsTxt => AppResources.F_ManageCredits;
        public string UserInfoTxt => AppResources.UserInfo;
        public string SettingsTxt => AppResources.Settings;
        public string DebugViewTxt => AppResources.F_Debug;
        public string SendFeedbackTxt => AppResources.F_SendFeedback;
        public string ShareAppTxt => AppResources.F_ShareApp;
        public string AdvancedModeText => AppResources.F_AdvancedModeSettingsLable;
        public string OnlineServicesText => AppResources.F_OnlineServices;
        public bool IsUserVisible => !string.IsNullOrEmpty(UserName);

        private bool isAdvancedModeToggled;
        public bool IsAdvancedModeToggled {
            get { return isAdvancedModeToggled; }
            set {
                isAdvancedModeToggled = value;
                if (isAdvancedModeToggled)
                {
                    ShowAsRootViewModel<AdvancedHomeViewModel>();
                }
                else
                {
                    ShowAsRootViewModel<BasicHomeViewModel>();
                }

                RaisePropertyChanged(nameof(IsAdvancedModeToggled));
            }
        }

        public string Version {
            get {
                var debug = string.Empty;
#if DEBUG
                debug = " debug";
#endif
                var appInfo = GetService<IAppInfo>();
                return $"{appInfo.ApplicationName} v{appInfo.Version}{debug}";
            }
        }
        public override void Loaded()
        {
            base.Loaded();
            //Mvx.RegisterSingleton<MasterDetailViewModel>(this);
            ShowAsRootViewModel<BasicHomeViewModel>();
        }

        public override void Resumed()
        {
            base.Resumed();

            //var isUserRegistered = _settings.GetValueOrDefault(SettingsKeys.IsUserRegistered, false);
            //var isAgreementAccepted = _settings.GetValueOrDefault(SettingsKeys.AgreementsAccepted, false);

            //if (!isAgreementAccepted)
            //{
            //    ShowAgreementPage();
            //}
            //else if (Osram.IsAdminApp && !isUserRegistered)
            //{
            //    ShowUserSettings();
            //}
            //ShowModalViewModel<SearchingDeviceViewModel>();

            //   }


            //if user is not logged it shows a login page
            // as T4T-122 the login page has to be hidden
            //CheckIfUserIsLogged();   
            AutoLogin();
            // SetCurrentNFCInterface();

            CheckLiability();

            CheckOnlineServices();
        }

        private void CheckLiability()
        {
            if (!_isLiabilityPopupShown)
            {
                _isLiabilityPopupShown = true;
                ShowModalViewModel<LiabilityViewModel>();
            }
        }

        private void CheckOnlineServices()
        {
            _isOnlineServicesPopupShown = settings.GetValueOrDefault(LiabilityViewModel.IsLiabilityPopupShown, false);

            if (_isOnlineServicesPopupShown && settings.GetValueOrDefault(OnlineServicesViewModel.IsOnlineServicesViewModelPopupShown, true))
            {
                _isOnlineServicesPopupShown = false;
                settings.AddOrUpdateValue(OnlineServicesViewModel.IsOnlineServicesViewModelPopupShown, false);                
                ShowViewModel<OnlineServicesViewModel>();
            }
        }

        public override void Close()
        {
            _messenger.Unsubscribe<Events.OnNfcReaderChangedEvent>(subscrToken);
            base.Close();

        }

        public override void Start()
        {
            SetCurrentNFCReader();
            base.Start();

        }

        public void AutoLogin()
        {
            bool isLoggedIn = settings.GetValueOrDefault("logged", false);
            if (!isLoggedIn)
            {
                settings.AddOrUpdateValue("logged", true);
            }
        }

        private void CheckIfUserIsLogged()
        {

            bool isLoggedIn = settings.GetValueOrDefault("logged", false);
            if (!isLoggedIn)
            {
                ShowLoginPage();
            }
        }

        private void ShowLoginPage()
        {
            ShowModalViewModel<LoginViewModel>();
        }

        //To be move in setting page when will be restored

        public ICommand OpenAboutCommand { get; }
        public ICommand OpenNfcReaderCommand { get; }
        //public ICommand OpenReportCommand { get; }
        public ICommand ManageCreditsCommand { get; }
        public ICommand OpenSendFeedbackCommand { get; }
        public ICommand OpenShareAppCommand { get; }
        public ICommand OpenChangeLanguageCommand { get; }
        public ICommand OpenOnlineServicesCommand { get; }

        public string ChangeLanguageTxt => AppResources.F_ChangeLanguage;
        public string AboutTxt => AppResources.About;
        public string NfcReaderTxt => AppResources.F_ChooseNfcReader;
        //public string ReportTxt => AppResources.F_Report;
        public string ReportTxt => AppResources.F_Reports_title;
        public string OnlineServicesTxt => AppResources.F_OnlineServices;

        private string _nfcInterfaceName;
        private string _nfcInterfaceIcon;

        public string NFCInterfaceName {
            get { return _nfcInterfaceName; }
            set {
                _nfcInterfaceName = value;
                RaisePropertyChanged(nameof(NFCInterfaceName));
            }
        }


        public string NFCInterfaceIcon {
            get { return _nfcInterfaceIcon; }
            set {
                _nfcInterfaceIcon = value;
                RaisePropertyChanged(nameof(NFCInterfaceIcon));
            }
        }

        private void SetCurrentNFCInterface(NfcReaderType nfcReaderType)
        {
            NFCInterfaceName = "NFC Reader";
            switch (nfcReaderType)
            {
                case NfcReaderType.Mobile:
                    NFCInterfaceIcon = "nfcIcon.png";
                    break;
                case NfcReaderType.Bluetooth:
                    NFCInterfaceIcon = "btIcon.png";
                    //setBTInformation();
                    break;
                default:
                    break;
            }
        }

        private void SetBTInformation()
        {
            NFCInterfaceIcon = "btIcon.png";
            string _interfaceName = AppResources.F_SearchDevice;
            if (BluetoothReader.IsDeviceConnected)
            {
                _interfaceName = BluetoothReader.ConnectedDevice.Name;
            }
            NFCInterfaceName = _interfaceName;
        }
    }
}
