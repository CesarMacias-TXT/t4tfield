﻿using MvvmCross.Commands;
using Osram.TFTBackend;
using Osram.TFTBackend.ConfigurationService.Contracts;
using Osram.TFTBackend.ImportService.Contracts;
using Osram.TFTBackend.KeyService.Contracts;

namespace Osram.T4TField.ViewModels
{
    class DebugViewModel : BaseViewModel
    {
        private IConfigurationService _configurationService;
        private IImportService _importService;
        private IKeyService _keyService;

        public DebugViewModel(IImportService importService, IConfigurationService configurationService, IKeyService keyService)
        {
            FirstTestCommand = new MvxCommand(OnFirstTestFunction);
            SecondTestCommand = new MvxCommand(OnSecondTestFunction);
            ThirdTestCommand = new MvxCommand(OnThirdTestFunction);
            FourthTestCommand = new MvxCommand(OnFourthTestFunction);

            _configurationService = configurationService;
            _importService = importService;
            _keyService = keyService;
        }

        private void OnFourthTestFunction()
        {
            //insert here (Bert button)
        }

        /// <summary>
        /// Test and debug button for Janos
        /// </summary>
        private void OnThirdTestFunction()
        {
            if (_keyService != null)
            {
                if(_keyService.IsDevicePasswordProtected)
                    Logger.Trace("Device is master key protected.");

                var decryptedPassword = _keyService.RetrieveMasterPassword(0x01020304);
                var encryptedPassword = _keyService.ValidateMasterPassword(0x01020304);
            }
        }

        private void OnSecondTestFunction()
        {
            //insert here (Raffaele button)
        }

        private void OnFirstTestFunction()
        {
            //insert here (Davide button)
        }

        public MvxCommand FirstTestCommand { get; }
        public MvxCommand SecondTestCommand { get; }
        public MvxCommand ThirdTestCommand { get; }
        public MvxCommand FourthTestCommand { get; }
    }
}
