﻿using Acr.UserDialogs;
using MvvmCross.Commands;
using Osram.T4TField.Controls.ListCellView;
using Osram.T4TField.Resources;
using Osram.T4TField.Utils;
using Osram.TFTBackend.BluetoothService.Contracts;
using Plugin.BLE.Abstractions.Contracts;
using Plugin.Permissions.Abstractions;
using System.Collections.ObjectModel;
using System;
using System.Linq;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace Osram.T4TField.ViewModels
{
    class SearchingBTDeviceViewModel : ConnectionBaseViewModel
    {
        IBluetoothReader _bluetoothNfcReader;
        private ObservableCollection<BTLEDeviceViewModel> _devices;
        private IUserDialogs _userDialogs;

        public MvxCommand ScanDeviceCommand { get; private set; }

        public MvxCommand DisconnectTertiumDeviceCommand { get; private set; }

        public SearchingBTDeviceViewModel(IBluetoothReader bluetoothNfcReader, IUserDialogs userDialogs)
        {

            Devices = new ObservableCollection<BTLEDeviceViewModel>();
            BTListItemTemplate = new DataTemplate(typeof(BTDeviceCell));
            ScanDeviceCommand = new MvxCommand(OnScanDeviceButtonCommand);
            DisconnectTertiumDeviceCommand = new MvxCommand(OnDisconnectTertiumDeviceCommand);
            _bluetoothNfcReader = bluetoothNfcReader;
            _bluetoothNfcReader.DeviceDiscovered += DeviceDiscovered;
            _bluetoothNfcReader.DeviceConnected += DeviceConnected;
            _bluetoothNfcReader.DeviceDisconnected += DeviceDisconnected;
            _bluetoothNfcReader.DeviceLost += DeviceLost;
            _bluetoothNfcReader.DeviceScanTimeoutElapsed += DeviceScanTimeoutElapsed;
            _bluetoothNfcReader.DeviceConnectionTimeoutElapsed += DeviceConnectionTimeoutElapsed;
            IsDeviceListEmpty = true;
            _userDialogs = userDialogs;
            Title = AppResources.F_Navigation_SeachBTDevice;
            TertiumBTStatusIcon = "tertiumConn.png";

        }

        private void DeviceConnectionTimeoutElapsed(object sender, IDevice e)
        {
            _userDialogs.Alert(AppResources.F_BTConnectionTimeOut);
            RemoveBTDeviceFromList(e);
            _selectedDevice = null;
            WorkingProgress = false;
        }

        public ObservableCollection<BTLEDeviceViewModel> Devices {
            get { return _devices; }
            set {
                _devices = value;
                RaisePropertyChanged(nameof(Devices));
            }
        }

        private BTLEDeviceViewModel _selectedDevice;
        public BTLEDeviceViewModel SelectedDevice {
            get { return _selectedDevice; }
            set {
                if (value != null)
                {
                    if (_bluetoothNfcReader.IsEnabled)
                    {
                        _selectedDevice = value;
                        WorkingProgress = true;
                        _bluetoothNfcReader.ConnectDeviceAsync(_selectedDevice.Device);
                    }
                    else
                    {
                        _userDialogs.Alert(new AlertConfig().Message = AppResources.AndroidBluetoothDisabled);
                        SetScanStatusInUserInterface(false);
                        Devices.Clear();
                    }
                }
            }
        }

        private async void OnScanDeviceButtonCommand()
        {
            Devices.Clear();
            SetScanStatusInUserInterface(true);
            if (_bluetoothNfcReader.IsEnabled)
            {
                bool isPermissionGranted = await PermissionUtils.Instance.CheckPermissionStatus(Permission.Location);
                if (isPermissionGranted)
                    await _bluetoothNfcReader.StartScanningForDevicesAsync();
                else
                    await _userDialogs.AlertAsync(AppResources.F_LocationPermissionNeeded);
            }
            else
            {
                await _userDialogs.AlertAsync(new AlertConfig().Message = AppResources.AndroidBluetoothDisabled);
            }
            SetScanStatusInUserInterface(false);
        }

        private void SetScanStatusInUserInterface(bool isScannigMode)
        {
            WorkingProgress = isScannigMode;
            IsScanButtonEnabled = !isScannigMode;
        }

        private async void OnDisconnectTertiumDeviceCommand()
        {
            if (_bluetoothNfcReader.IsDeviceConnected)
            {
                IDevice connectedDevice = _bluetoothNfcReader.ConnectedDevice;
                await _bluetoothNfcReader.DisconnectDeviceAsync(connectedDevice);
            }
        }

        private void DeviceConnected(object sender, IDevice device)
        {
            SetScanStatusInUserInterface(false);
            settings.AddOrUpdateValue("LastConnectedBTDevice", device.Id);
            ShowBluetoothDeviceMessageToUser(AppResources.F_BluetoothReaderConnected);
            SetConnectedDeviceUI(true, device);
        }

        private void DeviceDiscovered(object sender, IDevice device)
        {
            var deviceViewModel = Devices.SingleOrDefault(d => d.Device.Id == device.Id);
            IsDeviceListEmpty = false;
            // is new device
            if (deviceViewModel == null)
            {
                AddOrUpdateDevicePositionOnList(new BTLEDeviceViewModel(device));
                // RaisePropertyChanged(nameof(NoDevicesAvailable));
            }
            else //device already exists in list
            {
                deviceViewModel.Device = device;
            }
        }

        private void DeviceScanTimeoutElapsed(object sender, IDevice e)
        {
            SetScanStatusInUserInterface(false);
        }

        private void DeviceDisconnected(object sender, IDevice device)
        {
            ShowBluetoothDeviceMessageToUser(AppResources.F_BluetoothReaderDisconnected);
            SetConnectedDeviceUI(false, device);
        }

        private void DeviceLost(object sender, IDevice device)
        {
            ShowBluetoothDeviceMessageToUser(AppResources.F_BluetoothReaderDisconnected);
            SetConnectedDeviceUI(false, device);
        }

        private void RemoveBTDeviceFromList(IDevice device)
        {
            if (device != null)
            {
                var deviceViewModel = Devices.SingleOrDefault(d => d.Device.Id == device.Id);
                Devices.Remove(deviceViewModel);
                if (Devices.Count == 0)
                {
                    IsDeviceListEmpty = true;
                }
            }
            RaisePropertyChanged(nameof(Devices));
        }

        private void AddOrUpdateDevicePositionOnList(BTLEDeviceViewModel deviceViewModel)
        {
            // TODO Add logic for reorder list if needed
            Devices.Add(deviceViewModel);
        }

        void ShowBluetoothDeviceMessageToUser(string message)
        {
            var config = new AlertConfig();
            config.Message = message;
            _userDialogs.Alert(config);
        }

        private void RestoreTertiumDeviceStatus()
        {
            if (_bluetoothNfcReader.IsDeviceConnected)
            {
                IDevice device = _bluetoothNfcReader.ConnectedDevice;
                SetConnectedDeviceUI(true, device);
                SetScanStatusInUserInterface(false);
            }
            else
            {
                SetConnectedDeviceUI(false, null);
                OnScanDeviceButtonCommand();
            }

        }

        private void SetConnectedDeviceUI(bool tertiumDeviceIsConnected, IDevice device)
        {
            RemoveBTDeviceFromList(device);
            TertiumIsConnected = tertiumDeviceIsConnected;
            if (TertiumIsConnected && device != null)
            {
                TertiumBTStatusLabel = device.Name;
            }

        }

        public override Task Cleanup()
        {
            _bluetoothNfcReader.DeviceDiscovered -= DeviceDiscovered;
            _bluetoothNfcReader.DeviceConnected -= DeviceConnected;
            _bluetoothNfcReader.DeviceDisconnected -= DeviceDisconnected;
            _bluetoothNfcReader.DeviceLost -= DeviceLost;
            _bluetoothNfcReader.DeviceScanTimeoutElapsed -= DeviceScanTimeoutElapsed;
            _bluetoothNfcReader.DeviceConnectionTimeoutElapsed -= DeviceConnectionTimeoutElapsed;

            return base.Cleanup();
        }

        public override void Resumed()
        {
            base.Resumed();
            RestoreTertiumDeviceStatus();
            // setBTInformation();
        }

        /// <summary>
        /// set tertium device status
        /// </summary>
        private bool tertiumIsConnected;
        public bool TertiumIsConnected {
            get { return tertiumIsConnected; }
            set {
                tertiumIsConnected = value;
                RaisePropertyChanged();
            }
        }

        private string tertiumBTStatusLabel;
        public string TertiumBTStatusLabel {
            get { return tertiumBTStatusLabel; }
            set {
                tertiumBTStatusLabel = value;
                RaisePropertyChanged();
            }
        }

        private string tertiumBTStatusIcon;
        public string TertiumBTStatusIcon {
            get { return tertiumBTStatusIcon; }
            set {
                tertiumBTStatusIcon = value;
                RaisePropertyChanged();
            }
        }

        private bool isDeviceListEmpty;
        public bool IsDeviceListEmpty {
            get { return isDeviceListEmpty; }
            set {
                isDeviceListEmpty = value;
                RaisePropertyChanged();
            }
        }

        private bool isScanButtonEnabled;
        public bool IsScanButtonEnabled {
            get { return (isScanButtonEnabled && _bluetoothNfcReader.IsEnabled); }
            set {
                isScanButtonEnabled = value;
                RaisePropertyChanged();
            }
        }

        private DataTemplate _btListItemTemplate;
        public DataTemplate BTListItemTemplate {
            get { return _btListItemTemplate; }
            set {
                _btListItemTemplate = value;
                RaisePropertyChanged();
            }
        }
    }
}
