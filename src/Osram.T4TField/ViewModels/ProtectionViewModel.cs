﻿
using Acr.UserDialogs;
using MvvmCross.Commands;
using Osram.T4TField.Resources;
using Osram.TFTBackend;
using Osram.TFTBackend.PasswordService.Contracts;
using Plugin.Settings.Abstractions;
using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Input;
using static Osram.T4TField.ViewModels.AddNewServiceCodeViewModel;

namespace Osram.T4TField.ViewModels
{
    class ProtectionViewModel : ConnectionBaseViewModel
    {
        private readonly IUserDialogs _userDialogs;
        private readonly IPasswordService _passwordService;
        private readonly ISettings _settings;

        private Func<PasswordEntry, Task<bool>> _authenticationFunc;

        public ProtectionViewModel(IUserDialogs userDialogs, IPasswordService passwordService, ISettings settings)
        {
            _userDialogs = userDialogs;
            _passwordService = passwordService;
            _settings = settings;
            Title = AppResources.F_Navigation_Protection;
            ProtectionListItemSource = new ObservableCollection<PasswordEntryViewModel>();
            AddServiceKeyCommand = new MvxCommand(AddNewServiceCode);
            ItemClickCommand = new MvxCommand<PasswordEntry>(OnItemClickCommand);
            DeletePasswordCommand = new MvxCommand<PasswordEntry>(OnDeletePasswordCommand);
            UpdatePasswordCommand = new MvxCommand<PasswordEntry>(OnUpdatePasswordCommand);
        }

        private ObservableCollection<PasswordEntryViewModel> _protectionListItemSource;

        public ObservableCollection<PasswordEntryViewModel> ProtectionListItemSource {
            get { return _protectionListItemSource; }
            set {
                _protectionListItemSource = value;
                RaisePropertyChanged();
            }
        }

        public override void Start()
        {
            base.Start();
            _authenticationFunc = (ClassParameter as ProtectionViewModelParameter)?.Authentication;
        }

        public override async void Resumed()
        {
            base.Resumed();
            Logger.Trace("ProtectionViewModel.Resumed(): loading service keys ...");
            await UpdateProtectionList();
        }

        public async Task UpdateProtectionList()
        {
            var ProtectionList = new ObservableCollection<PasswordEntry>(await _passwordService.Query());
            var PasswordEntryViewModelList = new ObservableCollection<PasswordEntryViewModel>();
            int currentPasswordID = _settings.GetValueOrDefault("CurrentServicePasswordID", -999);

            foreach (PasswordEntry item in ProtectionList)
            {
                PasswordEntryViewModel newEntry = new PasswordEntryViewModel(item, SelectPassword, null, OnDeletePasswordCommand, OnUpdatePasswordCommand);
                if (item.Id == currentPasswordID)
                {
                    newEntry.SetSelected(true);
                    newEntry.IsSelected = true;
                }
                PasswordEntryViewModelList.Add(newEntry);
            }
            ProtectionListItemSource = PasswordEntryViewModelList;
        }

        private async void SelectPassword(ListItemViewModel listItemViewModel)
        {
            PasswordEntryViewModel _passwordEntryViewModel = (PasswordEntryViewModel)listItemViewModel;
            if (_passwordEntryViewModel == null) return;

            DeselectAllItem(_passwordEntryViewModel.Name);

            _settings.AddOrUpdateValue("CurrentServicePasswordID", _passwordEntryViewModel.PasswordEntry.Id);

            if (!_passwordEntryViewModel.IsSelected)
            {
                ClearCurrentServicePassword();
                _passwordEntryViewModel.SetSelected(false);
                return;
            }

            if (_authenticationFunc != null)
            {
                if (await _authenticationFunc(_passwordEntryViewModel.PasswordEntry))
                {
                    Close();
                }
                else
                {
                    await _userDialogs.AlertAsync(AppResources.F_ServicePasswordNotValid);
                }
            }
        }

        private void ClearCurrentServicePassword()
        {
            _settings.AddOrUpdateValue("CurrentServicePasswordID", -999);
        }

        private void DeselectAllItem(string name)
        {
            foreach (PasswordEntryViewModel PasswordEntry in ProtectionListItemSource)
            {
                if (!PasswordEntry.Name.Equals(name))
                {
                    PasswordEntry.SetSelected(false);
                    PasswordEntry.SetHasArrow(false);
                }
            }
        }

        public async void OnAddNewServiceCodeAsync(PasswordEntry passwordEntry)
        {
            bool alreadyExist = false;
            var passwordList = await _passwordService.Query();
            foreach (var item in passwordList)
            {
                if (item.Label == passwordEntry.Label)
                {
                    alreadyExist = true;
                }
            }
            if (alreadyExist)
            {
                await _userDialogs.AlertAsync(AppResources.F_ServiceKeyAlreadyUsed, AppResources.F_DialogProgrammingErrorTitle);
            }
            else
            {
                await _passwordService.Insert(passwordEntry);
            }
            await UpdateProtectionList();
        }

        public async void OnUpdateNewServiceCodeAsync(PasswordEntry passwordEntry)
        {
            bool alreadyExist = false;
            var passwordList = await _passwordService.Query();
            foreach (var item in passwordList.Where(p => p.Id != passwordEntry.Id))
            {
                if (item.Label == passwordEntry.Label)
                {
                    alreadyExist = true;
                }
            }
            if (alreadyExist)
            {
                await _userDialogs.AlertAsync(AppResources.F_ServiceKeyAlreadyUsed, AppResources.F_DialogProgrammingErrorTitle);
            }
            else
            {
                await _passwordService.Update(passwordEntry);
            }
            await UpdateProtectionList();
        }

        public ICommand AddServiceKeyCommand { get; set; }

        private void AddNewServiceCode()
        {
            AddNewServiceCodeParameter param = new AddNewServiceCodeParameter(true)
            {
                Title = AppResources.F_addSeviceCodePopLabel,
                ProgrammingAction = OnAddNewServiceCodeAsync,
                SelectedItem = null
            };
            MyShowViewModel<AddNewServiceCodeViewModel>(RequestType.Popup, param);
        }

        public ICommand ItemClickCommand { get; set; }

        private async void OnItemClickCommand(PasswordEntry passwordEntry)
        {
            Logger.Trace($"ProtectionViewModel.OnItemClickCommand(): tapped on key {passwordEntry.Label}");

            _settings.AddOrUpdateValue("CurrentServicePasswordID", passwordEntry.Id);

            if (_authenticationFunc != null)
            {
                if (await _authenticationFunc(passwordEntry))
                {
                    Close();
                }
                else
                {
                    await _userDialogs.AlertAsync(AppResources.AuthenticationFailed);
                }
            }
        }

        public ICommand DeletePasswordCommand { get; set; }

        public ICommand UpdatePasswordCommand { get; set; }

        private async void OnDeletePasswordCommand(PasswordEntry passwordEntry)
        {
            bool isDeleteCofirmed = await _userDialogs.ConfirmAsync(AppResources.F_DeletePasswordConfirmationMessage + ": " + passwordEntry.Label + "?");
            if (isDeleteCofirmed)
            {
                Logger.Trace($"ProtectionViewModel.OnDeletePasswordCommand(): deleteing key {passwordEntry.Label}");
                await _passwordService.Delete(passwordEntry);
                ClearCurrentServicePassword();
                await UpdateProtectionList();
            }
        }

        private void OnUpdatePasswordCommand(PasswordEntry passwordEntry)
        {
            Logger.Trace($"ProtectionViewModel.OnUpdatePasswordCommand(): deleteing key {passwordEntry.Label}");
            AddNewServiceCodeParameter param = new AddNewServiceCodeParameter(true)
            {
                Title = AppResources.F_updateSeviceCodePopLabel,
                ProgrammingAction = OnUpdateNewServiceCodeAsync,
                SelectedItem = passwordEntry
            };
            MyShowViewModel<AddNewServiceCodeViewModel>(RequestType.Popup, param);
        }

        #region Parameter for this ViewModel
        public class ProtectionViewModelParameter : BaseViewModelParameter
        {
            public ProtectionViewModelParameter(bool isModal, Func<PasswordEntry, Task<bool>> authentication)
                : base(isModal)
            {
                Authentication = authentication;
            }
            public Func<PasswordEntry, Task<bool>> Authentication { get; }

        }
        #endregion
    }
}
