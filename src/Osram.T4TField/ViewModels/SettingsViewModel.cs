﻿using Acr.UserDialogs;
using Osram.T4TField.Resources;
using Osram.TFTBackend.NfcService.NfcDataTypes;
using System.Windows.Input;
using Xamarin.Forms;

namespace Osram.T4TField.ViewModels
{
    public class SettingsViewModel : ConnectionBaseViewModel
    {
        private readonly IUserDialogs _userDialogs;

        private string _languageName;
        private string _flag;
        private string _nfcInterfaceName;
        private string _nfcInterfaceIcon;


        public SettingsViewModel(IUserDialogs userDialogs)
        {
            Title = AppResources.Settings;
            _userDialogs = userDialogs;
            OpenLanguageViewCommand = new Command(() => ShowViewModel<LanguageViewModel>());
            OpenAboutCommand = new Command(() => ShowViewModel<AboutViewModel>());
            OpenNfcReaderCommand = new Command(() => ShowViewModel<NfcReaderViewModel>());
            OpenReportCommand = new Command(() => ShowViewModel<ReportViewModel>());
        }

        public ICommand OpenLanguageViewCommand { get; }
        public ICommand OpenAboutCommand { get; }
        public ICommand OpenNfcReaderCommand { get; }
        public ICommand OpenReportCommand { get; }

        public string LanguageTxt => AppResources.Language;
        public string AboutTxt => AppResources.About;
        public string NfcReaderTxt => AppResources.F_ChooseNfcReader;
        public string ReportTxt => AppResources.F_Report;

        public string LanguageName {
            get { return _languageName; }
            set {
                _languageName = value;
                RaisePropertyChanged(nameof(LanguageName));
            }
        }

        public string Flag {
            get { return _flag; }
            set {
                _flag = value;
                RaisePropertyChanged(nameof(Flag));
            }
        }


        public string NFCInterfaceName {
            get { return _nfcInterfaceName; }
            set {
                _nfcInterfaceName = value;
                RaisePropertyChanged(nameof(NFCInterfaceName));
            }
        }


        public string NFCInterfaceIcon {
            get { return _nfcInterfaceIcon; }
            set {
                _nfcInterfaceIcon = value;
                RaisePropertyChanged(nameof(NFCInterfaceIcon));
            }
        }

        public override bool IsBusy {
            get { return base.IsBusy; }
            set {
                base.IsBusy = value;
                if (IsBusy)
                    _userDialogs.ShowLoading();
                else
                    _userDialogs.HideLoading();
            }
        }



        public override void Start()
        {
            base.Start();
            SetCurrentNFCInterface();
        }

        public override void Resumed()
        {
            base.Resumed();
            SetCurrentLaunguage();
            SetCurrentNFCInterface();

        }

        private void SetCurrentNFCInterface()
        {

            switch (GetCurrentNFCInterfaceType())
            {
                case NfcReaderType.Mobile:
                    NFCInterfaceName = "Mobile NFC";
                    NFCInterfaceIcon = "nfcIcon.png";
                    break;
                case NfcReaderType.Bluetooth:
                    SetBTInformation();
                    break;
                default:
                    break;
            }
        }

        private void SetBTInformation()
        {
            NFCInterfaceIcon = "btIcon.png";
            string _interfaceName = AppResources.F_SearchDevice;
            if (BluetoothReader.IsDeviceConnected)
            {
                _interfaceName = BluetoothReader.ConnectedDevice.Name;
            }
            NFCInterfaceName = _interfaceName;
        }

        private void SetCurrentLaunguage()
        {
            LanguageName = LocalizationService.CurrentLanguage.Name.ToUpper();
            Flag = LocalizationService.GetLocalizedFileName("flag.png");
            Title = AppResources.Settings;
        }
    }
}
