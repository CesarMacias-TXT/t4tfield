﻿using Acr.UserDialogs;
using MvvmCross;
using MvvmCross.Commands;
using Osram.T4TField.Resources;
using Osram.TFTBackend;
using Osram.TFTBackend.BaseTypes.Exceptions;
using Osram.TFTBackend.NfcService.Contracts;
using Osram.TFTBackend.NfcService.NfcDataTypes;
using Osram.TFTBackend.ProgrammingService;
using Osram.TFTBackend.ProgrammingService.Contracts;
using Rg.Plugins.Popup.Services;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace Osram.T4TField.ViewModels
{
    public class ProgrammingViewModel : ConnectionBaseViewModel
    {
        private IExtNfcReader _nfcReader;
        private IProgrammingService _programmingService;

        private static string tabSpace = "   ";
        private bool _isBusy = false;

        private Func<BaseViewModel, IProgrammingService, CancellationToken, Task<bool>> _programmingAction;
        private string _textDialogAfterAction;
        private ProgrammingType _programmingType;

        /// <summary>
        /// Progress percentage for the status bar
        /// </summary>
        private double progressPercentage = 0.0;
        /// <summary>
        /// Gets the status bar text
        /// </summary>
        private string statusText = AppResources.SearchingForDevice;
        /// <summary>
        /// Gets the information about Inturrupting NFC writes
        /// </summary>
        private string interruptingNFCText = string.Empty;
        /// <summary>
        /// The progress status
        /// </summary>
        private string progressStatus = tabSpace;
        private IUserDialogs _dialog;

        CancellationTokenSource _cancellationToken;

        public ProgrammingViewModel(IUserDialogs dialog, IExtNfcReader nfcReader, IProgrammingService programmingService)
        {
            _dialog = dialog;
            _nfcReader = nfcReader;
            _programmingService = programmingService;

            CloseCommand = new MvxCommand(OnCloseButtonCommand);
            ListenToProgrammingServiceEvents();
        }

        private void ListenToProgrammingServiceEvents()
        {
            //TODO life cycle management, connected events prevent garbage collection!
            _nfcReader.TagDetected += DeviceDetected;
            _nfcReader.TagUndetected += DeviceUndetected;
            _nfcReader.AdapterDisconnected += ProgrammerDisabled;
            _nfcReader.AdapterConnected += ProgrammeEnabled;

            _programmingService.ProgressChange += ProgressChange;
        }

        public override Task Cleanup()
        {
            //UnRegister here for the events from the services
            _nfcReader.TagDetected -= DeviceDetected;
            _nfcReader.TagUndetected -= DeviceUndetected;
            _nfcReader.AdapterDisconnected -= ProgrammerDisabled;
            _nfcReader.AdapterConnected -= ProgrammeEnabled;
            _programmingService.ProgressChange -= ProgressChange;
            return base.Cleanup();
        }

        private void OnCloseButtonCommand()
        {
            if (_programmingType == ProgrammingType.Write && _isBusy)
            {
                _dialog.Alert(AppResources.F_WriteNotStoppable, AppResources.F_DialogProgrammingSuccessfulTitle);
            }
            else
            {
                try
                {
                    _cancellationToken?.Cancel();
                }
                catch (ObjectDisposedException)
                {
                    Logger.Trace("OnCloseButtonCommand: Token Already disposed");
                }
            }
            ClosePopup();
        }

        private void ClosePopup()
        {
            Close();
            try
            {
                PopupNavigation.PopAsync();
            }
            catch (Exception) { }
        }

        public override void Close()
        {
            Cleanup();
            ResetProgressBar();
        }

        public override void Start()
        {
            base.Start();
            _programmingAction = (ClassParameter as ProgrammingParameter)?.ProgrammingAction;
            Title = (ClassParameter as ProgrammingParameter)?.Title;
            _textDialogAfterAction = (ClassParameter as ProgrammingParameter)?.TextDialogAfterAction;
            _programmingType = (ClassParameter as ProgrammingParameter)?.ProgrammingType ?? ProgrammingType.Read;
            _nfcReader.DetectTag();
            _cancellationToken = new CancellationTokenSource();

            if (_programmingType == ProgrammingType.Read)
            {
                InterruptingNFCText = string.Empty;
            }
            else
            {
                InterruptingNFCText = AppResources.F_InterruptingNFCText;
            }
        }

        public MvxCommand CloseCommand { get; private set; }

        /// <summary>
        /// Set the progress bar determinate/indeterminate
        /// </summary>
        private bool isProgressBarIndeterminate;
        public bool IsProgressBarIndeterminate {
            get { return isProgressBarIndeterminate; }
            set {
                isProgressBarIndeterminate = value;
                RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the programming progress percentage.
        /// </summary>
        /// <value>
        /// The progress percentage.
        /// </value>
        public double ProgressPercentage {
            get {
                return progressPercentage;
            }

            set {
                double progress = value;
                if (progress > 100)
                    progress = 100;

                progressPercentage = (double)progress / 100;

                if (progress == 0)
                {
                    ProgressStatus = tabSpace;
                }
                else
                {
                    ProgressStatus = string.Format("{0}%", progress);
                }
                RaisePropertyChanged();
                //RaisePropertyChanged(() => ProgressPercentage);
            }
        }
        
        public string ProgressStatus {
            get {
                return progressStatus;
            }

            set {
                progressStatus = value;
                RaisePropertyChanged();
                //RaisePropertyChanged(() => ProgressStatus);
            }
        }

        /// <summary>
        /// Gets the programming status text displayed in the status bar.
        /// </summary>
        /// <value>
        /// The status text.
        /// </value>
        public string StatusText {
            get {
                return statusText;
            }

            private set {
                statusText = value;
                RaisePropertyChanged(() => StatusText);
            }
        }

        public string InterruptingNFCText {
            get {
                return interruptingNFCText;
            }

            private set {
                interruptingNFCText = value;
                RaisePropertyChanged(() => InterruptingNFCText);
            }
        }

        #region Events Management
        private void StatusChange(object sender, string message)
        {
            StatusText = message;
        }

        private void ProgressChange(object sender, int value)
        {
            ProgressPercentage = value;
        }

        private void ProgrammeEnabled(object sender, EventArgs e)
        {
            var reader = Mvx.IoCProvider.Resolve<INfcCommunication>();
            if (reader.ReaderType == TFTBackend.NfcService.NfcDataTypes.NfcReaderType.Bluetooth)
                ShowProgrammerStatusToUser(true, ProgrammerTypeEnum.BL);
            else
                ShowProgrammerStatusToUser(true, ProgrammerTypeEnum.NFC);
        }

        private void ProgrammerDisabled(object sender, EventArgs e)
        {
            var reader = Mvx.IoCProvider.Resolve<INfcCommunication>();
            if (reader.ReaderType == TFTBackend.NfcService.NfcDataTypes.NfcReaderType.Bluetooth)
                ShowProgrammerStatusToUser(false, ProgrammerTypeEnum.BL);
            else
                ShowProgrammerStatusToUser(false, ProgrammerTypeEnum.NFC);
        }

        private void ShowProgrammerStatusToUser(bool enabled, ProgrammerTypeEnum programmerType)
        {
            ClosePopup();
            if (programmerType == ProgrammerTypeEnum.NFC)
            {
                if (enabled)
                {
                    _dialog.Alert(AppResources.F_NfcEnable);
                }
                else
                {
                    _dialog.Alert(AppResources.F_NfcDisabledUserMessage);
                }
            }
            else if (programmerType == ProgrammerTypeEnum.BL)
            {
                if (enabled)
                {
                    _dialog.Alert(AppResources.AndroidBluetoothEnabled);
                }
                else
                {
                    _dialog.Alert(AppResources.AndroidBluetoothDisabled);
                }
            }
        }

        private void DeviceUndetected(object sender, EventArgs e)
        {
            //StatusText = AppResources.F_DeviceUndetected;

            if (e?.GetType() == typeof(NfcEventArgs))
            {
                ClosePopup();
                NfcEventArgs ne = (NfcEventArgs)e;
                if (ne.EventType == NfcEventType.EcgNotFound)
                {
                    _dialog.Alert(AppResources.F_DialogEcgNotFound, AppResources.F_DialogProgrammingErrorTitle);
                }
                else if (ne.EventType == NfcEventType.DeviceNotFound)
                {
                    _dialog.Alert(AppResources.F_DialogDeviceNotFound, AppResources.F_DialogProgrammingErrorTitle);
                    base.RefreshNotificationArea();
                }
            }
        }

        private async void DeviceDetected(object sender, EventArgs e)
        {
            Logger.Trace("ProgrammingViewModel.DeviceDetected");
            if (_isBusy)
                return;
            try
            {
                _isBusy = true;
                if (_nfcReader.Connect())
                {
                    _cancellationToken.Token.ThrowIfCancellationRequested();
                    IsProgressBarIndeterminate = false;
                    StatusText = AppResources.F_StartProgramming;
                    bool success = await _programmingAction?.Invoke(this, _programmingService, _cancellationToken.Token);
                    _nfcReader.Disconnect();

                    // The operation is successful, close automatically the popop
                    // and (if is set) open the dialog to inform the user
                    if (success)
                    {
                        ClosePopup();
                        if (_textDialogAfterAction != null)
                        {
                            await _dialog.AlertAsync(_textDialogAfterAction, AppResources.F_DialogProgrammingSuccessfulTitle);
                        }
                    }
                    // The operation is failed but the operation is repeatible
                    // so it launch another _nfcReader.DetectTag()
                    else
                    {
                        ResetProgressBar();
                        StatusText = AppResources.F_TagOperationFailed;
                    }
                }
                else
                {
                    ResetProgressBar();
                    StatusText = AppResources.F_DeviceUndetected;
                }

            }
            // The rf password command is failed
            // so the operation is stopped
            catch (RFPasswordFailedException rfPasswordFailedException)
            {
                Logger.Exception("RFPassword", rfPasswordFailedException);                
                StatusText = AppResources.F_RFPasswordFailed;
                ResetProgressBar();
            }
            // The operation is failed and the operation is not repeatible
            // so the operation is stopped
            catch (OperationFailedException operationFailedException)
            {
                Logger.Exception("NFC", operationFailedException);
                StatusText = AppResources.F_TagOperationAborted;
                ClosePopup();
                ShowErrorMessage(operationFailedException.ErrorCode);
            }
            // The operation is Canceled by the user
            catch (OperationCanceledException ex)
            {
                Logger.Trace("OperationCanceledException", ex);
            }
            // The operation is failed and the operation is not repeatible
            // so the operation is stopped but why is not OperationFailedException?
            catch (Exception ex)
            {
                Logger.Exception("NFC", ex);
                ResetProgressBar();
            }
            finally
            {
                _isBusy = false;
                try
                {
                    _cancellationToken?.Dispose();
                    _cancellationToken = null;
                }
                catch (ObjectDisposedException)
                {
                    Logger.Trace("DeviceDetected: Token Already disposed");
                }
            }
        }

        private void ShowErrorMessage(int errorCode)
        {
            String errorMsg = null;
            switch (errorCode)
            {
                case ErrorCodes.DeviceNotSupported:
                    errorMsg = AppResources.ErrorDeviceNotSupported;
                    break;
                case ErrorCodes.MasterKeysNotMatching:
                    errorMsg = AppResources.ErrorMasterKeyNotMatching;
                    break;
                case ErrorCodes.MasterKeysNotSetted:
                    errorMsg = AppResources.ErrorMasterKeyNotSet;
                    break;
                case ErrorCodes.NoSourceTag:
                    errorMsg = AppResources.NoSourceTag;
                    break;
                case ErrorCodes.IncompatibleDevices:
                    errorMsg = AppResources.IncompatibleDevices;
                    break;
                case ErrorCodes.DifferentDevice:
                    errorMsg = AppResources.DifferentDevice;
                    break;
                case ErrorCodes.DifferentFamilyDevice:
                    errorMsg = AppResources.DifferentFamilyDevice;
                    break;
            }
            if (!String.IsNullOrWhiteSpace(errorMsg))
                _dialog.Alert(errorMsg, AppResources.F_DialogProgrammingErrorTitle);
        }

        private void ResetProgressBar()
        {
            IsProgressBarIndeterminate = true;
            ProgressPercentage = 0;

        }
        #endregion

        #region Parameter for this ViewModel
        public class ProgrammingParameter : BaseViewModelParameter
        {
            public ProgrammingParameter(bool isModal) : base(isModal)
            {
            }

            public String Title { get; set; }
            public Func<BaseViewModel, IProgrammingService, CancellationToken, Task<bool>> ProgrammingAction { get; set; }
            public String TextDialogAfterAction { get; set; }
            public ProgrammingType ProgrammingType { get; set; }
        }
        #endregion
    }

    public enum ProgrammingType
    {
        Read,
        Write
    }
}
