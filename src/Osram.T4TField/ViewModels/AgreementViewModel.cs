﻿using Plugin.Settings.Abstractions;
using Osram.T4TField.Resources;
using Osram.T4TField.Utils;
using MvvmCross.Commands;

namespace Osram.T4TField.ViewModels
{
    public class AgreementViewModel : ConnectionBaseViewModel
    {
        private readonly ISettings _settings;

        public AgreementViewModel(  ISettings settings)
        {
            _settings = settings;
            Title = AppResources.t4tField;
        }

        public bool IsModal
        {
            get { return ClassParameter != null && ClassParameter.IsModal; }
        }

        private MvxCommand _privacyPolicyCommand;
        public MvxCommand PrivacyPolicyCommand
        {
            get { return _privacyPolicyCommand ?? (_privacyPolicyCommand = new MvxCommand(PrivacyPolicy)); }
        }
        private void PrivacyPolicy()
        {
            ShowViewModel<PrivacyAgreementViewModel>();
        }

        private MvxCommand _termsOfUseCommand;
        public MvxCommand TermsOfUseCommand
        {
            get { return _termsOfUseCommand ?? (_termsOfUseCommand = new MvxCommand(TermsOfUse)); }
        }
        private void TermsOfUse()
        {
            ShowViewModel<TermsOfUseViewModel>();
        }

        private MvxCommand _acceptPrivacyAgreementCommand;
        public MvxCommand IAcceptCommand
        {
            get { return _acceptPrivacyAgreementCommand ?? (_acceptPrivacyAgreementCommand = new MvxCommand(AcceptAgreement)); }
        }
        private void AcceptAgreement()
        {
            _settings.AddOrUpdateValue(SettingsKeys.AgreementsAccepted, true);
            Cancel();
        }

        public class Parameter : BaseViewModelParameter
        {
            public Parameter(bool isModal) : base(isModal)
            {
            }
        }
    }
}