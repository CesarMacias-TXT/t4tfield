﻿using Osram.T4TField.Contracts;
using Osram.T4TField.Resources;
using Xamarin.Forms;
using System.Windows.Input;

namespace Osram.T4TField.ViewModels
{
    public class AboutViewModel : ConnectionBaseViewModel
    { 

         
        
            
        public ICommand OpenInprintCommand { get; }
        public ICommand OpenPrivacyCommand { get; }
        
        public ICommand OpenTermsConditionsCommand { get; }
        public ICommand OpenLicensesCommand { get; }

        public string InprintTxt => AppResources.F_ImprintTxt;
        public string TermsConditionsTxt => AppResources.F_TermsConditionsTxt;
        public string PrivacyPolicyTxt => AppResources.F_PrivacyPolicyTxt;
        public string LicensesTxt => AppResources.F_LicensesTxt;




        public AboutViewModel(IAppInfo appInfo)
        {

            OpenInprintCommand = new Command(() => ShowViewModel<ImprintViewModel>());
            OpenPrivacyCommand = new Command(() => ShowViewModel<PrivacyAgreementViewModel>());
            OpenTermsConditionsCommand = new Command(() => ShowViewModel<TermsOfUseViewModel>());
            OpenLicensesCommand = new Command(() => ShowViewModel<LicencesViewModel>());
            Title = AppResources.About;
      }

      






        public override void Start()
        {
            base.Start();
        }


    }
}