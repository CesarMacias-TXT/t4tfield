﻿using MvvmCross.ViewModels;
using MvvmCross.Commands;
using System;
using System.Threading.Tasks;
using System.Windows.Input;

namespace Osram.T4TField.ViewModels
{
    public class ListPurchaseViewModel : MvxViewModel
    {
        readonly Func<ListPurchaseViewModel, Task> ClickPurchaseAction;

        public ListPurchaseViewModel(Func<ListPurchaseViewModel, Task> clickPurchaseAction)
        {
            ClickPurchaseAction = clickPurchaseAction;
            PurchaseCommand = new MvxCommand(ClickItem);
        }

        public ICommand PurchaseCommand { get; set; }

        public virtual string Name { get; set; }

        public virtual string Description { get; set; }

        public virtual string PurchaseText { get; set; }

        public virtual string Price { get; set; }

        public virtual string Currency { get; set; }

        public virtual string Type { get; set; }

        private void ClickItem()
        {
            ClickPurchaseAction?.Invoke(this);
        }
    }
}
