﻿using System.Collections.Generic;
using Osram.TFTBackend.DataModel.Feature.Contracts;
using Osram.TFTBackend.DataModel.Helper;
using Osram.T4TField.Resources;
using System;
using System.Collections.ObjectModel;
using Plugin.Geolocator.Abstractions;
using System.Threading.Tasks;
using Osram.TFTBackend.LocationService.Contracts;
using Osram.T4TField.Messaging;
using MvvmCross.Plugin.Messenger;
using Osram.TFTBackend.LocationService;
using static Osram.TFTBackend.LocationService.EcgLocation;
using Osram.TFTBackend.KeyService.Contracts;

namespace Osram.T4TField.ViewModels.FeaturesViewModel
{
    public class AstroDimInformationViewModel : BaseFeatureViewModel
    {
        List<KeyValuePair<decimal, string>> SwitchOnRangeList;
        List<KeyValuePair<decimal, string>> SwitchOffRangeList;
        List<KeyValuePair<int, string>> AstroFadeTimeList;

        private IAstroDimFeature _astroDimFeature;
        private ILocationService _locationService;

        MvxSubscriptionToken _astroModeToken;

        public AstroDimInformationViewModel(IAstroDimFeature astroDimFeature, ILocationService locationService, IKeyService keyService) : base(keyService)
        {
            Title = AppResources.F_NavigationDimming;
            _astroDimFeature = astroDimFeature;
            _locationService = locationService;

            IFeature currentFeature = ((IFeature)astroDimFeature);
            IsEnabled = currentFeature.Info.IsEnabled;
            Lock = currentFeature.Info.Lock;

            SwitchOnFadeTimeListValues = new ObservableCollection<string>();
            SwitchOffFadeTimeListValues = new ObservableCollection<string>();
            AstroDimFadeTimeListValues = new ObservableCollection<string>();
            IsGPSLocation = false;
            FillGuiFields();
            SubscribeToMessages();
        }

        private void SubscribeToMessages()
        {
            _astroModeToken = _messenger.Subscribe<Events.SelectedAstroModeEvent>(OnChangeAstroDimMode);
        }
        
        private void UnsubscribeToMessages()
        {
            _astroModeToken.Dispose();
        }

        public override Task Cleanup()
        {
            UnsubscribeToMessages();
            return base.Cleanup();
        }

        private void FillGuiFields()
        {
            Task.Factory.StartNew(() => CreateGeoLocationValues());
            CreateSwitchOnFadeTimeValues();
            CreateSwitchOffFadeTimeValues();
            CreateAstroDimFadeTimeValues();
        }

        #region Switch ON
        private string switchOnFadeTimeValue;
        public string SwitchOnFadeTimeValue
        {
            get { return switchOnFadeTimeValue; }
            set
            {
                switchOnFadeTimeValue = value;
                RaisePropertyChanged();
            }
        }

        private ObservableCollection<string> switchOnFadeTimeListValues;
        public ObservableCollection<string> SwitchOnFadeTimeListValues
        {
            get { return switchOnFadeTimeListValues; }
            set
            {
                switchOnFadeTimeListValues = value;
                RaisePropertyChanged();
            }
        }

        private int switchOnSelectedIndex;
        public int SwitchOnSelectedIndex
        {
            get { return switchOnSelectedIndex; }
            set
            {
                if (value == -1)
                    return;
                switchOnSelectedIndex = value;
                if (switchOnSelectedIndex < SwitchOnRangeList.Count)
                {
                    var switchOnElement = SwitchOnRangeList[switchOnSelectedIndex];
                    _astroDimFeature.SwitchOnFadeTime = (int)switchOnElement.Key;
                }

            }
        }

        private void CreateSwitchOnFadeTimeValues()
        {
            SwitchOnRangeList = ConstantListValues.Instance.AstroDimSwitchOnRangeValues();
            foreach (KeyValuePair<decimal, string> v in SwitchOnRangeList)
            {
                SwitchOnFadeTimeListValues.Add(v.Value);
            }
            SwitchOnFadeTimeValue = SwitchOnRangeList.Find(x => x.Key == _astroDimFeature.SwitchOnFadeTime).Value;
            SwitchOnTitle = AppResources.F_SelectSwitchOnFadeTime;
        }
        #endregion

        #region AstroDim Fade Time
        private string astroDimFadeTimeValue;
        public string AstroDimFadeTimeValue
        {
            get { return astroDimFadeTimeValue; }
            set
            {
                astroDimFadeTimeValue = value;
                RaisePropertyChanged();
            }
        }
        private ObservableCollection<string> astroDimFadeTimeListValues;
        public ObservableCollection<string> AstroDimFadeTimeListValues
        {
            get { return astroDimFadeTimeListValues; }
            set
            {
                astroDimFadeTimeListValues = value;
                RaisePropertyChanged();
            }
        }

        private int astroDimSelectedIndex;
        public int AstroDimSelectedIndex
        {
            get { return astroDimSelectedIndex; }
            set
            {
                if (value == -1)
                    return;
                astroDimSelectedIndex = value;
                if (astroDimSelectedIndex < AstroFadeTimeList.Count)
                {
                    var astroDimElement = AstroFadeTimeList[astroDimSelectedIndex];
                    _astroDimFeature.AstroDimFadeTime = astroDimElement.Key;
                    //TODO notify AstroDimViewModel
                    //if (astroDimMode)
                    //{
                    //    ValidateAstroBasedTimeLevels(RSTime2, RSTime3, RSTime4, RSTime5);
                    //}
                    //else
                    //{
                    //    ValidateTimeBasedTimeLevels(RSTimeBasedTime2, RSTimeBasedTime3, RSTimeBasedTime4, RSTimeBasedTime5);
                    //}
                    _messenger.Publish(new Events.ChangedAstroDimFadeTimeEvent(this));

                }
            }
        }

        private void CreateAstroDimFadeTimeValues()
        {
            AstroFadeTimeList = ConstantListValues.Instance.AstroDimRangeValues();
            foreach (KeyValuePair<int, string> v in AstroFadeTimeList)
            {
                AstroDimFadeTimeListValues.Add(v.Value);
            }
            AstroDimFadeTimeValue = AstroFadeTimeList.Find(x => x.Key == _astroDimFeature.AstroDimFadeTime).Value;
            AstroDimTitle = AppResources.F_SelectAstroDimFadeTime;
        }
        #endregion

        #region Switch OFF
        private string switchOffValue;
        public string SwitchOffFadeTimeValue
        {
            get { return switchOffValue; }
            set
            {
                switchOffValue = value;
                RaisePropertyChanged();
            }
        }
        private ObservableCollection<string> switchOffFadeTimeListValues;
        public ObservableCollection<string> SwitchOffFadeTimeListValues
        {
            get { return switchOffFadeTimeListValues; }
            set
            {
                switchOffFadeTimeListValues = value;
                RaisePropertyChanged();
            }
        }

        private int switchOffSelectedIndex;
        public int SwitchOffSelectedIndex
        {
            get { return switchOffSelectedIndex; }
            set
            {
                if (value == -1)
                    return;
                switchOffSelectedIndex = value;
                if (switchOffSelectedIndex < SwitchOffRangeList.Count)
                {
                    var switchOffElement = SwitchOffRangeList[switchOffSelectedIndex];
                    _astroDimFeature.SwitchOffFadeTime = (int)switchOffElement.Key;
                }
            }
        }

        private void CreateSwitchOffFadeTimeValues()
        {
            SwitchOffRangeList = ConstantListValues.Instance.AstroDimSwitchOffRangeValues();
            foreach (KeyValuePair<decimal, string> v in SwitchOffRangeList)
            {
                SwitchOffFadeTimeListValues.Add(v.Value);
            }
            SwitchOffFadeTimeValue = SwitchOffRangeList.Find(x => x.Key == _astroDimFeature.SwitchOffFadeTime).Value;
            SwitchOffTitle = AppResources.F_SelectSwitchOffFadeTime;
        }
        #endregion

        public string SwitchOnTitle { get; set; }
        public string SwitchOffTitle { get; set; }
        public string AstroDimTitle { get; set; }

        private bool switchOffEnabled;
        public bool SwitchOffEnabled
        {
            get { return (switchOffEnabled && IsEditable); }
            set
            {
                switchOffEnabled = value;
                RaisePropertyChanged();
            }
        }

        #region Location
        private string utcZone = AppResources.F_Searching;
        public string UtcZone
        {
            get { return utcZone; }
            set
            {
                utcZone = value;
                RaisePropertyChanged();
            }
        }

        private string location = AppResources.F_Searching;
        public string Location
        {
            get { return location; }
            set
            {
                location = value;
                RaisePropertyChanged();
            }
        }



        public IList<Location> LocationList
        {
            get { return EcgLocation.Locations; }
        }

        public bool isLocationEnabled { get; set; } = true;

        public bool IsLocationEnabled {
            get {
                return (isLocationEnabled && IsEditable);
            }
            set {
                value = isLocationEnabled;
            }
        }

        private Location locationSelected;
        public Location LocationSelected
        {
            get { return locationSelected; }
            set
            {
                if (locationSelected != value)
                {
                    var loc = value;
                    _astroDimFeature.Location = loc;
                    
                    //skip the first time
                    if (locationSelected != null)
                        _messenger.Publish(new Events.SelectedAstroLocationEvent(this, loc));

                    locationSelected = loc;
                    UpdateLocationInfo();         
                }
            }
        }

        private string latitude = AppResources.F_Searching;
        public string Latitude
        {
            get { return latitude; }
            set
            {
                latitude = value;
                RaisePropertyChanged();
            }
        }

        private string longitude = AppResources.F_Searching;
        public string Longitude
        {
            get { return longitude; }
            set
            {
                longitude = value;
                RaisePropertyChanged();
            }
        }

        public bool IsGPSLocation { get; private set; }

        private async void CreateGeoLocationValues()
        {
            if (IsGPSLocation)
            {

                Position position = await _locationService.GetCurrentLocationAsync();
                if (position != null)
                {
                    TimeSpan offsetFromUtc = TimeZoneInfo.Local.GetUtcOffset(position.Timestamp.LocalDateTime);
                    Location = TimeZoneInfo.Local.DisplayName;
                    UtcZone = _locationService.GetTimeZoneUtcString(offsetFromUtc);
                    Latitude = position.Latitude.ToString("0.000");
                    Longitude = position.Longitude.ToString("0.000");
                }
                else
                {
                    Location = AppResources.F_NotAvailable;
                    UtcZone = AppResources.F_NotAvailable;
                    Latitude = AppResources.F_NotAvailable;
                    Longitude = AppResources.F_NotAvailable;
                }
            }
            else
            {

                LocationSelected = _astroDimFeature.Location;               
            }
        }

        private void UpdateLocationInfo()
        {
            UtcZone = LocationSelected.GetUTCFromOffset(LocationSelected.Offset);
            Latitude = LocationSelected.LatitudeValue.ToString();
            Longitude = LocationSelected.LongitudeValue.ToString();
        }
        #endregion

        private void OnChangeAstroDimMode(Events.SelectedAstroModeEvent ev)
        {
            if (ev.Mode == AstroDimMode.TimeBased)
            {
                SwitchOffFadeTimeValue = SwitchOffRangeList.Find(l => l.Key == 255).Value;
                SwitchOffEnabled = false;
                IsLocationEnabled = false;
            }
            else
            {
                IsLocationEnabled = true;
                SwitchOffEnabled = true;
            }
        }
    }
}
