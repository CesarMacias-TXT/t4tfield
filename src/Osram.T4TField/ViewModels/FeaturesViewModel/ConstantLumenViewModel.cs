﻿using Acr.UserDialogs;
using Osram.T4TField.Resources;
using Osram.TFTBackend.DataModel.Feature.Contracts;
using Osram.TFTBackend.KeyService.Contracts;

namespace Osram.T4TField.ViewModels
{
    class ConstantLumenViewModel : BaseFeatureViewModel
    {
        private IConstantLumenFeature ConstantLumenFeature { get; }

        public ConstantLumenViewModel(IConstantLumenFeature constantLumenFeature, IKeyService keyService, IUserDialogs userDialogs) : base(keyService)
        {
            Title = AppResources.F_Report_CostantLumen;
            IFeature currentFeature = constantLumenFeature;

            IsEnabled = constantLumenFeature.Enable == 1;

            Lock = currentFeature.Info.Lock;

            ConstantLumenFeature = constantLumenFeature;

            time2 = ConstantLumenFeature.Time2;
            time3 = ConstantLumenFeature.Time3;
            time4 = ConstantLumenFeature.Time4;
            time5 = ConstantLumenFeature.Time5;
            time6 = ConstantLumenFeature.Time6;
            time7 = ConstantLumenFeature.Time7;
            time8 = ConstantLumenFeature.Time8;

            adjustmentLevel1 = ConstantLumenFeature.AdjustmentLevel1;
            adjustmentLevel2 = ConstantLumenFeature.AdjustmentLevel2;
            adjustmentLevel3 = ConstantLumenFeature.AdjustmentLevel3;
            adjustmentLevel4 = ConstantLumenFeature.AdjustmentLevel4;
            adjustmentLevel5 = ConstantLumenFeature.AdjustmentLevel5;
            adjustmentLevel6 = ConstantLumenFeature.AdjustmentLevel6;
            adjustmentLevel7 = ConstantLumenFeature.AdjustmentLevel7;
            adjustmentLevel8 = ConstantLumenFeature.AdjustmentLevel8;
        }

        public bool Enable
        {
            get {
                return ConstantLumenFeature.Enable == 1; }
            set
            {
                ConstantLumenFeature.Enable = value ? 1 : 0;

                IsEnabled = value;
                RaiseAllPropertiesChanged();
            }
        }

        #region Time
        public int Time1
        {
            get { return ConstantLumenFeature.Time1; }
        }

        private int time2;
        public int Time2
        {
            get { return time2; }
            set
            {
                time2 = value;
                ConstantLumenFeature.Time2 = value;

                RaiseAllPropertiesChanged();
            }
        }

        private int time3;
        public int Time3
        {
            get { return time3; }
            set
            {
                time3 = value;
                ConstantLumenFeature.Time3 = value;

                RaiseAllPropertiesChanged();
            }
        }

        private int time4;
        public int Time4
        {
            get { return time4; }
            set
            {
                time4 = value;
                ConstantLumenFeature.Time4 = value;

                RaiseAllPropertiesChanged();
            }
        }

        private int time5;
        public int Time5
        {
            get { return time5; }
            set
            {
                time5 = value;
                ConstantLumenFeature.Time5 = value;

                RaiseAllPropertiesChanged();
            }
        }

        private int time6;
        public int Time6
        {
            get { return time6; }
            set
            {
                time6 = value;
                ConstantLumenFeature.Time6 = value;

                RaiseAllPropertiesChanged();
            }
        }

        private int time7;
        public int Time7
        {
            get { return time7; }
            set
            {
                time7 = value;
                ConstantLumenFeature.Time7 = value;

                RaiseAllPropertiesChanged();
            }
        }

        private int time8;
        public int Time8
        {
            get { return time8; }
            set
            {
                time8 = value;
                ConstantLumenFeature.Time8 = value;

                RaiseAllPropertiesChanged();
            }
        }
        #endregion

        #region Levels
        private int adjustmentLevel1;
        public int AdjustmentLevel1
        {
            get { return adjustmentLevel1; }
            set
            {
                adjustmentLevel1 = value;
                ConstantLumenFeature.AdjustmentLevel1 = value;

                RaiseAllPropertiesChanged();
            }
        }

        private int adjustmentLevel2;
        public int AdjustmentLevel2
        {
            get { return adjustmentLevel2; }
            set
            {
                adjustmentLevel2 = value;
                ConstantLumenFeature.AdjustmentLevel2 = value;

                RaiseAllPropertiesChanged();
            }
        }

        private int adjustmentLevel3;
        public int AdjustmentLevel3
        {
            get { return adjustmentLevel3; }
            set
            {
                adjustmentLevel3 = value;
                ConstantLumenFeature.AdjustmentLevel3 = value;

                RaiseAllPropertiesChanged();
            }
        }

        private int adjustmentLevel4;
        public int AdjustmentLevel4
        {
            get { return adjustmentLevel4; }
            set
            {
                adjustmentLevel4 = value;
                ConstantLumenFeature.AdjustmentLevel4 = value;

                RaiseAllPropertiesChanged();
            }
        }

        private int adjustmentLevel5;
        public int AdjustmentLevel5
        {
            get { return adjustmentLevel5; }
            set
            {
                adjustmentLevel5 = value;
                ConstantLumenFeature.AdjustmentLevel5 = value;

                RaiseAllPropertiesChanged();
            }
        }

        private int adjustmentLevel6;
        public int AdjustmentLevel6
        {
            get { return adjustmentLevel6; }
            set
            {
                adjustmentLevel6 = value;
                ConstantLumenFeature.AdjustmentLevel6 = value;

                RaiseAllPropertiesChanged();
            }
        }

        private int adjustmentLevel7;
        public int AdjustmentLevel7
        {
            get { return adjustmentLevel7; }
            set
            {
                adjustmentLevel7 = value;
                ConstantLumenFeature.AdjustmentLevel7 = value;

                RaiseAllPropertiesChanged();
            }
        }

        private int adjustmentLevel8;
        public int AdjustmentLevel8
        {
            get { return adjustmentLevel8; }
            set
            {
                adjustmentLevel8 = value;
                ConstantLumenFeature.AdjustmentLevel8 = value;

                RaiseAllPropertiesChanged();
            }
        }
        #endregion
    }
}
