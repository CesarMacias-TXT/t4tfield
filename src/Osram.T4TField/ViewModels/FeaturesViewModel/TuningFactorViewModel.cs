﻿using Acr.UserDialogs;
using MvvmCross.Commands;
using Osram.T4TField.Resources;
using Osram.TFTBackend;
using Osram.TFTBackend.DataModel.Data.Contracts;
using Osram.TFTBackend.DataModel.Feature.Contracts;
using Osram.TFTBackend.KeyService.Contracts;
using System;
using System.Threading.Tasks;
using System.Windows.Input;

namespace Osram.T4TField.ViewModels
{
    class TuningFactorViewModel : BaseFeatureViewModel
    {
        private ITuningFactorFeature TuningFactorFeature { get; }
        private IOperatingCurrentData OperatingCurrentFeature { get; }

        public ICommand SetDirectValue { get; set; }

        public TuningFactorViewModel(ITuningFactorFeature tuningFactorFeature, IOperatingCurrentData operatingCurrentFeature, IKeyService keyService, IUserDialogs userDialogs) : base(keyService)
        {
            Title = AppResources.F_NavigationTuning;
            IFeature currentFeature = ((IFeature)tuningFactorFeature);

            // MP + TF --> Tuning Factor
            // MP + !TF --> User cannot use the feature
            // !MP + TF --> Tuning Factor
            // !MP + !TF --> Operating Current

            if (IsMasterPasswordProtected)
            {
                IsEnabled = currentFeature.Info.IsEnabled;
            }
            else
            {
                // if no master password the user can use the feature
                IsEnabled = currentFeature.Info.IsEnabled || (operatingCurrentFeature.OperatingCurrent != 0);
            }

            if (!tuningFactorFeature.Info.IsEnabled)
            {
                //if (operatingCurrentFeature.OperatingCurrent == 0)
                //{
                //    IsEnabled = false;
                //}
                if (!IsMasterPasswordProtected || operatingCurrentFeature.OperatingCurrent != 0)
                {
                    IsEnabled = true;
                }
            }

            Lock = currentFeature.Info.Lock;

            TuningFactorFeature = tuningFactorFeature;
            OperatingCurrentFeature = operatingCurrentFeature;

            //if (!IsEnabled && Lock == ProtectionLock.None && !IsMasterPasswordProtected)
            if (!IsEnabled && Lock == ProtectionLock.None && !IsMasterPasswordProtected)
            {
                Task.Run(async () => await userDialogs.AlertAsync(AppResources.F_TuningFactorDisableMessage));
            }

            if (IsEnabled && Lock != ProtectionLock.Master)
            {
                SetDirectValue = new MvxCommand(OnSetDirectValue);
            }
        }

        //private bool IsUsingOperatingCurrent => !IsMasterPasswordProtected && !TuningFactorFeature.Info.IsEnabled && (OperatingCurrentFeature.OperatingCurrent != 0);
        private bool IsUsingOperatingCurrent => !TuningFactorFeature.Info.IsEnabled && (OperatingCurrentFeature.OperatingCurrent != 0);

        private void OnSetDirectValue()
        {
            Logger.TaggedTrace("TuningFactorViewModel.OnSetDirectValue", "Open popup for set direct value in TuningFactor");

            MyShowViewModel<TuningFactorPopupViewModel>(RequestType.Popup, new SetTuningFactorPopupParameter(true)
            {
                Value = CurrentOutput,
                MinValue = CurrentOutputMin,
                MaxValue = CurrentOutputMax,
                SetValue = OnChangeOutputValue,
            });
        }

        private void OnChangeOutputValue(int value)
        {
            if (IsUsingOperatingCurrent)
            {
                OperatingCurrentFeature.OperatingCurrent = value;
            }
            else if (ReferenceLumenOutput == 0)
            {
                TuningFactorFeature.TuningFactor = (int)Math.Round(((float)value / OperatingCurrentFeature.OperatingCurrent * 100));
            }
            else
            {
                TuningFactorFeature.TuningFactor = (int)Math.Round(((float)value / TuningFactorFeature.ReferenceLumenOutput * 100));
            }


            //In some devices there is this “combined” subdevice PwmConfig-0(instead of TF - 0), which holds the setvalues of current and tuning factor.
            //After an update of any of those properties, for the devices which additionally have the PwmOut - 0 subdevice(only 2 drivers: AM10971 und AM10972),
            //the PwmPulsWidth has to be calculated as follows:
            if (TuningFactorFeature.PwmPulsWidth != 0)
            {
                TuningFactorFeature.PwmPulsWidth = Math.Max(
                    5000 * (OperatingCurrentFeature.OperatingCurrent / OperatingCurrentFeature.PhysicalMaxLevel) * (TuningFactorFeature.TuningFactor / 100),
                    5000 * (OperatingCurrentFeature.PhysicalMinLevel / OperatingCurrentFeature.PhysicalMaxLevel))
                    + 32768;
            }

            CurrentOutput = value;
        }

        public int MaximumTuningFactorValue {
            get {
                if (IsUsingOperatingCurrent)
                {
                    return 100;
                }
                else
                {
                    return TuningFactorFeature.MaximumTuningFactor;
                }
            }
        }

        public int MinimumTuningFactorValue {
            get {
                if (IsUsingOperatingCurrent)
                {
                    return (int)((float)OperatingCurrentFeature.PhysicalMinLevel / OperatingCurrentFeature.PhysicalMaxLevel * 100);
                }
                else
                {
                    return TuningFactorFeature.MinimumTuningFactor;
                }
            }
        }

        public int MaximumTuningFactorValueInMa {
            get {
                if (IsUsingOperatingCurrent)
                {
                    return OperatingCurrentFeature.PhysicalMaxLevel;
                }
                else
                {
                    return GetMilliAmpereValueByTuningFactor(TuningFactorFeature.MaximumTuningFactor);
                }
            }
        }

        public int MinimumTuningFactorValueInMa {
            get {
                if (IsUsingOperatingCurrent)
                {
                    return OperatingCurrentFeature.PhysicalMinLevel;
                }
                else
                {
                    return GetMilliAmpereValueByTuningFactor(TuningFactorFeature.MinimumTuningFactor);
                }
            }
        }

        public int TuningFactorValue {
            get {
                if (IsUsingOperatingCurrent)
                {
                    return (int)Math.Round(((float)OperatingCurrentFeature.OperatingCurrent / OperatingCurrentFeature.PhysicalMaxLevel * 100));
                }
                else
                {
                    return TuningFactorFeature.TuningFactor;
                }
            }

            set {

                if (IsUsingOperatingCurrent)
                {
                    // Get the new value of Operating Current
                    var newValue = (int)Math.Round(((float)value * OperatingCurrentFeature.PhysicalMaxLevel / 100));

                    // In case of < min or > max se to min or max
                    if (newValue < OperatingCurrentFeature.PhysicalMinLevel)
                    {
                        newValue = OperatingCurrentFeature.PhysicalMinLevel;
                    }
                    else if (newValue > OperatingCurrentFeature.PhysicalMaxLevel)
                    {
                        newValue = OperatingCurrentFeature.PhysicalMaxLevel;
                    }

                    OperatingCurrentFeature.OperatingCurrent = newValue;

                    CurrentOutput = OperatingCurrentFeature.OperatingCurrent;
                    CurrentOutputUnit = AppResources.F_MilliAmpere;
                }
                else
                {
                    TuningFactorFeature.TuningFactor = value;

                    if (ReferenceLumenOutput == 0)
                    {
                        CurrentOutput = GetMilliAmpereValue();
                        CurrentOutputUnit = AppResources.F_MilliAmpere;
                    }
                    else
                    {
                        CurrentOutput = ReferenceLumenOutput;
                        CurrentOutputUnit = AppResources.F_Lumen;
                    }
                }

                RaisePropertyChanged();
            }
        }

        private int GetMilliAmpereValue()
        {
            //Fixed current -> Default Operating current x Tuning factor / 100
            if (OperatingCurrentFeature.OperatingCurrent == 0)
            {
                return TuningFactorFeature.TuningFactor;
            }

            return OperatingCurrentFeature.OperatingCurrent * TuningFactorFeature.TuningFactor / 100;
        }

        private int GetMilliAmpereValueByTuningFactor(int tuningFactorValue)
        {
            if(OperatingCurrentFeature.OperatingCurrent == 0)
            {
                return tuningFactorValue;
            }
            //Fixed current -> Default Operating current x Tuning factor / 100
            return OperatingCurrentFeature.OperatingCurrent * tuningFactorValue / 100;
        }

        public int ReferenceLumenOutput => (TuningFactorFeature.ReferenceLumenOutput * TuningFactorFeature.TuningFactor) / 100;

        private int currentOutput;
        public int CurrentOutput {
            get { return currentOutput; }
            set {
                currentOutput = value;

                if (IsUsingOperatingCurrent)
                {
                    CurrentOutputInMa = string.Empty;
                }
                else if (ReferenceLumenOutput == 0)
                {
                    CurrentOutputInMa = string.Empty;
                }
                else
                {
                    CurrentOutputInMa = GetMilliAmpereValue() + AppResources.F_MilliAmpere;
                }

                //In some devices there is this “combined” subdevice PwmConfig-0(instead of TF - 0), which holds the setvalues of current and tuning factor.
                //After an update of any of those properties, for the devices which additionally have the PwmOut - 0 subdevice(only 2 drivers: AM10971 und AM10972),
                //the PwmPulsWidth has to be calculated as follows:
                if (TuningFactorFeature.PwmPulsWidth != 0)
                {
                    TuningFactorFeature.PwmPulsWidth = Math.Max(
                        5000 * (OperatingCurrentFeature.OperatingCurrent / OperatingCurrentFeature.PhysicalMaxLevel) * (TuningFactorFeature.TuningFactor / 100),
                        5000 * (OperatingCurrentFeature.PhysicalMinLevel / OperatingCurrentFeature.PhysicalMaxLevel))
                        + 32768;
                }

                RaiseAllPropertiesChanged();
            }
        }

        public int CurrentOutputMin {
            get {
                if (IsUsingOperatingCurrent)
                {
                    return OperatingCurrentFeature.PhysicalMinLevel;
                }
                else
                {
                    if (ReferenceLumenOutput == 0)
                    {
                        return GetMilliAmpereValueByTuningFactor(TuningFactorFeature.MinimumTuningFactor);
                    }
                    else
                    {
                        return MinimumTuningFactorValue * TuningFactorFeature.ReferenceLumenOutput / 100;
                    }
                }
            }
        }

        public int CurrentOutputMax {
            get {
                if (IsUsingOperatingCurrent)
                {
                    return OperatingCurrentFeature.PhysicalMaxLevel;
                }
                else
                {
                    if (ReferenceLumenOutput == 0)
                    {
                        return GetMilliAmpereValueByTuningFactor(TuningFactorFeature.MaximumTuningFactor);
                    }
                    else
                    {
                        return MaximumTuningFactorValue * TuningFactorFeature.ReferenceLumenOutput / 100;
                    }
                }
            }
        }

        private string currentOutputInMa;
        public string CurrentOutputInMa {
            get { return currentOutputInMa; }
            set {
                currentOutputInMa = value;
                RaisePropertyChanged();
            }
        }

        public string CurrentOutputUnit { get; set; }

        public class SetTuningFactorPopupParameter : BaseViewModelParameter
        {
            public SetTuningFactorPopupParameter(bool isModal)
                : base(isModal)
            {

            }

            public int Value { get; set; }

            public int MinValue { get; set; }

            public int MaxValue { get; set; }

            public Action<int> SetValue { get; set; }
        }
    }
}
