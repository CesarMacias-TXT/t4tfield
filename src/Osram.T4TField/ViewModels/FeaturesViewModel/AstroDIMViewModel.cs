﻿using MvvmCross.Plugin.Messenger;
using System;
using System.Windows.Input;
using Osram.T4TField.Messaging;
using Osram.T4TField.Resources;
using Osram.TFTBackend.DataModel.Feature.Contracts;
using System.Threading.Tasks;
using Osram.TFTBackend;
using System.Globalization;
using Osram.TFTBackend.LocationService;
using Osram.TFTBackend.DataModel.Data.Contracts;
using Osram.TFTBackend.KeyService.Contracts;
using MvvmCross.Commands;

namespace Osram.T4TField.ViewModels.FeaturesViewModel
{
    public class AstroDimViewModel : BaseFeatureViewModel
    {
        IDimmingModeData _dimmingFeature;
        IAstroDimFeature _astroDimFeature;
        IOperatingCurrentData _operatingCurrentFeature;

        MvxSubscriptionToken _astroDimToken;
        MvxSubscriptionToken _astroModeToken;
        MvxSubscriptionToken _astroLocationToken;
        MvxSubscriptionToken _operatingModeToken;

        public OperatingModeEnum OperatingModeEnum;

        /// <summary>
        /// The real midnight time value.
        /// </summary>
        private DateTime realMidnightTime = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 0, 0, 0, 0);

        public AstroDimViewModel(IAstroDimFeature astroDimFeature, IDimmingModeData dimmingModeFeature, IOperatingCurrentData operatingCurrentFeature, IKeyService keyService) : base(keyService)
        {
            Title = AppResources.F_NavigationDimming;

            _astroDimFeature = astroDimFeature;
            _dimmingFeature = dimmingModeFeature;
            _operatingCurrentFeature = operatingCurrentFeature;

            IFeature currentFeature = ((IFeature)astroDimFeature);
            IsEnabled = currentFeature.Info.IsEnabled;
            Lock = currentFeature.Info.Lock;

            InitialiseCommands();
            Bind4DimDataModelToUI();
            SubscribeToMessages();
        }

        public override Task Cleanup()
        {
            UnsubscribeToMessages();
            return base.Cleanup();
        }

        private void SubscribeToMessages()
        {
            //AstroDim Fade Time subscription
            _astroDimToken = _messenger.Subscribe<Events.ChangedAstroDimFadeTimeEvent>((e) => ValidateTimeLevels());
            _astroModeToken = _messenger.Subscribe<Events.SelectedAstroModeEvent>(OnChangeAstroDimMode);
            _astroLocationToken = _messenger.Subscribe<Events.SelectedAstroLocationEvent>(OnChangeAstroLocation);

            _operatingModeToken = _messenger.Subscribe<Events.SelectedOperatingModeEvent>(OnChangeOperatingMode);
        }
     
        private void UnsubscribeToMessages()
        {
            _astroDimToken.Dispose();
            _astroModeToken.Dispose();
            _astroLocationToken.Dispose();
            _operatingModeToken.Dispose();
        }

        private void InitialiseCommands()
        {
            SetTimeCommand = new MvxCommand<int>(OnSetTimeCommand);
            ValidateOutputValue1 = new MvxCommand(OnValidateOutputValue1);
            ValidateOutputValue2 = new MvxCommand(OnValidateOutputValue2);
            ValidateOutputValue3 = new MvxCommand(OnValidateOutputValue3);
            ValidateOutputValue4 = new MvxCommand(OnValidateOutputValue4);
            ValidateOutputValue5 = new MvxCommand(OnValidateOutputValue5);
        }

        /// <summary>
        /// Methods to  set reference schedule time to default in Time based mode.
        /// Handles resetting the time values to default when the mode is changed from astro based to time based.
        /// </summary>
        private void SetTimeBasedRSTimeToDefault()
        {
            if (ReferenceScheduleTime == null)
                InitialiseReferenceScheduleTimeBasedTime();

            var timeDefault = _astroDimFeature.GetDefaultReferenceScheduleTime();
            ReferenceScheduleTime = timeDefault;
            RaisePropertyChanged(() => ReferenceScheduleTime);
        }

        private void InitialiseReferenceScheduleTimeBasedTime()
        {
            ReferenceScheduleTime = _astroDimFeature.GetTimeBasedReferenceScheduleTime();
        }

        private void InitialiseReferenceScheduleAstroBasedTime()
        {
            ReferenceScheduleTime = _astroDimFeature.GetAstroBasedReferenceScheduleTime();
        }

        private void UpdateTime(object sender, int number)
        {
            //remove temporary the change event for adjusting time value
            ReferenceScheduleTime.Changed -= UpdateTime;

            if (_astroDimFeature.AstroDimMode == AstroDimMode.TimeBased)
            {
                ReferenceScheduleTime[number] = ValidateTimeBaseTimeStepIncrement(_astroDimFeature.MinimumDifferencesInTime, (TimeValue)sender, number);
            }
            else if (_astroDimFeature.AstroDimMode == AstroDimMode.AstroBased)
            {
                ReferenceScheduleTime[number] = ValidateAstroBasedTimeStepIncrement(_astroDimFeature.MinimumDifferencesInTime, (TimeValue)sender, number);
            }

            RaisePropertyChanged("ReferenceScheduleTime");
            _astroDimFeature.UpdateDimDurationToDataModel(ReferenceScheduleTime);

            //add the change event
            ReferenceScheduleTime.Changed += UpdateTime;
        }

        #region AstroBased Reference Schedule Time Validations
        /// <summary>
        /// Validate time entry  with minimal steps required based upon device and astro dim value
        /// </summary>
        /// <param name="requiredTimeStamp">Required Time Stamp</param>
        /// <param name="time">Time number value.</param>
        /// <returns>The validated time based upon astro dim time and minimum steps required between two time stamps</returns>
        private DateTime ValidateAstroBasedTimeStepIncrement(TimeSpan requiredTimeStamp, TimeValue sender, int time)
        {
            if (time == 2)
                return ValidateTimeStep2Increment(requiredTimeStamp, sender, time);
            else if (time == 5)
                return ValidateTimeStep5Increment(requiredTimeStamp, sender, time);
            else
                return ValidateTimeStepIncrement(requiredTimeStamp, sender, time);
        }

        private DateTime ValidateTimeStep5Increment(TimeSpan requiredTimeStamp, TimeValue sender, int time)
        {
            DateTime previousTime = sender[time - 1];
            DateTime currentTime = sender[time];
            DateTime nextTime = DateTime.Parse("11:59").AddDays(1);

            TimeSpan duration1 = currentTime.Subtract(previousTime);
            TimeSpan durationNP = nextTime.Subtract(previousTime);

            if (previousTime < currentTime && currentTime <= nextTime)
            {
                if (duration1 >= requiredTimeStamp)
                {
                    return currentTime;
                }
                else if (durationNP >= TimeSpan.FromTicks(requiredTimeStamp.Ticks * 2))
                {
                    return previousTime.Add(requiredTimeStamp);
                }
                else if (durationNP > TimeSpan.FromTicks(requiredTimeStamp.Ticks))
                {
                    return previousTime.Add(requiredTimeStamp);
                }
                else
                {
                    return referenceScheduleTime[time];
                }
            }
            return currentTime;
        }

        /// <summary>
        /// Validate time entry for time 2 with minimal steps required based upon device and astro dim value
        /// </summary>
        private DateTime ValidateTimeStep2Increment(TimeSpan requiredTimeStamp, TimeValue sender, int time)
        {

            DateTime previousTime = DateTime.Parse("12:00");
            DateTime currentTime = sender[time];
            DateTime nextTime = sender[time + 1];

            TimeSpan duration1 = nextTime.Subtract(currentTime);
            TimeSpan durationNP = nextTime.Subtract(previousTime);

            if (previousTime <= currentTime && currentTime < nextTime)
            {
                if (duration1 >= requiredTimeStamp)
                {
                    return currentTime;
                }
                else if (durationNP >= TimeSpan.FromTicks(requiredTimeStamp.Ticks))
                {
                    return nextTime.Subtract(requiredTimeStamp);
                }
                else
                {
                    //odl value
                    return referenceScheduleTime[time];
                }
            }

            return currentTime;
        }

        private DateTime ValidateTimeStepIncrement(TimeSpan requiredTimeStamp, TimeValue sender, int time)
        {
            var currentTime = sender[time];
            var prevStamp = sender[time - 1];
            var nextStamp = sender[time + 1];

            TimeSpan diffrence1 = new TimeSpan(0, 0, 0, 0, 0);
            TimeSpan diffrence2 = new TimeSpan(0, 0, 0, 0, 0);
            string currentTimeFormat = currentTime.ToString("tt", CultureInfo.InvariantCulture);
            string prevTimeFormat = prevStamp.ToString("tt", CultureInfo.InvariantCulture);
            string nextTime = nextStamp.ToString("tt", CultureInfo.InvariantCulture);

            TimeSpan duration1 = currentTime.Subtract(prevStamp);
            TimeSpan duration2 = nextStamp.Subtract(currentTime);

            if ((prevTimeFormat.Contains(AstroBasedUtils.StrTimePM) && currentTimeFormat.Contains(AstroBasedUtils.StrTimePM) && nextTime.Contains(AstroBasedUtils.StrTimePM)) ||
                (prevTimeFormat.Contains(AstroBasedUtils.StrTimeAM) && currentTimeFormat.Contains(AstroBasedUtils.StrTimeAM) && nextTime.Contains(AstroBasedUtils.StrTimeAM)))
            {
                if (duration1 > requiredTimeStamp)
                {
                    if (duration2 > requiredTimeStamp)
                    {
                        return currentTime;
                    }
                    else
                    {
                        TimeSpan diffrenceNP = new TimeSpan(0, duration1.Hours, duration1.Minutes, 0, 0);
                        if (diffrenceNP >= TimeSpan.FromTicks(requiredTimeStamp.Ticks * 2))
                        {
                            return nextStamp.Subtract(requiredTimeStamp);
                        }
                        else
                        {
                            return referenceScheduleTime[time];
                        }
                    }
                }
                else
                {
                    return prevStamp.Add(requiredTimeStamp);
                }
            }
            else
            {
                if (prevTimeFormat.Contains(AstroBasedUtils.StrTimePM) && currentTimeFormat.Contains(AstroBasedUtils.StrTimePM) && nextTime.Contains(AstroBasedUtils.StrTimeAM))
                {
                    diffrence1 = new TimeSpan(0, duration1.Hours, duration1.Minutes, 0, 0);
                    diffrence2 = new TimeSpan(0, 23 + duration2.Hours, 60 + duration2.Minutes, 0, 0);
                }

                if (prevTimeFormat.Contains(AstroBasedUtils.StrTimePM) && currentTimeFormat.Contains(AstroBasedUtils.StrTimeAM) && nextTime.Contains(AstroBasedUtils.StrTimeAM))
                {
                    diffrence1 = new TimeSpan(0, 23 + duration1.Hours, 60 + duration1.Minutes, 0, 0);
                    diffrence2 = new TimeSpan(0, duration2.Hours, duration2.Minutes, 0, 0);
                }

                if (diffrence1 > requiredTimeStamp)
                {
                    if (diffrence2 > requiredTimeStamp)
                    {
                        return currentTime;
                    }
                    else
                    {
                        TimeSpan diffrenceNP = new TimeSpan(0, 23 + duration1.Hours, 60 + duration1.Minutes, 0, 0);
                        if (diffrenceNP >= TimeSpan.FromTicks(requiredTimeStamp.Ticks * 2))
                        {
                            return nextStamp.Subtract(requiredTimeStamp);
                        }
                        else
                        {
                            return referenceScheduleTime[time];
                        }
                    }
                }
                else
                {
                    return prevStamp.Add(requiredTimeStamp);
                }
            }

        }
        #endregion

        #region TimeBaded Reference Schedule Time Validations
        /// <summary>
        /// Validate time entry with minimal steps required based upon device and astro dim value for time based mode.
        /// </summary>
        /// <param name="requiredTimeStamp">Required Time Stamp</param>
        /// <param name="time">Time number value.</param>
        /// <returns>The validated time based upon astro dim time and minimum steps required between two time stamps.</returns>
        private DateTime ValidateTimeBaseTimeStepIncrement(TimeSpan requiredTimeStamp, TimeValue sender, int time)
        {
            var currentTime = sender[time];
            var prevStamp = sender[time - 1];
            var nextStamp = sender[time + 1];
            TimeSpan duration1 = currentTime.Subtract(prevStamp);
            TimeSpan duration2 = nextStamp.Subtract(currentTime);
            TimeSpan durationNP = nextStamp.Subtract(prevStamp);

            //special case Time 5
            if (time == 5)
            {
                if (duration1 > requiredTimeStamp)
                    return currentTime;
            }

            //general case check
            if (duration1 > requiredTimeStamp)
            {
                if (duration2 > requiredTimeStamp)
                {
                    return currentTime;
                }
                else
                {
                    TimeSpan diffrenceNP = new TimeSpan(0, durationNP.Hours, durationNP.Minutes, 0, 0);
                    if (diffrenceNP >= TimeSpan.FromTicks(requiredTimeStamp.Ticks * 2))
                    {
                        return nextStamp.Subtract(requiredTimeStamp);
                    }
                    else
                    {
                        //old value
                        return referenceScheduleTime[time];
                    }
                }
            }
            else
            {
                return prevStamp.Add(requiredTimeStamp);
            }
        }
        #endregion

        private void ValidateTimeLevels()
        {
            if (_astroDimFeature.AstroDimMode == AstroDimMode.AstroBased)
                ValidateAstroBasedTimeLevels();
            if (_astroDimFeature.AstroDimMode == AstroDimMode.TimeBased)
                ValidateTimeBasedTimeLevels();
        }

        private void UpdateOutputLevel(object sender, int e)
        {
            RaisePropertyChanged("ReferenceScheduleOutputLevel");
        }

        /// <summary>
        /// Method to update 4dim data model parameter values to UI properties.
        /// </summary>
        private void Bind4DimDataModelToUI()
        {
            if (_astroDimFeature.AstroDimMode == AstroDimMode.TimeBased)
            {
                InitialiseReferenceScheduleTimeBasedTime();
            }
            else if (_astroDimFeature.AstroDimMode == AstroDimMode.AstroBased)
            {
                InitialiseReferenceScheduleAstroBasedTime();
            }
            ReferenceScheduleOutputLevel = _astroDimFeature.DimmingLevels;
            IsTimeEnabled = true;
        }

        private OutputLevel referenceScheduleOutputLevel;
        public OutputLevel ReferenceScheduleOutputLevel
        {
            get { return referenceScheduleOutputLevel; }
            set
            {
                referenceScheduleOutputLevel = value;
                _astroDimFeature.DimmingLevels.Changed -= UpdateOutputLevel;
                _astroDimFeature.DimmingLevels.Changed += UpdateOutputLevel;
                RaisePropertyChanged(() => ReferenceScheduleOutputLevel);
            }
        }

        private TimeValue referenceScheduleTime;
        public TimeValue ReferenceScheduleTime
        {
            get { return referenceScheduleTime; }
            set
            {
                referenceScheduleTime = value;
                ReferenceScheduleTime.Changed -= UpdateTime;
                ReferenceScheduleTime.Changed += UpdateTime;
                RaisePropertyChanged(() => ReferenceScheduleTime);
            }
        }

        public OutputLevel OffRSOutputLevel => _astroDimFeature.DimmingLevelsOff;
        public OutputLevel MinRSOutputLevel => _astroDimFeature.DimmingLevelsMin;
        public OutputLevel MaxRSOutputLevel => _astroDimFeature.DimmingLevelsMax;
        public DimmingMode SelectedDimmingMode
        {
            get
            {
                if (_dimmingFeature != null)
                {
                    DimmingMode selectedDimmingMode = _dimmingFeature.ModesCollection.Find((x => x == (DimmingMode)_dimmingFeature.SelectedDimmingMode));
                    return selectedDimmingMode;
                }
                else
                {
                    return DimmingMode.None;
                }
            }
        }

        private void OnChangeAstroDimMode(Events.SelectedAstroModeEvent ev)
        {
            var selectedMode = ev.Mode;
            if (AstroBasedMode != selectedMode)
            {
                Logger.Trace("User Input: Feature = {0}, Property = {1}, SetValue = {2}", Title, "AstroBasedMode", selectedMode.ToString());

                AstroBasedMode = selectedMode;

                if (selectedMode == AstroDimMode.AstroBased)
                {
                    SetAstroBasedRSTimeToDefault();
                }
                else
                {
                    SetTimeBasedRSTimeToDefault();
                }
            }
        }

        /// <summary>
        /// Methods to  set reference schedule time to default in Astro based mode.
        /// Handles resetting the time values to default when the mode is changed from time based to astro based.
        /// </summary>
        private void SetAstroBasedRSTimeToDefault()
        {
            if (ReferenceScheduleTime == null)
                InitialiseReferenceScheduleAstroBasedTime();

            var timeDefault = _astroDimFeature.GetDefaultReferenceScheduleTime();
            ReferenceScheduleTime = timeDefault;
            RaisePropertyChanged(() => ReferenceScheduleTime);
        }

        public double PhysicalMinValue
        {
            get
            {
                if (_operatingCurrentFeature != null)
                {
                    return _operatingCurrentFeature.PhysicalMinLevel;
                }
                else
                {
                    return 0;
                }
            }
        }

        public AstroDimMode AstroBasedMode
        {
            get { return _astroDimFeature.AstroDimMode; ; }
            set
            {
                _astroDimFeature.AstroDimMode = value;
                RaisePropertyChanged(() => AstroBasedMode);
            }
        }

        /// <summary>
        /// Gets or sets the maximum dimming range value.
        /// </summary>
        /// <value>
        /// The maximum dimming range.
        /// </value>
        public int MaxDimmingRange
        {
            get { return _astroDimFeature.MaxDimmingRange; }
        }

        /// <summary>
        /// Gets or sets the minimum dimming range value.
        /// </summary>
        /// <value>
        /// The minimum dimming range.
        /// </value>
        public int MinDimmingRange
        {
            get { return _astroDimFeature.MinDimmingRange; }
        }

        /// <summary>
        /// Methods to validate each time level in time based mode on Astro Dim fade time value change.
        /// </summary>
        private void ValidateTimeBasedTimeLevels()
        {
            TimeSpan astroTimeStamp = _astroDimFeature.MinimumDifferencesInTime;
            DateTime t2 = ReferenceScheduleTime.Element2;
            DateTime t3 = ReferenceScheduleTime.Element3;
            DateTime t4 = ReferenceScheduleTime.Element4;
            DateTime t5 = ReferenceScheduleTime.Element5;

            DateTime newT3 = GetValidTime(astroTimeStamp, t2, t3);
            DateTime newT4 = GetValidTime(astroTimeStamp, newT3, t4);
            DateTime newT5 = GetValidTime(astroTimeStamp, newT4, t5);

            if (newT5.Date != DateTime.Now.Date && newT5.ToString("tt", CultureInfo.InvariantCulture).Contains("AM"))
            {
                string pretimeLevelTB = "23:59";
                TimeSpan difference = newT5.Subtract(DateTime.Parse(pretimeLevelTB));
                newT5 = newT5.Subtract(difference);

                TimeSpan duration45 = newT5.Subtract(newT4);
                if (duration45 < astroTimeStamp)
                {
                    if (duration45.Minutes > 0)
                    {
                        newT4 = newT4.Subtract(difference);
                    }
                    else
                    {
                        newT4 = newT5.Subtract(astroTimeStamp);
                    }
                }

                TimeSpan duration34 = newT4.Subtract(newT3);
                if (duration34 < astroTimeStamp)
                {
                    if (duration34.Minutes > 0)
                    {
                        newT3 = newT3.Subtract(difference);
                    }
                    else
                    {
                        newT3 = newT4.Subtract(astroTimeStamp);
                    }
                }

                TimeSpan duration23 = newT3.Subtract(t2);
                if (duration23 < astroTimeStamp)
                {
                    if (duration23.Minutes > 0)
                    {
                        t2 = newT3.Subtract(difference);
                    }
                    else
                    {
                        t2 = newT3.Subtract(astroTimeStamp);
                    }
                }
            }

            ReferenceScheduleTime.Changed -= UpdateTime;
            ReferenceScheduleTime.Element2 = t2;
            ReferenceScheduleTime.Element3 = newT3;
            ReferenceScheduleTime.Element4 = newT4;
            ReferenceScheduleTime.Element5 = newT5;
            ReferenceScheduleTime.Changed += UpdateTime;

            _astroDimFeature.UpdateDimDurationToDataModel(ReferenceScheduleTime);
        }

        /// <summary>
        /// Methods to validate each time level on Astro Dim fade time value change in Astro based mode.
        /// </summary>
        private void ValidateAstroBasedTimeLevels()
        {
            TimeSpan astroTimeStamp = _astroDimFeature.MinimumDifferencesInTime;

            DateTime t2 = ReferenceScheduleTime.Element2;
            DateTime t3 = ReferenceScheduleTime.Element3;
            DateTime t4 = ReferenceScheduleTime.Element4;
            DateTime t5 = ReferenceScheduleTime.Element5;

            DateTime newT3 = GetValidTime(astroTimeStamp, t2, t3);
            DateTime newT4 = GetValidTime(astroTimeStamp, newT3, t4);
            DateTime newT5 = GetValidTime(astroTimeStamp, newT4, t5);

            if (newT5.Date != DateTime.Now.Date && newT5.ToString("tt", CultureInfo.InvariantCulture).Contains(AstroBasedUtils.StrTimePM))
            {
                string pretimeLevelTB = "11:59";
                TimeSpan difference = newT5.Subtract(DateTime.Parse(pretimeLevelTB).AddDays(1));
                newT5 = newT5.Subtract(difference);

                TimeSpan duration45 = newT5.Subtract(newT4);
                if (duration45 < astroTimeStamp)
                {
                    if (duration45.Minutes > 0)
                    {
                        newT4 = newT4.Subtract(difference);
                    }
                    else
                    {
                        newT4 = newT5.Subtract(astroTimeStamp);
                    }
                }

                TimeSpan duration34 = newT4.Subtract(newT3);
                if (duration34 < astroTimeStamp)
                {
                    if (duration34.Minutes > 0)
                    {
                        newT3 = newT3.Subtract(difference);
                    }
                    else
                    {
                        newT3 = newT4.Subtract(astroTimeStamp);
                    }
                }

                TimeSpan duration23 = newT3.Subtract(t2);
                if (duration23 < astroTimeStamp)
                {
                    if (duration23.Minutes > 0)
                    {
                        t2 = newT3.Subtract(difference);
                    }
                    else
                    {
                        t2 = newT3.Subtract(astroTimeStamp);
                    }
                }
            }

            ReferenceScheduleTime.Changed -= UpdateTime;
            ReferenceScheduleTime.Element2 = t2;
            ReferenceScheduleTime.Element3 = newT3;
            ReferenceScheduleTime.Element4 = newT4;
            ReferenceScheduleTime.Element5 = newT5;
            ReferenceScheduleTime.Changed += UpdateTime;

            _astroDimFeature.UpdateDimDurationToDataModel(ReferenceScheduleTime);       
        }

        /// <summary>
        /// Validate each time entry separately based on astro dim fade time value change.
        /// </summary>=
        /// <param name="astroTimeStamp">Astro dim selected value.</param>
        /// <param name="prevTimeLevel">Previous textbox time.</param>
        /// <param name="currentTimeLevel">Current time.</param>
        /// <returns>Valid time.</returns>
        private DateTime GetValidTime(TimeSpan astroTimeStamp, DateTime prevTimeLevel, DateTime currentTimeLevel)
        {
            TimeSpan duration = currentTimeLevel.Subtract(prevTimeLevel);

            if (duration.Minutes < 0)
            {
                return prevTimeLevel.Add(astroTimeStamp);
            }

            if (duration > astroTimeStamp)
            {
                return currentTimeLevel;
            }
            else
            {
                return prevTimeLevel.Add(astroTimeStamp);
            }
        }

        public ICommand SetTimeCommand { get; set; }
        public ICommand ValidateOutputValue1 { get; set; }
        public ICommand ValidateOutputValue2 { get; set; }
        public ICommand ValidateOutputValue3 { get; set; }
        public ICommand ValidateOutputValue4 { get; set; }
        public ICommand ValidateOutputValue5 { get; set; }

        private bool isTimeEnabled { get; set; }
        public bool IsTimeEnabled
        {
            get
            {
               return (isTimeEnabled && IsEditable);
            }
            set
            {
                isTimeEnabled=value;
                RaisePropertyChanged(() => IsTimeEnabled);
            }
        }

        private void OnSetTimeCommand(int itemSelected)
        {
            if (!IsTimeEnabled)
                return;

            var timeValuesCopy = new TimeValue();
            ReferenceScheduleTime.CopyTo(timeValuesCopy);

            timeValuesCopy.Changed -= UpdateTime;
            timeValuesCopy.Changed += UpdateTime;

            // DimmingTimeViewModel
            MyShowViewModel<DimmingTimeViewModel>(RequestType.Popup, new SetDimmingTimeParameter(true)
            {
                TimeItemSelected = itemSelected,
                TimeValues = timeValuesCopy,
                DefaultValue = DateTime.Now
            });
        }

        private void OnValidateOutputValue(int outputLevel)
        {
            //ReferenceScheduleOutputLevel[outputLevel] = (int)_astroDimFeature.ValidateRangeOutputLevel(outputLevel);
            Task.Factory.StartNew(() =>
            {
                ReferenceScheduleOutputLevel[outputLevel] = (int)_astroDimFeature.ValidateRangeOutputLevel(outputLevel);
            });
        }

        private void OnValidateOutputValue1()
        {
            OnValidateOutputValue(1);
        }
        private void OnValidateOutputValue2()
        {
            OnValidateOutputValue(2);
        }
        private void OnValidateOutputValue3()
        {
            OnValidateOutputValue(3);
        }
        private void OnValidateOutputValue4()
        {
            OnValidateOutputValue(4);
        }
        private void OnValidateOutputValue5()
        {
            OnValidateOutputValue(5);
        }

        private void OnChangeAstroLocation(Events.SelectedAstroLocationEvent ev)
        {
            SetAstroBasedRSTimeToDefault();
        }

        private void OnChangeOperatingMode(Events.SelectedOperatingModeEvent ev)
        {
            OperatingModeEnum = ev.Mode;

            if (ev.Mode == OperatingModeEnum.OnOff || ev.Mode == OperatingModeEnum.None)
            {
                IsEnabled = false;
                IsTimeEnabled = false;
            }
            else
            {
                IsEnabled = true;
                IsTimeEnabled = true;
            }
        }

        public class SetDimmingTimeParameter : BaseViewModelParameter
        {
            public SetDimmingTimeParameter(bool isModal)
                : base(isModal)
            {
            }

            public int TimeItemSelected { get; set; }
            public TimeValue TimeValues { get; set; }
            public DateTime DefaultValue { get; set; }
        }
    }

}
