﻿using MvvmCross.Commands;
using Osram.TFTBackend.DataModel.Data.Contracts;
using Osram.TFTBackend.DataModel.Feature.Contracts;
using Rg.Plugins.Popup.Services;
using System;
using System.Windows.Input;
using Xamarin.Forms;

namespace Osram.T4TField.ViewModels
{
    public class TuningFactorPopupViewModel : BaseViewModel
    {
        private readonly ITuningFactorFeature _tuningFactorFeature;
        private readonly IOperatingCurrentData _operatingCurrentFeature;

        private Action<int> _setValue;

        public ICommand ConfirmValueCommand { get; set; }
        public ICommand CloseValueCommand { get; set; }

        public Layout ValueContainer { get; set; }

        private int _value;
        public int Value {
            get {
                return _value;
            }
            set {
                _value = value;
                if (MinValue != null && _value < MinValue)
                {
                    IsButtonEnabled = false;
                    return;
                }
                if (MaxValue != null && _value > MaxValue)
                {
                    IsButtonEnabled = false;
                    return;
                }

                IsButtonEnabled = true;
            }
        }
        public int? MinValue { get; set; }
        public int? MaxValue { get; set; }

        public TuningFactorPopupViewModel(ITuningFactorFeature tuningFactorFeature, IOperatingCurrentData operatingCurrentFeature)
        {
            _tuningFactorFeature = tuningFactorFeature;
            _operatingCurrentFeature = operatingCurrentFeature;

            ConfirmValueCommand = new MvxCommand(OnConfirmValueCommand);
            CloseValueCommand = new MvxCommand(OnCancelValueCommand);
        }

        private void OnCancelValueCommand()
        {
            PopupNavigation.PopAsync();
        }

        private void OnConfirmValueCommand()
        {
            _setValue.Invoke(Value);
            PopupNavigation.PopAsync();
        }

        private void SetOutputValue()
        {
            (ClassParameter as TuningFactorViewModel.SetTuningFactorPopupParameter).Value = Value;
        }

        public override void Start()
        {
            base.Start();

            Value = (int)(ClassParameter as TuningFactorViewModel.SetTuningFactorPopupParameter)?.Value;
            MinValue = (ClassParameter as TuningFactorViewModel.SetTuningFactorPopupParameter)?.MinValue;
            MaxValue = (ClassParameter as TuningFactorViewModel.SetTuningFactorPopupParameter)?.MaxValue;
            _setValue = (ClassParameter as TuningFactorViewModel.SetTuningFactorPopupParameter)?.SetValue;
        }

        private bool isButtonEnabled;

        public bool IsButtonEnabled {
            get { return isButtonEnabled; }
            set {
                isButtonEnabled = value;
                RaisePropertyChanged();
            }
        }
    }
}
