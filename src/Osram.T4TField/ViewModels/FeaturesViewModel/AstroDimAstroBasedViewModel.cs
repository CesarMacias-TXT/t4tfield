﻿using MvvmCross.Commands;
using MvvmCross.Plugin.Messenger;
using Osram.T4TField.Resources;
using Osram.T4TField.Utils;
using Osram.TFTBackend;
using Osram.TFTBackend.DataModel.Feature.Contracts;
using Osram.TFTBackend.LocationService;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Input;
using static Osram.T4TField.ViewModels.FeaturesViewModel.AstroDimViewModel;
using System.Globalization;
using Osram.TFTBackend.DataModel.Helper;
using Osram.TFTBackend.DataModel.Data.Contracts;
using Osram.TFTBackend.KeyService.Contracts;

namespace Osram.T4TField.ViewModels.FeaturesViewModel
{
    public class AstroDimAstroBasedViewModel : BaseFeatureViewModel
    {
        private IAstroDimFeature _astroDimFeature;
        private IDimmingModeData _dimmingFeature;
        private IOperatingCurrentData _operatingCurrentFeature;

        /// <summary>
        /// The lock location change value.
        /// </summary>
        private bool lockLocationChange = false;
        private int loopCount;

        MvxSubscriptionToken _astroLocationToken;
        MvxSubscriptionToken _astroDimUpdateToken;

        public AstroDimAstroBasedViewModel(IAstroDimFeature astroDimFeature, IDimmingModeData dimmingModeFeature, IOperatingCurrentData operatingCurrentFeature, IKeyService keyService) :base(keyService)
        {
            Title = AppResources.F_NavigationDimming;

            IFeature currentFeature = ((IFeature)astroDimFeature);
            IsEnabled = currentFeature.Info.IsEnabled;
            Lock = currentFeature.Info.Lock;

            _astroDimFeature = astroDimFeature;
            _dimmingFeature = dimmingModeFeature;
            _operatingCurrentFeature = operatingCurrentFeature;
            ScheduleModeSelectedIndex = 0;
            ScheduleTime = new TimeValue();
            ScheduleOutputLevel.Changed -= UpdateEnergySaving;
            ScheduleOutputLevel.Changed += UpdateEnergySaving;
            IsTimeEnabled = false;
            InitialiseCommands();
            SubscribeToMessages();
        }

        public override Task Cleanup()
        {
            UnsubscribeToMessages();
            return base.Cleanup();
        }

        private void SubscribeToMessages()
        {
            _astroLocationToken = _messenger.Subscribe<Messaging.Events.SelectedAstroLocationEvent>(OnChangeAstroLocation);
            _astroDimUpdateToken = _messenger.Subscribe<TFTBackend.Messaging.Events.AstroDimFeatureChangedEvent>(OnAstroDimUpdate);
        }

        private void UnsubscribeToMessages()
        {
            _astroLocationToken.Dispose();
            _astroDimUpdateToken.Dispose();
        }

        public ICommand SetTimeCommand { get; set; }
        private void InitialiseCommands()
        {
            SetTimeCommand = new MvxCommand<int>(OnSetTimeCommand);
        }

        private void OnSetTimeCommand(int itemSelected)
        {
            if (((itemSelected == 1 || itemSelected == 6) && IsTime1and6Enabled) ||
                    (itemSelected != 1 && itemSelected != 6 && IsTimeEnabled))
            {
                //create a copy in order preserve the old values
                var timeValuesCopy = new TimeValue();
                ScheduleTime.CopyTo(timeValuesCopy);

                timeValuesCopy.Changed -= UpdateTime;
                timeValuesCopy.Changed += UpdateTime;

                // DimmingTimeViewModel
                MyShowViewModel<DimmingTimeViewModel>(RequestType.Popup, new SetDimmingTimeParameter(true)
                {
                    TimeItemSelected = itemSelected,
                    TimeValues = timeValuesCopy,
                    DefaultValue = DefaultSunSetValue
                });
            }
        }

        private void OnChangeAstroLocation(Messaging.Events.SelectedAstroLocationEvent obj)
        {
            UpdateScheduleValues();
            UpdateEnergySaving(this, 0);
        }

        private void OnAstroDimUpdate(TFTBackend.Messaging.Events.AstroDimFeatureChangedEvent obj)
        {
            UpdateScheduleValues();
        }

        private void UpdateEnergySaving(object sender, int e)
        {
            RaisePropertyChanged("CalculateDayLightSaving");
        }

        public void UpdateScheduleValues()
        {
            CalculateSchedulerValues();
            if (_astroDimFeature.AstroDimMode == AstroDimMode.AstroBased && !IsManualMode)
            {
                SetSchedulerValue(AstroBasedDate.Day, AstroBasedDate.Month);
            }
            else if (_astroDimFeature.AstroDimMode != AstroDimMode.AstroBased)
            {
                SetSchedulerValue(AstroBasedDate.Day, AstroBasedDate.Month);
            }
            RaisePropertyChanged("CalculateDayLightSaving");
        }

        private DateTime astroBasedDate = new DateTime(DateTime.Now.Year, 12, 21);
        public DateTime AstroBasedDate
        {
            get { return astroBasedDate; }
            set
            {
                astroBasedDate = value;
                SetSchedulerValue(astroBasedDate.Day, astroBasedDate.Month);
            }
        }

        private TimeValue scheduleTime;
        public TimeValue ScheduleTime
        {
            get { return scheduleTime; }
            set
            {
                scheduleTime = value;
                ScheduleTime.Changed -= UpdateTime;
                ScheduleTime.Changed += UpdateTime;
                RaisePropertyChanged(() => ScheduleTime);
            }
        }

        public bool isTimeEnabled { get; set; }
        public bool IsTimeEnabled {
            get {
                return (isTimeEnabled && IsEditable);
            }
            set {
                isTimeEnabled = value;
            }
        }

        public bool isTime1and6Enabled { get; set; }
        public bool IsTime1and6Enabled
        {
            get
            {
                return (isTime1and6Enabled && IsEditable);
            }
            set
            {
                isTime1and6Enabled = value;
            }
        }
     
        private void UpdateTime(object sender, int timeNum)
        {
            ScheduleTime.Changed -= UpdateTime;
            ScheduleTime[timeNum] = ValidateScheduleTime((TimeValue)sender, timeNum);
            CalculateSchedulerValues();
            RaisePropertyChanged("ScheduleTime");
            ScheduleTime.Changed += UpdateTime;
        }

        private DateTime ValidateScheduleTime(TimeValue value, int timeNum)
        {
            if (timeNum == 1)
            {
                if (value[timeNum].Day == ScheduleTime.Element6.Day)
                {
                    TimeSpan sunRiseValue = new TimeSpan(ScheduleTime.Element6.Hour, ScheduleTime.Element6.Minute, ScheduleTime.Element6.Second);
                    sunRiseValue = sunRiseValue.Subtract(new TimeSpan(00, 05, 00));
                    TimeSpan changedValue = new TimeSpan(value[timeNum].Hour, value[timeNum].Minute, value[timeNum].Second);
                    if (sunRiseValue <= changedValue)
                    {
                        //old value
                        return scheduleTime[timeNum];
                    }
                    else
                    {
                        return value[timeNum];
                    }
                }
                else
                {
                    TimeSpan minSunSetValue = new TimeSpan(12, 00, 00);
                    TimeSpan changedValue = new TimeSpan(value[timeNum].Hour, value[timeNum].Minute, value[timeNum].Second);
                    if (changedValue < minSunSetValue)
                    {
                        return scheduleTime[timeNum];
                    }
                    else
                    {
                        return value[timeNum];
                    }
                }
            }

            if (timeNum == 6)
            {
                if (value[timeNum].Day == ScheduleTime.Element1.Day)
                {
                    TimeSpan sunSetValue = new TimeSpan(ScheduleTime.Element1.Hour, ScheduleTime.Element1.Minute, ScheduleTime.Element1.Second);
                    sunSetValue = sunSetValue.Add(new TimeSpan(00, 05, 00));
                    TimeSpan changedValue = new TimeSpan(value[timeNum].Hour, value[timeNum].Minute, value[timeNum].Second);
                    if (sunSetValue >= changedValue)
                    {
                        return scheduleTime[timeNum];
                    }
                    else
                    {
                        return value[timeNum];
                    }
                }
                else
                {
                    TimeSpan maxSunRiseValue = new TimeSpan(11, 59, 00);
                    TimeSpan changedValue = new TimeSpan(value[timeNum].Hour, value[timeNum].Minute, value[timeNum].Second);
                    if (changedValue > maxSunRiseValue)
                    {
                        return scheduleTime[timeNum];
                    }
                    else
                    {
                        return value[timeNum];
                    }
                }
            }

            return value[timeNum];
        }

        /// <summary>
        /// Gets or sets default sun rise value.
        /// </summary>
        /// <value>Gets or Sets the Default Sun Rise Value.</value>
        public DateTime DefaultSunRiseValue { get; set; }

        /// <summary>
        /// Gets or sets default Sunset Value.
        /// </summary>
        /// <value>Gets or Sets Default sunset Value.</value>
        public DateTime DefaultSunSetValue { get; set; }

        private bool isDaylightSavingEnabled = false;
        public bool IsDaylightSavingEnabled
        {
            get { return (isDaylightSavingEnabled && IsEditable); }
            set
            {
                isDaylightSavingEnabled = value;
                UpdateScheduleValues();
                RaisePropertyChanged(() => IsDaylightSavingEnabled);
            }
        }

        public string CalculateDayLightSaving
        {
            get
            {
                string messageText = string.Format(AppResources.F_AstroDimAstroBasedEnergySaving, CalculateSaving());
                return messageText;
            }
        }

        public bool isManualMode { get; set; }
        public bool IsManualMode
        {
            get
            {
               return (isManualMode);
            }
            set
            {
                isManualMode = value;
                RaisePropertyChanged("IsManualMode");
                RaisePropertyChanged("IsToBeHidden");
            }
        }

        public bool isToBeHidden { get; set; }
        public bool IsToBeHidden
        {
            get
            {
                var not_IsManualMode = !IsManualMode;
                return (not_IsManualMode && IsEditable);
            }
            set
            {
                isToBeHidden = value;
            }
        }

        private List<AstroSchedules> schedulerList;
        /// <summary>
        /// Gets or sets the scheduler list.
        /// </summary>
        /// <value>
        /// The scheduler list.
        /// </value>
        public List<AstroSchedules> SchedulerList
        {
            get
            {
                if (schedulerList == null)
                {
                    schedulerList = new List<AstroSchedules>();
                }

                return schedulerList;
            }

            set
            {
                schedulerList = value;
                RaisePropertyChanged(() => SchedulerList);
            }
        }

        public List<ScheduleMode> ScheduleModeList { get; set; } = new List<ScheduleMode>() { new ScheduleMode(AppResources.F_AstroDimSunriseSunset, ScheduleType.Sunrise_Sunset), new ScheduleMode(AppResources.F_AstroDimManual, ScheduleType.Manual) };

        public int ScheduleModeSelectedIndex { get; set; }

        private ScheduleMode scheduleModeSelectedItem;
        public ScheduleMode ScheduleModeSelectedItem
        {
            get { return scheduleModeSelectedItem; }
            set
            {
                scheduleModeSelectedItem = value;
                if (scheduleModeSelectedItem.SType == ScheduleType.Manual)
                {
                    SetManualScheduleTime();
                }
                else
                {
                    SetSunsetSunriseScheduleTime();
                }
                RaisePropertyChanged("IsManualMode");
            }
        }

        private void SetSunsetSunriseScheduleTime()
        {
            Logger.Trace(string.Format("User Input: Feature = {0}, Property = {1}, SetValue = {2}", Title, "IsManual", false));
            IsManualMode = false;
            CalculateSchedulerValues();
            SetSchedulerValue(AstroBasedDate.Day, AstroBasedDate.Month);
            IsTime1and6Enabled = IsManualMode;
            RaisePropertyChanged("IsTime1and6Enabled");
        }

        private void SetManualScheduleTime()
        {
            Logger.Trace(string.Format("User Input: Feature = {0}, Property = {1}, SetValue = {2}", Title, "IsManual", true));
            IsManualMode = true;
            loopCount = 0;
            DateTime d = new DateTime(DateTime.Now.Year, AstroBasedDate.Month, AstroBasedDate.Day);
            DateTime sunrise, sunset;
            SetSunLocation(d, out sunset, out sunrise);
            scheduleTime.Changed -= UpdateTime;
            DefaultSunSetValue = scheduleTime.Element1 = sunset;
            DefaultSunRiseValue = scheduleTime.Element6 = sunrise;
            scheduleTime.Changed += UpdateTime;
            CalculateSchedulerValues();
            IsTime1and6Enabled = IsManualMode;
            RaisePropertyChanged("IsTime1and6Enabled");
            RaisePropertyChanged("ScheduleTime");
        }

        /// <summary>
        /// Calculates the scheduler group values based on dim length,dim shift etc.
        /// </summary>
        private void CalculateSchedulerValues()
        {
            try
            {
                lockLocationChange = true;
                var daylightSavingLowerBound = new DateTime(DateTime.Now.Year, 3, 31, 00, 00, 00);
                var daylightSavingUpperBound = new DateTime(DateTime.Now.Year, 10, 26, 00, 00, 00);

                var dtscheduleTimeSR1 = new DateTime();
                var dtscheduleTimeSR6 = new DateTime();
                DateTime dtscheduleTimeSR2 = DateTime.MinValue;
                DateTime dtscheduleTimeSR3 = DateTime.MinValue;
                DateTime dtscheduleTimeSR4 = DateTime.MinValue;
                DateTime dtscheduleTimeSR5 = DateTime.MinValue;

                var duration23 = new TimeSpan();
                var duration34 = new TimeSpan();
                var duration45 = new TimeSpan();

                TimeValue rsTime = _astroDimFeature.GetAstroBasedReferenceScheduleTime();
                duration23 = rsTime.Element3.Subtract(rsTime.Element2);

                duration34 = rsTime.Element4.Subtract(rsTime.Element3);

                duration45 = rsTime.Element5.Subtract(rsTime.Element4);

                var dimShift1 = new TimeSpan();
                var dimShift2 = new TimeSpan();
                var dimlength1 = new TimeSpan();
                var dimlength2 = new TimeSpan();


                if (scheduleModeSelectedItem.SType == ScheduleType.Manual && _astroDimFeature.AstroDimMode == AstroDimMode.AstroBased)
                {
                    var d = new DateTime(DateTime.Now.Year, AstroBasedDate.Month, AstroBasedDate.Day);

                    var fullSpan = scheduleTime.Element6.Subtract(scheduleTime.Element1);
                    var virtualMidnight = scheduleTime.Element1.Add(new TimeSpan(fullSpan.Ticks / 2));
                    var dimStartTimeSpan = new TimeSpan();

                    dimStartTimeSpan = new TimeSpan(0, _astroDimFeature.DimStartTime, 0);


                    AstroBasedUtils.CalculateScheduleValues(d, true, scheduleTime.Element1, scheduleTime.Element6, dimlength1, dimlength2, dimShift1, dimShift2, virtualMidnight, duration23, duration34, duration45, dimStartTimeSpan,
                        out dtscheduleTimeSR2, out dtscheduleTimeSR3, out dtscheduleTimeSR4, out dtscheduleTimeSR5);


                    scheduleTime.Changed -= UpdateTime;
                    scheduleTime.Element2 = dtscheduleTimeSR2;
                    scheduleTime.Element3 = dtscheduleTimeSR3;
                    scheduleTime.Element4 = dtscheduleTimeSR4;
                    scheduleTime.Element5 = dtscheduleTimeSR5;
                    scheduleTime.Changed += UpdateTime;

                    ValidateScheduleOutputValues(new AstroSchedules(d, d.DayToDouble(), 12, scheduleTime.Element1, scheduleTime.Element6, scheduleTime.Element2, scheduleTime.Element3, scheduleTime.Element4, scheduleTime.Element5, false));
                    loopCount++;
                }
                else
                {
                    schedulerList = new List<AstroSchedules>();
                    foreach (DateTime d in AstroBasedUtils.GetDateListOfYear(DateTime.Now.Year))
                    {
                        SetSunLocation(d, out dtscheduleTimeSR1, out dtscheduleTimeSR6);

                        TimeSpan fullSpan = dtscheduleTimeSR6.Subtract(dtscheduleTimeSR1);
                        DateTime virtualMidnight = dtscheduleTimeSR1.Add(new TimeSpan(fullSpan.Ticks / 2));
                        TimeSpan dimStartTimeSpan = new TimeSpan();

                        dimStartTimeSpan = new TimeSpan(0, _astroDimFeature.DimStartTime, 0);


                        AstroBasedUtils.CalculateScheduleValues(d, true, dtscheduleTimeSR1, dtscheduleTimeSR6, dimlength1, dimlength2, dimShift1, dimShift2, virtualMidnight, duration23, duration34, duration45, dimStartTimeSpan,
                            out dtscheduleTimeSR2, out dtscheduleTimeSR3, out dtscheduleTimeSR4, out dtscheduleTimeSR5);

                        if (IsDaylightSavingEnabled)
                        {
                            if (_astroDimFeature.Location.LatitudeValue < 0 && (d.CompareTo(daylightSavingLowerBound) < 0 || daylightSavingUpperBound.CompareTo(d) < 0))
                            {
                                dtscheduleTimeSR1 = dtscheduleTimeSR1.AddHours(1);
                                dtscheduleTimeSR2 = dtscheduleTimeSR2.AddHours(1);
                                dtscheduleTimeSR3 = dtscheduleTimeSR3.AddHours(1);
                                dtscheduleTimeSR4 = dtscheduleTimeSR4.AddHours(1);
                                dtscheduleTimeSR5 = dtscheduleTimeSR5.AddHours(1);
                                dtscheduleTimeSR6 = dtscheduleTimeSR6.AddHours(1);
                            }
                            else if (_astroDimFeature.Location.LatitudeValue >= 0 && (d.CompareTo(daylightSavingLowerBound) > 0 && daylightSavingUpperBound.CompareTo(d) > 0))
                            {
                                dtscheduleTimeSR1 = dtscheduleTimeSR1.AddHours(1);
                                dtscheduleTimeSR2 = dtscheduleTimeSR2.AddHours(1);
                                dtscheduleTimeSR3 = dtscheduleTimeSR3.AddHours(1);
                                dtscheduleTimeSR4 = dtscheduleTimeSR4.AddHours(1);
                                dtscheduleTimeSR5 = dtscheduleTimeSR5.AddHours(1);
                                dtscheduleTimeSR6 = dtscheduleTimeSR6.AddHours(1);
                            }
                        }

                        double midLine = (dtscheduleTimeSR1.ADTimeToDouble() + dtscheduleTimeSR6.ADTimeToDouble()) / 2;
                        schedulerList.Add(new AstroSchedules(d, d.DayToDouble(), midLine, dtscheduleTimeSR1, dtscheduleTimeSR6, dtscheduleTimeSR2, dtscheduleTimeSR3, dtscheduleTimeSR4, dtscheduleTimeSR5, false));
                    }
                }

                RaisePropertyChanged("ScheduleTime");
            }
            catch
            {
            }

            lockLocationChange = false;
        }

        /// <summary>
        /// Methods to set schedule group values.
        /// </summary>
        /// <param name="day">selected day</param>
        /// <param name="month">selected month</param>
        private void SetSchedulerValue(int day, int month)
        {
            if (!lockLocationChange)
            {
                try
                {
                    if (IsManualMode && loopCount > 0)
                    {
                        DateTime d = new DateTime(DateTime.Now.Year, AstroBasedDate.Month, AstroBasedDate.Day, 0, 0, 0);
                        ValidateScheduleOutputValues(new AstroSchedules(d, d.DayToDouble(), 12, ScheduleTime.Element1, ScheduleTime.Element6, ScheduleTime.Element2,
                            ScheduleTime.Element3, ScheduleTime.Element4, ScheduleTime.Element5, false));
                    }
                    else
                    {
                        if (day > 0 && month > 0)
                        {
                            DateTime selectedDate = new DateTime(DateTime.Now.Year, month, day);
                            if (SchedulerList.Count > 0)
                            {
                                lock (schedulerList)
                                {
                                    AstroSchedules tmpValue = schedulerList.Where(x => x.AstroDate == selectedDate).Select(x => x).FirstOrDefault();
                                    if (tmpValue != null)
                                    {
                                        DefaultSunSetValue = tmpValue.DTScheduleSunSet;

                                        ScheduleTime.Changed -= UpdateTime;
                                        ScheduleTime.Element1 = tmpValue.DTScheduleSunSet;
                                        ScheduleTime.Element2 = tmpValue.DTScheduleTimeSR2;
                                        ScheduleTime.Element3 = tmpValue.DTScheduleTimeSR3;
                                        ScheduleTime.Element4 = tmpValue.DTScheduleTimeSR4;
                                        ScheduleTime.Element5 = tmpValue.DTScheduleTimeSR5;
                                        ScheduleTime.Element6 = tmpValue.DTScheduleSunRise;
                                        ScheduleTime.Changed += UpdateTime;
                                    }

                                    RaisePropertyChanged("ScheduleTime");

                                    ValidateScheduleOutputValues(tmpValue);
                                }
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    Logger.Exception("AstroDim SetSchedulerValue", ex);
                }
            }
        }

        public OutputLevel ScheduleOutputLevel => _astroDimFeature.DimmingLevels;

        public bool barsEnabled { get; set; }
        public bool BarsEnabled {
            get {
                return (barsEnabled && IsEditable);
            }set {
                barsEnabled = value;
            }
        }

        /// <summary>
        /// Methods to set sun location.
        /// </summary>
        /// <param name="dateTime">Schedule Date.</param>
        /// <param name="sunSet">SunSet Value.</param>
        /// <param name="sunRise">SunRise Value.</param>
        public void SetSunLocation(DateTime dateTime, out DateTime sunSet, out DateTime sunRise)
        {
            SunDawn sunlocationCurrentDay;
            SunDawn sunlocationNextDay;
            sunSet = new DateTime();
            sunRise = new DateTime();
            AstroBasedUtils.GetSunLocation(dateTime, out sunlocationCurrentDay, out sunlocationNextDay, _astroDimFeature.Location.Offset, _astroDimFeature.Location.LatitudeValue, _astroDimFeature.Location.LongitudeValue);
            if (sunlocationCurrentDay != null && sunlocationNextDay != null)
            {
                sunSet = AstroBasedUtils.DoubleTimeToDate(dateTime, sunlocationCurrentDay.Set);
                dateTime = dateTime.AddDays(1);
                sunRise = AstroBasedUtils.DoubleTimeToDate(dateTime, sunlocationCurrentDay.Rise);
            }
        }

        /// <summary>
        /// Calculates day-light saving based on reference schedule output values.
        /// </summary>
        /// <returns>Saving value.</returns>
        private decimal CalculateSaving()
        {
            decimal result = 0;

            if (SchedulerList != null && SchedulerList.Count > 0)
            {
                result = Convert.ToDecimal(CalculateSavings(ScheduleOutputLevel.Element1, ScheduleOutputLevel.Element2, ScheduleOutputLevel.Element3, ScheduleOutputLevel.Element4, ScheduleOutputLevel.Element5));
            }

            return Math.Truncate(Math.Round(result, 2) * 100 / 365);
        }
        /// <summary>
        /// Calculate energy savings based on dimlevels.
        /// </summary>
        /// <param name="onLevel">Output level 1.</param>
        /// <param name="dimLevel1">Output level 2.</param>
        /// <param name="dimLevel2">Output level 3.</param>
        /// <param name="dimLevel3">Output level 4.</param>
        /// <param name="dimLevel4">Output level 5.</param>
        /// <returns>Sum of saving result value of all days in year.</returns>
        private double CalculateSavings(double onLevel, double dimLevel1, double dimLevel2, double dimLevel3, double dimLevel4)
        {
            double result = 0;
            if (dimLevel4 == dimLevel1 && onLevel == dimLevel1 && dimLevel1 == dimLevel2 && dimLevel2 == dimLevel3 && dimLevel4 == dimLevel3)
            {
                return (366 * (100 - onLevel)) / 100;
            }

            for (int i = 0; i < SchedulerList.Count; i++)
            {
                DateTime t2 = SchedulerList[i].DTScheduleTimeSR2;
                DateTime t3 = SchedulerList[i].DTScheduleTimeSR3;
                DateTime t4 = SchedulerList[i].DTScheduleTimeSR4;
                DateTime t5 = SchedulerList[i].DTScheduleTimeSR5;
                DateTime t1 = SchedulerList[i].DTScheduleSunSet;
                DateTime t6 = SchedulerList[i].DTScheduleSunRise;

                if (SchedulerList[i].DTScheduleSunSet >= SchedulerList[i].DTScheduleTimeSR2) //if sunset is after T2: if (sunset > T2) then T2 = sunset
                {
                    t2 = SchedulerList[i].DTScheduleSunSet;
                }

                if (SchedulerList[i].DTScheduleSunSet >= SchedulerList[i].DTScheduleTimeSR3) //if sunset is after T2: if (sunset > T3) then T3 = sunset
                {
                    t3 = SchedulerList[i].DTScheduleSunSet;
                }

                if (SchedulerList[i].DTScheduleSunSet >= SchedulerList[i].DTScheduleTimeSR4) //if sunset is after T2: if (sunset > T4) then T4 = sunset
                {
                    t4 = SchedulerList[i].DTScheduleSunSet;
                }

                if (SchedulerList[i].DTScheduleSunSet >= SchedulerList[i].DTScheduleTimeSR5) //if sunset is after T2: if (sunset > T5) then T5 = sunset
                {
                    t5 = SchedulerList[i].DTScheduleSunSet;
                }

                if (SchedulerList[i].DTScheduleSunRise <= SchedulerList[i].DTScheduleTimeSR5) //if sunset is after T2: if (sunrise < T5 ) then T5 = sunrise
                {
                    t5 = SchedulerList[i].DTScheduleSunRise;
                }

                if (SchedulerList[i].DTScheduleSunRise <= SchedulerList[i].DTScheduleTimeSR4) //if sunset is after T3: if (sunrise < T4 ) then T4 = sunrise
                {
                    t4 = SchedulerList[i].DTScheduleSunRise;
                }

                if (SchedulerList[i].DTScheduleSunRise <= SchedulerList[i].DTScheduleTimeSR3) //if sunset is after T4: if (sunrise < T3 ) then T3 = sunrise
                {
                    t3 = SchedulerList[i].DTScheduleSunRise;
                }

                if (SchedulerList[i].DTScheduleSunRise <= SchedulerList[i].DTScheduleTimeSR2) //if sunset is after T5: if (sunrise < T2 ) then T2 = sunrise
                {
                    t2 = SchedulerList[i].DTScheduleSunRise;
                }

                TimeSpan duration12 = t2.Subtract(t1);
                double length12 = AstroBasedUtils.GetTimeInDoubleValue(duration12);

                TimeSpan duration23 = t3.Subtract(t2);
                double length23 = AstroBasedUtils.GetTimeInDoubleValue(duration23);

                TimeSpan duration34 = t4.Subtract(t3);
                double length34 = AstroBasedUtils.GetTimeInDoubleValue(duration34);

                TimeSpan duration45 = t5.Subtract(t4);
                double length45 = AstroBasedUtils.GetTimeInDoubleValue(duration45);

                TimeSpan duration56 = t6.Subtract(t5);
                double length56 = AstroBasedUtils.GetTimeInDoubleValue(duration56);

                TimeSpan fullspan = t6.Subtract(t1);
                double fullLength = AstroBasedUtils.GetTimeInDoubleValue(fullspan);

                result += ((100 - onLevel) * length12 + (100 - dimLevel1) * length23 + (100 - dimLevel2) * length34 + ((100 - dimLevel3) * length45) + (100 - dimLevel4) * length56) / (fullLength * 100);
            }

            return result;
        }

        private void ValidateScheduleOutputValues(AstroSchedules selSchedulesValue)
        {
            var list = InitErrorValidationObjects(selSchedulesValue);
            ValidateErrorObjects(list);
        }

        private void ValidateErrorObjects(List<ErrorValidationValue> list)
        {
            for (int i = 1; i <= 6; i++)
            {
                switch (i)
                {
                    case 1: // Validation on T1 Level Value
                        {
                            if (list[1].ErrorId == 1 ||
                                list[1].ErrorId == 2 ||
                                list[1].ErrorId == 5 ||
                                list[1].ErrorId == 6 ||
                                list[1].ErrorId == 7)
                            {
                                this.ScheduleOutputLevel.IsValid1 = false;
                            }else
                            {
                                this.ScheduleOutputLevel.IsValid1 = true;
                            }

                        }

                        break;

                    case 2: // Validation on T2 Level Value
                        {
                            if (list[1].ErrorId == 3 ||
                                list[1].ErrorId == 4 ||
                                list[2].ErrorId == 1 ||
                                list[2].ErrorId == 2 ||
                                list[2].ErrorId == 5 ||
                                list[2].ErrorId == 6 ||
                                list[2].ErrorId == 7 ||
                                list[1].ErrorId == 8)
                            {
                                this.ScheduleOutputLevel.IsValid2 = false;
                            }
                            else
                            {
                                this.ScheduleOutputLevel.IsValid2 = true;
                            }

                        }

                        break;

                    case 3: // Validation on T3 Level Value
                        {
                            if (list[2].ErrorId == 3 ||
                                list[2].ErrorId == 4 ||
                                list[3].ErrorId == 1 ||
                                list[3].ErrorId == 2 ||
                                list[3].ErrorId == 5 ||
                                list[3].ErrorId == 6 ||
                                list[3].ErrorId == 7 ||
                                list[2].ErrorId == 8)
                            {
                                this.ScheduleOutputLevel.IsValid3 = false;
                            }
                            else
                            {
                                this.ScheduleOutputLevel.IsValid3 = true;
                            }

                        }

                        break;

                    case 4: // Validation on T4 Level Value
                        {
                            if (list[3].ErrorId == 3 ||
                                list[3].ErrorId == 4 ||
                                list[4].ErrorId == 1 ||
                                list[4].ErrorId == 2 ||
                                list[4].ErrorId == 5 ||
                                list[4].ErrorId == 6 ||
                                list[4].ErrorId == 7 ||
                                list[3].ErrorId == 8)
                            {
                                this.ScheduleOutputLevel.IsValid4 = false;
                            }
                            else
                            {
                                this.ScheduleOutputLevel.IsValid4 = true;
                            }

                        }

                        break;

                    case 5: // Validation on T5 Level Value
                        {
                            if (list[4].ErrorId == 3 ||
                                list[4].ErrorId == 4 ||
                                list[4].ErrorId == 8)
                            {
                                this.ScheduleOutputLevel.IsValid5 = false;
                            }
                            else
                            {
                                this.ScheduleOutputLevel.IsValid5 = true;
                            }

                        }

                        break;

                    case 6:
                        {
                        }

                        break;
                }
            }
        }

        private List<ErrorValidationValue> InitErrorValidationObjects(AstroSchedules selSchedulesValue)
        {
            bool astroDimMode = _astroDimFeature.AstroDimMode == AstroDimMode.AstroBased;
            List<ErrorValidationValue> values = new List<ErrorValidationValue>();

            //Sun Set Time--------------------------------------------------
            if (astroDimMode)
            {
                values.Add(new ErrorValidationValue()
                {
                    ErrorId = 0,
                    Value = selSchedulesValue.DTScheduleSunSet,
                    OutputLevel = 0,
                    OperatingTime = selSchedulesValue.DTScheduleSunSet.TimeToDouble(),
                    ScaleValueOfTime = selSchedulesValue.DTScheduleSunSet.ADTimeToDouble(),
                    IsTodaysValue = selSchedulesValue.DTScheduleSunSet.ToString("tt", CultureInfo.InvariantCulture).Contains("PM")
                });
            }
            else
            {
                values.Add(new ErrorValidationValue()
                {
                    ErrorId = 0,
                    Value = selSchedulesValue.DTScheduleSunSet,
                    OutputLevel = 0,
                    OperatingTime = selSchedulesValue.DTScheduleSunSet.TimeToDouble(),
                    ScaleValueOfTime = selSchedulesValue.DTScheduleSunSet.TimeToDouble(),
                    IsTodaysValue = true
                });
            }

            //Level 2 --------------------------------------------------
            if (astroDimMode)
            {
                values.Add(new ErrorValidationValue()
                {
                    ErrorId = 0,
                    Value = selSchedulesValue.DTScheduleTimeSR2,
                    OutputLevel = Convert.ToDouble(ScheduleOutputLevel.Element1),
                    OperatingTime = selSchedulesValue.DTScheduleTimeSR2.TimeToDouble(),
                    ScaleValueOfTime = selSchedulesValue.DTScheduleTimeSR2.ADTimeToDouble(),
                    IsTodaysValue = selSchedulesValue.DTScheduleTimeSR2.ToString("tt", CultureInfo.InvariantCulture).Contains("PM")
                });
            }
            else
            {
                values.Add(new ErrorValidationValue()
                {
                    ErrorId = 0,
                    Value = selSchedulesValue.DTScheduleTimeSR2,
                    OutputLevel = Convert.ToDouble(ScheduleOutputLevel.Element1),
                    OperatingTime = selSchedulesValue.DTScheduleTimeSR2.TimeToDouble(),
                    ScaleValueOfTime = selSchedulesValue.DTScheduleTimeSR2.TimeToDouble(),
                    IsTodaysValue = true
                });
            }

            //Level 3 --------------------------------------------------
            if (astroDimMode)
            {
                values.Add(new ErrorValidationValue()
                {
                    ErrorId = 0,
                    Value = selSchedulesValue.DTScheduleTimeSR3,
                    OutputLevel = Convert.ToDouble(ScheduleOutputLevel.Element2),
                    OperatingTime = selSchedulesValue.DTScheduleTimeSR3.TimeToDouble(),
                    ScaleValueOfTime = selSchedulesValue.DTScheduleTimeSR3.ADTimeToDouble(),
                    IsTodaysValue = selSchedulesValue.DTScheduleTimeSR3.ToString("tt", CultureInfo.InvariantCulture).Contains("PM")
                });
            }
            else
            {
                values.Add(new ErrorValidationValue()
                {
                    ErrorId = 0,
                    Value = selSchedulesValue.DTScheduleTimeSR3,
                    OutputLevel = Convert.ToDouble(ScheduleOutputLevel.Element2),
                    OperatingTime = selSchedulesValue.DTScheduleTimeSR3.TimeToDouble(),
                    ScaleValueOfTime = selSchedulesValue.DTScheduleTimeSR3.TimeToDouble(),
                    IsTodaysValue = true
                });
            }

            //Level 4 --------------------------------------------------
            if (astroDimMode)
            {
                values.Add(new ErrorValidationValue()
                {
                    ErrorId = 0,
                    Value = selSchedulesValue.DTScheduleTimeSR4,
                    OutputLevel = Convert.ToDouble(ScheduleOutputLevel.Element3),
                    OperatingTime = selSchedulesValue.DTScheduleTimeSR4.TimeToDouble(),
                    ScaleValueOfTime = selSchedulesValue.DTScheduleTimeSR4.ADTimeToDouble(),
                    IsTodaysValue = selSchedulesValue.DTScheduleTimeSR4.ToString("tt", CultureInfo.InvariantCulture).Contains("PM")
                });
            }
            else
            {
                values.Add(new ErrorValidationValue()
                {
                    ErrorId = 0,
                    Value = selSchedulesValue.DTScheduleTimeSR4,
                    OutputLevel = Convert.ToDouble(ScheduleOutputLevel.Element3),
                    OperatingTime = selSchedulesValue.DTScheduleTimeSR4.TimeToDouble(),
                    ScaleValueOfTime = selSchedulesValue.DTScheduleTimeSR4.TimeToDouble(),
                    IsTodaysValue = true
                });
            }

            //Level 5 --------------------------------------------------
            if (astroDimMode)
            {
                values.Add(new ErrorValidationValue()
                {
                    ErrorId = 0,
                    Value = selSchedulesValue.DTScheduleTimeSR5,
                    OutputLevel = Convert.ToDouble(ScheduleOutputLevel.Element4),
                    OperatingTime = selSchedulesValue.DTScheduleTimeSR5.TimeToDouble(),
                    ScaleValueOfTime = selSchedulesValue.DTScheduleTimeSR5.ADTimeToDouble(),
                    IsTodaysValue = selSchedulesValue.DTScheduleTimeSR5.ToString("tt", CultureInfo.InvariantCulture).Contains("PM")
                });
            }
            else
            {
                values.Add(new ErrorValidationValue()
                {
                    ErrorId = 0,
                    Value = selSchedulesValue.DTScheduleTimeSR5,
                    OutputLevel = Convert.ToDouble(ScheduleOutputLevel.Element4),
                    OperatingTime = selSchedulesValue.DTScheduleTimeSR5.TimeToDouble(),
                    ScaleValueOfTime = selSchedulesValue.DTScheduleTimeSR5.TimeToDouble(),
                    IsTodaysValue = true
                });
            }

            //Sun Rise Time--------------------------------------------------
            if (astroDimMode)
            {
                values.Add(new ErrorValidationValue()
                {
                    ErrorId = 0,
                    Value = selSchedulesValue.DTScheduleSunRise,
                    OutputLevel = Convert.ToDouble(ScheduleOutputLevel.Element5),
                    OperatingTime = selSchedulesValue.DTScheduleSunRise.TimeToDouble(),
                    ScaleValueOfTime = selSchedulesValue.DTScheduleSunRise.ADTimeToDouble(),
                    IsTodaysValue = selSchedulesValue.DTScheduleSunRise.ToString("tt", CultureInfo.InvariantCulture).Contains("PM")
                });
            }
            else
            {
                values.Add(new ErrorValidationValue()
                {
                    ErrorId = 0,
                    Value = selSchedulesValue.DTScheduleSunRise,
                    OutputLevel = Convert.ToDouble(ScheduleOutputLevel.Element5),
                    OperatingTime = selSchedulesValue.DTScheduleSunRise.TimeToDouble(),
                    ScaleValueOfTime = selSchedulesValue.DTScheduleSunRise.TimeToDouble(),
                    IsTodaysValue = true
                });
            }

            //Switch On Fade--------------------------------------------------
            var tmpSwichontime = ConstantListValues.Instance.AstroDimSwitchOnRangeValues().Find(x => x.Key == _astroDimFeature.SwitchOnFadeTime).Value;
            var switchOnFade = AstroBasedUtils.AstroFadeTimeToDouble(tmpSwichontime);

            //Switch off Fade--------------------------------------------------
            var tmpSwichOfftime = ConstantListValues.Instance.AstroDimSwitchOffRangeValues().Find(x => x.Key == _astroDimFeature.SwitchOffFadeTime).Value;
            var switchOffFade = AstroBasedUtils.AstroFadeTimeToDouble(tmpSwichOfftime);

            ////Astro Dim Scheduler value validation
            if (astroDimMode)
            {
                for (int i = 1; i <= 4; i++)
                {
                    if (values[i].IsTodaysValue && values[i].OperatingTime < 12)
                    {
                        values[i].ErrorId = 1;
                    }
                    else if (values[i].IsTodaysValue == values[0].IsTodaysValue && values[i].ScaleValueOfTime <= values[0].ScaleValueOfTime)
                    {
                        values[i].ErrorId = 2;
                    }
                    else if (values[i].IsTodaysValue == false && values[i].OperatingTime >= 12)
                    {
                        values[i].ErrorId = 3;
                    }
                    else if (values[i].IsTodaysValue == values[5].IsTodaysValue && values[i].ScaleValueOfTime > values[5].ScaleValueOfTime)
                    {
                        values[i].ErrorId = 4;
                    }
                    else if (values[i].ScaleValueOfTime < values[i - 1].ScaleValueOfTime && values[i - 1].OperatingTime > values[0].OperatingTime)
                    {
                        values[i].ErrorId = 5;
                    }
                    else if (values[i].ScaleValueOfTime > values[i + 1].ScaleValueOfTime && values[i + 1].OperatingTime < values[5].OperatingTime)
                    {
                        values[i].ErrorId = 6;
                    }
                    else if (values[i - 1].IsTodaysValue == values[i].IsTodaysValue && values[0].ScaleValueOfTime + switchOnFade >= values[i].ScaleValueOfTime)
                    {
                        values[i].ErrorId = 7;
                    }
                    else if (values[i].IsTodaysValue == values[i + 1].IsTodaysValue && values[i].ScaleValueOfTime >= values[5].ScaleValueOfTime - switchOffFade)
                    {
                        values[i].ErrorId = 8;
                    }
                    else
                    {
                        values[i].ErrorId = 0;
                    }
                }
            }
            return values;
        }

        public class ScheduleMode
        {
            public string Name { get; set; }
            public ScheduleType SType { get; set; }

            public ScheduleMode(string v, ScheduleType sunrise_Sunset)
            {
                this.Name = v;
                this.SType = sunrise_Sunset;
            }
        }

        public enum ScheduleType
        {
            Sunrise_Sunset,
            Manual
        }
    }
}
