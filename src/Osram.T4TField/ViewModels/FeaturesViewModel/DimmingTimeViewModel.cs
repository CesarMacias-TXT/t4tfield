﻿using System;
using Xamarin.Forms;
using System.Windows.Input;
using MvvmCross.Commands;
using Osram.T4TField.ViewModels.FeaturesViewModel;
using Rg.Plugins.Popup.Services;
using Osram.TFTBackend.DataModel.Feature.Contracts;
using System.Globalization;
using Osram.TFTBackend.LocationService;

namespace Osram.T4TField.ViewModels
{
    public class DimmingTimeViewModel : BaseViewModel
    {
        private DateTime realMidnightTime = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 0, 0, 0, 0);
        private IAstroDimFeature _astroDimFeature;
        /// <summary>
        /// The day changed time value
        /// </summary>
        private TimeSpan dayChangedTime;
        private DateTime defaultValue;
        private DateTime dateUpdated;

        public DimmingTimeViewModel(IAstroDimFeature astroDimFeature)
        {
            _astroDimFeature = astroDimFeature;
            ConfirmTimeCommand = new MvxCommand(OnConfirmTimeCommand);
            CloseTimeCommand = new MvxCommand(OnCancelTimeCommand);
        }

        private void OnCancelTimeCommand()
        {
            PopupNavigation.PopAsync();
        }

        private void OnConfirmTimeCommand()
        {            
            SetDateTimeValue(itemSelected, dateUpdated);
            PopupNavigation.PopAsync();
        }

        public override void Start()
        {
            base.Start();
            // inserire lettura parametri
            itemSelected = (ClassParameter as AstroDimViewModel.SetDimmingTimeParameter)?.TimeItemSelected ?? 1;
            defaultValue = (ClassParameter as AstroDimViewModel.SetDimmingTimeParameter)?.DefaultValue ?? DateTime.Now;
            dayChangedTime = _astroDimFeature.AstroDimMode == AstroDimMode.AstroBased ? new TimeSpan(12, 0, 0) : new TimeSpan(0, 0, 0);

            TimeCurrent = GetDateTimeValue(itemSelected);
            if (itemSelected != 1)
            {
                TimePrev = GetDateTimeValue(itemSelected - 1);
            }
            if (itemSelected != 6)
            {
                TimeNext = GetDateTimeValue(itemSelected + 1);
            }
            
            CurrentHour = TimeCurrent.Hour;
            CurrentMinutes = TimeCurrent.Minute;
            CurrentTimeStatusColor = Styles.ActivityIndicatorColor;
            CurrentTime = TimeCurrent.ToString("HH:mm");
            
            //first not visible if astroBased and selected == 2 (in ReferenceSchedule) or selected == 1 (in Schedule)
            FirstTimeVisible = !(_astroDimFeature.AstroDimMode == AstroDimMode.AstroBased && itemSelected == 2) && !(itemSelected == 1);
            //last not visible if astroBased and selected == 5 (in ReferenceSchedule) or selected == 6 (in Schedule)
            LastTimeVisible = !(_astroDimFeature.AstroDimMode == AstroDimMode.AstroBased && itemSelected == 5) && !(itemSelected == 6);        
        }

        private DateTime GetDateTimeValue(int itemSelected)
        {
            TimeValue values = (ClassParameter as AstroDimViewModel.SetDimmingTimeParameter)?.TimeValues;
            if (values == null)
                throw new Exception("No SetDimmingTimeParameter error");

            switch (itemSelected)
            {
                case 1: return values.Element1;
                case 2: return values.Element2;
                case 3: return values.Element3;
                case 4: return values.Element4;
                case 5: return values.Element5;
                case 6: return values.Element6;
                default: throw new Exception("Item Selected Error");
            }
        }

        private DateTime SetDateTimeValue(int itemSelected, DateTime value)
        {
            TimeValue values = (ClassParameter as AstroDimViewModel.SetDimmingTimeParameter)?.TimeValues;
            if (values == null)
                throw new Exception("No SetDimmingTimeParameter error");

            switch (itemSelected)
            {
                case 1: return values.Element1 = value;
                case 2: return values.Element2 = value;
                case 3: return values.Element3 = value;
                case 4: return values.Element4 = value;
                case 5: return values.Element5 = value;
                case 6: return values.Element6 = value;
                default: throw new Exception("Item Selected Error");
            }
        }

        private bool CheckDateRange(DateTime prev, DateTime curr, DateTime next)
        {
            if (curr.CompareTo(prev) > 0 && curr.CompareTo(next) < 0)
                return true;

            return false;
        }

        private bool ValidateTime()
        {
            var tmpTimeSpan = new TimeSpan(CurrentHour, CurrentMinutes, 0);
            DateTime tmpDefaultValue = defaultValue;
            dateUpdated = new DateTime(TimeCurrent.Year, TimeCurrent.Month, TimeCurrent.Day, currentHour, currentMinutes, 0);
            if (tmpTimeSpan < dayChangedTime)
            {
                string strDate = string.Format("{0} {1}:00", tmpDefaultValue.AddDays(1).ToString("d"), getTimeString());
                dateUpdated = Convert.ToDateTime(strDate);
            }
            else
            {
                string strDate = string.Format("{0} {1}:00", tmpDefaultValue.ToString("d"), getTimeString());
                dateUpdated = Convert.ToDateTime(strDate);
            }
            if (_astroDimFeature.AstroDimMode == AstroDimMode.TimeBased)
            {
                return ValidateTimeBasedTime(dateUpdated);
            }
            else
            {
                return ValidateAstroBasedTime(dateUpdated);
            }
        }

        private bool ValidateAstroBasedTime(DateTime newTime)
        {
            //in astrobased mode the items 1 and 6 can only be selected in Manual mode
            if (itemSelected == 1)
                return true;
            else if (itemSelected == 2)
                return ValidateTimeInIncreasingOrder(newTime, TimeNext);
            else if (itemSelected == 5)
                return ValidateTimeInIncreasingOrder(TimePrev, newTime);
            else if (itemSelected == 6)
                return true;
            else
                return ValidateTimeInIncreasingOrder(newTime, TimePrev, TimeNext);             
        }   

        private bool ValidateTimeBasedTime(DateTime newTime)
        {           
            return CheckDateRange(TimePrev, newTime, TimeNext);            
        }

        /// <summary>
        /// Validates time entry for the time level on moving from PM to AM i.e in increasing order or not.
        /// </summary>
        /// <param name="value">Current time</param>
        /// <param name="nextStamp">Next textbox time</param>
        /// <returns>Returns validated time in increasing order.</returns>
        private bool ValidateTimeInIncreasingOrder(DateTime value, DateTime nextStamp)
        {
            if (nextStamp != value)
            {
                string currentTime = value.ToString("tt", CultureInfo.InvariantCulture);
                string nextTime = nextStamp.ToString("tt", CultureInfo.InvariantCulture);

                //All time in one day i.e before 12:00 midnight
                if (currentTime.Contains(AstroBasedUtils.StrTimePM) && nextTime.Contains(AstroBasedUtils.StrTimePM))
                {
                    return value < nextStamp;
                }

                //All time in next day i.e after 12:00 midnight
                if (currentTime.Contains(AstroBasedUtils.StrTimeAM) && nextTime.Contains(AstroBasedUtils.StrTimeAM))
                {
                    return value < nextStamp;
                }

                if (currentTime.Contains(AstroBasedUtils.StrTimePM) && nextTime.Contains(AstroBasedUtils.StrTimeAM))
                {
                    return true;
                }

                if (currentTime.Contains(AstroBasedUtils.StrTimeAM) && nextTime.Contains(AstroBasedUtils.StrTimePM))
                {
                    return false;
                }
            }

            return false;
        }
        
        /// <summary>
        /// Validate time entry for the time level is moving from PM to am i.e in increasing order or not
        /// </summary>
        /// <param name="value">Current time</param>
        /// <param name="prevStamp">Previous textbox time</param>
        /// <param name="nextStamp">Next textbox time</param>
        /// <returns>Returns validated time in increasing order.</returns>
        public bool ValidateTimeInIncreasingOrder(DateTime value, DateTime prevStamp, DateTime nextStamp)
        {
            if (nextStamp != value && value != prevStamp && prevStamp != nextStamp)
            {
                string currentTime = value.ToString("tt", CultureInfo.InvariantCulture);
                string prevTime = prevStamp.ToString("tt", CultureInfo.InvariantCulture);
                string nextTime = nextStamp.ToString("tt", CultureInfo.InvariantCulture);

                //All time in one day i.e before 12:00 midnight
                if (currentTime.Contains(AstroBasedUtils.StrTimePM) && prevTime.Contains(AstroBasedUtils.StrTimePM) && nextTime.Contains(AstroBasedUtils.StrTimePM))
                {
                    return prevStamp < value && value < nextStamp;
                }

                //All time in next day i.e after 12:00 midnight
                if (prevTime.Contains(AstroBasedUtils.StrTimeAM) && currentTime.Contains(AstroBasedUtils.StrTimeAM) && nextTime.Contains(AstroBasedUtils.StrTimeAM))
                {
                    return (prevStamp < value) && (value < nextStamp);
                }

                if (prevTime.Contains(AstroBasedUtils.StrTimePM) && currentTime.Contains(AstroBasedUtils.StrTimeAM) && nextTime.Contains(AstroBasedUtils.StrTimeAM))
                {
                    return value < nextStamp;
                }

                if (prevTime.Contains(AstroBasedUtils.StrTimePM) && currentTime.Contains(AstroBasedUtils.StrTimePM) && nextTime.Contains(AstroBasedUtils.StrTimeAM))
                {
                    return prevStamp < value;
                }

                if (prevTime.Contains(AstroBasedUtils.StrTimeAM) && currentTime.Contains(AstroBasedUtils.StrTimeAM) && nextTime.Contains(AstroBasedUtils.StrTimePM))
                {
                    return value < nextStamp;
                }

                if (prevTime.Contains(AstroBasedUtils.StrTimePM) && currentTime.Contains(AstroBasedUtils.StrTimeAM) && nextTime.Contains(AstroBasedUtils.StrTimePM))
                {
                    return false;
                }

                if (prevTime.Contains(AstroBasedUtils.StrTimeAM) && currentTime.Contains(AstroBasedUtils.StrTimePM) && nextTime.Contains(AstroBasedUtils.StrTimeAM))
                {
                    return false;
                }
            }

            return false;
        }

        private void updateCurrentValue()
        {            
            if (ValidateTime())
            {
                RightTimeInserted();
            }
            else
            {
                WrongTimeInserted();
            }
            CurrentTime = getTimeString();
        }

        private void WrongTimeInserted()
        {
            CurrentTimeStatusColor = Styles.OsramColor;
            //disable button
            IsButtonEnabled = false;
        }

        private void RightTimeInserted()
        {
            CurrentTimeStatusColor = Styles.DarkGray;
            //enable button
            IsButtonEnabled = true;
        }

        private string getTimeString()
        {

            var h = currentHour + string.Empty;
            var m = currentMinutes + string.Empty;
            if (h.Length == 1)
            {
                h = "0" + h;
            }

            if (m.Length == 1)
            {
                m = "0" + m;
            }
            return h + ":" + m;
        }

        private int currentHour;
        public int CurrentHour
        {
            get { return currentHour; }
            set
            {
                currentHour = value;
                updateCurrentValue();
                RaisePropertyChanged();
            }
        }

        private int currentMinutes;
        public int CurrentMinutes
        {
            get { return currentMinutes; }
            set
            {
                currentMinutes = value;
                updateCurrentValue();
                RaisePropertyChanged();
            }
        }

        private string currentTime;
        public string CurrentTime
        {
            get { return currentTime; }
            set
            {
                currentTime = value;
                RaisePropertyChanged();
            }
        }

        private Color currentTimeStatusColor;
        private int itemSelected;

        private DateTime timePrev;
        public DateTime TimePrev
        {
            get { return timePrev; }
            set
            {
                timePrev = value;
                RaisePropertyChanged();
            }
        }
        private DateTime timeNext;
        public DateTime TimeNext
        {
            get { return timeNext; }
            set
            {
                timeNext = value;
                RaisePropertyChanged();
            }
        }

        private DateTime timeCurrent;
        public DateTime TimeCurrent
        {
            get { return timeCurrent; }
            set
            {
                timeCurrent = value;
                RaisePropertyChanged();
            }
        }

        public Color CurrentTimeStatusColor
        {
            get { return currentTimeStatusColor; }
            set
            {
                currentTimeStatusColor = value;
                RaisePropertyChanged();
            }
        }

        public ICommand ConfirmTimeCommand { get; set; }
        public ICommand CloseTimeCommand { get; set; }

        private bool isButtonEnabled;

        public bool IsButtonEnabled
        {
            get { return isButtonEnabled; }
            set
            {
                isButtonEnabled = value;
                RaisePropertyChanged();
            }
        }

        public bool FirstTimeVisible { get; private set; }
        public bool LastTimeVisible { get; private set; }
    }
}
