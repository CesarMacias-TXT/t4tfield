﻿using System.Collections.Generic;
using MvvmCross.Commands;
using Rg.Plugins.Popup.Services;
using Osram.T4TField.Messaging;
using Osram.T4TField.Resources;
using Osram.TFTBackend;
using Osram.TFTBackend.PersistenceService;

namespace Osram.T4TField.ViewModels
{
    public class SelectConfigurationViewModel : BaseViewModel
    {
        private bool _selectionDone;

        public IMvxCommand SelectItemCommand { get; }
        public List<EcgDeviceInfo> Items { get; set; }

        public EcgDeviceInfo DeviceInfo { get; private set; }

        public SelectConfigurationViewModel()
        {
            SelectItemCommand = new MvxCommand<EcgDeviceInfo>(OnSelectItemCommand);
            Title = AppResources.SelectConfigurationTitle;
        }

        public override void Start()
        {
            base.Start();
            Items = (ClassParameter as Parameter)?.Items;
        }

        private async void OnSelectItemCommand(EcgDeviceInfo item)
        {
            DeviceInfo = item;
            await PopupNavigation.PopAsync();
            Logger.Trace($"SelectConfigurationViewModel.OnSelectItemCommand {item.Description}");
            _messenger.Publish(new Events.SelectedConfigurationEvent(this, item.Id));
            _selectionDone = true;

            //  Cancel();
        }

        public override void Close()
        {
            if (!_selectionDone)
                _messenger.Publish(new Events.SelectedConfigurationEvent(this, -1));
            base.Close();
        }

        #region Parameter for this ViewModel
        public class Parameter : BaseViewModelParameter
        {
            public Parameter(bool isModal, List<EcgDeviceInfo> items)
                : base(isModal)
            {
                Items = items;
            }

            public List<EcgDeviceInfo> Items { get; }
        }
        #endregion
    }
}
