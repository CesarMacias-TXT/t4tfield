﻿using Acr.UserDialogs;
using MvvmCross.ViewModels;
using MvvmCross;
using MvvmCross.Commands;
using MvvmCross.Plugin.Messenger;
using Osram.T4TField.Contracts;
using Osram.T4TField.Resources;
using Osram.TFTBackend;
using Osram.TFTBackend.BaseTypes;
using Osram.TFTBackend.BaseTypes.Exceptions;
using Osram.TFTBackend.BluetoothService.Contracts;
using Osram.TFTBackend.ConfigurationService.Contracts;
using Osram.TFTBackend.DataModel;
using Osram.TFTBackend.DataModel.Feature.Contracts;
using Osram.TFTBackend.InAppPurchaseService.Contracts;
using Osram.TFTBackend.KeyService.Contracts;
using Osram.TFTBackend.Messaging;
using Osram.TFTBackend.NfcService.Contracts;
using Osram.TFTBackend.NfcService.NfcDataTypes;
using Osram.TFTBackend.ProgrammingService.Contracts;
using Plugin.Settings.Abstractions;
using System;
using System.IO;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;

namespace Osram.T4TField.ViewModels
{
    public class HomeViewModel : ConnectionBaseViewModel
    {

        private readonly MvxSubscriptionToken _tokenConfigurationChanged;
        private readonly MvxSubscriptionToken _tokenConfigurationReset;

        protected IUserDialogs _userDialogs;
        protected IConfigurationService _configurationService;
        private ISelectConfigurationUiService _selectConfigurationUiService;
        private readonly IInAppPurchaseService _inAppPurchaseService;

        private readonly IFeatureManager _featureManager;

        public HomeViewModel(IConfigurationService configurationService, ISelectConfigurationUiService selectConfigurationUiService, IUserDialogs dialog, ISettings settings, IKeyService keyService, IInAppPurchaseService inAppPurchaseService)
        {
            Title = AppResources.F_Navigation_Home;
            IsOnlyReadEcgBtn = false;
            _configurationService = configurationService;
            _selectConfigurationUiService = selectConfigurationUiService;
            _userDialogs = dialog;
            InitialiseHomeGui();
            _tokenConfigurationChanged = _messenger.Subscribe<Events.ConfigurationChangedEvent>(ShowConfigurationData);
            _tokenConfigurationReset = _messenger.Subscribe<Events.ConfigurationResetEvent>(ResetConfigurationData);
            _settings = settings;
            _featureManager = Mvx.IoCProvider.Resolve<IFeatureManager>();
            _keyService = keyService;
            _inAppPurchaseService = inAppPurchaseService;
        }

        public virtual void ShowConfigurationData(Events.ConfigurationChangedEvent configurationEvent)
        {
            Logger.Trace("ShowConfigurationData");
            if (configurationEvent.Configuration != null)
            {
                if (configurationEvent.Configuration.ThePicture != null && configurationEvent.Configuration.ThePicture.Length > 0)
                {
                    ECGImageSource = ImageSource.FromStream(() => new MemoryStream(configurationEvent.Configuration.ThePicture));
                }
                else
                {
                    ECGImageSource = "ecgDefault.png";
                }
                LuminaireName = configurationEvent.Configuration.LuminaireName;
                Gtin = AppResources.F_GTIN + ": " + configurationEvent.Configuration.Gtin.ToString();
                BasicCode = AppResources.F_BasicCode + ": " + configurationEvent.Configuration.BasicCode;
                IsECGInfoBoxVisible = true;
            }
            else
            {
                _userDialogs.Alert(AppResources.ErrorDeviceNotSupported, AppResources.F_DialogProgrammingErrorTitle);
            }
        }

        public virtual void ResetConfigurationData(Events.ConfigurationResetEvent configurationEvent)
        {
            Logger.Trace("ResetConfigurationData");
            OnRemoveSelectedECGCommand();
        }

        public override Task Cleanup()
        {
            _messenger.Unsubscribe<Events.ConfigurationChangedEvent>(_tokenConfigurationChanged);
            _messenger.Unsubscribe<Events.ConfigurationChangedEvent>(_tokenConfigurationReset);
            return base.Cleanup();
        }

        public IMvxCommand NewConfigurationFromLuminaireCommand { get; set; }
        public IMvxCommand NewMonitoringDataConfigurationFromLuminaireCommand { get; set; }
        public ICommand OpenProductionConfigurationsCommand { get; set; }
        public ICommand RemoveSelectedECGCommand { get; set; }
        public ICommand OpenSearchBTDeviceViewCommand { get; set; }

        public ICommand NFCReaderSwithCommand { get; set; }

        private async Task OnSelectEcgButtonCommandAsync()
        {
            if (CheckIfAdapterIsReady())
            {
                bool isPositionGuidePopupShown = settings.GetValueOrDefault(PositioningGuideViewModel.IsPositioningGuidePopupShown, false);
                if (isPositionGuidePopupShown)
                {
                    await ReadEcgPopup();
                }
                else
                {
                    await ShowViewModel<PositioningGuideViewModel>(new PositioningGuideViewModel.BTGuidePopupParameter(true)
                    {
                        Action = ReadEcgPopup,
                    });
                }
            }
            else
            {
                await _userDialogs.AlertAsync(new AlertConfig().Message = GetAdapterDisabledMessage());
            }
        }

        private async Task OnSelectMonitoringDataEcgButtonCommandAsync()
        {
            if (CheckIfAdapterIsReady())
            {
                //if (await _inAppPurchaseService.CanConsumePurchaseAsync())
                //{
                //    bool isPaidFeaturesPopupShown = settings.GetValueOrDefault(PaidFeaturesPopupViewModel.IsPaidFeaturesPopupViewModelPopupShown, false);

                //    if (isPaidFeaturesPopupShown)
                //    {
                //        await ShowPositionGuidePopup();
                //    }
                //    else
                //    {
                //        MyShowViewModel<PaidFeaturesPopupViewModel>(RequestType.Popup, new PaidFeaturesPopupViewModel.BlankDriversPopupParameter(true)
                //        {
                //            Title = AppResources.F_PaidFeature,
                //            Action = ShowPositionGuidePopup,
                //        });
                //    }                    
                //}
                //else
                //{
                //    _userDialogs.Alert(new AlertConfig()
                //    {
                //        Message = AppResources.F_ManageSubscriptionNoPremium,
                //        OnAction = () => ShowViewModel<ManageSubscriptionViewModel>(),
                //    });
                //}

                await ShowPositionGuidePopup();
            }
            else
            {
                await _userDialogs.AlertAsync(new AlertConfig().Message = GetAdapterDisabledMessage());
            }
        }

        private async Task ShowPositionGuidePopup()
        {
            bool isPositionGuidePopupShown = settings.GetValueOrDefault(PositioningGuideViewModel.IsPositioningGuidePopupShown, false);

            if (isPositionGuidePopupShown)
            {
                await ReadMonitorigDataEcgPopup();
            }
            else
            {
                await ShowViewModel<PositioningGuideViewModel>(new PositioningGuideViewModel.BTGuidePopupParameter(true)
                {
                    Action = ReadMonitorigDataEcgPopup,
                });
            }
        }

        private async Task ReadEcgPopup()
        {
            MyShowViewModel<ProgrammingViewModel>(RequestType.Popup, new ProgrammingViewModel.ProgrammingParameter(true)
            {
                Title = AppResources.F_ProgrammingViewReadingPopupTitle,
                ProgrammingAction = ReadEcg,
                ProgrammingType = ProgrammingType.Read
            });
        }

        private async Task ReadMonitorigDataEcgPopup()
        {
            MyShowViewModel<ProgrammingViewModel>(RequestType.Popup, new ProgrammingViewModel.ProgrammingParameter(true)
            {
                Title = AppResources.F_ProgrammingViewReadingPopupTitle,
                ProgrammingAction = ReadMonitoringDataEcg,
                ProgrammingType = ProgrammingType.Read
            });
        }

        private async Task<bool> ReadEcg(BaseViewModel vm, IProgrammingService programmingService, CancellationToken cancellationToken)
        {
            Logger.Trace("ReadEcg started");
            cancellationToken.ThrowIfCancellationRequested();
            EcgSelector selector = programmingService.ReadDevice(cancellationToken);
            if (selector.IsValid)
            {
                cancellationToken.ThrowIfCancellationRequested();
                IConfiguration configuration = await _selectConfigurationUiService.SelectConfiguration(vm, selector);
                cancellationToken.ThrowIfCancellationRequested();
                if (configuration == null)
                {
                    throw new OperationFailedException("ReadEcg: Device is not supported!", ErrorCodes.DeviceNotSupported);
                }
                programmingService.GetPropertyValues(_configurationService.GetConfigurationProperties());
                UpdateFeatureList();

                Logger.Trace("ReadEcg success");
                return true;
            }
            Logger.Trace("ReadEcg failed");
            return false;
        }

        private async Task<bool> ReadMonitoringDataEcg(BaseViewModel vm, IProgrammingService programmingService, CancellationToken cancellationToken)
        {
            Logger.Trace("ReadEcg started");
            cancellationToken.ThrowIfCancellationRequested();
            EcgSelector selector = programmingService.ReadDevice(cancellationToken);
            if (selector.IsValid)
            {
                cancellationToken.ThrowIfCancellationRequested();
                IConfiguration configuration = await _selectConfigurationUiService.SelectConfiguration(vm, selector);
                cancellationToken.ThrowIfCancellationRequested();
                if (configuration == null)
                {
                    throw new OperationFailedException("ReadEcg: Device is not supported!", ErrorCodes.DeviceNotSupported);
                }
                programmingService.GetPropertyValues(_configurationService.GetConfigurationProperties());
                UpdateMonitoringDataFeatureList();

                //await _inAppPurchaseService.ConsumePurchaseAsync();

                Logger.Trace("ReadEcg success");
                return true;
            }
            Logger.Trace("ReadEcg failed");
            return false;
        }

        private void UpdateFeatureList()
        {
            _featureManager.UpdateFeatureList();
            FeatureListUpdated();
        }

        private void UpdateMonitoringDataFeatureList()
        {
            _featureManager.UpdateFeatureList();
            MonitoringDataFeatureListUpdated();
        }

        protected virtual void FeatureListUpdated()
        { }

        protected virtual void MonitoringDataFeatureListUpdated()
        { }

        private void OnOpenProductionConfigurationsCommand()
        {
            _ = ShowViewModel<SelectOstrupViewModel>();
        }

        public virtual void OnRemoveSelectedECGCommand()
        {
            IsSelectECGVisible = true;
            IsECGInfoBoxVisible = false;
        }

        private ImageButton addEcgBtn;
        public ImageButton AddEcgBtn {
            get { return addEcgBtn; }
        }


        private ImageButton openEcgBtn;
        public ImageButton OpenEcgBtn {
            get { return openEcgBtn; }
        }


        private ImageButton readEcgBtn;
        public ImageButton ReadEcgBtn {
            get { return readEcgBtn; }
        }


        private bool isECGInfoBoxVisible;
        public bool IsECGInfoBoxVisible {
            get { return isECGInfoBoxVisible; }
            set {
                isECGInfoBoxVisible = value;
                RaisePropertyChanged();
            }
        }

        private bool isOnlyReadEcgBtn;
        public bool IsOnlyReadEcgBtn {
            get { return isOnlyReadEcgBtn; }
            set {
                isOnlyReadEcgBtn = value;
                RaisePropertyChanged();
            }
        }




        private bool isSelectECGVisible;
        public bool IsSelectECGVisible {
            get { return isSelectECGVisible; }
            set {
                isSelectECGVisible = value;
                RaisePropertyChanged();
            }
        }

        private string _luminaireName;
        public string LuminaireName {
            get { return _luminaireName; }
            set {
                _luminaireName = value;
                RaisePropertyChanged();
            }
        }

        private string _gtin;
        public string Gtin {
            get { return _gtin; }
            set {
                _gtin = value;
                RaisePropertyChanged();
            }
        }

        private ImageSource ecgImageSource;
        public ImageSource ECGImageSource {
            get { return ecgImageSource; }
            set {
                ecgImageSource = value;
                RaisePropertyChanged();
            }
        }

        private string _basicCode;
        private ISettings _settings;
        protected IKeyService _keyService;

        public string BasicCode {
            get { return _basicCode; }
            set {
                _basicCode = value;
                RaisePropertyChanged();
            }
        }


        private void InitialiseHomeGui()
        {

            NewConfigurationFromLuminaireCommand = new MvxAsyncCommand(OnSelectEcgButtonCommandAsync);
            NewMonitoringDataConfigurationFromLuminaireCommand = new MvxAsyncCommand(OnSelectMonitoringDataEcgButtonCommandAsync);
            OpenProductionConfigurationsCommand = new MvxCommand(OnOpenProductionConfigurationsCommand);
            RemoveSelectedECGCommand = new MvxCommand(OnRemoveSelectedECGCommand);
            OpenSearchBTDeviceViewCommand = new MvxCommand(OnOpenSearchBTDeviceViewCommand);

            NFCReaderSwithCommand = new MvxCommand(OnNFCReaderSwithCommand);

            addEcgBtn = new ImageButton();
            addEcgBtn.IconLink = "add.png";
            addEcgBtn.Label = AppResources.F_New;
            addEcgBtn.OnClickButton = NewConfigurationFromLuminaireCommand;

            openEcgBtn = new ImageButton();
            openEcgBtn.IconLink = "openECG.png";
            openEcgBtn.Label = AppResources.F_Open;
            openEcgBtn.OnClickButton = OpenProductionConfigurationsCommand;

            readEcgBtn = new ImageButton();
            readEcgBtn.IconLink = "ReadECG.png";
            readEcgBtn.Label = AppResources.F_Read;
            readEcgBtn.OnClickButton = NewConfigurationFromLuminaireCommand;

            IsECGInfoBoxVisible = false;
            IsSelectECGVisible = true;
        }

        private async void OnOpenSearchBTDeviceViewCommand()
        {
            await ShowViewModel<SearchingBTDeviceViewModel>();
        }

        protected override void OnLanguageChanged()
        {
            AddEcgBtn.Label = AppResources.F_New;
            OpenEcgBtn.Label = AppResources.F_Open;
            ReadEcgBtn.Label = AppResources.F_Read;

            Title = AppResources.F_Navigation_Home;
        }

        private void OnNFCReaderSwithCommand()
        {
            NfcReaderType currentNFcInterfaceType = GetCurrentNFCInterfaceType();

            var nextNFcInterfaceType = currentNFcInterfaceType == NfcReaderType.Bluetooth ? NfcReaderType.Mobile : NfcReaderType.Bluetooth;

            // Check if is Available the next NFC Interface
            if (nextNFcInterfaceType == NfcReaderType.Mobile)
            {
                IMobileNfcReader mobileNfcReader = null;
                if (!Mvx.IoCProvider.TryResolve(out mobileNfcReader) || mobileNfcReader == null || !mobileNfcReader.IsAvailable)
                {
                    Logger.TaggedTrace("HomeViewModel.OnNFCReaderSwithCommand", "Trying to set mobile NFC but it is not available.");
                    return;
                }
            }
            else if (nextNFcInterfaceType == NfcReaderType.Bluetooth)
            {
                IBluetoothReader bluetoothReader = null;
                if (!Mvx.IoCProvider.TryResolve(out bluetoothReader) || bluetoothReader == null)
                {
                    Logger.TaggedTrace("HomeViewModel.OnNFCReaderSwithCommand", "Trying to set bluetooth NFC but it is not available.");
                    return;
                }
            }

            _messenger.Publish(new Events.OnNfcReaderChangedEvent(this, nextNFcInterfaceType));
            _settings.AddOrUpdateValue("NFCSelectedInterface", (decimal)nextNFcInterfaceType);
            //prepareNFCNotificationArea();
            //UpdateStatus();
            RefreshNotificationArea();
        }



    }


    public class ImageButton : MvxViewModel
    {
        public string iconLink;
        public string label;

        private bool isEnabled;
        private bool isSelected;


        public virtual ICommand OnClickButton {
            get; set;
        }

        virtual public string Label {
            get {
                return label;
            }

            set {
                label = value;
                RaisePropertyChanged();
            }
        }

        virtual public string IconLink {
            get {
                return iconLink;
            }

            set {
                iconLink = value;
                RaisePropertyChanged();
            }
        }

        virtual public bool IsEnabled {
            get {
                return isEnabled;
            }

            set {
                isEnabled = value;
                RaisePropertyChanged();
            }
        }

        public virtual bool IsSelected {
            get {
                return isSelected;
            }

            set {
                isSelected = value;
                RaisePropertyChanged();
            }
        }

        private bool isVisible;
        virtual public bool IsVisible {
            get {
                return isVisible;
            }

            set {
                isVisible = value;
                RaisePropertyChanged();
            }
        }



        public virtual string SelectedIconLink { get; internal set; }
    }


    public class FeatureInfoButton : ImageButton
    {

        public IFeatureInfo FeatureInfo { get; set; }

        public FeatureInfoButton(IFeatureInfo FeatureInfo)
        {
            this.FeatureInfo = FeatureInfo;
        }




        public string WarningIconLink { get; internal set; }

        public string ErrorIconLink { get; internal set; }

        public string LockMasterIconLink { get; internal set; }

        public string LockServiceIconLink { get; internal set; }

    }
}
