﻿using MvvmCross.Commands;
using Osram.TFTBackend.PasswordService.Contracts;
using System;
using System.Windows.Input;

namespace Osram.T4TField.ViewModels
{
    public class PasswordEntryViewModel : ListItemViewModel
    {
        public ICommand DeletePasswordCommand { get; set; }
        public ICommand UpdatePasswordCommand { get; set; }

        public PasswordEntryViewModel(PasswordEntry passwordEntry, Action<ListItemViewModel> selectItemAction, Action<ListItemViewModel> clickItemAction, Action<PasswordEntry> OnDeletePasswordCommand, Action<PasswordEntry> OnUpdatePasswordCommand) : base(selectItemAction, clickItemAction)
        {
            DeletePasswordCommand = new MvxCommand<PasswordEntry>(OnDeletePasswordCommand);  
            PasswordEntry = passwordEntry;
            UpdatePasswordCommand = new MvxCommand<PasswordEntry>(OnUpdatePasswordCommand); ;
            Name = passwordEntry.Label;
            //this.DeleteAutomationID = "Delete" + passwordEntry.Id;
            //this.RowAutomationID = "Row" + passwordEntry.Id;
            //this.EditAutomationID = "Edit" + passwordEntry.Id;
            HasArrow = true;
        }

        //public string DeleteAutomationID { get; set; }

        //public string RowAutomationID { get; set; }

        //public string EditAutomationID { get; set; }

        public PasswordEntry PasswordEntry { get; set; }
    }
}