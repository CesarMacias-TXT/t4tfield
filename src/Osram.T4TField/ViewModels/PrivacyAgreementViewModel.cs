﻿using System;
using System.IO;
using MvvmCross.Commands;
using Plugin.Settings.Abstractions;
using Osram.T4TField.Resources;
using Osram.T4TField.Utils;
using Osram.TFTBackend;

namespace Osram.T4TField.ViewModels
{
    public class PrivacyAgreementViewModel : ConnectionBaseViewModel
    {
        private readonly ISettings _settings;
        private string _privacyAgreement;

        public PrivacyAgreementViewModel(ISettings settings)
        {
            _settings = settings;
            Title = AppResources.PrivacyAgreement;
        }

        public override void Start()
        {
            base.Start();

            try
            {
                var resource = "Osram.T4TField.Resources.PrivacyAgreement.html";
                var stream = ResourceUtils.GetLocalizedResourceStream(LocalizationService, resource);

                var privacyAgreementText = "";
                if (stream != null)
                {
                    using (var reader = new StreamReader(stream))
                    {
                        privacyAgreementText = reader.ReadToEnd();
                    }
                }
                _privacyAgreement = privacyAgreementText; // "<div>" + Regex.Replace(privacyAgreementText, @"\r\n?|\n", "<br />") + "</div>";
            }
            catch (Exception ex)
            {
                Logger.Exception(nameof(Start), ex);
            }
        }

        public new Parameter ClassParameter => (Parameter)base.ClassParameter;

        public string PrivacyAgreement
        {
            get { return _privacyAgreement; }
        }

        public bool IsModal
        {
            get { return ClassParameter != null && ClassParameter.IsModal; }
        }

        private MvxCommand _iAcceptCommand;
        public MvxCommand IAcceptCommand
        {
            get
            {
                _iAcceptCommand = _iAcceptCommand ?? new MvxCommand(IAccept);
                return _iAcceptCommand;
            }
        }

        private void IAccept()
        {
            _settings.AddOrUpdateValue(SettingsKeys.PrivacyAgreementAccepted, true);
            Cancel();
        }

        public class Parameter : BaseViewModelParameter
        {
            public Parameter(bool isModal) : base(isModal)
            {
            }
        }
    }
}