﻿using MvvmCross.Commands;
using Osram.T4TField.Resources;
using Plugin.Settings.Abstractions;
using System.Windows.Input;

namespace Osram.T4TField.ViewModels
{
    public class LiabilityViewModel : ConnectionBaseViewModel
    {

        public const string IsLiabilityPopupShown = "IsLiabilityPopupShown";

        private readonly ISettings _settings;
        public ICommand ConfirmCommand { get; set; }

        public LiabilityViewModel(ISettings settings)
        {
            Title = AppResources.F_LiabilityWarning;
            _settings = settings;
            ConfirmCommand = new MvxCommand(OnConfirmCommand);
        }

        private void OnConfirmCommand()
        {
            settings.AddOrUpdateValue(IsLiabilityPopupShown, true);
            GoBackHome();
        }

        public override void Start()
        {
            base.Start();
        }
    }
}
