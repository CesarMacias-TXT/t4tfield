﻿using System;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using System.Windows.Input;
using Osram.T4TField.Resources;
using Osram.TFTBackend;
using Osram.TFTBackend.PersistenceService;
using Osram.TFTBackend.PersistenceService.Contracts;
using Acr.UserDialogs;
using MvvmCross.Plugin.Messenger;
using Osram.TFTBackend.Messaging;

namespace Osram.T4TField.ViewModels
{
    class SelectOstrupViewModel : ConnectionBaseViewModel
    {
        private IPersistenceService _persistenceService;
        private readonly MvxSubscriptionToken _token;

        public SelectOstrupViewModel(IPersistenceService persistenceService, IUserDialogs userDialogs)
        {
            _persistenceService = persistenceService;
            _userDialogs = userDialogs;
            Title = AppResources.SelectOstrupConfiguration;
            _token = _messenger.Subscribe<Events.OsrtupImportFinished>(OnImportFinished);
            OstrupConfigurations = new ObservableCollection<EcgDeviceInfoViewModel>();
        }

        private async void OnImportFinished(Events.OsrtupImportFinished obj)
        {
            await UpdateOsrtupConfigurationList();
        }

        public override async void Resumed()
        {
            Logger.Trace("SelectOstrupViewModel Resumed");
            base.Resumed();
            await UpdateOsrtupConfigurationList();
        }

        private ObservableCollection<EcgDeviceInfoViewModel> _ostrupConfigurations;
        private IUserDialogs _userDialogs;

        public ObservableCollection<EcgDeviceInfoViewModel> OstrupConfigurations
        {
            get { return _ostrupConfigurations; }
            set
            {
                _ostrupConfigurations = value;
                RaisePropertyChanged();
            }
        }

        public ICommand ItemClickCommand { get; set; }

        public void OnItemClickCommand(ListItemViewModel listItemViewModel)
        {
            EcgDeviceInfoViewModel ecgDeviceInfoViewModel = (EcgDeviceInfoViewModel)listItemViewModel;
            if (ecgDeviceInfoViewModel == null) return;
           
            Logger.Trace($"SelectOstrupViewModel.OnItemClickCommand(): tapped on key {ecgDeviceInfoViewModel.Name}");
            ShowViewModel<OsrtupProgrammingViewModel>(new OsrtupProgrammingViewModel.OsrtupProgrammingParameter(true, ecgDeviceInfoViewModel.ecgDeviceInfo));
        }

        public async Task UpdateOsrtupConfigurationList()
        {
            Logger.Trace("SelectOstrupViewModel UpdateOsrtupConfigurationList");
            var ecgDeviceInfoList = new ObservableCollection<EcgDeviceInfo>(await _persistenceService.SelectGetAvailableOstrupConfigurations());
            var ostrupConfigurationsViewModelList = new ObservableCollection<EcgDeviceInfoViewModel>();

            foreach (EcgDeviceInfo item in ecgDeviceInfoList)
            {
                EcgDeviceInfoViewModel newEntry = new EcgDeviceInfoViewModel(item, null, OnItemClickCommand, OnDeleteOstrupFile);
                 ostrupConfigurationsViewModelList.Add(newEntry);
            }

            OstrupConfigurations = ostrupConfigurationsViewModelList;
        }

        private async void OnDeleteOstrupFile(EcgDeviceInfo ecgDeviceInfo)
        {
            bool isDeleteConfirmed = await _userDialogs.ConfirmAsync(AppResources.F_DeletePasswordConfirmationMessage + ": " + ecgDeviceInfo.FileName + "?");
            if (isDeleteConfirmed)
            {
                await _persistenceService.DeleteConfiguration(ecgDeviceInfo.Id);
                await UpdateOsrtupConfigurationList();
            }
        }
    }
}
