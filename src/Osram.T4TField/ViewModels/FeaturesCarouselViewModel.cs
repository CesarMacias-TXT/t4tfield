﻿using Acr.UserDialogs;
using MvvmCross.ViewModels;
using MvvmCross;
using MvvmCross.Plugin.Messenger;
using Osram.T4TField.Messaging;
using Osram.T4TField.Resources;
using Osram.T4TField.ViewModels.FeaturesViewModel;
using Osram.T4TField.Views.TemplateSelectors;
using Osram.TFTBackend;
using Osram.TFTBackend.BaseTypes.Exceptions;
using Osram.TFTBackend.ConfigurationService.Contracts;
using Osram.TFTBackend.ConfigurationService.DataModel;
using Osram.TFTBackend.DataModel.Data.Contracts;
using Osram.TFTBackend.DataModel.Feature.Contracts;
using Osram.TFTBackend.InAppPurchaseService.Contracts;
using Osram.TFTBackend.KeyService.Contracts;
using Osram.TFTBackend.PasswordService.Contracts;
using Osram.TFTBackend.ProgrammingService.Contracts;
using Plugin.Settings.Abstractions;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;
using MvvmCross.Commands;
using System.ComponentModel;

namespace Osram.T4TField.ViewModels
{
    public class FeaturesCarouselViewModel : ConnectionBaseViewModel
    {
        private readonly IConfigurationService _configurationService;
        private readonly IUserDialogs _userDialogs;
        private readonly IKeyService _keyService;
        private readonly ISettings _settings;
        private readonly IPasswordService _passwordService;
        private readonly IInAppPurchaseService _inAppPurchaseService;

        private MvxSubscriptionToken _operatingModeToken;

        public IMvxCommand PartialProgramingCommand { get; private set; }

        public FeaturesCarouselViewModel(IConfigurationService configurationService, IUserDialogs userDialogs, IKeyService keyService, ISettings settings, IPasswordService passwordService, IInAppPurchaseService inAppPurchaseService)
        {
            _configurationService = configurationService;
            _userDialogs = userDialogs;
            _keyService = keyService;
            _settings = settings;
            _passwordService = passwordService;
            _inAppPurchaseService = inAppPurchaseService;

            PositionSelectedCommand = new MvxCommand<SelectedPositionChangedEventArgs>(OnPositionChanged);
            PartialProgramingCommand = new MvxAsyncCommand(OnPartialProgramingCommand);
            featuresTemplateSelector = new FeaturesTemplateSelector();

            SubscribeToMessages();
        }

        private void InitHeadersType()
        {
            Headers = new List<HeaderCarouselObject>();
            HeaderCarouselObject headerCarousel = new HeaderCarouselObject();
            isFeaturesCarouselEditable = true;// _keyService.IsDevicePasswordProtected;// true;// FeaturesViewModels.First().IsEditable;
            headerCarousel.IsEditable = isFeaturesCarouselEditable;
            headerCarousel.type = HeaderCarousel.AstroMode;
            Headers.Add(headerCarousel);
        }

        private void SubscribeToMessages()
        {
            _operatingModeToken = _messenger.Subscribe<Events.SelectedOperatingModeEvent>(OnChangeOperatingMode);
        }

        private void UnsubscribeToMessages()
        {
            _operatingModeToken.Dispose();
        }

        private async Task OnPartialProgramingCommand()
        {

            if (CheckIfAdapterIsReady())
            {
                if (ServicePasswordRequired())
                {

                    int currentPasswordID = _settings.GetValueOrDefault("CurrentServicePasswordID", -999);

                    if (currentPasswordID != -999)
                    {
                        PasswordEntry CurrentPassword = (PasswordEntry)await _passwordService.QueryById(currentPasswordID);
                        if (await Authenticate(CurrentPassword))
                        {
                            await OpenProgrammingViewModelAsync();
                        }
                        else
                        {
                            //_userDialogs.Alert(AppResources.F_ServicePasswordNotValid);
                            await _userDialogs.AlertAsync(AppResources.F_ServicePasswordNotValid);
                            ShowViewModel<ProtectionViewModel>(new ProtectionViewModel.ProtectionViewModelParameter(true, Authenticate));
                        }
                    }
                    else
                    {
                        // no service password setted
                        //_userDialogs.Alert(AppResources.F_NoServicePasswordSetted);
                        await _userDialogs.AlertAsync(AppResources.F_NoServicePasswordSetted);
                        ShowViewModel<ProtectionViewModel>(new ProtectionViewModel.ProtectionViewModelParameter(true, Authenticate));
                    }
                }
                else
                {
                    // no service password needed
                    await OpenProgrammingViewModelAsync();
                }
            }
            else
            {
                await _userDialogs.AlertAsync(new AlertConfig().Message = GetAdapterDisabledMessage());
            }
        }

        public async Task<bool> Authenticate(PasswordEntry passwordEntry)
        {
            if (_keyService.ValidateServicePassword(passwordEntry.Password()))
            {
                _keyService.SetServicePasswordForDeviceProgramming(passwordEntry.Password());
                // service password valid, ready to be programmed
                await OpenProgrammingViewModelAsync();
                return await Task.FromResult(true);
            }
            return await Task.FromResult(false);
        }

        private async Task OpenProgrammingViewModelAsync()
        {
            if (CheckIfAdapterIsReady())
            {
                //if (await _inAppPurchaseService.CanConsumePurchaseAsync() || CurrentCarouselPage is TuningFactorViewModel)
                if (await _inAppPurchaseService.CanConsumePurchaseAsync() || (!(CurrentCarouselPage is AstroDimViewModel)))
                {
                    bool isPaidFeaturesPopupShown = settings.GetValueOrDefault(PaidFeaturesPopupViewModel.IsPaidFeaturesPopupViewModelPopupShown, false);

                    //if (isPaidFeaturesPopupShown || CurrentCarouselPage is TuningFactorViewModel)
                    if (isPaidFeaturesPopupShown || (!(CurrentCarouselPage is AstroDimViewModel)))
                    {
                        await ShowPositionGuidePopup();
                    }
                    else
                    {
                        MyShowViewModel<PaidFeaturesPopupViewModel>(RequestType.Popup, new PaidFeaturesPopupViewModel.BlankDriversPopupParameter(true)
                        {
                            Title = AppResources.F_PaidFeature,
                            Action = ShowPositionGuidePopup,
                        });
                    }
                }
                else
                {
                    _userDialogs.Alert(new AlertConfig()
                    {
                        Message = AppResources.F_ManageSubscriptionNoPremium,
                        OnAction = () => ShowViewModel<ManageSubscriptionViewModel>(),
                    });
                }
            }
            else
            {
                await _userDialogs.AlertAsync(new AlertConfig().Message = GetAdapterDisabledMessage());
            }
        }

        private async Task ShowPositionGuidePopup()
        {
            bool isPositionGuidePopupShown = settings.GetValueOrDefault(PositioningGuideViewModel.IsPositioningGuidePopupShown, false);
            if (isPositionGuidePopupShown)
            {
                await ProgramDevicePopup();
            }
            else
            {
                ShowViewModel<PositioningGuideViewModel>(new PositioningGuideViewModel.BTGuidePopupParameter(true)
                {
                    Action = ProgramDevicePopup,
                });
            }
        }

        private byte[] getServicePasswordByteArray(PasswordEntry Password)
        {
            var ServicePasswordByteArray = new byte[4] { (byte)(Password.PasswordByte1), (byte)(Password.PasswordByte2), (byte)(Password.PasswordByte3), (byte)(Password.PasswordByte4) };
            Logger.Trace(BitConverter.ToUInt32(ServicePasswordByteArray, 0) + "");
            return ServicePasswordByteArray;
        }

        private async Task ProgramDevicePopup()
        {
            MyShowViewModel<ProgrammingViewModel>(RequestType.Popup, new ProgrammingViewModel.ProgrammingParameter(true)
            {
                Title = AppResources.F_ProgrammingViewWritingPopupTitle,
                ProgrammingAction = ProgramDevice,
                ProgrammingType = ProgrammingType.Write,
                TextDialogAfterAction = AppResources.F_DialogProgrammingSuccessfulText
            });
        }

        private async Task<bool> ProgramDevice(BaseViewModel vm, IProgrammingService programmingService, CancellationToken cancellationToken)
        {
            try
            {
                //retrieve Service Password setted
                uint currentPassword = 0;
                int currentPasswordID = _settings.GetValueOrDefault("CurrentServicePasswordID", -999);
                
                if (currentPasswordID != -999)
                {
                    PasswordEntry CurrentPassword = await _passwordService.QueryById(currentPasswordID);
                    currentPassword = CurrentPassword.Password();
                }
                
                //if (CurrentCarouselPage is TuningFactorViewModel ? programmingService.IsSameFamilyDevice(currentPassword) : programmingService.IsSameDevice())
                if (programmingService.IsSameDevice())
                {
                    IEnumerable<EcgProperty> cells = _configurationService.GetConfigurationProperties();
                    programmingService.ProgramConfiguration(cells);

                    //if (!(CurrentCarouselPage is TuningFactorViewModel))
                    if (CurrentCarouselPage is AstroDimViewModel)
                    {
                        await _inAppPurchaseService.ConsumePurchaseAsync();
                    }

                    return await Task.FromResult(true);
                }
                else
                {
                    throw new OperationFailedException("CopyAndPasteViewModel.PasteDevice: incompatible devices", CurrentCarouselPage is TuningFactorViewModel ?  ErrorCodes.DifferentFamilyDevice : ErrorCodes.DifferentDevice);
                }
            }
            catch (RFPasswordFailedException rfPasswordFailedException)
            {
                Logger.Exception("CopyAndPasteViewModel.PasteDevice: RF password command failed.", rfPasswordFailedException);
                throw rfPasswordFailedException;
            }
            catch (OperationCanceledException e)
            {
                throw e;
            }
            catch (OperationFailedException operationFailedException)
            {
                Logger.Exception("CopyAndPasteViewModel.PasteDevice", operationFailedException);
                throw operationFailedException;
            }
            catch (Exception e)
            {
                Logger.Exception("ProgrammDevice", e);
            }
            return await Task.FromResult(false);
        }



        public override void Start()
        {
            base.Start();
            FeaturesViewModels = (ClassParameter as CarouselBaseParameter)?.Pages;
            AppMode = (ClassParameter as CarouselBaseParameter)?.AppMode;
            FeaturesList = (ClassParameter as CarouselBaseParameter)?.FeaturesList;
            SelectedFeature = (FeatureType)(ClassParameter as CarouselBaseParameter)?.SelectedFeature;
            Position = ((CarouselBaseParameter)ClassParameter).StartPosition;
            SetCurrentPage(Position);
            InitHeadersType();
        }

        public bool ServicePasswordRequired()
        {
            // to be change when real partial programming will be available
            IEnumerable<FeatureInfoButton> currentFeature = FeaturesList.Where(x => x.FeatureInfo.Lock == ProtectionLock.Service);
            return (currentFeature.Count() > 0);
        }

        public override Task Cleanup()
        {
            //clean up of each carousel viewmodel
            foreach (var vm in FeaturesViewModels)
            {
                vm.Cleanup();
            }

            UnsubscribeToMessages();

            return base.Cleanup();
        }

        private void SetCurrentPage(int position)
        {
            SetMode();

            CurrentCarouselPage = FeaturesViewModels.ElementAt(position);

            CurrentCarouselPage.PropertyChanged += (object sender, PropertyChangedEventArgs e) => OnCurrentCarouselPageIsEditableChanged(sender, e);

            if (CurrentCarouselPage is AstroDimViewModel || IsAdvancedMode && CurrentCarouselPage is AstroDimInformationViewModel)
            {
                IsGlobalParamBarVisible = 60;
            }
            else
            {
                IsGlobalParamBarVisible = 0;
            }

            Title = CurrentCarouselPage.Title;

            // The programming button has to be always on if the view is the AstroDim
            // due to the fact that there is the Opearating mode picker
            //if ((SelectedFeature == FeatureType.AstroDIM || SelectedFeature == FeatureType.StepDIM) && CurrentCarouselPage.IsMasterPasswordProtected)
            if (SelectedFeature == FeatureType.AstroDIM || SelectedFeature == FeatureType.StepDIM)
            {
                if (((AstroDimViewModel)CurrentCarouselPage).OperatingModeEnum == OperatingModeEnum.None)
                {
                    IsPartialProgramminBtnEnable = false;
                }
                else
                {
                    IsPartialProgramminBtnEnable = true;
                }
            }
            else
            {
                IsPartialProgramminBtnEnable = CurrentCarouselPage.IsEditable;
            }


            ShowHideProgramButtonByMode();
        }

        public void OnCurrentCarouselPageIsEditableChanged(object sender, PropertyChangedEventArgs e)
        {
            if(e.PropertyName == "IsEditable")
            {
                IsPartialProgramminBtnEnable = CurrentCarouselPage.IsEditable;
            }
        }

        private void OnChangeOperatingMode(Events.SelectedOperatingModeEvent ev)
        {
            if (SelectedFeature == FeatureType.AstroDIM || SelectedFeature == FeatureType.StepDIM)
            {
                if (ev.Mode == OperatingModeEnum.None)
                {
                    IsPartialProgramminBtnEnable = false;
                }
                else
                {
                    IsPartialProgramminBtnEnable = true;
                }
            }
        }

        private void ShowHideProgramButtonByMode()
        {
            if (IsAdvancedMode)
            {
                _isProgramButtonVisible = 0;
            }
            else
            {
                _isProgramButtonVisible = 50;
            }
        }

        private void SetMode()
        {
            if (AppMode.Equals(T4TFAppMode.ADVANCED_MODE))
            {
                IsAdvancedMode = true;
            }
            else
            {
                IsAdvancedMode = false;
            }
        }





        public bool IsAdvancedMode {
            get {
                return isAdvancedMode;
            }

            set {
                isAdvancedMode = value;

            }
        }

        public bool IsPartialProgramminBtnEnable {
            get {
                return isPartialProgramminBtnEnable;
            }

            set {
                isPartialProgramminBtnEnable = value;
                RaisePropertyChanged(() => IsPartialProgramminBtnEnable);
            }
        }




        public FeatureType SelectedFeature {
            get {
                return selectedFeature;
            }

            set {
                selectedFeature = value;

            }
        }




        public ObservableCollection<FeatureInfoButton> FeaturesList {
            get {
                return featuresList;
            }

            set {
                featuresList = value;

            }
        }


        private void OnPositionChanged(SelectedPositionChangedEventArgs pos)
        {
            SetCurrentPage((int)pos.SelectedPosition);
        }

        private FeaturesTemplateSelector featuresTemplateSelector;
        public FeaturesTemplateSelector FeaturesTemplateSelector {
            get { return featuresTemplateSelector; }

        }

        BaseFeatureViewModel _currentPage;
        public BaseFeatureViewModel CurrentCarouselPage {
            get {
                return _currentPage;
            }
            set {
                SetProperty(ref _currentPage, value);
            }
        }


        private int _position;
        public int Position {
            get {
                return _position;
            }
            set {
                SetProperty(ref _position, value);
            }
        }

        private int _isGlobalParamBarVisible;
        private int _isProgramButtonVisible;
        private T4TFAppMode? AppMode;
        private bool isAdvancedMode;
        private bool isFeaturesCarouselEditable;
        private ObservableCollection<FeatureInfoButton> featuresList;
        private FeatureType selectedFeature;
        private bool isPartialProgramminBtnEnable;

        public int IsGlobalParamBarVisible {
            get {
                return _isGlobalParamBarVisible;
            }
            set {
                SetProperty(ref _isGlobalParamBarVisible, value);

            }
        }


        public int IsProgramButtonVisible {
            get {
                return _isProgramButtonVisible;
            }
            set {
                SetProperty(ref _isProgramButtonVisible, value);

            }
        }




        public enum T4TFAppMode
        {
            ADVANCED_MODE,
            BASIC_MODE
        };


        public MvxObservableCollection<BaseFeatureViewModel> FeaturesViewModels { get; private set; }

        public ICommand PositionSelectedCommand { get; set; }

        public IList<HeaderCarouselObject> Headers { get; set; }

        public abstract class CarouselBaseParameter : BaseViewModelParameter
        {
            public CarouselBaseParameter(bool isModal, T4TFAppMode appMode, ObservableCollection<FeatureInfoButton> featuresList, FeatureType selectedFeature)
                : base(isModal)
            {
                SelectedFeature = selectedFeature;
                AppMode = appMode;
                FeaturesList = featuresList;
            }


            public T4TFAppMode AppMode;

            public FeatureType SelectedFeature { get; set; }

            //  public List<BaseFeatureViewModel> Pages { get; set; }

            public ObservableCollection<FeatureInfoButton> FeaturesList { get; set; }
            public MvxObservableCollection<BaseFeatureViewModel> Pages { get; set; }
            public int StartPosition { get; set; }
        }



        public class CarouselParameter<T> : CarouselBaseParameter where T : BaseFeatureViewModel
        {
            public CarouselParameter(T4TFAppMode appMode, ObservableCollection<FeatureInfoButton> featuresList, FeatureType selectedFeature) : base(true, appMode, featuresList, selectedFeature)
            {
                Pages = new MvxObservableCollection<BaseFeatureViewModel>
                {   
                    Mvx.IoCProvider.IoCConstruct(typeof(T)) as T
                };
            }
        }

        public class CarouselParameter<T1, T2> : CarouselBaseParameter
            where T1 : BaseFeatureViewModel
            where T2 : BaseFeatureViewModel
        {
            public CarouselParameter(T4TFAppMode appMode, ObservableCollection<FeatureInfoButton> featuresList, FeatureType selectedFeature) : base(true, appMode, featuresList, selectedFeature)
            {
                Pages = new MvxObservableCollection<BaseFeatureViewModel>
                {
                    Mvx.IoCProvider.IoCConstruct(typeof(T1)) as T1,
                    Mvx.IoCProvider.IoCConstruct(typeof(T2)) as T2
                };
            }
        }
    }

}
