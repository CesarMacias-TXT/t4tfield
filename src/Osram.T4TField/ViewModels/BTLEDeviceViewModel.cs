﻿using Plugin.BLE.Abstractions;
using Plugin.BLE.Abstractions.Contracts;
using System;

namespace Osram.T4TField.ViewModels
{
    public class BTLEDeviceViewModel:BaseViewModel
    {
        private IDevice _device;
        private bool _isConnected;
        private string _name;



        public BTLEDeviceViewModel(IDevice device)
        {
            if (device == null)
                throw new ArgumentNullException(nameof(device));


            Device = device;
            Name= Device.Name;
            _isConnected = device.State == DeviceState.Connected;
        }


        public string Name
        {
            get { return _name; }
            set
            {
                _name = value;
                RaisePropertyChanged(nameof(Name));
            }
        }


        public IDevice Device
        {
            get { return _device; }
            set
            {
                _device = value;
                 RaisePropertyChanged(nameof(Device));
            }
        }



       
    }
}