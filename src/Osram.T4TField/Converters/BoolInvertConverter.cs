﻿using System;
using System.Globalization;
using MvvmCross.Converters;
using Xamarin.Forms;

namespace Osram.T4TField.Converters
{
    public class BoolInvertConverter : MvxValueConverter<bool, bool>, IValueConverter
    {
        protected override bool Convert(bool value, Type targetType, object parameter, CultureInfo culture)
        {
            return !value;
        }
    }
}