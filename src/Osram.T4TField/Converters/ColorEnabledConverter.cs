﻿using MvvmCross.Converters;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace Osram.T4TField.Converters
{
    public class ColorEnabledConverter : MvxValueConverter<bool, Color>, IValueConverter
    {
        protected override Color Convert(bool value, Type targetType, object parameter, CultureInfo culture)
        {
            return value ? Styles.OsramColor : Styles.DarkGray;

        }
    }
}
