﻿using MvvmCross.Converters;
using System;
using System.Globalization;
using Xamarin.Forms;

namespace Osram.T4TField.Converters
{
    public class ButtonColorEnabledConverter : MvxValueConverter<bool, Color>, IValueConverter
    {
        protected override Color Convert(bool value, Type targetType, object parameter, CultureInfo culture)
        {
            return value ? Styles.OsramColor : Styles.OsramColorDisabled;

        }
    }
}
