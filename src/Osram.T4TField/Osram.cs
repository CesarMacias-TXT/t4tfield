﻿namespace Osram.T4TField
{
    public enum AppState
    {
        Resumed,
        Suspended
    }

    public static class Osram
    {
        public const string MRLicenseKey = "YL8C-VQS9-WWGZ-QZH2-PX4W-QALY-NLDP-BJAM-CNKY-FEGC-U9EW-8WAD-39WX";
        static Osram()
        {
            AppState = AppState.Resumed;
        }

        public static AppState AppState { get; set; }
    }
}