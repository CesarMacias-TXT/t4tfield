using System;
using Osram.T4TField.Contracts;
using Xamarin.Forms;

namespace Osram.T4TField.Services
{
    public class PlatformSpecificCalls : IPlatformSpecificCalls
    {
        public void BeginInvokeOnMainThread(Action action)
        {
            Device.BeginInvokeOnMainThread(action);
        }
    }
}