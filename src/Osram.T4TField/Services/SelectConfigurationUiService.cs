﻿using System.Threading.Tasks;
using MvvmCross.Plugin.Messenger;
using Osram.T4TField.Contracts;
using Osram.T4TField.Messaging;
using Osram.T4TField.ViewModels;
using Osram.TFTBackend.BaseTypes;
using Osram.TFTBackend.ConfigurationService.Contracts;
using Osram.TFTBackend.ConfigurationService.DataModel;
using Osram.TFTBackend.PersistenceService;
using Osram.TFTBackend.PersistenceService.Contracts;

namespace Osram.T4TField.Services
{
    public class SelectConfigurationUiService : ISelectConfigurationUiService
    {
        private IMvxMessenger _messenger;
        private IConfigurationService _configurationService;
        private IPersistenceService _persistenceService;
        private MvxSubscriptionToken _token;
        private TaskCompletionSource<int> _taskCompletionSource;

        public SelectConfigurationUiService(IMvxMessenger messenger, IConfigurationService configurationService, IPersistenceService persistenceService)
        {
            _messenger = messenger;
            _configurationService = configurationService;
            _persistenceService = persistenceService;
        }

        public async Task<IConfiguration> SelectConfiguration(BaseViewModel vm, EcgSelector selector)
        {
            EcgDeviceInfo deviceInfo = await _persistenceService.SelectDevice(selector);
            if (deviceInfo != null)
            {
                EcgTuple ecg = _persistenceService.GetDeviceConfiguration(deviceInfo.Id);
                return _configurationService.SelectConfiguration(ecg);
            }
            return null;
        }

        private void OnSelectedConfigurationEvent(Events.SelectedConfigurationEvent ev)
        {
         
            _taskCompletionSource?.TrySetResult(ev.SelectedConfigurationId);
        }
    }
}
