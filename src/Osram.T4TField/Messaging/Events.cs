﻿using MvvmCross.Plugin.Messenger;
using Osram.TFTBackend.DataModel.Feature.Contracts;
using static Osram.TFTBackend.LocationService.EcgLocation;
using Osram.TFTBackend.DataModel.Data.Contracts;

namespace Osram.T4TField.Messaging
{
    public class Events
    {
        public class OnLanguageChangedEvent : MvxMessage
        {
            public OnLanguageChangedEvent(object sender) : base(sender) { }
        }

        public class SelectedConfigurationEvent : MvxMessage
        {
            public int SelectedConfigurationId { get; }

            public SelectedConfigurationEvent(object sender, int selectedConfigurationId) : base(sender)
            {
                SelectedConfigurationId = selectedConfigurationId;
            }
        }

        public class ChangedAstroDimFadeTimeEvent : MvxMessage
        {
            public ChangedAstroDimFadeTimeEvent(object sender) : base(sender)
            {
            }
        }

        public class SelectedAstroModeEvent : MvxMessage
        {
            public AstroDimMode Mode {get;}
            public SelectedAstroModeEvent(object sender, AstroDimMode mode) : base(sender)
            {
                Mode = mode;
            }
        }

        public class SelectedOperatingModeEvent : MvxMessage
        {
            public OperatingModeEnum Mode { get; }
            public SelectedOperatingModeEvent(object sender, OperatingModeEnum mode) : base(sender)
            {
                Mode = mode;
            }
        }

        public class SelectedAstroLocationEvent : MvxMessage
        {
            public Location Location { get; }
            public SelectedAstroLocationEvent(object sender, Location loc) : base(sender)
            {
                Location = loc;
            }
        }
    }
}
