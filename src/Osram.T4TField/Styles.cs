﻿using Xamarin.Forms;

namespace Osram.T4TField
{
    public static class Styles
    {
        public static readonly string OsramColorHexaString = "#e95b0d";

        public static readonly Thickness CellContentPadding = Device.RuntimePlatform == Device.Android
            ? new Thickness(5)
            : new Thickness(10, 5, 5, 5);

        public static readonly Thickness SelectableCellContentPadding = new Thickness(5, 5, 15, 5);
        public static readonly Thickness HeaderCellContentPadding = new Thickness(5, 5, 5, 5);

        public static readonly Color White = Color.FromRgb(0xFF, 0xFF, 0xFF);

        public static readonly Color OsramColor = Color.FromRgb(0xE9, 0x5B, 0x0D);
        public static readonly Color OsramColorDisabled = Color.FromRgb(0xF4, 0xB2, 0x8F);
        public static readonly Color OsramColorSelected = Color.FromHex("#c74e0b");

        public static readonly Color ButtonTextColor = Color.FromRgb(0xE9, 0x5B, 0x0D);
        public static readonly Color ButtonTintColor = Color.FromRgb(0xE9, 0x5B, 0x0D);
        //public static readonly Color OsramColor = Color.FromRgb(233, 91, 13);
        public static readonly GridLength ConnectionStatusIndicatorHeight = new GridLength(3, GridUnitType.Absolute);
        public static readonly Color CellTitleColor = Color.FromRgb(0xD0, 0xD0, 0xD0);
        public static readonly Color CellBackgroundColor = Color.FromRgb(0xE0, 0xE0, 0xE0);
        public static readonly double CellTitleHeight = 50;

        public static readonly Color BlackColor = Color.FromRgb(0x00, 0x00, 0x00);
        public static readonly Color MainDarkTextColor = Color.FromRgba(0, 0, 0, 0.87);
        public static readonly Color SecondaryDarkTextColor = Color.FromRgba(0, 0, 0, 0.54);
        public static readonly Color DisabledDarkTextColor = Color.FromRgba(0, 0, 0, 0.38);
        public static readonly Color DividerDarkColor = Color.FromRgba(0, 0, 0, 0.12);

        public static readonly Color MainLightTextColor = Color.FromRgba(1, 1, 1, 1);
        public static readonly Color SecondaryLightTextColor = Color.FromRgba(1, 1, 1, 0.70);
        public static readonly Color DisabledLightTextColor = Color.FromRgba(1, 1, 1, 0.50);
        public static readonly Color DividerLightColor = Color.FromRgba(1, 1, 1, 0.12);

        public static readonly Color SliderWheelTextShadow = Color.FromRgb(0xE9, 0x5B, 0x0D);
        public static readonly Color SliderWheelText = Color.FromRgb(0xE9, 0x5B, 0x0D);
        public static readonly Color SelectableCheckMarkColor = Color.FromRgb(0xE9, 0x5B, 0x0D);
        public static readonly Color TabBarTintColor = Color.FromRgb(0xE9, 0x5B, 0x0D);
        public static readonly Color DividerColor = Color.FromRgba(0, 0, 0, 0.87);
        public static readonly Color ActivityIndicatorColor = Color.FromRgba(0, 0, 0, 0.87);
        public static readonly Color MainMenuBackGround = Color.White;

        public static readonly Color DarkGray = Color.FromHex("979797");
        public static readonly Color StakckLayoutBtnDisable = Color.FromHex("eeeeee");
    }
}
