﻿using MvvmCross.ViewModels;
using MvvmCross.IoC;
using MvvmCross;
using Osram.TFTBackend;
using System.Threading.Tasks;

namespace Osram.T4TField
{
    public class OsramMvxApp : MvxApplication
    {
        public override void Initialize()
        {
            BackendSetup.InitIoC();

            CreatableTypes()
                .EndingWith("Service")
                .AsInterfaces()
                .RegisterAsLazySingleton();

            // Start
            RegisterCustomAppStart<AppStart>();
        }


        protected override IMvxViewModelLocator CreateDefaultViewModelLocator()
        {
            var presenter = new MyViewModelLocator();
            Mvx.IoCProvider.RegisterSingleton<IMvxViewModelLocator>(presenter);
            Mvx.IoCProvider.RegisterSingleton<IMvxViewModelLoader>(new ViewPresenter.MvxViewModelLoader(Mvx.IoCProvider.Resolve<IMvxViewModelLocatorCollection>()));

            return presenter;
        }

        public override Task Startup()
        {
            // TODO check if Startup() is really need it

            return Task.CompletedTask;
        }
    }
}