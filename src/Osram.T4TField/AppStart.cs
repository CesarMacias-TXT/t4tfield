﻿using MvvmCross.ViewModels;
using MvvmCross.Navigation;
using System.Threading.Tasks;
using Osram.T4TField.ViewModels;

namespace Osram.T4TField
{
    public class AppStart : MvxAppStart
    {
        private readonly IMvxNavigationService _navigationService;
        
        public AppStart(
            IMvxApplication application,
            IMvxNavigationService navigationService
            )
            : base(application, navigationService)
        {
            _navigationService = navigationService;
            // NavigateToFirstViewModel(_navigationService);
        }
        
        protected override async Task NavigateToFirstViewModel(object hint = null)
        {
            //var isUserLoggedIn = _authenticationService.
            await _navigationService.Navigate<MasterDetailViewModel>();
        }
        
        /*protected override async Task NavigateToFirstViewModel(object hint = null)
        {
            try
            {
                // You need to run Task sync otherwise code would continue before completing.
                var tcs = new TaskCompletionSource<bool>();
                //Task.Run(async () => tcs.SetResult(await _authenticationService.AuthenticateAsync()));
                //var isAuthenticated = tcs.Task.Result;
                var isAuthenticated = true;

                if (isAuthenticated)
                {
                    //You need to Navigate sync so the screen is added to the root before continuing.
                    NavigationService.Navigate<MasterDetailViewModel>().GetAwaiter().GetResult();
                }
                else
                {
                    NavigationService.Navigate<LoginViewModel>().GetAwaiter().GetResult();
                }
            }
            catch (System.Exception exception)
            {
                throw exception.MvxWrap("Problem navigating to ViewModel {0}", typeof(MasterDetailViewModel).Name);
            }
        }
        */
    }
}