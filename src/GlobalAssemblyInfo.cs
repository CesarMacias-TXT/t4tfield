﻿using System.Reflection;
using System.Resources;
using System.Runtime.InteropServices;

[assembly: AssemblyCompany("Osram")]
[assembly: AssemblyProduct("T4TField")]
[assembly: AssemblyCopyright("Copyright © OSRAM 2020")]
[assembly: NeutralResourcesLanguage("en")]
[assembly: ComVisible(false)]