﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Osram.TFTBackend.NfcService.ISOStandard;
using Osram.TFTBackend.NfcService.NfcDataTypes;
using Osram.TFTBackend.BluetoothService;
using System.Text;
using Osram.TFTBackend.Tertium;

namespace TertiumTest
{
    [TestClass]
    public class TertiumCommand
    {
        private static TertiumLib lib;

        [ClassInitialize]
        public static void InitialiseTertiumLib(TestContext context)
        {      
            lib = new TertiumLib();                      
        }

        [TestMethod]
        [TestCategory("Command Wrapping")]
        public void ReadingCommand()
        {
            //address 0
            ByteArray cmdReading = ISO15693_Utils.CreateReadSingleBlockCommand(0);
            byte[] res = lib.CreateTertiumCommandFromIsoStandard(cmdReading);
            byte[] resExpected = Encoding.UTF8.GetBytes("#:0A200000\r\n");
            CollectionAssert.AreEqual(resExpected, res);

            //address 10
            cmdReading = ISO15693_Utils.CreateReadSingleBlockCommand(10);
            res = lib.CreateTertiumCommandFromIsoStandard(cmdReading);
            resExpected = Encoding.UTF8.GetBytes("#:0A200A00\r\n");
            CollectionAssert.AreEqual(resExpected, res);

            //address 500
            cmdReading = ISO15693_Utils.CreateReadSingleBlockCommand(500);
            res = lib.CreateTertiumCommandFromIsoStandard(cmdReading);
            resExpected = Encoding.UTF8.GetBytes("#:0A20F401\r\n");
            CollectionAssert.AreEqual(resExpected, res);
        }

        [TestMethod]
        [TestCategory("Command Wrapping")]
        public void WritingCommand()
        {
            //block 0 data 01 01 01 01
            ByteArray cmdWriting = ISO15693_Utils.CreateWriteSingleBlockCommand(0, new byte[] { 1, 1, 1, 1 });
            byte[] res = lib.CreateTertiumCommandFromIsoStandard(cmdWriting);
            byte[] resExpected = Encoding.UTF8.GetBytes("#:0A21000001010101\r\n");
            CollectionAssert.AreEqual(resExpected, res);

            //block 10 data 01 02 03 04
            cmdWriting = ISO15693_Utils.CreateWriteSingleBlockCommand(10, new byte[] { 1, 2, 3, 4 });
            res = lib.CreateTertiumCommandFromIsoStandard(cmdWriting);
            resExpected = Encoding.UTF8.GetBytes("#:0A210A0001020304\r\n");
            CollectionAssert.AreEqual(resExpected, res);
        }


        [TestMethod]
        [TestCategory("Command Wrapping")]
        public void InventoryCommand()
        {
            //block 0 data 01 01 01 01
            ByteArray cmdInventory = ISO15693_Utils.CreateInventoryCommand();
            byte[] res = lib.CreateTertiumCommandFromIsoStandard(cmdInventory);
            byte[] resExpected = Encoding.UTF8.GetBytes("#:260100\r\n");
            CollectionAssert.AreEqual(resExpected, res);
        }
    }
}
