﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using MvvmCross.Plugins.Messenger;
using MvvmCross.Test.Core;
using Ploeh.AutoFixture;
using Ploeh.AutoFixture.AutoMoq;
using Plugin.BLE.Abstractions.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TertiumTest;
using TertiumTest.Mocks;
using Osram.TFTBackend.BluetoothService;
using Osram.TFTBackend.BluetoothService.Contracts;
using Osram.TFTBackend.NfcService;
using Osram.TFTBackend.NfcService.Contracts;
using Osram.TFTBackend.NfcService.ISOStandard;
using Osram.TFTBackend.NfcService.NfcDataTypes;
using Osram.TFTBackend.Tertium;

namespace TertiumTest
{
    [TestClass]
    public class BluetoothOperations : MvxIoCSupportingTest
    {
        private static Fixture fixture = new Fixture();
        private static BluetoothNfcReader blReader;
        private NfcReader reader;
        private TertiumLib lib = new TertiumLib();
        
        private static AdapterMock _adapterMock;

        [ClassInitialize]
        public static void BluetoothInit(TestContext context)
        {
            fixture.Customize(new AutoMoqCustomization());
            fixture.Freeze<IMvxMessenger>();            
        }

        private static void InitBluetoothLowLevelMocks()
        {
            _adapterMock = new AdapterMock();
            var filterTertium = new byte[] { 23, 95, 143, 35, 165, 112, 73, 189, 150, 39, 129, 90, 106, 39, 222, 42 };
            var listIdDevices = new List<Guid>();
            listIdDevices.Add(new Guid(filterTertium));
            _adapterMock.ConfigureIds(listIdDevices);
            _adapterMock.ScanTimeout = 0;
            fixture.Inject<IBluetoothLE>(new BluetoothLEMock(_adapterMock));
            
        }

        [TestInitialize]
        public void Initialise()
        {
            base.Setup();
            InitBluetoothLowLevelMocks();
        }

        /// <summary>
        /// //Init the mock reader to reply with the param string
        /// </summary>
        /// <param name="retryCount">how many times retry the operation</param>
        /// <param name="values">//first two chars represent the error/succed code</param>
        private void InitReaderWithResponse(int retryCount, params string[] values)
        {
            var responseMock = new ResponseQueueMock();
            responseMock.SetResponse(values);
            fixture.Inject<IResponseQueue>(responseMock);
            blReader = fixture.Create<BluetoothNfcReader>();
            Ioc.RegisterSingleton<INfcCommunication>(blReader);
            reader = fixture.Create<NfcReader>();
            blReader.RetryCount = retryCount;
            reader.RetryCount = retryCount;
        }

        private void InitReaderWithResponse(params string[] values)
        {
            InitReaderWithResponse(0, values);
        }

        private async Task StartScanAndConnect()
        {
            blReader.DeviceDiscovered += async (e, device) =>
             await blReader.ConnectDeviceAsync(device)
            ;
            await blReader.StartScanningForDevicesAsync();

        }

        private DeviceMock GetDevice()
        {
            return ((DeviceMock)_adapterMock.ConnectedDevices.First());
        }

        [TestMethod]
        [TestCategory("Detecting")]
        public void WHEN_BluetoothNfcReader_detects_a_tag_THEN_it_raises_TagDetected_event_and_should_has_tag_serial_number()
        {
            //Arrange
            InitReaderWithResponse("00FF8877665544332211");
            int tagDetectedCount = 0;
            reader.TagDetected += ((e, id) => tagDetectedCount++);

            //Act
            StartScanAndConnect().Wait();
            reader.DetectTag().Wait();

            //Assert
            Assert.AreEqual(reader.NfcTagSerial, "11-22-33-44-55-66-77-88");
            Assert.AreEqual(tagDetectedCount, 1);
        }


        [TestMethod]
        [TestCategory("Detecting")]
        public void WHEN_BluetoothNfcReader_does_not_detect_a_tag_THEN_it_does_not_raise_TagDetected_event_and_should_not_has_tag_serial_number()
        {
            //Arrange
            InitReaderWithResponse("03");
            int tagDetectedCount = 0;
            reader.TagDetected += ((e, id) => tagDetectedCount++);

            //Act
            StartScanAndConnect().Wait();
            reader.DetectTag().Wait();

            //Assert
            Assert.AreEqual(reader.NfcTagSerial, null);
            Assert.AreEqual(tagDetectedCount, 0);
        }

        [TestMethod]
        [TestCategory("Reading")]
        public void WHEN_BluetoothNfcReader_read_failed_THEN_exception_raises_and_no_bytes_are_read()
        {
            //Arrange
            InitReaderWithResponse("03");
            int readFailedCount = 0;
            ByteArray resp = null;

            //Act
            StartScanAndConnect().Wait();
            try
            {
                resp = reader.Read(0, 5);
            }
            catch
            {
                readFailedCount++;
            }

            //Assert
            Assert.AreEqual(1, readFailedCount);
            Assert.AreEqual(resp, null);
        }

        [TestMethod]
        [TestCategory("Reading")]
        public void WHEN_BluetoothNfcReader_one_block_read_succeded_THEN_no_exception_raises_and_bytes_are_read()
        {
            //Arrange
            InitReaderWithResponse("000C03AFA3");
            int readFailedCount = 0;
            ByteArray resp = null;
            string expected = "0C 03 AF A3";
            
            //Act
            StartScanAndConnect().Wait();
            try
            {
                resp = reader.Read(0, 4);
            }
            catch
            {
                readFailedCount++;
            }

            //Assert
            Assert.AreEqual(0, readFailedCount);
            Assert.AreEqual(resp.ToString(), expected);
        }


        [TestMethod]
        [TestCategory("Reading")]
        public void WHEN_BluetoothNfcReader_more_blocks_read_succeded_THEN_no_exception_raises_and_bytes_are_read()
        {
            //Arrange
            InitReaderWithResponse("000C03AFA3", "0011223344");
            int readFailedCount = 0;
            ByteArray resp = null;
            string expected = "0C 03 AF A3 11 22 33";
            
            //Act
            StartScanAndConnect().Wait();
            try
            {
                resp = reader.Read(0, 7);
            }
            catch
            {
                readFailedCount++;
            }

            //Assert
            Assert.AreEqual(0, readFailedCount);
            Assert.AreEqual(resp.ToString(), expected);
        }

        [TestMethod]
        [TestCategory("Reading")]
        public void WHEN_BluetoothNfcReader_read_fails_are_less_then_retryCount_THEN_read_succeds()
        {
            //Arrange
            int retryCount = 2;
            InitReaderWithResponse(retryCount, "000C03AFA3", "03" , "0011223344");
            int readFailedCount = 0;
            ByteArray resp = null;
            string expected = "0C 03 AF A3 11";
            
            //Act
            StartScanAndConnect().Wait();
            try
            {
                resp = reader.Read(0, 5);
            }
            catch
            {
                readFailedCount++;
            }

            //Assert
            Assert.AreEqual(0, readFailedCount);
            Assert.AreEqual(resp.ToString(), expected);
        }

        [TestMethod]
        [TestCategory("Reading")]
        public void WHEN_BluetoothNfcReader_read_fails_are_greater_then_retryCount_THEN_read_failed()
        {
            //Arrange
            int retryCount = 1;
            InitReaderWithResponse(retryCount, "000C03AFA3", "03", "03", "0011223344");
            int readFailedCount = 0;
            ByteArray resp = null;

            //Act
            StartScanAndConnect().Wait();
            try
            {
                resp = reader.Read(0, 5);
            }
            catch
            {
                readFailedCount++;
            }

            //Assert
            Assert.AreEqual(1, readFailedCount);
            Assert.AreEqual(resp, null);
        }

        [TestMethod]
        [TestCategory("Reading")]
        public void WHEN_BluetoothNfcReader_read_fails_are_equal_to_retryCount_THEN_read_failed()
        {
            //Arrange
            int retryCount = 2;
            InitReaderWithResponse(retryCount, "000C03AFA3", "03", "03", "0011223344");
            int readFailedCount = 0;
            ByteArray resp = null;

            //Act
            StartScanAndConnect().Wait();
            try
            {
                resp = reader.Read(0, 5);
            }
            catch
            {
                readFailedCount++;
            }

            //Assert
            Assert.AreEqual(1, readFailedCount);
            Assert.AreEqual(resp, null);
        }


        [TestMethod]
        [TestCategory("Writing")]
        public void WHEN_BluetoothNfcReader_write_failed_THEN_exception_raises_and_no_bytes_are_written()
        {
            //Arrange
            InitReaderWithResponse("03");
            int writeFailedCount = 0;
            ByteArray data = new ByteArray(1, 4);

            //Act
            StartScanAndConnect().Wait();

            try
            {
               reader.Write(0, data);
            }
            catch
            {
                writeFailedCount++;
            }

            //Assert
            Assert.AreEqual(1, writeFailedCount);          
        }

        [TestMethod]
        [TestCategory("Writing")]
        public void WHEN_BluetoothNfcReader_one_block_write_succeded_THEN_no_exception_raises()
        {
            //Arrange
            InitReaderWithResponse("00");
            int writeFailedCount = 0;
            ByteArray data = new ByteArray(1, 4);
            byte[] writtenBlockExpected = lib.CreateTertiumCommandFromIsoStandard(
                ISO15693_Utils.CreateWriteSingleBlockCommand(0, new byte[] { 1, 1, 1, 1 }));
            byte[] writtenBlockActual = null;
            
            //Act
            StartScanAndConnect().Wait();
            DeviceMock device = GetDevice();
            device.ValueWritten += ((sender, array) =>
            {
                writtenBlockActual = array;            
            });

            try
            {
                reader.Write(0, data);
            }
            catch
            {
                writeFailedCount++;
            }

            //Assert
            Assert.AreEqual(0, writeFailedCount);
            CollectionAssert.AreEqual(writtenBlockExpected, writtenBlockActual);
        }

        [TestMethod]
        [TestCategory("Writing")]
        public void WHEN_BluetoothNfcReader_more_blocks_write_succeded_THEN_no_exception_raises_and_filling_memory_bytes_read()
        {
            #region Arrange
            //3 responses -> first block reading filling bytes, first block writing success, second block writing success
            InitReaderWithResponse("0099999999", "00", "00");
            int writeFailedCount = 0, writingCount = 0;
            ByteArray data = new ByteArray(1, 6);         
            byte[] firstWrittenBlockExpected = lib.CreateTertiumCommandFromIsoStandard(
                ISO15693_Utils.CreateWriteSingleBlockCommand(0, new byte[] { 1, 1, 1, 1}));
            byte[] secondWrittenBlockExpected = lib.CreateTertiumCommandFromIsoStandard(
                ISO15693_Utils.CreateWriteSingleBlockCommand(0, new byte[] {1, 1, 99, 99 }));
            #endregion

            #region Act
            StartScanAndConnect().Wait();
            DeviceMock device = GetDevice();
            device.ValueWritten += ((sender, array) =>
            {
                //Assert
                switch(writingCount)
                {
                    case 1: CollectionAssert.AreEqual(firstWrittenBlockExpected, array); break;
                    case 2: CollectionAssert.AreEqual(secondWrittenBlockExpected, array); break;
                    default: break;
                }
                writingCount++;      
            });
            try
            {
                //write two blocks because 6 > blockSize = 4
                reader.Write(0, data);
            }
            catch
            {
                writeFailedCount++;
            }
            #endregion

            #region Assert
            Assert.AreEqual(0, writeFailedCount);
            //other assertions are in the Act phase
            #endregion
        }

        [TestMethod]
        [TestCategory("Writing")]
        public void WHEN_BluetoothNfcReader_write_fails_are_less_then_retryCount_THEN_write_succeds()
        {
            //Arrange
            int retryCount = 2;
            InitReaderWithResponse(retryCount, "000C03AFA3", "03", "0011223344");
            int failedCount = 0;
            ByteArray data = new ByteArray(1, 8);

            //Act
            StartScanAndConnect().Wait();
            try
            {
                reader.Write(0, data);
            }
            catch
            {
                failedCount++;
            }

            //Assert
            Assert.AreEqual(0, failedCount);
        }

        [TestMethod]
        [TestCategory("Writing")]
        public void WHEN_BluetoothNfcReader_write_fails_are_greater_then_retryCount_THEN_write_failed()
        {
            //Arrange
            int retryCount = 1;
            InitReaderWithResponse(retryCount, "000C03AFA3", "03", "03", "0011223344");
            int failedCount = 0;
            ByteArray data = new ByteArray(1, 8);

            //Act
            StartScanAndConnect().Wait();
            try
            {
                reader.Write(0, data);
            }
            catch
            {
                failedCount++;
            }

            //Assert
            Assert.AreEqual(1, failedCount);
        }

        [TestMethod]
        [TestCategory("Writing")]
        public void WHEN_BluetoothNfcReader_write_fails_are_equal_to_retryCount_THEN_write_failed()
        {
            //Arrange
            int retryCount = 2;
            InitReaderWithResponse(retryCount, "000C03AFA3", "03", "03", "0011223344");
            int failedCount = 0;
            ByteArray data = new ByteArray(1, 8);

            //Act
            StartScanAndConnect().Wait();
            try
            {
                reader.Write(0, data);
            }
            catch
            {
                failedCount++;
            }

            //Assert
            Assert.AreEqual(1, failedCount);
        }

    }
}
