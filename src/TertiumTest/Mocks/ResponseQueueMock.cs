﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TertiumTest.Mocks;
using Osram.TFTBackend.BluetoothService.Contracts;
using Osram.TFTBackend.NfcService.NfcDataTypes;

namespace TertiumTest.Mocks
{
    class ResponseQueueMock : IResponseQueue
    {
        Queue<string> responses = new Queue<string>();

        public ResponseQueueMock()
        {
        }

        public bool IsCompleted { get; set; }


        public void Add(byte[] item)
        {
            
        }

        public ByteArray GetResponseBytes()
        {
            return ByteArray.FromString(responses.Dequeue());
        }

        public void ResetQueue()
        {
            //
        }

        public void SetResponse(string[] values)
        {
            foreach (var response in values)
            {
                responses.Enqueue(PadStringInCouples(response));
            } 
            IsCompleted = true;
        }


        private string PadStringInCouples(string asciiString)
        {
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < asciiString.Length; i += 2)
            {
                sb.Append(asciiString.Substring(i, 2));
                sb.Append(" ");
            }
            return sb.ToString();
        }
    }
}
