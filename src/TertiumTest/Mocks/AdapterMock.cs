﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Plugin.BLE.Abstractions;
using Plugin.BLE.Abstractions.Contracts;
using Plugin.BLE.Abstractions.EventArgs;

namespace TertiumTest
{
    public class BluetoothLEMock : IBluetoothLE
    {
        public BluetoothState State { get; private set; }
        public bool IsAvailable { get; private set; }
        public bool IsOn { get; private set; }
        public IAdapter Adapter { get; private set; }
#pragma warning disable CS0067
        public event EventHandler<BluetoothStateChangedArgs> StateChanged;
#pragma warning restore CS0067

        public BluetoothLEMock(AdapterMock adapter)
        {
            Adapter = adapter;
        }
    }

    public class AdapterMock : IAdapter
    {
        private List<Guid> _idList;

        public AdapterMock()
        {
            DiscoveredDevices = new List<IDevice>();
            ConnectedDevices = new List<IDevice>();
        }

        public async Task StartScanningForDevicesAsync(Guid[] serviceUuids = null, Func<IDevice, bool> deviceFilter = null, bool allowDuplicates = false,
            CancellationToken cancellationToken = default(CancellationToken))
        {  
            await StartScanningForDevices();
        }

        public Task StopScanningForDevicesAsync()
        {
            IsScanning = false;
            return Task.FromResult(true);
        }

        public Task ConnectToDeviceAsync(IDevice device, ConnectParameters connectParameters = default(ConnectParameters), CancellationToken cancellationToken = default(CancellationToken))
        {
            ConnectedDevices.Add(device);

            DeviceConnected?.Invoke(this, new DeviceErrorEventArgs { Device = device });

            return Task.FromResult(true);
        }


        public Task DisconnectDeviceAsync(IDevice device)
        {
            if (ConnectedDevices.Contains(device))
                ConnectedDevices.Remove(device);

            DeviceDisconnected?.Invoke(this, new DeviceErrorEventArgs { Device = device });

            return Task.FromResult(true);
        }

        public bool IsScanning { get; private set; }
        public int ScanTimeout { get; set; }
        public IList<IDevice> DiscoveredDevices { get; private set; }
        public IList<IDevice> ConnectedDevices { get; private set; }

        public ScanMode ScanMode
        {
            get
            {
                throw new NotImplementedException();
            }

            set
            {
                throw new NotImplementedException();
            }
        }

        
        //public event EventHandler<DeviceErrorEventArgs> DeviceConnectionError;
        //public event EventHandler<DeviceBondStateChangedEventArgs> DeviceBondStateChanged;

        public event EventHandler<DeviceEventArgs> DeviceDiscovered;
        public event EventHandler<DeviceEventArgs> DeviceConnected;
        public event EventHandler<DeviceEventArgs> DeviceDisconnected;
#pragma warning disable CS0067
        public event EventHandler<DeviceEventArgs> DeviceAdvertised;
        public event EventHandler<DeviceErrorEventArgs> DeviceConnectionLost;
#pragma warning restore CS0067
        public event EventHandler ScanTimeoutElapsed;

        private async Task StartScanningForDevices()
        {
            IsScanning = true;

            DiscoveredDevices.Clear();

            for (var i = 0; i < _idList.Count; i++)
            {
                if (DiscoveredDevices.All(discoveredDevice => discoveredDevice.Id != _idList[i]) &&
                    ConnectedDevices.All(connectedDevice => connectedDevice.Id != _idList[i]))
                {

                    DeviceMock tempDevice = new DeviceMock($"Device {i}", _idList[i]);        
                    DiscoveredDevices.Add(tempDevice);

                    DeviceDiscovered?.Invoke(this, new DeviceEventArgs { Device = tempDevice });
                }
            }

            await Task.Delay(ScanTimeout);

            IsScanning = false;
            ScanTimeoutElapsed?.Invoke(this, EventArgs.Empty);
        }

        public void ConfigureIds(List<Guid> ids)
        {
            _idList = ids;
        }

        public Task<IDevice> ConnectToKnownDeviceAsync(Guid deviceGuid, ConnectParameters connectParameters = default(ConnectParameters), CancellationToken cancellationToken = default(CancellationToken))
        {
            throw new NotImplementedException();
        }

        public List<IDevice> GetSystemConnectedOrPairedDevices(Guid[] services = null)
        {
            throw new NotImplementedException();
        }
    }
}