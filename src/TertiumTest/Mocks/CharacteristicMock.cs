﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Plugin.BLE.Abstractions;
using Plugin.BLE.Abstractions.Contracts;
using Plugin.BLE.Abstractions.EventArgs;
using Plugin.BLE.Abstractions.Extensions;

namespace TertiumTest
{
    public class CharacteristicMock : ICharacteristic
    {
        private bool _areUpdatesEnabled;
        private byte[] _value;

        public CharacteristicMock(Guid id, string name = "MyCharacteristic", bool canRead = true, bool canWrite = true, bool canUpdate = true)
        {
            Id = id;
            Name = name;
            CanRead = canRead;
            CanUpdate = canUpdate;
            CanWrite = canWrite;
            Value = new byte[0];
        }

        public Task StartUpdatesAsync()
        {
            _areUpdatesEnabled = true;
            return Task.FromResult(true);
        }

        public Task StopUpdatesAsync()
        {
            _areUpdatesEnabled = false;
            return Task.FromResult(true);
        }

        public Task<byte[]> ReadAsync()
        {
            return Task.FromResult(Value);
        }


        public void Write(byte[] data)
        {
            Value = data;
            ValueUpdated?.Invoke(this, new CharacteristicUpdatedEventArgs(this));
            ValueWritten?.Invoke(this, data);
        }

       

        public Task<byte[]> ReadAsync(CancellationToken cancellationToken = default(CancellationToken))
        {
            return Task.FromResult(Value);
        }

        public Task<bool> WriteAsync(byte[] data, CancellationToken cancellationToken = default(CancellationToken))
        {
            Write(data);
            return Task.FromResult(true);
        }

        public Task<IList<IDescriptor>> GetDescriptorsAsync(CancellationToken cancellationToken = default(CancellationToken))
        {
            throw new NotImplementedException();
        }

        public Task<IDescriptor> GetDescriptorAsync(Guid id, CancellationToken cancellationToken = default(CancellationToken))
        {
            throw new NotImplementedException();
        }

        public Guid Id { get; }

        public string Uuid => Id.ToString();

        public byte[] Value
        {
            get { return _value; }
            protected set
            {
                _value = value;
                if (_areUpdatesEnabled)
                {
                    ValueUpdated?.Invoke(this, new CharacteristicUpdatedEventArgs(this));
                }
            }
        }

        public string StringValue => Value.ToHexString();

        public IList<IDescriptor> Descriptors => new IDescriptor[0];

        public object NativeCharacteristic => null;

        public string Name { get; }

        public CharacteristicPropertyType Properties => CharacteristicPropertyType.Read | CharacteristicPropertyType.WriteWithoutResponse;

        public bool CanRead { get; }
        public bool CanWrite { get; }
        public bool CanUpdate { get; }

        public CharacteristicWriteType WriteType
        {
            get { return CharacteristicWriteType.Default; }
            set { }
        }

        Plugin.BLE.Abstractions.Contracts.IService ICharacteristic.Service
        { get; }

        public event EventHandler<CharacteristicUpdatedEventArgs> ValueUpdated;
        public event EventHandler<byte[]> ValueWritten;
    }
}