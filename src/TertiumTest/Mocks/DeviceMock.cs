﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Plugin.BLE.Abstractions;
using Plugin.BLE.Abstractions.Contracts;
using Ploeh.AutoFixture;
using Ploeh.AutoFixture.AutoMoq;
using System.Threading;

namespace TertiumTest
{
    /// <summary>
    /// Simple device without any services or characteristics
    /// </summary>
    public class DeviceMock : IDevice
    {
        Fixture fixture = new Fixture();
        CharacteristicMock writeCharacteristic, readCharacteristic;

        public DeviceMock(string name, Guid id = default(Guid), DeviceState state = DeviceState.Connected)
        {
            Id = id;
            Name = name;
            State = state;

            AdvertisementRecords = new List<AdvertisementRecord>
            {
                new AdvertisementRecord(AdvertisementRecordType.UuidsComplete128Bit, GetGoodBytes(id))
            };

            //add services and characteristic
            writeCharacteristic = new CharacteristicMock(new Guid("1cce1ea8-bd34-4813-a00a-c76e028fadcb"));
            readCharacteristic = new CharacteristicMock(new Guid("cacc07ff-ffff-4c48-8fae-a9ef71b75e26"));
            writeCharacteristic.ValueWritten += WriteCharacteristic_ValueWritten;

            IList<IService> services = new List<IService>();
            //add right service
            var service = new ServiceMock(new Guid("175f8f23-a570-49bd-9627-815a6a27de2a"));
            service.Characteristics.Add(writeCharacteristic);
            service.Characteristics.Add(readCharacteristic);
            services.Add(service);

            //add automatic fake services
            fixture.Customize(new AutoMoqCustomization());
            fixture.AddManyTo(services, 3);

            Services = services;
        }

        private void WriteCharacteristic_ValueWritten(object sender, byte[] e)
        {
            ValueWritten?.Invoke(this, e);
        }

        public Guid Id { get; }

        public Task<IList<IService>> GetServicesAsync()
        {
            return Task.FromResult(Services);
        }

        public Task<IService> GetServiceAsync(Guid id, CancellationToken cancellationToken = default(CancellationToken))
        {
            return Task.FromResult(Services.FirstOrDefault(s => s.Id.Equals(id)));
        }

        public Task<bool> UpdateRssiAsync()
        {
            return Task.FromResult(true);
        }

        public void Dispose()
        {
            writeCharacteristic.ValueWritten -= WriteCharacteristic_ValueWritten;
        }

        public string Name { get; }
        public int Rssi { get; }
        public object NativeDevice { get; private set; }
        public DeviceState State { get; private set; }

        public IList<AdvertisementRecord> AdvertisementRecords { get; }
        public IList<IService> Services { get; private set; }

        protected byte[] GetGoodBytes(Guid guid)
        {
            var hex = guid.ToString().Replace("-", "");
            return Enumerable.Range(0, hex.Length)
                .Where(x => x % 2 == 0)
                .Select(x => Convert.ToByte(hex.Substring(x, 2), 16))
                .ToArray();
        }

        public Task<IList<IService>> GetServicesAsync(CancellationToken cancellationToken = default(CancellationToken))
        {
            throw new NotImplementedException();
        }

    
        public Task<int> RequestMtuAsync(int requestValue)
        {
            throw new NotImplementedException();
        }

        public bool UpdateConnectionInterval(ConnectionInterval interval)
        {
            throw new NotImplementedException();
        }

        public event EventHandler<byte[]> ValueWritten;

    }
}