﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Plugin.BLE.Abstractions.Contracts;

namespace TertiumTest
{
    public class ServiceMock : IService
    {
        public ServiceMock(Guid id, string name = "MyService", bool isPrimary = true)
        {
            Id = id;
            Name = name;
            IsPrimary = isPrimary;

            Characteristics = new List<ICharacteristic>();
        }

      
        public Guid Id { get; private set; }

        public Task<IList<ICharacteristic>> GetCharacteristicsAsync()
        {
            return Task.FromResult(Characteristics);
        }

        public Task<ICharacteristic> GetCharacteristicAsync(Guid id)
        {
            return Task.FromResult(Characteristics.FirstOrDefault(c => c.Id == id));
        }

        public void Dispose()
        {
            throw new NotImplementedException();
        }

        public string Name { get; private set; }
        public bool IsPrimary { get; private set; }
        public IList<ICharacteristic> Characteristics { get; protected set; }

        public IDevice Device { get; }
    }
}